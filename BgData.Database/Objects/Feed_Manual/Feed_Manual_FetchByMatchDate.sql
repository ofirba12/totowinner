﻿CREATE PROCEDURE [dbo].[Feed_Manual_FetchByMatchDate]
	@FeedDate DATETIME
AS
  DECLARE @D1 DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, @FeedDate))
  DECLARE @D2 DATETIME = DATEADD(dd, 1, DATEDIFF(dd, 0, @FeedDate))

	SELECT * FROM [dbo].[Feed_Manual]
	WHERE [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
