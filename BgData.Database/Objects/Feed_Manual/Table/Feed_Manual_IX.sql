﻿CREATE COLUMNSTORE INDEX [Feed_Manual_IX]
	ON [dbo].[Feed_Manual]
	([SportId], [CountryId], [LeagueId], [HomeId], [AwayId], [GameStart], [LastUpdate])
