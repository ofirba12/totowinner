﻿CREATE PROCEDURE [dbo].[Feed_Manual_Fetch]
	@LastFetched DATETIME = NULL
AS
	SELECT * FROM [dbo].[Feed_Manual]
	WHERE LastUpdate >= ISNULL(@LastFetched, [LastUpdate])
RETURN 0
