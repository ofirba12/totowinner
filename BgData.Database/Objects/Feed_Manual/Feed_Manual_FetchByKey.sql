﻿CREATE PROCEDURE [dbo].[Feed_Manual_FetchByKey]
	@CountryId	INT,
	@LeagueId	INT,
	@HomeId		INT,
	@AwayId		INT,
	@GameTime DATETIME
AS
	SELECT * FROM [dbo].[Feed_Manual]
	WHERE [CountryId]	= @CountryId
		AND [LeagueId]	= @LeagueId
		AND [HomeId]	= @HomeId
		AND [AwayId]	= @AwayId
		AND [GameStart] = @GameTime
RETURN 0
