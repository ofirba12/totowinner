﻿CREATE PROCEDURE [dbo].[Feed_Manual_FetchByLastUpdate]
	@UpdateDate DATETIME
AS
  DECLARE @D1 DATETIME = DATEADD(dd, 0, DATEDIFF(dd, 0, @UpdateDate))
  DECLARE @D2 DATETIME = DATEADD(dd, 1, DATEDIFF(dd, 0, @UpdateDate))

	SELECT * FROM [dbo].[Feed_Manual]
	WHERE [LastUpdate] BETWEEN @D1 AND DATEADD(s, -1, @D2)
RETURN 0
