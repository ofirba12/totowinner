﻿CREATE PROCEDURE [dbo].[FeedBgInfo_Merge]
	@Collection Feed_Bg_Info_UDT READONLY
AS

MERGE dbo.[Feed_Bg_Info] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId] = [Target].[MatchId] 
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[BadMatches]				=[Src].[BadMatches]				
		,[Target].[HomeMatches]				=[Src].[HomeMatches]		
		,[Target].[AwayMatches]				=[Src].[AwayMatches]	
		,[Target].[Head2HeadMatches]		=[Src].[Head2HeadMatches]	
		,[Target].[LastUpdate]				=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[MatchId]
		,[BadMatches]			
		,[HomeMatches]		
		,[AwayMatches]	
		,[Head2HeadMatches]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[MatchId]			
			,[Src].[BadMatches]			
			,[Src].[HomeMatches]		
			,[Src].[AwayMatches]		
			,[Src].[Head2HeadMatches]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO