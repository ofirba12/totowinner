﻿CREATE TYPE [dbo].[Feed_Bg_Info_UDT] AS TABLE(
	[MatchId]						BIGINT					NOT NULL,
	[BadMatches]					INT						NOT NULL,
	[HomeMatches]					INT						NOT NULL,
	[AwayMatches]					INT						NOT NULL,
	[Head2HeadMatches]				INT						NOT NULL,
	[LastUpdate]					DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)