﻿CREATE TABLE [dbo].[Feed_Winner]
(
	[EventId]		INT				NOT NULL,
	[ProgramId]		BIGINT			NOT NULL,
	[SportId]		TINYINT			NOT NULL,
	[TotoId]		INT				NOT NULL,
	[HomeId]		INT				NOT NULL,
	[GuestId]		INT				NOT NULL,
	[BetTypeId]		SMALLINT		NOT NULL,
	[Place]			INT				NOT NULL,
	[Name]			NVARCHAR(600)	NOT NULL,
	[GameStart]		DATETIME		NOT NULL,
	[Status]		NVARCHAR(100)	NULL,
	[HomeOdd]		FLOAT			NULL,
	[DrawOdd]		FLOAT			NULL,
	[AwayOdd]		FLOAT			NULL,
	[Country]		NVARCHAR(200)	NOT NULL,
	[League]		NVARCHAR(600)	NOT NULL,
	[LastUpdate]	DATETIME		NOT NULL
)
