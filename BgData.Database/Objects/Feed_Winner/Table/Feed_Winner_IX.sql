﻿CREATE COLUMNSTORE INDEX [Feed_Winner_IX]
	ON [dbo].[Feed_Winner]
	([SportId], [ProgramId], [HomeId], [GuestId], [GameStart], [BetTypeId], [TotoId], [Status])
