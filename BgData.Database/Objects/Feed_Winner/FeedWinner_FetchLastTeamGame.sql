﻿CREATE PROCEDURE [dbo].[FeedWinner_FetchLastTeamGame]
	@TeamId INT
AS

  SELECT TOP 1 * FROM [dbo].[Feed_Winner] 
  WHERE [HomeId] = @TeamId 
	OR [GuestId] = @TeamId
  ORDER BY [GameStart] DESC

RETURN 0
