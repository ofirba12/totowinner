﻿CREATE PROCEDURE [dbo].[FeedWinner_Merge]
	@Collection Feed_Winner_UDT READONLY
AS

MERGE dbo.[Feed_Winner] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[EventId] = [Target].[EventId] AND 
		[Src].[ProgramId] = [Target].[ProgramId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[SportId]	= [Src].[SportId]
				,[Target].[TotoId]		= [Src].[TotoId]
				,[Target].[HomeId]		= [Src].[HomeId]	
				,[Target].[GuestId]		= [Src].[GuestId]		
				,[Target].[BetTypeId]	= [Src].[BetTypeId]
				,[Target].[Place]		= [Src].[Place]		
				,[Target].[Name]		= [Src].[Name]		
				,[Target].[GameStart]	= [Src].[GameStart]		
				,[Target].[Status]		= [Src].[Status]		
				,[Target].[HomeOdd]		= [Src].[HomeOdd]		
				,[Target].[DrawOdd]		= [Src].[DrawOdd]		
				,[Target].[AwayOdd]		= [Src].[AwayOdd]		
				,[Target].[Country]		= [Src].[Country]
				,[Target].[League]		= [Src].[League]
				,[Target].[LastUpdate]	= [Src].[LastUpdate]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [EventId]
				,[ProgramId]
				,[SportId]
				,[TotoId]
				,[HomeId]			
				,[GuestId]	
				,[BetTypeId]	
				,[Place]			
				,[Name]			
				,[GameStart]
				,[Status]		
				,[HomeOdd]	
				,[DrawOdd]	
				,[AwayOdd]	
				,[Country]		
				,[League]		
				,[LastUpdate]
				)
		VALUES ([Src]. [EventId]
	  			,[Src].[ProgramId]
				,[Src].[SportId]
	  			,[Src].[TotoId]
				,[Src].[HomeId]	
	  			,[Src].[GuestId]	
				,[Src].[BetTypeId]
	  			,[Src].[Place]	
				,[Src].[Name]		
	  			,[Src].[GameStart]
				,[Src].[Status]	
	  			,[Src].[HomeOdd]	
				,[Src].[DrawOdd]	
	  			,[Src].[AwayOdd]	
				,[Src].[Country]	
				,[Src].[League]	
				,[Src].[LastUpdate]
				)
				;

RETURN 0


GO