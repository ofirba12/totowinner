﻿CREATE PROCEDURE [dbo].[FeedWinner_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

  SELECT * FROM [dbo].[Feed_Winner] 
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
