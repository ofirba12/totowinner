﻿CREATE TYPE [dbo].[Feed_Goalserve_BasketballQuarters_TT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[MatchId]		INT						NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[HomeId]		INT						NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SportId], [MatchId], [LeagueId], [HomeId], [AwayId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)