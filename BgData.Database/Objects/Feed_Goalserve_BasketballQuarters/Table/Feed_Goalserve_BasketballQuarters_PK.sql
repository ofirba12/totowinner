﻿ALTER TABLE [dbo].[Feed_Goalserve_BasketballQuarters]
	ADD CONSTRAINT [Feed_Goalserve_BasketballQuarters_PK]
	PRIMARY KEY ([MatchId], [LeagueId], [HomeId], [AwayId])
