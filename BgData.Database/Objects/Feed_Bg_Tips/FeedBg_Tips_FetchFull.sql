﻿CREATE PROCEDURE [dbo].[FeedBg_Tips_FetchFull]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

    SELECT FBT.[MatchId],
	FBT.[Type],
	FBT.[Title],
	FBT.[Value],
	FBT.[Stars],
	FBT.[Description],
	FBT.[FreezeTip],
	FBT.[LastUpdate] AS TipLastUpdate,
	FB.[Source],
	FB.[SportId],
	FB.[CountryId],
	FB.[LeagueId],
	FB.[HomeId],
	FB.[AwayId],
	FB.[GameStart],
	FB.[Status],
	FB.[IsCup],
	FB.[HomeOdd],
	FB.[DrawOdd],
	FB.[AwayOdd],
	FB.[HomeScore],
	FB.[AwayScore],
	FB.[LastUpdate] AS FeedLastUpdate 
	FROM [dbo].[Feed_Bg_Tips] FBT
	JOIN [dbo].[Feed_Bg] FB 
		ON  FB.MatchId = FBT.MatchId
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
