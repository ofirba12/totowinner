﻿CREATE TABLE [dbo].[Feed_Bg_Tips]
(
	[MatchId]						BIGINT					NOT NULL,
	[Type]							TINYINT					NOT NULL,
	[Title]							NVARCHAR(300)			NOT NULL,
	[Value]							INT						NOT NULL,
	[Stars]							TINYINT					NOT NULL,
	[Description]					NVARCHAR(MAX)			NOT NULL,
	[FreezeTip]						BIT						NOT NULL,
	[LastUpdate]					DATETIME				NOT NULL

)
