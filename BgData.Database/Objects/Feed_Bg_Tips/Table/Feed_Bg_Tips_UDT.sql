﻿CREATE TYPE [dbo].[Feed_Bg_Tips_UDT] AS TABLE(
	[MatchId]				BIGINT					NOT NULL,
	[Type]					TINYINT					NOT NULL,
	[Title]					NVARCHAR(300)			NOT NULL,
	[Value]					INT						NOT NULL,
	[Stars]					TINYINT					NOT NULL,
	[Description]			VARCHAR(MAX)			NOT NULL,
	[FreezeTip]				BIT						NOT NULL,
	[LastUpdate]			DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId], [Type] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)