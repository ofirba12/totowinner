﻿CREATE PROCEDURE [dbo].[FeedBgTips_Merge]
	@Collection Feed_Bg_Tips_UDT READONLY
AS

MERGE dbo.[Feed_Bg_Tips] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId]				= [Target].[MatchId] AND
		[Src].[Type]				= [Target].[Type]
	WHEN MATCHED AND [Target].[FreezeTip]=0 THEN 
		UPDATE SET
		[Target].[Title]			=[Src].[Title]		
		,[Target].[Value]			=[Src].[Value]			
		,[Target].[Stars]			=[Src].[Stars]			
		,[Target].[Description]		=[Src].[Description]			
		,[Target].[FreezeTip]		=[Src].[FreezeTip]			
		,[Target].[LastUpdate]		=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[MatchId]
		,[Type]
		,[Title]		
		,[Value]		
		,[Stars]		
		,[Description]	
		,[LastUpdate]		
				)
		VALUES (
			[Src].[MatchId]			
			,[Src].[Type]
			,[Src].[Title]	
			,[Src].[Value]		
			,[Src].[Stars]	
			,[Src].[Description]	
			,[Src].[FreezeTip]
			,[Src].[LastUpdate]	
				)
				;

RETURN 0

GO