﻿CREATE TYPE [dbo].[Feed_Bg_Quarters_UDT] AS TABLE(
	[MatchId]		BIGINT					NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[MatchId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)