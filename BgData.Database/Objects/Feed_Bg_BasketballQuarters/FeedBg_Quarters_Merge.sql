﻿CREATE PROCEDURE [dbo].[FeedBg_Quarters_Merge]
	@Collection Feed_Bg_Quarters_UDT READONLY
AS

MERGE dbo.[Feed_Bg_BasketballQuarters] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId] = [Target].[MatchId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[HomeQ1]	= [Src].[HomeQ1]
				,[Target].[AwayQ1]		= [Src].[AwayQ1]
				,[Target].[HomeQ2]		= [Src].[HomeQ2]
				,[Target].[AwayQ2]		= [Src].[AwayQ2]	
				,[Target].[HomeQ3]		= [Src].[HomeQ3]	
				,[Target].[AwayQ3]		= [Src].[AwayQ3]
				,[Target].[HomeQ4]		= [Src].[HomeQ4]	
				,[Target].[AwayQ4]		= [Src].[AwayQ4]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [MatchId]	
				,[HomeQ1]	
				,[AwayQ1]		
				,[HomeQ2]
				,[AwayQ2]
				,[HomeQ3]	
				,[AwayQ3]	
				,[HomeQ4]	
				,[AwayQ4]
				)
		VALUES ([Src].[MatchId]
	  			,[Src].[HomeQ1]
				,[Src].[AwayQ1]		
				,[Src].[HomeQ2]
				,[Src].[AwayQ2]
				,[Src].[HomeQ3]	
				,[Src].[AwayQ3]	
				,[Src].[HomeQ4]	
				,[Src].[AwayQ4]
				)
				;

RETURN 0