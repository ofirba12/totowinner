﻿CREATE PROCEDURE [dbo].[BgTeams_BlackFilter_Delete]
	@TeamId INT,
	@FilterBgType TINYINT
AS
	DELETE FROM BgTeams_BlackFilter WHERE TeamId = @TeamId AND FilterBgType = @FilterBgType

RETURN 0
