﻿CREATE TABLE [dbo].[BgTeams_BlackFilter]
(
	[TeamId]			INT			NOT NULL,
	[FilterBgType]		TINYINT		NOT NULL,
	[FilterBgId]		INT			NOT NULL,
)
/*
[FilterBgType] BgMapperType
	Countries = 1
	Leagues = 2
*/
