﻿CREATE PROCEDURE [dbo].[BgTeams_BlackFilter_Merge]
	@TeamId			INT,
	@FilterBgType	TINYINT,
	@FilterBgId		INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgTeams_BlackFilter] AS [Target]
	USING ( SELECT @TeamId		AS "TeamId"
				,@FilterBgType	AS "FilterBgType"
				,@FilterBgId	AS "FilterBgId"
	) AS [Source]	ON [Source].[TeamId]	= [Target].[TeamId]
		AND [Source].[FilterBgType]	= [Target].[FilterBgType]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[FilterBgId] = [Source].[FilterBgId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TeamId]
			,[FilterBgType]	
			,[FilterBgId]
			)
		VALUES ([Source].[TeamId]
			,[Source].[FilterBgType]	
			,[Source].[FilterBgId]
			);
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
