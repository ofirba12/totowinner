﻿CREATE PROCEDURE [dbo].BgManualMapperMerge
	@MapperParameter BgManualMapperUDT READONLY
AS

MERGE dbo.[BgManualMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND
		   [Source].[ManualName] = [Target].[ManualName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
				,[ManualName]
				)
		VALUES ([Source].[BgId]
				,[Source].[ManualName]
				)
				;
RETURN 0


GO