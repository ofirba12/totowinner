﻿ALTER TABLE [dbo].[BgManualMapper]
	ADD CONSTRAINT [FK_BgManualMapper_BgMapper_BgId]
	FOREIGN KEY (BgId)
	REFERENCES [BgMapper] (BgId)
