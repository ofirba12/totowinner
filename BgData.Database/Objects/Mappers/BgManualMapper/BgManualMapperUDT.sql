﻿CREATE TYPE [dbo].BgManualMapperUDT AS TABLE
(
	[BgId]					INT NOT NULL,
	[ManualName]			NVARCHAR(600) NOT NULL
)