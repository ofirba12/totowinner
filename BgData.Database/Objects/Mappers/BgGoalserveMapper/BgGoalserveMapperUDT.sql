﻿CREATE TYPE [dbo].BgGoalserveMapperUDT AS TABLE
(
	[BgId]					INT NOT NULL,
	[GoalserveId]			INT NULL,
	[GoalserveName]			NVARCHAR(600) NOT NULL
)