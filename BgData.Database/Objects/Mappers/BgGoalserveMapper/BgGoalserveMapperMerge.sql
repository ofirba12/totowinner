﻿CREATE PROCEDURE [dbo].BgGoalserveMapperMerge
	@MapperParameter BgGoalserveMapperUDT READONLY
AS

MERGE dbo.[BgGoalserveMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND
		   ISNULL([Source].[GoalserveId],0) = ISNULL([Target].[GoalserveId],0) AND
		   [Source].[GoalserveName] = [Target].[GoalserveName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
	  			,[GoalserveId]
				,[GoalserveName]
				)
		VALUES ([Source].[BgId]
				,[Source].[GoalserveId]
				,[GoalserveName]
				)
				;
RETURN 0


GO