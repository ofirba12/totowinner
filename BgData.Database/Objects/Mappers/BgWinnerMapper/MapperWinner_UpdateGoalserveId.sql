﻿CREATE PROCEDURE [dbo].[MapperWinner_UpdateGoalserveId]
	@SportId		TINYINT,
	@WinnerId		INT,
	@GoalserveId	INT
AS
	UPDATE BgWinnerMapper 
	SET GoalserveId = @GoalserveId
	WHERE SportId = @SportId AND WinnerId = @WinnerId
RETURN 0
