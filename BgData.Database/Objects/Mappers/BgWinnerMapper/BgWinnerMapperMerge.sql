﻿CREATE PROCEDURE [dbo].[BgWinnerMapperMerge]
	@MapperParameter BgWinnerMapperUDT READONLY
AS

MERGE dbo.[BgWinnerMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[SportId] = [Target].[SportId] AND 
		   [Source].[WinnerId] = [Target].[WinnerId] 
	--WHEN MATCHED THEN 
	--	UPDATE SET	[Target].[WinnerName] = [Source].[WinnerName],				   
	--				[Target].[BgId] = [Source].[BgId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SportId]
	  			,[WinnerId]
				,[WinnerName]
				,[BgId]
				,[GoalserveId]
				)
		VALUES ([Source].[SportId]
				,[Source].[WinnerId]
	  			,[Source].[WinnerName]
				,NULL
				,NULL
				)
				;

RETURN 0


GO