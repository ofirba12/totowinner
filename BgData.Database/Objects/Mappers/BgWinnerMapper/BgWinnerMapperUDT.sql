﻿CREATE TYPE [dbo].[BgWinnerMapperUDT] AS TABLE
(
	[SportId]		TINYINT			NOT NULL,
	[WinnerId]		INT				NOT NULL,
	[WinnerName]	NVARCHAR(600)	NOT NULL
PRIMARY KEY CLUSTERED 
(
	[SportId],[WinnerId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)