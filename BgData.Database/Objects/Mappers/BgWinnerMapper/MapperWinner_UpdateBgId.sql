﻿CREATE PROCEDURE [dbo].[MapperWinner_UpdateBgId]
	@Sport		TINYINT,
	@WinnerId	INT,
	@BgId		INT
AS
	UPDATE BgWinnerMapper 
	SET BgId = @BgId
	WHERE SportId = @Sport AND WinnerId = @WinnerId
RETURN 0
