﻿CREATE TABLE [dbo].[BgWinnerMapper]
(
	[SportId]		TINYINT			NOT NULL,
	[WinnerId]		INT				NOT NULL,
	[WinnerName]	NVARCHAR(600)	NOT NULL,
	[BgId]			INT				NULL,
	[GoalserveId]	INT				NULL
)
