﻿CREATE PROCEDURE [dbo].[BgMapper_Fetch]
AS
	SELECT BG.BgId, BG.BgSport, BG.BgType, BG.BgName, BGGM.GoalserveId, BGGM.GoalserveName, BGMM.ManualName FROM BgMapper BG
	LEFT JOIN BgGoalserveMapper BGGM
		ON BG.BgId = BGGM.BgId
	LEFT JOIN BgManualMapper BGMM
		ON BG.BgId = BGMM.BgId
RETURN 0
