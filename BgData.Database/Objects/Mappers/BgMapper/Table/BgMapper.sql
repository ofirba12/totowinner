﻿CREATE TABLE [dbo].[BgMapper]
(
	[BgId]			INT NOT NULL,
	[BgSport]		TINYINT NOT NULL,
	[BgType]		TINYINT NOT NULL,
	[BgName]		NVARCHAR(600) NULL
)
--countries start with 100
--leagues start with 10000
--teams start with 100000