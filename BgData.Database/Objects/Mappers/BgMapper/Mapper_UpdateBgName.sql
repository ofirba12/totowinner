﻿CREATE PROCEDURE [dbo].[Mapper_UpdateBgName]
	@BgId int,
	@BgName NVARCHAR(600) NULL
AS
	UPDATE BgMapper 
	SET BgName = @BgName
	WHERE BgId = @BgId
RETURN 0
