﻿CREATE TYPE [dbo].[BgMapperUDT] AS TABLE
(
	[BgId]					INT NOT NULL,
	[BgSport]				TINYINT NOT NULL,
	[BgType]				TINYINT NOT NULL,
	[BgName]				NVARCHAR(600) NULL
PRIMARY KEY CLUSTERED 
(
	[BgId],[BgSport],[BgType]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)