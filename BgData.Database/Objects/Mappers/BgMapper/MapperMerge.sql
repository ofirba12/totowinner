﻿CREATE PROCEDURE [dbo].[MapperMerge]
	@MapperParameter BgMapperUDT READONLY
AS

MERGE dbo.[BgMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND 
		   [Source].[BgType] = [Target].[BgType] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[BgName] = [Source].[BgName]				   
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
	  			,[BgType]
				,[BgName]
				)
		VALUES ([Source].[BgId]
				,[Source].[BgType]
	  			,[Source].[BgName]
				)
				;

RETURN 0


GO