﻿CREATE PROCEDURE [dbo].[BgSystem_Merge]
	@Name	VARCHAR(900),
	@Value	VARCHAR(900),
	@ValueType TINYINT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgSystem] AS [Target]
	USING ( SELECT	 @Name		AS	"Name"
					,@Value		AS	"Value"
					,@ValueType AS	"ValueType"
	) AS [Source]	ON [Source].[Name]	= [Target].[Name]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Value]		= [Source].[Value],
				   [Target].[ValueType]		= [Source].[ValueType]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Name]
				,[Value]
				,[ValueType])
		VALUES ([Source].[Name]
				,[Source].[Value]
				,[Source].[ValueType]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
