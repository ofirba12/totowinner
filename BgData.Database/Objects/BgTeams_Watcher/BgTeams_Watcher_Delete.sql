﻿CREATE PROCEDURE [dbo].[BgTeams_Watcher_Delete]
	@TeamId INT
AS
	DELETE FROM BgTeams_Watcher WHERE TeamId = @TeamId
RETURN 0
