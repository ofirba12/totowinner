﻿CREATE PROCEDURE [dbo].[BgTeams_Watcher_Merge]
	@Collection BgTeams_Watcher_UDT READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgTeams_Watcher] AS [Target]
	USING @Collection AS [Source]	
		ON	[Source].[TeamId] = [Target].[TeamId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[TotalShows] = [Source].[TotalShows]
				,[Target].[MissingOdds] = [Source].[MissingOdds] 
				,[Target].[MissingResult] = [Source].[MissingResult]
				,[Target].[LastUpdate] = GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TeamId]
			,[TotalShows]
			,[MissingOdds]
			,[MissingResult]	
			,[LastUpdate]
			)
		VALUES ([Source].[TeamId]
			,[Source].[TotalShows]	
			,[Source].[MissingOdds]
			,[Source].[MissingResult]
			,GetDate()
			);
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
