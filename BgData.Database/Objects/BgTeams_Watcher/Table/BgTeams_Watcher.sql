﻿CREATE TABLE [dbo].[BgTeams_Watcher]
(
	[TeamId]		INT			NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL,
	[TotalShows]	INT			NOT NULL,
	[MissingOdds]	INT			NOT NULL,
	[MissingResult]	INT			NOT NULL
)
