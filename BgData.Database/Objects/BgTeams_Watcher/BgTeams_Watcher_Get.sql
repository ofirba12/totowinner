﻿CREATE PROCEDURE [dbo].[BgTeams_Watcher_Get]
	@SportId TINYINT
AS
	SELECT W.* FROM BgTeams_Watcher W
	JOIN BgMapper M ON M.BgId = W.TeamId
	WHERE M.BgSport = @SportId AND M.BgType = 3
RETURN 0
