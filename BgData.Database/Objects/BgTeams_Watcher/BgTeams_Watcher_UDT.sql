﻿CREATE TYPE [dbo].[BgTeams_Watcher_UDT] AS TABLE
(
	[TeamId]					INT NOT NULL,
	[TotalShows]				INT NOT NULL,
	[MissingOdds]				INT NOT NULL,
	[MissingResult]				INT NOT NULL
PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)