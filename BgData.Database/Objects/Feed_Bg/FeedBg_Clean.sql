﻿CREATE PROCEDURE [dbo].[FeedBg_Clean]
	@Collection BgMatchIdsUDT READONLY
AS

DELETE dbo.[Feed_Bg] 
	FROM dbo.[Feed_Bg]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg].[MatchId] = [Target].[MatchId] 

DELETE dbo.[Feed_Bg_BasketballQuarters] 
	FROM dbo.[Feed_Bg_BasketballQuarters]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg_BasketballQuarters].[MatchId] = [Target].[MatchId] 
RETURN 0
GO