﻿CREATE PROCEDURE [dbo].[FeedBg_Delete]
	@Collection Feed_Bg_UDT READONLY
AS

DELETE dbo.[Feed_Bg] 
	FROM dbo.[Feed_Bg]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg].[SportId] = [Target].[SportId] AND
		[Feed_Bg].[CountryId] = [Target].[CountryId] AND
		[Feed_Bg].[LeagueId] = [Target].[LeagueId] AND
		[Feed_Bg].[HomeId] = [Target].[HomeId] AND
		[Feed_Bg].[AwayId] = [Target].[AwayId] AND
		CONVERT(DATE, [Feed_Bg].[GameStart]) = CONVERT(DATE, [Target].[GameStart]) --date only
RETURN 0
GO