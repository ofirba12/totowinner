﻿CREATE PROCEDURE [dbo].[FeedBg_FilterFetch]
	@SportId INT,
	@DaysAdd INT, -- 0: Today, -1: Yesterday, 1: Tommorrow
    @GmtOffset INT,
    @CountryId INT = NULL,
    @LeagueId INT = NULL
AS
    DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
    DECLARE @FROM DATETIME = DATEADD(hh, @GmtOffset, @D1)

    DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))
    DECLARE @TO DATETIME = DATEADD(hh, @GmtOffset, @D2)

  SELECT FB.*,
      FBB.[HomeQ1],
      FBB.[AwayQ1],
      FBB.[HomeQ2],
      FBB.[AwayQ2],
      FBB.[HomeQ3],
      FBB.[AwayQ3],
      FBB.[HomeQ4],
      FBB.[AwayQ4] 
  FROM [dbo].[Feed_Bg] FB
  LEFT JOIN [dbo].[Feed_Bg_BasketballQuarters] FBB
    ON FBB.MatchId = FB.MatchId  
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @FROM AND DATEADD(s, -1, @TO)
    AND [CountryId] = ISNULL(@CountryId, [CountryId])
    AND [LeagueId] = ISNULL(@LeagueId, [LeagueId])

RETURN 0
