﻿CREATE TYPE [dbo].[Feed_Bg_UDT] AS TABLE(
	[Source]		TINYINT					NOT NULL,
	[SportId]		INT						NOT NULL,
	[CountryId]		INT						NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[HomeId]		INT						NOT NULL,
	[AwayId]		INT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[IsCup]			BIT						NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[SportId],[CountryId],[LeagueId],[HomeId],[AwayId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)