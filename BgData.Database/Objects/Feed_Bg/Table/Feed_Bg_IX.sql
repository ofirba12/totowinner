﻿CREATE COLUMNSTORE INDEX [Feed_Bg_IX]
	ON [dbo].[Feed_Bg]
	([SportId], [CountryId], [LeagueId], [HomeId], [AwayId], [GameStart])
