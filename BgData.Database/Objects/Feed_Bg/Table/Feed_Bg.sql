﻿CREATE TABLE [dbo].[Feed_Bg]
(
	[MatchId]				BIGINT					IDENTITY(500000,1)	NOT NULL,
	[Source]				TINYINT					NOT NULL,			
	[SportId]				INT						NOT NULL,
	[CountryId]				INT						NULL,
	[LeagueId]				INT						NOT NULL,
	[HomeId]				INT						NOT NULL,
	[AwayId]				INT						NOT NULL,
	[GameStart]				DATETIME				NOT NULL,
	[Status]				VARCHAR(100)			NULL,
	[IsCup]					BIT						NOT NULL,
	[HomeOdd]				FLOAT					NULL,
	[DrawOdd]				FLOAT					NULL,
	[AwayOdd]				FLOAT					NULL,
	[HomeScore]				INT						NULL,
	[AwayScore]				INT						NULL,
	[LastUpdate]			DATETIME				NOT NULL
)
