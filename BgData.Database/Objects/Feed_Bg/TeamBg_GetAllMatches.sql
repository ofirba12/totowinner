﻿CREATE PROCEDURE [dbo].[TeamBg_GetAllMatches]
    @TeamId INT
AS

  SELECT FB.*,
      FBB.[HomeQ1],
      FBB.[AwayQ1],
      FBB.[HomeQ2],
      FBB.[AwayQ2],
      FBB.[HomeQ3],
      FBB.[AwayQ3],
      FBB.[HomeQ4],
      FBB.[AwayQ4] 
  FROM [dbo].[Feed_Bg] FB
  LEFT JOIN [dbo].[Feed_Bg_BasketballQuarters] FBB
    ON FBB.MatchId = FB.MatchId  
  WHERE [HomeId] = @TeamId OR 
    [AwayId] = @TeamId

RETURN 0
