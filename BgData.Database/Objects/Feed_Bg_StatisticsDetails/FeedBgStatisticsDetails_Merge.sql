﻿CREATE PROCEDURE [dbo].[FeedBgStatisticsDetails_Merge]
	@Collection Feed_Bg_StatisticsDetails_UDT READONLY
AS

MERGE dbo.[Feed_Bg_StatisticsDetails] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId]				= [Target].[MatchId] AND
		[Src].[Type]				= [Target].[Type] AND
		[Src].[StatisticIndex]		= [Target].[StatisticIndex] AND
		[Src].[Population]			= [Target].[Population]
	WHEN MATCHED THEN 
		UPDATE SET
		[Target].[Lines]			=[Src].[Lines]		
		,[Target].[Percent]			=[Src].[Percent]			
		,[Target].[LastUpdate]		=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[MatchId]
		,[Type]
		,[StatisticIndex]	
		,[Population]		
		,[Lines]	
		,[Percent]	
		,[LastUpdate]		
				)
		VALUES (
			[Src].[MatchId]			
			,[Src].[Type]
			,[Src].[StatisticIndex]	
			,[Src].[Population]		
			,[Src].[Lines]	
			,[Src].[Percent]	
			,[Src].[LastUpdate]	
				)
				;

RETURN 0

GO