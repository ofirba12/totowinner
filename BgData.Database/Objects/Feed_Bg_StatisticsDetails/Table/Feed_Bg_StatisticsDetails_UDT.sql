﻿CREATE TYPE [dbo].[Feed_Bg_StatisticsDetails_UDT] AS TABLE(
	[MatchId]						BIGINT					NOT NULL,
	[Type]							TINYINT					NOT NULL,
	[StatisticIndex]				TINYINT					NOT NULL,
	[Population]					TINYINT					NOT NULL,
	[Lines]							INT						NULL,
	[Percent]						TINYINT					NULL,
	[LastUpdate]					DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId], [Type], [StatisticIndex], [Population] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)