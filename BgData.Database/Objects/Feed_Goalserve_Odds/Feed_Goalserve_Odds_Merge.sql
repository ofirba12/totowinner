﻿CREATE PROCEDURE [dbo].[Feed_Goalserve_Odds_Merge]
	@Collection Feed_Goalserve_Odds_UDT READONLY
AS

DECLARE @CurrentTS DATETIME = GetDate();

MERGE dbo.[Feed_Goalserve_Odds] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[SportId] = [Target].[SportId] AND 
		[Src].[MatchId] = [Target].[MatchId] AND
		[Src].[Bookmaker] = [Target].[Bookmaker] AND
		[Src].[HomeOdd] = [Target].[HomeOdd] AND
		[Src].[DrawOdd] = [Target].[DrawOdd] AND
		[Src].[AwayOdd] = [Target].[AwayOdd] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[LastUpdate] = @CurrentTS
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SportId]
				,[MatchId]
				,[Bookmaker]
				,[HomeOdd]	
				,[DrawOdd]	
				,[AwayOdd]	
				,[Inserted]
				)
		VALUES ([Src].[SportId]
	  			,[Src].[MatchId]
				,[Src].[Bookmaker]
	  			,[Src].[HomeOdd]	
				,[Src].[DrawOdd]	
	  			,[Src].[AwayOdd]	
				,@CurrentTS
				)
				;

RETURN 0


GO