﻿CREATE PROCEDURE [dbo].[Feed_Goalserve_Odds_Fetch]
	@SportId INT,
	@GoalserveHomeId INT,
	@GoalserveAwayId INT,
	@GameStart DATETIME
AS
	SELECT FGO.* 
	FROM [dbo].[Feed_Goalserve] FG
	JOIN [Feed_Goalserve_Odds] FGO
	ON FG.MatchId = FGO.MatchId
	WHERE FG.SportId = @SportId and FG.HomeId=@GoalserveHomeId and AwayId=@GoalserveAwayId AND GameStart=@GameStart

RETURN 0
