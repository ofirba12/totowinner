﻿CREATE TABLE [dbo].[Feed_Goalserve_Odds]
(
	[SportId]		INT						NOT NULL,
	[MatchId]		INT						NOT NULL,
	[Bookmaker]		NVARCHAR(200)			NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[Inserted]		DATETIME				NOT NULL,
	[LastUpdate]	DATETIME				NULL
)
