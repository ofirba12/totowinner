﻿CREATE TYPE [dbo].[Feed_Goalserve_Odds_UDT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[MatchId]		INT						NOT NULL,
	[Bookmaker]		NVARCHAR(200)			NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	PRIMARY KEY CLUSTERED 
(
	[SportId],[MatchId],[Bookmaker]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)