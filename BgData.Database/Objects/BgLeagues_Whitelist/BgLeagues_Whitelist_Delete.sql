﻿CREATE PROCEDURE [dbo].[BgLeagues_Whitelist_Delete]
	@LeagueId INT
AS
	DELETE FROM BgLeagues_Whitelist WHERE LeagueId = @LeagueId

RETURN 0
