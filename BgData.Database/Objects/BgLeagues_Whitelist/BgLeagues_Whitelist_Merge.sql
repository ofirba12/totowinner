﻿CREATE PROCEDURE [dbo].[BgLeagues_Whitelist_Merge]
	@LeagueId			INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgLeagues_Whitelist] AS [Target]
	USING ( SELECT @LeagueId		AS "LeagueId"
	) AS [Source]	ON [Source].[LeagueId]	= [Target].[LeagueId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([LeagueId]
			)
		VALUES ([Source].[LeagueId]
			);
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
