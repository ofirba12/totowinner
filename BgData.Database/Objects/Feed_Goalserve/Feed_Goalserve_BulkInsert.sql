﻿CREATE PROCEDURE [dbo].[Feed_Goalserve_BulkInsert]
	@Feed AS [Feed_Goalserve_TT] READONLY,
	@QuartersFeed AS [Feed_Goalserve_BasketballQuarters_TT] READONLY
AS
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @LastUpdateTS DATETIME = GetDate();

	INSERT INTO [Feed_Goalserve](
		[MatchId],
		[SportId]	 ,
		[Status]	 ,
		[CountryId]	 ,
		[Country]	 ,
		[League]	 ,
		[LeagueId]	 ,
		[IsCup]		 ,
		[GameStart]	 ,
		[Home]		 ,
		[HomeId]	 ,
		[Away]		 ,
		[AwayId]	 ,
		[HomeOdd]	 ,
		[DrawOdd]	 ,
		[AwayOdd]	 ,
		[HomeScore]	 ,
		[AwayScore]	 , 
		[LastUpdate]
	)
	SELECT [MatchId],
			[SportId]	,
			[Status]	,
			[CountryId]	,
			[Country]	,
			[League]	,
			[LeagueId]	,
			[IsCup]		,
			[GameStart]	,
			[Home]		,
			[HomeId]	,
			[Away]		,
			[AwayId]	,
			[HomeOdd]	,
			[DrawOdd]	,
			[AwayOdd]	,
			[HomeScore]	,
			[AwayScore]	,
			[LastUpdate] = @LastUpdateTS
	FROM @Feed;

	INSERT INTO [Feed_Goalserve_BasketballQuarters](
		[MatchId],
		[LeagueId],
		[HomeId],
		[AwayId],
		[HomeQ1],
		[AwayQ1],
		[HomeQ2],
		[AwayQ2],
		[HomeQ3],
		[AwayQ3],
		[HomeQ4],
		[AwayQ4]
	)
	SELECT [MatchId],
		[LeagueId],
		[HomeId],
		[AwayId],
		[HomeQ1], 
		[AwayQ1], 
		[HomeQ2], 
		[AwayQ2], 
		[HomeQ3], 
		[AwayQ3], 
		[HomeQ4], 
		[AwayQ4]
	FROM @QuartersFeed

	--SELECT [MatchId], [HomeQ1], [AwayQ1], [HomeQ2], [AwayQ2], [HomeQ3], [AwayQ3], [HomeQ4], [AwayQ4]
	--FROM (
	--	SELECT [MatchId], F.[SportId], F.[CountryId], F.[LeagueId], F.[GameStart], F.[HomeId], F.[AwayId], [HomeQ1], [AwayQ1], [HomeQ2], [AwayQ2], [HomeQ3], [AwayQ3], [HomeQ4], [AwayQ4]
	--	FROM [Feed_Goalserve] FG
	--	JOIN @Feed F ON FG.[SportId] = F.[SportId] 
	--		AND FG.[LeagueId] = F.[LeagueId]
	--		AND FG.[GameStart] = F.[GameStart]
	--		AND FG.[HomeId] = F.[HomeId]
	--		AND FG.[AwayId] = F.[AwayId]
	--	JOIN @QuartersFeed QF ON QF.[SportId] = F.[SportId]
	--		AND QF.[LeagueId] = F.[LeagueId]
	--		AND QF.[HomeId] = F.[HomeId]
	--		AND QF.[AwayId] = F.[AwayId]
	--) AS JoinedFeed;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
