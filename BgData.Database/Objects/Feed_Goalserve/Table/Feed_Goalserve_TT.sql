﻿CREATE TYPE [dbo].[Feed_Goalserve_TT] AS TABLE(
	[MatchId]		INT						NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NULL,
	[League]		NVARCHAR(600)			NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	PRIMARY KEY CLUSTERED 
(
	[SportId],[MatchId],[LeagueId],[HomeId],[AwayId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)