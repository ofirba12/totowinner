﻿CREATE TABLE [dbo].[Feed_Goalserve]
(
	[MatchId]		INT						NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NOT NULL,
	[League]		NVARCHAR(600)			NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NOT NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL
)
