﻿CREATE COLUMNSTORE INDEX [Feed_Goalserve_IX]
	ON [dbo].[Feed_Goalserve]
	([SportId], [CountryId], [LeagueId], [HomeId], [AwayId], [GameStart])
