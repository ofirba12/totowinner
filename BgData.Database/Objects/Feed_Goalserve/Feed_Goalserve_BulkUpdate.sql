﻿CREATE PROCEDURE [dbo].[Feed_Goalserve_BulkUpdate]
	@Feed AS [Feed_Goalserve_TT] READONLY,
    @QuartersFeed AS [Feed_Goalserve_BasketballQuarters_TT] READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
    DECLARE @LastUpdateTS DATETIME = GetDate();

    UPDATE Feed_Goalserve
    SET
        Feed_Goalserve.[Status]    = ISNULL(F.[Status],		FG.[Status]),
        Feed_Goalserve.[HomeOdd]   = ISNULL(F.[HomeOdd],    FG.[HomeOdd]),
        Feed_Goalserve.[DrawOdd]   = ISNULL(F.[DrawOdd],    FG.[DrawOdd]),
        Feed_Goalserve.[AwayOdd]   = ISNULL(F.[AwayOdd],    FG.[AwayOdd]),
        Feed_Goalserve.[HomeScore] = ISNULL(F.[HomeScore],  FG.[HomeScore]),
        Feed_Goalserve.[AwayScore] = ISNULL(F.[AwayScore],  FG.[AwayScore]),
        Feed_Goalserve.[LastUpdate]= @LastUpdateTS
    FROM Feed_Goalserve FG
        INNER JOIN @Feed F
        ON FG.MatchId = F.MatchId AND 
            FG.SportId = F.SportId AND
            FG.LeagueId = F.LeagueId AND 
            FG.HomeId = F.HomeId AND
            FG.AwayId = F.AwayId 

MERGE dbo.[Feed_Goalserve_BasketballQuarters] AS [Target]
	USING @QuartersFeed AS [Source]	
		ON [Source].[MatchId] = [Target].[MatchId] AND 
           [Source].LeagueId = [Target].LeagueId AND 
           [Source].HomeId = [Target].HomeId AND
           [Source].AwayId = [Target].AwayId 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[HomeQ1] = [Source].[HomeQ1],
				   [Target].[HomeQ2] = [Source].[HomeQ2],
				   [Target].[HomeQ3] = [Source].[HomeQ3],
				   [Target].[HomeQ4] = [Source].[HomeQ4],
				   [Target].[AwayQ1] = [Source].[AwayQ1],
				   [Target].[AwayQ2] = [Source].[AwayQ2],
				   [Target].[AwayQ3] = [Source].[AwayQ3],
				   [Target].[AwayQ4] = [Source].[AwayQ4]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [MatchId]
	  			,[LeagueId]
				,[HomeId]
				,[AwayId]
				,[HomeQ1]
				,[HomeQ2]
				,[HomeQ3]
				,[HomeQ4]
				,[AwayQ1]
				,[AwayQ2]
				,[AwayQ3]
				,[AwayQ4]
				)
		VALUES ([Source].[MatchId]
	  			,[Source].[LeagueId]
				,[Source].[HomeId]
				,[Source].[AwayId]
				,[Source].[HomeQ1]
				,[Source].[HomeQ2]
				,[Source].[HomeQ3]
				,[Source].[HomeQ4]
				,[Source].[AwayQ1]
				,[Source].[AwayQ2]
				,[Source].[AwayQ3]
				,[Source].[AwayQ4]
				)
				;


    --UPDATE Feed_Goalserve_BasketballQuarters
    --SET
    --    Feed_Goalserve_BasketballQuarters.HomeQ1 = QF.HomeQ1,
    --    Feed_Goalserve_BasketballQuarters.HomeQ2 = QF.HomeQ2,
    --    Feed_Goalserve_BasketballQuarters.HomeQ3 = QF.HomeQ3,
    --    Feed_Goalserve_BasketballQuarters.HomeQ4 = QF.HomeQ4,
    --    Feed_Goalserve_BasketballQuarters.AwayQ1 = QF.AwayQ1,
    --    Feed_Goalserve_BasketballQuarters.AwayQ2 = QF.AwayQ2,
    --    Feed_Goalserve_BasketballQuarters.AwayQ3 = QF.AwayQ3,
    --    Feed_Goalserve_BasketballQuarters.AwayQ4 = QF.AwayQ4
    --FROM Feed_Goalserve_BasketballQuarters FGB
    --    INNER JOIN @QuartersFeed QF
    --    ON FGB.MatchId = QF.MatchId 

		--INNER JOIN (
		--		SELECT FG.MatchId, QF.* FROM
		--			Feed_Goalserve FG
		--		INNER JOIN  @QuartersFeed QF
		--		ON QF.SportId = FG.SportId AND 
		--			QF.LeagueId = FG.LeagueId AND 
		--			QF.HomeId = FG.HomeId AND
		--			QF.AwayId = FG.AwayId
		--		INNER JOIN @Feed F
		--		ON FG.SportId = F.SportId AND 
		--			FG.LeagueId = F.LeagueId AND 
		--			FG.HomeId = F.HomeId AND
		--			FG.AwayId = F.AwayId AND
		--			FG.GameStart = F.GameStart
		--) A
		--ON 
		--A.MatchId = FGB.MatchId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END