﻿CREATE PROCEDURE [dbo].[USP_Sync]
AS
BEGIN
	DECLARE @Entity_BK_BEFORE##COUNTER BIGINT; 
	DECLARE @Entity_BK_AFTER##COUNTER BIGINT; 
	DECLARE @Entity_REMOTE##COUNTER BIGINT; 

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[Feed_Goalserve]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [Feed_Goalserve_BK]
	EXEC [USP_FeedGoalserve__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [Feed_Goalserve_BK]
	PRINT '[Feed_Goalserve] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[Feed_Goalserve_BasketballQuarters]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [Feed_Goalserve_BasketballQuarters_BK]
	EXEC [USP_Feed_Goalserve_BasketballQuarters__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [Feed_Goalserve_BasketballQuarters_BK]
	PRINT '[Feed_Goalserve_BasketballQuarters] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[BgMapper]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [BgMapper_BK]
	EXEC [USP_BgMapper__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [BgMapper_BK]
	PRINT '[BBgMapper] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[BgGoalserveMapper]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [BgGoalserveMapper_BK]
	EXEC [USP_BgGoalserveMapper__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [BgGoalserveMapper_BK]
	PRINT '[BgGoalserveMapper] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[BgManualMapper]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [BgManualMapper_BK]
	EXEC [USP_BgManualMapper__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [BgManualMapper_BK]
	PRINT '[BgManualMapper] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[Feed_Manual]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [Feed_Manual_BK]
	EXEC [USP_Feed_Manual__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [Feed_Manual_BK]
	PRINT '[Feed_Manual] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[Feed_Bg]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [Feed_Bg_BK]
	EXEC [USP_Feed_Bg__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [Feed_Bg_BK]
	PRINT '[Feed_Bg] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	SELECT @Entity_REMOTE##COUNTER=count(*) from [3.125.32.136].[BgData].[dbo].[BgTeams_BlackFilter]
	SELECT @Entity_BK_BEFORE##COUNTER=count(*) from [BgTeams_BlackFilter_BK]
	EXEC [USP_BgTeams_BlackFilter__Sync]
	SELECT @Entity_BK_AFTER##COUNTER=count(*) from [BgTeams_BlackFilter_BK]
	PRINT '[BgTeams_BlackFilter] BEFORE: ' + CAST(@Entity_BK_BEFORE##COUNTER AS VARCHAR) + ' AFTER: '+ CAST(@Entity_BK_AFTER##COUNTER AS VARCHAR) + ' REMOTE: '+ CAST(@Entity_REMOTE##COUNTER AS VARCHAR)

	--ADD [Feed_Bg_Statistics]
	--NO BACKUP for watcher

END