﻿CREATE PROCEDURE [dbo].[USP_BgGoalserveMapper__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [BgGoalserveMapper_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[BgGoalserveMapper] AS SOURCE
		ON TARGET.[BgId] = SOURCE.[BgId] AND
		   TARGET.[GoalserveId] = SOURCE.[GoalserveId]
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[BgId],
			[GoalserveId],
			[GoalserveName]
		) 
		VALUES (
			SOURCE.[BgId],
			SOURCE.[GoalserveId],
			SOURCE.[GoalserveName]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[GoalserveName]	= SOURCE.[GoalserveName]
		 ;

	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END