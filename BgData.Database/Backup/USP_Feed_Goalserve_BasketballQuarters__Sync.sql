﻿CREATE PROCEDURE [dbo].[USP_Feed_Goalserve_BasketballQuarters__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [Feed_Goalserve_BasketballQuarters_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[Feed_Goalserve_BasketballQuarters] AS SOURCE
		ON TARGET.MatchId = SOURCE.MatchId AND 
			TARGET.LeagueId = SOURCE.LeagueId AND
			TARGET.HomeId = SOURCE.HomeId AND
			TARGET.AwayId = SOURCE.AwayId
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[MatchId],
			[LeagueId],
			[HomeId],
			[AwayId],
			[HomeQ1],
			[AwayQ2],
			[AwayQ1],
			[HomeQ2],
			[HomeQ3],
			[AwayQ3],
			[HomeQ4],
			[AwayQ4]
			) 
		VALUES (
			SOURCE.[MatchId],
			SOURCE.[LeagueId],
			SOURCE.[HomeId],
			SOURCE.[AwayId],
			SOURCE.[HomeQ1],
			SOURCE.[AwayQ2],
			SOURCE.[AwayQ1],
			SOURCE.[HomeQ2],
			SOURCE.[HomeQ3],
			SOURCE.[AwayQ3],
			SOURCE.[HomeQ4],
			SOURCE.[AwayQ4]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
	 	 [Target].[HomeQ1]				=SOURCE.[HomeQ1],
		 [Target].[AwayQ2]				=SOURCE.[AwayQ2],
		 [Target].[AwayQ1]				=SOURCE.[AwayQ1],
		 [Target].[HomeQ2]				=SOURCE.[HomeQ2],
		 [Target].[HomeQ3]				=SOURCE.[HomeQ3],
		 [Target].[AwayQ3]				=SOURCE.[AwayQ3],
		 [Target].[HomeQ4]				=SOURCE.[HomeQ4],
		 [Target].[AwayQ4]				=SOURCE.[AwayQ4]
		 ;

	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END