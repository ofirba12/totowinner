﻿CREATE PROCEDURE [dbo].[USP_Feed_Manual__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [Feed_Manual_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[Feed_Manual] AS SOURCE
		ON TARGET.MatchId = SOURCE.MatchId 
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[MatchId],
			[SportId],
			[Status],
			[CountryId],
			[Country],
			[League],
			[LeagueId],
			[IsCup],
			[GameStart],
			[Home],
			[HomeId],
			[Away],
			[AwayId],
			[HomeOdd],
			[DrawOdd],
			[AwayOdd],
			[HomeScore],
			[AwayScore],
			[LastUpdate],
			[HomeQ1],
			[AwayQ1],
			[HomeQ2],
			[AwayQ2],
			[HomeQ3],
			[AwayQ3],
			[HomeQ4],
			[AwayQ4],
			[Ignore]
	) 
		VALUES (
			SOURCE.[MatchId],
			SOURCE.[SportId],
			SOURCE.[Status],
			SOURCE.[CountryId],
			SOURCE.[Country],
			SOURCE.[League],
			SOURCE.[LeagueId],
			SOURCE.[IsCup],
			SOURCE.[GameStart],
			SOURCE.[Home],
			SOURCE.[HomeId],
			SOURCE.[Away],
			SOURCE.[AwayId],
			SOURCE.[HomeOdd],
			SOURCE.[DrawOdd],
			SOURCE.[AwayOdd],
			SOURCE.[HomeScore],
			SOURCE.[AwayScore],
			SOURCE.[LastUpdate],
			SOURCE.[HomeQ1],
			SOURCE.[AwayQ1],
			SOURCE.[HomeQ2],
			SOURCE.[AwayQ2],
			SOURCE.[HomeQ3],
			SOURCE.[AwayQ3],
			SOURCE.[HomeQ4],
			SOURCE.[AwayQ4],
			SOURCE.[Ignore]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
		[Target].[SportId]		=	SOURCE.[SportId],
		[Target].[Status]		=	SOURCE.[Status],
		[Target].[CountryId]	=	SOURCE.[CountryId],
		[Target].[Country]		=	SOURCE.[Country],
		[Target].[League]		=	SOURCE.[League],
		[Target].[LeagueId]		=	SOURCE.[LeagueId],
		[Target].[IsCup]		=	SOURCE.[IsCup],
		[Target].[GameStart]	=	SOURCE.[GameStart],
		[Target].[Home]			=	SOURCE.[Home],
		[Target].[HomeId]		=	SOURCE.[HomeId],
		[Target].[Away]			=	SOURCE.[Away],
		[Target].[AwayId]		=	SOURCE.[AwayId],
		[Target].[HomeOdd]		=	SOURCE.[HomeOdd],
		[Target].[DrawOdd]		=	SOURCE.[DrawOdd],
		[Target].[AwayOdd]		=	SOURCE.[AwayOdd],
		[Target].[HomeScore]	=	SOURCE.[HomeScore],
		[Target].[AwayScore]	=	SOURCE.[AwayScore],
		[Target].[LastUpdate]	=	SOURCE.[LastUpdate],
		[Target].[HomeQ1]		=	SOURCE.[HomeQ1],
		[Target].[AwayQ1]		=	SOURCE.[AwayQ1],
		[Target].[HomeQ2]		=	SOURCE.[HomeQ2],
		[Target].[AwayQ2]		=	SOURCE.[AwayQ2],
		[Target].[HomeQ3]		=	SOURCE.[HomeQ3],
		[Target].[AwayQ3]		=	SOURCE.[AwayQ3],
		[Target].[HomeQ4]		=	SOURCE.[HomeQ4],
		[Target].[AwayQ4]		=	SOURCE.[AwayQ4],
		[Target].[Ignore]		=	SOURCE.[Ignore]
		 ;

	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END