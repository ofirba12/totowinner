﻿CREATE PROCEDURE [dbo].[USP_Feed_Bg__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [Feed_Bg_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[Feed_Bg] AS SOURCE
		ON TARGET.MatchId = SOURCE.MatchId
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[MatchId],
			[Source],
			[SportId],
			[CountryId],
			[LeagueId],
			[HomeId],
			[AwayId],
			[GameStart],
			[Status],
			[IsCup],
			[HomeOdd],
			[DrawOdd],
			[AwayOdd],
			[HomeScore],
			[AwayScore],
			[LastUpdate]
			) 
		VALUES (
			SOURCE.[MatchId],
			SOURCE.[Source],
			SOURCE.[SportId],
			SOURCE.[CountryId],
			SOURCE.[LeagueId],
			SOURCE.[HomeId],
			SOURCE.[AwayId],
			SOURCE.[GameStart],
			SOURCE.[Status],
			SOURCE.[IsCup],
			SOURCE.[HomeOdd],
			SOURCE.[DrawOdd],
			SOURCE.[AwayOdd],
			SOURCE.[HomeScore],
			SOURCE.[AwayScore],
			SOURCE.[LastUpdate]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
		[Target].[Source]		= SOURCE.[Source],
		[Target].[SportId]		= SOURCE.[SportId],
		[Target].[CountryId]	= SOURCE.[CountryId],
		[Target].[LeagueId]		= SOURCE.[LeagueId],
		[Target].[HomeId]		= SOURCE.[HomeId],
		[Target].[AwayId]		= SOURCE.[AwayId],
		[Target].[GameStart]	= SOURCE.[GameStart],
		[Target].[Status]		= SOURCE.[Status],
		[Target].[IsCup]		= SOURCE.[IsCup],
		[Target].[HomeOdd]		= SOURCE.[HomeOdd],
		[Target].[DrawOdd]		= SOURCE.[DrawOdd],
		[Target].[AwayOdd]		= SOURCE.[AwayOdd],
		[Target].[HomeScore]	= SOURCE.[HomeScore],
		[Target].[AwayScore]	= SOURCE.[AwayScore],
		[Target].[LastUpdate]	= SOURCE.[LastUpdate]
		;

	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END