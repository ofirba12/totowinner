﻿CREATE PROCEDURE [dbo].[USP_BgTeams_BlackFilter__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [BgTeams_BlackFilter_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[BgTeams_BlackFilter] AS SOURCE
		ON TARGET.[TeamId] = SOURCE.[TeamId] AND
		   TARGET.[FilterBgType] = SOURCE.[FilterBgType]
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[TeamId],
			[FilterBgType],
			[FilterBgId]
		) 
		VALUES (
			SOURCE.[TeamId],
			SOURCE.[FilterBgType],
			SOURCE.[FilterBgId]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
	 	 [Target].[FilterBgId]	= SOURCE.[FilterBgId]
		 ;

	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END