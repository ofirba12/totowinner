﻿CREATE PROCEDURE [dbo].[USP_FeedGoalserve__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [Feed_Goalserve_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[Feed_Goalserve] AS SOURCE
		ON TARGET.MatchId = SOURCE.MatchId AND 
			TARGET.SportId = SOURCE.SportId AND
			TARGET.LeagueId = SOURCE.LeagueId AND
			TARGET.HomeId = SOURCE.HomeId AND
			TARGET.AwayId = SOURCE.AwayId
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[MatchId],
			[SportId],
			[Status],
			[CountryId],
			[Country],
			[League],
			[LeagueId],
			[IsCup],
			[GameStart],
			[Home],
			[HomeId],
			[Away],
			[AwayId],
			[HomeOdd],
			[DrawOdd],
			[AwayOdd],
			[HomeScore],
			[AwayScore],
			[LastUpdate]) 
		VALUES (
			SOURCE.[MatchId],
			SOURCE.[SportId],
			SOURCE.[Status],
			SOURCE.[CountryId],
			SOURCE.[Country],
			SOURCE.[League],
			SOURCE.[LeagueId],
			SOURCE.[IsCup],
			SOURCE.[GameStart],
			SOURCE.[Home],
			SOURCE.[HomeId],
			SOURCE.[Away],
			SOURCE.[AwayId],
			SOURCE.[HomeOdd],
			SOURCE.[DrawOdd],
			SOURCE.[AwayOdd],
			SOURCE.[HomeScore],
			SOURCE.[AwayScore],
			SOURCE.[LastUpdate]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
	 	 [Target].[Status]				=SOURCE.[Status],
		 [Target].[CountryId]			=SOURCE.[CountryId],
		 [Target].[Country]				=SOURCE.[Country],
		 [Target].[League]				=SOURCE.[League],
		 [Target].[IsCup]				=SOURCE.[IsCup],
		 [Target].[GameStart]			=SOURCE.[GameStart],
		 [Target].[Home]				=SOURCE.[Home],
		 [Target].[Away]				=SOURCE.[Away],
		 [Target].[HomeOdd]				=SOURCE.[HomeOdd],
		 [Target].[DrawOdd]				=SOURCE.[DrawOdd],
		 [Target].[AwayOdd]				=SOURCE.[AwayOdd],
		 [Target].[HomeScore]			=SOURCE.[HomeScore],
		 [Target].[AwayScore]			=SOURCE.[AwayScore],
		 [Target].[LastUpdate]			=SOURCE.[LastUpdate]
		 ;


	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END