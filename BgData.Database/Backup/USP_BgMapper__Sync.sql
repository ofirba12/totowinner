﻿CREATE PROCEDURE [dbo].[USP_BgMapper__Sync]
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMsg VARCHAR(1000)

	MERGE [BgMapper_BK] AS TARGET
	USING [3.125.32.136].[BgData].[dbo].[BgMapper] AS SOURCE
		ON TARGET.[BgId] = SOURCE.[BgId]
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[BgId],
			[BgSport],
			[BgType],
			[BgName]
		) 
		VALUES (
			SOURCE.[BgId],
			SOURCE.[BgSport],
			SOURCE.[BgType],
			SOURCE.[BgName]
		) 
	WHEN NOT MATCHED BY SOURCE 
		THEN DELETE
	WHEN MATCHED THEN 
		UPDATE SET
	 	 [Target].[BgSport]	= SOURCE.[BgSport],
		 [Target].[BgType]	= SOURCE.[BgType],
		 [Target].[BgName]	= SOURCE.[BgName]
		 ;

	END TRY
	BEGIN CATCH
		SET @ErrMsg = ERROR_MESSAGE()
		RAISERROR(@ErrMsg, 16, 1)
	END CATCH
END