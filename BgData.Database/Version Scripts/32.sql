﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE PROCEDURE [dbo].[FeedBg_Tips_FetchFull]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

  SELECT * FROM [dbo].[Feed_Bg_Tips] FBT
	JOIN [dbo].[Feed_Bg] FB 
		ON  FB.MatchId = FBT.MatchId
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0


GO
