﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[Feed_Bg_Tips]
(
	[MatchId]						BIGINT					NOT NULL,
	[Type]							TINYINT					NOT NULL,
	[Title]							NVARCHAR(300)			NOT NULL,
	[Value]							INT						NOT NULL,
	[Stars]							TINYINT					NOT NULL,
	[Description]					NVARCHAR(MAX)			NOT NULL,
	[FreezeTip]						BIT						NOT NULL,
	[LastUpdate]					DATETIME				NOT NULL

)

GO
ALTER TABLE [dbo].[Feed_Bg_Tips]
	ADD CONSTRAINT [PK_Feed_Bg_Tips]
	PRIMARY KEY ([MatchId], [Type])
GO
CREATE TYPE [dbo].[Feed_Bg_Tips_UDT] AS TABLE(
	[MatchId]				BIGINT					NOT NULL,
	[Type]					TINYINT					NOT NULL,
	[Title]					NVARCHAR(300)			NOT NULL,
	[Value]					INT						NOT NULL,
	[Stars]					TINYINT					NOT NULL,
	[Description]			VARCHAR(MAX)			NOT NULL,
	[FreezeTip]				BIT						NOT NULL,
	[LastUpdate]			DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId], [Type] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedBgTips_Merge]
	@Collection Feed_Bg_Tips_UDT READONLY
AS

MERGE dbo.[Feed_Bg_Tips] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId]				= [Target].[MatchId] AND
		[Src].[Type]				= [Target].[Type]
	WHEN MATCHED AND [Target].[FreezeTip]=0 THEN 
		UPDATE SET
		[Target].[Title]			=[Src].[Title]		
		,[Target].[Value]			=[Src].[Value]			
		,[Target].[Stars]			=[Src].[Stars]			
		,[Target].[Description]		=[Src].[Description]			
		,[Target].[FreezeTip]		=[Src].[FreezeTip]			
		,[Target].[LastUpdate]		=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[MatchId]
		,[Type]
		,[Title]		
		,[Value]		
		,[Stars]		
		,[Description]	
		,[FreezeTip]
		,[LastUpdate]		
				)
		VALUES (
			[Src].[MatchId]			
			,[Src].[Type]
			,[Src].[Title]	
			,[Src].[Value]		
			,[Src].[Stars]	
			,[Src].[Description]	
			,[Src].[FreezeTip]
			,[Src].[LastUpdate]	
				)
				;

RETURN 0

GO