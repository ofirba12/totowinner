﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[Feed_Winner]
(
	[EventId]		INT				NOT NULL,
	[ProgramId]		BIGINT			NOT NULL,
	[SportId]		TINYINT			NOT NULL,
	[TotoId]		INT				NOT NULL,
	[HomeId]		INT				NOT NULL,
	[GuestId]		INT				NOT NULL,
	[BetTypeId]		SMALLINT		NOT NULL,
	[Place]			INT				NOT NULL,
	[Name]			NVARCHAR(600)	NOT NULL,
	[GameStart]		DATETIME		NOT NULL,
	[Status]		NVARCHAR(100)	NULL,
	[HomeOdd]		FLOAT			NULL,
	[DrawOdd]		FLOAT			NULL,
	[AwayOdd]		FLOAT			NULL,
	[Country]		NVARCHAR(200)	NOT NULL,
	[League]		NVARCHAR(600)	NOT NULL,
	[LastUpdate]	DATETIME		NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Winner]
	ADD CONSTRAINT [Feed_Winner_PK]
	PRIMARY KEY ([EventId],[ProgramId])
GO
CREATE COLUMNSTORE INDEX [Feed_Winner_IX]
	ON [dbo].[Feed_Winner]
	([SportId], [ProgramId], [HomeId], [GuestId], [GameStart], [BetTypeId], [TotoId], [Status])
GO
CREATE TYPE [dbo].[Feed_Winner_UDT] AS TABLE(
	[EventId]		INT				NOT NULL,
	[ProgramId]		BIGINT			NOT NULL,
	[SportId]		TINYINT			NOT NULL,
	[TotoId]		INT				NOT NULL,
	[HomeId]		INT				NOT NULL,
	[GuestId]		INT				NOT NULL,
	[BetTypeId]		SMALLINT		NOT NULL,
	[Place]			INT				NOT NULL,
	[Name]			NVARCHAR(600)	NOT NULL,
	[GameStart]		DATETIME		NOT NULL,
	[Status]		NVARCHAR(100)	NULL,
	[HomeOdd]		FLOAT			NULL,
	[DrawOdd]		FLOAT			NULL,
	[AwayOdd]		FLOAT			NULL,
	[Country]		NVARCHAR(200)	NOT NULL,
	[League]		NVARCHAR(600)	NOT NULL,
	[LastUpdate]	DATETIME		NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[EventId],[ProgramId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedWinner_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

  SELECT * FROM [dbo].[Feed_Winner] 
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
GO
CREATE PROCEDURE [dbo].[FeedWinner_Merge]
	@Collection Feed_Winner_UDT READONLY
AS

MERGE dbo.[Feed_Winner] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[EventId] = [Target].[EventId] AND 
		[Src].[ProgramId] = [Target].[ProgramId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[SportId]	= [Src].[SportId]
				,[Target].[TotoId]		= [Src].[TotoId]
				,[Target].[HomeId]		= [Src].[HomeId]	
				,[Target].[GuestId]		= [Src].[GuestId]		
				,[Target].[BetTypeId]	= [Src].[BetTypeId]
				,[Target].[Place]		= [Src].[Place]		
				,[Target].[Name]		= [Src].[Name]		
				,[Target].[GameStart]	= [Src].[GameStart]		
				,[Target].[Status]		= [Src].[Status]		
				,[Target].[HomeOdd]		= [Src].[HomeOdd]		
				,[Target].[DrawOdd]		= [Src].[DrawOdd]		
				,[Target].[AwayOdd]		= [Src].[AwayOdd]		
				,[Target].[Country]		= [Src].[Country]
				,[Target].[League]		= [Src].[League]
				,[Target].[LastUpdate]	= [Src].[LastUpdate]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [EventId]
				,[ProgramId]
				,[SportId]
				,[TotoId]
				,[HomeId]			
				,[GuestId]	
				,[BetTypeId]	
				,[Place]			
				,[Name]			
				,[GameStart]
				,[Status]		
				,[HomeOdd]	
				,[DrawOdd]	
				,[AwayOdd]	
				,[Country]		
				,[League]		
				,[LastUpdate]
				)
		VALUES ([Src]. [EventId]
	  			,[Src].[ProgramId]
				,[Src].[SportId]
	  			,[Src].[TotoId]
				,[Src].[HomeId]	
	  			,[Src].[GuestId]	
				,[Src].[BetTypeId]
	  			,[Src].[Place]	
				,[Src].[Name]		
	  			,[Src].[GameStart]
				,[Src].[Status]	
	  			,[Src].[HomeOdd]	
				,[Src].[DrawOdd]	
	  			,[Src].[AwayOdd]	
				,[Src].[Country]	
				,[Src].[League]	
				,[Src].[LastUpdate]
				)
				;

RETURN 0


GO
CREATE TABLE [dbo].[BgWinnerMapper]
(
	[SportId]		TINYINT			NOT NULL,
	[WinnerId]		INT				NOT NULL,
	[WinnerName]	NVARCHAR(600)	NOT NULL,
	[BgId]			INT				NULL
)
GO
ALTER TABLE [dbo].[BgWinnerMapper]
	ADD CONSTRAINT [BgWinnerMapper_PK]
	PRIMARY KEY ([SportId],[WinnerId])
GO
CREATE TYPE [dbo].[BgWinnerMapperUDT] AS TABLE
(
	[SportId]		TINYINT			NOT NULL,
	[WinnerId]		INT				NOT NULL,
	[WinnerName]	NVARCHAR(600)	NOT NULL
PRIMARY KEY CLUSTERED 
(
	[SportId],[WinnerId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[BgWinnerMapperMerge]
	@MapperParameter BgWinnerMapperUDT READONLY
AS

MERGE dbo.[BgWinnerMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[SportId] = [Target].[SportId] AND 
		   [Source].[WinnerId] = [Target].[WinnerId] 
	--WHEN MATCHED THEN 
	--	UPDATE SET	[Target].[WinnerName] = [Source].[WinnerName],				   
	--				[Target].[BgId] = [Source].[BgId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SportId]
	  			,[WinnerId]
				,[WinnerName]
				,[BgId]
				)
		VALUES ([Source].[SportId]
				,[Source].[WinnerId]
	  			,[Source].[WinnerName]
				,NULL
				)
				;

RETURN 0


GO

CREATE PROCEDURE [dbo].[MapperWinner_UpdateBgId]
	@Sport		TINYINT,
	@WinnerId	INT,
	@BgId		INT
AS
	UPDATE BgWinnerMapper 
	SET BgId = @BgId
	WHERE SportId = @Sport AND WinnerId = @WinnerId
RETURN 0
GO
CREATE PROCEDURE [dbo].[BgWinnerMapper_Fetch]
AS
	SELECT * FROM BgWinnerMapper
RETURN 0
GO