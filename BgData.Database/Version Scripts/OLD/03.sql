﻿USE BgData
GO
DROP TABLE [Feed_Goalserve]
GO
CREATE TABLE [dbo].[Feed_Goalserve]
(
	[MatchId]		INT						NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NOT NULL,
	[League]		NVARCHAR(600)			NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NOT NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL



)
GO
ALTER TABLE [dbo].[Feed_Goalserve]
	ADD CONSTRAINT [Feed_Goalserve_PK]
	PRIMARY KEY (MatchId, SportId)

GO
ALTER PROCEDURE Feed_Goalserve_BulkInsert
AS
BEGIN
SELECT 1
END

GO

ALTER PROCEDURE Feed_Goalserve_BulkUpdate
AS
BEGIN
SELECT 1
END
GO

DROP TYPE Feed_Goalserve_TT
GO
CREATE TYPE [dbo].[Feed_Goalserve_TT] AS TABLE(
	[MatchId]		INT						NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NULL,
	[League]		NVARCHAR(600)			NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	PRIMARY KEY CLUSTERED 
(
	[SportId],[MatchId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
DROP TYPE [Feed_Goalserve_BasketballQuarters_TT]
GO
CREATE TYPE [dbo].[Feed_Goalserve_BasketballQuarters_TT] AS TABLE(
	[MatchId]		INT						NOT NULL,
	[SportId]		INT						NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SportId], [MatchId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)
GO

ALTER PROCEDURE [dbo].[Feed_Goalserve_BulkInsert]
	@Feed AS [Feed_Goalserve_TT] READONLY,
	@QuartersFeed AS [Feed_Goalserve_BasketballQuarters_TT] READONLY
AS
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @LastUpdateTS DATETIME = GetDate();

	INSERT INTO [Feed_Goalserve](
		[MatchId],
		[SportId]	 ,
		[Status]	 ,
		[CountryId]	 ,
		[Country]	 ,
		[League]	 ,
		[LeagueId]	 ,
		[IsCup]		 ,
		[GameStart]	 ,
		[Home]		 ,
		[HomeId]	 ,
		[Away]		 ,
		[AwayId]	 ,
		[HomeOdd]	 ,
		[DrawOdd]	 ,
		[AwayOdd]	 ,
		[HomeScore]	 ,
		[AwayScore]	 , 
		[LastUpdate]
	)
	SELECT [MatchId],
			[SportId]	,
			[Status]	,
			[CountryId]	,
			[Country]	,
			[League]	,
			[LeagueId]	,
			[IsCup]		,
			[GameStart]	,
			[Home]		,
			[HomeId]	,
			[Away]		,
			[AwayId]	,
			[HomeOdd]	,
			[DrawOdd]	,
			[AwayOdd]	,
			[HomeScore]	,
			[AwayScore]	,
			[LastUpdate] = @LastUpdateTS
	FROM @Feed;

	INSERT INTO [Feed_Goalserve_BasketballQuarters](
		[MatchId],
		[HomeQ1],
		[AwayQ1],
		[HomeQ2],
		[AwayQ2],
		[HomeQ3],
		[AwayQ3],
		[HomeQ4],
		[AwayQ4]
	)
	SELECT [MatchId], 
		[HomeQ1], 
		[AwayQ1], 
		[HomeQ2], 
		[AwayQ2], 
		[HomeQ3], 
		[AwayQ3], 
		[HomeQ4], 
		[AwayQ4]
	FROM @QuartersFeed

	--SELECT [MatchId], [HomeQ1], [AwayQ1], [HomeQ2], [AwayQ2], [HomeQ3], [AwayQ3], [HomeQ4], [AwayQ4]
	--FROM (
	--	SELECT [MatchId], F.[SportId], F.[CountryId], F.[LeagueId], F.[GameStart], F.[HomeId], F.[AwayId], [HomeQ1], [AwayQ1], [HomeQ2], [AwayQ2], [HomeQ3], [AwayQ3], [HomeQ4], [AwayQ4]
	--	FROM [Feed_Goalserve] FG
	--	JOIN @Feed F ON FG.[SportId] = F.[SportId] 
	--		AND FG.[LeagueId] = F.[LeagueId]
	--		AND FG.[GameStart] = F.[GameStart]
	--		AND FG.[HomeId] = F.[HomeId]
	--		AND FG.[AwayId] = F.[AwayId]
	--	JOIN @QuartersFeed QF ON QF.[SportId] = F.[SportId]
	--		AND QF.[LeagueId] = F.[LeagueId]
	--		AND QF.[HomeId] = F.[HomeId]
	--		AND QF.[AwayId] = F.[AwayId]
	--) AS JoinedFeed;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0

GO

ALTER PROCEDURE [dbo].[Feed_Goalserve_BulkUpdate]
	@Feed AS [Feed_Goalserve_TT] READONLY,
    @QuartersFeed AS [Feed_Goalserve_BasketballQuarters_TT] READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
    DECLARE @LastUpdateTS DATETIME = GetDate();

    UPDATE Feed_Goalserve
    SET
        Feed_Goalserve.[Status]    = ISNULL(F.[Status],		FG.[Status]),
        Feed_Goalserve.[HomeOdd]   = ISNULL(F.[HomeOdd],    FG.[HomeOdd]),
        Feed_Goalserve.[DrawOdd]   = ISNULL(F.[DrawOdd],    FG.[DrawOdd]),
        Feed_Goalserve.[AwayOdd]   = ISNULL(F.[AwayOdd],    FG.[AwayOdd]),
        Feed_Goalserve.[HomeScore] = ISNULL(F.[HomeScore],  FG.[HomeScore]),
        Feed_Goalserve.[AwayScore] = ISNULL(F.[AwayScore],  FG.[AwayScore]),
        Feed_Goalserve.[LastUpdate]= @LastUpdateTS
    FROM Feed_Goalserve FG
        INNER JOIN @Feed F
        ON FG.MatchId = F.MatchId AND 
            FG.SportId = F.SportId 
   --     ON FG.SportId = F.SportId AND 
   --         FG.LeagueId = F.LeagueId AND 
   --         FG.HomeId = F.HomeId AND
   --         FG.AwayId = F.AwayId AND
			--FG.GameStart = F.GameStart

MERGE dbo.[Feed_Goalserve_BasketballQuarters] AS [Target]
	USING @QuartersFeed AS [Source]	ON [Source].[MatchId] = [Target].[MatchId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[HomeQ1] = [Source].[HomeQ1],
				   [Target].[HomeQ2] = [Source].[HomeQ2],
				   [Target].[HomeQ3] = [Source].[HomeQ3],
				   [Target].[HomeQ4] = [Source].[HomeQ4],
				   [Target].[AwayQ1] = [Source].[AwayQ1],
				   [Target].[AwayQ2] = [Source].[AwayQ2],
				   [Target].[AwayQ3] = [Source].[AwayQ3],
				   [Target].[AwayQ4] = [Source].[AwayQ4]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [MatchId]
				,[HomeQ1]
				,[HomeQ2]
				,[HomeQ3]
				,[HomeQ4]
				,[AwayQ1]
				,[AwayQ2]
				,[AwayQ3]
				,[AwayQ4]
				)
		VALUES ([Source].[MatchId]
				,[Source].[HomeQ1]
				,[Source].[HomeQ2]
				,[Source].[HomeQ3]
				,[Source].[HomeQ4]
				,[Source].[AwayQ1]
				,[Source].[AwayQ2]
				,[Source].[AwayQ3]
				,[Source].[AwayQ4]
				)
				;


    --UPDATE Feed_Goalserve_BasketballQuarters
    --SET
    --    Feed_Goalserve_BasketballQuarters.HomeQ1 = QF.HomeQ1,
    --    Feed_Goalserve_BasketballQuarters.HomeQ2 = QF.HomeQ2,
    --    Feed_Goalserve_BasketballQuarters.HomeQ3 = QF.HomeQ3,
    --    Feed_Goalserve_BasketballQuarters.HomeQ4 = QF.HomeQ4,
    --    Feed_Goalserve_BasketballQuarters.AwayQ1 = QF.AwayQ1,
    --    Feed_Goalserve_BasketballQuarters.AwayQ2 = QF.AwayQ2,
    --    Feed_Goalserve_BasketballQuarters.AwayQ3 = QF.AwayQ3,
    --    Feed_Goalserve_BasketballQuarters.AwayQ4 = QF.AwayQ4
    --FROM Feed_Goalserve_BasketballQuarters FGB
    --    INNER JOIN @QuartersFeed QF
    --    ON FGB.MatchId = QF.MatchId 

		--INNER JOIN (
		--		SELECT FG.MatchId, QF.* FROM
		--			Feed_Goalserve FG
		--		INNER JOIN  @QuartersFeed QF
		--		ON QF.SportId = FG.SportId AND 
		--			QF.LeagueId = FG.LeagueId AND 
		--			QF.HomeId = FG.HomeId AND
		--			QF.AwayId = FG.AwayId
		--		INNER JOIN @Feed F
		--		ON FG.SportId = F.SportId AND 
		--			FG.LeagueId = F.LeagueId AND 
		--			FG.HomeId = F.HomeId AND
		--			FG.AwayId = F.AwayId AND
		--			FG.GameStart = F.GameStart
		--) A
		--ON 
		--A.MatchId = FGB.MatchId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END

GO
