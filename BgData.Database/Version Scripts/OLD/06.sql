﻿USE BgData
GO
CREATE TABLE [dbo].[BgMapper]
(
	[BgId]			INT NOT NULL,
	[BgType]		TINYINT NOT NULL,
	[BgName]		NVARCHAR(600) NULL
)
--countries start with 100
--leagues start with 1000
--teams start with 50000
ALTER TABLE [dbo].[BgMapper]
	ADD CONSTRAINT [PK_BgMapper]
	PRIMARY KEY (BgId)

CREATE TABLE [dbo].[BgGoalserveMapper]
(
	[BgId]				INT NOT NULL,
	[GoalserveId]		INT NOT NULL,
	[GoalserveName]		NVARCHAR(600) NOT NULL
)

GO
CREATE TYPE [dbo].BgGoalserveMapperUDT AS TABLE
(
	[BgId]					INT NOT NULL,
	[GoalserveId]			INT NOT NULL,
	[GoalserveName]			NVARCHAR(600) NOT NULL
PRIMARY KEY CLUSTERED 
(
	[BgId],[GoalserveId] ASC
)WITH (IGNORE_DUP_KEY = ON)
)
GO

ALTER TABLE [dbo].[BgGoalserveMapper]
	ADD CONSTRAINT [FK_BgMapper_BgId]
	FOREIGN KEY (BgId)
	REFERENCES [BgMapper] (BgId)
GO
CREATE TYPE [dbo].[BgMapperUDT] AS TABLE
(
	[BgId]					INT NOT NULL,
	[BgType]				TINYINT NOT NULL,
	[BgName]				NVARCHAR(600) NULL
PRIMARY KEY CLUSTERED 
(
	[BgId],[BgType]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[MapperMerge]
	@MapperParameter BgMapperUDT READONLY
AS

MERGE dbo.[BgMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND 
		   [Source].[BgType] = [Target].[BgType] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[BgName] = [Source].[BgName]				   
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
	  			,[BgType]
				,[BgName]
				)
		VALUES ([Source].[BgId]
				,[Source].[BgType]
	  			,[Source].[BgName]
				)
				;

RETURN 0


GO
CREATE PROCEDURE [dbo].BgGoalserveMapperMerge
	@MapperParameter BgGoalserveMapperUDT READONLY
AS

MERGE dbo.[BgGoalserveMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND
		   [Source].[GoalserveId] = [Target].[GoalserveId] AND
		   [Source].[GoalserveName] = [Target].[GoalserveName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
	  			,[GoalserveId]
				,[GoalserveName]
				)
		VALUES ([Source].[BgId]
				,[Source].[GoalserveId]
				,[GoalserveName]
				)
				;
RETURN 0

GO
CREATE PROCEDURE [dbo].[BgMapper_Fetch]
AS
	SELECT BG.BgId, BG.BgType, BG.BgName, BGGM.GoalserveId, BGGM.GoalserveName FROM BgMapper BG
	JOIN BgGoalserveMapper BGGM
		ON BG.BgId = BGGM.BgId
RETURN 0

GO