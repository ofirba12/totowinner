﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TYPE [dbo].[BgTeams_Watcher_UDT] AS TABLE
(
	[TeamId]					INT NOT NULL,
	[TotalShows]				INT NOT NULL,
	[MissingOdds]				INT NOT NULL,
	[MissingResult]				INT NOT NULL
PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE TABLE [dbo].[BgTeams_Watcher]
(
	[TeamId]		INT			NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL,
	[TotalShows]	INT			NOT NULL,
	[MissingOdds]	INT			NOT NULL,
	[MissingResult]	INT			NOT NULL
)
GO
ALTER TABLE [dbo].[BgTeams_Watcher]
	ADD CONSTRAINT [PK_BgTeams_Watcher]
	PRIMARY KEY (TeamId)
GO
CREATE PROCEDURE [dbo].[BgTeams_Watcher_Merge]
	@Collection BgTeams_Watcher_UDT READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgTeams_Watcher] AS [Target]
	USING @Collection AS [Source]	
		ON	[Source].[TeamId] = [Target].[TeamId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[TotalShows] = [Source].[TotalShows]
				,[Target].[MissingOdds] = [Source].[MissingOdds] 
				,[Target].[MissingResult] = [Source].[MissingResult]
				,[Target].[LastUpdate] = GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TeamId]
			,[TotalShows]
			,[MissingOdds]
			,[MissingResult]	
			,[LastUpdate]
			)
		VALUES ([Source].[TeamId]
			,[Source].[TotalShows]	
			,[Source].[MissingOdds]
			,[Source].[MissingResult]
			,GetDate()
			);
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END

GO
CREATE PROCEDURE [dbo].[BgTeams_Watcher_Delete]
	@TeamId INT
AS
	DELETE FROM BgTeams_Watcher WHERE TeamId = @TeamId
RETURN 0
GO
CREATE PROCEDURE [dbo].[BgTeams_Watcher_Get]
	@SportId TINYINT
AS
	SELECT W.* FROM BgTeams_Watcher W
	JOIN BgMapper M ON M.BgId = W.TeamId
	WHERE M.BgSport = @SportId AND M.BgType = 3
RETURN 0
GO

