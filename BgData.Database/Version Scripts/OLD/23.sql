﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[BgTeams_BlackFilter]
(
	[TeamId]			INT			NOT NULL,
	[FilterBgType]		TINYINT		NOT NULL,
	[FilterBgId]		INT			NOT NULL,
)
/*
[FilterBgType] BgMapperType
	Countries = 1
	Leagues = 2
*/
GO
ALTER TABLE [dbo].[BgTeams_BlackFilter]
	ADD CONSTRAINT [PK_BgTeams_BlackFilter]
	PRIMARY KEY (TeamId, FilterBgType)
GO
CREATE PROCEDURE [dbo].[BgTeams_BlackFilter_Merge]
	@TeamId			INT,
	@FilterBgType	TINYINT,
	@FilterBgId		INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgTeams_BlackFilter] AS [Target]
	USING ( SELECT @TeamId		AS "TeamId"
				,@FilterBgType	AS "FilterBgType"
				,@FilterBgId	AS "FilterBgId"
	) AS [Source]	ON [Source].[TeamId]	= [Target].[TeamId]
		AND [Source].[FilterBgType]	= [Target].[FilterBgType]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[FilterBgId] = [Source].[FilterBgId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TeamId]
			,[FilterBgType]	
			,[FilterBgId]
			)
		VALUES ([Source].[TeamId]
			,[Source].[FilterBgType]	
			,[Source].[FilterBgId]
			);
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END

GO
CREATE PROCEDURE [dbo].[BgTeams_BlackFilter_GetAll]
AS
	SELECT * FROM BgTeams_BlackFilter
RETURN 0
GO
CREATE PROCEDURE [dbo].[BgTeams_BlackFilter_Delete]
	@TeamId INT,
	@FilterBgType TINYINT
AS
	DELETE FROM BgTeams_BlackFilter WHERE TeamId = @TeamId AND FilterBgType = @FilterBgType

RETURN 0
GO