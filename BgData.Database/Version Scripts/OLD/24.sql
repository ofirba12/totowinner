﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
DELETE FROM [dbo].[BgSystem]
GO
CREATE TABLE [dbo].[Feed_Bg_Statistics]
(
	[MatchId]						BIGINT					NOT NULL,
	[BadMatches]					INT						NOT NULL,
	[HomeMatches]					INT						NOT NULL,
	[AwayMatches]					INT						NOT NULL,
	[Head2HeadMatches]				INT						NOT NULL,
	[FullTimeHomeWin]				INT						NULL,
	[FullTimeDraw]					INT						NULL,
	[FullTimeAwayWin]				INT						NULL,
	[FullTimeHomeWinH2H]			INT						NULL,
	[FullTimeDrawH2H]				INT						NULL,
	[FullTimeAwayWinH2H]			INT						NULL,
	[Over3_5]						INT						NULL,
	[Under3_5]						INT						NULL,
	[Over3_5H2H]					INT						NULL,
	[Under3_5H2H]					INT						NULL,
	[Over2_5]						INT						NULL,
	[Under2_5]						INT						NULL,
	[Over2_5H2H]					INT						NULL,
	[Under2_5H2H]					INT						NULL,
	[Over1_5]						INT						NULL,
	[Under1_5]						INT						NULL,
	[Over1_5H2H]					INT						NULL,
	[Under1_5H2H]					INT						NULL,
	[Range0_1]						INT						NULL,
	[Range2_3]						INT						NULL,
	[Range4Plus]					INT						NULL,
	[Range0_1H2H]					INT						NULL,
	[Range2_3H2H]					INT						NULL,
	[Range4PlusH2H]					INT						NULL,
	[BothScore]						INT						NULL,
	[NoBothScore]					INT						NULL,
	[BothScoreH2H]					INT						NULL,
	[NoBothScoreH2H]				INT						NULL,
	[HomeDraw]						INT						NULL,
	[NoDraw]						INT						NULL,
	[DrawAway]						INT						NULL,
	[HomeDrawH2H]					INT						NULL,
	[NoDrawH2H]						INT						NULL,
	[DrawAwayH2H]					INT						NULL,
	[HomePower]						FLOAT					NULL,
	[AwayPower]						FLOAT					NULL,
	[LastUpdate]					DATETIME				NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Bg_Statistics]
	ADD CONSTRAINT [PK_Feed_Bg_Statistics]
	PRIMARY KEY ([MatchId])
GO
CREATE TYPE [dbo].[Feed_Bg_Statistics_UDT] AS TABLE(
	[MatchId]						BIGINT					NOT NULL,
	[BadMatches]					INT						NOT NULL,
	[HomeMatches]					INT						NOT NULL,
	[AwayMatches]					INT						NOT NULL,
	[Head2HeadMatches]				INT						NOT NULL,
	[FullTimeHomeWin]				INT						NULL,
	[FullTimeDraw]					INT						NULL,
	[FullTimeAwayWin]				INT						NULL,
	[FullTimeHomeWinH2H]			INT						NULL,
	[FullTimeDrawH2H]				INT						NULL,
	[FullTimeAwayWinH2H]			INT						NULL,
	[Over3_5]						INT						NULL,
	[Under3_5]						INT						NULL,
	[Over3_5H2H]					INT						NULL,
	[Under3_5H2H]					INT						NULL,
	[Over2_5]						INT						NULL,
	[Under2_5]						INT						NULL,
	[Over2_5H2H]					INT						NULL,
	[Under2_5H2H]					INT						NULL,
	[Over1_5]						INT						NULL,
	[Under1_5]						INT						NULL,
	[Over1_5H2H]					INT						NULL,
	[Under1_5H2H]					INT						NULL,
	[Range0_1]						INT						NULL,
	[Range2_3]						INT						NULL,
	[Range4Plus]					INT						NULL,
	[Range0_1H2H]					INT						NULL,
	[Range2_3H2H]					INT						NULL,
	[Range4PlusH2H]					INT						NULL,
	[BothScore]						INT						NULL,
	[NoBothScore]					INT						NULL,
	[BothScoreH2H]					INT						NULL,
	[NoBothScoreH2H]				INT						NULL,
	[HomeDraw]						INT						NULL,
	[NoDraw]						INT						NULL,
	[DrawAway]						INT						NULL,
	[HomeDrawH2H]					INT						NULL,
	[NoDrawH2H]						INT						NULL,
	[DrawAwayH2H]					INT						NULL,
	[HomePower]						FLOAT					NULL,
	[AwayPower]						FLOAT					NULL,
	[LastUpdate]					DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO

CREATE PROCEDURE [dbo].[FeedBgStatistics_Merge]
	@Collection Feed_Bg_Statistics_UDT READONLY
AS

MERGE dbo.[Feed_Bg_Statistics] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId] = [Target].[MatchId] 
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[BadMatches]				=[Src].[BadMatches]				
		,[Target].[HomeMatches]				=[Src].[HomeMatches]		
		,[Target].[AwayMatches]				=[Src].[AwayMatches]			
		,[Target].[Head2HeadMatches]		=[Src].[Head2HeadMatches]		
		,[Target].[FullTimeHomeWin]			=[Src].[FullTimeHomeWin]		
		,[Target].[FullTimeDraw]			=[Src].[FullTimeDraw]			
		,[Target].[FullTimeAwayWin]			=[Src].[FullTimeAwayWin]		
		,[Target].[FullTimeHomeWinH2H]		=[Src].[FullTimeHomeWinH2H]	
		,[Target].[FullTimeDrawH2H]			=[Src].[FullTimeDrawH2H]		
		,[Target].[FullTimeAwayWinH2H]		=[Src].[FullTimeAwayWinH2H]	
		,[Target].[Over3_5]					=[Src].[Over3_5]				
		,[Target].[Under3_5]				=[Src].[Under3_5]				
		,[Target].[Over3_5H2H]				=[Src].[Over3_5H2H]			
		,[Target].[Under3_5H2H]				=[Src].[Under3_5H2H]			
		,[Target].[Over2_5]					=[Src].[Over2_5]				
		,[Target].[Under2_5]				=[Src].[Under2_5]				
		,[Target].[Over2_5H2H]				=[Src].[Over2_5H2H]			
		,[Target].[Under2_5H2H]				=[Src].[Under2_5H2H]			
		,[Target].[Over1_5]					=[Src].[Over1_5]				
		,[Target].[Under1_5]				=[Src].[Under1_5]				
		,[Target].[Over1_5H2H]				=[Src].[Over1_5H2H]			
		,[Target].[Under1_5H2H]				=[Src].[Under1_5H2H]	
		,[Target].[Range0_1]				=[Src].[Range0_1]
		,[Target].[Range2_3]				=[Src].[Range2_3]
		,[Target].[Range4Plus]				=[Src].[Range4Plus]
		,[Target].[Range0_1H2H]				=[Src].[Range0_1H2H]
		,[Target].[Range2_3H2H]				=[Src].[Range2_3H2H]
		,[Target].[Range4PlusH2H]			=[Src].[Range4PlusH2H]
		,[Target].[BothScore]				=[Src].[BothScore]			
		,[Target].[NoBothScore]				=[Src].[NoBothScore]			
		,[Target].[BothScoreH2H]			=[Src].[BothScoreH2H]			
		,[Target].[NoBothScoreH2H]			=[Src].[NoBothScoreH2H]		
		,[Target].[HomeDraw]				=[Src].[HomeDraw]				
		,[Target].[NoDraw]					=[Src].[NoDraw]				
		,[Target].[DrawAway]				=[Src].[DrawAway]				
		,[Target].[HomeDrawH2H]				=[Src].[HomeDrawH2H]			
		,[Target].[NoDrawH2H]				=[Src].[NoDrawH2H]			
		,[Target].[DrawAwayH2H]				=[Src].[DrawAwayH2H]			
		,[Target].[HomePower]				=[Src].[HomePower]			
		,[Target].[AwayPower]				=[Src].[AwayPower]			
		,[Target].[LastUpdate]				=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[MatchId]
		,[BadMatches]			
		,[HomeMatches]		
		,[AwayMatches]		
		,[Head2HeadMatches]	
		,[FullTimeHomeWin]	
		,[FullTimeDraw]		
		,[FullTimeAwayWin]	
		,[FullTimeHomeWinH2H]	
		,[FullTimeDrawH2H]	
		,[FullTimeAwayWinH2H]	
		,[Over3_5]			
		,[Under3_5]			
		,[Over3_5H2H]			
		,[Under3_5H2H]		
		,[Over2_5]			
		,[Under2_5]			
		,[Over2_5H2H]			
		,[Under2_5H2H]		
		,[Over1_5]			
		,[Under1_5]			
		,[Over1_5H2H]			
		,[Under1_5H2H]	
		,[Range0_1]
		,[Range2_3]
		,[Range4Plus]
		,[Range0_1H2H]
		,[Range2_3H2H]
		,[Range4PlusH2H]
		,[BothScore]			
		,[NoBothScore]		
		,[BothScoreH2H]		
		,[NoBothScoreH2H]		
		,[HomeDraw]			
		,[NoDraw]				
		,[DrawAway]			
		,[HomeDrawH2H]		
		,[NoDrawH2H]			
		,[DrawAwayH2H]		
		,[HomePower]			
		,[AwayPower]			
		,[LastUpdate]			
				)
		VALUES (
			[Src].[MatchId]			
			,[Src].[BadMatches]			
			,[Src].[HomeMatches]		
			,[Src].[AwayMatches]		
			,[Src].[Head2HeadMatches]	
			,[Src].[FullTimeHomeWin]	
			,[Src].[FullTimeDraw]		
			,[Src].[FullTimeAwayWin]	
			,[Src].[FullTimeHomeWinH2H]	
			,[Src].[FullTimeDrawH2H]	
			,[Src].[FullTimeAwayWinH2H]	
			,[Src].[Over3_5]			
			,[Src].[Under3_5]			
			,[Src].[Over3_5H2H]			
			,[Src].[Under3_5H2H]		
			,[Src].[Over2_5]			
			,[Src].[Under2_5]			
			,[Src].[Over2_5H2H]			
			,[Src].[Under2_5H2H]		
			,[Src].[Over1_5]			
			,[Src].[Under1_5]			
			,[Src].[Over1_5H2H]			
			,[Src].[Under1_5H2H]	
			,[Src].[Range0_1]
			,[Src].[Range2_3]
			,[Src].[Range4Plus]
			,[Src].[Range0_1H2H]
			,[Src].[Range2_3H2H]
			,[Src].[Range4PlusH2H]
			,[Src].[BothScore]			
			,[Src].[NoBothScore]		
			,[Src].[BothScoreH2H]		
			,[Src].[NoBothScoreH2H]		
			,[Src].[HomeDraw]			
			,[Src].[NoDraw]				
			,[Src].[DrawAway]			
			,[Src].[HomeDrawH2H]		
			,[Src].[NoDrawH2H]			
			,[Src].[DrawAwayH2H]		
			,[Src].[HomePower]			
			,[Src].[AwayPower]			
			,[Src].[LastUpdate]			
				)
				;

RETURN 0

GO
