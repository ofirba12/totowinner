﻿USE BgData
GO

CREATE TABLE [dbo].[Feed_Goalserve]
(
	[MatchId]		INT IDENTITY(1,1)		NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NOT NULL,
	[Country]		NVARCHAR(200)			NOT NULL,
	[League]		NVARCHAR(600)			NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NOT NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Goalserve]
	ADD CONSTRAINT [Feed_Goalserve_PK]
	PRIMARY KEY (MatchId)
GO
CREATE COLUMNSTORE INDEX [Feed_Goalserve_IX]
	ON [dbo].[Feed_Goalserve]
	([SportId], [CountryId], [LeagueId], [HomeId], [AwayId], [GameStart])
GO
CREATE TYPE [dbo].[Feed_Goalserve_TT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NOT NULL,
	[Country]		NVARCHAR(200)			NULL,
	[League]		NVARCHAR(600)			NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NULL,
	[GameStart]		DATETIME				NULL,
	[Home]			NVARCHAR(600)			NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	PRIMARY KEY CLUSTERED 
(
	[SportId],[CountryId],[LeagueId],[HomeId],[AwayId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[Feed_Goalserve_BulkInsert]
	@Feed AS [Feed_Goalserve_TT] READONLY
AS
	INSERT INTO [Feed_Goalserve](
		[SportId]	 ,
		[Status]	 ,
		[CountryId]	 ,
		[Country]	 ,
		[League]	 ,
		[LeagueId]	 ,
		[IsCup]		 ,
		[GameStart]	 ,
		[Home]		 ,
		[HomeId]	 ,
		[Away]		 ,
		[AwayId]	 ,
		[HomeOdd]	 ,
		[DrawOdd]	 ,
		[AwayOdd]	 ,
		[HomeScore]	 ,
		[AwayScore]	 , 
		[LastUpdate]
	)
	SELECT [SportId]	,
			[Status]	,
			[CountryId]	,
			[Country]	,
			[League]	,
			[LeagueId]	,
			[IsCup]		,
			[GameStart]	,
			[Home]		,
			[HomeId]	,
			[Away]		,
			[AwayId]	,
			[HomeOdd]	,
			[DrawOdd]	,
			[AwayOdd]	,
			[HomeScore]	,
			[AwayScore]	,
			[LastUpdate] = GetDate()
	FROM @Feed;
RETURN 0
GO
CREATE PROCEDURE [dbo].[Feed_Goalserve_BulkUpdate]
	@Feed AS [Feed_Goalserve_TT] READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);

    UPDATE Feed_Goalserve
    SET
        Feed_Goalserve.[Status]    = ISNULL(F.[Status],		FG.[Status]),
        Feed_Goalserve.[HomeOdd]   = ISNULL(F.[HomeOdd],    FG.[HomeOdd]),
        Feed_Goalserve.[DrawOdd]   = ISNULL(F.[DrawOdd],    FG.[DrawOdd]),
        Feed_Goalserve.[AwayOdd]   = ISNULL(F.[AwayOdd],    FG.[AwayOdd]),
        Feed_Goalserve.[HomeScore] = ISNULL(F.[HomeScore],  FG.[HomeScore]),
        Feed_Goalserve.[AwayScore] = ISNULL(F.[AwayScore],  FG.[AwayScore]),
        Feed_Goalserve.[LastUpdate]= GetDate()
    FROM Feed_Goalserve FG
        INNER JOIN @Feed F
        ON FG.SportId = F.SportId AND 
            FG.CountryId = F.CountryId AND 
            FG.LeagueId = F.LeagueId AND 
            FG.HomeId = F.HomeId AND
            FG.AwayId = F.AwayId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Feed_Goalserve_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS

  SELECT * FROM [dbo].[Feed_Goalserve]
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE())) AND DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))
RETURN 0
GO