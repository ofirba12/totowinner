﻿USE BgData
GO
BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[Feed_Bg]
(
	[MatchId]				BIGINT					IDENTITY(500000,1)	NOT NULL,
	[Source]				TINYINT					NOT NULL,			
	[SportId]				INT						NOT NULL,
	[CountryId]				INT						NULL,
	[LeagueId]				INT						NOT NULL,
	[HomeId]				INT						NOT NULL,
	[AwayId]				INT						NOT NULL,
	[GameStart]				DATETIME				NOT NULL,
	[Status]				VARCHAR(100)			NULL,
	[IsCup]					BIT						NOT NULL,
	[HomeOdd]				FLOAT					NULL,
	[DrawOdd]				FLOAT					NULL,
	[AwayOdd]				FLOAT					NULL,
	[HomeScore]				INT						NULL,
	[AwayScore]				INT						NULL,
	[LastUpdate]			DATETIME				NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Bg]
	ADD CONSTRAINT [PK_Feed_Bg]
	PRIMARY KEY ([MatchId])
GO
CREATE COLUMNSTORE INDEX [Feed_Bg_IX]
	ON [dbo].[Feed_Bg]
	([SportId], [CountryId], [LeagueId], [HomeId], [AwayId], [GameStart])
GO
CREATE TYPE [dbo].[Feed_Bg_UDT] AS TABLE(
	[Source]		TINYINT					NOT NULL,
	[SportId]		INT						NOT NULL,
	[CountryId]		INT						NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[HomeId]		INT						NOT NULL,
	[AwayId]		INT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[IsCup]			BIT						NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[SportId],[CountryId],[LeagueId],[HomeId],[AwayId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedBg_Merge]
	@Collection Feed_Bg_UDT READONLY
AS

MERGE dbo.[Feed_Bg] AS [Target]
	USING @Collection AS [Src]	
		ON [Src].[Source] = [Target].[Source] AND
		[Src].[SportId] = [Target].[SportId] AND
		[Src].[CountryId] = [Target].[CountryId] AND
		[Src].[LeagueId] = [Target].[LeagueId] AND
		[Src].[HomeId] = [Target].[HomeId] AND
		[Src].[AwayId] = [Target].[AwayId] AND
		CONVERT(DATE, [Src].[GameStart]) = CONVERT(DATE, [Target].[GameStart]) --date only
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Source]		= [Src].[Source]
				,[Target].[SportId]			= [Src].[SportId]
				,[Target].[CountryId]		= [Src].[CountryId]
				,[Target].[LeagueId]		= [Src].[LeagueId]	
				,[Target].[HomeId]			= [Src].[HomeId]		
				,[Target].[AwayId]			= [Src].[AwayId]		
				,[Target].[GameStart]		= [Src].[GameStart]
				,[Target].[Status]			= [Src].[Status]		
				,[Target].[IsCup]			= [Src].[IsCup]		
				,[Target].[HomeOdd]			= [Src].[HomeOdd]		
				,[Target].[DrawOdd]			= [Src].[DrawOdd]		
				,[Target].[AwayOdd]			= [Src].[AwayOdd]		
				,[Target].[HomeScore]		= [Src].[HomeScore]
				,[Target].[AwayScore]		= [Src].[AwayScore]
				,[Target].[LastUpdate]		= [Src].[LastUpdate]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Source]	
				,[SportId]	
				,[CountryId]		
				,[LeagueId]	
				,[HomeId]		
				,[AwayId]		
				,[GameStart]	
				,[Status]	
				,[IsCup]		
				,[HomeOdd]	
				,[DrawOdd]	
				,[AwayOdd]	
				,[HomeScore]		
				,[AwayScore]		
				,[LastUpdate]
				)
		VALUES ([Src].[Source]
	  			,[Src].[SportId]
				,[Src].[CountryId]
	  			,[Src].[LeagueId]
				,[Src].[HomeId]
	  			,[Src].[AwayId]
				,[Src].[GameStart]
	  			,[Src].[Status]
				,[Src].[IsCup]
	  			,[Src].[HomeOdd]
				,[Src].[DrawOdd]
	  			,[Src].[AwayOdd]
				,[Src].[HomeScore]
	  			,[Src].[AwayScore]
				,[Src].[LastUpdate]
				)
				;

RETURN 0

GO

---DROP 2
CREATE TABLE [dbo].[Feed_Bg_BasketballQuarters]
(	[MatchId]		INT		NOT NULL,
	[LeagueId]		INT		NOT NULL,
	[HomeId]		INT		NOT NULL,
	[AwayId]		INT		NOT NULL,
	[HomeQ1]		INT		NOT NULL,
	[AwayQ2]		INT		NOT NULL,
	[AwayQ1]		INT		NOT NULL,
	[HomeQ2]		INT		NOT NULL,
	[HomeQ3]		INT		NOT NULL,
	[AwayQ3]		INT		NOT NULL,
	[HomeQ4]		INT		NOT NULL,
	[AwayQ4]		INT		NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Bg_BasketballQuarters]
	ADD CONSTRAINT [Feed_Bg_BasketballQuarters_PK]
	PRIMARY KEY ([MatchId], [LeagueId], [HomeId], [AwayId])
GO
CREATE TYPE [dbo].[Feed_Bg_BasketballQuarters_UDT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[MatchId]		INT						NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[HomeId]		INT						NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SportId], [MatchId], [LeagueId], [HomeId], [AwayId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedBg_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

  SELECT FB.*,
      FBB.[HomeQ1],
      FBB.[AwayQ1],
      FBB.[HomeQ2],
      FBB.[AwayQ2],
      FBB.[HomeQ3],
      FBB.[AwayQ3],
      FBB.[HomeQ4],
      FBB.[AwayQ4] 
  FROM [dbo].[Feed_Bg] FB
  LEFT JOIN [dbo].[Feed_Bg_BasketballQuarters] FBB
  ON FBB.MatchId = FB.MatchId AND 
     FBB.LeagueId = FB.LeagueId AND
     FBB.HomeId = FB.HomeId AND
     FBB.AwayId = FB.AwayId 
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
GO

CREATE PROCEDURE [dbo].[FeedBg_Delete]
	@Collection Feed_Bg_UDT READONLY
AS

DELETE dbo.[Feed_Bg] 
	FROM dbo.[Feed_Bg]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg].[SportId] = [Target].[SportId] AND
		[Feed_Bg].[CountryId] = [Target].[CountryId] AND
		[Feed_Bg].[LeagueId] = [Target].[LeagueId] AND
		[Feed_Bg].[HomeId] = [Target].[HomeId] AND
		[Feed_Bg].[AwayId] = [Target].[AwayId] AND
		CONVERT(DATE, [Feed_Bg].[GameStart]) = CONVERT(DATE, [Target].[GameStart]) --date only
RETURN 0
GO