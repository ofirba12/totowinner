﻿USE BgData
GO
BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[BgManualMapper]
(
	[BgId]				INT NOT NULL,
	[ManualName]		NVARCHAR(600) NOT NULL
)
GO
ALTER TABLE [dbo].[BgManualMapper]
	ADD CONSTRAINT [FK_BgManualMapper_BgMapper_BgId]
	FOREIGN KEY (BgId)
	REFERENCES [BgMapper] (BgId)
GO
CREATE TYPE [dbo].BgManualMapperUDT AS TABLE
(
	[BgId]					INT NOT NULL,
	[ManualName]			NVARCHAR(600) NOT NULL
)
GO
CREATE PROCEDURE [dbo].BgManualMapperMerge
	@MapperParameter BgManualMapperUDT READONLY
AS

MERGE dbo.[BgManualMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND
		   [Source].[ManualName] = [Target].[ManualName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
				,[ManualName]
				)
		VALUES ([Source].[BgId]
				,[Source].[ManualName]
				)
				;
RETURN 0


GO
ALTER PROCEDURE [dbo].[BgMapper_Fetch]
AS
	SELECT BG.BgId, BG.BgSport, BG.BgType, BG.BgName, BGGM.GoalserveId, BGGM.GoalserveName FROM BgMapper BG
	LEFT JOIN BgGoalserveMapper BGGM
		ON BG.BgId = BGGM.BgId
	LEFT JOIN BgManualMapper BGMM
		ON BG.BgId = BGMM.BgId
RETURN 0
GO
ALTER PROCEDURE [dbo].[BgSystem_Merge]
	@Name	VARCHAR(900),
	@Value	VARCHAR(900),
	@ValueType TINYINT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[BgSystem] AS [Target]
	USING ( SELECT	 @Name		AS	"Name"
					,@Value		AS	"Value"
					,@ValueType AS	"ValueType"
	) AS [Source]	ON [Source].[Name]	= [Target].[Name]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Value]		= [Source].[Value],
				   [Target].[ValueType]		= [Source].[ValueType]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Name]
				,[Value]
				,[ValueType])
		VALUES ([Source].[Name]
				,[Source].[Value]
				,[Source].[ValueType]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
ALTER PROCEDURE [dbo].[BgMapper_Fetch]
AS
	SELECT BG.BgId, BG.BgSport, BG.BgType, BG.BgName, BGGM.GoalserveId, BGGM.GoalserveName, BGMM.ManualName FROM BgMapper BG
	LEFT JOIN BgGoalserveMapper BGGM
		ON BG.BgId = BGGM.BgId
	LEFT JOIN BgManualMapper BGMM
		ON BG.BgId = BGMM.BgId
RETURN 0
