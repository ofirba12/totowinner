﻿USE BgData
GO
DROP TABLE [dbo].[BgGoalserveMapper]
DROP TABLE [dbo].[BgMapper]
GO
ALTER PROCEDURE [MapperMerge]
AS
BEGIN
SELECT 1
END
GO
ALTER PROCEDURE [BgGoalserveMapperMerge]
AS
BEGIN
SELECT 1
END
GO
DROP TYPE [BgMapperUDT]
DROP TYPE [BgGoalserveMapperUDT]
GO
CREATE TABLE [dbo].[BgMapper]
(
	[BgId]			INT NOT NULL,
	[BgSport]		TINYINT NOT NULL,
	[BgType]		TINYINT NOT NULL,
	[BgName]		NVARCHAR(600) NULL
)
--countries start with 100
--leagues start with 10000
--teams start with 100000
ALTER TABLE [dbo].[BgMapper]
	ADD CONSTRAINT [PK_BgMapper]
	PRIMARY KEY (BgId)
GO
CREATE TYPE [dbo].[BgMapperUDT] AS TABLE
(
	[BgId]					INT NOT NULL,
	[BgSport]				TINYINT NOT NULL,
	[BgType]				TINYINT NOT NULL,
	[BgName]				NVARCHAR(600) NULL
PRIMARY KEY CLUSTERED 
(
	[BgId],[BgSport],[BgType]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE TYPE [dbo].BgGoalserveMapperUDT AS TABLE
(
	[BgId]					INT NOT NULL,
	[GoalserveId]			INT NULL,
	[GoalserveName]			NVARCHAR(600) NOT NULL
)
GO
CREATE TABLE [dbo].[BgGoalserveMapper]
(
	[BgId]				INT NOT NULL,
	[GoalserveId]		INT NULL,
	[GoalserveName]		NVARCHAR(600) NOT NULL
)
GO
ALTER TABLE [dbo].[BgGoalserveMapper]
	ADD CONSTRAINT [FK_BgMapper_BgId]
	FOREIGN KEY (BgId)
	REFERENCES [BgMapper] (BgId)
GO

ALTER PROCEDURE [dbo].[MapperMerge]
	@MapperParameter BgMapperUDT READONLY
AS

MERGE dbo.[BgMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND 
		   [Source].[BgSport] = [Target].[BgSport] AND
		   [Source].[BgType] = [Target].[BgType] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[BgName] = [Source].[BgName]				   
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
				,[BgSport]
	  			,[BgType]
				,[BgName]
				)
		VALUES ([Source].[BgId]
				,[Source].[BgSport]
				,[Source].[BgType]
	  			,[Source].[BgName]
				)
				;

RETURN 0

GO
ALTER PROCEDURE [dbo].BgGoalserveMapperMerge
	@MapperParameter BgGoalserveMapperUDT READONLY
AS

MERGE dbo.[BgGoalserveMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[BgId] = [Target].[BgId] AND
		   ISNULL([Source].[GoalserveId],0) = ISNULL([Target].[GoalserveId],0) AND
		   [Source].[GoalserveName] = [Target].[GoalserveName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [BgId]
	  			,[GoalserveId]
				,[GoalserveName]
				)
		VALUES ([Source].[BgId]
				,[Source].[GoalserveId]
				,[GoalserveName]
				)
				;
RETURN 0
GO
ALTER PROCEDURE [dbo].[BgMapper_Fetch]
AS
	SELECT BG.BgId, BG.BgSport, BG.BgType, BG.BgName, BGGM.GoalserveId, BGGM.GoalserveName FROM BgMapper BG
	LEFT JOIN BgGoalserveMapper BGGM
		ON BG.BgId = BGGM.BgId
RETURN 0

GO