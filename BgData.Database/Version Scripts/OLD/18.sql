﻿USE BgData
GO
BEGIN TRANSACTION
--COMMIT
--ROLLBACK
DROP PROCEDURE [FeedBg_Quarters_Merge]
DROP TYPE [Feed_Bg_Quarters_UDT]
GO
DROP TABLE [Feed_Bg_BasketballQuarters]
GO
CREATE TABLE [dbo].[Feed_Bg_BasketballQuarters]
(	[MatchId]		BIGINT	NOT NULL,
	[HomeQ1]		INT		NOT NULL,
	[AwayQ2]		INT		NOT NULL,
	[AwayQ1]		INT		NOT NULL,
	[HomeQ2]		INT		NOT NULL,
	[HomeQ3]		INT		NOT NULL,
	[AwayQ3]		INT		NOT NULL,
	[HomeQ4]		INT		NOT NULL,
	[AwayQ4]		INT		NOT NULL
)

GO
ALTER TABLE [dbo].[Feed_Bg_BasketballQuarters]
	ADD CONSTRAINT [Feed_Bg_BasketballQuarters_PK]
	PRIMARY KEY ([MatchId])
GO
CREATE TYPE [dbo].[Feed_Bg_Quarters_UDT] AS TABLE(
	[MatchId]		BIGINT					NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[MatchId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)
GO

CREATE PROCEDURE [dbo].[FeedBg_Quarters_Merge]
	@Collection Feed_Bg_Quarters_UDT READONLY
AS

MERGE dbo.[Feed_Bg_BasketballQuarters] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId] = [Target].[MatchId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[HomeQ1]	= [Src].[HomeQ1]
				,[Target].[AwayQ1]		= [Src].[AwayQ1]
				,[Target].[HomeQ2]		= [Src].[HomeQ2]
				,[Target].[AwayQ2]		= [Src].[AwayQ2]	
				,[Target].[HomeQ3]		= [Src].[HomeQ3]	
				,[Target].[AwayQ3]		= [Src].[AwayQ3]
				,[Target].[HomeQ4]		= [Src].[HomeQ4]	
				,[Target].[AwayQ4]		= [Src].[AwayQ4]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [MatchId]	
				,[HomeQ1]	
				,[AwayQ1]		
				,[HomeQ2]
				,[AwayQ2]
				,[HomeQ3]	
				,[AwayQ3]	
				,[HomeQ4]	
				,[AwayQ4]
				)
		VALUES ([Src].[MatchId]
	  			,[Src].[HomeQ1]
				,[Src].[AwayQ1]		
				,[Src].[HomeQ2]
				,[Src].[AwayQ2]
				,[Src].[HomeQ3]	
				,[Src].[AwayQ3]	
				,[Src].[HomeQ4]	
				,[Src].[AwayQ4]
				)
				;

RETURN 0
GO
---------------------------------------------------------

ALTER PROCEDURE [dbo].[FeedBg_Clean]
	@Collection BgMatchIdsUDT READONLY
AS

DELETE dbo.[Feed_Bg] 
	FROM dbo.[Feed_Bg]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg].[MatchId] = [Target].[MatchId] 

DELETE dbo.[Feed_Bg_BasketballQuarters] 
	FROM dbo.[Feed_Bg_BasketballQuarters]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg_BasketballQuarters].[MatchId] = [Target].[MatchId] 
RETURN 0
GO