﻿USE BgData
GO
DROP TABLE [Feed_Goalserve]
GO
CREATE TABLE [dbo].[Feed_Goalserve]
(
	[MatchId]		INT IDENTITY(1,1)		NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NOT NULL,
	[League]		NVARCHAR(600)			NOT NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NOT NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL



)
GO
ALTER TABLE [dbo].[Feed_Goalserve]
	ADD CONSTRAINT [Feed_Goalserve_PK]
	PRIMARY KEY (MatchId)
GO
CREATE TABLE [dbo].[Feed_Goalserve_BasketballQuarters]
(
	[MatchId] INT NOT NULL,
	[HomeQ1] INT NOT NULL,
	[AwayQ1] INT NOT NULL,
	[HomeQ2] INT NOT NULL,
	[AwayQ2] INT NOT NULL,
	[HomeQ3] INT NOT NULL,
	[AwayQ3] INT NOT NULL,
	[HomeQ4] INT NOT NULL,
	[AwayQ4] INT NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Goalserve_BasketballQuarters]
	ADD CONSTRAINT [Feed_Goalserve_BasketballQuarters_PK]
	PRIMARY KEY (MatchId)
GO
ALTER PROCEDURE Feed_Goalserve_BulkInsert
AS
BEGIN
SELECT 1
END

GO

ALTER PROCEDURE Feed_Goalserve_BulkUpdate
AS
BEGIN
SELECT 1
END
GO

DROP TYPE Feed_Goalserve_TT
GO
CREATE TYPE [dbo].[Feed_Goalserve_TT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NULL,
	[League]		NVARCHAR(600)			NULL,
	[LeagueId]		INT						NOT NULL,
	[IsCup]			BIT						NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NULL,
	[HomeId]		INT						NOT NULL,
	[Away]			NVARCHAR(600)			NULL,
	[AwayId]		INT						NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	PRIMARY KEY CLUSTERED 
(
	[SportId],[LeagueId],[HomeId],[AwayId],[GameStart] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE TYPE [dbo].[Feed_Goalserve_BasketballQuarters_TT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[CountryId]		INT						NULL,
	[LeagueId]		INT						NOT NULL,
	[HomeId]		INT						NOT NULL,
	[AwayId]		INT						NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SportId],[LeagueId],[HomeId],[AwayId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)
GO

ALTER PROCEDURE [dbo].[Feed_Goalserve_BulkInsert]
	@Feed AS [Feed_Goalserve_TT] READONLY,
	@QuartersFeed AS [Feed_Goalserve_BasketballQuarters_TT] READONLY
AS
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @LastUpdateTS DATETIME = GetDate();

	INSERT INTO [Feed_Goalserve](
		[SportId]	 ,
		[Status]	 ,
		[CountryId]	 ,
		[Country]	 ,
		[League]	 ,
		[LeagueId]	 ,
		[IsCup]		 ,
		[GameStart]	 ,
		[Home]		 ,
		[HomeId]	 ,
		[Away]		 ,
		[AwayId]	 ,
		[HomeOdd]	 ,
		[DrawOdd]	 ,
		[AwayOdd]	 ,
		[HomeScore]	 ,
		[AwayScore]	 , 
		[LastUpdate]
	)
	SELECT [SportId]	,
			[Status]	,
			[CountryId]	,
			[Country]	,
			[League]	,
			[LeagueId]	,
			[IsCup]		,
			[GameStart]	,
			[Home]		,
			[HomeId]	,
			[Away]		,
			[AwayId]	,
			[HomeOdd]	,
			[DrawOdd]	,
			[AwayOdd]	,
			[HomeScore]	,
			[AwayScore]	,
			[LastUpdate] = @LastUpdateTS
	FROM @Feed;

	INSERT INTO [Feed_Goalserve_BasketballQuarters](
		[MatchId],
		[HomeQ1],
		[AwayQ1],
		[HomeQ2],
		[AwayQ2],
		[HomeQ3],
		[AwayQ3],
		[HomeQ4],
		[AwayQ4]
	)
	SELECT [MatchId], [HomeQ1], [AwayQ1], [HomeQ2], [AwayQ2], [HomeQ3], [AwayQ3], [HomeQ4], [AwayQ4]
	FROM (
		SELECT [MatchId], F.[SportId], F.[CountryId], F.[LeagueId], F.[GameStart], F.[HomeId], F.[AwayId], [HomeQ1], [AwayQ1], [HomeQ2], [AwayQ2], [HomeQ3], [AwayQ3], [HomeQ4], [AwayQ4]
		FROM [Feed_Goalserve] FG
		JOIN @Feed F ON FG.[SportId] = F.[SportId] 
			AND FG.[LeagueId] = F.[LeagueId]
			AND FG.[GameStart] = F.[GameStart]
			AND FG.[HomeId] = F.[HomeId]
			AND FG.[AwayId] = F.[AwayId]
		JOIN @QuartersFeed QF ON QF.[SportId] = F.[SportId]
			AND QF.[LeagueId] = F.[LeagueId]
			AND QF.[HomeId] = F.[HomeId]
			AND QF.[AwayId] = F.[AwayId]
	) AS JoinedFeed;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
GO

ALTER PROCEDURE [dbo].[Feed_Goalserve_BulkUpdate]
	@Feed AS [Feed_Goalserve_TT] READONLY,
    @QuartersFeed AS [Feed_Goalserve_BasketballQuarters_TT] READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
    DECLARE @LastUpdateTS DATETIME = GetDate();

    UPDATE Feed_Goalserve
    SET
        Feed_Goalserve.[Status]    = ISNULL(F.[Status],		FG.[Status]),
        Feed_Goalserve.[HomeOdd]   = ISNULL(F.[HomeOdd],    FG.[HomeOdd]),
        Feed_Goalserve.[DrawOdd]   = ISNULL(F.[DrawOdd],    FG.[DrawOdd]),
        Feed_Goalserve.[AwayOdd]   = ISNULL(F.[AwayOdd],    FG.[AwayOdd]),
        Feed_Goalserve.[HomeScore] = ISNULL(F.[HomeScore],  FG.[HomeScore]),
        Feed_Goalserve.[AwayScore] = ISNULL(F.[AwayScore],  FG.[AwayScore]),
        Feed_Goalserve.[LastUpdate]= @LastUpdateTS
    FROM Feed_Goalserve FG
        INNER JOIN @Feed F
        ON FG.SportId = F.SportId AND 
            FG.LeagueId = F.LeagueId AND 
            FG.HomeId = F.HomeId AND
            FG.AwayId = F.AwayId AND
			FG.GameStart = F.GameStart

    UPDATE Feed_Goalserve_BasketballQuarters
    SET
        Feed_Goalserve_BasketballQuarters.HomeQ1 = A.HomeQ1,
        Feed_Goalserve_BasketballQuarters.HomeQ2 = A.HomeQ2,
        Feed_Goalserve_BasketballQuarters.HomeQ3 = A.HomeQ3,
        Feed_Goalserve_BasketballQuarters.HomeQ4 = A.HomeQ4,
        Feed_Goalserve_BasketballQuarters.AwayQ1 = A.AwayQ1,
        Feed_Goalserve_BasketballQuarters.AwayQ2 = A.AwayQ2,
        Feed_Goalserve_BasketballQuarters.AwayQ3 = A.AwayQ3,
        Feed_Goalserve_BasketballQuarters.AwayQ4 = A.AwayQ4
    FROM Feed_Goalserve_BasketballQuarters FGB
		INNER JOIN (
				SELECT FG.MatchId, QF.* FROM
					Feed_Goalserve FG
				INNER JOIN  @QuartersFeed QF
				ON QF.SportId = FG.SportId AND 
					QF.LeagueId = FG.LeagueId AND 
					QF.HomeId = FG.HomeId AND
					QF.AwayId = FG.AwayId
				INNER JOIN @Feed F
				ON FG.SportId = F.SportId AND 
					FG.LeagueId = F.LeagueId AND 
					FG.HomeId = F.HomeId AND
					FG.AwayId = F.AwayId AND
					FG.GameStart = F.GameStart
		) A
		ON 
		A.MatchId = FGB.MatchId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
ALTER PROCEDURE [dbo].[Feed_Goalserve_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS

  SELECT FG.*, 
      FGB.[HomeQ1],
      FGB.[AwayQ1],
      FGB.[HomeQ2],
      FGB.[AwayQ2],
      FGB.[HomeQ3],
      FGB.[AwayQ3],
      FGB.[HomeQ4],
      FGB.[AwayQ4] FROM [dbo].[Feed_Goalserve] FG
  LEFT JOIN [dbo].[Feed_Goalserve_BasketballQuarters] FGB
  ON FGB.MatchId = FG.MatchId
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE())) AND DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))
RETURN 0
GO