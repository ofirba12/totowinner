﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[Feed_Bg_StatisticsDetails]
(
	[MatchId]						BIGINT					NOT NULL,
	[Type]							TINYINT					NOT NULL,
	[StatisticIndex]				TINYINT					NOT NULL,
	[Population]					TINYINT					NOT NULL,
	[Lines]							INT						NULL,
	[Percent]						TINYINT					NULL,
	[LastUpdate]					DATETIME				NOT NULL
)
/* Type
FullTime-HomeWin-HomeWin
FullTime-HomeWin-AwayLoose
FullTime-HomeWin-H2H
FullTime-Draw-HomeDraw
FullTime-Draw-AwayDraw
FullTime-Draw-H2H
FullTime-AwayWin-HomeLoose
FullTime-AwayWin-AwayWin
FullTime-AwayWin-H2H
OverUnder-Over3_5-HomeOver
OverUnder-Over3_5-AwayOver
OverUnder-Under3_5-HomeUnder
OverUnder-Under3_5-AwayUnder
OverUnder-Over3_5-H2H
OverUnder-Unde3_5-H2H
OverUnder-Over2_5-HomeOver
OverUnder-Over2_5-AwayOver
OverUnder-Under2_5-HomeUnder
OverUnder-Under2_5-AwayUnder
OverUnder-Over2_5-H2H
OverUnder-Under2_5-H2H
OverUnder-Over1_5-HomeOver
OverUnder-Over1_5-AwayOver
OverUnder-Under1_5-HomeUnder
OverUnder-Under1_5-AwayUnder
OverUnder-Over1_5-H2H
OverUnder-Unde1_5-H2H
Range-InRange0_1-Home
Range-InRange0_1-Away
Range-InRange0_1-H2H
Range-InRange2_3-Home
Range-InRange2_3-Away
Range-InRange2_3-H2H
Range-InRange4Pplus-Home
Range-InRange4Pplus-Away
Range-InRange4Pplus-H2H
BothScore-Yes-Home
BothScore-Yes-Away
BothScore-Yes-H2H
BothScore-No-Home
BothScore-No-Away
BothScore-No-H2H
WinDrawLose-WinDraw-HomeWinDraw
WinDrawLose-WinDraw-AwayDrawLose
WinDrawLose-WinDraw-H2H
WinDrawLose-NoDraw-Home
WinDrawLose-NoDraw-Away
WinDrawLose-NoDraw-H2H
WinDrawLose-DrawLose-Home
WinDrawLose-DrawLose-Away
WinDrawLose-DrawLose-H2H
*/
ALTER TABLE [dbo].[Feed_Bg_StatisticsDetails]
	ADD CONSTRAINT [PK_Feed_Bg_StatisticsDetails]
	PRIMARY KEY ([MatchId], [Type], [StatisticIndex], [Population])
GO
CREATE TYPE [dbo].[Feed_Bg_StatisticsDetails_UDT] AS TABLE(
	[MatchId]						BIGINT					NOT NULL,
	[Type]							TINYINT					NOT NULL,
	[StatisticIndex]				TINYINT					NOT NULL,
	[Population]					TINYINT					NOT NULL,
	[Lines]							INT						NULL,
	[Percent]						TINYINT					NULL,
	[LastUpdate]					DATETIME				NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId], [Type], [StatisticIndex], [Population] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedBgStatisticsDetails_Merge]
	@Collection Feed_Bg_StatisticsDetails_UDT READONLY
AS

MERGE dbo.[Feed_Bg_StatisticsDetails] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId]				= [Target].[MatchId] AND
		[Src].[Type]				= [Target].[Type] AND
		[Src].[StatisticIndex]		= [Target].[StatisticIndex] AND
		[Src].[Population]			= [Target].[Population]
	WHEN MATCHED THEN 
		UPDATE SET
		[Target].[Lines]			=[Src].[Lines]		
		,[Target].[Percent]			=[Src].[Percent]			
		,[Target].[LastUpdate]		=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[MatchId]
		,[Type]
		,[StatisticIndex]	
		,[Population]		
		,[Lines]	
		,[Percent]	
		,[LastUpdate]		
				)
		VALUES (
			[Src].[MatchId]			
			,[Src].[Type]
			,[Src].[StatisticIndex]	
			,[Src].[Population]		
			,[Src].[Lines]	
			,[Src].[Percent]	
			,[Src].[LastUpdate]	
				)
				;

RETURN 0

GO