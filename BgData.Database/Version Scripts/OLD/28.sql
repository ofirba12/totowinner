﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE PROCEDURE [dbo].[FeedBg_FetchByMatchId]
	@MatchId BIGINT
AS

  SELECT FB.*,
      FBB.[HomeQ1],
      FBB.[AwayQ1],
      FBB.[HomeQ2],
      FBB.[AwayQ2],
      FBB.[HomeQ3],
      FBB.[AwayQ3],
      FBB.[HomeQ4],
      FBB.[AwayQ4] 
  FROM [dbo].[Feed_Bg] FB
  LEFT JOIN [dbo].[Feed_Bg_BasketballQuarters] FBB
    ON FBB.MatchId = FB.MatchId  
  WHERE FB.[MatchId] = @MatchId
RETURN 0

GO