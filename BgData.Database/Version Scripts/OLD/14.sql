﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TYPE [dbo].[BgMatchIdsUDT] AS TABLE(
	[MatchId]		BIGINT					NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[MatchId]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedBg_Clean]
	@Collection BgMatchIdsUDT READONLY
AS

DELETE dbo.[Feed_Bg] 
	FROM dbo.[Feed_Bg]
	INNER JOIN @Collection AS [Target] ON 
		[Feed_Bg].[MatchId] = [Target].[MatchId] 
RETURN 0
GO