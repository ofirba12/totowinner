﻿USE BgData
GO
BEGIN TRANSACTION
--COMMIT
--ROLLBACK
--DROP PROCEDURE 
DROP TYPE Feed_Bg_BasketballQuarters_UDT
GO
DROP TABLE Feed_Bg_BasketballQuarters
GO
CREATE TABLE [dbo].[Feed_Bg_BasketballQuarters]
(	[MatchId]		INT		NOT NULL,
	[HomeQ1]		INT		NOT NULL,
	[AwayQ2]		INT		NOT NULL,
	[AwayQ1]		INT		NOT NULL,
	[HomeQ2]		INT		NOT NULL,
	[HomeQ3]		INT		NOT NULL,
	[AwayQ3]		INT		NOT NULL,
	[HomeQ4]		INT		NOT NULL,
	[AwayQ4]		INT		NOT NULL
)
GO
ALTER TABLE [dbo].[Feed_Bg_BasketballQuarters]
	ADD CONSTRAINT [Feed_Bg_BasketballQuarters_PK]
	PRIMARY KEY ([MatchId])
GO
CREATE TYPE [dbo].[Feed_Bg_Quarters_UDT] AS TABLE(
	[MatchId]		INT						NOT NULL,
	[HomeQ1]		INT						NOT NULL,
	[AwayQ1]		INT						NOT NULL,
	[HomeQ2]		INT						NOT NULL,
	[AwayQ2]		INT						NOT NULL,
	[HomeQ3]		INT						NOT NULL,
	[AwayQ3]		INT						NOT NULL,
	[HomeQ4]		INT						NOT NULL,
	[AwayQ4]		INT						NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[MatchId] ASC
	)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[FeedBg_Quarters_Merge]
	@Collection Feed_Bg_Quarters_UDT READONLY
AS

MERGE dbo.[Feed_Bg_BasketballQuarters] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[MatchId] = [Target].[MatchId] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[HomeQ1]	= [Src].[HomeQ1]
				,[Target].[AwayQ1]		= [Src].[AwayQ1]
				,[Target].[HomeQ2]		= [Src].[HomeQ2]
				,[Target].[AwayQ2]		= [Src].[AwayQ2]	
				,[Target].[HomeQ3]		= [Src].[HomeQ3]	
				,[Target].[AwayQ3]		= [Src].[AwayQ3]
				,[Target].[HomeQ4]		= [Src].[HomeQ4]	
				,[Target].[AwayQ4]		= [Src].[AwayQ4]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [MatchId]	
				,[HomeQ1]	
				,[AwayQ1]		
				,[HomeQ2]
				,[AwayQ2]
				,[HomeQ3]	
				,[AwayQ3]	
				,[HomeQ4]	
				,[AwayQ4]
				)
		VALUES ([Src].[MatchId]
	  			,[Src].[HomeQ1]
				,[Src].[AwayQ1]		
				,[Src].[HomeQ2]
				,[Src].[AwayQ2]
				,[Src].[HomeQ3]	
				,[Src].[AwayQ3]	
				,[Src].[HomeQ4]	
				,[Src].[AwayQ4]
				)
				;

RETURN 0


GO
ALTER PROCEDURE [dbo].[FeedBg_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

  SELECT FB.*,
      FBB.[HomeQ1],
      FBB.[AwayQ1],
      FBB.[HomeQ2],
      FBB.[AwayQ2],
      FBB.[HomeQ3],
      FBB.[AwayQ3],
      FBB.[HomeQ4],
      FBB.[AwayQ4] 
  FROM [dbo].[Feed_Bg] FB
  LEFT JOIN [dbo].[Feed_Bg_BasketballQuarters] FBB
    ON FBB.MatchId = FB.MatchId  
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
GO