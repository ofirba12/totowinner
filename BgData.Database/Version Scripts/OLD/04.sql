﻿USE BgData
GO
ALTER PROCEDURE [dbo].[Feed_Goalserve_Fetch]
	@SportId INT,
	@DaysAdd INT -- 0: Today, -1: Yesterday, 1: Tommorrow
AS
  DECLARE @D1 DATETIME = DATEADD(dd, @DaysAdd, DATEDIFF(dd, 0, GETDATE()))
  DECLARE @D2 DATETIME = DATEADD(dd, @DaysAdd+1, DATEDIFF(dd, 0, GETDATE()))

  SELECT FG.*, 
      FGB.[HomeQ1],
      FGB.[AwayQ1],
      FGB.[HomeQ2],
      FGB.[AwayQ2],
      FGB.[HomeQ3],
      FGB.[AwayQ3],
      FGB.[HomeQ4],
      FGB.[AwayQ4] FROM [dbo].[Feed_Goalserve] FG
  LEFT JOIN [dbo].[Feed_Goalserve_BasketballQuarters] FGB
  ON FGB.MatchId = FG.MatchId
  WHERE [SportId] = @SportId
	AND [GameStart] BETWEEN @D1 AND DATEADD(mi, -1, @D2)
RETURN 0
