﻿USE BgData
GO
CREATE TABLE [dbo].[BgSystem]
(
	[Name] VARCHAR(900) NOT NULL,
	[Value] VARCHAR(900) NOT NULL,
	[ValueType] TINYINT NOT NULL
)
/*
1 - INT
2 - DATETIME
3 - BOOL
*/
GO
ALTER TABLE [dbo].[BgSystem]
	ADD CONSTRAINT [PK_System]
	PRIMARY KEY ([Name])
GO
CREATE PROCEDURE [dbo].[BgSystem_Merge]
	@Name	VARCHAR(900),
	@Value	VARCHAR(900),
	@ValueType TINYINT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[System] AS [Target]
	USING ( SELECT	 @Name		AS	"Name"
					,@Value		AS	"Value"
					,@ValueType AS	"ValueType"
	) AS [Source]	ON [Source].[Name]	= [Target].[Name]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Value]		= [Source].[Value],
				   [Target].[ValueType]		= [Source].[ValueType]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Name]
				,[Value]
				,[ValueType])
		VALUES ([Source].[Name]
				,[Source].[Value]
				,[Source].[ValueType]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE TABLE [dbo].[Feed_Manual]
(
	[MatchId]		INT IDENTITY(30000,1)	NOT NULL,
	[SportId]		INT						NOT NULL,
	[Status]		VARCHAR(100)			NULL,
	[CountryId]		INT						NULL,
	[Country]		NVARCHAR(200)			NOT NULL,
	[League]		NVARCHAR(600)			NOT NULL,
	[LeagueId]		INT						NULL,
	[IsCup]			BIT						NOT NULL,
	[GameStart]		DATETIME				NOT NULL,
	[Home]			NVARCHAR(600)			NOT NULL,
	[HomeId]		INT						NULL,
	[Away]			NVARCHAR(600)			NOT NULL,
	[AwayId]		INT						NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[HomeScore]		INT						NULL,
	[AwayScore]		INT						NULL,
	[LastUpdate]	DATETIME				NOT NULL,
	[HomeQ1]		INT						NULL,
	[AwayQ1]		INT						NULL,
	[HomeQ2]		INT						NULL,
	[AwayQ2]		INT						NULL,
	[HomeQ3]		INT						NULL,
	[AwayQ3]		INT						NULL,
	[HomeQ4]		INT						NULL,
	[AwayQ4]		INT						NULL,
	[Ignore]		BIT						NOT NULL
)


GO
ALTER TABLE [dbo].[Feed_Manual]
	ADD CONSTRAINT [PK_Feed_Manual]
	PRIMARY KEY ([MatchId])
GO
CREATE PROCEDURE [dbo].[Feed_Manual_Fetch]
	@LastFetched DATETIME = NULL
AS
	SELECT * FROM [dbo].[Feed_Manual]
	WHERE LastUpdate >= ISNULL(@LastFetched, [LastUpdate])
RETURN 0
GO
CREATE PROCEDURE [dbo].[Feed_Manual_Merge]
	@MatchId		INT,
	@SportId		INT,
	@Status			VARCHAR(100),
	@CountryId		INT = NULL,
	@Country		NVARCHAR(200),			
	@League			NVARCHAR(600),			
	@LeagueId		INT = NULL,
	@IsCup			BIT,					
	@GameStart		DATETIME,
	@Home			NVARCHAR(600),
	@HomeId			INT = NULL,		
	@Away			NVARCHAR(600),
	@AwayId			INT = NULL,		
	@HomeOdd		FLOAT,					
	@DrawOdd		FLOAT,					
	@AwayOdd		FLOAT,					
	@HomeScore		INT,				
	@AwayScore		INT,					
	@LastUpdate		DATETIME,
	@Ignore			BIT,					
	@HomeQ1			INT = NULL,				
	@AwayQ1			INT = NULL,					
	@HomeQ2			INT = NULL,					
	@AwayQ2			INT = NULL,					
	@HomeQ3			INT = NULL,					
	@AwayQ3			INT = NULL,					
	@HomeQ4			INT = NULL,					
	@AwayQ4			INT = NULL			
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Feed_Manual] AS [Target]
	USING ( SELECT @MatchId		AS "MatchId"
				,@SportId		AS "SportId"
				,@Status		AS "Status"
				,@CountryId		AS "CountryId"
				,@Country		AS "Country"
				,@League		AS "League"
				,@LeagueId		AS "LeagueId"
				,@IsCup			AS "IsCup"
				,@GameStart		AS "GameStart"
				,@Home			AS "Home"
				,@HomeId		AS "HomeId"
				,@Away			AS "Away"
				,@AwayId		AS "AwayId"
				,@HomeOdd		AS "HomeOdd"
				,@DrawOdd		AS "DrawOdd"
				,@AwayOdd		AS "AwayOdd"
				,@HomeScore		AS "HomeScore"
				,@AwayScore		AS "AwayScore"
				,@LastUpdate	AS "LastUpdate"
				,@Ignore		AS "Ignore"
				,@HomeQ1		AS "HomeQ1"
				,@AwayQ1		AS "AwayQ1"
				,@HomeQ2		AS "HomeQ2"
				,@AwayQ2		AS "AwayQ2"
				,@HomeQ3		AS "HomeQ3"
				,@AwayQ3		AS "AwayQ3"
				,@HomeQ4		AS "HomeQ4"
				,@AwayQ4		AS "AwayQ4"
	) AS [Source]	ON [Source].[MatchId]	= [Target].[MatchId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[SportId] = [Source].[SportId]
				,[Target].[Status] = [Source].[Status]
				,[Target].[CountryId] = [Source].[CountryId] 
				,[Target].[Country] = [Source].[Country]
				,[Target].[League] = [Source].[League]
				,[Target].[LeagueId] = [Source].[LeagueId]
				,[Target].[IsCup] = [Source].[IsCup]
				,[Target].[GameStart] = [Source].[GameStart]
				,[Target].[Home] = [Source].[Home]
				,[Target].[HomeId] = [Source].[HomeId]
				,[Target].[Away] = [Source].[Away]
				,[Target].[AwayId] = [Source].[AwayId]
				,[Target].[HomeOdd] = [Source].[HomeOdd]
				,[Target].[DrawOdd] = [Source].[DrawOdd]
				,[Target].[AwayOdd] = [Source].[AwayOdd]
				,[Target].[HomeScore] = [Source].[HomeScore]
				,[Target].[AwayScore] = [Source].[AwayScore]
				,[Target].[LastUpdate] = [Source].[LastUpdate]
				,[Target].[Ignore] = [Source].[Ignore]
				,[Target].[HomeQ1] = [Source].[HomeQ1]
				,[Target].[AwayQ1] = [Source].[AwayQ1]
				,[Target].[HomeQ2] = [Source].[HomeQ2]
				,[Target].[AwayQ2] = [Source].[AwayQ2]
				,[Target].[HomeQ3] = [Source].[HomeQ3]
				,[Target].[AwayQ3] = [Source].[AwayQ3]
				,[Target].[HomeQ4] = [Source].[HomeQ4]
				,[Target].[AwayQ4] = [Source].[AwayQ4]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([SportId]
			,[Status]	
			,[CountryId]
			,[Country]
			,[League]	
			,[LeagueId]
			,[IsCup]
			,[GameStart]
			,[Home]
			,[HomeId]
			,[Away]	
			,[AwayId]
			,[HomeOdd]	
			,[DrawOdd]	
			,[AwayOdd]	
			,[HomeScore]
			,[AwayScore]
			,[LastUpdate]	
			,[HomeQ1]	
			,[AwayQ1]		
			,[HomeQ2]		
			,[AwayQ2]		
			,[HomeQ3]		
			,[AwayQ3]		
			,[HomeQ4]		
			,[AwayQ4]	
			,[Ignore]
			)
		VALUES ([Source].[SportId]
			,[Source].[Status]	
			,[Source].[CountryId]
			,[Source].[Country]
			,[Source].[League]	
			,[Source].[LeagueId]
			,[Source].[IsCup]
			,[Source].[GameStart]
			,[Source].[Home]
			,[Source].[HomeId]
			,[Source].[Away]	
			,[Source].[AwayId]
			,[Source].[HomeOdd]	
			,[Source].[DrawOdd]	
			,[Source].[AwayOdd]	
			,[Source].[HomeScore]
			,[Source].[AwayScore]
			,[Source].[LastUpdate]	
			,[Source].[HomeQ1]	
			,[Source].[AwayQ1]		
			,[Source].[HomeQ2]		
			,[Source].[AwayQ2]		
			,[Source].[HomeQ3]		
			,[Source].[AwayQ3]		
			,[Source].[HomeQ4]		
			,[Source].[AwayQ4]				
			,[Source].[Ignore]				
			)
			OUTPUT Inserted.MatchId
			;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END


GO