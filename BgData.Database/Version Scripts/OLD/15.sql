﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
ALTER PROCEDURE [dbo].[FeedBg_Merge]
	@Collection Feed_Bg_UDT READONLY
AS

MERGE dbo.[Feed_Bg] AS [Target]
	USING @Collection AS [Src]	
		ON --[Src].[Source] = [Target].[Source] AND
		[Src].[SportId] = [Target].[SportId] AND
		--[Src].[CountryId] = [Target].[CountryId] AND
		--[Src].[LeagueId] = [Target].[LeagueId] AND
		[Src].[HomeId] = [Target].[HomeId] AND
		[Src].[AwayId] = [Target].[AwayId] AND
		CONVERT(DATE, [Src].[GameStart]) = CONVERT(DATE, [Target].[GameStart]) --date only
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Source]		= [Src].[Source]
				,[Target].[SportId]			= [Src].[SportId]
				,[Target].[CountryId]		= [Src].[CountryId]
				,[Target].[LeagueId]		= [Src].[LeagueId]	
				,[Target].[HomeId]			= [Src].[HomeId]		
				,[Target].[AwayId]			= [Src].[AwayId]		
				,[Target].[GameStart]		= [Src].[GameStart]
				,[Target].[Status]			= [Src].[Status]		
				,[Target].[IsCup]			= [Src].[IsCup]		
				,[Target].[HomeOdd]			= [Src].[HomeOdd]		
				,[Target].[DrawOdd]			= [Src].[DrawOdd]		
				,[Target].[AwayOdd]			= [Src].[AwayOdd]		
				,[Target].[HomeScore]		= [Src].[HomeScore]
				,[Target].[AwayScore]		= [Src].[AwayScore]
				,[Target].[LastUpdate]		= [Src].[LastUpdate]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Source]	
				,[SportId]	
				,[CountryId]		
				,[LeagueId]	
				,[HomeId]		
				,[AwayId]		
				,[GameStart]	
				,[Status]	
				,[IsCup]		
				,[HomeOdd]	
				,[DrawOdd]	
				,[AwayOdd]	
				,[HomeScore]		
				,[AwayScore]		
				,[LastUpdate]
				)
		VALUES ([Src].[Source]
	  			,[Src].[SportId]
				,[Src].[CountryId]
	  			,[Src].[LeagueId]
				,[Src].[HomeId]
	  			,[Src].[AwayId]
				,[Src].[GameStart]
	  			,[Src].[Status]
				,[Src].[IsCup]
	  			,[Src].[HomeOdd]
				,[Src].[DrawOdd]
	  			,[Src].[AwayOdd]
				,[Src].[HomeScore]
	  			,[Src].[AwayScore]
				,[Src].[LastUpdate]
				)
				;

RETURN 0


GO