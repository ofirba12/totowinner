﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
ALTER TABLE [dbo].[BgWinnerMapper]
	ADD [GoalserveId]	INT				NULL
GO

ALTER PROCEDURE [dbo].[BgWinnerMapperMerge]
	@MapperParameter BgWinnerMapperUDT READONLY
AS

MERGE dbo.[BgWinnerMapper] AS [Target]
	USING @MapperParameter AS [Source]	
		ON [Source].[SportId] = [Target].[SportId] AND 
		   [Source].[WinnerId] = [Target].[WinnerId] 
	--WHEN MATCHED THEN 
	--	UPDATE SET	[Target].[WinnerName] = [Source].[WinnerName],				   
	--				[Target].[BgId] = [Source].[BgId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SportId]
	  			,[WinnerId]
				,[WinnerName]
				,[BgId]
				,[GoalserveId]
				)
		VALUES ([Source].[SportId]
				,[Source].[WinnerId]
	  			,[Source].[WinnerName]
				,NULL
				,NULL
				)
				;

RETURN 0
GO

CREATE PROCEDURE [dbo].[MapperWinner_UpdateGoalserveId]
	@SportId		TINYINT,
	@WinnerId		INT,
	@GoalserveId	INT
AS
	UPDATE BgWinnerMapper 
	SET GoalserveId = @GoalserveId
	WHERE SportId = @SportId AND WinnerId = @WinnerId
RETURN 0
GO

