﻿USE BgData
GO
--BEGIN TRANSACTION
--COMMIT
--ROLLBACK
CREATE TABLE [dbo].[Feed_Goalserve_Odds]
(
	[SportId]		INT						NOT NULL,
	[MatchId]		INT						NOT NULL,
	[Bookmaker]		NVARCHAR(200)			NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	[Inserted]		DATETIME				NOT NULL,
	[LastUpdate]	DATETIME				NULL
)
GO
ALTER TABLE [dbo].[Feed_Goalserve_Odds]
	ADD CONSTRAINT [Feed_Goalserve_Odds_PK]
	PRIMARY KEY ([SportId], [MatchId], [Bookmaker], [Inserted])
GO
CREATE TYPE [dbo].[Feed_Goalserve_Odds_UDT] AS TABLE(
	[SportId]		INT						NOT NULL,
	[MatchId]		INT						NOT NULL,
	[Bookmaker]		NVARCHAR(200)			NOT NULL,
	[HomeOdd]		FLOAT					NULL,
	[DrawOdd]		FLOAT					NULL,
	[AwayOdd]		FLOAT					NULL,
	PRIMARY KEY CLUSTERED 
(
	[SportId],[MatchId],[Bookmaker]  ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[Feed_Goalserve_Odds_Merge]
	@Collection Feed_Goalserve_Odds_UDT READONLY
AS

DECLARE @CurrentTS DATETIME = GetDate();

MERGE dbo.[Feed_Goalserve_Odds] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[SportId] = [Target].[SportId] AND 
		[Src].[MatchId] = [Target].[MatchId] AND
		[Src].[Bookmaker] = [Target].[Bookmaker] AND
		[Src].[HomeOdd] = [Target].[HomeOdd] AND
		[Src].[DrawOdd] = [Target].[DrawOdd] AND
		[Src].[AwayOdd] = [Target].[AwayOdd] 
	WHEN MATCHED THEN 
		UPDATE SET [Target].[LastUpdate] = @CurrentTS
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SportId]
				,[MatchId]
				,[Bookmaker]
				,[HomeOdd]	
				,[DrawOdd]	
				,[AwayOdd]	
				,[Inserted]
				)
		VALUES ([Src].[SportId]
	  			,[Src].[MatchId]
				,[Src].[Bookmaker]
	  			,[Src].[HomeOdd]	
				,[Src].[DrawOdd]	
	  			,[Src].[AwayOdd]	
				,@CurrentTS
				)
				;

RETURN 0
GO
CREATE PROCEDURE [dbo].[Feed_Goalserve_Odds_Fetch]
	@SportId INT,
	@GoalserveHomeId INT,
	@GoalserveAwayId INT,
	@GameStart DATETIME
AS
	SELECT FGO.* 
	FROM [dbo].[Feed_Goalserve] FG
	JOIN [Feed_Goalserve_Odds] FGO
	ON FG.MatchId = FGO.MatchId
	WHERE FG.SportId = @SportId and FG.HomeId=@GoalserveHomeId and AwayId=@GoalserveAwayId AND GameStart=@GameStart

RETURN 0
GO
