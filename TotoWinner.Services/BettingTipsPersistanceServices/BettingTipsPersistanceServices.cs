﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Service;
using TotoWinner.Services.Persistance.BettingTips;
using TotoWinner.Services.Persistance.TotoWinner;

namespace TotoWinner.Services
{
    public partial class BettingTipsPersistanceServices : Singleton<BettingTipsPersistanceServices>, IBettingTipsPersistanceServices
    {
        private const string FinishTipGeneration = "FinishTipGeneration";
        #region GetAllTippers
        public List<Tippers_GetAll_Result> GetAllTippers()
        {
            try
            {
                List<Tippers_GetAll_Result> tippers = null;
                using (var ent = new BettingTipsEntities())
                {

                    tippers = ent.Tippers_GetAll().ToList();
                }
                return tippers;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying get all tippers", ex);
            }
        }
        #endregion

        #region UpdateTipper
        public void UpdateTipper(int tipperId, string tipperName)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {

                    ent.Tippers_Merge(tipperId, tipperName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying update tipperId [{tipperId}], tipper [{tipperName}]", ex);
            }
        }
        #endregion

        #region AddTipper
        public int AddTipper(string tipperName)
        {
            try
            {
                var tipperId = -1;
                using (var ent = new BettingTipsEntities())
                {

                    var result = ent.Tippers_Merge(-1, tipperName);
                    tipperId = result.First().Value;
                }
                return tipperId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying add tipper [{tipperName}]", ex);
            }
        }
        #endregion
        #region DeleteTipper
        public void DeleteTipper(int tipperId)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {

                    ent.Tipper_Delete(tipperId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying delete tipperId [{tipperId}]", ex);
            }
        }
        #endregion
        public void UpdateFinishTipGenerationState(bool finishedGeneration)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.SystemState_Merge(FinishTipGeneration, finishedGeneration.ToString().ToLower());
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to set FinishTipGeneration flag in SystemState table", ex);
            }
        }
        public bool ReadFinishTipGenerationState()
        {
            try
            {
                var flag = false;
                using (var ent = new BettingTipsEntities())
                {
                    var val = ent.SystemStates.Where(i => i.Name == FinishTipGeneration).FirstOrDefault();
                    if (val != null && !bool.TryParse(val.Value, out flag))
                    {
                        flag = false;
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to read FinishTipGeneration flag from SystemState table", ex);
            }
        }

        public void AddProgram(long programId, DateTime programDate)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.Programs_Merge(programId, programDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add program {programId} with date {programDate}", ex);
            }
        }
        public List<Persistance.BettingTips.Program> GetAllPrograms()
        {
            try
            {
                List<Persistance.BettingTips.Program> programs = null;
                using (var ent = new BettingTipsEntities())
                {
                    programs = ent.Programs.ToList();
                }
                return programs;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all programs", ex);
            }
        }
        public DateTime? GetProgramDate(long programId)
        {
            try
            {
                DateTime? programDate = null;
                using (var ent = new BettingTipsEntities())
                {
                    var program = ent.Programs.Where(p => p.ProgramId == programId).FirstOrDefault();
                    programDate = program?.ProgramDate;
                }
                return programDate;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get program {programId}", ex);
            }
        }

        public int InsertTip(long programId,
            int gameIndex,
            int sportId,
            int totoId,
            DateTime gameStart,
            bool tipAutoType,
            short powerTip,
            string betName,
            string betLeague,
            string country,
            bool betParent,
            char betMark,
            bool isSingle,
            double betRate,
            string status,
            string tipTime,
            string tipContent,
            string tipMarks,
            string tipUrl,
            int tipperId)
        {
            var tipTypeStr = tipAutoType == true ? "Automatic" : "Manual";
            try
            {
                var insertedTS = DateTime.Now;
                var tipId = -1;
                using (var ent = new BettingTipsEntities())
                {
                    //using (var transaction = ent.Database.BeginTransaction())
                    //{
                    var result = ent.Tips_Merge(programId,
                    gameIndex,
                    sportId,
                    totoId,
                    gameStart,
                    tipAutoType,
                    (byte)powerTip,
                    insertedTS,
                    null,
                    betName,
                    betLeague,
                    country,
                    betParent,
                    betMark.ToString(),
                    isSingle,
                    betRate,
                    status,
                    tipTime,
                    tipContent,
                    tipMarks, tipUrl, tipperId);
                    tipId = result.First().Value;

                    ent.Tips_Audit(programId, gameIndex, insertedTS, $"{tipTypeStr} Tip {tipId} Inserted {(SportIds)sportId} [{betName}]");

                    //transaction.Commit();
                    //}
                }
                return tipId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add {tipTypeStr} tip for {(SportIds)sportId}  for program {programId} gameIndex {gameIndex}", ex);
            }
        }

        public void UpdateTipWithOfficialResults(long programId, int gameIndex, string mark, string result)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.Tips_UpdateResults(programId, gameIndex, result, mark, null, null);
                    ent.Tips_Audit(programId, gameIndex, DateTime.Now, $"Tip automatic results changed to {result} mark [{mark}]");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying update official result for tip programId {programId}, game index {gameIndex}, mark [{result}], result [{result}]", ex);
            }
        }
        public void UpdateTipWithManualResults(long programId, int gameIndex, string mark, string result)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.Tips_UpdateResults(programId, gameIndex, null, null, result, mark);
                    ent.Tips_Audit(programId, gameIndex, DateTime.Now, $"Tip maual results changed to {result} mark [{mark}]");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying update manual result for tip programId {programId}, game index {gameIndex}, mark [{mark}],  result [{result}]", ex);
            }
        }
        public List<Tips_GetAll_Result> GetAllTips()
        {
            try
            {
                List<Tips_GetAll_Result> tips = null;
                using (var ent = new BettingTipsEntities())
                {
                    tips = ent.Tips_GetAll().ToList();
                }
                return tips;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all tips", ex);
            }
        }
        public List<int> GetAllSubscriberTodayAndFutureTipIds(long subscriber)
        {
            try
            {
                List<int> tips = null;
                using (var ent = new BettingTipsEntities())
                {
                    var query =
                       from su in ent.SubscribersUrls
                       join t in ent.Tips on su.TipId equals t.TipId
                       where su.SubscriberId == subscriber
                       select t;

                    tips = query
                        .Where(t => DbFunctions.TruncateTime(t.GameStart) >= DateTime.Today.Date)
                        .Select(i => i.TipId)
                        .ToList();
                }
                return tips;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all subscriber {subscriber} tips that were sent today", ex);
            }
        }
        public Dictionary<int, int> CountClicks(List<int> tipIds)
        {
            //SELECT TipId, COUNT(*) AS total
            //FROM [SubscribersUrls]
            //WHERE VisitUrlTS IS NOT NULL
            //GROUP BY TipId;
            Dictionary<int, int> counters;
            using (var ent = new BettingTipsEntities())
            {
                var clickCounts =
                        from p in ent.SubscribersUrls
                        where tipIds.Contains(p.TipId) && p.VisitUrlTS != null 
                        group p by p.TipId into g
                        select new { TipId = g.Key, ClicksCount = g.Count() };
                counters = clickCounts.ToList().ToDictionary(x => x.TipId, c => c.ClicksCount);

            }
            return counters;
        }
        public List<Persistance.BettingTips.Tip> GetAllStackTips()
        {
            try
            {
                List<Persistance.BettingTips.Tip> tips = null;
                using (var ent = new BettingTipsEntities())
                {
                    tips = ent.Tips.Where(t => t.Status == "פתוח")
                        .OrderByDescending(p => p.PowerTip)
                        .ThenBy(d => d.GameStart)
                        .ToList()
                        ;

                }
                return tips;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all tips", ex);
            }
        }
        public Persistance.BettingTips.Tip GetTip(int tipId)
        {
            try
            {
                Persistance.BettingTips.Tip tip = null;
                using (var ent = new BettingTipsEntities())
                {
                    tip = ent.Tips.FirstOrDefault(t => t.TipId == tipId);
                }
                return tip;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to tip with Id {tipId}", ex);
            }
        }
        public List<long> GetPrograms(DateTime programDate)
        {
            try
            {
                List<long> programIds = new List<long>();
                using (var ent = new BettingTipsEntities())
                {
                    programIds = ent.Programs.Where(p => DbFunctions.TruncateTime(p.ProgramDate) == programDate)
                        .Select(i => i.ProgramId)
                        .ToList()
                        ;
                }
                return programIds;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get programs for programDate {programDate}", ex);
            }
        }

        public List<Persistance.BettingTips.Tip> GetWarehouseTips(List<long> programs)
        {
            try
            {
                List<Persistance.BettingTips.Tip> tips = null;
                using (var ent = new BettingTipsEntities())
                {
                    tips = ent.Tips.Where(t => programs.Contains(t.ProgramId) && t.Status == "סגור")
                        .OrderByDescending(p => p.PowerTip)
                        .ThenBy(d => d.GameStart)
                        .ToList()
                        ;
                }
                return tips;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get warehouse tips programs {string.Join(",", programs)}", ex);
            }
        }
        public List<Tips_GetAllWithNoResults_Result> GetAllTipsWithNoResults()
        {
            try
            {
                List<Tips_GetAllWithNoResults_Result> tips = null;
                using (var ent = new BettingTipsEntities())
                {
                    tips = ent.Tips_GetAllWithNoResults().ToList();
                }
                return tips;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all tips with no results", ex);
            }
        }
        public List<Tips_GetCloseForBetCandidates_Result> GetCloseForBetCandidates()
        {
            try
            {
                List<Tips_GetCloseForBetCandidates_Result> tips = null;
                using (var ent = new BettingTipsEntities())
                {
                    tips = ent.Tips_GetCloseForBetCandidates().ToList();
                }
                return tips;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all tips close for bets candidates", ex);
            }
        }
        public void EraseAllDatabase()
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.EraseAllData();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to erase all data", ex);
            }
        }
        public void UpdateTipStatus(long programId, int gameIndex, string status)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.Tips_UpdateStatus(programId, gameIndex, status);
                    ent.Tips_Audit(programId, gameIndex, DateTime.Now, $"Tip Status changed to {status}");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying update tip status", ex);
            }
        }
        public bool IsTipperInUse(int tipperId)
        {
            try
            {
                var inUse = false;
                using (var ent = new BettingTipsEntities())
                {
                    inUse = ent.Tips.Any(t=> t.TipperId == tipperId);
                }
                return inUse;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all tips", ex);
            }
        }
    }
}
