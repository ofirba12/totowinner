﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Service;

namespace TotoWinner.Services
{
    public partial class BettingTipsPersistanceServices : Singleton<BettingTipsPersistanceServices>, IBettingTipsPersistanceServices
    {
        private BettingTipsPersistanceServices() 
        {
            if (!Directory.Exists(BettingTipsConstants.RepositoryPath))
                Directory.CreateDirectory(BettingTipsConstants.RepositoryPath);
            if (!Directory.Exists(BettingTipsConstants.ResultPath))
                Directory.CreateDirectory(BettingTipsConstants.ResultPath);
        }

        public void CsvDump(BettingTipsStacks stackes, BettingTipsEventsRepository infoRepository)
        {
            var csvData = new List<List<string>>();
            var filename = $"{infoRepository.RoundId}_STACK.csv";
            var filepath = Path.Combine(BettingTipsConstants.ResultPath, filename);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath, false, System.Text.Encoding.UTF8))
            {
                for (var powertip = 6; powertip >= 0; powertip--)
                {
                    foreach (var stack in stackes.PowertipStacks[powertip])
                    {
                        var key = new BettingTipKey(stack.EventId, stack.Type);
                        if (stackes.EventTotalData.ContainsKey(stack.EventId))
                        {
                            var line = GenerateLine(infoRepository.Events[stack.EventId], stackes.EventTotalData[stack.EventId], stackes.TipDescription[key], stack.Type);
                            csvData.Add(line);
                        }
                    }
                    if (csvData.Count > 0)
                        file.WriteLine($"Powertip {powertip}");
                    foreach (var csvLine in csvData)
                        file.WriteLine(string.Join(",", csvLine));
                    csvData.Clear();
                }
            }
            Console.WriteLine($"File {filename} has been created under {BettingTipsConstants.ResultPath} folder.");
        }

        private List<string> GenerateLine(BettingTipsParentEvent bettingTipsParentEvent, BettingTipsTotalsRepository bettingTipsTotalsRepository, BettingTip tip, BetType type)
        {
            var line = new List<string>();
            line.Add(bettingTipsParentEvent.Place.ToString());
            line.Add(bettingTipsParentEvent.Time.ToString("t"));
            line.Add(bettingTipsParentEvent.Status);
            line.Add(bettingTipsParentEvent.Bet.Name.Replace(',', ';'));
            line.Add(bettingTipsParentEvent.LeagueName.Replace(',', ';'));
            line.Add(bettingTipsParentEvent.IsFather ? "אבא" : "בן");
            line.Add(bettingTipsParentEvent.Bet.Single.ToString());
            line.Add(tip.Time);
            line.Add(tip.Content);
            line.Add(tip.Marking);
            line.Add(tip.PrefixURL);
            line.Add(tip.URL);
            var btr = bettingTipsTotalsRepository;
            switch (type)
            {
                case BetType.Home:
                    line.Add("Home");
                    line.Add(bettingTipsParentEvent.Bet.Home.ToString());
                    GenerateGroupLine(line, btr.HomeLastGameTotals.HomeWinPercent, btr.HomeLastGameTotals.HomeMomentumWinPercent, btr.HomeLastGameTotals.VerboseHome.ToList());
                    GenerateGroupLine(line, btr.GuestLastGameTotals.HomeWinPercent, btr.GuestLastGameTotals.HomeMomentumWinPercent, btr.GuestLastGameTotals.VerboseHome.ToList());
                    GenerateGroupLine(line, btr.HomeLastGameTotals.HomeExactWinPercent, btr.HomeLastGameTotals.HomeExactMomentumWinPercent, btr.HomeLastGameTotals.VerboseExactHome.ToList());
                    GenerateGroupLine(line, btr.GuestLastGameTotals.HomeExactWinPercent, btr.GuestLastGameTotals.HomeExactMomentumWinPercent, btr.GuestLastGameTotals.VerboseExactHome.ToList());
                    GenerateGroupLine(line, btr.Head2HeadTotals.HomeWinPercent, btr.Head2HeadTotals.HomeMomentumWinPercent, btr.Head2HeadTotals.VerboseHome.Take(10).ToList());
                    GenerateGroupLine(line, btr.Head2HeadTotals.HomeExactWinPercent, btr.Head2HeadTotals.HomeExactMomentumWinPercent, btr.Head2HeadTotals.VerboseExactHome.Take(10).ToList());
                    break;
                case BetType.Draw:
                    line.Add("Draw");
                    line.Add(bettingTipsParentEvent.Bet.Draw.ToString());
                    GenerateGroupLine(line, btr.HomeLastGameTotals.DrawWinPercent, btr.HomeLastGameTotals.DrawMomentumWinPercent, btr.HomeLastGameTotals.VerboseDraw.ToList());
                    GenerateGroupLine(line, btr.GuestLastGameTotals.DrawWinPercent, btr.GuestLastGameTotals.DrawMomentumWinPercent, btr.GuestLastGameTotals.VerboseDraw.ToList());
                    GenerateGroupLine(line, btr.HomeLastGameTotals.DrawExactWinPercent, btr.HomeLastGameTotals.DrawExactMomentumWinPercent, btr.HomeLastGameTotals.VerboseExactDraw.ToList());
                    GenerateGroupLine(line, btr.GuestLastGameTotals.DrawExactWinPercent, btr.GuestLastGameTotals.DrawExactMomentumWinPercent, btr.GuestLastGameTotals.VerboseExactDraw.ToList());
                    GenerateGroupLine(line, btr.Head2HeadTotals.DrawWinPercent, btr.Head2HeadTotals.DrawMomentumWinPercent, btr.Head2HeadTotals.VerboseDraw.Take(10).ToList());
                    GenerateGroupLine(line, btr.Head2HeadTotals.DrawExactWinPercent, btr.Head2HeadTotals.DrawExactMomentumWinPercent, btr.Head2HeadTotals.VerboseExactDraw.Take(10).ToList());
                    break;
                case BetType.Away:
                    line.Add("Away");
                    line.Add(bettingTipsParentEvent.Bet.Away.ToString());
                    GenerateGroupLine(line, btr.HomeLastGameTotals.AwayWinPercent, btr.HomeLastGameTotals.AwayMomentumWinPercent, btr.HomeLastGameTotals.VerboseAway.ToList());
                    GenerateGroupLine(line, btr.GuestLastGameTotals.AwayWinPercent, btr.GuestLastGameTotals.AwayMomentumWinPercent, btr.GuestLastGameTotals.VerboseAway.ToList());
                    GenerateGroupLine(line, btr.HomeLastGameTotals.AwayExactWinPercent, btr.HomeLastGameTotals.AwayExactMomentumWinPercent, btr.HomeLastGameTotals.VerboseExactAway.ToList());
                    GenerateGroupLine(line, btr.GuestLastGameTotals.AwayExactWinPercent, btr.GuestLastGameTotals.AwayExactMomentumWinPercent, btr.GuestLastGameTotals.VerboseExactAway.ToList());
                    GenerateGroupLine(line, btr.Head2HeadTotals.AwayWinPercent, btr.Head2HeadTotals.AwayMomentumWinPercent, btr.Head2HeadTotals.VerboseAway.Take(10).ToList());
                    GenerateGroupLine(line, btr.Head2HeadTotals.AwayExactWinPercent, btr.Head2HeadTotals.AwayExactMomentumWinPercent, btr.Head2HeadTotals.VerboseExactAway.Take(10).ToList());
                    break;
            }
            return line;
        }

        private void GenerateGroupLine(List<string> line, decimal winPercent, decimal momentumWinPercent, List<int> verbose, bool isSummaryLine = false)
        {
            var percent = !isSummaryLine ? "%" : "";
            line.Add($"{Math.Round(winPercent, 0).ToString()}{percent}");
            line.Add($"{Math.Round(momentumWinPercent, 0).ToString()}{percent}");
        }
    }
}
