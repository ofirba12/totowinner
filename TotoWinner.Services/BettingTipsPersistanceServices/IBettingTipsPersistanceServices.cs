﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;
using TotoWinner.Services.Persistance.BettingTips;

namespace TotoWinner.Service
{
    public interface IBettingTipsPersistanceServices
    {
        void CsvDump(BettingTipsStacks stackes, BettingTipsEventsRepository infoRepository);

        void AddProgram(long programId, DateTime programDate);
        DateTime? GetProgramDate(long programId);

        int InsertTip(long programId,
                    int gameIndex,
                    int sportId,
                    int totoId,
                    DateTime gameStart,
                    bool tipAutoType,
                    short powerTip,
                    string betName,
                    string betLeague,
                    string country,
                    bool betParent,
                    char betMark, //1,X,2
                    bool isSingle,
                    double betRate,
                    string status,
                    string tipTime,
                    string tipContent,
                    string tipMarks,
                    string tipUrl,
                    int tipperId);

        List<Tips_GetAll_Result> GetAllTips();
        List<Services.Persistance.BettingTips.Tip> GetAllStackTips();
        Services.Persistance.BettingTips.Tip GetTip(int tipId);
        List<long> GetPrograms(DateTime programDate);
        List<Program> GetAllPrograms();
        List<Services.Persistance.BettingTips.Tip> GetWarehouseTips(List<long> programs);
        List<Tips_GetAllWithNoResults_Result> GetAllTipsWithNoResults();
        List<Tips_GetCloseForBetCandidates_Result> GetCloseForBetCandidates();
        Dictionary<int, int> CountClicks(List<int> tipIds);

        void UpdateTipWithOfficialResults(long programId, int gameIndex, string tipOfficialMark, string tipOfficialResult);
        void UpdateTipWithManualResults(long programId, int gameIndex, string tipManualMark, string tipManualResult);
        void UpdateTipStatus(long programId, int gameIndex, string status);

        void EraseAllDatabase();
        void UpdateFinishTipGenerationState(bool finishedGeneration);
        List<int> GetAllSubscriberTodayAndFutureTipIds(long subscriber);
        bool IsTipperInUse(int tipperId);
    }
}
