﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public static class BettingTipsServicesHelpers
    {
        public static T DynamicResolver<T>(dynamic parent,
            GoalServeDataAttributes? child,
            GoalServeDataAttributes attribute,
            ResolveEnum type,
            Dictionary<GoalServeDataAttributes, string> mapper)
        {
            if (mapper.ContainsKey(attribute))
                return DynamicResolver<T>(parent,
                    child.HasValue
                        ? mapper[child.Value]
                        : null,
                    mapper[attribute],
                    type);
            return default(T);
        }
        public static T DynamicResolver<T>(dynamic parent, string child, string attribute, ResolveEnum type)
        {
            dynamic dAttribute = null;
            try
            {
                var dNode = string.IsNullOrEmpty(child)
                    ? parent
                    : parent[child];
                dNode.TryGetValue(attribute, out dAttribute);
                switch (type)
                {
                    case ResolveEnum.INT_NULLABLE:
                        return string.IsNullOrEmpty(dAttribute.ToString())
                            ? null
                            : int.Parse(dAttribute.ToString());
                    case ResolveEnum.INT:
                        return int.Parse(dAttribute.ToString());
                    case ResolveEnum.BOOL:
                        return bool.Parse(dAttribute.ToString());
                    case ResolveEnum.DATE2:
                        return DateTime.ParseExact(dAttribute.ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    case ResolveEnum.DATE:
                        return DateTime.Parse(dAttribute.ToString());
                    case ResolveEnum.DECIMAL:
                        return decimal.Parse(dAttribute);
                    case ResolveEnum.STRING:
                        return dAttribute.ToString();
                    case ResolveEnum.LONG:
                        return long.Parse(dAttribute.ToString());
                }
                throw new Exception($"{child}:{attribute} with type {type} is not supported");
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to resolve value=[{dAttribute?.ToString()}] of {child}:{attribute} to type {type}", ex);
            }
        }
        internal static string Hebrew(string message)
        {
            return new string(message.Reverse().ToArray());
        }
        public static string GetSportEmoji(string sport)
        {
            var emoji = string.Empty;
            switch (sport)
            {
                case "כדורגל":
                    emoji = GetSportEmoji(SportIds.Soccer);
                    break;
                case "כדורסל":
                    emoji = GetSportEmoji(SportIds.Basketball);
                    break;
                case "טניס":
                    emoji = GetSportEmoji(SportIds.Tennis);
                    break;
                case "כדור יד":
                    emoji = GetSportEmoji(SportIds.Handball);
                    break;
                case "בייסבול":
                    emoji = GetSportEmoji(SportIds.Baseball);
                    break;
                case "פוטבול אמריקאי":
                    emoji = GetSportEmoji(SportIds.Football);
                    break;
            }
            return emoji;
        }
        public static string GetSportEmoji(SportIds sport)
        {
            var emoji = string.Empty;
            switch (sport)
            {
                case SportIds.Soccer:
                    emoji = $"⚽";
                    break;
                case SportIds.Basketball:
                    emoji = $"🏀";
                    break; 
                case SportIds.Tennis:
                    emoji = $"🎾";
                    break;
                case SportIds.Handball:
                    emoji = $"🤾";
                    break;
                case SportIds.Baseball:
                    emoji = $"⚾";
                    break;
                case SportIds.Football:
                    emoji = $"🏈";
                    break;
            }
            return emoji;
        }
        public static string HebrewTranslate(SportIds sport)
        {
            var hebrew = "--";
            switch (sport)
            {
                case SportIds.Soccer:
                    hebrew = "כדורגל";
                    break;
                case SportIds.Basketball:
                    hebrew = "כדורסל";
                    break;
                case SportIds.Tennis:
                    hebrew = "טניס";
                    break;
                case SportIds.Handball:
                    hebrew = "כדור יד";
                    break;
                case SportIds.Baseball:
                    hebrew = "בייסבול";
                    break;
                case SportIds.Football:
                    hebrew = "פוטבול אמריקאי";
                    break;
            }
            return hebrew;
        }
        internal static Tuple<string, string> HebrewTranslate(DateTime date)
        {
            CultureInfo jewishCulture = CultureInfo.CreateSpecificCulture("he-IL");
            jewishCulture.DateTimeFormat.Calendar = new HebrewCalendar();

            var hebrew = new Tuple<string, string>(date.ToString("dddd", jewishCulture).ToString(), $"{date.Hour}:{date.Minute.ToString("D2")}");
            return hebrew;
        }

        //string countryCode = "US";
        //string countryName = "United States";
        //string countryFlagEmoji = GetCountryFlagEmoji(countryCode, countryName);
        public static string GetCountryFlagEmoji(string countryCode)
        {
            if (string.IsNullOrEmpty(countryCode))
                return string.Empty;
            // Convert the country code to uppercase
            countryCode = countryCode.ToUpper();

            // Calculate the Unicode code points for the two characters in the country code
            int firstCodePoint = 0x1F1E6 + (countryCode[0] - 'A');
            int secondCodePoint = 0x1F1E6 + (countryCode[1] - 'A');

            // Convert the code points to Unicode characters and concatenate them with the country name
            string countryFlagEmoji = char.ConvertFromUtf32(firstCodePoint) + char.ConvertFromUtf32(secondCodePoint);
            return countryFlagEmoji;
        }
        public static string GetHebrewCountryCode(string hebrewCountry)
        {

            var countryCodesInHebrew = new Dictionary<string, string>()
            {
                { "ארצות הברית", "US" },
                { "אנגליה", "GB" },
                { "סקוטלנד", "GB" },
                { "וויילס", "GB" },
                { "צרפת", "FR" },
                { "גרמניה", "DE" },
                { "איטליה", "IT" },
                { "ספרד", "ES" },
                { "יפן", "JP" },
                { "סין", "CN" },
                { "רוסיה", "RU" },
                { "ברזיל", "BR" },
                { "הודו", "IN" },
                { "אוסטרליה", "AU" },
                { "קנדה", "CA" },
                { "דרום קוריאה", "KR" },
                { "מקסיקו", "MX" },
                { "אינדונזיה", "ID" },
                { "הולנד", "NL" },
                { "ערב הסעודית", "SA" },
                { "טורקיה", "TR" },
                { "שווייץ", "CH" },
                { "שוודיה", "SE" },
                { "פולין", "PL" },
                { "ארגנטינה", "AR" },
                { "בלגיה", "BE" },
                { "תאילנד", "TH" },
                { "אוסטריה", "AT" },
                { "נורווגיה", "NO" },
                { "איחוד האמירויות", "AE" },
                { "איראן", "IR" },
                { "מלזיה", "MY" },
                { "דנמרק", "DK" },
                { "סינגפור", "SG" },
                { "דרום אפריקה", "ZA" },
                { "מצרים", "EG" },
                { "פינלנד", "FI" },
                { "הפיליפינים", "PH" },
                { "אירלנד", "IE" },
                { "צפון אירלנד", "IE" },
                { "ישראל", "IL" },
                { "פורטוגל", "PT" },
                { "יוון", "GR" },
                { "פקיסטן", "PK" },
                { "צ'כיה", "CZ" },
                { "הונגריה", "HU" },
                { "רומניה", "RO" },
                { "צ'ילה", "CL" },
                { "ניגריה", "NG" },
                { "קולומביה", "CO" },
                { "וייטנאם", "VN" },
                { "בנגלדש", "BD" },
                { "אוקראינה", "UA" },
                { "סרביה", "RS" },
                { "מרוקו", "MA" },
                { "עיראק", "IQ" },
                { "כווית", "KW" },
                { "אלג'יריה", "DZ" },
                { "עומאן", "OM" },
                { "קטאר", "QA" },
                { "תוניסיה", "TN" },
                { "לוקסמבורג", "LU" },
                { "בלארוס", "BY" },
                { "קרואטיה", "HR" },
                { "בולגריה", "BG" },
                { "אקוודור", "EC" },
                { "הרפובליקה הדומיניקנית", "DO" },
                { "ניו זילנד", "NZ" },
                { "גואטמלה", "GT" },
                { "אל סלבדור", "SV" },
                { "הונדורס", "HN" },
                { "ניקרגואה", "NI" },
                { "קוסטה ריקה", "CR" },
                { "פנמה", "PA" },
                { "סלובקיה", "SK" },
                { "ליטא", "LT" },
                { "סלובניה", "SI" },
                { "אסטוניה", "EE" },
                { "לטביה", "LV" },
                { "קזחסטן", "KZ" },
                { "אוזבקיסטן", "UZ" },
                { "קניה", "KE" },
                { "לבנון", "LB" },
                { "גאנה", "GH" },
                { "קפריסין", "CY" },
                { "אוגנדה", "UG" },
                { "מקדוניה", "MK" },
                { "מאוריציוס", "MU" },
                { "ארמניה", "AM" },
                { "אלבניה", "AL" },
                { "נפאל", "NP" },
                { "קמרון", "CM" },
                { "חוף השנהב", "CI" },
                { "סנגל", "SN" },
                { "ג'מייקה", "JM" },
                { "בוליביה", "BO" },
                { "טנזניה", "TZ" },
                { "פרגוואי", "PY" },
                {  "זימבבואה", "ZW" },
                { "בוטסואנה", "BW" },
                { "אורוגוואי", "UY" },
                { "פרו", "PE" }
            };
            return countryCodesInHebrew.ContainsKey(hebrewCountry)
                ? countryCodesInHebrew[hebrewCountry]
                : null;
        }
    }
}
//var countryNamesInHebrew = new Dictionary<string, string>()
//{
//    { "US", "ארצות הברית" },
//    { "GB", "הממלכה המאוחדת" },
//    { "FR", "צרפת" },
//    { "DE", "גרמניה" },
//    { "IT", "איטליה" },
//    { "ES", "ספרד" },
//    { "JP", "יפן" },
//    { "CN", "סין" },
//    { "RU", "רוסיה" },
//    { "BR", "ברזיל" },
//    { "IN", "הודו" },
//    { "AU", "אוסטרליה" },
//    { "CA", "קנדה" },
//    { "KR", "קוריאה הדרומית" },
//    { "MX", "מקסיקו" },
//    { "ID", "אינדונזיה" },
//    { "NL", "הולנד" },
//    { "SA", "ערב הסעודית" },
//    { "TR", "טורקיה" },
//    { "CH", "שווייץ" },
//    { "SE", "שוודיה" },
//    { "PL", "פולין" },
//    { "AR", "ארגנטינה" },
//    { "BE", "בלגיה" },
//    { "TH", "תאילנד" },
//    { "AT", "אוסטריה" },
//    { "NO", "נורווגיה" },
//    { "AE", "איחוד האמירויות הערביות" },
//    { "IR", "איראן" },
//    { "MY", "מלזיה" },
//    { "DK", "דנמרק" },
//    { "SG", "סינגפור" },
//    { "ZA", "דרום אפריקה" },
//    { "EG", "מצרים" },
//    { "FI", "פינלנד" },
//    { "PH", "הפיליפינים" },
//    { "IE", "אירלנד" },
//    { "IL", "ישראל" },
//    { "PT", "פורטוגל" },
//    { "GR", "יוון" },
//    { "PK", "פקיסטן" },
//    { "CZ", "צ'כיה" },
//    { "HU", "הונגריה" },
//    { "RO", "רומניה" },
//    { "CL", "צ'ילה" },
//    { "NG", "ניגריה" },
//    { "CO", "קולומביה" },
//    { "VN", "וייטנאם" },
//    { "BD", "בנגלדש" },
//    { "UA", "אוקראינה" },
//    { "RS", "סרביה" },
//    { "MA", "מרוקו" },
//    { "IQ", "עיראק" },
//    { "KW", "כווית" },
//    { "DZ", "אלג'יריה" },
//    { "OM", "עומאן" },
//    { "QA", "קטאר" },
//    { "TN", "תוניסיה" },
//    { "LU", "לוקסמבורג" },
//    { "BY", "בלארוס" },
//    { "HR", "קרואטיה" },
//    { "BG", "בולגריה" },
//    { "EC", "אקוודור" },
//    { "DO", "הרפובליקה הדומיניקנית" },
//    { "NZ", "ניו זילנד" },
//    { "GT", "גואטמלה" },
//    { "SV", "אל סלבדור" },
//    { "HN", "הונדורס" },
//    { "NI", "ניקרגואה" },
//    { "CR", "קוסטה ריקה" },
//    { "PA", "פנמה" },
//    { "SK", "סלובקיה" },
//    { "LT", "ליטא" },
//    { "SI", "סלובניה" },
//    { "EE", "אסטוניה" },
//    { "LV", "לטביה" },
//    { "KZ", "קזחסטן" },
//    { "UZ", "אוזבקיסטן" },
//    { "KE", "קניה" },
//    { "LB", "לבנון" },
//    { "GH", "גאנה" },
//    { "CY", "קפריסין" },
//    { "UG", "אוגנדה" },
//    { "MK", "מקדוניה" },
//    { "MU", "מאוריציוס" },
//    { "AM", "ארמניה" },
//    { "AL", "אלבניה" },
//    { "NP", "נפאל" },
//    { "CM", "קמרון" },
//    { "CI", "חוף השנהב" },
//    { "SN", "סנגל" },
//    { "JM", "ג'מייקה" },
//    { "BO", "בוליביה" },
//    { "TZ", "טנזניה" },
//    { "PY", "פרגוואי" },
//    { "ZW", "זימבבואה" },
//    { "BW", "בוטסואנה" }
//};

