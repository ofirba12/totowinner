﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Service;

namespace TotoWinner.Services
{
    public partial class BettingTipsServices : Singleton<BettingTipsServices>, IBettingTipsServices
    {
        public BettingTipsStacks CollectData(BettingTipsEventsRepository repository, int momentum)
        {
            var allTotals = new Dictionary<int, BettingTipsTotalsRepository>(); //eventid, totals
            var powertipStacks = new Dictionary<int, List<BettingTipKey>>() // power, {eventid, type}
            {
                { 0 , new List<BettingTipKey>() },
                { 1 , new List<BettingTipKey>() },
                { 2 , new List<BettingTipKey>() },
                { 3 , new List<BettingTipKey>() },
                { 4 , new List<BettingTipKey>() },
                { 5 , new List<BettingTipKey>() },
                { 6 , new List<BettingTipKey>() }
            };
            var tipsCollection = new Dictionary<BettingTipKey, BettingTip>(); //{eventid, bettype}, BettingTip
            var errors = new List<string>();
            foreach (var eventData in repository.Events.Values)
            {
                CollectEventData(momentum, allTotals, powertipStacks, tipsCollection, eventData, errors);
            }
            var stacks = new BettingTipsStacks(allTotals, powertipStacks, tipsCollection, errors);
            return stacks;
        }

        private void CollectEventData(int momentum, 
            Dictionary<int, BettingTipsTotalsRepository> allTotals, 
            Dictionary<int, List<BettingTipKey>> powertipStacks, 
            Dictionary<BettingTipKey, BettingTip> tipsCollection, 
            BettingTipsParentEvent eventData,
            List<string> errors)
        {
            try
            {
                var category = FindCategory(eventData);
                if (category.HasValue)
                {
                    var homeLastGameTotals = PrepareLastGames(eventData.LastGames.Games[eventData.HomeId], eventData.Bet, GameType.Home, category.Value, momentum);
                    var guestLastGameTotals = PrepareLastGames(eventData.LastGames.Games[eventData.GuestId], eventData.Bet, GameType.Guest, category.Value, momentum);
                    var h2h = PrepareHead2Head(eventData.HomeTeamName, eventData.GuestTeamName, eventData.Head2Head, eventData.Bet, category.Value, momentum);
                    var totals = new BettingTipsTotalsRepository(homeLastGameTotals, guestLastGameTotals, h2h);
                    if (eventData.Bet.Home != -1)
                    {
                        var key = new BettingTipKey(eventData.EventId, BetType.Home);
                        powertipStacks[totals.HomePowertip].Add(key);
                        var buildTipRequest = GenerateBuildTipRequest(eventData, totals, category.Value, momentum, key.Type);
                        var bettingTip = BuildTip(buildTipRequest);
                        tipsCollection.Add(key, bettingTip);
                    }
                    if (eventData.Bet.Draw != -1)
                    {
                        var key = new BettingTipKey(eventData.EventId, BetType.Draw);
                        powertipStacks[totals.DrawPowertip].Add(key);
                        var buildTipRequest = GenerateBuildTipRequest(eventData, totals, category.Value, momentum, key.Type);
                        var bettingTip = BuildTip(buildTipRequest);
                        tipsCollection.Add(key, bettingTip);
                    }
                    if (eventData.Bet.Away != -1)
                    {
                        var key = new BettingTipKey(eventData.EventId, BetType.Away);
                        powertipStacks[totals.AwayPowertip].Add(key);
                        var buildTipRequest = GenerateBuildTipRequest(eventData, totals, category.Value, momentum, key.Type);
                        var bettingTip = BuildTip(buildTipRequest);
                        tipsCollection.Add(key, bettingTip);
                    }
                    allTotals.Add(eventData.EventId, totals);
                }
            }
            catch(Exception ex)
            {
                errors.Add($"Skipping event: error found trying to collect event data for {eventData.SportId} game [{eventData.Place}] [{eventData.HomeTeamName}-{eventData.GuestTeamName}] league {eventData.LeagueName}{Environment.NewLine}{ex.ToString()}");
            }
        }

        private FileType? FindCategory(BettingTipsParentEvent eventData)
        {
            if ((SportIds)eventData.SportId == SportIds.Soccer && eventData.IsFather == true)
                return FileType.SoccerFather;
            if ((SportIds)eventData.SportId == SportIds.Soccer && eventData.IsFather == false && eventData.Bet.Name.Contains("(2-3)"))
                return FileType.SoccerChild23Gols;
            if ((SportIds)eventData.SportId == SportIds.Soccer && eventData.IsFather == false && eventData.Bet.Name.Contains(@"מעל/מתחת שערים (2.5)"))
                return FileType.SoccerChild2point5Gols;
            if ((SportIds)eventData.SportId == SportIds.Basketball && eventData.IsFather == false && eventData.Bet.Name.Contains(@"מעל/מתחת נקודות") &&
                    !eventData.Bet.Period.Contains("מחצית") &&
                    !eventData.Bet.Period.Contains("רבע"))
                return FileType.BasketballBarrier;
            if ((SportIds)eventData.SportId == SportIds.Basketball && eventData.IsFather == true && eventData.Bet.Name.Contains(@"+)"))
                return FileType.BasketBallFatherExtraPoints;

            return null;
        }
        internal BettingTipsTotalsGamesContainer PrepareLastGames(List<BettingTipsGameInfo> lastGames, BettingTipsBet bet, GameType gameType, FileType fileType, int momentum)
        {
            var verboseHome = new List<int>();
            var verboseDraw = new List<int>();
            var verboseAway = new List<int>();
            var verboseExactHome = new List<int>();
            var verboseExactDraw = new List<int>();
            var verboseExactAway = new List<int>();
            foreach (var game in lastGames)
            {
                CollectLastGamesExact(gameType, verboseExactHome, verboseExactDraw, verboseExactAway, game, bet, fileType);
                CollectLastGames(gameType, verboseHome, verboseDraw, verboseAway, game, bet, fileType);
            }
            var totals = new BettingTipsTotalsGamesContainer(momentum,
                verboseHome.ToArray(),
                verboseDraw.ToArray(),
                verboseAway.ToArray(),
                verboseExactHome.ToArray(),
                verboseExactDraw.ToArray(),
                verboseExactAway.ToArray());
            return totals;
        }
        internal void CollectLastGamesExact(GameType gameType, List<int> verboseExactHome, List<int> verboseExactDraw, List<int> verboseExactAway, BettingTipsGameInfo game, BettingTipsBet bet, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BasketBallFatherExtraPoints:
                    var teamScore = game.TeamScore + (bet.BarrierHome ?? 0);
                    var OpponentScore = game.OpponentScore + (bet.BarrierGuest ?? 0);
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {
                        if (teamScore > OpponentScore)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.BasketballBarrier:
                    var totalScore = game.TeamScore + game.OpponentScore;
                    var barrier = (bet.BarrierHome ?? 0) + (bet.BarrierGuest ?? 0);
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {
                        if (totalScore > barrier)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (totalScore == barrier)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    var gols = game.TeamScore + game.OpponentScore;
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {
                        if (gols <= 1)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (gols <= 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols >= 4)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChild2point5Gols:
                    gols = game.TeamScore + game.OpponentScore;
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {

                        if (gols >= 3)
                        {
                            verboseExactHome.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols < 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                default://FileType.SoccerFather
                    {
                        if (gameType == GameType.Home)
                        {
                            if ((game.Place == GameLocation.HomeGame && game.Status == GameResult.Win))
                            {
                                verboseExactHome.Add(1);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(0);
                            }
                            else if ((game.Place == GameLocation.HomeGame && game.Status == GameResult.Draw))
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(1);
                                verboseExactAway.Add(0);
                            }
                            else if ((game.Place == GameLocation.HomeGame && game.Status == GameResult.Lost))
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(1);
                            }
                        }
                        else if (gameType == GameType.Guest)
                        {
                            if (game.Place == GameLocation.Outside && game.Status == GameResult.Lost)
                            {
                                verboseExactHome.Add(1);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(0);
                            }
                            else if (game.Place == GameLocation.Outside && game.Status == GameResult.Draw)
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(1);
                                verboseExactAway.Add(0);
                            }
                            else if (game.Place == GameLocation.Outside && game.Status == GameResult.Win)
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(1);
                            }
                        }
                        break;
                    }
            }
        }
        internal void CollectLastGames(GameType gameType, List<int> verboseHome, List<int> verboseDraw, List<int> verboseAway, BettingTipsGameInfo game, BettingTipsBet bet, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BasketBallFatherExtraPoints:
                    decimal teamScore;
                    decimal OpponentScore;
                    if (gameType == GameType.Home)
                    {
                        teamScore = game.TeamScore + (bet.BarrierHome ?? 0);
                        OpponentScore = game.OpponentScore + (bet.BarrierGuest ?? 0);
                    }
                    else
                    {
                        teamScore = game.TeamScore + (bet.BarrierGuest ?? 0);
                        OpponentScore = game.OpponentScore + (bet.BarrierHome ?? 0);
                    }
                    if (teamScore > OpponentScore)
                    {
                        verboseHome.Add(gameType == GameType.Home ? 1 : 0);
                        verboseDraw.Add(0);
                        verboseAway.Add(gameType == GameType.Home ? 0 : 1);
                    }
                    else if (teamScore == OpponentScore)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else
                    {
                        verboseHome.Add(gameType == GameType.Home ? 0 : 1);
                        verboseDraw.Add(0);
                        verboseAway.Add(gameType == GameType.Home ? 1 : 0);
                    }
                    break;
                case FileType.BasketballBarrier:
                    var totalScore = game.TeamScore + game.OpponentScore;
                    var barrier = (bet.BarrierHome ?? 0) + (bet.BarrierGuest ?? 0);
                    if (totalScore > barrier)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (totalScore == barrier)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    var gols = game.TeamScore + game.OpponentScore;
                    if (gols <= 1)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (gols <= 3)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols >= 4)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    break;
                case FileType.SoccerChild2point5Gols:
                    gols = game.TeamScore + game.OpponentScore;
                    if (gols >= 3)
                    {
                        verboseHome.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols < 3)
                    {
                        verboseHome.Add(0);
                        verboseAway.Add(1);
                    }
                    break;
                default://FileType.SoccerFather
                    {
                        if ((gameType == GameType.Home && game.Status == GameResult.Win) ||
                            (gameType == GameType.Guest && game.Status == GameResult.Lost))
                        {
                            verboseHome.Add(1);
                            verboseDraw.Add(0);
                            verboseAway.Add(0);
                        }
                        else if ((gameType == GameType.Home && game.Status == GameResult.Draw) ||
                                 (gameType == GameType.Guest && game.Status == GameResult.Draw))
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(1);
                            verboseAway.Add(0);
                        }
                        if ((gameType == GameType.Home && game.Status == GameResult.Lost) ||
                            (gameType == GameType.Guest && game.Status == GameResult.Win))
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(0);
                            verboseAway.Add(1);
                        }
                    }
                    break;
            }
        }

        internal BettingTipsTotalsGamesContainer PrepareHead2Head(string homeTeamName, string guestTeamName, List<BettingTipsHeadToHead> head2Head, BettingTipsBet bet, FileType fileType, int momentum)
        {
            var verboseHome = new List<int>();
            var verboseDraw = new List<int>();
            var verboseAway = new List<int>();
            var verboseExactHome = new List<int>();
            var verboseExactDraw = new List<int>();
            var verboseExactAway = new List<int>();

            foreach (var game in head2Head)
            {
                CollectHead2Head(homeTeamName, guestTeamName, verboseHome, verboseDraw, verboseAway, verboseExactHome, verboseExactDraw, verboseExactAway, game, bet, fileType);
            }
            var totals = new BettingTipsTotalsGamesContainer(momentum,
                verboseHome.ToArray(),
                verboseDraw.ToArray(),
                verboseAway.ToArray(),
                verboseExactHome.ToArray(),
                verboseExactDraw.ToArray(),
                verboseExactAway.ToArray());
            return totals;
        }

        private static void CollectHead2Head(string homeTeamName, string guestTeamName, List<int> verboseHome, List<int> verboseDraw, List<int> verboseAway, List<int> verboseExactHome, List<int> verboseExactDraw, List<int> verboseExactAway, BettingTipsHeadToHead game, BettingTipsBet bet, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BasketBallFatherExtraPoints:
                    decimal teamScore;
                    decimal OpponentScore;
                    if (guestTeamName == game.Home)
                    {
                        teamScore = game.HomeScore + (bet.BarrierGuest ?? 0);
                        OpponentScore = game.GuestScore + (bet.BarrierHome ?? 0);
                        if (teamScore > OpponentScore)
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(0);
                            verboseAway.Add(1);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(1);
                            verboseAway.Add(0);
                        }
                        else
                        {
                            verboseHome.Add(1);
                            verboseDraw.Add(0);
                            verboseAway.Add(0);
                        }
                    }
                    else
                    {
                        teamScore = game.HomeScore + (bet.BarrierHome ?? 0);
                        OpponentScore = game.GuestScore + (bet.BarrierGuest ?? 0);
                        if (teamScore > OpponentScore)
                        {
                            verboseHome.Add(1);
                            verboseDraw.Add(0);
                            verboseAway.Add(0);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(1);
                            verboseAway.Add(0);
                        }
                        else
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(0);
                            verboseAway.Add(1);
                        }
                    }

                    if (game.Home == homeTeamName)
                    {
                        if (teamScore > OpponentScore)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.BasketballBarrier:
                    var totalScore = game.HomeScore + game.GuestScore;
                    var barrier = (bet.BarrierHome ?? 0) + (bet.BarrierGuest ?? 0);
                    if (totalScore > barrier)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (totalScore == barrier)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    if (game.Home == homeTeamName)
                    {
                        if (totalScore > barrier)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (totalScore == barrier)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    var gols = game.HomeScore + game.GuestScore;
                    if (gols <= 1)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (gols <= 3)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols >= 4)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    if (game.Home == homeTeamName)
                    {
                        if (gols <= 1)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (gols <= 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols >= 4)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChild2point5Gols:
                    gols = game.HomeScore + game.GuestScore;
                    if (gols >= 3)
                    {
                        verboseHome.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols < 3)
                    {
                        verboseHome.Add(0);
                        verboseAway.Add(1);
                    }
                    if (game.Home == homeTeamName)
                    {
                        if (gols >= 3)
                        {
                            verboseExactHome.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols < 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                default://FileType.SoccerFather.
                    if (game.HomeScore > game.GuestScore)
                    {
                        verboseHome.Add(game.Home == homeTeamName ? 1 : 0);
                        verboseDraw.Add(0);
                        verboseAway.Add(game.Home == guestTeamName ? 1 : 0);
                    }
                    else if (game.HomeScore < game.GuestScore)
                    {
                        verboseHome.Add(game.Guest == homeTeamName ? 1 : 0);
                        verboseDraw.Add(0);
                        verboseAway.Add(game.Guest == guestTeamName ? 1 : 0);
                    }
                    else
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }

                    if (game.Home == homeTeamName)
                    {
                        if (game.HomeScore > game.GuestScore)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (game.HomeScore == game.GuestScore)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
            }
        }

    }
}
