﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Service;

namespace TotoWinner.Services
{
    public partial class BettingTipsServices : Singleton<BettingTipsServices>, IBettingTipsServices
    {
        #region Fetcher
        public string GetWinnerLineResponse(DateTime wnDate, bool useCach)
        {
            var winnerLineJson = string.Empty;
            if (useCach)
            {
                var filePath = Path.Combine(BettingTipsConstants.RepositoryPath, $"WinnerLine_{wnDate.Day}_{wnDate.Month}_{wnDate.Year}.json");
                if (!File.Exists(filePath))
                {
                    var wnDateStr = wnDate.ToString("yyyy-MM-dd 12:00:00");
                    var winnerLineUrl = $"https://www.bankerim.co.il/api/winnerLine/getDate?date={wnDateStr}";
                    winnerLineJson = ExternalServices.Instance.FetchDataFromUrl(winnerLineUrl);
                    File.WriteAllText(filePath, winnerLineJson);
                }
                else
                {
                    winnerLineJson = File.ReadAllText(filePath);
                }
            }
            else
            {
                var wnDateStr = wnDate.ToString("yyyy-MM-dd 12:00:00");
                var winnerLineUrl = $"https://www.bankerim.co.il/api/winnerLine/getDate?date={wnDateStr}";
                winnerLineJson = ExternalServices.Instance.FetchDataFromUrl(winnerLineUrl);
            }
            return winnerLineJson;
        }

        private string GetEventInfo(int eventId, bool useCach)
        {
            var eventJson = string.Empty;
            if (useCach)
            {

                var filePath = Path.Combine(BettingTipsConstants.RepositoryPath, $"Event_{eventId}.json");
                if (!File.Exists(filePath))
                {
                    var eventInfoUrl = $"https://www.bankerim.co.il/api/winnerLine/getEventInfo?eventId={eventId}";
                    eventJson = ExternalServices.Instance.FetchDataFromUrl(eventInfoUrl);
                    File.WriteAllText(filePath, eventJson);
                }
                else
                {
                    eventJson = File.ReadAllText(filePath);
                }
            }
            else
            {
                var eventInfoUrl = $"https://www.bankerim.co.il/api/winnerLine/getEventInfo?eventId={eventId}";
                eventJson = ExternalServices.Instance.FetchDataFromUrl(eventInfoUrl);
            }
            return eventJson;
        }
        #endregion
    }
}
