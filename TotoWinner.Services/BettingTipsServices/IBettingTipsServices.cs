﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;
using TotoWinner.Data.BettingTips;

namespace TotoWinner.Service
{
    public interface IBettingTipsServices
    {
        string GetWinnerLineResponse(DateTime wnDate, bool useCach);
        BettingTipsEventsRepository ParseWinnerLineResponse(dynamic dWinnerLineResponse, long roundIdInput, DateTime winnerLineDate, bool useCash, List<SportIds> sportsToParse);
        BettingTipsStacks CollectData(BettingTipsEventsRepository repository, int momentum);
        void PersistProgramAndTips(BettingTipsStacks stackes, BettingTipsEventsRepository infoRepository);
        //List<Tip> GetAllTips();
        void AddManualTip(long programId, 
            SportIds sport, 
            int gameIndex, 
            string country,
            string league,
            DateTime gameStart, 
            int powertip,
            bool isSingle, 
            char tipMark, 
            string tipContent, 
            string tipUrl, 
            decimal betRate, 
            string betName, 
            bool isFather, 
            int tipperId);

        List<Tip> GetAllTipsWithNoResults();
        void CollectTipsAndUpdateResults(List<Tip> tipsToUpdate);
        void SetCloseStatusForCandidateTips();
        void FinishTipGeneration();
        void SetPrograms(List<long> programIds, DateTime programsDate);
        Tip GetNextActiveTips(BettingTipsFilter filter, List<int> SubscriberAlreadySentTipIds, List<Tip> currentStackTips = null);
        Dictionary<int, int> GetTipsCounters(List<Tip> tips);
    }
}
