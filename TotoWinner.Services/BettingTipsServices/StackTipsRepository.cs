﻿using System.Collections.Generic;
using TotoWinner.Common;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace TotoWinner.Services
{
    public class StackTipsRepository
    {
        public ICacheProvider Cashe { get; set; }
        private int CacheExpirationInMin;
        public StackTipsRepository(int cacheExpirationInMin = 5) : this(new CacheProvider())
        {
            this.CacheExpirationInMin = cacheExpirationInMin;
        }
        private StackTipsRepository(ICacheProvider casheProvider)
        {
            this.Cashe = casheProvider;
        }

        public List<Data.Tip> GetActiveStackTips()
        {
            var tips = this.Cashe.Get("ActiveStackTips") as List<Data.Tip>;
            if (tips == null)
            {
                tips = BettingTipsServices.Instance.GetAllActiveStackTips();
                this.Cashe.Set("ActiveStackTips", tips, this.CacheExpirationInMin);
            }
            return tips;
        }
        public void ClearCache()
        {
            this.Cashe.Invalidate("ActiveStackTips");
        }
    }
}
