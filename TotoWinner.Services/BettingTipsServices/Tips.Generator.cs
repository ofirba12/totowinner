﻿using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BettingTips;
using TotoWinner.Service;

namespace TotoWinner.Services
{
    public partial class BettingTipsServices : Singleton<BettingTipsServices>, IBettingTipsServices
    {
        private BettingTipsServices() { }

        public void SetPrograms(List<long> programIds, DateTime programsDate)
        {
            foreach (var programId in programIds)
                _btPersistanceSrv.AddProgram(programId, programsDate);
        }

        public List<TipProgram> GetAllPrograms()
        {
            var programs = _btPersistanceSrv.GetAllPrograms()
                .Select(p => new TipProgram(p.ProgramId, p.ProgramDate))
                .ToList();
            return programs;
        }

        private BuildTipRequest GenerateBuildTipRequest(BettingTipsParentEvent eventData, BettingTipsTotalsRepository totals, FileType fileType, int momentum, BetType betType)
        {
            try
            {
                var hebrewTime = BettingTipsServicesHelpers.HebrewTranslate(eventData.Time);
                var sport = BettingTipsServicesHelpers.HebrewTranslate(eventData.SportId);
                var day = hebrewTime.Item1;
                var time = hebrewTime.Item2;
                var homeTeam = eventData.HomeTeamName;
                var guestTeam = eventData.GuestTeamName;
                var league = eventData.LeagueName;
                var gameIndex = eventData.Place;

                var indexType = GetIndexType(totals, betType);

                int totalGames = 0, verboseTotal = 0, verboseMomentumTotal = 0;
                decimal winPercent = 0, momentumWinPercent = 0;
                if (indexType.HasValue)
                    GetIndexTypeData(indexType.Value, totals, ref totalGames,
                        ref verboseTotal,
                        ref verboseMomentumTotal,
                        ref winPercent,
                        ref momentumWinPercent);
                var extraPointString = FetchExtraPoints(eventData, fileType, indexType);
                var request = new BuildTipRequest(eventData,
                    eventData.SportId,
                    eventData.Time,
                    gameIndex,
                    eventData.TotoId,
                    homeTeam,
                    guestTeam,
                    league,
                    totalGames,
                    winPercent,
                    momentumWinPercent,
                    verboseTotal,
                    verboseMomentumTotal,
                    momentum,
                    extraPointString,
                    indexType,
                    betType,
                    fileType);

                return request;
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occured trying to generate tip request for game [{eventData.Place}] [{eventData.HomeTeamName}-{eventData.GuestTeamName}] league {eventData.LeagueName}", ex);
            }
        }

        private string FetchExtraPoints(BettingTipsParentEvent eventData, FileType fileType, IndexType? indexType)
        {
            var extraPointStr = "לא נמצא";
            if (fileType == FileType.BasketBallFatherExtraPoints)
            {
                if (indexType.HasValue && 
                   (eventData.Bet.BarrierHome.HasValue && indexType.HasValue || eventData.Bet.BarrierGuest.HasValue))
                {
                    var barrier = eventData.Bet.BarrierHome.HasValue
                        ? eventData.Bet.BarrierHome.Value
                        : eventData.Bet.BarrierGuest.Value;
                    var homePlusSign = eventData.Bet.BarrierHome.HasValue;
                    switch (indexType.Value)
                    {
                        case IndexType.HomeLastHome:
                        case IndexType.HomeLastExactHome:
                        case IndexType.Head2HeadHome:
                        case IndexType.Head2HeadExactHome:
                        case IndexType.GuestLastAway:
                        case IndexType.GuestLastExactAway:
                            var sign = homePlusSign
                                ? "+"
                                : "-";
                            extraPointStr = $"({barrier}{sign})";
                            break;
                        case IndexType.GuestLastHome:
                        case IndexType.GuestLastExactHome:
                        case IndexType.HomeLastAway:
                        case IndexType.HomeLastExactAway:
                        case IndexType.Head2HeadAway:
                        case IndexType.Head2HeadExactAway:
                            sign = homePlusSign
                                ? "-"
                                : "+";
                            extraPointStr = $"({barrier}{sign})";
                            break;
                    }
                }
            }
            else if (fileType == FileType.BasketballBarrier)
            {
                extraPointStr = eventData.Bet.BarrierHome.HasValue
                    ? $"{eventData.Bet.BarrierHome.Value}"
                    : $"{eventData.Bet.BarrierGuest.Value}";
            }
            return extraPointStr;
        }

        private void GetIndexTypeData(IndexType indexType, BettingTipsTotalsRepository totals,
            ref int totalGames, ref int verboseTotal, ref int verboseMomentumTotal, ref decimal winPercent, ref decimal momentumWinPercent)
        {
            switch (indexType)
            {
                case IndexType.HomeLastHome:
                    totalGames = totals.HomeLastGameTotals.VerboseHome.Count();
                    winPercent = totals.HomeLastGameTotals.HomeWinPercent;
                    momentumWinPercent = totals.HomeLastGameTotals.HomeMomentumWinPercent;
                    verboseTotal = totals.HomeLastGameTotals.Home;
                    verboseMomentumTotal = totals.HomeLastGameTotals.HomeMomentum;
                    break;
                case IndexType.HomeLastDraw:
                    totalGames = totals.HomeLastGameTotals.VerboseDraw.Count();
                    winPercent = totals.HomeLastGameTotals.DrawWinPercent;
                    momentumWinPercent = totals.HomeLastGameTotals.DrawMomentumWinPercent;
                    verboseTotal = totals.HomeLastGameTotals.Draw;
                    verboseMomentumTotal = totals.HomeLastGameTotals.DrawMomentum;
                    break;
                case IndexType.HomeLastAway:
                    totalGames = totals.HomeLastGameTotals.VerboseAway.Count();
                    winPercent = totals.HomeLastGameTotals.AwayWinPercent;
                    momentumWinPercent = totals.HomeLastGameTotals.AwayMomentumWinPercent;
                    verboseTotal = totals.HomeLastGameTotals.Away;
                    verboseMomentumTotal = totals.HomeLastGameTotals.AwayMomentum;
                    break;

                case IndexType.GuestLastHome:
                    totalGames = totals.GuestLastGameTotals.VerboseHome.Count();
                    winPercent = totals.GuestLastGameTotals.HomeWinPercent;
                    momentumWinPercent = totals.GuestLastGameTotals.HomeMomentumWinPercent;
                    verboseTotal = totals.GuestLastGameTotals.Home;
                    verboseMomentumTotal = totals.GuestLastGameTotals.HomeMomentum;
                    break;
                case IndexType.GuestLastDraw:
                    totalGames = totals.GuestLastGameTotals.VerboseDraw.Count();
                    winPercent = totals.GuestLastGameTotals.DrawWinPercent;
                    momentumWinPercent = totals.GuestLastGameTotals.DrawMomentumWinPercent;
                    verboseTotal = totals.GuestLastGameTotals.Draw;
                    verboseMomentumTotal = totals.GuestLastGameTotals.DrawMomentum;
                    break;
                case IndexType.GuestLastAway:
                    totalGames = totals.GuestLastGameTotals.VerboseAway.Count();
                    winPercent = totals.GuestLastGameTotals.AwayWinPercent;
                    momentumWinPercent = totals.GuestLastGameTotals.AwayMomentumWinPercent;
                    verboseTotal = totals.GuestLastGameTotals.Away;
                    verboseMomentumTotal = totals.GuestLastGameTotals.AwayMomentum;
                    break;

                case IndexType.HomeLastExactHome:
                    totalGames = totals.HomeLastGameTotals.VerboseExactHome.Count();
                    winPercent = totals.HomeLastGameTotals.HomeExactWinPercent;
                    momentumWinPercent = totals.HomeLastGameTotals.HomeExactMomentumWinPercent;
                    verboseTotal = totals.HomeLastGameTotals.HomeExact;
                    verboseMomentumTotal = totals.HomeLastGameTotals.HomeExactMomentum;
                    break;
                case IndexType.HomeLastExactDraw:
                    totalGames = totals.HomeLastGameTotals.VerboseExactDraw.Count();
                    winPercent = totals.HomeLastGameTotals.DrawExactWinPercent;
                    momentumWinPercent = totals.HomeLastGameTotals.DrawExactMomentumWinPercent;
                    verboseTotal = totals.HomeLastGameTotals.DrawExact;
                    verboseMomentumTotal = totals.HomeLastGameTotals.DrawExactMomentum;
                    break;
                case IndexType.HomeLastExactAway:
                    totalGames = totals.HomeLastGameTotals.VerboseExactAway.Count();
                    winPercent = totals.HomeLastGameTotals.AwayExactWinPercent;
                    momentumWinPercent = totals.HomeLastGameTotals.AwayExactMomentumWinPercent;
                    verboseTotal = totals.HomeLastGameTotals.AwayExact;
                    verboseMomentumTotal = totals.HomeLastGameTotals.AwayExactMomentum;
                    break;

                case IndexType.GuestLastExactHome:
                    totalGames = totals.GuestLastGameTotals.VerboseExactHome.Count();
                    winPercent = totals.GuestLastGameTotals.HomeExactWinPercent;
                    momentumWinPercent = totals.GuestLastGameTotals.HomeExactMomentumWinPercent;
                    verboseTotal = totals.GuestLastGameTotals.HomeExact;
                    verboseMomentumTotal = totals.GuestLastGameTotals.HomeExactMomentum;
                    break;
                case IndexType.GuestLastExactDraw:
                    totalGames = totals.GuestLastGameTotals.VerboseExactDraw.Count();
                    winPercent = totals.GuestLastGameTotals.DrawExactWinPercent;
                    momentumWinPercent = totals.GuestLastGameTotals.DrawExactMomentumWinPercent;
                    verboseTotal = totals.GuestLastGameTotals.DrawExact;
                    verboseMomentumTotal = totals.GuestLastGameTotals.DrawExactMomentum;
                    break;
                case IndexType.GuestLastExactAway:
                    totalGames = totals.GuestLastGameTotals.VerboseExactAway.Count();
                    winPercent = totals.GuestLastGameTotals.AwayExactWinPercent;
                    momentumWinPercent = totals.GuestLastGameTotals.AwayExactMomentumWinPercent;
                    verboseTotal = totals.GuestLastGameTotals.AwayExact;
                    verboseMomentumTotal = totals.GuestLastGameTotals.AwayExactMomentum;
                    break;

                case IndexType.Head2HeadHome:
                    totalGames = totals.Head2HeadTotals.VerboseHome.Count();
                    winPercent = totals.Head2HeadTotals.HomeWinPercent;
                    momentumWinPercent = totals.Head2HeadTotals.HomeMomentumWinPercent;
                    verboseTotal = totals.Head2HeadTotals.Home;
                    verboseMomentumTotal = totals.Head2HeadTotals.HomeMomentum;
                    break;
                case IndexType.Head2HeadDraw:
                    totalGames = totals.Head2HeadTotals.VerboseDraw.Count();
                    winPercent = totals.Head2HeadTotals.DrawWinPercent;
                    momentumWinPercent = totals.Head2HeadTotals.DrawMomentumWinPercent;
                    verboseTotal = totals.Head2HeadTotals.Draw;
                    verboseMomentumTotal = totals.Head2HeadTotals.DrawMomentum;
                    break;
                case IndexType.Head2HeadAway:
                    totalGames = totals.Head2HeadTotals.VerboseAway.Count();
                    winPercent = totals.Head2HeadTotals.AwayWinPercent;
                    momentumWinPercent = totals.Head2HeadTotals.AwayMomentumWinPercent;
                    verboseTotal = totals.Head2HeadTotals.Away;
                    verboseMomentumTotal = totals.Head2HeadTotals.AwayMomentum;
                    break;

                case IndexType.Head2HeadExactHome:
                    totalGames = totals.Head2HeadTotals.VerboseExactHome.Count();
                    winPercent = totals.Head2HeadTotals.HomeExactWinPercent;
                    momentumWinPercent = totals.Head2HeadTotals.HomeExactMomentumWinPercent;
                    verboseTotal = totals.Head2HeadTotals.HomeExact;
                    verboseMomentumTotal = totals.Head2HeadTotals.HomeExactMomentum;
                    break;
                case IndexType.Head2HeadExactDraw:
                    totalGames = totals.Head2HeadTotals.VerboseExactDraw.Count();
                    winPercent = totals.Head2HeadTotals.DrawExactWinPercent;
                    momentumWinPercent = totals.Head2HeadTotals.DrawExactMomentumWinPercent;
                    verboseTotal = totals.Head2HeadTotals.DrawExact;
                    verboseMomentumTotal = totals.Head2HeadTotals.DrawExactMomentum;
                    break;
                case IndexType.Head2HeadExactAway:
                    totalGames = totals.Head2HeadTotals.VerboseExactAway.Count();
                    winPercent = totals.Head2HeadTotals.AwayExactWinPercent;
                    momentumWinPercent = totals.Head2HeadTotals.AwayExactMomentumWinPercent;
                    verboseTotal = totals.Head2HeadTotals.AwayExact;
                    verboseMomentumTotal = totals.Head2HeadTotals.AwayExactMomentum;
                    break;
            }
        }

        private IndexType? GetIndexType(BettingTipsTotalsRepository totals, BetType betType)
        {
            var collection = new List<BettingTipTotalColumn>();
            switch (betType)
            {
                case BetType.Home:
                    if (totals.HomeLastGameTotals.VerboseHome.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.HomeLastHome, totals.HomeLastGameTotals.HomeWinPercent, totals.HomeLastGameTotals.HomeMomentumWinPercent));
                    if (totals.GuestLastGameTotals.VerboseHome.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.GuestLastHome, totals.GuestLastGameTotals.HomeWinPercent, totals.GuestLastGameTotals.HomeMomentumWinPercent));
                    if (totals.HomeLastGameTotals.VerboseExactHome.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.HomeLastExactHome, totals.HomeLastGameTotals.HomeExactWinPercent, totals.HomeLastGameTotals.HomeExactMomentumWinPercent));
                    if (totals.GuestLastGameTotals.VerboseExactHome.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.GuestLastExactHome, totals.GuestLastGameTotals.HomeExactWinPercent, totals.GuestLastGameTotals.HomeExactMomentumWinPercent));
                    if (totals.Head2HeadTotals.VerboseHome.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.Head2HeadHome, totals.Head2HeadTotals.HomeWinPercent, totals.Head2HeadTotals.HomeMomentumWinPercent));
                    if (totals.Head2HeadTotals.VerboseExactHome.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.Head2HeadExactHome, totals.Head2HeadTotals.HomeExactWinPercent, totals.Head2HeadTotals.HomeExactMomentumWinPercent));
                    break;
                case BetType.Draw:
                    if (totals.HomeLastGameTotals.VerboseDraw.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.HomeLastDraw, totals.HomeLastGameTotals.DrawWinPercent, totals.HomeLastGameTotals.DrawMomentumWinPercent));
                    if (totals.GuestLastGameTotals.VerboseDraw.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.GuestLastDraw, totals.GuestLastGameTotals.DrawWinPercent, totals.GuestLastGameTotals.DrawMomentumWinPercent));
                    if (totals.HomeLastGameTotals.VerboseExactDraw.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.HomeLastExactDraw, totals.HomeLastGameTotals.DrawExactWinPercent, totals.HomeLastGameTotals.DrawExactMomentumWinPercent));
                    if (totals.GuestLastGameTotals.VerboseExactDraw.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.GuestLastExactDraw, totals.GuestLastGameTotals.DrawExactWinPercent, totals.GuestLastGameTotals.DrawExactMomentumWinPercent));
                    if (totals.Head2HeadTotals.VerboseDraw.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.Head2HeadDraw, totals.Head2HeadTotals.DrawWinPercent, totals.Head2HeadTotals.DrawMomentumWinPercent));
                    if (totals.Head2HeadTotals.VerboseExactDraw.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.Head2HeadExactDraw, totals.Head2HeadTotals.DrawExactWinPercent, totals.Head2HeadTotals.DrawExactMomentumWinPercent));
                    break;
                case BetType.Away:
                    if (totals.HomeLastGameTotals.VerboseAway.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.HomeLastAway, totals.HomeLastGameTotals.AwayWinPercent, totals.HomeLastGameTotals.AwayMomentumWinPercent));
                    if (totals.GuestLastGameTotals.VerboseAway.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.GuestLastAway, totals.GuestLastGameTotals.AwayWinPercent, totals.GuestLastGameTotals.AwayMomentumWinPercent));
                    if (totals.HomeLastGameTotals.VerboseExactAway.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.HomeLastExactAway, totals.HomeLastGameTotals.AwayExactWinPercent, totals.HomeLastGameTotals.AwayExactMomentumWinPercent));
                    if (totals.GuestLastGameTotals.VerboseExactAway.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.GuestLastExactAway, totals.GuestLastGameTotals.AwayExactWinPercent, totals.GuestLastGameTotals.AwayExactMomentumWinPercent));
                    if (totals.Head2HeadTotals.VerboseAway.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.Head2HeadAway, totals.Head2HeadTotals.AwayWinPercent, totals.Head2HeadTotals.AwayMomentumWinPercent));
                    if (totals.Head2HeadTotals.VerboseExactAway.Count() > 2)
                        collection.Add(new BettingTipTotalColumn(IndexType.Head2HeadExactAway, totals.Head2HeadTotals.AwayExactWinPercent, totals.Head2HeadTotals.AwayExactMomentumWinPercent));
                    break;
            }
            var validColumnFound = true;
            IndexType? selectedIndex = null;
            do
            {
                BettingTipTotalColumn selected = null;
                var have100 = collection.Where(i => i.WinPercent == 100 || i.WinMomentumPercent == 100);
                if (have100.Count() > 1)
                    selected = have100.Aggregate((cur, x) => (x.WinPercent + x.WinMomentumPercent) < (cur.WinPercent + cur.WinMomentumPercent)
                    ? cur
                    : x);
                if (have100.Count() == 1)
                    selected = have100.First();
                else
                {
                    if (collection.All(i => i.WinPercent == 0 && i.WinMomentumPercent == 0))
                        selected = collection.FirstOrDefault();
                    else
                        selected = collection.Aggregate((cur, x) => (x.WinPercent + x.WinMomentumPercent) < (cur.WinPercent + cur.WinMomentumPercent)
                        ? cur
                        : x);
                }

                if (selected != null)
                    selectedIndex = selected.IndexType;

                int totalGames = 0, verboseTotal = 0, verboseMomentumTotal = 0;
                decimal winPercent = 0, momentumWinPercent = 0;

                if (selectedIndex.HasValue)
                    GetIndexTypeData(selectedIndex.Value, totals, ref totalGames,
                        ref verboseTotal,
                        ref verboseMomentumTotal,
                        ref winPercent,
                        ref momentumWinPercent);

                if ((winPercent < 50 || momentumWinPercent < 75) && collection.Count() > 0)
                {
                    collection.Remove(selected);
                    validColumnFound = false;
                }
                else
                    validColumnFound = true;
            }
            while (validColumnFound == false);

            return selectedIndex;
        }

        private BettingTip BuildTip(BuildTipRequest request)
        {
            var contentTip = string.Empty;
            decimal? barrier = null;
            if (request.MomentumWinPercent >= 75)
            {
                if (request.FileType == FileType.BasketballBarrier)
                {
                    barrier = request.EventData.Bet.BarrierHome ?? request.EventData.Bet.BarrierGuest;
                    if (barrier.HasValue && barrier.Value >= 100)
                        contentTip = BuildContentTip(request);
                    else
                        contentTip = string.Empty;
                }
                else
                    contentTip = BuildContentTip(request);
            }

            var mark = string.Empty;
            switch (request.BetType)
            {
                case BetType.Home:
                    mark = "1";
                    break;
                case BetType.Draw:
                    mark = "X";
                    break;
                case BetType.Away:
                    mark = "2";
                    break;
            }
            var markingTip = BuildTipAutomaticMarking(request.GameIndex, 
                mark.First(), 
                request.BetType, 
                request.FileType, 
                request.HomeTeam, 
                request.GuestTeam,
                barrier);
            //var url = $"https://www.winner.co.il/mainbook/events/{request.TotoId}?utm_source=bankerim&utm_medium=in_game_menu";
            //https://www.winner.co.il/mainbook/events/4817596?utm_source=bankerim&utm_medium=in_game_menu
            //var url = $"https://www.winner.co.il/mainbook/events/{request.TotoId}?utm_source=bankerim&utm_medium=in_game_menu";
            //https://m.winner.co.il/aff/e/4817483?Referrer=bankerim&ReferUrl=homebutton&campaign=General
            //var url = $"https://m.winner.co.il/aff/e/{request.TotoId}?Referrer=bankerim&ReferUrl=homebutton&campaign=General";
            var url = $"https://www.winner.co.il/aff/e/{request.TotoId}?Referrer=bankerim&ReferUrl=homebutton&campaign=General";
            var urlPrefixTip = $"למעבר למשלוח טיפ וקבלת טיפ נוסף >>> ";

            var timeTip = BuildTipTime(request.Sport, request.GameStart);
            var bettingTip = new BettingTip(timeTip, contentTip, markingTip, urlPrefixTip, url);
            return bettingTip;
        }
        private string BuildTipManualMarking(int gameIndex, char mark)
        {
            var markingTip = $"משחק מספר {gameIndex} בטופס הווינר סימון {mark}";
            return markingTip;
        }
        private string BuildTipAutomaticMarking(int gameIndex, char mark, BetType betType, FileType fileType, string homeTeam, string guestTeam, decimal? barrier)
        {
            var ourTip = string.Empty;
            switch (fileType)
            {
                case FileType.SoccerFather:
                    switch(betType)
                    {
                        case BetType.Home:
                            ourTip = $"הטיפ שלנו: {homeTeam} מנצחת.";
                            break;
                        case BetType.Draw:
                            ourTip = $"הטיפ שלנו:משחק יסתיים ללא הכרעה, תיקו";
                            break;
                        case BetType.Away:
                            ourTip = $"הטיפ שלנו: {guestTeam} מנצחת.";
                            break;
                    }
                    break;
                case FileType.BasketBallFatherExtraPoints:
                    switch (betType)
                    {
                        case BetType.Home:
                            ourTip = $"הטיפ שלנו: {homeTeam} עומדת בליין שנקבע.";
                            break;
                        case BetType.Away:
                            ourTip = $"הטיפ שלנו: {guestTeam} עומדת בליין שנקבע.";
                            break;
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    switch (betType)
                    {
                        case BetType.Home:
                            ourTip = $"הטיפ שלנו: משחק יסתיים עם מקסימום שער בודד";
                            break;
                        case BetType.Draw:
                            ourTip = $"הטיפ שלנו: משחק יסתיים בטווח של 2 עד 3 שערים";
                            break;
                        case BetType.Away:
                            ourTip = $"הטיפ שלנו: משחק יסתיים עם מינמום ארבעה שערים למשחק";
                            break;
                    }
                    break;
                case FileType.SoccerChild2point5Gols:
                    switch (betType)
                    {
                        case BetType.Home:
                            ourTip = $"הטיפ שלנו: משחק יסתיים עם מינימום שלושה שערים למשחק";
                            break;
                        case BetType.Away:
                            ourTip = $"הטיפ שלנו: משחק יסתיים עם מקסימום שני שערים";
                            break;
                    }
                    break;
                case FileType.BasketballBarrier:
                    switch (betType)
                    {
                        case BetType.Home:
                            ourTip = $"הטיפ שלנו: משחק יסתיים מעל {barrier} נקודות";
                            break;
                        case BetType.Away:
                            ourTip = $"הטיפ שלנו: משחק יסתיים מתחת  {barrier} נקודות";
                            break;
                    }
                    break;
            }
            var markingTip = $"משחק מספר {gameIndex} בטופס הווינר סימון {mark}";
            var fullMarkTip = $"{ourTip}{Environment.NewLine}{markingTip}";
            return fullMarkTip;
        }
        private string BuildTipTime(SportIds sportType, DateTime gameStart)
        {
            var hebrewTime = BettingTipsServicesHelpers.HebrewTranslate(gameStart);
            var sport = BettingTipsServicesHelpers.HebrewTranslate(sportType);
            var day = hebrewTime.Item1;
            var time = hebrewTime.Item2;

            var timeTip = $"הטיפ להימור {sport}{Environment.NewLine}{day} שעה {time}";

            return timeTip;
        }
        private string BuildContentTip(BuildTipRequest request)
        {
            var contentTip = string.Empty;
            var showOnly = 0; //0 all
            switch (request.IndexType)
            {
                case IndexType.HomeLastHome:
                case IndexType.HomeLastDraw:
                case IndexType.HomeLastAway:
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Home && (showOnly == 0 || showOnly == 1))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"המארחת {request.HomeTeam} עם {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה. במשחקיה האחרונים היא במומנטום לא רע כשהיא עם {request.VerboseMomentumTotal} נצחונות ב-{request.Momentum} משחקים אחרונים.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"המארחת {request.HomeTeam} אחת הקבוצות החזקות נמצאת בכושר משחק מעולה כשהיא עם לא פחות מאשר {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה כאשר היא גם שומרת על מאזן מרשים ב-{request.Momentum} המשחקים האחרונים.";
                    }
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 2))
                        contentTip = $"המארחת {request.HomeTeam} מרבה לסיים בתוצאות תיקו; כאשר רק לאחרונה מתוך {request.Momentum} משחקים היא מסיימת {request.VerboseMomentumTotal} משחקים בתוצאת תיקו.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Away && (showOnly == 0 || showOnly == 3))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"המארחת {request.HomeTeam} עם {request.VerboseTotal} הפסדים ב-{request.TotalGames} משחקים אחרונים שלה; נמצאת גם במומנטום של הפסדים לאחרונה כשהיא עם {request.VerboseMomentumTotal} הפסדים ב-{request.Momentum} משחקים אחרונים.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"המארחת {request.HomeTeam} אחת הקבוצות החלשות בתקופה האחרונה; כאשר היא עם לא פחות מאשר {request.VerboseTotal} הפסדים ב-{request.TotalGames} משחקים אחרונים שלה.";
                    }
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 4))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם לפחות 3 שערים למשחק כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם 3 שערים לפחות.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 5))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם מקסימום 2 שערים למשחק כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם מקסימום 2 שערים למשחק.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 6))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם מקסימום שער אחד; במשחקיה האחרונים היא עם {request.VerboseMomentumTotal} משחקים עם מקסימום שער בודד במשחק מתוך {request.Momentum} משחקים אחרונים שלה.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 7))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בטווח של 2-3 שערים; במשחקיה האחרונים היא עם {request.VerboseMomentumTotal} משחקים עם טווח של 2-3 שערים במשחק מתוך {request.Momentum} משחקים אחרונים שלה.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 8))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם מינימום 4 שערים; במשחקיה האחרונים היא עם {request.VerboseMomentumTotal} משחקים עם מינימום 4 שערים במשחק מתוך {request.Momentum} משחקים אחרונים שלה.";

                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    //contentTip = $"המארחת {request.HomeTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} המארחת {request.HomeTeam} פייבוריטית לעמוד בליין במשחק זה";
                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    //contentTip = $"האורחת {request.GuestTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} האורחת {request.GuestTeam} פייבוריטית לעמוד בליין במשחק זה";

                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"המארחת {request.HomeTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה; מסיימת מעל {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מעל רף נקודות זה.";
                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"המארחת {request.HomeTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה; מסיימת מתחת {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מתחת רף נקודות זה.";

                    break;
                case IndexType.GuestLastHome:
                case IndexType.GuestLastDraw:
                case IndexType.GuestLastAway:
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Home && (showOnly == 0 || showOnly == 1))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"האורחת {request.GuestTeam} עם {request.VerboseTotal} הפסדים ב-{request.TotalGames} משחקים אחרונים שלה; נמצאת גם במומנטום של הפסדים לאחרונה כשהיא עם {request.VerboseMomentumTotal} הפסדים ב-{request.Momentum} משחקים אחרונים.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"האורחת {request.GuestTeam} אחת הקבוצות החלשות בליגה במומנטום של הפסדים כאשר היא עם לא פחות מאשר {request.VerboseTotal} הפסדים ב-{request.TotalGames} משחקים אחרונים שלה.";
                    }
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 2))
                        contentTip = $"האורחת {request.GuestTeam} מרבה לסיים בתוצאות תיקו; כאשר רק לאחרונה מתוך {request.Momentum} משחקים היא מסיימת {request.VerboseMomentumTotal} משחקים בתוצאת תיקו.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Away && (showOnly == 0 || showOnly == 3))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"הקבוצה האורחת {request.GuestTeam} עם {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה; נמצאת במשחקים האחרונים במומנטום לא רע כשהיא עם {request.VerboseMomentumTotal} נצחונות ב-{request.Momentum} משחקים אחרונים.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"הקבוצה האורחת {request.GuestTeam} אחת הקבוצות החזקות בתקופה האחרונה; נמצאת בכושר משחק שיא כשהיא עם לא פחות מאשר {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה כאשר היא שומרת על מאזן מרשים גם ב-{request.Momentum} המשחקים האחרונים שלה.";
                    }
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 4))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם לפחות 3 שערים למשחק כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם 3 שערים לפחות.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 5))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם מקסימום 2 שערים למשחק כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם 2 שערים מקסימום למשחק.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 6))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתןך {request.TotalGames} משחקים אחרונים שלה עם מקסימום שער אחד; במשחקיה האחרונים היא עם {request.VerboseMomentumTotal} משחקים עם מקסימום שער בודד במשחק מתוך {request.Momentum} משחקים אחרונים שלה.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 7))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בטווח של 2-3 שערים; במשחקיה האחרונים היא עם {request.VerboseMomentumTotal} משחקים עם טווח של 2-3 שערים במשחק מתוך {request.Momentum} משחקים אחרונים שלה.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 8))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה עם מינימום 4 שערים; במשחקיה האחרונים היא עם {request.VerboseMomentumTotal} משחקים עם מינימום 4 שערים במשחק מתוך {request.Momentum} משחקים אחרונים שלה.";

                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"המארחת {request.HomeTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"האורחת {request.GuestTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";
                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"האורחת {request.GuestTeam} פייבוריטית לעמוד בליין במשחק זה";
                    //    //contentTip = $"המארחת {request.HomeTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא לא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא לא עומדת בליין.";                   

                    if (request.FileType == FileType.BasketballBarrier && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"האורחת {request.GuestTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה; מסיימת מעל {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מעל רף נקודות זה.";
                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"האורחת {request.GuestTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה; מסיימת מתחת {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מתחת רף נקודות זה.";
                    break;
                case IndexType.HomeLastExactHome:
                case IndexType.HomeLastExactDraw:
                case IndexType.HomeLastExactAway:
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Home && (showOnly == 0 || showOnly == 1))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"המארחת {request.HomeTeam} עם {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים כאשר היא היתה המארחת; קבוצה מאוד יציבה בבית.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"המארחת {request.HomeTeam} אחת הקבוצות החזקות כשהיא משחקת בבית; המאזן שלה הוא {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה כאשר היא שומרת על מאזן מרשים של {request.VerboseMomentumTotal} נצחונות במשחקים האחרונים שלה בבית.";
                    }
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 2))
                        contentTip = $"המארחת {request.HomeTeam} מרבה לסיים משחקים ללא הכרעה כשהיא משחקת בבית; כאשר רק לאחרונה מתוך {request.Momentum} משחקים ששיחקה בבית היא מסיימת {request.VerboseMomentumTotal} משחקים בתוצאת תיקו.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Away && (showOnly == 0 || showOnly == 3))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"המארחת {request.HomeTeam} עם {request.VerboseTotal} הפסדים ב-{request.TotalGames} משחקים אחרונים שלה כאשר היא משחקת בבית; נמצאת גם במומנטום של הפסדים לאחרונה כשהיא עם {request.VerboseMomentumTotal} הפסדים ב-{request.Momentum} משחקי הבית האחרונים שלה.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"המארחת {request.HomeTeam} עם לא פחות מאשר {request.VerboseTotal} הפסדים ב-{request.TotalGames} משחקים אחרונים שלה במשחקי בית.";
                    }
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 4))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית עם לפחות 3 שערים למשחק; כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם 3 שערים לפחות.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 5))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית; עם מקסימום 2 שערים למשחק כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם מקסימום 2 שערים למשחק.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 6))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית עם מקסימום שער בודד למשחק; במשחקיה האחרונים שלה בבית היא עם {request.VerboseMomentumTotal} משחקים עם שער בודד מקסימום למשחק מתוך {request.Momentum} משחקים אחרונים שלה בבית.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 7))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית עם טווח של 2-3 שערים למשחק; במשחקיה האחרונים שלה בבית היא עם {request.VerboseMomentumTotal} משחקים עם טווח של 2-3 שערים למשחק מתוך {request.Momentum} משחקים אחרונים שלה בבית.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 8))
                        contentTip = $"המארחת {request.HomeTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית עם מינימום 4 שערים למשחק; במשחקיה האחרונים שלה בבית היא עם {request.VerboseMomentumTotal} משחקים עם מינימום 4ה שערים למשחק מתוך {request.Momentum} משחקים אחרונים שלה בבית.";

                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} המארחת {request.HomeTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////                        contentTip = $"המארחת {request.HomeTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";
                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} האורחת {request.GuestTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////                        contentTip = $"האורחת {request.GuestTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";

                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"המארחת {request.HomeTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית מסיימת מעל {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מעל רף נקודות זה.";
                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"המארחת {request.HomeTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בבית מסיימת מתחת {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מתחת רף נקודות זה.";
                    break;
                case IndexType.GuestLastExactHome:
                case IndexType.GuestLastExactDraw:
                case IndexType.GuestLastExactAway:
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Home && (showOnly == 0 || showOnly == 1))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"האורחת {request.GuestTeam} עם {request.VerboseTotal} הפסדים ב- {request.TotalGames} משחקי חוץ אחרונים שלה; נמצאת גם במומנטום של הפסדים לאחרונה כשהיא עם {request.VerboseMomentumTotal} הפסדים ב-{request.Momentum} משחקי החוץ שלה.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"האורחת {request.GuestTeam} אחת הקבוצות החלשות במשחקי חוץ; כאשר היא עם מאזן לא מחמיא של רצף הפסדים ב-{request.Momentum} משחקים האחרונים שלה כאשר היא משחקת בחוץ.";
                    }
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 2))
                        contentTip = $"האורחת {request.GuestTeam} מרבה לסיים משחקים שהיא משחקת בחוץ בתיקו; כאשר רק לאחרונה מתוך {request.Momentum} משחקים ששיחקה בחוץ היא מסיימת {request.VerboseMomentumTotal} משחקים בתוצאת תיקו.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Away && (showOnly == 0 || showOnly == 3))
                    {
                        if (request.WinPercent >= 50 && request.WinPercent < 80)
                            contentTip = $"הקבוצה האורחת {request.GuestTeam} עם {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה בחוץ; קבוצה מאוד חזקה בחוץ לאחרונה כשהיא עם {request.VerboseMomentumTotal} נצחונות ב-{request.Momentum} משחקים האחרונים שלה.";
                        else if (request.WinPercent >= 80)
                            contentTip = $"הקבוצה האורחת {request.GuestTeam} נמצאת בכושר משחק מעולה כאשר היא עם {request.VerboseTotal} נצחונות ב-{request.TotalGames} משחקים אחרונים שלה; כאשר היא גם שומרת על מאזן מרשים גם ב-{request.Momentum} המשחקים האחרונים שלה.";
                    }
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 4))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ עם לפחות 3 שערים למשחק; כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם 3 שערים לפחות.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 5))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ עם מקסימום 2 שערים למשחק כאשר ב-{request.Momentum} משחקים אחרונים שלה היא מסיימת ב-{request.VerboseMomentumTotal} משחקים עם מקסימום 2 שערים למשחק.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 6))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ עם מקסימום שער בודד למשחק; במשחקיה האחרונים שלה בחוץ היא עם {request.VerboseMomentumTotal} משחקים עם שער בודד מקסימום למשחק מתוך {request.Momentum} משחקים אחרונים שלה בחוץ.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 7))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ עם טווח של 2-3 שערים למשחק; במשחקיה האחרונים שלה בחוץ היא עם {request.VerboseMomentumTotal} משחקים עם טווח של 2-3 שערים למשחק מתוך {request.Momentum} משחקים אחרונים שלה בחוץ.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 8))
                        contentTip = $"האורחת {request.GuestTeam} מסיימת {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ עם מינימום 4 שערים למשחק; במשחקיה האחרונים שלה בחוץ היא עם {request.VerboseMomentumTotal} משחקים עם מינימום 4 שערים למשחק מתוך {request.Momentum} משחקים אחרונים שלה בחוץ.";

                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} האורחת {request.GuestTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"האורחת {request.GuestTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";
                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} המארחת {request.HomeTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"המארחת {request.HomeTeam} בכל המשחקים האחרונים שלה עם תוספת נקודות של {request.ExtraPointsString} היא עומדת בליין ב-{request.VerboseTotal} מתוך {request.TotalGames} כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא עומדת בליין.";

                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"האורחת {request.GuestTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ מסיימת מעל {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מעל רף נקודות זה.";
                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"האורחת {request.GuestTeam} בכל {request.VerboseTotal} מתוך {request.TotalGames} משחקים אחרונים שלה בחוץ מסיימת מתחת {request.ExtraPointsString} נקודות למשחק כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים היא מסיימת מתחת רף נקודות זה.";
                    break;
                case IndexType.Head2HeadHome:
                case IndexType.Head2HeadDraw:
                case IndexType.Head2HeadAway:
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 1))
                        contentTip = $"מתוך {request.TotalGames} משחקים בניהן בשנים האחרונות {request.HomeTeam} מנצחת ב-{request.VerboseTotal} כאשר ב-{request.Momentum} המשחקים האחרונים היא מנצחת ב-{request.VerboseMomentumTotal} משחקים.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 2))
                        contentTip = $"שתי הקבוצות מסיימות לרוב בתיקו במשחקים בניהן; כאשר ב-{request.TotalGames} המפגשים האחרונים בניהן {request.VerboseTotal} משחקים הן סיימו ללא הכרעה.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 3))
                        contentTip = $"מתוך {request.TotalGames} משחקים בניהן בשנים האחרונות; {request.GuestTeam} מנצחת ב-{request.VerboseTotal} כאשר ב-{request.Momentum} המשחקים האחרונים היא מנצחת ב-{request.VerboseMomentumTotal}";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 4))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים בניהן מסיימות לפחות ב-3 שערים למשחק; כאשר ב-{request.VerboseMomentumTotal} משחקים מתוך {request.Momentum} משחקים אחרונים הן מסיימות מעל 2.5 שערים.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 5))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים בניהן מסיימות מקסימום 2 שערים למשחק; כאשר ב-{request.VerboseMomentumTotal} משחקים מתוך {request.Momentum} משחקים אחרונים הן מסיימות מתחת ל-2.5 שערים.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 6))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים בניהן מסיימות עם שער בודד למשחק; כאשר במשחקים האחרונים {request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים בניהן בכל משחק יש מקסימום שער בודד למשחק.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 7))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן מסיימות בטווח של 2-3 שערים למשחק במשחקים אחרונים.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 8))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן מסיימות עם מינימום 4 שערים למשחק במשחקים אחרונים.";

                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} המארחת {request.HomeTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן כאשר אם היינו מוסיפים {request.ExtraPointsString} ל-{request.HomeTeam} היא היתה עומדת בליין.";
                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} האורחת {request.GuestTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן כאשר אם היינו מוסיפים {request.ExtraPointsString} ל-{request.GuestTeam} היא היתה עומדת בליין.";

                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן; מסיימות מעל {request.ExtraPointsString} נקודות; כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים בניהן הן מסיימות מעל רף נקודות זה.";
                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן; מסיימות מתחת {request.ExtraPointsString} נקודות; כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים בניהן הן מסיימות מתחת רף נקודות זה.";
                    break;
                case IndexType.Head2HeadExactHome:
                case IndexType.Head2HeadExactDraw:
                case IndexType.Head2HeadExactAway:
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 1))
                        contentTip = $"מתוך {request.TotalGames} משחקים בניהן בשנים האחרונות כאשר {request.HomeTeam} מארחת את המשחק במגרש הביתי שלה; היא מנצחת ב-{request.VerboseTotal} משחקים.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 2))
                        contentTip = $"שתי הקבוצות מרבות לסיים בתיקו את המפגשים בניהן כאשר {request.HomeTeam} מארחת בבית את {request.GuestTeam} ב-{request.TotalGames} משחקים אחרונים בניהן כאשר {request.HomeTeam} מארחת; הן סיימו {request.VerboseTotal} בתוצאת תיקו.";
                    if (request.FileType == FileType.SoccerFather && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 3))
                        contentTip = $"מתוך {request.TotalGames} משחקים בניהן בשנים האחרונות; כאשר {request.GuestTeam} משחקת בחוץ היא מנצחת ב-{request.VerboseTotal}.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 4))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן כאשר {request.HomeTeam} משחקת בבית מסיימות לפחות ב-3 שערים למשחק.";
                    if (request.FileType == FileType.SoccerChild2point5Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 5))
                        contentTip = $"שתי הקבוצות ב-{request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן כאשר {request.HomeTeam} משחקת בבית מסיימות מקסימום 2 שערים למשחק.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 6))
                        contentTip = $"שתי הקבוצות ב- {request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן כאשר {request.HomeTeam} משחקת בבית; מסיימות עם מקסימום שער בודד למשחק במשחקים האחרונים.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Draw && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 7))
                        contentTip = $"שתי הקבוצות ב- {request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן כאשר {request.HomeTeam} משחקת בבית; מסיימות עם טווח של 2-3 שערים למשחק במשחקים האחרונים.";
                    if (request.FileType == FileType.SoccerChild23Gols && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 8))
                        contentTip = $"שתי הקבוצות ב- {request.VerboseTotal} מתוך {request.TotalGames} משחקים בניהן כאשר {request.HomeTeam} משחקת בבית; מסיימות עם מינימום 4 שערים למשחק במשחקים האחרונים.";

                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} המארחת {request.HomeTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן; כאשר {request.HomeTeam} מארחת את המשחק; עם תוספת נקודות של {request.ExtraPointsString} ל-{request.HomeTeam} היא היתה עומדת בליין.";
                    //if (request.FileType == FileType.BasketBallFatherExtraPoints && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                    //    contentTip = $"עם תוספת נקודות של {request.ExtraPointsString} האורחת {request.GuestTeam} פייבוריטית לעמוד בליין במשחק זה";
                    ////contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן; כאשר {request.GuestTeam} אורחת של המשחק; עם תוספת נקודות של {request.ExtraPointsString} ל-{request.GuestTeam} היא היתה עומדת בליין.";

                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Home && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן; כאשר {request.HomeTeam} מארחת את המשחק; הן מסיימות מעל {request.ExtraPointsString} נקודות; כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים בניהן הן מסיימות מעל רף נקודות זה.";
                    if (request.FileType == FileType.BasketballBarrier && request.BetType == BetType.Away && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 10))
                        contentTip = $"ב-{request.VerboseTotal} מתוך {request.TotalGames} מפגשים אחרונים בניהן; כאשר {request.HomeTeam} מארחת את המשחק; הן מסיימות מתחת {request.ExtraPointsString} נקודות; כאשר ב-{request.VerboseMomentumTotal} מתוך {request.Momentum} משחקים אחרונים בניהן הן מסיימות מתחת רף נקודות זה.";
                    break;
            }

            if (request.FileType == FileType.BasketBallFatherExtraPoints && request.WinPercent >= 50 && (showOnly == 0 || showOnly == 9))
                contentTip = $"על פי נתוני המשחקים האחרונים של שתי הקבוצות והיסטוריית המפגשים בניהן, אנו צופים עמידה בליין במשחק זה.";

            return contentTip;
        }
    }
}
