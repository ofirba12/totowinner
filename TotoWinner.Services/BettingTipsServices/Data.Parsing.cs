﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Service;

namespace TotoWinner.Services
{
    public partial class BettingTipsServices : Singleton<BettingTipsServices>, IBettingTipsServices
    {
        #region Parser
        public BettingTipsEventsRepository ParseWinnerLineResponse(dynamic dWinnerLineResponse, long roundIdInput, DateTime winnerLineDate, bool useCash, List<SportIds> sportsToParse)
        {
            var result = new Dictionary<int, BettingTipsParentEvent>();
            var skippedEventsInfo = new List<string>();
            var events = dWinnerLineResponse["events"];
            var parsingErrors = new List<string>();
            foreach (var evnt in events)
            {
                try
                {
                    var sportId = BettingTipsServicesHelpers.DynamicResolver<int>(evnt, "sport", "id", ResolveEnum.INT); //2 כדורגל
                    if (!sportsToParse.Contains((SportIds)sportId))
//                    if (sportId != (int)SportIds.Soccer && sportId != (int)SportIds.Basketball)
                        continue;
                    var roundId = BettingTipsServicesHelpers.DynamicResolver<long>(evnt, "round", "id", ResolveEnum.LONG);
                    if (roundId != roundIdInput)
                        continue;
                    var place = BettingTipsServicesHelpers.DynamicResolver<int>(evnt, "round", "place", ResolveEnum.INT);
                    var eventId = BettingTipsServicesHelpers.DynamicResolver<int>(evnt, null, "id", ResolveEnum.INT);
                    var totoId = BettingTipsServicesHelpers.DynamicResolver<int>(evnt, null, "totoId", ResolveEnum.INT);
                    var gameResult = BettingTipsServicesHelpers.DynamicResolver<string>(evnt, null, "result", ResolveEnum.STRING);
                    var league = BettingTipsServicesHelpers.DynamicResolver<string>(evnt, "league", "name", ResolveEnum.STRING);
                    var country = BettingTipsServicesHelpers.DynamicResolver<string>(evnt, "country", "name", ResolveEnum.STRING);
                    var isFather = BettingTipsServicesHelpers.DynamicResolver<bool>(evnt, null, "isFather", ResolveEnum.BOOL);
                    var time = BettingTipsServicesHelpers.DynamicResolver<DateTime>(evnt, null, "time", ResolveEnum.DATE);
                    var status = BettingTipsServicesHelpers.DynamicResolver<string>(evnt, "status", "description", ResolveEnum.STRING);
                    var bet = ParseBet(evnt["bet"], (SportIds)sportId, roundId, eventId, totoId, skippedEventsInfo);
                    if (bet == null)
                        continue;
                    var homeId = BettingTipsServicesHelpers.DynamicResolver<int>(evnt, "home", "id", ResolveEnum.INT);
                    var homeTeamName = BettingTipsServicesHelpers.DynamicResolver<string>(evnt, "home", "name", ResolveEnum.STRING);
                    var guestId = BettingTipsServicesHelpers.DynamicResolver<int>(evnt, "guest", "id", ResolveEnum.INT);
                    var guestTeamName = BettingTipsServicesHelpers.DynamicResolver<string>(evnt, "guest", "name", ResolveEnum.STRING);
                    var eventInfoJson = GetEventInfo(eventId, useCash);

                    var eventInfoDict = WebApiInvoker.JsonDeserializeDynamic(eventInfoJson);
                    var lastGames = ParseLastGames(eventInfoDict, homeId, homeTeamName, guestId, guestTeamName, skippedEventsInfo);
                    var h2h = ParseH2H(eventInfoDict, skippedEventsInfo);
                    var btParentEvent = new BettingTipsParentEvent(eventId,
                        totoId,
                        sportId,
                        roundId,
                        place,
                        country,
                        league,
                        isFather,
                        time,
                        status,
                        homeId,
                        homeTeamName,
                        guestId,
                        guestTeamName,
                        bet, lastGames, h2h,
                        gameResult);
                    result.Add(eventId, btParentEvent);
                }
                catch(Exception ex)
                {
                    skippedEventsInfo.Add($"Unexpected error found: {evnt.ToString()}{Environment.NewLine}{ex.ToString()}");
                }
            }
            var repository = new BettingTipsEventsRepository(result, skippedEventsInfo, roundIdInput, winnerLineDate);
            return repository;
        }

        private List<BettingTipsHeadToHead> ParseH2H(dynamic eventInfoDict, List<string> skippedEventsInfo)
        {
            eventInfoDict.TryGetValue("h2h", out dynamic dH2h);
            var collection = new List<BettingTipsHeadToHead>();
            foreach (var dh2hLine in dH2h)
            {
                var progress = string.Empty;
                try
                {
                    var date = BettingTipsServicesHelpers.DynamicResolver<DateTime>(dh2hLine, null, "date", ResolveEnum.DATE);
                    progress = $"{progress} date {date} ";
                    var home = BettingTipsServicesHelpers.DynamicResolver<string>(dh2hLine, null, "home", ResolveEnum.STRING);
                    progress = $"{progress} home {home} ";
                    var guest = BettingTipsServicesHelpers.DynamicResolver<string>(dh2hLine, null, "guest", ResolveEnum.STRING);
                    progress = $"{progress} guest {guest} ";
                    var score = BettingTipsServicesHelpers.DynamicResolver<string>(dh2hLine, null, "score", ResolveEnum.STRING);
                    progress = $"{progress} score {score} ";
                    var league = BettingTipsServicesHelpers.DynamicResolver<string>(dh2hLine, null, "league", ResolveEnum.STRING);
                    progress = $"{progress} league {league} ";
                    var h2hLine = new BettingTipsHeadToHead(date, home, guest, score, league);
                    collection.Add(h2hLine);
                }
                catch (Exception ex)
                {
                    skippedEventsInfo.Add($"Head 2 Head found invalid and will be skipped, parsed items {progress}{Environment.NewLine}{ex.ToString()}");
                }
            }
            return collection;
        }

        private BettingTipsLastGames ParseLastGames(dynamic eventInfoDict, int homeId, string homeTeamName, int guestId, string guestTeamName, List<string> skippedEventsInfo)
        {
            var homeGames = GetLastGames(eventInfoDict, "home", homeTeamName, skippedEventsInfo);
            var guestGames = GetLastGames(eventInfoDict, "guest", guestTeamName, skippedEventsInfo);
            var result = new BettingTipsLastGames(homeId, guestId, homeGames, guestGames);
            return result;
        }
        private List<BettingTipsGameInfo> GetLastGames(dynamic eventInfoDict, string team, string teamName, List<string> skippedEventsInfo)
        {
            var dTeam = eventInfoDict[team];
            dTeam.TryGetValue("lastGames", out dynamic dlastGames);
            var games = new List<BettingTipsGameInfo>();
            foreach (var game in dlastGames)
            {
                var progress = string.Empty;
                try
                {
                    var date = BettingTipsServicesHelpers.DynamicResolver<DateTime>(game, null, "date", ResolveEnum.DATE);
                    progress = $"{progress} date {date} ";
                    var place = BettingTipsServicesHelpers.DynamicResolver<int>(game, null, "place", ResolveEnum.INT);
                    progress = $"{progress} place {place} ";
                    var opponent = BettingTipsServicesHelpers.DynamicResolver<string>(game, null, "opponent", ResolveEnum.STRING);
                    progress = $"{progress} opponent {opponent} ";
                    var status = BettingTipsServicesHelpers.DynamicResolver<int>(game, null, "status", ResolveEnum.INT);
                    progress = $"{progress} status {status} ";
                    var league = BettingTipsServicesHelpers.DynamicResolver<string>(game, null, "league", ResolveEnum.STRING);
                    progress = $"{progress} league {league} ";
                    var score = BettingTipsServicesHelpers.DynamicResolver<string>(game, null, "score", ResolveEnum.STRING);
                    progress = $"{progress} score {score} ";
                    var gameInfo = new BettingTipsGameInfo(date, place, opponent, status, league, score, teamName);
                    games.Add(gameInfo);
                }
                catch (Exception ex)
                {
                    skippedEventsInfo.Add($"Last game found invalid for team {team}, teamName {teamName} and will be skipped {Environment.NewLine}parsed items {progress}{Environment.NewLine}{ex.ToString()}");
                }
            }
            return games;
        }
        private BettingTipsBet ParseBet(dynamic bet, SportIds sportId, long roundId, int eventId, int totoId, List<string> skippedEventsInfo)
        {
            try
            {
                var isSingle = BettingTipsServicesHelpers.DynamicResolver<bool>(bet, null, "single", ResolveEnum.BOOL);
                var isDouble = BettingTipsServicesHelpers.DynamicResolver<bool>(bet, null, "double", ResolveEnum.BOOL);
                bet["1"].TryGetValue("current", out dynamic dhomeBet);
                bet["X"].TryGetValue("current", out dynamic dDrawBet);
                bet["2"].TryGetValue("current", out dynamic dAwayBet);
                bet["description"].TryGetValue("value", out dynamic dBetName);
                bet["description"].TryGetValue("prefix", out dynamic dBetNamePrefix);
                bet["period"].TryGetValue("name", out dynamic dPeriod);
                bet["type"].TryGetValue("id", out dynamic dBetTypeId);
                bet["type"].TryGetValue("name", out dynamic dBetTypeName);
                var homeBet = decimal.Parse(dhomeBet);
                var drawBet = decimal.Parse(dDrawBet);
                var awayBet = decimal.Parse(dAwayBet);
                var betName = $"{dBetNamePrefix?.ToString()}{dBetName.ToString()}";
                var betTypeId = int.Parse(dBetTypeId);
                var betTypeName = $"{dBetTypeName}";
                decimal? barrierHome = null;
                decimal? barrierGuest = null;
                if (sportId == SportIds.Basketball)
                {
                    var teams = betName.Split('-');
                    barrierHome = GetBarrierFromName(teams[0]);
                    barrierGuest = GetBarrierFromName(teams[1]);
                }
                var homeBetWon = BettingTipsServicesHelpers.DynamicResolver<bool>(bet["1"], null, "won", ResolveEnum.BOOL);
                var drawBetWon = BettingTipsServicesHelpers.DynamicResolver<bool>(bet["X"], null, "won", ResolveEnum.BOOL);
                var awayBetWon = BettingTipsServicesHelpers.DynamicResolver<bool>(bet["2"], null, "won", ResolveEnum.BOOL);
                var result = new BettingTipsBet(isSingle, isDouble, betTypeId, betTypeName, homeBet, drawBet, awayBet, betName, dPeriod.ToString(), barrierHome, barrierGuest,
                    homeBetWon, drawBetWon, awayBetWon);
                return result;
            }
            catch(Exception ex)
            {
                skippedEventsInfo.Add($"Bet found invalid for sport {sportId}, roundId {roundId}, eventId {eventId}, totoId {totoId} and will be skipped {Environment.NewLine}{ex.ToString()}");
            }
            return null;
        }

        private decimal? GetBarrierFromName(string betName)
        {
            decimal? result = null;
            string[] numbers = Regex.Split(betName, @"\D+");
            var numberStr = string.Empty;
            if (numbers.ToList().Any(n => !string.IsNullOrEmpty(n)))
            {
                foreach (var number in numbers)
                {
                    if (!string.IsNullOrEmpty(number))
                        numberStr = string.IsNullOrEmpty(numberStr)
                            ? $"{number}"
                            : $"{numberStr}.{number}";
                }
                result = decimal.Parse(numberStr);
            }
            return result;
        }
        #endregion
    }
}
