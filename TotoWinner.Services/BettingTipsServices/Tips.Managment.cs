﻿using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BettingTips;
using TotoWinner.Service;

namespace TotoWinner.Services
{
    public partial class BettingTipsServices : Singleton<BettingTipsServices>, IBettingTipsServices
    {
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;

        public void FinishTipGeneration()
        {
            _btPersistanceSrv.UpdateFinishTipGenerationState(true);
        }
        public void SetCloseStatusForCandidateTips()
        {
            var programIdStep = "NA";
            var gameIndexStep = "NA";
            try
            {
                var candidates = _btPersistanceSrv.GetCloseForBetCandidates();
                foreach (var candidate in candidates)
                {
                    programIdStep = candidate.ProgramId.ToString();
                    gameIndexStep = candidate.GameIndex.ToString();
                    _btPersistanceSrv.UpdateTipStatus(candidate.ProgramId, candidate.GameIndex, "סגור");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to update with status 'סגור', error found in program id={programIdStep}, gameIndex={gameIndexStep}", ex);
            }
        }
        public void CollectTipsAndUpdateResults(List<Tip> tipsToUpdate)
        {
            var programIdStep = "NA";
            var gameIndexStep = "NA";
            var sportsToParse = new List<SportIds>() { SportIds.Soccer, SportIds.Basketball };
            try
            {
                var programs = tipsToUpdate.Select(t => t.ProgramId).ToList().Distinct();
                foreach (var programId in programs)
                {
                    programIdStep = programId.ToString();
                    var programDate = _btPersistanceSrv.GetProgramDate(programId);
                    var tipsDict = tipsToUpdate
                        .Where(p => p.ProgramId == programId)
                        .ToDictionary(x => x.GameIndex, x => x);
                    if (programDate.HasValue)
                    {
                        var jsonData = GetWinnerLineResponse(programDate.Value, false);
                        var data = WebApiInvoker.JsonDeserializeDynamic(jsonData);
                        var repository = ParseWinnerLineResponse(data, programId, programDate.Value, false, sportsToParse);
                        var updatedGames = new List<int>();
                        foreach (BettingTipsParentEvent eventData in repository.Events.Values)
                        {
                            if (tipsDict.ContainsKey(eventData.Place))
                            {
                                gameIndexStep = eventData.Place.ToString();
                                var tip = tipsDict[eventData.Place];
                                if (eventData.Bet.FinalHomeWin)
                                    tip.TipOfficialMark = "1";
                                else if (eventData.Bet.FinalDraw)
                                    tip.TipOfficialMark = "X";
                                else if (eventData.Bet.FinalAwayWin)
                                    tip.TipOfficialMark = "2";
                                else
                                    tip.TipOfficialMark = null;
                                tip.TipOfficialResult = string.IsNullOrEmpty(eventData.GameResult) ? null : eventData.GameResult;
                                if (!string.IsNullOrEmpty(tip.TipOfficialMark) && !string.IsNullOrEmpty(tip.TipOfficialResult))
                                {
                                    _btPersistanceSrv.UpdateTipWithOfficialResults(tip.ProgramId, tip.GameIndex, tip.TipOfficialMark, tip.TipOfficialResult);
                                    updatedGames.Add(tip.GameIndex);
                                }
                            }
                        }
                        var gameWithNoResults = tipsDict.Keys.Except(updatedGames);
                        foreach (var gameIndex in gameWithNoResults)
                        {
                            if (tipsDict[gameIndex].GameStart <= DateTime.Today.AddDays(-2))
                                _btPersistanceSrv.UpdateTipWithManualResults(programId, gameIndex, "1", "[N : N]");
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occured trying to update results for all tips with no results, error found in program id={programIdStep}, gameIndex={gameIndexStep}", ex);
            }
        }
        public List<Tip> GetWarehouseTips(DateTime programDate)
        {
            var programs = _btPersistanceSrv.GetPrograms(programDate.Date);
            var tips = _btPersistanceSrv.GetWarehouseTips(programs).Select(
                t => new Tip(
                    t.TipId,
                    t.ProgramId,
                    t.PowerTip,
                    (SportIds)t.SportId,
                    t.TotoId,
                    t.Country,
                    t.GameIndex,
                    t.GameStart,
                    t.TipType,
                    t.Status,
                    t.BetName,
                    t.BetLeague,
                    t.BetParentChild,
                    t.IsSingle,
                    t.BetType.First(),
                    (decimal)t.BetRate,
                    t.TipTime,
                    t.TipContent,
                    t.TipMarks,
                    t.TipUrl,
                    t.TipOfficialResult,
                    t.TipOfficialMark,
                    t.TipManualResult,
                    t.TipManualMark,
                    t.Active,
                    t.TipperId
                    )
                );
            return tips.ToList();
        }

        public Tip GetNextActiveTips(BettingTipsFilter filter, List<int>SubscriberAlreadySentTipIds, List<Tip> currentStackTips = null)
        {
            var allTips = (currentStackTips ?? GetAllActiveStackTips())
                .Where(t=> SubscriberAlreadySentTipIds!= null && !SubscriberAlreadySentTipIds.Contains(t.TipId));

            Tip nextTip = null;
            switch (filter.FilterType)
            {
                case BettingTipFilterType.NoFilter:
                    nextTip = allTips.FirstOrDefault(t => t.BetRate <= filter.MaxRate.Value);
                    break;
                case BettingTipFilterType.Favorite:
                    nextTip = allTips.FirstOrDefault(t => t.BetRate <= filter.MaxRate.Value);
                    break;
                case BettingTipFilterType.In3HoursAndFavorite:
                    nextTip = allTips.FirstOrDefault(t => t.BetRate <= filter.MaxRate.Value && t.GameStart <= DateTime.Now.AddHours(3));
                    break;
                case BettingTipFilterType.SingleHighRate:
                    nextTip = allTips.FirstOrDefault(t => t.BetRate >= filter.MinRate.Value && t.BetRate <= filter.MaxRate.Value);
                    break;
                default:
                    nextTip = allTips.FirstOrDefault(t => t.BetRate <= filter.MaxRate.Value);
                    break;
                    //case BettingTipFilterType.Filtered:
                    //    nextTip = allTips.FirstOrDefault(t => t.BetRate <= filter.MaxRate.Value && t.Sport == filter.SportId && t.Country == filter.Country);
                    //    break;
            }
            return nextTip;
        }
            
        public List<Tip> GetAllActiveStackTips()
        {
            var tips = _btPersistanceSrv.GetAllStackTips().Select(
                t => new Tip(
                    t.TipId,
                    t.ProgramId,
                    t.PowerTip,
                    (SportIds)t.SportId,
                    t.TotoId,
                    t.Country,
                    t.GameIndex,
                    t.GameStart,
                    t.TipType,
                    t.Status,
                    t.BetName,
                    t.BetLeague,
                    t.BetParentChild,
                    t.IsSingle,
                    t.BetType.First(),
                    (decimal)t.BetRate,
                    t.TipTime,
                    t.TipContent,
                    t.TipMarks,
                    t.TipUrl,
                    t.TipOfficialResult,
                    t.TipOfficialMark,
                    t.TipManualResult,
                    t.TipManualMark,
                    t.Active,
                    t.TipperId
                    )
                )
                .Where(t => t.IsActive == true);
            return tips.ToList();
        }
        public Tip GetTip(int tipId)
        {
            var t = _btPersistanceSrv.GetTip(tipId);
            Tip tip = null;
            if (t != null)
            {
                tip = new Tip(
                    t.TipId,
                    t.ProgramId,
                    t.PowerTip,
                    (SportIds)t.SportId,
                    t.TotoId,
                    t.Country,
                    t.GameIndex,
                    t.GameStart,
                    t.TipType,
                    t.Status,
                    t.BetName,
                    t.BetLeague,
                    t.BetParentChild,
                    t.IsSingle,
                    t.BetType.First(),
                    (decimal)t.BetRate,
                    t.TipTime,
                    t.TipContent,
                    t.TipMarks,
                    t.TipUrl,
                    t.TipOfficialResult,
                    t.TipOfficialMark,
                    t.TipManualResult,
                    t.TipManualMark,
                    t.Active,
                    t.TipperId
                    );
            }
            return tip;
        }
        public List<Tip> GetAllStackTips()
        {
            var tips = _btPersistanceSrv.GetAllStackTips().Select(
                t => new Tip(
                    t.TipId,
                    t.ProgramId,
                    t.PowerTip,
                    (SportIds)t.SportId,
                    t.TotoId,
                    t.Country,
                    t.GameIndex,
                    t.GameStart,
                    t.TipType,
                    t.Status,
                    t.BetName,
                    t.BetLeague,
                    t.BetParentChild,
                    t.IsSingle,
                    t.BetType.First(),
                    (decimal)t.BetRate,
                    t.TipTime,
                    t.TipContent,
                    t.TipMarks,
                    t.TipUrl,
                    t.TipOfficialResult,
                    t.TipOfficialMark,
                    t.TipManualResult,
                    t.TipManualMark,
                    t.Active,
                    t.TipperId
                    )
                );
            return tips.ToList();
        }
        public Dictionary<int, int> GetTipsCounters(List<Tip> tips)
        {
            var counters = _btPersistanceSrv.CountClicks(tips.Select(t => t.TipId).ToList());
            return counters;
        }
        public List<Tip> GetAllTipsWithNoResults()
        {
            var tips = _btPersistanceSrv.GetAllTipsWithNoResults().Select(
                t => new Tip(
                    t.TipId,
                    t.ProgramId,
                    t.PowerTip,
                    (SportIds)t.SportId,
                    t.TotoId,
                    t.Country,
                    t.GameIndex,
                    t.GameStart,
                    t.TipType,
                    t.Status,
                    t.BetName,
                    t.BetLeague,
                    t.BetParentChild,
                    t.IsSingle,
                    t.BetType.First(),
                    (decimal)t.BetRate,
                    t.TipTime,
                    t.TipContent,
                    t.TipMarks,
                    t.TipUrl,
                    t.TipOfficialResult,
                    t.TipOfficialMark,
                    t.TipManualResult,
                    t.TipManualMark,
                    t.Active,
                    t.TipperId
                    )
                )
                .Where(tip => tip.Status == "סגור"
                    && (string.IsNullOrEmpty(tip.TipOfficialResult) || string.IsNullOrEmpty(tip.TipOfficialMark) 
                    || string.IsNullOrEmpty(tip.TipManualResult)));

            return tips.ToList();
        }
        public void PersistProgramAndTips(BettingTipsStacks stackes, BettingTipsEventsRepository infoRepository)
        {
            try
            {
                _btPersistanceSrv.AddProgram(infoRepository.RoundId, infoRepository.ProgramDate);
                var tips = new Dictionary<int, Tip>(); //game index, tip
                for (var powertip = 6; powertip >= 0; powertip--)
                {
                    foreach (var stack in stackes.PowertipStacks[powertip])
                    {
                        var key = new BettingTipKey(stack.EventId, stack.Type);
                        if (stackes.TipDescription.ContainsKey(key))
                        {
                            var tip = CreateTip(infoRepository.Events[stack.EventId],
                                stackes.TipDescription[key],
                                stack.Type,
                                infoRepository.RoundId,
                                powertip,
                                true);
                            if (!tips.ContainsKey(tip.GameIndex) && PassFilter(tip))
                                tips.Add(tip.GameIndex, tip);
                        }
                    }
                }
                foreach (var tip in tips.Values)
                {
                    _btPersistanceSrv.InsertTip(tip.ProgramId,
                                tip.GameIndex,
                                (int)tip.Sport,
                                tip.TotoId,
                                tip.GameStart,
                                tip.IsAutomaticTip,
                                (short)tip.PowerTip,
                                tip.BetName,
                                tip.BetLeague,
                                tip.Country,
                                tip.IsParentBet,
                                tip.BetMark,
                                tip.IsSingle,
                                (double)tip.BetRate,
                                tip.Status,
                                tip.TipTime,
                                tip.TipContent,
                                tip.TipMarks,
                                tip.TipUrl,
                                tip.TipperId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to persist program and tips for programId={infoRepository.RoundId} with date {infoRepository.ProgramDate}", ex);
            }
        }

        private bool PassFilter(Tip tip)
        {
            var isValid = tip.BetRate >= (decimal)1.25
                && tip.IsSingle
                && !string.IsNullOrEmpty(tip.TipContent)
                && tip.PowerTip > 0;

            return isValid;
        }

        //advisorName add to database
        public void AddManualTip(long programId, SportIds sport, int gameIndex, string country, string league, DateTime gameStart, int powertip,
            bool isSingle, char tipMark, string tipContent, string tipUrl, decimal betRate, string betName, bool isFather, int tipperId)
        {
            var tip = new Tip(0,
                programId,
                powertip,
                sport,
                -1,
                country,
                gameIndex,
                gameStart,
                false,
                betName,
                league,
                isFather,
                isSingle,
                tipMark,
                betRate,
                BuildTipTime(sport, gameStart),
                tipContent,
                BuildTipManualMarking(gameIndex, tipMark),
                tipUrl,
                null, null, null, null,
                true,
                tipperId
                );

            _btPersistanceSrv.InsertTip(tip.ProgramId,
                               tip.GameIndex,
                               (int)tip.Sport,
                               tip.TotoId,
                               tip.GameStart,
                               tip.IsAutomaticTip,
                               (short)tip.PowerTip,
                               tip.BetName,
                               tip.BetLeague,
                               tip.Country,
                               tip.IsParentBet,
                               tip.BetMark,
                               tip.IsSingle,
                               (double)tip.BetRate,
                               tip.Status,
                               tip.TipTime,
                               tip.TipContent,
                               tip.TipMarks,
                               tip.TipUrl,
                               tip.TipperId);
        }

        private Tip CreateTip(BettingTipsParentEvent bettingTipsParentEvent,
            BettingTip tip,
            BetType type,
            long programId,
            int powertip,
            bool isAutomaticTip)
        {
            var betMark = '*';
            var betRate = (decimal)0;
            switch (type)
            {
                case BetType.Home:
                    betMark = '1';
                    betRate = bettingTipsParentEvent.Bet.Home;
                    break;
                case BetType.Draw:
                    betMark = 'X';
                    betRate = bettingTipsParentEvent.Bet.Draw;
                    break;
                case BetType.Away:
                    betMark = '2';
                    betRate = bettingTipsParentEvent.Bet.Away;
                    break;
            }

            var t = new Tip(0,
                programId,
                powertip,
                bettingTipsParentEvent.SportId,
                bettingTipsParentEvent.TotoId,
                bettingTipsParentEvent.Country,
                bettingTipsParentEvent.Place,
                bettingTipsParentEvent.Time,
                isAutomaticTip,
                bettingTipsParentEvent.Bet.Name,
                bettingTipsParentEvent.LeagueName,
                bettingTipsParentEvent.IsFather,
                bettingTipsParentEvent.Bet.Single,
                betMark,
                betRate,
                tip.Time,
                tip.Content.Replace(';',','),
                tip.Marking,
                tip.URL,
                null, null, null, null,
                true,
                1
                );
            return t;
        }
    }
}
