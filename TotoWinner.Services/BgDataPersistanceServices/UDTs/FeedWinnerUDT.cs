﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Winner_UDT")]
    public class FeedWinnerUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int EventId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public long ProgramId { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public byte SportId { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public int TotoId { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public int HomeId { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public int GuestId { get; set; }
        [UserDefinedTableTypeColumn(7)]
        public short BetTypeId { get; set; }
        [UserDefinedTableTypeColumn(8)]
        public int Place { get; set; }
        [UserDefinedTableTypeColumn(9)]
        public string Name { get; set; }
        [UserDefinedTableTypeColumn(10)]
        public DateTime GameStart { get; set; }
        [UserDefinedTableTypeColumn(11)]
        public string Status { get; set; }
        [UserDefinedTableTypeColumn(12)]
        public decimal? HomeOdd { get; set; }
        [UserDefinedTableTypeColumn(13)]
        public decimal? DrawOdd { get; set; }
        [UserDefinedTableTypeColumn(14)]
        public decimal? AwayOdd { get; set; }
        [UserDefinedTableTypeColumn(15)]
        public string Country { get; set; }
        [UserDefinedTableTypeColumn(16)]
        public string League { get; set; }
        [UserDefinedTableTypeColumn(17)]
        public DateTime LastUpdate { get; set; }

        public FeedWinnerUDT() { }
    }
}

