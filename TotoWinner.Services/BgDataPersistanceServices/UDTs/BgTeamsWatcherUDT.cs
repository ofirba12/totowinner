﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("BgTeams_Watcher_UDT")]
    public class BgTeamsWatcherUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int TeamId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int TotalShows { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public int MissingOdds { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public int MissingResult { get; set; }
        public BgTeamsWatcherUDT() { }
    }
}

