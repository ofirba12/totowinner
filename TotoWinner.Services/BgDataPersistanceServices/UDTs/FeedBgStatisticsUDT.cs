﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Bg_Statistics_UDT")]
    public class FeedBgStatisticsUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public long MatchId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int StatisticIndex { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public int? FullTimeHomeWin { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public int? FullTimeDraw { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public int? FullTimeAwayWin { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public int? FullTimeHomeWinH2H { get; set; }
        [UserDefinedTableTypeColumn(7)]
        public int? FullTimeDrawH2H { get; set; }
        [UserDefinedTableTypeColumn(8)]
        public int? FullTimeAwayWinH2H { get; set; }
        [UserDefinedTableTypeColumn(9)]
        public int? Over3_5 { get; set; }
        [UserDefinedTableTypeColumn(10)]
        public int? Under3_5 { get; set; }
        [UserDefinedTableTypeColumn(11)]
        public int? Over3_5H2H { get; set; }
        [UserDefinedTableTypeColumn(12)]
        public int? Under3_5H2H { get; set; }
        [UserDefinedTableTypeColumn(13)]
        public int? Over2_5 { get; set; }
        [UserDefinedTableTypeColumn(14)]
        public int? Under2_5 { get; set; }
        [UserDefinedTableTypeColumn(15)]
        public int? Over2_5H2H { get; set; }
        [UserDefinedTableTypeColumn(16)]
        public int? Under2_5H2H { get; set; }
        [UserDefinedTableTypeColumn(17)]
        public int? Over1_5 { get; set; }
        [UserDefinedTableTypeColumn(18)]
        public int? Under1_5 { get; set; }
        [UserDefinedTableTypeColumn(19)]
        public int? Over1_5H2H { get; set; }
        [UserDefinedTableTypeColumn(20)]
        public int? Under1_5H2H { get; set; }
        [UserDefinedTableTypeColumn(21)]
        public int? Range0_1 { get; set; }
        [UserDefinedTableTypeColumn(22)]
        public int? Range2_3 { get; set; }
        [UserDefinedTableTypeColumn(23)]
        public int? Range4Plus { get; set; }
        [UserDefinedTableTypeColumn(24)]
        public int? Range0_1H2H { get; set; }
        [UserDefinedTableTypeColumn(25)]
        public int? Range2_3H2H { get; set; }
        [UserDefinedTableTypeColumn(26)]
        public int? Range4PlusH2H { get; set; }
        [UserDefinedTableTypeColumn(27)]
        public int? BothScore { get; set; }
        [UserDefinedTableTypeColumn(28)]
        public int? NoBothScore { get; set; }
        [UserDefinedTableTypeColumn(29)]
        public int? BothScoreH2H { get; set; }
        [UserDefinedTableTypeColumn(30)]
        public int? NoBothScoreH2H { get; set; }
        [UserDefinedTableTypeColumn(31)]
        public int? HomeDraw { get; set; }
        [UserDefinedTableTypeColumn(32)]
        public int? NoDraw { get; set; }
        [UserDefinedTableTypeColumn(33)]
        public int? DrawAway { get; set; }
        [UserDefinedTableTypeColumn(34)]
        public int? HomeDrawH2H { get; set; }
        [UserDefinedTableTypeColumn(35)]
        public int? NoDrawH2H { get; set; }
        [UserDefinedTableTypeColumn(36)]
        public int? DrawAwayH2H { get; set; }
        [UserDefinedTableTypeColumn(37)]
        public decimal? HomePower { get; set; }
        [UserDefinedTableTypeColumn(38)]
        public decimal? AwayPower { get; set; }
        [UserDefinedTableTypeColumn(39)]
        public DateTime LastUpdate { get; set; }

        public FeedBgStatisticsUDT() { }
    }
}

