﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Services
{
    public class GoalserveMatchTableType
    {
        public int MatchId { get; }
        public int SportId { get; }
        public string Status { get; }
        public int? CountryId { get; }
        public string Country { get; }
        public string League { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public DateTime GameStart { get; }
        public string Home { get; }
        public int HomeId { get; }
        public string Away { get; }
        public int AwayId { get; }
        public decimal? HomeOdd { get; }
        public decimal? DrawOdd { get; }
        public decimal? AwayOdd { get; }
        public int? HomeScore { get; }
        public int? AwayScore { get; }
        public GoalserveMatchTableType(int matchId,
            int sportId,
            string status,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int homeId,
            string away,
            int awayId,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null)
        {
            this.SportId = sportId;
            this.MatchId = matchId;
            this.Status = status;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
        }
    }
}