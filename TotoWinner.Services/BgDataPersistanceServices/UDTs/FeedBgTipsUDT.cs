﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Bg_Tips_UDT")]
    public class FeedBgTipsUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public long MatchId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public byte Type { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public string Title { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public int Value { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public byte Stars { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public string Descriptions { get; set; }
        [UserDefinedTableTypeColumn(7)]
        public bool FreezeTip { get; set; }
        [UserDefinedTableTypeColumn(8)]
        public DateTime LastUpdate { get; set; }

        public FeedBgTipsUDT() { }
    }
}

