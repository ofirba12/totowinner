﻿using EntityFrameworkExtras.EF6;

namespace TotoWinner.Services
{
    [UserDefinedTableType("BgManualMapperUDT")]
    public class BgManualMapperUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int BgId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public string ManualName { get; set; }
        public BgManualMapperUDT() { }
    }
}