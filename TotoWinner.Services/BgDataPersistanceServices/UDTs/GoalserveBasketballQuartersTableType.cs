﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Services
{
    public class GoalserveBasketballQuartersTableType
    {
        public int SportId { get; }
        public int MatchId { get; }
        public int LeagueId { get; }
        public int HomeId { get; }
        public int AwayId { get; }
        public int HomeQ1 { get; }
        public int AwayQ1 { get; }
        public int HomeQ2 { get; }
        public int AwayQ2 { get; }
        public int HomeQ3 { get; }
        public int AwayQ3 { get; }
        public int HomeQ4 { get; }
        public int AwayQ4 { get; }

        public GoalserveBasketballQuartersTableType(int sportId,
            int matchId,
            int leagueId,
            int homeId,
            int awayId,
            int homeQ1,
            int awayQ1,
            int homeQ2,
            int awayQ2,
            int homeQ3,
            int awayQ3,
            int homeQ4,
            int awayQ4
            )
        {
            this.SportId = sportId;
            this.MatchId = matchId;
            this.LeagueId = leagueId;
            this.HomeId = homeId;
            this.AwayId = awayId;
            this.HomeQ1 = homeQ1;
            this.AwayQ1 = awayQ1;
            this.HomeQ2 = homeQ2;
            this.AwayQ2 = awayQ2;
            this.HomeQ3 = homeQ3;
            this.AwayQ3 = awayQ3;
            this.HomeQ4 = homeQ4;
            this.AwayQ4 = awayQ4;
        }
    }
}