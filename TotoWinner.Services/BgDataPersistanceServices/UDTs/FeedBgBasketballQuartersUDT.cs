﻿using EntityFrameworkExtras.EF6;
using System;
namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Bg_Quarters_UDT")]
    public class FeedBgBasketballQuartersUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public long MatchId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int HomeQ1 { get; }
        [UserDefinedTableTypeColumn(3)]
        public int AwayQ1 { get; }
        [UserDefinedTableTypeColumn(4)]
        public int HomeQ2 { get; }
        [UserDefinedTableTypeColumn(5)]
        public int AwayQ2 { get; }
        [UserDefinedTableTypeColumn(6)]
        public int HomeQ3 { get; }
        [UserDefinedTableTypeColumn(7)]
        public int AwayQ3 { get; }
        [UserDefinedTableTypeColumn(8)]
        public int HomeQ4 { get; }
        [UserDefinedTableTypeColumn(9)]
        public int AwayQ4 { get; }
        public FeedBgBasketballQuartersUDT() { }
        public FeedBgBasketballQuartersUDT(long matchId,
            int homeQ1,
            int awayQ1,
            int homeQ2,
            int awayQ2,
            int homeQ3,
            int awayQ3,
            int homeQ4,
            int awayQ4
            )
        {
            this.MatchId = matchId;
            this.HomeQ1 = homeQ1;
            this.AwayQ1 = awayQ1;
            this.HomeQ2 = homeQ2;
            this.AwayQ2 = awayQ2;
            this.HomeQ3 = homeQ3;
            this.AwayQ3 = awayQ3;
            this.HomeQ4 = homeQ4;
            this.AwayQ4 = awayQ4;
        }
    }
}