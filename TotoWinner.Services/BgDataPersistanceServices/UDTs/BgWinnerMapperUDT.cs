﻿using EntityFrameworkExtras.EF6;

namespace TotoWinner.Services
{
    [UserDefinedTableType("BgWinnerMapperUDT")]
    public class BgWinnerMapperUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public byte SportId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int WinnerId { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public string WinnerName { get; set; }
        public BgWinnerMapperUDT() { }
    }
}