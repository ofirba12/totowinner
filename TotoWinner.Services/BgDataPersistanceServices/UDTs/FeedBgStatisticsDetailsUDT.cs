﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Bg_StatisticsDetails_UDT")]
    public class FeedBgStatisticsDetailsUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public long MatchId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public byte Type { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public byte StatisticIndex { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public byte Population { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public int? Lines { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public byte? Percent { get; set; }
        [UserDefinedTableTypeColumn(7)]
        public DateTime LastUpdate { get; set; }

        public FeedBgStatisticsDetailsUDT() { }
    }
}

