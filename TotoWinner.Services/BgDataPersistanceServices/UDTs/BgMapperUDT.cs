﻿using EntityFrameworkExtras.EF6;

namespace TotoWinner.Services
{
    [UserDefinedTableType("BgMapperUDT")]
    public class BgMapperUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int BgId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public byte BgSport { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public short BgType { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public string BgName { get; set; }
        public BgMapperUDT() { }
    }
}