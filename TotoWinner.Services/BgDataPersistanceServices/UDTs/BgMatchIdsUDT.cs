﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("BgMatchIdsUDT")]
    public class BgMatchIdsUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public long MatchId { get; set; }
        public BgMatchIdsUDT() { }
    }
}

