﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Bg_UDT")]
    public class FeedBgUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public byte Source { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int SportId { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public int CountryId { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public int LeagueId { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public int HomeId { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public int AwayId { get; set; }
        [UserDefinedTableTypeColumn(7)]
        public DateTime GameStart { get; set; }
        [UserDefinedTableTypeColumn(8)]
        public string Status { get; set; }
        [UserDefinedTableTypeColumn(9)]
        public bool IsCup { get; set; }
        [UserDefinedTableTypeColumn(10)]
        public decimal? HomeOdd { get; set; }
        [UserDefinedTableTypeColumn(11)]
        public decimal? DrawOdd { get; set; }
        [UserDefinedTableTypeColumn(12)]
        public decimal? AwayOdd { get; set; }
        [UserDefinedTableTypeColumn(13)]
        public int? HomeScore { get; set; }
        [UserDefinedTableTypeColumn(14)]
        public int? AwayScore { get; set; }
        [UserDefinedTableTypeColumn(15)]
        public DateTime LastUpdate { get; set; }

        public FeedBgUDT() { }
    }
}

