﻿using EntityFrameworkExtras.EF6;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Goalserve_Odds_UDT")]
    public class FeedGoalserveOddsUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int SportId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int MatchId { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public string Bookname { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public decimal? HomeOdd { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public decimal? DrawOdd { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public decimal? AwayOdd { get; set; }
        public FeedGoalserveOddsUDT() { }
    }
}