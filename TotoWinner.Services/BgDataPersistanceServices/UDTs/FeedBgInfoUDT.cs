﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("Feed_Bg_Info_UDT")]
    public class FeedBgInfoUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public long MatchId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int BadMatches { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public int HomeMatches { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public int AwayMatches { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public int Head2HeadMatches { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public DateTime LastUpdate { get; set; }

        public FeedBgInfoUDT() { }
    }
}

