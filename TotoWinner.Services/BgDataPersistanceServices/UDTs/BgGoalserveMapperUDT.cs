﻿using EntityFrameworkExtras.EF6;

namespace TotoWinner.Services
{
    [UserDefinedTableType("BgGoalserveMapperUDT")]
    public class BgGoalserveMapperUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int BgId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int? GoalserveId { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public string GoalserveName { get; set; }
        public BgGoalserveMapperUDT() { }
    }
}