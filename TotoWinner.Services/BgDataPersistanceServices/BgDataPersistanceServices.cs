﻿using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;
using TotoWinner.Services.Persistance.BgData;
using EntityFrameworkExtras.EF6;
using TotoWinner.Data.BgBettingData.BgDataApi;
using TotoWinner.Data;
using TotoWinner.Services.Persistance.TotoWinner;

namespace TotoWinner.Services
{
    public class BgDataPersistanceServices : Singleton<BgDataPersistanceServices>, IBgDataPersistanceServices
    {
        private BgDataPersistanceServices() { }
        #region Goalserve
        public List<Feed_Goalserve_Fetch_Result> GoalserveMatchesOnlyGet(int sportId, List<int> matchIds)
        {
            try
            {
                List<Feed_Goalserve_Fetch_Result> matches = new List<Feed_Goalserve_Fetch_Result>();
                using (var ent = new BgDataEntities())
                {
                    var matchesOnly = ent.Feed_Goalserve
                        .Where(m => m.SportId == sportId && matchIds.Contains(m.MatchId))
                        .ToDictionary(x => new MatchKey(x.MatchId, x.LeagueId, x.HomeId, x.AwayId), x => x);
                    var bb = ent.Feed_Goalserve_BasketballQuarters
                        .Where(m => matchIds.Contains(m.MatchId))
                        .ToDictionary(x => new MatchKey(x.MatchId, x.LeagueId, x.HomeId, x.AwayId), x => x);
                    foreach (var match in matchesOnly)
                    {
                        matches.Add(new Feed_Goalserve_Fetch_Result()
                        {
                            SportId = sportId,
                            MatchId = match.Value.MatchId,
                            Status = match.Value.Status,
                            CountryId = match.Value.CountryId,
                            Country = match.Value.Country,
                            League = match.Value.League,
                            LeagueId = match.Value.LeagueId,
                            IsCup = match.Value.IsCup,
                            GameStart = match.Value.GameStart,
                            Home = match.Value.Home,
                            HomeId = match.Value.HomeId,
                            Away = match.Value.Away,
                            AwayId = match.Value.AwayId,
                            HomeOdd = match.Value.HomeOdd,
                            DrawOdd = match.Value.DrawOdd,
                            AwayOdd = match.Value.AwayOdd,
                            HomeScore = match.Value.HomeScore,
                            AwayScore = match.Value.AwayScore,
                            HomeQ1 = bb.ContainsKey(match.Key) ? bb[match.Key].HomeQ1 : (int?)null,
                            HomeQ2 = bb.ContainsKey(match.Key) ? bb[match.Key].HomeQ2 : (int?)null,
                            HomeQ3 = bb.ContainsKey(match.Key) ? bb[match.Key].HomeQ3 : (int?)null,
                            HomeQ4 = bb.ContainsKey(match.Key) ? bb[match.Key].HomeQ4 : (int?)null,
                            AwayQ1 = bb.ContainsKey(match.Key) ? bb[match.Key].AwayQ1 : (int?)null,
                            AwayQ2 = bb.ContainsKey(match.Key) ? bb[match.Key].AwayQ2 : (int?)null,
                            AwayQ3 = bb.ContainsKey(match.Key) ? bb[match.Key].AwayQ3 : (int?)null,
                            AwayQ4 = bb.ContainsKey(match.Key) ? bb[match.Key].AwayQ4 : (int?)null
                        });
                    }
                }
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to {matchIds.Count()} matches for sport {sportId}", ex);
            }
        }

        public List<Feed_Goalserve_Fetch_Result> GoalserveFeedGet(int sportId, int daysBack) //0: Today, 1: yesterday
        {
            try
            {
                List<Feed_Goalserve_Fetch_Result> matches = new List<Feed_Goalserve_Fetch_Result>();
                using (var ent = new BgDataEntities())
                {
                    matches = ent.Feed_Goalserve_Fetch(sportId, daysBack)
                        .ToList();
                }
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get geed for sport {sportId} , daysBack {daysBack}", ex);
            }
        }
        public void GoalserveFeedInsert(List<GoalserveMatch> matches)
        {
            try
            {
                var matchesTT = matches.ConvertAll(m => new GoalserveMatchTableType(
                    m.MatchId,
                    m.SportId,
                    m.Status,
                    m.CountryId,
                    m.Country,
                    m.League,
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart,
                    m.Home,
                    m.HomeId,
                    m.Away,
                    m.AwayId,
                    m.OddsAndScores?.HomeOdd,
                    m.OddsAndScores?.DrawOdd,
                    m.OddsAndScores?.AwayOdd,
                    m.OddsAndScores?.HomeScore,
                    m.OddsAndScores?.AwayScore
                ));
                var quartersTT = matches
                    .Where(m => m.OddsAndScores != null && m.OddsAndScores.BasketballQuatersScores != null)
                    .ToList()
                    .ConvertAll(m => new GoalserveBasketballQuartersTableType(
                    m.SportId,
                    m.MatchId,
                    m.LeagueId,
                    m.HomeId,
                    m.AwayId,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ1Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ1Score,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ2Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ2Score,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ3Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ3Score,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ4Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ4Score
                    ));
                using (var ent = new BgDataEntities())
                {
                    ent.ExecuteTablesValueProcedure<GoalserveMatchTableType, GoalserveBasketballQuartersTableType>(matchesTT, quartersTT,
                        "Feed_Goalserve_BulkInsert",
                        new string[] { "@Feed", "@QuartersFeed" },
                        new string[] { "Feed_Goalserve_TT", "Feed_Goalserve_BasketballQuarters_TT" });
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to bulk insert feed", ex);
            }
        }

        public void GoalserveFeedUpdate(List<GoalserveMatch> matches)
        {
            try
            {
                var matchesTT = matches.ConvertAll(m => new GoalserveMatchTableType(
                    m.MatchId,
                    m.SportId,
                    m.Status,
                    m.CountryId,
                    m.Country,
                    m.League,
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart,
                    m.Home,
                    m.HomeId,
                    m.Away,
                    m.AwayId,
                    m.OddsAndScores?.HomeOdd,
                    m.OddsAndScores?.DrawOdd,
                    m.OddsAndScores?.AwayOdd,
                    m.OddsAndScores?.HomeScore,
                    m.OddsAndScores?.AwayScore
                ));
                var quartersTT = matches
                    .Where(m => m.OddsAndScores != null && m.OddsAndScores.BasketballQuatersScores != null)
                    .ToList()
                    .ConvertAll(m => new GoalserveBasketballQuartersTableType(
                    m.SportId,
                    m.MatchId,
                    m.LeagueId,
                    m.HomeId,
                    m.AwayId,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ1Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ1Score,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ2Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ2Score,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ3Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ3Score,
                    m.OddsAndScores.BasketballQuatersScores.LocalTeamQ4Score,
                    m.OddsAndScores.BasketballQuatersScores.AwayTeamQ4Score
                    ));
                using (var ent = new BgDataEntities())
                {
                    ent.ExecuteTablesValueProcedure<GoalserveMatchTableType, GoalserveBasketballQuartersTableType>(matchesTT, quartersTT,
                        "Feed_Goalserve_BulkUpdate",
                        new string[] { "@Feed", "@QuartersFeed" },
                        new string[] { "Feed_Goalserve_TT", "Feed_Goalserve_BasketballQuarters_TT" });
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to bulk update feed", ex);
            }
        }

        #region MergeGoalserveFeedOdds
        public void MergeGoalserveFeedOdds(List<GoalserveMatchOdds> tips)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var proc = new FeedGoalserveOddsMergeStoredProcedure();
                            proc.Collection = tips.ConvertAll<FeedGoalserveOddsUDT>(t => new FeedGoalserveOddsUDT()
                            {
                                SportId = t.SportId,
                                MatchId = t.MatchId,
                                Bookname = t.Bookmaker,
                                HomeOdd = t.HomeOdd,
                                DrawOdd = t.DrawOdd,
                                AwayOdd = t.AwayOdd
                            });

                            ent.Database.ExecuteStoredProcedure<FeedGoalserveOddsUDT>(proc);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge goalserve feed odds, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge goalserve feed odds", ex);
            }
        }
        #endregion
        #endregion

        #region Mapper
        public List<BgMapperItem> GetBgMappers()
        {
            try
            {
                var mapper = new List<BgMapperItem>();
                using (var ent = new BgDataEntities())
                {
                    mapper = ent.BgMapper_Fetch()
                        .ToList()
                        .ConvertAll<BgMapperItem>(i => new BgMapperItem(i.BgId, i.BgSport, i.BgType, i.BgName, i.GoalserveId, i.GoalserveName, i.ManualName));
                }
                return mapper;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all mappers", ex);
            }
        }
        public void MergeMappers(List<BgMapper> bgMappers, List<BgManualMapper> bgManualMapper)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgMapperProc = new BgMapperMergeStoredProcedure();
                            bgMapperProc.MapperParameter = bgMappers.ConvertAll(m => new BgMapperUDT()
                            {
                                BgId = m.BgId,
                                BgSport = (byte)m.Sport,
                                BgType = (short)m.MapperType,
                                BgName = m.BgName
                            });
                            ent.Database.ExecuteStoredProcedure<BgMapperUDT>(bgMapperProc);

                            var bgManualMapperProc = new BgManualMapperMergeStoredProcedure();
                            bgManualMapperProc.MapperParameter = bgManualMapper.ConvertAll(m => new BgManualMapperUDT()
                            {
                                BgId = m.BgId,
                                ManualName = m.DisplayName
                            });
                            ent.Database.ExecuteStoredProcedure<BgManualMapperUDT>(bgManualMapperProc);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge mappers(bg + manual), transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg mappers", ex);
            }
        }
        public void MergeMappers(List<BgMapper> bgMappers, List<BgGoalserveMapper> bgGoalserveMapper)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgMapperProc = new BgMapperMergeStoredProcedure();
                            bgMapperProc.MapperParameter = bgMappers.ConvertAll(m => new BgMapperUDT()
                            {
                                BgId = m.BgId,
                                BgSport = (byte)m.Sport,
                                BgType = (short)m.MapperType,
                                BgName = m.BgName
                            });
                            ent.Database.ExecuteStoredProcedure<BgMapperUDT>(bgMapperProc);

                            var bgGoalserveMapperProc = new BgGoalserveMapperMergeStoredProcedure();
                            bgGoalserveMapperProc.MapperParameter = bgGoalserveMapper.ConvertAll(m => new BgGoalserveMapperUDT()
                            {
                                BgId = m.BgId,
                                GoalserveId = m.GoalserveId,
                                GoalserveName = m.GoalserveName
                            });
                            ent.Database.ExecuteStoredProcedure<BgGoalserveMapperUDT>(bgGoalserveMapperProc);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge mappers(bg + goldserve), transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg mappers", ex);
            }
        }

        public void UpdateMapperBgName(int bgId, string bgName)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.Mapper_UpdateBgName(bgId, bgName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update mapper bgId={bgId} with BgName={bgName}", ex);
            }
        }
        #endregion
        public void SetBgSystemItem(string name, string value, BgSystemFieldType itemType)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.BgSystem_Merge(name, value, (byte)itemType);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bgSystemItem name={name} value={value} itemType={itemType}", ex);
            }
        }
        public List<BgSystem> GetBgSystemItems()
        {
            try
            {
                var items = new List<BgSystem>();
                using (var ent = new BgDataEntities())
                {
                    items = ent.BgSystems.ToList();
                }
                return items;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get bg system items", ex);
            }
        }
        #region Merge And Ignore
        public void IgnoreBgFeed(List<long> bgMatchIds)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    var bgFeedProc = new FeedBgCleanStoredProcedure();
                    bgFeedProc.Collection = bgMatchIds.ConvertAll(m => new BgMatchIdsUDT()
                    {
                        MatchId = m
                    });
                    ent.Database.ExecuteStoredProcedure<BgMatchIdsUDT>(bgFeedProc);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete bg bgfeed", ex);
            }
        }
        public void MergeBgFeed(List<BgMatch> bgMatches)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgFeedProc = new FeedBgMergeStoredProcedure();
                            bgFeedProc.Collection = bgMatches.ConvertAll(m => new FeedBgUDT()
                            {
                                Source = (byte)m.Source,
                                SportId = (int)m.Sport,
                                CountryId = m.CountryId.Value,
                                LeagueId = m.LeagueId,
                                HomeId = m.HomeId,
                                AwayId = m.AwayId,
                                GameStart = m.GameStart,
                                Status = m.Status,
                                IsCup = m.IsCup,
                                HomeOdd = m.HomeOdd,
                                DrawOdd = m.DrawOdd,
                                AwayOdd = m.AwayOdd,
                                HomeScore = m.HomeScore,
                                AwayScore = m.AwayScore,
                                LastUpdate = m.LastUpdate
                            });
                            ent.Database.ExecuteStoredProcedure<FeedBgUDT>(bgFeedProc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge bg feed, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg feed", ex);
            }
        }
        public void MergeQuatersBgFeed(List<BgMatch> bbMatches)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    var bgFeedProc = new FeedBgQuartersMergeStoredProcedure();
                    bgFeedProc.Collection = bbMatches
                    .Where(q => q.HQ1.HasValue && q.AQ1.HasValue) //its all quarters or nothing
                    .ToList()
                    .ConvertAll(m => new FeedBgBasketballQuartersUDT(
                        m.MatchId,
                        m.HQ1.Value,
                        m.AQ1.Value,
                        m.HQ2.Value,
                        m.AQ2.Value,
                        m.HQ3.Value,
                        m.AQ3.Value,
                        m.HQ4.Value,
                        m.AQ4.Value
                    ));
                    ent.Database.ExecuteStoredProcedure<FeedBgBasketballQuartersUDT>(bgFeedProc);

                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg feed quarters", ex);
            }
        }
        #endregion

        #region Fetch
        public FeedBg_FetchByMatchId_Result FetchMatch(long matchId)
        {
            try
            {
                FeedBg_FetchByMatchId_Result item = null;
                using (var ent = new BgDataEntities())
                {
                    item = ent.FeedBg_FetchByMatchId(matchId).FirstOrDefault();
                }
                return item;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch match with matchId={matchId}", ex);
            }
        }
        public List<FeedBg_Fetch_Result> FetchBgFeed(BgSport sport, int daysFromToday)
        {
            try
            {
                List<FeedBg_Fetch_Result> items = null;
                using (var ent = new BgDataEntities())
                {
                    items = ent.FeedBg_Fetch((int)sport, daysFromToday).ToList();
                }
                return items;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch bg data for sport={sport} daysFromToday={daysFromToday}", ex);
            }
        }
        public List<FeedBg_FilterFetch_Result> FetchFilterBgFeed(BgSport sport, int daysFromToday, int gmtOffset, int? countryId, int? leagueId)
        {
            try
            {
                List<FeedBg_FilterFetch_Result> items = null;
                using (var ent = new BgDataEntities())
                {
                    items = ent.FeedBg_FilterFetch((int)sport, daysFromToday, gmtOffset, countryId, leagueId).ToList();
                }
                return items;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch and filter bg data for sport={sport} daysFromToday={daysFromToday}, gmtOffset={gmtOffset}, countryId={countryId}, leagueId={leagueId}", ex);
            }
        }
        #endregion

        #region Analysis
        public List<TeamBg_GetAllMatches_Result> GetAllTeamMatches(int teamBgId)
        {
            try
            {
                List<TeamBg_GetAllMatches_Result> items = null;
                using (var ent = new BgDataEntities())
                {
                    items = ent.TeamBg_GetAllMatches(teamBgId).ToList();
                }
                return items;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all matches for bg team {teamBgId}", ex);
            }
        }
        public void MergeBgTeamsWatcher(List<BgTeamAnalysis> bgMatches)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    var bgFeedProc = new BgTeamsWatcher_MergeStoredProcedure();
                    bgFeedProc.Collection = bgMatches.ConvertAll(m => new BgTeamsWatcherUDT()
                    {
                        TeamId = m.TeamId,
                        TotalShows = m.TotalShows,
                        MissingOdds = m.MissingOdds,
                        MissingResult = m.MissingResult
                    });
                    ent.Database.ExecuteStoredProcedure<BgTeamsWatcherUDT>(bgFeedProc);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg teams watcher", ex);
            }
        }
        public List<BgTeams_Watcher_Get_Result> GetBgTeamsWatcher(BgSport sport)
        {
            try
            {
                List<BgTeams_Watcher_Get_Result> result = null;
                using (var ent = new BgDataEntities())
                {
                    result = ent.BgTeams_Watcher_Get((byte)sport).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get bg teams watcher for sport {sport}", ex);
            }
        }
        public void DeleteBgTeamWatcher(int teamId)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.BgTeams_Watcher_Delete(teamId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete bg team {teamId} watcher", ex);
            }
        }
        #endregion

        #region BgTeams BlackFilter
        public void MergeBgTeamsBlackFilter(int teamId, BgMapperType type, int bgIdToFilter)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.BgTeams_BlackFilter_Merge(teamId, (byte)type, bgIdToFilter);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg teams black filter for team {teamId}, {type} filterId {bgIdToFilter}", ex);
            }
        }
        public void DeleteBgTeamsBlackFilter(int teamId, BgMapperType type)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.BgTeams_BlackFilter_Delete(teamId, (byte)type);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete bg teams black filter for team {teamId}, {type}", ex);
            }
        }
        public List<BgTeams_BlackFilter_GetAll_Result> GetAllBgTeamsBlackFilters()
        {
            try
            {
                List<BgTeams_BlackFilter_GetAll_Result> result = null;
                using (var ent = new BgDataEntities())
                {
                    result = ent.BgTeams_BlackFilter_GetAll().ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all bg teams black filters", ex);
            }
        }

        #endregion

        #region BgFeed Statistics Merge
        public void MergeBgFeedStatistics(List<BgMatchStatistics> bgMatchesStatistics)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgFeedStatisticsProc = new FeedBgStatisticsMergeStoredProcedure();
                            bgFeedStatisticsProc.Collection = bgMatchesStatistics.ConvertAll(m => new FeedBgStatisticsUDT()
                            {
                                MatchId = m.MatchId,
                                StatisticIndex = m.StatisticIndex,
                                FullTimeHomeWin = m.FullTimeHomeWin,
                                FullTimeDraw = m.FullTimeDraw,
                                FullTimeAwayWin = m.FullTimeAwayWin,
                                FullTimeHomeWinH2H = m.FullTimeHomeWinH2H,
                                FullTimeDrawH2H = m.FullTimeDrawH2H,
                                FullTimeAwayWinH2H = m.FullTimeAwayWinH2H,
                                Over3_5 = m.Over3_5,
                                Under3_5 = m.Under3_5,
                                Over3_5H2H = m.Over3_5H2H,
                                Under3_5H2H = m.Under3_5H2H,
                                Over2_5 = m.Over2_5,
                                Under2_5 = m.Under2_5,
                                Over2_5H2H = m.Over2_5H2H,
                                Under2_5H2H = m.Under2_5H2H,
                                Over1_5 = m.Over1_5,
                                Under1_5 = m.Under1_5,
                                Over1_5H2H = m.Over1_5H2H,
                                Under1_5H2H = m.Under1_5H2H,
                                Range0_1 = m.Range0_1,
                                Range2_3 = m.Range2_3,
                                Range4Plus = m.Range4Plus,
                                Range0_1H2H = m.Range0_1H2H,
                                Range2_3H2H = m.Range2_3H2H,
                                Range4PlusH2H = m.Range4PlusH2H,
                                BothScore = m.BothScore,
                                NoBothScore = m.NoBothScore,
                                BothScoreH2H = m.BothScoreH2H,
                                NoBothScoreH2H = m.NoBothScoreH2H,
                                HomeDraw = m.HomeDraw,
                                NoDraw = m.NoDraw,
                                DrawAway = m.DrawAway,
                                HomeDrawH2H = m.HomeDrawH2H,
                                NoDrawH2H = m.NoDrawH2H,
                                DrawAwayH2H = m.DrawAwayH2H,
                                HomePower = m.HomePower,
                                AwayPower = m.AwayPower,
                                LastUpdate = m.LastUpdate
                            });
                            ent.Database.ExecuteStoredProcedure<FeedBgStatisticsUDT>(bgFeedStatisticsProc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge bg feed statistics, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg feed statistics", ex);
            }
        }

        #endregion

        #region BgFeed Statistics Details Merge
        public void MergeBgFeedStatisticsDetails(List<BgMatchStatisticsDetails> bgMatchesStatistics)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgFeedStatisticsDetailsProc = new FeedBgStatisticsDetailsMergeStoredProcedure();
                            bgFeedStatisticsDetailsProc.Collection = bgMatchesStatistics.ConvertAll(m => new FeedBgStatisticsDetailsUDT()
                            {
                                MatchId = m.MatchId,
                                Type = (byte)m.Type,
                                StatisticIndex = (byte)m.StatisticIndex,
                                Population = (byte)m.Population,
                                Lines= (int?)m.Lines,
                                Percent = (byte?)m.Percent,
                                LastUpdate = m.LastUpdate
                            });
                            ent.Database.ExecuteStoredProcedure<FeedBgStatisticsDetailsUDT>(bgFeedStatisticsDetailsProc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge bg feed statistics details, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg feed statistics", ex);
            }
        }

        #endregion
        #region MergeBgFeedTips
        public void MergeBgFeedTips(List<BgMatchTypedTip> tips)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgProc = new FeedBgTipsMergeStoredProcedure();
                            bgProc.Collection = tips.ConvertAll<FeedBgTipsUDT>(t => new FeedBgTipsUDT()
                            {
                                MatchId = t.MatchId,
                                Type = (byte)t.Type,
                                Title = t.Title,
                                Value = t.Value,
                                Stars = (byte)t.Stars,
                                Descriptions = string.Join("~", t.Descriptions),
                                FreezeTip = t.FreezeTip,
                                LastUpdate = t.LastUpdate
                            });

                            ent.Database.ExecuteStoredProcedure<FeedBgTipsUDT>(bgProc);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge bg feed tips, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg feed tips", ex);
            }
        }
        #endregion

        #region GetBgFeedStatistics
        public List<Feed_Bg_Statistics> GetBgFeedStatistics(List<long> bgMatchIds)
        {
            try
            {
                List<Feed_Bg_Statistics> result = null;
                using (var ent = new BgDataEntities())
                {
                    result = ent.Feed_Bg_Statistics.Where(i => bgMatchIds.Contains(i.MatchId)).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get bg feed statitsics", ex);
            }
        }
        #endregion
        #region GetBgFeedStatisticsDetails
        public List<Feed_Bg_StatisticsDetails> GetBgFeedStatisticsDetails(List<long> bgMatchIds)
        {
            try
            {
                List<Feed_Bg_StatisticsDetails> result = null;
                using (var ent = new BgDataEntities())
                {
                    result = ent.Feed_Bg_StatisticsDetails.Where(i => bgMatchIds.Contains(i.MatchId)).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get bg feed statitsics details", ex);
            }
        }
        #endregion

        #region GetBgFeedInfo
        public List<Feed_Bg_Info> GetBgFeedInfo(List<long> bgMatchIds)
        {
            try
            {
                List<Feed_Bg_Info> result = null;
                using (var ent = new BgDataEntities())
                {
                    result = ent.Feed_Bg_Info.Where(i => bgMatchIds.Contains(i.MatchId)).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get bg feed info", ex);
            }
        }
        #endregion
        #region BgFeed Info Merge
        public void MergeBgFeedInfo(List<BgMatchInfo> bgMatchesStatistics)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var bgFeedInfoProc = new FeedBgInfoMergeStoredProcedure();
                            bgFeedInfoProc.Collection = bgMatchesStatistics.ConvertAll(m => new FeedBgInfoUDT()
                            {
                                MatchId = m.MatchId,
                                BadMatches = m.BadMatches,
                                HomeMatches = m.HomeMatches,
                                AwayMatches = m.AwayMatches,
                                Head2HeadMatches = m.Head2HeadMatches,
                                LastUpdate = m.LastUpdate
                            });
                            ent.Database.ExecuteStoredProcedure<FeedBgInfoUDT>(bgFeedInfoProc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge bg feed info, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg feed info", ex);
            }
        }

        #endregion
        #region BgLeagues Whitelist
        public void MergeBgLeaguesWhitelist(int leagueId)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.BgLeagues_Whitelist_Merge(leagueId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg leagues whitelist for league {leagueId}", ex);
            }
        }
        public void DeleteBgLeaguesWhitelist(int leagueId)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.BgLeagues_Whitelist_Delete(leagueId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete bg leagues whitelist, leagueId {leagueId}", ex);
            }
        }
        public List<int> GetAllBgLeaguesWhitelist()
        {
            try
            {
                List<int> result = null;
                using (var ent = new BgDataEntities())
                {
                    result = ent.BgLeagues_Whitelist_GetAll()
                        .Where(l => l.HasValue)
                        .Select(l => l.Value).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all bg teams black filters", ex);
            }
        }

        #endregion

        #region GetMatchOdds
        public List<Feed_Goalserve_Odds_Fetch_Result> GetMatchOdds(int goalsrveSportId, int goalserveHomeId, int goalserveAwayId, DateTime gameStart)
        {
            try
            {
                List<Feed_Goalserve_Odds_Fetch_Result> allOdds = null;
                using (var ent = new BgDataEntities())
                {
                    allOdds = ent.Feed_Goalserve_Odds_Fetch(goalsrveSportId, goalserveHomeId, goalserveAwayId, gameStart)
                        .ToList();
                }
                return allOdds;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to gett match odds for goalsrveSportId={goalsrveSportId}, goalserveHomeId={goalserveHomeId}, goalserveAwayId={goalserveAwayId}, gameStart={gameStart} ", ex);
            }
        }
        #endregion
    }
}
