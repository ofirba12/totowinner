﻿using EntityFrameworkExtras.EF6;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services.Persistance.BgData;

namespace TotoWinner.Services
{
    public class BgDataWinnerPersistanceServices : Singleton<BgDataWinnerPersistanceServices>, IBgDataWinnerPersistanceServices
    {
        private BgDataWinnerPersistanceServices() { }

        public void WinnerFeedMerge(List<BgWinnerFeed> feed)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var proc = new FeedWinnerMergeStoredProcedure();
                            var lastUpdate = DateTime.Now;
                            proc.Collection = feed.ConvertAll(f => new FeedWinnerUDT()
                            {
                                EventId = f.EventId,
                                ProgramId = f.ProgramId,
                                SportId = (byte)f.SportId,
                                TotoId = f.TotoId,
                                HomeId = f.HomeId,
                                GuestId = f.GuestId,
                                BetTypeId = (short)f.BetTypeId,
                                Place = f.Place,
                                Name = f.Name,
                                GameStart = f.GameStart,
                                Status = f.Status,
                                HomeOdd = f.HomeOdd,
                                DrawOdd = f.DrawOdd,
                                AwayOdd = f.AwayOdd,
                                Country = f.Country,
                                League = f.League,
                                LastUpdate = lastUpdate
                            });
                            ent.Database.ExecuteStoredProcedure<FeedBgUDT>(proc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to merge winner feed, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to bulk merge winner feed", ex);
            }
        }
        public void MergeMappers(List<BgWinnerMapper> mapper)
        {
            try
            {
                var collection = mapper.ConvertAll<BgWinnerMapperUDT>(m => new BgWinnerMapperUDT()
                {
                    SportId = (byte)m.Sport,
                    WinnerId = m.WinnerId,
                    WinnerName = m.WinnerName
                }
                );
                using (var ent = new BgDataEntities())
                {
                    var bgWinnerMergeProc = new BgWinnerMapperMergeStoredProcedure();
                    bgWinnerMergeProc.MapperParameter = collection;
                    ent.Database.ExecuteStoredProcedure<BgWinnerMapperUDT>(bgWinnerMergeProc);

                    //var bgWinnerGoalserverMergeProc = new WinnerGoalserveMapperMergeStoredProcedure();
                    //bgWinnerGoalserverMergeProc.MapperParameter = collection;
                    //ent.Database.ExecuteStoredProcedure<BgWinnerMapperUDT>(bgWinnerGoalserverMergeProc);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to bulk merge winner mapper", ex);
            }
        }
        public void UpdateWinnerMapperBgId(BgWinnerMapper item)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.MapperWinner_UpdateBgId((byte)item.Sport, item.WinnerId, item.BgId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update winner mapper with BgId {item.ToString()}", ex);
            }
        }
        public void UpdateWinnerMapperGoalserveId(BgWinnerMapper item)
        {
            try
            {
                using (var ent = new BgDataEntities())
                {
                    ent.MapperWinner_UpdateGoalserveId((byte)item.Sport, item.WinnerId, item.GoalserveId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update winner mapper with GoalserveId {item.ToString()}", ex);
            }
        }
        public List<BgWinnerMapper> GetWinnerMapper()
        {
            try
            {
                var mapper = new List<BgWinnerMapper>();
                using (var ent = new BgDataEntities())
                {
                    mapper = ent.BgWinnerMapper_Fetch()
                        .ToList()
                        .ConvertAll<BgWinnerMapper>(m => new BgWinnerMapper((SportIds)m.SportId, m.WinnerId, m.WinnerName, m.BgId, m.GoalserveId));
                }
                return mapper;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get winner mapper", ex);
            }
        }
        public List<FeedWinner_Fetch_Result> FetchWinnerFeed(SportIds sport, int daysFromToday)
        {
            try
            {
                List<FeedWinner_Fetch_Result> items = null;
                using (var ent = new BgDataEntities())
                {
                    items = ent.FeedWinner_Fetch((int)sport, daysFromToday).ToList();
                }
                return items;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch winner data for sport={sport} daysFromToday={daysFromToday}", ex);
            }
        }
        public FeedWinner_FetchLastTeamGame_Result FetchLastTeamGame(int teamId)
        {
            try
            {
                FeedWinner_FetchLastTeamGame_Result item = null;
                using (var ent = new BgDataEntities())
                {
                    item = ent.FeedWinner_FetchLastTeamGame(teamId).FirstOrDefault();
                }
                return item;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch last game for teamId={teamId} from winner feed", ex);
            }
        }
    }
}
