﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBg_Quarters_Merge")]
    public class FeedBgQuartersMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<FeedBgBasketballQuartersUDT> Collection { get; set; }
    }
}
