﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("BgTeams_Watcher_Merge")]
    public class BgTeamsWatcher_MergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgTeamsWatcherUDT> Collection { get; set; }
    }
}
