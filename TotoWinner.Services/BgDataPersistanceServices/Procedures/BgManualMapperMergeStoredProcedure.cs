﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("BgManualMapperMerge")]
    public class BgManualMapperMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgManualMapperUDT> MapperParameter { get; set; }
    }
}
