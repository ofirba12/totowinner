﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBgStatisticsDetails_Merge")]
    public class FeedBgStatisticsDetailsMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<FeedBgStatisticsDetailsUDT> Collection { get; set; }
    }
}
