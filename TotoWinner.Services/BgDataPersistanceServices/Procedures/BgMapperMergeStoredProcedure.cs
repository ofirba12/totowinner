﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("MapperMerge")]
    public class BgMapperMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgMapperUDT> MapperParameter { get; set; }
    }
}
