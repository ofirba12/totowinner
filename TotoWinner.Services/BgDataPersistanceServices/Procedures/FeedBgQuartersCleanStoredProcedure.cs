﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBg_Quarters_Clean")]
    public class FeedBgQuartersCleanStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgMatchIdsUDT> Collection { get; set; }
    }
}
