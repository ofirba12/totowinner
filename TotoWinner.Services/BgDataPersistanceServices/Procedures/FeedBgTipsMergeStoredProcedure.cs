﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBgTips_Merge")]
    public class FeedBgTipsMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<FeedBgTipsUDT> Collection { get; set; }
    }
}
