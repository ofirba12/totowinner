﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("BgGoalserveMapperMerge")]
    public class BgGoalserveMapperMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgGoalserveMapperUDT> MapperParameter { get; set; }
    }
}
