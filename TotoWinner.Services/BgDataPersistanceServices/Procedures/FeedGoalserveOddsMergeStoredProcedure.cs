﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("Feed_Goalserve_Odds_Merge")]
    public class FeedGoalserveOddsMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<FeedGoalserveOddsUDT> Collection { get; set; }
    }
}
