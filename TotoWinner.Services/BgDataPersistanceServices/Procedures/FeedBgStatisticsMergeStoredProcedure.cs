﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBgStatistics_Merge")]
    public class FeedBgStatisticsMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<FeedBgStatisticsUDT> Collection { get; set; }
    }
}
