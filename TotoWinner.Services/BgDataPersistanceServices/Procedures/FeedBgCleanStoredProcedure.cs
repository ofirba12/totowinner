﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBg_Clean")]
    public class FeedBgCleanStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgMatchIdsUDT> Collection { get; set; }
    }
}
