﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("FeedBgInfo_Merge")]
    public class FeedBgInfoMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<FeedBgInfoUDT> Collection { get; set; }
    }
}
