﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("WinnerGoalserveMapperMerge")]
    public class WinnerGoalserveMapperMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<BgWinnerMapperUDT> MapperParameter { get; set; }
    }
}
