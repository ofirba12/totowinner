﻿using System;
using System.Collections.Generic;
using TotoWinner.Data.BgBettingData.Goalserve;
using TotoWinner.Services.Persistance.BgData;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    internal interface IBgDataPersistanceServices
    {
        List<Feed_Goalserve_Fetch_Result> GoalserveFeedGet(int sportId, int daysBack);
        void GoalserveFeedInsert(List<GoalserveMatch> matches);
        void GoalserveFeedUpdate(List<GoalserveMatch> matches);
        List<Feed_Goalserve_Fetch_Result> GoalserveMatchesOnlyGet(int sportId, List<int> matchIds);
        List<BgMapperItem> GetBgMappers();
        void MergeMappers(List<BgMapper> bgMappers, List<BgGoalserveMapper> bgGoalserveMapper);
        void UpdateMapperBgName(int bgId, string bgName);
        List<Feed_Goalserve_Odds_Fetch_Result> GetMatchOdds(int goalsrveSportId, int goalserveHomeId, int goalserveAwayId, DateTime gameStart);
    }
}
