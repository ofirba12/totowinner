﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public interface IStatisticalDataServices
    {
        RelationsData GenerateRelations(ProgramWithGames program);
        CalculatedProgramStatistics Generate(ProgramWithGames program);
        char[] GetMappedLettersRow(decimal[] currentRow);
    }
}
