﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Services.Persistance;

namespace TotoWinner.Services
{
    public class StatisticalDataServices : Singleton<StatisticalDataServices>, IStatisticalDataServices
    {
        private PersistanceServices _persistanceSrv = PersistanceServices.Instance;
        private ProgramServices _programSrv = ProgramServices.Instance;
        private static char A = 'A';
        private static char B = 'B';
        private static char C = 'C';

        private static string[] _letters = { "A", "B", "C", "AB", "AC", "BC" };
        private List<int> _shapes2;
        private List<int> _shapes3;
        private List<string> _lettersShapes2;
        private List<string> _lettersShapes3;

        private StatisticalDataServices()
        {
            _shapes2 = new List<int> { 0, 1, 2, 10, 11, 12, 20, 21, 22 };
            _lettersShapes2 = new List<string> { "AA", "AB", "AC", "BA", "BB", "BC", "CA", "CB", "CC" };
            _shapes3 = new List<int> { 0, 1, 2, 10, 11, 12, 20, 21, 22, 100, 101, 102, 110, 111, 112, 120, 121, 122, 200, 201, 202, 210, 211, 212, 220, 221, 222 };
            _lettersShapes3 = new List<string> { "AAA", "AAB", "AAC", "ABA", "ABB", "ABC", "ACA", "ACB", "ACC", "BAA", "BAB", "BAC", "BBA", "BBB", "BBC", "BCA", "BCB", "BCC", "CAA", "CAB", "CAC", "CBA", "CBB", "CBC", "CCA", "CCB", "CCC" };
        }
        public RelationsData GenerateRelations(ProgramWithGames program)
        {
            var relations = new RelationsData();
            for (int rowIndex = 0; rowIndex < program.Games.Count; rowIndex++)
            {
                var currentRow = new[]
                {
                        (decimal)program.Games[rowIndex+1].Rate1,
                        (decimal)program.Games[rowIndex+1].RateX,
                        (decimal)program.Games[rowIndex+1].Rate2
                    };
                AggregateRelations(currentRow, relations, program.Games[rowIndex + 1].Won);
            }
            CalculateRelations(relations);
            return relations;
        }
        public CalculatedProgramStatistics Generate(ProgramWithGames program)
        {
            try
            {
                var gamesFinalResults = program.Games.OrderBy(g => g.Value.Index).ToDictionary(x => x.Key, y =>
                {
                    int won = 0;
                    switch (y.Value.Won)
                    {
                        case "1":
                            won = 0;
                            break;
                        case "X":
                            won = 1;
                            break;
                        case "2":
                            won = 2;
                            break;
                    }
                    return won;
                });
                var options = gamesFinalResults.Values.ToList<int>();
                var lettersOptions = new List<char>();
                var relations = GenerateRelations(program);
                for (int rowIndex = 0; rowIndex < program.Games.Count; rowIndex++)
                {
                    var currentRow = new[]
                    {
                        (decimal)program.Games[rowIndex+1].Rate1,
                        (decimal)program.Games[rowIndex+1].RateX,
                        (decimal)program.Games[rowIndex+1].Rate2
                    };
                    char[] mappedRow = this.GetMappedLettersRow(currentRow);
                    lettersOptions.Add(mappedRow[options[rowIndex]]);
                }
                
                var amount1X2 = new BasicData3()
                {
                    Amount1 = options.Count(o => o == 0),
                    Amount2 = options.Count(o => o == 1),
                    Amount3 = options.Count(o => o == 2)
                };
                var amountABC = new BasicData3()
                {
                    Amount1 = lettersOptions.Count(o => o == 'A'),
                    Amount2 = lettersOptions.Count(o => o == 'B'),
                    Amount3 = lettersOptions.Count(o => o == 'C')
                };
                var sequenceLength1X2 = new BasicData3()
                {
                    Amount1 = CountSequenceLength(options, 0),
                    Amount2 = CountSequenceLength(options, 1),
                    Amount3 = CountSequenceLength(options, 2)
                };
                var sequenceLengthABC = new BasicData3()
                {
                    Amount1 = CountSequenceLength(lettersOptions, 'A'),
                    Amount2 = CountSequenceLength(lettersOptions, 'B'),
                    Amount3 = CountSequenceLength(lettersOptions, 'C')
                };
                var break1X2 = new BasicData1()
                {
                    Amount = CountBreaks(options)
                };
                var breakABC = new BasicData1()
                {
                    Amount = CountBreaks(lettersOptions)
                };
                var shapes2Data1X2 = new Shapes2Data1X2();
                var shapeId = 1;
                Dictionary<int, int> shapes2Counters = CountShape2(options);
                foreach (int shape2 in _shapes2)
                {
                    var value = shapes2Counters[shape2];
                    switch (shapeId)
                    {
                        case 1:
                            shapes2Data1X2.Amount11 = value;
                            break;
                        case 2:
                            shapes2Data1X2.Amount1X = value;
                            break;
                        case 3:
                            shapes2Data1X2.Amount12 = value;
                            break;
                        case 4:
                            shapes2Data1X2.AmountX1 = value;
                            break;
                        case 5:
                            shapes2Data1X2.AmountXX = value;
                            break;
                        case 6:
                            shapes2Data1X2.AmountX2 = value;
                            break;
                        case 7:
                            shapes2Data1X2.Amount21 = value;
                            break;
                        case 8:
                            shapes2Data1X2.Amount2X = value;
                            break;
                        case 9:
                            shapes2Data1X2.Amount22 = value;
                            break;
                    }
                    shapeId++;
                }

                var shapes2DataABC = new Shapes2DataABC();
                shapeId = 1;
                Dictionary<string, int> lettersShapes2Counters = CountLettersShape2(lettersOptions);
                foreach (string shape2 in _lettersShapes2)
                {
                    var value = lettersShapes2Counters[shape2];
                    switch (shapeId)
                    {
                        case 1:
                            shapes2DataABC.AmountAA = value;
                            break;
                        case 2:
                            shapes2DataABC.AmountAB = value;
                            break;
                        case 3:
                            shapes2DataABC.AmountAC = value;
                            break;
                        case 4:
                            shapes2DataABC.AmountBA = value;
                            break;
                        case 5:
                            shapes2DataABC.AmountBB = value;
                            break;
                        case 6:
                            shapes2DataABC.AmountBC = value;
                            break;
                        case 7:
                            shapes2DataABC.AmountCA = value;
                            break;
                        case 8:
                            shapes2DataABC.AmountCB = value;
                            break;
                        case 9:
                            shapes2DataABC.AmountCC = value;
                            break;
                    }
                    shapeId++;
                }
                var shapesGroups2Data1X2 = new GroupsBasicData();
                shapesGroups2Data1X2.Amount0 = CountShapeGroup(options, CountShape2, 0);
                shapesGroups2Data1X2.Amount1 = CountShapeGroup(options, CountShape2, 1);
                shapesGroups2Data1X2.Amount2 = CountShapeGroup(options, CountShape2, 2);
                shapesGroups2Data1X2.Amount3Plus = CountShapeGroup(options, CountShape2, 3);

                var shapesGroups2DataABC = new GroupsBasicData();
                shapesGroups2DataABC.Amount0 = CountShapeGroup(lettersOptions, CountLettersShape2, 0);
                shapesGroups2DataABC.Amount1 = CountShapeGroup(lettersOptions, CountLettersShape2, 1);
                shapesGroups2DataABC.Amount2 = CountShapeGroup(lettersOptions, CountLettersShape2, 2);
                shapesGroups2DataABC.Amount3Plus = CountShapeGroup(lettersOptions, CountLettersShape2, 3);

                var shapes3Data1X2 = new Shapes3Data1X2();
                shapeId = 1;
                Dictionary<int, int> shapes3Counters = CountShape3(options);
                foreach (int shape3 in _shapes3)
                {
                    var value = shapes3Counters[shape3];
                    switch (shapeId)
                    {
                        case 1:
                            shapes3Data1X2.Amount1.Amount11 = value;
                            break;
                        case 2:
                            shapes3Data1X2.Amount1.Amount1X = value;
                            break;
                        case 3:
                            shapes3Data1X2.Amount1.Amount12 = value;
                            break;
                        case 4:
                            shapes3Data1X2.Amount1.AmountX1 = value;
                            break;
                        case 5:
                            shapes3Data1X2.Amount1.AmountXX = value;
                            break;
                        case 6:
                            shapes3Data1X2.Amount1.AmountX2 = value;
                            break;
                        case 7:
                            shapes3Data1X2.Amount1.Amount21 = value;
                            break;
                        case 8:
                            shapes3Data1X2.Amount1.Amount2X = value;
                            break;
                        case 9:
                            shapes3Data1X2.Amount1.Amount22 = value;
                            break;
                        case 10:
                            shapes3Data1X2.AmountX.Amount11 = value;
                            break;
                        case 11:
                            shapes3Data1X2.AmountX.Amount1X = value;
                            break;
                        case 12:
                            shapes3Data1X2.AmountX.Amount12 = value;
                            break;
                        case 13:
                            shapes3Data1X2.AmountX.AmountX1 = value;
                            break;
                        case 14:
                            shapes3Data1X2.AmountX.AmountXX = value;
                            break;
                        case 15:
                            shapes3Data1X2.AmountX.AmountX2 = value;
                            break;
                        case 16:
                            shapes3Data1X2.AmountX.Amount21 = value;
                            break;
                        case 17:
                            shapes3Data1X2.AmountX.Amount2X = value;
                            break;
                        case 18:
                            shapes3Data1X2.AmountX.Amount22 = value;
                            break;
                        case 19:
                            shapes3Data1X2.Amount2.Amount11 = value;
                            break;
                        case 20:
                            shapes3Data1X2.Amount2.Amount1X = value;
                            break;
                        case 21:
                            shapes3Data1X2.Amount2.Amount12 = value;
                            break;
                        case 22:
                            shapes3Data1X2.Amount2.AmountX1 = value;
                            break;
                        case 23:
                            shapes3Data1X2.Amount2.AmountXX = value;
                            break;
                        case 24:
                            shapes3Data1X2.Amount2.AmountX2 = value;
                            break;
                        case 25:
                            shapes3Data1X2.Amount2.Amount21 = value;
                            break;
                        case 26:
                            shapes3Data1X2.Amount2.Amount2X = value;
                            break;
                        case 27:
                            shapes3Data1X2.Amount2.Amount22 = value;
                            break;
                    }
                    shapeId++;
                }

                var shapes3DataABC = new Shapes3DataABC();
                shapeId = 1;
                Dictionary<string, int> lettersShapes3Counters = CountLettersShape3(lettersOptions);
                foreach (string shape3 in _lettersShapes3)
                {
                    var value = lettersShapes3Counters[shape3];
                    switch (shapeId)
                    {
                        case 1:
                            shapes3DataABC.AmountA.AmountAA = value;
                            break;
                        case 2:
                            shapes3DataABC.AmountA.AmountAB = value;
                            break;
                        case 3:
                            shapes3DataABC.AmountA.AmountAC = value;
                            break;
                        case 4:
                            shapes3DataABC.AmountA.AmountBA = value;
                            break;
                        case 5:
                            shapes3DataABC.AmountA.AmountBB = value;
                            break;
                        case 6:
                            shapes3DataABC.AmountA.AmountBC = value;
                            break;
                        case 7:
                            shapes3DataABC.AmountA.AmountCA = value;
                            break;
                        case 8:
                            shapes3DataABC.AmountA.AmountCB = value;
                            break;
                        case 9:
                            shapes3DataABC.AmountA.AmountCC = value;
                            break;
                        case 10:
                            shapes3DataABC.AmountB.AmountAA = value;
                            break;
                        case 11:
                            shapes3DataABC.AmountB.AmountAB = value;
                            break;
                        case 12:
                            shapes3DataABC.AmountB.AmountAC = value;
                            break;
                        case 13:
                            shapes3DataABC.AmountB.AmountBA = value;
                            break;
                        case 14:
                            shapes3DataABC.AmountB.AmountBB = value;
                            break;
                        case 15:
                            shapes3DataABC.AmountB.AmountBC = value;
                            break;
                        case 16:
                            shapes3DataABC.AmountB.AmountCA = value;
                            break;
                        case 17:
                            shapes3DataABC.AmountB.AmountCB = value;
                            break;
                        case 18:
                            shapes3DataABC.AmountB.AmountCC = value;
                            break;
                        case 19:
                            shapes3DataABC.AmountC.AmountAA = value;
                            break;
                        case 20:
                            shapes3DataABC.AmountC.AmountAB = value;
                            break;
                        case 21:
                            shapes3DataABC.AmountC.AmountAC = value;
                            break;
                        case 22:
                            shapes3DataABC.AmountC.AmountBA = value;
                            break;
                        case 23:
                            shapes3DataABC.AmountC.AmountBB = value;
                            break;
                        case 24:
                            shapes3DataABC.AmountC.AmountBC = value;
                            break;
                        case 25:
                            shapes3DataABC.AmountC.AmountCA = value;
                            break;
                        case 26:
                            shapes3DataABC.AmountC.AmountCB = value;
                            break;
                        case 27:
                            shapes3DataABC.AmountC.AmountCC = value;
                            break;
                    }
                    shapeId++;
                }
                var shapesGroups3Data1X2 = new GroupsBasicData();
                shapesGroups3Data1X2.Amount0 = CountShapeGroup(options, CountShape3, 0);
                shapesGroups3Data1X2.Amount1 = CountShapeGroup(options, CountShape3, 1);
                shapesGroups3Data1X2.Amount2 = CountShapeGroup(options, CountShape3, 2);
                shapesGroups3Data1X2.Amount3Plus = CountShapeGroup(options, CountShape3, 3);

                var shapesGroups3DataABC = new GroupsBasicData();
                shapesGroups3DataABC.Amount0 = CountShapeGroup(lettersOptions, CountLettersShape3, 0);
                shapesGroups3DataABC.Amount1 = CountShapeGroup(lettersOptions, CountLettersShape3, 1);
                shapesGroups3DataABC.Amount2 = CountShapeGroup(lettersOptions, CountLettersShape3, 2);
                shapesGroups3DataABC.Amount3Plus = CountShapeGroup(lettersOptions, CountLettersShape3, 3);

                var result = new CalculatedProgramStatistics(program,
                    options, 
                    lettersOptions, 
                    amount1X2, 
                    amountABC,
                    sequenceLength1X2,
                    sequenceLengthABC,
                    break1X2,
                    breakABC,
                    shapes2Data1X2,
                    shapesGroups2Data1X2,
                    shapes2DataABC,
                    shapesGroups2DataABC,
                    shapes3Data1X2,
                    shapesGroups3Data1X2,
                    shapes3DataABC,
                    shapesGroups3DataABC,
                    relations
                    );

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to generate historical data to program: {program.Number}, {program.EndDate}, id={program.Id}", ex);
            }
        }

        public char[] GetMappedLettersRow(decimal[] currentRow)
        {
            decimal min = currentRow.Min();
            decimal max = currentRow.Max();

            if (min == max) return new[] { A, B, C };

            decimal one = currentRow[0], x = currentRow[1], two = currentRow[2];

            if (one != x && one != two && x != two)
            {
                char[] mappedRow = new char[3];

                decimal[] minmaxmid = { one, x, two };
                minmaxmid = minmaxmid.OrderBy(k => k).ToArray();

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (minmaxmid[i] == currentRow[j])
                        {
                            mappedRow[j] = _letters[i][0];
                            break;
                        }
                    }
                }
                return mappedRow;
            }

            if (one == two)
            {
                if (one > x)
                {
                    return new[] { B, A, C };
                }

                if (one < x)
                {
                    return new[] { A, C, B };
                }
            }

            if (two > one) return new[] { A, B, C };

            return new[] { C, B, A };
        }
        #region Data Helper

        private void AggregateRelations(decimal[] currentRow, RelationsData relations, string won)
        {
            relations.Min += currentRow.Min();
            relations.Max += currentRow.Max();
            var sum = currentRow.Sum();
            relations.Middle += Decimal.Round(Decimal.Divide(sum,3),2);
            relations.Home += currentRow[0];
            relations.Draft += currentRow[1];
            relations.Away += currentRow[2];
            relations.Sum += sum;
            if (won.Contains('1'))
                relations.FinalResult += currentRow[0];
            else if (won.Contains('X'))
                relations.FinalResult += currentRow[1];
            else if (won.Contains('2'))
                relations.FinalResult += currentRow[2];
        }
        private void CalculateRelations(RelationsData relations)
        {
            relations.DeltaMin = relations.FinalResult - relations.Min;
            relations.DeltaMiddle = relations.FinalResult - relations.Middle;
            relations.DeltaMax = relations.FinalResult - relations.Max;
            relations.DeltaHome = relations.FinalResult - relations.Home;
            relations.DeltaDraft = relations.FinalResult - relations.Draft;
            relations.DeltaAway = relations.FinalResult - relations.Away;
        }

        public int CountSequenceLength(List<int> list, int what)
        {
            int maxSeq = 0;
            int sequanceCounter = 0;
            foreach (int item in list)
            {
                if (item == what)
                {
                    sequanceCounter++;
                    maxSeq = Math.Max(maxSeq, sequanceCounter);
                }
                else
                {
                    if (sequanceCounter > 0)
                    {
                        sequanceCounter = 0;
                    }
                }
            }
            return maxSeq;
        }

        public int CountSequenceLength(List<char> list, int what)
        {
            return CountSequenceLength(list.Select(i => (int)i).ToList(), what);
        }

        private int CountBreaks(List<int> list)
        {
            int breaksCounter = 0;
            for (int i = 0; i < list.Count - 1; i++)
            {
                if (list[i] != list[i + 1]) breaksCounter++;
            }
            return breaksCounter;
        }

        private int CountBreaks(List<char> list)
        {
            return CountBreaks(list.Select(i => (int)i).ToList());
        }

        private Dictionary<string, int> CountLettersShape2(List<char> lettersOptions)
        {
            Dictionary<string, int> counters = new Dictionary<string, int>();
            foreach (string cId in _lettersShapes2) counters[cId] = 0;
            for (int i = 0; i < lettersOptions.Count - 1; i++)
            {
                string key = string.Format("{0}{1}", lettersOptions[i], lettersOptions[i + 1]);
                counters[key]++;
            }
            return counters;
        }

        private Dictionary<int, int> CountShape2(List<int> options)
        {
            Dictionary<int, int> counters = new Dictionary<int, int>();
            foreach (int cId in _shapes2) counters[cId] = 0;
            for (int i = 0; i < options.Count - 1; i++)
            {
                counters[options[i] * 10 + options[i + 1]]++;
            }
            return counters;
        }

        private Dictionary<string, int> CountLettersShape3(List<char> lettersOptions)
        {
            Dictionary<string, int> counters = new Dictionary<string, int>();
            foreach (string cId in _lettersShapes3) counters[cId] = 0;
            for (int i = 0; i < lettersOptions.Count - 2; i++)
            {
                string key = string.Format("{0}{1}{2}", lettersOptions[i], lettersOptions[i + 1], lettersOptions[i + 2]);
                counters[key]++;
            }
            return counters;
        }

        private Dictionary<int, int> CountShape3(List<int> options)
        {
            Dictionary<int, int> counters = new Dictionary<int, int>();
            foreach (int cId in _shapes3) counters[cId] = 0;
            for (int i = 0; i < options.Count - 2; i++)
            {
                counters[options[i] * 100 + options[i + 1] * 10 + options[i + 2]]++;
            }
            return counters;
        }

        private int CountShapeGroup<T,L>(L list, Func<L, Dictionary<T, int>> countFunc, int what)
        {
            if (what < 3)
                return countFunc(list).Count(s => s.Value == what);
            return countFunc(list).Count(s => s.Value >= 3);
        }


    }
    #endregion
}