﻿using System.Collections.Generic;
using Telegram.Bot;
using TotoWinner.Data;
using TotoWinner.Data.TelegramBot;

namespace TotoWinner.Services
{
    public interface ITelegramBotServices
    {
        List<PushTipTime> GetPushTipTimes();
        List<BotSubsriber> GetSubsribers(bool onlyActive);
        string PrepareTipForPublish(Tip tip);
        BotRegisterResult VerifyAndRegisterSubscriber(string registrationCode, long chatId);
        int InsertSubscriber(BotSubsriber subscriber);
        void UpdateSubscriber(BotSubsriber subscriber);
        void DeleteSubscriber(long? clientChatId, int registrationId);
        BotSubsriber GetSubscriberByRegistarionId(int registrationId);
        BotSubsriber GetSubscriberByChatId(long clientChatId);
        Tip FindTipToPublish(long subscriberId);
        void SentTip(Tip tipToPublish, long subscriberId, TelegramBotClient botClient);
        BotSubsribersStatistics GenerateStatistics();
        void SetButtonStatistics(BotButtonType button);
    }
}
