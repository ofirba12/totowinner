﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BettingTips;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Data;
using Telegram.Bot;

namespace TotoWinner.Services
{
    public partial class TelegramBotServices : Singleton<TelegramBotServices>, ITelegramBotServices
    {
        private TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        protected BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;

        private string TipUrl;
        private TelegramBotServices()
        {
            this.TipUrl = ConfigurationManager.AppSettings["TipUrl"].ToString();
        }
        public int InsertSubscriber(BotSubsriber subscriber)
        {
            var registrationId = _botPersistanceSrv.InsertSubscriber(subscriber.FirstName,
                subscriber.LastName,
                subscriber.CellNumber,
                subscriber.Mail,
                subscriber.Remarks);
            return registrationId;
        }
        public void UpdateSubscriber(BotSubsriber subscriber)
        {
            _botPersistanceSrv.UpdateSubscriber(subscriber.RegistrationId,
                subscriber.ClientChatId.Value,
                subscriber.FirstName,
                subscriber.LastName,
                subscriber.CellNumber,
                subscriber.Mail,
                subscriber.Remarks,
                subscriber.BlockedDate,
                subscriber.BlockageReason);
        }
        public Tip FindTipToPublish(long subscriberId)
        {
            var sentTipsToday = _btPersistanceSrv.GetAllSubscriberTodayAndFutureTipIds(subscriberId);
            var tip = _btTipsSrv.GetNextActiveTips(new BettingTipsFilter(Data.BettingTipFilterType.NoFilter), sentTipsToday);
            return tip;
        }
        public void SentTip(Tip tipToPublish, long subscriberId, TelegramBotClient botClient)
        {
            if (tipToPublish != null)
            {
                var fullTipDescription = PrepareTipForPublish(tipToPublish);
                if (!string.IsNullOrEmpty(fullTipDescription))
                {
                    var urlGuid = Guid.NewGuid();
                    var tipDescription = fullTipDescription.Replace("[#GUID#]", urlGuid.ToString());
                    _botPersistanceSrv.InsertSubscriberTip(urlGuid, tipToPublish.TipId, subscriberId);

                    SendMessage(botClient, subscriberId, tipDescription);
                }
            }
            else
                SendMessage(botClient, subscriberId, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*");
        }
        private async void SendMessage(TelegramBotClient botClient, long chatId, string message)
        {
            Task.Delay(1000).Wait();
            await botClient.SendTextMessageAsync(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }
        public List<PushTipTime> GetPushTipTimes()
        {
            var pushList = _botPersistanceSrv.GetBroadcastSchedules();
            //var pushList = new List<PushTipTime>();
            //for (var h = 9; h < 23; h++)
            //{
            //    pushList.Add(new PushTipTime(h, 0));
            //    //pushList.Add(new PushTipTime(h, 5));
            //    //pushList.Add(new PushTipTime(h, 10));
            //    //pushList.Add(new PushTipTime(h, 15));
            //    //pushList.Add(new PushTipTime(h, 20));
            //    //pushList.Add(new PushTipTime(h, 25));
            //    //pushList.Add(new PushTipTime(h, 30));
            //    //pushList.Add(new PushTipTime(h, 35));
            //    //pushList.Add(new PushTipTime(h, 40));
            //    //pushList.Add(new PushTipTime(h, 45));
            //    //pushList.Add(new PushTipTime(h, 50));
            //    //pushList.Add(new PushTipTime(h, 55));
            //}
            return pushList;
        }

        public List<BotSubsriber> GetSubsribers(bool onlyActive)
        {
            List<TotoWinner.Services.Persistance.BettingTips.Subscriber> response = null;
            if (onlyActive)
                response = _botPersistanceSrv.GetSubscribers().Where(s => s.SubscriberChatId.HasValue && !s.BlockedDate.HasValue).ToList();

            else
                response = _botPersistanceSrv.GetSubscribers();
            return response.Select(s => new BotSubsriber(s.SubscriberChatId,
                s.FirstName,
                s.LastName,
                s.CellNumber,
                s.Mail,
                s.Id,
                s.RegisteredDate,
                s.Remarks,
                s.BlockedDate,
                s.BlockageReason))
                .ToList();
        }

        public void DeleteSubscriber(long? clientChatId, int registrationId)
        {
            if (clientChatId.HasValue)
                _botPersistanceSrv.DeleteSubscriberUrlHistory(clientChatId.Value);
            _botPersistanceSrv.DeleteSubscriber(registrationId);
        }

        public string PrepareTipForPublish(Tip tip)
        {
            var description = string.Empty;
            if (tip != null)
            {
                description = $"{tip.TipTime}{Environment.NewLine}*{tip.BetLeague}*{Environment.NewLine}*{tip.BetName}*{Environment.NewLine}{Environment.NewLine}{tip.TipContent}{Environment.NewLine}*{tip.TipMarks}*{Environment.NewLine}{Environment.NewLine}לצפייה ביחס המעודכן של הטיפ באתר הווינר ולקבלת טיפ נוסף, לחץ כאן>>>>{Environment.NewLine}{this.TipUrl}";
                //Working with hyper link creates alert confirmation to open link (using localhost will not show a hyperlink, need to use domain)
                //var hyperlink = $"[לחץ כאן]({this.TipUrl})";
                //description = $"{tip.TipTime}{Environment.NewLine}*{tip.BetLeague}*{Environment.NewLine}*{tip.BetName}*{Environment.NewLine}{Environment.NewLine}{tip.TipContent}{Environment.NewLine}*{tip.TipMarks}*{Environment.NewLine}{Environment.NewLine}מעבר לטיפ הנוכחית באתר הווינר ולקבלת טיפ נוסף,{Environment.NewLine}{hyperlink}";
            }
            return description;
        }


        public BotRegisterResult VerifyAndRegisterSubscriber(string registrationCode, long chatId)
        {
            try
            {
                var replyMessage = "-תקלה-";
                var isSuccess = false;
                if (int.TryParse(registrationCode, out var registrationId))
                {
                    var validCode = _botPersistanceSrv.VerifyRegistrationCode(registrationId, chatId);
                    if (!validCode)
                    {
                        replyMessage = "*קוד לקוח שהוקש לא ניתן לשיוך.* אנא הקש שוב קוד לקוח שקיבלת";
                    }
                    else
                    {
                        var verifyProcessStatus = _botPersistanceSrv.RegisterSubscriber(registrationId, chatId);
                        if (verifyProcessStatus)
                        {
                            replyMessage = "החיבור עבר בהצלחה.";
                            isSuccess = true;
                        }
                        else
                            replyMessage = "*קוד לקוח שהוקש אינו תקין.* אנא הקש שוב קוד לקוח שקיבלת";
                    }
                }
                else
                {
                    replyMessage = "*קוד לקוח שהוקש אינו חוקי.* אנא הקש קוד לקוח שקיבלת, קוד בין 5 ספרות";
                }

                var response = new BotRegisterResult(registrationCode, replyMessage, isSuccess);
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to register client with code {registrationCode} and chatId {chatId}", ex);
            }
        }

        public BotSubsriber GetSubscriberByRegistarionId(int registrationId)
        {
            var s = _botPersistanceSrv.GetSubscriberByRegistarionId(registrationId);
            var botSubscriber = new BotSubsriber(s.SubscriberChatId,
                s.FirstName,
                s.LastName,
                s.CellNumber,
                s.Mail,
                s.Id,
                s.RegisteredDate,
                s.Remarks,
                s.BlockedDate,
                s.BlockageReason);

            return botSubscriber;
        }
        public BotSubsriber GetSubscriberByChatId(long clientChatId)
        {
            var s = _botPersistanceSrv.GetSubscriberByChatId(clientChatId);
            var botSubscriber = new BotSubsriber(s.SubscriberChatId,
                s.FirstName,
                s.LastName,
                s.CellNumber,
                s.Mail,
                s.Id,
                s.RegisteredDate,
                s.Remarks,
                s.BlockedDate,
                s.BlockageReason);

            return botSubscriber;
        }

        public BotSubsribersStatistics GenerateStatistics()
        {
            var statistics = new Dictionary<long, BotSubsribersStatistic>();
            var subscribers = _botPersistanceSrv.GetSubscribers();
            var urls = _botPersistanceSrv.GetSubscribersUrls();
            foreach (var url in urls)
            {
                var subscriber = subscribers.FirstOrDefault(s => s.SubscriberChatId == url.SubscriberId);
                if (subscriber == null)
                    continue;
                if (!statistics.ContainsKey(url.SubscriberId))
                {
                    statistics.Add(url.SubscriberId, new BotSubsribersStatistic(url.SubscriberId,
                        subscriber.FirstName,
                        subscriber.LastName,
                        subscriber.CellNumber,
                        subscriber.BlockedDate.HasValue ? "חסום" : "פעיל"));
                }
                statistics[url.SubscriberId].PushedUrlsCounter++;
                if (url.VisitedTS.HasValue)
                {
                    statistics[url.SubscriberId].UrlVisitedCounter++;
                    if (statistics[url.SubscriberId].LastActivity < url.VisitedTS.Value)
                    {
                        statistics[url.SubscriberId].LastActivity = url.VisitedTS.Value;
                        var diffResult = DateTime.Now.Subtract(url.VisitedTS.Value).TotalDays;
                        statistics[url.SubscriberId].DaysNotActive = diffResult;
                    }
                }
            }
            var result = new BotSubsribersStatistics(statistics);
            return result;
        }
        public void SetButtonStatistics(BotButtonType button)
        {
            _botPersistanceSrv.SetButtonStatistics(button);
        }
    }
}