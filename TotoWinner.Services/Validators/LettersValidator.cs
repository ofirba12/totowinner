﻿using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class LettersValidator : IValidator
    {
        private readonly Dictionary<int, bool[]> _options;
        private readonly int _min;
        private readonly int _max;

        public LettersValidator(FilterData filterData, Dictionary<byte, char[]> mappedProbability)
        {
            _options = new Dictionary<int, bool[]>();
            for (int i = 0; i < 16; i++) _options[i] = new[] { false, false, false };
            for (byte currInputRowIndex = 0; currInputRowIndex < filterData.FilterDataRow.Length; currInputRowIndex++)
            {
                if (filterData.FilterDataRow[currInputRowIndex] == null) continue;

                char[] currFilterCell = filterData.FilterDataRow[currInputRowIndex].ToCharArray();
                for (byte currColIndex = 0; currColIndex < 3; currColIndex++)
                {
                    _options[currInputRowIndex][currColIndex] = currFilterCell.Contains(mappedProbability[currInputRowIndex][currColIndex]);
                }
            }
            //_min = int.Parse(filterData.FilterMin);
            //_max = int.Parse(filterData.FilterMax);
            _min = Utilities.DecimalStrToInt(filterData.FilterMin);
            _max = Utilities.DecimalStrToInt(filterData.FilterMax);
        }

        public bool CheckResult(Dictionary<byte, byte> result)
        {
            int numOfMatches = 0;
            foreach (byte resultId in result.Keys)
            {
                if (_options[resultId][result[resultId]]) numOfMatches++;
            }
            return numOfMatches >= _min && numOfMatches <= _max;
        }
    }
}
