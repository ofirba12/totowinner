﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Data
{
    public partial class ExcludeConstraintsOptionsValidator : IValidator
    {
        public enum FilterType { Sum, Occur, Seq, Breaks}

        public Dictionary<byte, decimal[]> _probSettins { get; private set; }
        public Dictionary<FilterType, bool[]> _isFilterActive { get; private set; }
        public List<int> _seqLengthEx { get; private set; }
        public decimal[] _minMaxSum { get; private set; }
        public List<int[]> _minMax { get; private set; }
        public int _breaksMin { get; private set; }
        public int _breaksMax { get; private set; }

        public ExcludeConstraintsOptionsValidator(ExcludeConstraintsOptions excludeConstraints, Dictionary<byte, decimal[]> probSettins)
        {
            _isFilterActive = new Dictionary<FilterType, bool[]>();
            _isFilterActive.Add(FilterType.Sum, new bool[] { excludeConstraints.isExSumActive });
            _isFilterActive.Add(FilterType.Occur, new bool[] { excludeConstraints.isExOneActive, excludeConstraints.isExXActive, excludeConstraints.isExTwoActive });
            _isFilterActive.Add(FilterType.Seq, new bool[] { excludeConstraints.isExOneSActive, excludeConstraints.isExXSActive, excludeConstraints.isExTwoSActive });
            _isFilterActive.Add(FilterType.Breaks, new bool[] { excludeConstraints.isExBreakActive });

            _probSettins = probSettins;
            if (_isFilterActive[FilterType.Sum][0])
            _minMaxSum = new decimal[] { decimal.Parse(excludeConstraints.MinMaxSettings[0][0]), decimal.Parse(excludeConstraints.MinMaxSettings[0][1]) };

            _minMax = new List<int[]>();
            _seqLengthEx = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                if (_isFilterActive[FilterType.Occur][i])
                { 
                _minMax.Add(new int[] { int.Parse(excludeConstraints.MinMaxSettings[i + 1][0]), int.Parse(excludeConstraints.MinMaxSettings[i + 1][1]) });
                }
                else
                { 
                    _minMax.Add(new int[] { 0, 0 }); 
                }
                if (_isFilterActive[FilterType.Seq][i])
                {
                    _seqLengthEx.Add(int.Parse(excludeConstraints.SequenciesLengths[i]));
                }
                else
                {
                    _seqLengthEx.Add(0);
                }
            }

            if (_isFilterActive[FilterType.Breaks][0])
            {
                _breaksMin = int.Parse(excludeConstraints.BreakMinMax[0]);
                _breaksMax = int.Parse(excludeConstraints.BreakMinMax[1]);
            }

        }

        public bool CheckResult(Dictionary<byte, byte> result)
        {
            decimal sum = 0;
            int numOfBreaks = 0;
            Dictionary<int, int> counters = new Dictionary<int, int>();
            for (int i = 0; i < 3; i++) counters[i] = 0;
            int prevOption = -1;
            int optionSeqLength = 0;
            foreach (var resultRow in result)
            {
                sum += _probSettins[resultRow.Key][resultRow.Value];
                counters[resultRow.Value]++;

                if (prevOption != -1 && prevOption != resultRow.Value)
                {
                    if (_isFilterActive[FilterType.Seq][prevOption] && _seqLengthEx[prevOption] == optionSeqLength) return false;
                    optionSeqLength = 0;
                    numOfBreaks++;
                }
                prevOption = resultRow.Value;
                optionSeqLength++;
            }
            if (_isFilterActive[FilterType.Seq][prevOption] && _seqLengthEx[prevOption] == optionSeqLength) return false;

            if (_isFilterActive[FilterType.Sum][0] && sum >= _minMaxSum[0] && sum <= _minMaxSum[1]) return false;
            if (_isFilterActive[FilterType.Breaks][0] && numOfBreaks >= _breaksMin && numOfBreaks <= _breaksMax) return false;
            foreach (var counterRow in counters)
            {
                if (_isFilterActive[FilterType.Occur][counterRow.Key] && counterRow.Value >= _minMax[counterRow.Key][0] && counterRow.Key <= _minMax[counterRow.Key][1]) return false;
            }

            return true;
        }

    }
}
