﻿using System.Collections.Generic;
using TotoWinner.Common;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class LettersShapesValidator : IValidator
    {
        private string[] _shapes2 = new string[] { "AA", "AB", "AC", "BA", "BB", "BC", "CA", "CB", "CC" };
        private string[] _shapes3 = new string[] { "AAA", "AAB", "AAC", "ABA", "ABB", "ABC", "ACA", "ACB", "ACC", "BAA", "BAB", "BAC", "BBA", "BBB", "BBC", "BCA", "BCB", "BCC", "CAA", "CAB", "CAC", "CBA", "CBB", "CBC", "CCA", "CCB", "CCC" };
        private Dictionary<string, int[]> _constraints;
        private ShapeFilterType _filterType;
        Dictionary<byte, char[]> _mappedProbability;
        private int _min;
        private int _max;

        public LettersShapesValidator(FilterData filterData, Dictionary<byte, char[]> mappedProbability)
        {
            _mappedProbability = mappedProbability;
            _filterType = filterData.ShapeFilterType;
            int numOfShapes = 0;
            string[] shapes = null;
            switch (filterData.ShapeFilterType)
            {
                case ShapeFilterType.LetterShapes2: numOfShapes = 9; shapes = _shapes2; break;
                case ShapeFilterType.LetterShapes3: numOfShapes = 27; shapes = _shapes3; break;
            }

            _constraints = new Dictionary<string, int[]>();
            for (int i = 0; i < numOfShapes * 2; i += 2)
            {
                _constraints.Add(shapes[i / 2], new int[] 
                {
                    //int.Parse(filterData.FilterDataRow[i]),
                    //int.Parse(filterData.FilterDataRow[i + 1])
                    Utilities.DecimalStrToInt(filterData.FilterDataRow[i]),
                    Utilities.DecimalStrToInt(filterData.FilterDataRow[i + 1])
                });
            }

            //_min = int.Parse(filterData.FilterMin);
            //_max = int.Parse(filterData.FilterMax);
            _min = Utilities.DecimalStrToInt(filterData.FilterMin);
            _max = Utilities.DecimalStrToInt(filterData.FilterMax);
        }

        public bool CheckResult(Dictionary<byte, byte> result)
        {
            Dictionary<string, int> shapesCounters = new Dictionary<string, int>();
            foreach (string key in _constraints.Keys) shapesCounters.Add(key, 0);

            CountShapes(result, shapesCounters);

            int numOfMatchCounters = 0;
            foreach (string counterId in shapesCounters.Keys)
            {
                if (shapesCounters[counterId] >= _constraints[counterId][0] &&
                    shapesCounters[counterId] <= _constraints[counterId][1]) numOfMatchCounters++;
            }

            return numOfMatchCounters >= _min && numOfMatchCounters <= _max;
        }

        private void CountShapes(Dictionary<byte, byte> result, Dictionary<string, int> shapesCounters)
        {
            switch (_filterType)
            {
                case ShapeFilterType.LetterShapes2:
                    for (byte i = 0; i < result.Count - 1; i++)
                    {
                        string key = string.Format("{0}{1}", _mappedProbability[i][result[i]], _mappedProbability[(byte)(i + 1)][result[(byte)(i + 1)]]);
                        shapesCounters[key]++;
                    }
                    break;
                case ShapeFilterType.LetterShapes3:
                    for (byte i = 0; i < result.Count - 2; i++)
                    {
                        string key = string.Format("{0}{1}{2}", _mappedProbability[i][result[i]], _mappedProbability[(byte)(i + 1)][result[(byte)(i + 1)]], _mappedProbability[(byte)(i + 2)][result[(byte)(i + 2)]]);
                        shapesCounters[key]++;
                    }
                    break;
            }
        }
    }
}
