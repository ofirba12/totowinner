﻿using System.Collections.Generic;
using TotoWinner.Common;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class RegularShapesValidator : IValidator
    {
        private int[] _shapes = new int[] { 0, 1, 2, 10, 11, 12, 20, 21, 22, 100, 101, 102, 110, 111, 112, 120, 121, 122, 200, 201, 202, 210, 211, 212, 220, 221, 222 };
        private Dictionary<int, int[]> _constraints;
        private ShapeFilterType _filterType;
        private int _min;
        private int _max;

        public RegularShapesValidator(FilterData filterData)
        {
            _filterType = filterData.ShapeFilterType;
            int numOfShapes = 0;
            switch (filterData.ShapeFilterType)
            {
                case ShapeFilterType.RegularShapes2: numOfShapes = 9; break;
                case ShapeFilterType.RegularShapes3: numOfShapes = 27; break;
            }

            _constraints = new Dictionary<int, int[]>();
            for (int i = 0; i < numOfShapes * 2; i += 2)
            {
                _constraints.Add(_shapes[i / 2], new[] 
                    {
                        Utilities.DecimalStrToInt(filterData.FilterDataRow[i]),
                        Utilities.DecimalStrToInt(filterData.FilterDataRow[i + 1])
                        //int.Parse(filterData.FilterDataRow[i]),
                        //int.Parse(filterData.FilterDataRow[i + 1])
                    });
            }

            //_min = int.Parse(filterData.FilterMin);
            //_max = int.Parse(filterData.FilterMax);
            _min = Utilities.DecimalStrToInt(filterData.FilterMin);
            _max = Utilities.DecimalStrToInt(filterData.FilterMax);
        }

        public bool CheckResult(Dictionary<byte, byte> result)
        {
            Dictionary<int, int> shapesCounters = new Dictionary<int, int>();
            foreach (int key in _constraints.Keys) shapesCounters.Add(key, 0);

            CountShapes(result, shapesCounters);

            int numOfMatchCounters = 0;
            foreach (int counterId in shapesCounters.Keys)
            {
                if (shapesCounters[counterId] >= _constraints[counterId][0] &&
                    shapesCounters[counterId] <= _constraints[counterId][1]) numOfMatchCounters++;
            }

            return numOfMatchCounters >= _min && numOfMatchCounters <= _max;
        }

        private void CountShapes(Dictionary<byte, byte> result, Dictionary<int, int> shapesCounters)
        {
            switch (_filterType)
            {
                case ShapeFilterType.RegularShapes2:
                    for (byte i = 0; i < result.Count - 1; i++)
                    {
                        int key = result[i] * 10 + result[(byte)(i + 1)];
                        shapesCounters[key]++;
                    }
                    break;
                case ShapeFilterType.RegularShapes3:
                    for (byte i = 0; i < result.Count - 2; i++)
                    {
                        int key = result[i] * 100 + result[(byte)(i + 1)] * 10 + result[(byte)(i + 2)];
                        shapesCounters[key]++;
                    }
                    break;
            }
        }
    }
}
