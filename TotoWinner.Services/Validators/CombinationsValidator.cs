﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class CombinationsValidator : IValidator
    {
        private Dictionary<int, bool[]> _options;
        private int _min;
        private int _max;

        public CombinationsValidator(FilterData filterData)
        {
            _options = new Dictionary<int, bool[]>();
            for (int i = 0; i < 16; i++) _options[i] = new bool[3];
            int currColumnIndex = 0;
            foreach (string filterDataRow in filterData.FilterDataRow)
            {
                foreach (char selection in filterDataRow.ToCharArray())
                {
                    switch (selection)
                    {
                        case '1': _options[currColumnIndex][0] = true; break;
                        case 'x':
                        case 'X': _options[currColumnIndex][1] = true; break;
                        case '2': _options[currColumnIndex][2] = true; break;
                    }
                }
                currColumnIndex++;
            }

            _min = decimal.ToInt32(decimal.Parse(filterData.FilterMin)); //int.Parse(filterData.FilterMin); 
            _max = decimal.ToInt32(decimal.Parse(filterData.FilterMax)); //int.Parse(filterData.FilterMax);
        }

        public bool CheckResult(Dictionary<byte, byte> result)
        {
            int numOfMatches = 0;
            foreach (byte resultId in result.Keys)
            {
                if (_options[resultId][result[resultId]]) numOfMatches++;
            }
            return numOfMatches >= _min && numOfMatches <= _max;
        }
    }
}
