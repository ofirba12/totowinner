﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class LettersMinMaxAndSeqValidator : IValidator
    {
        private Dictionary<int, int[]> _lettersMinMax;
        private Dictionary<int, int[]> _lettersSeqLenghts;
        private Dictionary<byte, char[]> _lettersMappedProbability;
        private int[] _letters;
        private int _lettersBreakMin;
        private int _lettersBreakMax;

        private void Init(CombinationLettersSettings settings, char letter)
        {
            MinMax letterMinMix = null;
            MinMax sequanceMinMix = null;
            var index = 0;
            switch (letter)
            {
                case 'A':
                    letterMinMix = settings.MinMaxA;
                    sequanceMinMix = settings.MinMaxSequenceLengthA;
                    index = 0;
                    break;
                case 'B':
                    letterMinMix = settings.MinMaxB;
                    sequanceMinMix = settings.MinMaxSequenceLengthB;
                    index = 1;
                    break;
                case 'C':
                    letterMinMix = settings.MinMaxC;
                    sequanceMinMix = settings.MinMaxSequenceLengthC;
                    index = 2;
                    break;
                default:
                    break;
            }
            _lettersMinMax.Add(index, new int[2]);
            _lettersSeqLenghts.Add(index, new int[2]);
            if (letterMinMix != null)
            {
                _lettersMinMax[index][0] = (int)letterMinMix.Min;
                _lettersMinMax[index][1] = (int)letterMinMix.Max;
            }
            else
            {
                _lettersMinMax[index] = new int[] { 0, 16 };
            }
            if (sequanceMinMix != null)
            {
                _lettersSeqLenghts[index][0] = (int)sequanceMinMix.Min;
                _lettersSeqLenghts[index][1] = (int)sequanceMinMix.Max;
            }
            else
            {
               _lettersSeqLenghts[index] = new int[] { 0, 16 };
            }
            if (_lettersSeqLenghts[index][0] > _lettersMinMax[index][0])
                _lettersMinMax[index][0] = _lettersSeqLenghts[index][0];

        }
        public LettersMinMaxAndSeqValidator(CombinationLettersSettings settings, Dictionary<byte, char[]> lettersMappedProbability)
        {
            _letters = new[] { 0, 1, 2 };

            _lettersMappedProbability = lettersMappedProbability;
            _lettersMinMax = new Dictionary<int, int[]>();
            _lettersSeqLenghts = new Dictionary<int, int[]>();
            Init(settings, 'A');//A
            Init(settings, 'B');//B
            Init(settings, 'C');//C
            if (settings.MinMaxBreak != null)
            {
                _lettersBreakMin = (int)settings.MinMaxBreak.Min;
                _lettersBreakMax = (int)settings.MinMaxBreak.Max;
            }
            else
            {
                _lettersBreakMin = 0;
                _lettersBreakMax = 15; //WHY ON SCREEN IT IS 15
            }
        }

        public bool CheckResult(Dictionary<byte, byte> result)
        {
            bool[] seqFound = { false, false, false };

            Dictionary<int, int> counters = new Dictionary<int, int>();
            counters.Add(0, 0);
            counters.Add(1, 0);
            counters.Add(2, 0);

            int breaksCount = 0;
            int prevLetter = int.MinValue;

            Dictionary<int, int> seqLength = new Dictionary<int, int>();
            seqLength.Add(0, 0);
            seqLength.Add(1, 0);
            seqLength.Add(2, 0);

            foreach (var resultRow in result)
            {
                int currLetter = _lettersMappedProbability[resultRow.Key][resultRow.Value] - 65;

                counters[currLetter]++;


                if (resultRow.Key > 0 && currLetter != prevLetter)
                {
                    int prevSeqLength = seqLength[prevLetter];
                    if (!seqFound[prevLetter] &&
                        prevSeqLength >= _lettersSeqLenghts[prevLetter][0]) seqFound[prevLetter] = true;

                    seqLength[prevLetter] = 0;
                    breaksCount++;
                }
                seqLength[currLetter]++;
                if (seqLength[currLetter] > _lettersSeqLenghts[currLetter][1]) return false;
                prevLetter = currLetter;
            }
            foreach (int letter in _letters)
            {
                if (!(counters[letter] >= _lettersMinMax[letter][0] && counters[letter] <= _lettersMinMax[letter][1])) return false;
            }

            if (!(breaksCount >= _lettersBreakMin && breaksCount <= _lettersBreakMax)) return false;

            foreach (int letter in _letters)
            {
                if (!seqFound[letter] &&
                    seqLength[letter] >= _lettersSeqLenghts[letter][0] &&
                    seqLength[letter] <= _lettersSeqLenghts[letter][1]) seqFound[letter] = true;
            }

            return seqFound[0] && seqFound[1] && seqFound[2];
        }
    }
}
