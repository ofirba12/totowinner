﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public interface ISystemsServices
    {
        int SaveSystem(string uniqueId, SystemType systemType, string systemName, SaveShrinkSystemRequest saveData);
        int SaveSystem(string uniqueId, SystemType systemType, string systemName, SaveOptimizerSystemRequest saveData);
        int SaveSystem(string uniqueId, string systemName, ShrinkSystemPayload saveData);
        
        List<SystemParameters> GetAllSystemsKeys(SystemType type);
        
        T LoadSystem<T>(int systemId) where T : ISaveSystemRequest;
        SaveShrinkSystemRequest LoadSystemForOptimizationV1(int systemId);
        ShrinkSystemPayload LoadSystemForOptimizationV2(int systemId);
        void DeleteSystem(int systemId);
        UnionSystemResult UnionSystems(List<int> systemIds);
        List<SystemParametersWithFinalResult> GetAllSystemsForUnion(SystemType sType, ProgramType pType);
    }
}
