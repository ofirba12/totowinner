﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class SystemsServices : Singleton<SystemsServices>, ISystemsServices
    {
        private SystemsServices() { }

        public int SaveSystem(string uniqueId, SystemType systemType, string systemName, SaveShrinkSystemRequest saveData)
        {
            try
            {
                var systemId = this.SaveSystem(uniqueId, systemType, systemName, JsonConvert.SerializeObject(saveData));
                return systemId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to save shrink system {systemName}", ex);
            }
        }
        public int SaveSystem(string uniqueId, string systemName, ShrinkSystemPayload saveData)
        {
            try
            {
                var systemId = this.SaveSystem(uniqueId, SystemType.Shrink, systemName, JsonConvert.SerializeObject(saveData));
                return systemId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to save shrink system {systemName}", ex);
            }
        }
        public int SaveSystem(string uniqueId, SystemType systemType, string systemName, SaveOptimizerSystemRequest saveData)
        {
            try
            {
                if (string.IsNullOrEmpty(uniqueId) && systemType == SystemType.Optimizer)
                {
                    var shrinkSystem = this.LoadSystem<SaveShrinkSystemRequest>(saveData.OptimizedSystemId);
                    uniqueId = shrinkSystem.UniqueId;
                }
                var systemId = this.SaveSystem(uniqueId, systemType, systemName, JsonConvert.SerializeObject(saveData));
                if (systemType == SystemType.Optimizer)
                {
                    var dbServices = Services.PersistanceServices.Instance;
                    dbServices.ShrinkUpdateReference(saveData.OptimizedSystemId, systemId);
                }
                return systemId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to save optimizer system {systemName}", ex);
            }
        }
        private int SaveSystem(string uniqueId, SystemType systemType, string systemName, string saveData)
        {
            var items = uniqueId.Split('#');
            var programId = int.Parse(items[0]);
            ProgramType programType;
            Enum.TryParse(items[1], out programType);
            var system = new SystemParameters()
            {
                Type = systemType,
                ProgramId = programId,
                ProgramType = programType,
                Name = systemName,
                RefrenceSystemId = null,
                SystemPayload = saveData
            };

            var dbServices = Services.PersistanceServices.Instance;
            var systemId = dbServices.SystemAddOrUpdate(system);
            return systemId;
        }
        public List<SystemParameters> GetAllSystemsKeys(SystemType type)
        {
            try
            {
                var dbServices = Services.PersistanceServices.Instance;
                var systems = dbServices.GetSystems(type);
                systems.ForEach(item => item.SystemPayload = null);
                return systems;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all systems keys of type {type}", ex);
            }
        }
        public List<SystemParametersWithFinalResult> GetAllSystemsForUnion(SystemType sType, ProgramType pType)
        {
            try
            {
                var dbServices = Services.PersistanceServices.Instance;
                var systems = dbServices.GetSystems(sType);
                var programTypeSystems = systems
                    .ConvertAll<SystemParametersWithFinalResult>(i =>
                        {
                            ShrinkSystemPayload shrinkData = null;
                            if (sType == SystemType.Shrink)
                            {
                                shrinkData = LoadSystem<ShrinkSystemPayload>(i.SystemId);
                            }
                            else if (sType == SystemType.Optimizer)
                            {
                                var executionSrv = ExecutionServices.Instance;
                                var data = LoadSystem<SaveOptimizerSystemRequest>(i.SystemId);
                                shrinkData = LoadSystemForOptimizationV2(data.OptimizedSystemId);
                                shrinkData.Result = executionSrv.GetOptimizerExecutionShrinkResult(data.Execution.ExecutionId);
                                if (shrinkData.Result == null)
                                    return null;
                            }
                            return new SystemParametersWithFinalResult()
                            {
                                FinalResult = shrinkData.Result.ResultsStatistics.Found,
                                Name = i.Name,
                                ProgramId = i.ProgramId,
                                LastUpdate = i.LastUpdate,
                                ProgramType = i.ProgramType,
                                RefrenceSystemId = i.RefrenceSystemId,
                                SystemId = i.SystemId,
                                Type = i.Type,
                                SystemPayload = null
                            };
                        })
                    .Where(sys => sys != null && sys.ProgramType == pType)
                    .ToList();

                return programTypeSystems;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all systems for union  of type {sType}, {pType}", ex);
            }
        }
        public UnionSystemResult UnionSystems(List<int> systemIds)
        {
            try
            {
                var systems = GetSystems(systemIds);
                var payloads = new List<ShrinkSystemPayload>();
                var combinedResults = new List<IEnumerable<CombinedResult>>();
                foreach (var system in systems)
                {
                    ShrinkSystemPayload payload = null;
                    if (system.Type == SystemType.Shrink)
                    {
                        payload = JsonConvert.DeserializeObject<ShrinkSystemPayload>(system.SystemPayload);
                    }
                    else if (system.Type == SystemType.Optimizer)
                    {
                        var executionSrv = ExecutionServices.Instance;
                        var data = LoadSystem<SaveOptimizerSystemRequest>(system.SystemId);
                        payload = LoadSystemForOptimizationV2(data.OptimizedSystemId);
                        payload.Result = executionSrv.GetOptimizerExecutionShrinkResult(data.Execution.ExecutionId);
                    }
                    payloads.Add(payload);
                    var results = payload.Definitions.Form.CleanOnDiff == true
                        ? payload.Result.FinalResults.Where(res => res.IsIncludedInResults == true)
                        : payload.Result.FinalResults;
                    combinedResults.Add(results);
                }
                var unionResults = Services.CalculationServices.Instance.UnionResults(combinedResults);
                return unionResults;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to union systems by ids {string.Join(",", systemIds)}", ex);
            }
        }
        private List<SystemParameters> GetSystems(List<int> systemIds)
        {
            try
            {
                var dbServices = Services.PersistanceServices.Instance;
                var systems = new List<SystemParameters>();
                foreach (var systemId in systemIds)
                {
                    var system = dbServices.SystemLoad(systemId);
                    systems.Add(system);
                }
                return systems;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get systems by ids {string.Join(",", systemIds)}", ex);
            }
        }
        public T LoadSystem<T>(int systemId) where T : ISaveSystemRequest
        {
            try
            {
                var dbServices = Services.PersistanceServices.Instance;
                var system = dbServices.SystemLoad(systemId);
                var shrinkSystem = JsonConvert.DeserializeObject<T>(system.SystemPayload);
                return shrinkSystem;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get load system: systemId {systemId}", ex);
            }
        }
        public void DeleteSystem(int systemId)
        {
            try
            {
                var dbServices = Services.PersistanceServices.Instance;
                dbServices.SystemDelete(systemId);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete system: systemId {systemId}", ex);
            }
        }
        public SaveShrinkSystemRequest LoadSystemForOptimizationV1(int systemId)
        {
            try
            {
                var shrinkSystem = this.LoadSystem<SaveShrinkSystemRequest>(systemId);
                shrinkSystem.System.FormSettings.Combination1X2Settings.RemoveAll(combination => combination.IsActive == false);
                shrinkSystem.System.FormSettings.CombinationLettersSettings.LettersSettings.RemoveAll(combination => combination.IsActive == false);
                shrinkSystem.System.FormSettings.Shapes2.RemoveAll(shape => shape.IsActive == false);
                shrinkSystem.System.FormSettings.Shapes3.RemoveAll(shape => shape.IsActive == false);
                shrinkSystem.System.FormSettings.LettersShapes2.RemoveAll(shape => shape.IsActive == false);
                shrinkSystem.System.FormSettings.LettersShapes3.RemoveAll(shape => shape.IsActive == false);
                return shrinkSystem;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get load system for optimization: systemId {systemId}", ex);
            }
        }
        public ShrinkSystemPayload LoadSystemForOptimizationV2(int systemId)
        {
            try
            {
                var shrinkSystem = this.LoadSystem<ShrinkSystemPayload>(systemId);
                shrinkSystem.Definitions.Form.FormSettings.Combination1X2Settings.RemoveAll(combination => combination.IsActive == false);
                shrinkSystem.Definitions.Form.FormSettings.CombinationLettersSettings.LettersSettings.RemoveAll(combination => combination.IsActive == false);
                shrinkSystem.Definitions.Form.FormSettings.Shapes2.RemoveAll(shape => shape.IsActive == false);
                shrinkSystem.Definitions.Form.FormSettings.Shapes3.RemoveAll(shape => shape.IsActive == false);
                shrinkSystem.Definitions.Form.FormSettings.LettersShapes2.RemoveAll(shape => shape.IsActive == false);
                shrinkSystem.Definitions.Form.FormSettings.LettersShapes3.RemoveAll(shape => shape.IsActive == false);
                return shrinkSystem;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get load system for optimization: systemId {systemId}", ex);
            }
        }
    }
}
