﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class SearchEngine
    {
        private readonly Dictionary<byte, byte> _currentResultsColumn = new Dictionary<byte, byte>();
        private readonly Dictionary<byte, byte> _testOptions;
        private readonly Dictionary<byte, decimal[]> _probSettings;
        private readonly ConstraintsSettings _constrSettings;
        private readonly bool _isRunTest;
        private readonly List<IValidator> _validators;
        private readonly List<CombinedResult> _combinedResults;
        private readonly bool _isOneDiffFiltered;
        private readonly int _numOfRows;

        public SearchEngine(Dictionary<byte, decimal[]> probSettings, 
            ConstraintsSettings constrSettings, 
            List<IValidator> validators, 
            Dictionary<byte, byte> testSettings, bool isRunTest, bool isOneDiffFiltered)
        {
            _combinedResults = new List<CombinedResult>();
            _probSettings = probSettings;
            _constrSettings = constrSettings;
            _testOptions = testSettings;
            _isRunTest = isRunTest;
            _validators = validators;
            _isOneDiffFiltered = isOneDiffFiltered;
            _numOfRows = constrSettings.Options.Count;
        }

        public SearchResults Search()
        {
            FindPermutations(0, 0, 0, 0, 0, 0, 0, 0, 0, false, false, false);
            ResultsStatistics resultsStatistics = new ResultsStatistics();

            if (_isOneDiffFiltered)
            {
                ExcludeFromFoundResults();
            }

            resultsStatistics.Found = _combinedResults.Count(cr => !_isOneDiffFiltered || cr.IsIncludedInResults);
            resultsStatistics.TestsCounters = new Dictionary<byte, int>();
            for (byte i = 0; i <= _numOfRows; i++)
            {
                resultsStatistics.TestsCounters[i] = _isRunTest ? _combinedResults.Count(cr => (!_isOneDiffFiltered || cr.IsIncludedInResults) && cr.TestResult == i) : 0;
            }

            SearchResults searchResults = new SearchResults
            {
                ResultsStatistics = resultsStatistics,
                FinalResults = _combinedResults
            };

            return searchResults;
        }
        //PREV: CombinedResult.InsertIntoMap
        public void CombinedResultInsertIntoMap(CombinedResult orgResult, CombinedResult refResult, byte maxNumOfDiffs)
        {
            if (orgResult == refResult) return;

            byte numOfDiffs = 0;
            for (byte key = 0; key < orgResult._resultColumn.Length; key++)
            {
                if (orgResult._resultColumn[key] != refResult._resultColumn[key]) numOfDiffs++;

                if (numOfDiffs > maxNumOfDiffs) return;
            }
            orgResult._pointersToResults.Add(refResult);
            refResult._pointersToResults.Add(orgResult);
        }

        //PREV: CombinedResult.IncludeInResults
        internal void CombinedResultIncludeInResults(CombinedResult orgResult)
        {
            orgResult._isIncludedInResults = true;

            foreach (var pointedResult in orgResult._pointersToResults)
            {
                foreach (var x in pointedResult._pointersToResults)
                {
                    if (x == orgResult) continue;
                    x._pointersToResults.Remove(pointedResult);
                }

                pointedResult._isSkipped = true;
                pointedResult._pointersToResults.Clear();
            }

            orgResult._pointersToResults.Clear();
        }
        private void ExcludeFromFoundResults()
        {
            if (_combinedResults.Count == 0) return;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            int inMapHits = 0;
            foreach (var res in _combinedResults)
            {
                foreach (var refRes in _combinedResults)
                {
                    if (refRes.IsInMap)
                    {
                        inMapHits++;
                        continue;
                    }
                    CombinedResultInsertIntoMap(res, refRes, 1);
                }

                res.IsInMap = true;
            }
            sw.Stop();
            Debug.WriteLine("Map built: " + sw.Elapsed + ", in map hits: " + inMapHits);

            sw.Restart();
            while (true)
            {
                if (!_combinedResults.Any(cr => !cr.IsSkipped && !cr.IsIncludedInResults)) break;

                var crWithMaxPointers = _combinedResults.Where(cr => !cr.IsSkipped && !cr.IsIncludedInResults)
                    .Aggregate((cr1, cr2) => cr1.NumOfPointers >= cr2.NumOfPointers ? cr1 : cr2);

                CombinedResultIncludeInResults(crWithMaxPointers);
            }
            sw.Stop();
            Debug.WriteLine("Filter done: " + sw.Elapsed);

        }

        private void FindPermutations(byte index, decimal currentSum, byte seq1, byte seqX, byte seq2, byte count1, byte countX, byte count2, byte countBreak, bool foundSeq1, bool foundSeqX, bool foundSeq2)
        {

            if (seq1 > _constrSettings.SeqLengths[0][1]) return;
            if (seqX > _constrSettings.SeqLengths[1][1]) return;
            if (seq2 > _constrSettings.SeqLengths[2][1]) return;

            if (count1 > _constrSettings.MinMax[0][1]) return;
            if (countX > _constrSettings.MinMax[1][1]) return;
            if (count2 > _constrSettings.MinMax[2][1]) return;

            int remaininDepth = _numOfRows - index;
            if (_constrSettings.MinMax[0][0] > count1 + remaininDepth) return;
            if (_constrSettings.MinMax[1][0] > countX + remaininDepth) return;
            if (_constrSettings.MinMax[2][0] > count2 + remaininDepth) return;

            if (index == _numOfRows)
            {
                foundSeq1 = foundSeq1 || (seq1 >= _constrSettings.SeqLengths[0][0]);
                foundSeqX = foundSeqX || (seqX >= _constrSettings.SeqLengths[1][0]);
                foundSeq2 = foundSeq2 || (seq2 >= _constrSettings.SeqLengths[2][0]);
                if (!(foundSeq1 && foundSeqX && foundSeq2)) return;

                if (countBreak - 1 < _constrSettings.BreakMin) return;

                if (!(currentSum >= _constrSettings.MinSum && currentSum <= _constrSettings.MaxSum)) return;

                if (!ValidateResult()) return;

                byte testCount = 0;
                if (_isRunTest)
                {
                    RunTest(ref testCount);
                }

                SaveResult(testCount);

                return;
            }
            if (currentSum > _constrSettings.MaxSum) return;

            for (byte i = 0; i < 3; i++)
            {
                if (_constrSettings.Options[index][i])
                {
                    _currentResultsColumn[index] = i;
                    decimal newSum = currentSum + _probSettings[index][i];

                    byte newCountBreak;
                    bool foundSeq1Loc, foundSeqXLoc, foundSeq2Loc;
                    switch (i)
                    {
                        case 0:
                            foundSeqXLoc = foundSeqX || (seqX >= _constrSettings.SeqLengths[1][0]);
                            foundSeq2Loc = foundSeq2 || (seq2 >= _constrSettings.SeqLengths[2][0]);
                            newCountBreak = (seq1 == 0) ? (byte)(countBreak + 1) : countBreak;
                            if (newCountBreak - 1 > _constrSettings.BreakMax) break;
                            FindPermutations((byte)(index + 1), newSum, (byte)(seq1 + 1), 0, 0, (byte)(count1 + 1), countX, count2, newCountBreak, foundSeq1, foundSeqXLoc, foundSeq2Loc); break;
                        case 1:
                            foundSeq1Loc = foundSeq1 || (seq1 >= _constrSettings.SeqLengths[0][0]);
                            foundSeq2Loc = foundSeq2 || (seq2 >= _constrSettings.SeqLengths[2][0]);
                            newCountBreak = (seqX == 0) ? (byte)(countBreak + 1) : countBreak;
                            if (newCountBreak - 1 > _constrSettings.BreakMax) break;
                            FindPermutations((byte)(index + 1), newSum, 0, (byte)(seqX + 1), 0, count1, (byte)(countX + 1), count2, newCountBreak, foundSeq1Loc, foundSeqX, foundSeq2Loc); break;
                        case 2:
                            foundSeq1Loc = foundSeq1 || (seq1 >= _constrSettings.SeqLengths[0][0]);
                            foundSeqXLoc = foundSeqX || (seqX >= _constrSettings.SeqLengths[1][0]);
                            newCountBreak = (seq2 == 0) ? (byte)(countBreak + 1) : countBreak;
                            if (newCountBreak - 1 > _constrSettings.BreakMax) break;
                            FindPermutations((byte)(index + 1), newSum, 0, 0, (byte)(seq2 + 1), count1, countX, (byte)(count2 + 1), newCountBreak, foundSeq1Loc, foundSeqXLoc, foundSeq2); break;
                    }


                }
            }
        }

        private void SaveResult(int testResult)
        {
            _combinedResults.Add(new CombinedResult(_currentResultsColumn, testResult));
        }

        private bool ValidateResult()
        {
            if (_validators == null || _validators.Count == 0) return true;

            foreach (IValidator validator in _validators)
            {
                if (!validator.CheckResult(_currentResultsColumn)) return false;
            }
            return true;
        }

        private void RunTest(ref byte testCount)
        {
            foreach (var resultRow in _currentResultsColumn)
            {
                if (_testOptions[resultRow.Key] == resultRow.Value) testCount++;
            }
        }
    }
}
