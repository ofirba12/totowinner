﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    //public partial class ExcludeConstraintsOptionsValidator : IValidator
    //{
    //    public bool CheckResult(Dictionary<byte, byte> result)
    //    {
    //        decimal sum = 0;
    //        int numOfBreaks = 0;
    //        Dictionary<int, int> counters = new Dictionary<int, int>();
    //        for (int i = 0; i < 3; i++) counters[i] = 0;
    //        int prevOption = -1;
    //        int optionSeqLength = 0;
    //        foreach (var resultRow in result)
    //        {
    //            sum += _probSettins[resultRow.Key][resultRow.Value];
    //            counters[resultRow.Value]++;

    //            if (prevOption != -1 && prevOption != resultRow.Value)
    //            {
    //                if (_isFilterActive[FilterType.Seq][prevOption] && _seqLengthEx[prevOption] == optionSeqLength) return false;
    //                optionSeqLength = 0;
    //                numOfBreaks++;
    //            }
    //            prevOption = resultRow.Value;
    //            optionSeqLength++;
    //        }
    //        if (_isFilterActive[FilterType.Seq][prevOption] && _seqLengthEx[prevOption] == optionSeqLength) return false;

    //        if (_isFilterActive[FilterType.Sum][0] && sum >= _minMaxSum[0] && sum <= _minMaxSum[1]) return false;
    //        if (_isFilterActive[FilterType.Breaks][0] && numOfBreaks >= _breaksMin && numOfBreaks <= _breaksMax) return false;
    //        foreach (var counterRow in counters)
    //        {
    //            if (_isFilterActive[FilterType.Occur][counterRow.Key] && counterRow.Value >= _minMax[counterRow.Key][0] && counterRow.Key <= _minMax[counterRow.Key][1]) return false;
    //        }

    //        return true;

    //    }
    //}
    //public static class ValidatorsExternsions
    //{
    //    public static bool CheckResult(this ExcludeConstraintsOptionsValidator validator,
    //        Dictionary<byte, byte> result)
    //    {
    //        decimal sum = 0;
    //        int numOfBreaks = 0;
    //        Dictionary<int, int> counters = new Dictionary<int, int>();
    //        for (int i = 0; i < 3; i++) counters[i] = 0;
    //        int prevOption = -1;
    //        int optionSeqLength = 0;
    //        foreach (var resultRow in result)
    //        {
    //            sum += validator._probSettins[resultRow.Key][resultRow.Value];
    //            counters[resultRow.Value]++;

    //            if (prevOption != -1 && prevOption != resultRow.Value)
    //            {
    //                if (validator._isFilterActive[ExcludeConstraintsOptionsValidator.FilterType.Seq][prevOption] && 
    //                    validator._seqLengthEx[prevOption] == optionSeqLength) return false;
    //                optionSeqLength = 0;
    //                numOfBreaks++;
    //            }
    //            prevOption = resultRow.Value;
    //            optionSeqLength++;
    //        }
    //        if (validator._isFilterActive[ExcludeConstraintsOptionsValidator.FilterType.Seq][prevOption] && 
    //            validator._seqLengthEx[prevOption] == optionSeqLength) return false;

    //        if (validator._isFilterActive[ExcludeConstraintsOptionsValidator.FilterType.Sum][0] && 
    //            sum >= validator._minMaxSum[0] && 
    //            sum <= validator._minMaxSum[1]) return false;
    //        if (validator._isFilterActive[ExcludeConstraintsOptionsValidator.FilterType.Breaks][0] && 
    //            numOfBreaks >= validator._breaksMin && 
    //            numOfBreaks <= validator._breaksMax) return false;
    //        foreach (var counterRow in counters)
    //        {
    //            if (validator._isFilterActive[ExcludeConstraintsOptionsValidator.FilterType.Occur][counterRow.Key] && 
    //                counterRow.Value >= validator._minMax[counterRow.Key][0] && 
    //                counterRow.Key <= validator._minMax[counterRow.Key][1]) return false;
    //        }

    //        return true;
    //    }
    //}
}
