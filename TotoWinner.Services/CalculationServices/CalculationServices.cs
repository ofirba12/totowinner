﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class CalculationServices : Singleton<CalculationServices>, ICalculationServices
    {
        private static readonly char A = 'A';
        private static readonly char B = 'B';
        private static readonly char C = 'C';
        private static readonly string[] _letters = { "A", "B", "C", "AB", "AC", "BC" };

        private CalculationServices() { }

        public ConstraintsSettings BuildConstrSettings(BetsFormCreateRequest request)
        {
            try
            {
                var includeSettings = request.FormSettings.IncludeSettings;
                ConstraintsSettings constSettings = new ConstraintsSettings();

                constSettings.MinSum = includeSettings.amountSum.Min;
                constSettings.MaxSum = includeSettings.amountSum.Max;

                constSettings.BreakMin = Convert.ToByte(includeSettings.sequenceBreak.Min);
                constSettings.BreakMax = Convert.ToByte(includeSettings.sequenceBreak.Max);

                constSettings.SeqLengths = new Dictionary<byte, byte[]>();
                constSettings.SeqLengths.Add(0, new[] { Convert.ToByte(includeSettings.sequence1.Min), Convert.ToByte(includeSettings.sequence1.Max) });
                constSettings.SeqLengths.Add(1, new[] { Convert.ToByte(includeSettings.sequenceX.Min), Convert.ToByte(includeSettings.sequenceX.Max) });
                constSettings.SeqLengths.Add(2, new[] { Convert.ToByte(includeSettings.sequence2.Min), Convert.ToByte(includeSettings.sequence2.Max) });

                constSettings.MinMax = new Dictionary<byte, byte[]>();
                constSettings.MinMax.Add(0, new[] { Convert.ToByte(includeSettings.amount1.Min), Convert.ToByte(includeSettings.amount1.Max) });
                constSettings.MinMax.Add(1, new[] { Convert.ToByte(includeSettings.amountX.Min), Convert.ToByte(includeSettings.amountX.Max) });
                constSettings.MinMax.Add(2, new[] { Convert.ToByte(includeSettings.amount2.Min), Convert.ToByte(includeSettings.amount2.Max) });

                constSettings.Options = new Dictionary<int, bool[]>();
                foreach (var betLine in request.EventsBets)
                {
                    constSettings.Options.Add(betLine.position, new[] { betLine.bet1, betLine.betX, betLine.bet2 });
                }

                return constSettings;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to build constraints settings.", ex);
            }
        }
        private ExcludeConstraintsOptions BuildExcludeConstraintsOptions(BetsFormCreateRequest request)
        {
            try
            {
                var excludeSettings = request.FormSettings.ExcludeSettings;
                ExcludeConstraintsOptions excludeConstraintsOptions = new ExcludeConstraintsOptions();

                excludeConstraintsOptions.MinMaxSettings = new List<string[]>
                {
                    new[] { excludeSettings.amountSum.Min.ToString(), excludeSettings.amountSum.Max.ToString() },
                    new[] { excludeSettings.amount1.Min.ToString(), excludeSettings.amount1.Max.ToString() },
                    new[] { excludeSettings.amountX.Min.ToString(), excludeSettings.amountX.Max.ToString() },
                    new[] { excludeSettings.amount2.Min.ToString(), excludeSettings.amount2.Max.ToString() }
                };
                excludeConstraintsOptions.SequenciesLengths = new List<string>()
                {
                    excludeSettings.sequence1.ToString(),
                    excludeSettings.sequenceX.ToString(),
                    excludeSettings.sequence2.ToString()
                };
                excludeConstraintsOptions.BreakMinMax = new string[2]
                {
                    excludeSettings.sequenceBreak.Min.ToString(),
                    excludeSettings.sequenceBreak.Max.ToString()
                };
                excludeConstraintsOptions.isExSumActive = excludeSettings.ActivateSumAmountFilter;
                excludeConstraintsOptions.isExOneActive = excludeSettings.ActivateOneAmountFilter;
                excludeConstraintsOptions.isExXActive = excludeSettings.ActivateXAmountFilter;
                excludeConstraintsOptions.isExTwoActive = excludeSettings.ActivateTwoAmountFilter;
                excludeConstraintsOptions.isExOneSActive = excludeSettings.ActivateOneSequenceFilter;
                excludeConstraintsOptions.isExXSActive = excludeSettings.ActivateXSequenceFilter;
                excludeConstraintsOptions.isExTwoSActive = excludeSettings.ActivateTwoSequenceFilter;
                excludeConstraintsOptions.isExBreakActive = excludeSettings.ActivateBreakFilter;

                return excludeConstraintsOptions;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to build constraints settings.", ex);
            }
        }

        public UnionSystemResult UnionResults(List<IEnumerable<CombinedResult>> combinedResults)
        {
            try
            {
                //make sure combinedResults are after cleanOneDiff decision
                var unionRepo = new Dictionary<string, CombinedResult>();
                var counterBefore = 0;
                var counterAfter = 0;
                var counters = new Dictionary<int, Container1X2>(); //gameindex, <counter1,counterX,counter2>
                foreach (var result in combinedResults)
                {
                    foreach (var finalResult in result)
                    {
                        var gamesResult = new List<char>();
                        for (var gameInd = 0; gameInd < finalResult.Result.Length; gameInd++)
                        {
                            gamesResult.Add(finalResult.Result[gameInd].ToString().First());
                        }
                        var keyResult = String.Join("", gamesResult);
                        if (!unionRepo.ContainsKey(keyResult))
                        {
                            unionRepo.Add(keyResult, finalResult);
                            counterAfter++;
                            for (var gameInd = 0; gameInd < keyResult.Length; gameInd++)
                            {
                                if (!counters.ContainsKey(gameInd))
                                    counters.Add(gameInd, new Container1X2());
                                switch (keyResult[gameInd])
                                {
                                    case '0': //1
                                        counters[gameInd].Item1++;
                                        break;
                                    case '1'://X
                                        counters[gameInd].ItemX++;
                                        break;
                                    case '2'://2
                                        counters[gameInd].Item2++;
                                        break;
                                }
                            }
                        }
                        counterBefore++;
                    }
                }
                var countersPercent = new Dictionary<int, Container1X2>(); //gameindex, <counter1%,counterX%,counter2%>
                foreach (var counter in counters)
                {
                    countersPercent.Add(counter.Key, new Container1X2());
                    var sum = counter.Value.Item1 + counter.Value.ItemX + counter.Value.Item2;
                    countersPercent[counter.Key].Item1 = sum > 0 
                        ? Convert.ToInt32(counter.Value.Item1 * 100 / sum) 
                        : 0;
                    countersPercent[counter.Key].ItemX = sum > 0 
                        ? Convert.ToInt32(counter.Value.ItemX * 100 / sum)
                        : 0; 
                    countersPercent[counter.Key].Item2 = sum > 0 
                        ? Convert.ToInt32(counter.Value.Item2 * 100 / sum) 
                        : 0; 
                }
                var unionResults = new UnionSystemResult(unionRepo.Values.AsEnumerable<CombinedResult>(),
                    counterBefore,
                    counterAfter,
                    counters,
                    countersPercent
                    );
                return unionResults;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to union results of {combinedResults.Count} systems.", ex);
            }
        }
        public void GeneratePlayerFile(IEnumerable<CombinedResult> results, string playerFullPath)
        {
            try
            {
                var resultsRepository = new List<string>();
                foreach (var finalResult in results)
                {
                    var gameResult = new List<char>();
                    for (var gameInd = 0; gameInd < finalResult.Result.Length; gameInd++)
                    {
                        char transformResult = ' ';
                        switch (finalResult.Result[gameInd])
                        {
                            case 0:
                                transformResult = '1';
                                break;
                            case 1:
                                transformResult = 'X';
                                break;
                            case 2:
                                transformResult = '2';
                                break;
                        }
                        gameResult.Add(transformResult);
                    }
                    resultsRepository.Add(String.Join(",", gameResult));
                }
                using (StreamWriter sw = File.CreateText(playerFullPath))
                {
                    foreach (var line in resultsRepository)
                        sw.WriteLine(line);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to generate player file from IEnumerable<CombinedResult>.", ex);
            }
        }
        public void GenerateSmartFile(IEnumerable<CombinedResult> results, string srtFullPath)
        {
            try
            {
                var resultsRepository = new List<string>();
                foreach (var finalResult in results)
                {
                    var gameResult = new List<char>();
                    for (var gameInd = 0; gameInd < finalResult.Result.Length; gameInd++)
                    {
                        char transformResult = ' ';
                        switch (finalResult.Result[gameInd])
                        {
                            case 0:
                                transformResult = '1';
                                break;
                            case 1:
                                transformResult = '2';
                                break;
                            case 2:
                                transformResult = '4';
                                break;
                        }
                        gameResult.Add(transformResult);
                    }
                    resultsRepository.Add(String.Join("", gameResult));
                }
                using (StreamWriter sw = File.CreateText(srtFullPath))
                {
                    foreach (var line in resultsRepository)
                        sw.WriteLine(line);
                }
                //using (var archive = ZipFile.Open(zipFullPath, ZipArchiveMode.Create))
                //{
                //    archive.CreateEntryFromFile(srtFullPath, srtFilename);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to generate player file from IEnumerable<CombinedResult>.", ex);
            }
        }
        public void GeneratePlayerFile(SearchResults seResults, bool cleanOneDiff, string playerFullPath)
        {
            try
            {
                var results = cleanOneDiff == true
                    ? seResults.FinalResults.Where(res => res.IsIncludedInResults == true)
                    : seResults.FinalResults;
                GeneratePlayerFile(results, playerFullPath);
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to generate player file from SearchResults.", ex);
            }
        }

        private List<FilterData> BuildCombinationsData(BetsFormCreateRequest request, CombinationType combinationType)
        {
            var res = new List<FilterData>();
            var combinations = combinationType == CombinationType.Pattern1X2
                ? request.FormSettings.Combination1X2Settings
                : request.FormSettings.CombinationLettersSettings.LettersSettings;
            foreach (var set in combinations)
            {
                res.Add(new FilterData()
                {
                    FilterDataRow = set.Combinations,
                    FilterMin = set.MinMax.Min.ToString(),
                    FilterMax = set.MinMax.Max.ToString(),
                    IsActive = set.IsActive
                });
            }
            return res;
        }
        private List<FilterData> BuildShapesData(BetsFormCreateRequest request, ShapeFilterType shapeType)
        {
            var res = new List<FilterData>();
            List<ShapesSettings> shapes = null;
            switch (shapeType)
            {
                case ShapeFilterType.LetterShapes2:
                    shapes = request.FormSettings.LettersShapes2;
                    break;
                case ShapeFilterType.RegularShapes2:
                    shapes = request.FormSettings.Shapes2;
                    break;
                case ShapeFilterType.LetterShapes3:
                    shapes = request.FormSettings.LettersShapes3;
                    break;
                case ShapeFilterType.RegularShapes3:
                    shapes = request.FormSettings.Shapes3;
                    break;
            }

            foreach (var shape in shapes)
            {
                var shapeRawData = new string[shape.Combinations.Count() * 2];
                var i = 0;
                foreach (var combination in shape.Combinations)
                {
                    shapeRawData[i] = combination.Min.ToString();
                    shapeRawData[i + 1] = combination.Max.ToString();
                    i += 2;
                }
                res.Add(new FilterData()
                {
                    FilterDataRow = shapeRawData,
                    FilterMin = shape.MinMax.Min.ToString(),
                    FilterMax = shape.MinMax.Max.ToString(),
                    IsActive = shape.IsActive,
                    ShapeFilterType = shapeType
                });
            }
            return res;
        }
        private char[] GetMappedLettersRow(decimal[] currentRow)
        {
            decimal min = currentRow.Min();
            decimal max = currentRow.Max();

            if (min == max) return new[] { A, B, C };
            decimal one = currentRow[0], x = currentRow[1], two = currentRow[2];
            if (one != x && one != two && x != two)
            {
                char[] mappedRow = new char[3];
                decimal[] minmaxmid = { one, x, two };
                minmaxmid = minmaxmid.OrderBy(k => k).ToArray();
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (minmaxmid[i] == currentRow[j])
                        {
                            mappedRow[j] = _letters[i][0];
                            break;
                        }
                    }
                }
                return mappedRow;
            }

            if (one == two)
            {
                if (one > x)
                {
                    return new[] { B, A, C };
                }

                if (one < x)
                {
                    return new[] { A, C, B };
                }
            }

            if (two > one) return new[] { A, B, C };

            return new[] { C, B, A };
        }
        public List<IValidator> BuildValidators(BetsFormCreateRequest request, Dictionary<byte, decimal[]> probSettings)
        {
            try
            {
                List<IValidator> validators = new List<IValidator>();
                Dictionary<byte, char[]> mappedProbability = new Dictionary<byte, char[]>();
                for (byte i = 0; i < probSettings.Count(); i++)
                {
                    mappedProbability.Add(i, GetMappedLettersRow(probSettings[i]));
                }

                if (request.FormSettings.ExcludeSettings != null)
                {
                    var excludeConstraintsOptions = BuildExcludeConstraintsOptions(request);
                    validators.Add(new ExcludeConstraintsOptionsValidator(excludeConstraintsOptions, probSettings));
                }
                if (request.FormSettings.Combination1X2Settings != null)
                {
                    var combinationsFiltersData = BuildCombinationsData(request, CombinationType.Pattern1X2);
                    foreach (FilterData filterData in combinationsFiltersData.Where(fd => fd.IsActive))
                        validators.Add(new CombinationsValidator(filterData));
                }
                //if (request.formSettings.CombinationLettersSettings?.LettersSettings != null)
                if (request.FormSettings.CombinationLettersSettings != null)
                {
                    validators.Add(new LettersMinMaxAndSeqValidator(request.FormSettings.CombinationLettersSettings, mappedProbability));

                    var combinationsFiltersData = BuildCombinationsData(request, CombinationType.PatternLetters);
                    foreach (FilterData filterData in combinationsFiltersData.Where(fd => fd.IsActive))
                        validators.Add(new LettersValidator(filterData, mappedProbability));
                }
                if (request.FormSettings.Shapes2 != null)
                {
                    var shapesData = BuildShapesData(request, ShapeFilterType.RegularShapes2);
                    foreach (FilterData filterData in shapesData.Where(fd => fd.IsActive))
                        validators.Add(new RegularShapesValidator(filterData));
                }
                if (request.FormSettings.LettersShapes2 != null)
                {
                    var shapesData = BuildShapesData(request, ShapeFilterType.LetterShapes2);
                    foreach (FilterData filterData in shapesData.Where(fd => fd.IsActive))
                        validators.Add(new LettersShapesValidator(filterData, mappedProbability));
                }
                if (request.FormSettings.Shapes3 != null)
                {
                    var shapesData = BuildShapesData(request, ShapeFilterType.RegularShapes3);
                    foreach (FilterData filterData in shapesData.Where(fd => fd.IsActive))
                        validators.Add(new RegularShapesValidator(filterData));
                }
                if (request.FormSettings.LettersShapes3 != null)
                {
                    var shapesData = BuildShapesData(request, ShapeFilterType.LetterShapes3);
                    foreach (FilterData filterData in shapesData.Where(fd => fd.IsActive))
                        validators.Add(new LettersShapesValidator(filterData, mappedProbability));
                }

                return validators;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to build validators.", ex);
            }
        }

        public Dictionary<byte, decimal[]> BuildProbSettings(BetsFormCreateRequest request)
        {
            try
            {
                Dictionary<byte, decimal[]> output = new Dictionary<byte, decimal[]>();
                foreach (var ratesLine in request.EventsRates)
                {
                    output.Add(Convert.ToByte(ratesLine.position), new[] { Convert.ToDecimal(ratesLine.rate1), Convert.ToDecimal(ratesLine.rateX), Convert.ToDecimal(ratesLine.rate2) });
                }
                return output;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to build prob settings.", ex);
            }
        }

        public SearchResults CalculateBets(BetsCalculationInputs input)
        {
            try
            {
                SearchEngine se = new SearchEngine(input.ProbSettings, input.ConstrSettings, input.Validators, null, false, input.CleanOneDiff);
                SearchResults seResults = se.Search();

                return seResults;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to calculate bets.", ex);
            }
        }
        public void GenerateExcelFile(IEnumerable<CombinedResult> finalResults, string fullFilename, string excelTemplatePath)
        {
            try
            {
                if (!File.Exists(excelTemplatePath))
                    throw new Exception($"Excel template file was not found: {excelTemplatePath}");

                var combinedResultsToPrint = finalResults.ToList();
                var fullFileName = ResultsToExcelSaver.SaveToFile(combinedResultsToPrint, fullFilename, excelTemplatePath);
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to save results to excel.", ex);
            }

        }
        //REMOVE THIS CODE ONCE FORM DISPATCHER IS APPROVED
        public string ResultsToExcel(SearchResults calcResult, bool isOneDiffCleanChecked, string fullFilename, string excelTemplatePath)
        {
            try
            {
                if (!File.Exists(excelTemplatePath))
                    throw new Exception($"Excel template file was not found: {excelTemplatePath}");

                var finalResults = calcResult.FinalResults.Where(cr => !isOneDiffCleanChecked || cr.IsIncludedInResults);
                var combinedResultsToPrint = finalResults.ToList();
                var fullFileName = ResultsToExcelSaver.SaveToFile(combinedResultsToPrint, fullFilename, excelTemplatePath);
                return fullFileName;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured trying to save results to excel.", ex);
            }
        }

        public SearchResults CalculateSystem(BetsFormCreateRequest request)
        {
            try
            {
                request.Validate();
                var constrSettings = this.BuildConstrSettings(request);
                var probSettings = this.BuildProbSettings(request);
                var validators = this.BuildValidators(request, probSettings);
                var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, request.CleanOnDiff);

                var seResults = this.CalculateBets(betInputs);
                return seResults;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to calculate system with id {request.FormId}", ex);
            }
        }
    }
}
//private SearchResults HandleRequest(FormCreateRequest request, string fullFileName)
//{
//    ConstraintsSettings constrSettings = GetConstrSettingsFromRequest(request);

//    Dictionary<byte, decimal[]> probSettings = GetProbSettingsFromRequest(request);

//    SearchEngine se = new SearchEngine(probSettings, constrSettings, null, null, false, false);
//    SearchResults seResults = se.Search();

//    ResultsToExcelSaver.SaveResultsToExel(seResults.FinalResults, fullFileName);

//    return seResults;
//}
