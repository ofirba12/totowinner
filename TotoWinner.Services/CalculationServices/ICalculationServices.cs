﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public interface ICalculationServices
    {
        SearchResults CalculateBets(BetsCalculationInputs input);

        ConstraintsSettings BuildConstrSettings(BetsFormCreateRequest request);
        Dictionary<byte, decimal[]> BuildProbSettings(BetsFormCreateRequest request);
        List<IValidator> BuildValidators(BetsFormCreateRequest request, Dictionary<byte, decimal[]> probSettings);
        string ResultsToExcel(SearchResults calcResult, bool isOneDiffCleanChecked, string fullFilename, string excelTemplatePath);
        SearchResults CalculateSystem(BetsFormCreateRequest request);
        UnionSystemResult UnionResults(List<IEnumerable<CombinedResult>> combinedResults);
        void GeneratePlayerFile(SearchResults seResults, bool cleanOneDiff, string playerFullPath);
        void GeneratePlayerFile(IEnumerable<CombinedResult> results, string playerFullPath);
        void GenerateSmartFile(IEnumerable<CombinedResult> results, string srtFullPath);
    }
}
