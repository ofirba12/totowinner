﻿using System;
using System.Collections.Generic;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Services.Persistance.TotoWinner;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class PersistanceServices : Singleton<PersistanceServices>, IPersistanceServices
    {
        private InMemoryBacktestingPersistanceServices _inMemoryBtPersistanceSrv = InMemoryBacktestingPersistanceServices.Instance;

        private PersistanceServices() { }
        #region GetGamesValidations
        public List<GamesValidation> GetGamesValidations(int programId)
        {
            try
            {
                List<GamesValidation> coll = null;
                using (var ent = new TWEntities())
                {
                    coll = (from validation in ent.GamesValidations
                            where validation.ProgramId == programId
                            select validation)
                               .ToList();
                }
                return coll;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get games validation for programId {programId}", ex);
            }
        }
        #endregion

        #region GetGames
        public List<Game> GetGames(int programId)
        {
            try
            {
                List<Game> gamesColl = null;
                using (var ent = new TWEntities())
                {
                    gamesColl = (from games in ent.Games
                                 where games.ProgramId == programId
                                 select games)
                               .ToList();
                }
                return gamesColl;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get games for programId {programId}", ex);
            }
        }
        #endregion

        #region GetProgramsData
        public List<Program> GetProgramsData(ProgramType programType, DateTime fromEndDate, DateTime toEndDate)
        {
            try
            {
                List<Program> programs = null;
                using (var ent = new TWEntities())
                {
                    programs = (from program in ent.Programs
                                where (ProgramType)program.Type == programType &&
                                    (program.EndDate >= fromEndDate.Date && program.EndDate <= toEndDate.Date)
                                orderby program.EndDate descending
                                select program)
                               .ToList();
                }
                return programs;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get {programType} programs between dates {fromEndDate} - {toEndDate}", ex);
            }
        }

        public List<Program> GetProgramsData(List<int> programIds)
        {
            try
            {
                List<Program> programs = null;
                using (var ent = new TWEntities())
                {
                    programs = (from program in ent.Programs
                                where programIds.Contains(program.Id)
                                select program)
                               .ToList();
                }
                return programs;
            }
            catch (Exception ex)
            {
                var input = string.Join(",", programIds);
                throw new Exception($"An error occurred trying to get programs data for ids {input}", ex);
            }
        }
        public List<Program> GetProgramsDataByProgramNumbers(List<int> programNumbers)
        {
            try
            {
                List<Program> programs = null;
                using (var ent = new TWEntities())
                {
                    programs = (from program in ent.Programs
                                where programNumbers.Contains(program.Number)
                                select program)
                               .ToList();
                }
                return programs;
            }
            catch (Exception ex)
            {
                var input = string.Join(",", programNumbers);
                throw new Exception($"An error occurred trying to get programs data for program numbers {input}", ex);
            }
        }

        #endregion

        #region LookforOpenForBetsPrograms
        public List<int> LookforOpenForBetsPrograms()
        {
            try
            {
                List<int> programsIds = null;
                using (var ent = new TWEntities())
                {
                    programsIds = (from game in ent.Games
                                   where game.OpenForBets == true
                                   select game.ProgramId)
                                  .Distinct()
                                  .ToList();
                }
                return programsIds;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred trying to look for program open for bets program ids", ex);
            }
        }
        #endregion

        #region IsProgramsDataEmpty
        public bool IsProgramsDataEmpty()
        {
            try
            {
                var result = false;
                using (var ent = new TWEntities())
                {
                    result = !ent.Programs.Any();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred trying to check if there is no programs data", ex);
            }
        }
        #endregion

        #region ProgramUpsert
        public int ProgramUpsert(ProgramType programType, short year, int totoRoundId, DateTime endDate)
        {
            try
            {
                var programId = -1;
                using (var ent = new TWEntities())
                {
                    var result = ent.Programs_Merge((byte)programType,
                        year,
                        totoRoundId,
                        endDate);
                    programId = result.First().Value;
                }
                return programId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to Upsert Program programType: [{programType}], year: [{year}], totoRoundId: [{totoRoundId}], endDate: [{endDate}]",
                    ex);
            }
        }
        #endregion

        #region GameUpsert
        public void GameUpsert(int programId, byte index, string gameTime, string description,
            string leagueName,
            string gameResult,
            bool openForBets,
            string betColor,
            double rate1,
            double rateX,
            double rate2,
            string won,
            List<ProgramValidationStatus> validations)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        var result = ent.Games_Merge(programId, index, gameTime, description,
                            leagueName, gameResult, openForBets, betColor, rate1, rateX, rate2, won);
                        ent.GamesValidations_Delete(programId);
                        foreach (var validation in validations)
                        {
                            if (validation != ProgramValidationStatus.ValidForCalculation)
                            {
                                ent.GamesValidation_Merge(programId,
                                    index,
                                    (byte)validation);
                            }
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to Upsert Game programId: [{programId}], index: [{index}], gameTime: [{gameTime}], description: [{description}]",
                    ex);
            }
        }
        #endregion

        #region SystemAddOrUpdate
        public int SystemAddOrUpdate(SystemParameters system)
        {
            try
            {
                var systemId = -1;
                using (var ent = new TWEntities())
                {
                    ent.Database.CommandTimeout = 180;
                    var result = ent.System_Merge((short)system.Type, system.Name, system.ProgramId, (short)system.ProgramType,
                        system.SystemPayload);
                    systemId = result.First().Value;
                }
                return systemId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to AddOrUpdate twSystem Name: [{system.Name}], Type: [{system.ProgramType}], ProgramId: [{system.ProgramId}], Id: [{system.SystemId}]",
                    ex);
            }
        }
        #endregion

        #region SystemDelete
        public void SystemDelete(int systemId)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    var twSystem = ent.System_Delete(systemId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete twSystem Id: [{systemId}]",
                    ex);
            }
        }
        //public void SystemDelete(int systemId)
        //{
        //    try
        //    {
        //        using (var ent = new TWEntities())
        //        {
        //            var twSystem = ent.TWSystems.FirstOrDefault(s => s.Id == systemId);// .Find(system.SystemId);//Find(systemId);
        //            if (twSystem != null)
        //            {
        //                ent.TWSystems.Remove(twSystem);
        //                ent.SaveChanges();
        //            }
        //            else
        //            {
        //                throw new Exception("twSystem was not found");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"An error occurred trying to delete twSystem Id: [{systemId}]",
        //            ex);
        //    }
        //}
        #endregion

        #region SystemLoad
        public SystemParameters SystemLoad(int systemId)
        {
            try
            {
                SystemParameters result = null;
                using (var ent = new TWEntities())
                {
                    var twSystem = ent.System_Get(systemId, null);
                    result = ToSystemParameters(twSystem.First());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to load twSystem Id: [{systemId}]",
                    ex);
            }
        }
        #endregion

        #region GetSystems
        public List<SystemParameters> GetSystems(SystemType type)
        {
            try
            {
                List<SystemParameters> systems = null;
                using (var ent = new TWEntities())
                {
                    var query = ent.System_Get(null, (short)type)
                        .ToList()
                        .ConvertAll<SystemParameters>(tw => this.ToSystemParameters(tw));
                    systems = query.ToList();
                }

                return systems;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all systems information for type {type}",
                    ex);
            }
        }

        #endregion

        #region ExecutionStatusUpsert
        public int ExecutionStatusUpsert(int? executionId, SystemType systemType, ExecutionStatus status, string parameters = null, string results = null)
        {
            try
            {
                var exId = executionId;
                using (var ent = new TWEntities())
                {
                    var result = ent.Execution_StatusMerge(executionId, (byte)systemType, (short)status, parameters, results);
                    exId = result.First();
                }
                return exId.Value;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to upsert (merge) execution status: executionId {executionId} | status {status} | parameters {parameters}",
                    ex);
            }
        }
        #endregion

        #region ExecutionGet
        public ExecutionRecord ExecutionGet(int executionId)
        {
            try
            {
                ExecutionRecord record = null;
                using (var ent = new TWEntities())
                {
                    var result = ent.Execution_StatusGet(executionId);
                    var entity = result.FirstOrDefault();
                    if (entity != null)
                        record = new ExecutionRecord()
                        {
                            ExecutionId = entity.ExecutionId,
                            Status = (ExecutionStatus)entity.Status,
                            Parameters = entity.Parameters,
                            Results = entity.Results
                        };
                }
                return record;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get execution status: executionId {executionId}",
                    ex);
            }
        }
        #endregion

        #region ExecutionOptimizedBetsUpsert
        public void ExecutionOptimizedBetsUpsert(int? executionId, int gameIndex, string bet, bool isFinalBet)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    ent.Execution_OptimizedBetsMerge(executionId, gameIndex, bet, isFinalBet);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to upsert (merge) execution optimized bet: executionId {executionId} | gameIndex {gameIndex} | bet {bet} | | isFinalBet {isFinalBet}",
                    ex);
            }
        }
        #endregion

        #region ExecutionOptimizedBetsGet
        public List<OptimizedBet> ExecutionOptimizedBetsGet(int executionId)
        {
            try
            {
                List<OptimizedBet> result = null;
                using (var ent = new TWEntities())
                {
                    var records = ent.Execution_OptimizedBetsGet(executionId);
                    result = records.ToList().Select(item => new OptimizedBet(item.GameIndex, item.Bet, item.IsFinalBet)).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get execution optimized bet: executionId {executionId}",
                    ex);
            }
        }
        #endregion

        #region ExecutionQueuedGet
        public ExecutionRecord ExecutionQueuedGet()
        {
            try
            {
                ExecutionRecord result = null;
                using (var ent = new TWEntities())
                {
                    var record = ent.Execution_GetQueued();
                    var res = record.FirstOrDefault();
                    if (res != null)
                        result = new ExecutionRecord()
                        {
                            ExecutionId = res.ExecutionId,
                            Status = (ExecutionStatus)res.Status,
                            Parameters = res.Parameters,
                            SystemType = (SystemType)res.SystemType
                        };
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get queued execution",
                    ex);
            }
        }
        #endregion

        public List<OptimizedCondition> ExecutionOptimizedConditionsGet(int executionId)
        {
            try
            {
                List<OptimizedCondition> result = null;
                using (var ent = new TWEntities())
                {
                    var records = ent.Execution_OptimizedConditions_Get(executionId);
                    result = records.ToList().Select(item =>
                        new OptimizedCondition(item.ConditionTitle,
                            (ConditionType)item.ConditionType,
                            new MinMax(Convert.ToDecimal(item.Min), Convert.ToDecimal(item.Max)))).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get execution optimized condition: executionId {executionId}",
                    ex);
            }
        }
        public void ExecutionOptimizedConditionUpsert(int? executionId, string title, ConditionType type, double min, double max)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    ent.Execution_OptimizedConditionsMerge(executionId, (byte)type, title, min, max);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to upsert (merge) execution optimized condition: executionId {executionId} | title {title} | type {type} | min {min} | max {max}",
                    ex);
            }
        }
        public List<OptimizedAdvancedCondition> ExecutionOptimizedAdvancedConditionsGet(int executionId)
        {
            try
            {
                List<OptimizedAdvancedCondition> result = null;
                using (var ent = new TWEntities())
                {
                    var records = ent.Execution_OptimizedAdvancedConditions_Get(executionId);
                    result = records.ToList().Select(item =>
                        new OptimizedAdvancedCondition((AdvancedConditionType)item.ConditionType,
                            item.Index,
                            new MinMax(Convert.ToDecimal(item.Min), Convert.ToDecimal(item.Max)))).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get execution optimized advanced condition: executionId {executionId}",
                    ex);
            }
        }
        public void ExecutionOptimizedAdvancedConditionUpsert(int? executionId, AdvancedConditionType type, int index, double min, double max)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    ent.Execution_OptimizedAdvancedConditionsMerge(executionId, (byte)type, (byte)index, min, max);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to upsert (merge) execution optimized advanced condition: executionId {executionId} | type {type} | index {index} | min {min} | max {max}",
                    ex);
            }
        }

        #region Helpers
        private SystemParameters ToSystemParameters(System_Get_Result twSystem)
        {
            try
            {
                var result = new SystemParameters()
                {
                    Name = twSystem.Name,
                    ProgramId = twSystem.ProgramId,
                    ProgramType = (ProgramType)twSystem.ProgramType,
                    SystemId = twSystem.Id,
                    Type = (SystemType)twSystem.Type,
                    RefrenceSystemId = twSystem.ReferenceId,
                    SystemPayload = twSystem.RawData,
                    LastUpdate = twSystem.LastUpdate
                };
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to convert TWSystem to SystemParameteres",
                    ex);
            }
        }

        public void ShrinkUpdateReference(int shrinkSystemId, int optimizerSystemId)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    ent.ShrinkUpdateReference(shrinkSystemId, optimizerSystemId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred updating shrink system {shrinkSystemId} reference to optimizer {optimizerSystemId}",
                    ex);
            }
        }

        #endregion

        #region AddBacktestingResults
        public void AddBacktestingResults(int executionId, int programId,
            Dictionary<short, Bet> bets,
            Dictionary<short, BacktestingStrikeResult> results,
            bool useInMemoryPersistance = false)
        {
            try
            {
                var baseBets = bets.Select(kvp =>
                {
                    return new BaseBet()
                    {
                        ExecutionId = executionId,
                        ProgramId = programId,
                        GameIndex = kvp.Key,
                        Win_1 = kvp.Value.bet1,
                        Win_X = kvp.Value.betX,
                        Win_2 = kvp.Value.bet2
                    };
                }).ToList();

                var backtestingResults = results.Select(kvp =>
                {
                    return new BacktestingStrikeResult()
                    {
                        ExecutionId = executionId,
                        ProgramId = programId,
                        StrikeBet = kvp.Key,
                        Counter = kvp.Value.Counter
                    };
                }).ToList();

                if (useInMemoryPersistance)
                {
                    _inMemoryBtPersistanceSrv.AddBacktestingResults(executionId, programId, baseBets, backtestingResults);
                }
                else
                {
                    using (var ent = new TWEntities())
                    {
                        using (var transaction = ent.Database.BeginTransaction())
                        {
                            ent.ExecuteTableValueProcedure<BaseBet>(baseBets, "InsertBacktestingBaseBets", "@BaseBets", "TWBacktestingBaseBetsTVP");
                            ent.ExecuteTableValueProcedure<BacktestingStrikeResult>(backtestingResults, "InsertBacktestingResults", "@BacktestingResults", "TWBacktestingResultsTVP");
                            transaction.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add backtesting results for executionId={executionId}, programId={programId}",
                    ex);
            }
        }
        #endregion

        #region FetchBacktestingBaseBets
        public List<TWBacktestingBaseBet> FetchBacktestingBaseBets(int executionId, int programId, bool useInMemoryPersistance = false)
        {
            try
            {
                List<TWBacktestingBaseBet> baseBets = null;
                if (useInMemoryPersistance)
                {
                    var key = new BacktestingResultKey(executionId, programId);
                    baseBets = _inMemoryBtPersistanceSrv.BaseBets[key]
                        .ConvertAll<TWBacktestingBaseBet>(i => new TWBacktestingBaseBet()
                        {
                            ExecutionId = key.ExecutionId,
                            ProgramId = key.ProgramId,
                            GameIndex = (byte)i.GameIndex,
                            Win_1 = i.Win_1,
                            Win_2 = i.Win_2,
                            Win_X = i.Win_X,
                            Inserted = DateTime.Now
                        });
                }
                else
                {
                    using (var ent = new TWEntities())
                    {
                        baseBets = ent.TWBacktestingBaseBets.Where(item => item.ExecutionId == executionId && item.ProgramId == programId).ToList();
                    }
                }
                return baseBets;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch backtesting base bets for executionId={executionId}, programId={programId}",
                    ex);
            }
        }
        #endregion

        #region GetBacktestingProgramResults
        public List<TWBacktestingResult> GetBacktestingProgramResults(int executionId, bool useInMemoryPersistance = false)
        {
            try
            {
                List<TWBacktestingResult> results = null;
                if (useInMemoryPersistance)
                {
                    results = new List<TWBacktestingResult>();
                    foreach (var key in _inMemoryBtPersistanceSrv.BacktestingResults.Keys)
                        foreach (var val in _inMemoryBtPersistanceSrv.BacktestingResults[key])
                        {
                            results.Add(new TWBacktestingResult()
                            {
                                Inserted = DateTime.Now,
                                ExecutionId = key.ExecutionId,
                                ProgramId = key.ProgramId,
                                BetStrike = (byte)val.StrikeBet,
                                Counter = val.Counter
                            });
                        }
                }
                else
                {
                    using (var ent = new TWEntities())
                    {
                        results = ent.TWBacktestingResults.Where(i => i.ExecutionId == executionId)
                            .OrderBy(j => j.Inserted)
                            .ToList();
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get backtesting program results for executionId={executionId}",
                    ex);
            }
        }
        #endregion

        #region Templates
        #region TemplateUpsert
        public List<Template> TemplateGet(int? templateId)
        {
            try
            {
                List<Template> templates;
                using (var ent = new TWEntities())
                {
                    var result = ent.Template_Get(templateId).ToList();
                    templates = result.ConvertAll<Template>(i => new Template(i.Id, (ProgramType)i.Type, i.Name, i.Value));

                }
                return templates;
            }
            catch (Exception ex)
            {
                var templateIdStr = templateId.HasValue
                    ? templateId.Value.ToString()
                    : "All";
                throw new Exception($"An error occurred trying to get template id [{templateIdStr}]",
                    ex);
            }
        }
        #endregion

        #region TemplateUpsert
        public int TemplateUpsert(Template template)
        {
            try
            {
                var templateId = -1;
                using (var ent = new TWEntities())
                {
                    var result = ent.Template_Merge((byte)template.Type,
                        template.Name,
                        template.Value);
                    templateId = result.First().Value;
                }
                return templateId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to Upsert Template [{template.ToString()}]",
                    ex);
            }
        }
        #endregion

        #region TemplateDelete
        public void TemplateDelete(int templateId)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    var twSystem = ent.Template_Delete(templateId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete Template with Id: [{templateId}]",
                    ex);
            }
        }
        #endregion
        #endregion

        #region UnionSystemResults
        public int UnionSystemResultsInsert(string systemIds, string serializedResultsData)
        {
            try
            {
                var unionResultId = -1;
                using (var ent = new TWEntities())
                {
                    var result = ent.UnionSystemResults_Merge(unionResultId, systemIds, null, serializedResultsData);
                    unionResultId = result.First().Value;
                }
                return unionResultId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to insert union system results of systemIds: [{systemIds}]",
                    ex);
            }
        }
        public void UnionSystemResultsUpdate(int unionResultId, string smartFilename)
        {
            try
            {
                using (var ent = new TWEntities())
                {
                    ent.UnionSystemResults_Merge(unionResultId, null, smartFilename, null);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update union system results with id [{unionResultId}], smart filename [{smartFilename}]",
                    ex);
            }
        }
        public string UnionSystemResultsGetResults(int unionResultId)
        {
            try
            {
                var rawUnionResultData = string.Empty;
                using (var ent = new TWEntities())
                {
                    var results = ent.UnionSystemResults_Get(unionResultId);
                    rawUnionResultData = results.First().RawResultsData;
                }
                return rawUnionResultData;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get union system results (only results) with id [{unionResultId}]",
                    ex);
            }
        }
        public string UnionSystemResultsGetSmartFile(int unionResultId)
        {
            try
            {
                var smart = string.Empty;
                using (var ent = new TWEntities())
                {
                    var results = ent.UnionSystemResults_Get(unionResultId);
                    smart = results.First().SMART;
                }
                return smart;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get union system results smart file with id [{unionResultId}]",
                    ex);
            }
        }
        public List<SmartFileContainer> GetSmartFilesToDownload()
        {
            try
            {
                List<SmartFileContainer> files;
                using (var ent = new TWEntities())
                {
                    var results = ent.UnionSystemResults_GetAllSmartFiles();
                    files = results.ToList()
                        .ConvertAll<SmartFileContainer>(file =>
                        {
                            return new SmartFileContainer(file.Id, file.SystemIds, file.LastUpdate, file.SMART);
                        });
                }
                return files;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all smart file to download",
                    ex);
            }
        }
        #endregion

        #region RealTimeRatesPipe
        public void RealTimeRatesPipeUsert(ProgramType programType, List<RatesPipe> rates)
        {
            try
            {
                var rateTVP = rates.ConvertAll<RatesPipeTableValueTypeItem>(item =>
                {
                    return new RatesPipeTableValueTypeItem()
                    {
                        ProgramType = (byte)programType,
                        GameIndex = item.GameIndex,
                        Rate1 = item.Rate1,
                        RateX = item.RateX,
                        Rate2 = item.Rate2
                    };
                });
                using (var ent = new TWEntities())
                {
                    ent.ExecuteTableValueProcedure<RatesPipeTableValueTypeItem>(rateTVP, "RealTimeRatesPipe_Merge", "@Rates", "RealTimeRatesPipeTVP");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to upsert real time rates pipe",
                    ex);
            }
        }

        public List<RatesPipe> GetRealTimeRatesPipe(ProgramType programType)
        {
            try
            {
                List<RatesPipe> result = null;
                using (var ent = new TWEntities())
                {
                    result = ent.RealTimeRatesPipe_Get((byte)programType)
                        .ToList()
                        .ConvertAll<RatesPipe>(rate =>
                        {
                            return new RatesPipe()
                            {
                                GameIndex = rate.GameIndex,
                                Rate1 = rate.Rate1,
                                RateX = rate.RateX,
                                Rate2 = rate.Rate2,
                            };
                        }
                        );
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get rates pipe for {programType}",
                    ex);
            }
        }
        #endregion
    }
}