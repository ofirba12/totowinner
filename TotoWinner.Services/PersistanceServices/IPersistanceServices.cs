﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;
using TotoWinner.Services.Persistance.TotoWinner;

namespace TotoWinner.Services
{
    public interface IPersistanceServices
    {
        int SystemAddOrUpdate(SystemParameters system);
        void ShrinkUpdateReference(int shrinkSystemId, int optimizerSystemId);
        SystemParameters SystemLoad(int systemId);
        void SystemDelete(int systemId);
        List<SystemParameters> GetSystems(SystemType type);
        ExecutionRecord ExecutionQueuedGet();
        ExecutionRecord ExecutionGet(int executionId);
        int ExecutionStatusUpsert(int? executionId, SystemType systemType, ExecutionStatus status, string parameters, string results);
        List<OptimizedBet> ExecutionOptimizedBetsGet(int executionId);
        void ExecutionOptimizedBetsUpsert(int? executionId, int gameIndex, string bet, bool isFinalBet);

        List<OptimizedCondition> ExecutionOptimizedConditionsGet(int executionId);
        void ExecutionOptimizedConditionUpsert(int? executionId, string title, ConditionType type, double min, double max);
        List<OptimizedAdvancedCondition> ExecutionOptimizedAdvancedConditionsGet(int executionId);
        void ExecutionOptimizedAdvancedConditionUpsert(int? executionId, AdvancedConditionType type, int index, double min, double max);

        int ProgramUpsert(ProgramType programType, short year, int totoRoundId, DateTime endDate);
        void GameUpsert(int programId, byte index, string gameTime, string description,
            string leagueName,
            string gameResult,
            bool openForBets,
            string betColor,
            double rate1,
            double rateX,
            double rate2,
            string won,
            List<ProgramValidationStatus> validations);
        bool IsProgramsDataEmpty();
        List<int> LookforOpenForBetsPrograms();
        List<Program> GetProgramsData(List<int> programIds);
        List<Program> GetProgramsDataByProgramNumbers(List<int> programNumbers);
        List<Program> GetProgramsData(ProgramType programType, DateTime fromEndDate, DateTime toEndDate);

        List<GamesValidation> GetGamesValidations(int programId);
        List<Game> GetGames(int programId);
        List<TWBacktestingBaseBet> FetchBacktestingBaseBets(int executionId, int programId, bool useInMemoryPersistance = false);
        void AddBacktestingResults(int executionId, int programId,
            Dictionary<short, Bet> bets,
            Dictionary<short, BacktestingStrikeResult> results,
            bool useInMemoryPersistance = false);
        List<TWBacktestingResult> GetBacktestingProgramResults(int executionId, bool useInMemoryPersistance = false);

        int TemplateUpsert(Template template);
        void TemplateDelete(int templateId);
        List<Template> TemplateGet(int? templateId);

        int UnionSystemResultsInsert(string systemIds, string serializedResultsData);
        void UnionSystemResultsUpdate(int unionResultId, string smartFilename);
        string UnionSystemResultsGetResults(int unionResultId);
        string UnionSystemResultsGetSmartFile(int unionResultId);
    }
}
