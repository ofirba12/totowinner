﻿using System;
using System.Collections.Generic;
using TotoWinner.Common.Infra;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class InMemoryBacktestingPersistanceServices : Singleton<InMemoryBacktestingPersistanceServices>, IInMemoryBacktestingPersistanceServices
    {
        public List<ProgramWithGames> ProgramWithGamesRepository { get; private set; }
        public Dictionary<BacktestingResultKey, List<BaseBet>> BaseBets  { get; private set; }
        public Dictionary<BacktestingResultKey, List<BacktestingStrikeResult>> BacktestingResults { get; private set; }
        public Dictionary<int, ExecutionStatus> ExecutionStatus { get; private set; }
        public Dictionary<int, string> ExecutionTemplatesRepo { get; private set; }
        public int CurrentExecutionId { get; private set; }
        private InMemoryBacktestingPersistanceServices()
        {
            this.ExecutionStatus = new Dictionary<int, ExecutionStatus>();
            this.CurrentExecutionId = 0;
            this.BaseBets = new Dictionary<BacktestingResultKey, List<BaseBet>>();
            this.BacktestingResults = new Dictionary<BacktestingResultKey, List<BacktestingStrikeResult>>();
            this.ExecutionTemplatesRepo = new Dictionary<int, string>();
        }

        public void AddProgramWithGamesRepository(List<ProgramWithGames> repo)
        {
            this.ProgramWithGamesRepository = repo;
        }
        public void AddBacktestingResults(int executionId, int programId, List<BaseBet> baseBets, List<BacktestingStrikeResult> backtestingResults)
        {
            var key = new BacktestingResultKey(executionId, programId);
            if (!this.BaseBets.ContainsKey(key))
                this.BaseBets.Add(key, null);
            this.BaseBets[key] = baseBets;

            if (!this.BacktestingResults.ContainsKey(key))
                this.BacktestingResults.Add(key, null);
            this.BacktestingResults[key] = backtestingResults;
        }

        public void UpdateExecutionStatus(int executionId, ExecutionStatus status)
        {
            if (!this.ExecutionStatus.ContainsKey(executionId))
                this.ExecutionStatus.Add(executionId, status);
            else
                this.ExecutionStatus[executionId] = status;
        }

        public int StartExecution(string template)
        {
            var executionId = ++this.CurrentExecutionId;
            this.ExecutionTemplatesRepo.Add(executionId, template);
            return executionId;
        }
    }
}