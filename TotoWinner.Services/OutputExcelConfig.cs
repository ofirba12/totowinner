﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Collections.Generic;
using System.IO;

namespace TotoWinner.Services
{
    class OutputExcelConfig
    {
        private short ALLOWEDROWS_COUNT = 30;
        private short ALLOWEDCOLS_COUNT = 16;


        public short[] AllowedRows { get; private set; }
        public short[] AllowedCols { get; private set; }
        public Dictionary<short, short> RowsHeights { get; private set; }
        public Dictionary<short, int> ColsWidths { get; private set; }

        public IFont Font { get; private set; }

        public OutputExcelConfig(string filePath)
        {
            AllowedCols = new short[ALLOWEDCOLS_COUNT];
            AllowedRows = new short[ALLOWEDROWS_COUNT];
            RowsHeights = new Dictionary<short, short>();
            ColsWidths = new Dictionary<short, int>();
            ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = 437;

            ReadConfig(filePath);
        }

        private void ReadConfig(string filePath)
        {
            XSSFWorkbook hssfwb;
            using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                hssfwb = new XSSFWorkbook(file);
            }

            ISheet sheet = hssfwb.GetSheet("Example");


            for (short rowId = 0; rowId < 40; rowId++)
            {
                var row = sheet.GetRow(rowId);
                RowsHeights.Add(rowId, row == null ? sheet.DefaultRowHeight : row.Height);
            }


            for (short colId = 0; colId < 30; colId++)
            {
                ColsWidths.Add(colId, sheet.GetColumnWidth(colId));
            }

            short numOfCollectedRows = 0;
            short numOfCollectedCols = 0;
            for (short colId = 0; colId < 30; colId++)
            {
                bool isColumnSet = false;
                for (short rowId = 0; rowId < 40; rowId++)
                {
                    var row = sheet.GetRow(rowId);
                    if (row == null) continue;

                    var cell = row.GetCell(colId);
                    if (cell == null) continue;

                    if (cell.StringCellValue == "x")
                    {
                        if (numOfCollectedRows < ALLOWEDROWS_COUNT)
                        {
                            AllowedRows[numOfCollectedRows++] = rowId;
                        }

                        if (numOfCollectedCols < ALLOWEDCOLS_COUNT && !isColumnSet)
                        {
                            AllowedCols[numOfCollectedCols++] = colId;
                            isColumnSet = true;
                        }

                        if (Font == null)
                        {
                            Font = cell.CellStyle.GetFont(hssfwb);
                        }
                    }
                }
            }
        }
    }
}
