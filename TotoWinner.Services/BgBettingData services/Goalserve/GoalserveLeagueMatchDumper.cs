﻿using log4net;
using System.Collections.Generic;
using System.IO;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class GoalserveLeagueMatchDumper
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool IsActive { get; }
        public int LeagueId { get; }
        public int MatchId { get; }

        public GoalserveLeagueMatchDumper(bool isActive)
        {
            this.IsActive = isActive;
        }
        public GoalserveLeagueMatchDumper(int leagueId, int matchId)
        {
            this.IsActive = true;
            this.LeagueId = leagueId;
            this.MatchId = matchId;
        }

        public void DumpBookmakers(int leagueId, int matchId, List<BgGoalServeBookmaker> bookmakers)
        {
            if (this.IsActive && this.LeagueId == leagueId && this.MatchId == matchId)
            {
                _log.Debug($">> Bookmakers: LeagueId={leagueId}, MatchId={matchId}");
                foreach (var book in bookmakers)
                    _log.Debug($">> {book.ToString()}");
            }
        }

        internal void DumpJson(int? leagueId, string cashFilePath, string jsonResponse)
        {
            if (this.IsActive && (!leagueId.HasValue || this.LeagueId == leagueId))
                File.WriteAllText(cashFilePath, jsonResponse);
        }

        internal void DumpAllMatchesFromOdds(int leagueId, List<Dictionary<int, List<BgGoalServeBookmaker>>> matchOddsCollection)
        {
            if (this.IsActive && this.LeagueId == leagueId)
            {
                var matches = new List<int>();
                foreach (var item in matchOddsCollection)
                    matches.AddRange(item.Keys);
                _log.Debug($">> Matches analyzed from odds: LeagueId={leagueId}");
                foreach (var match in matches)
                    _log.Debug($">> {match}");
            }
        }
        internal void DumpAllMatches(int leagueId, List<MatchKey> matches)
        {
            if (this.IsActive && this.LeagueId == leagueId)
            {
                _log.Debug($">> Matches found: LeagueId={leagueId}");
                foreach (var match in matches)
                    _log.Debug($">> {match.ToString()}");
            }
        }
    }
}
