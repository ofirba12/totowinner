﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;

namespace TotoWinner.Services
{
    public partial class GoalServeProviderServices : Singleton<GoalServeProviderServices>, IGoalServeProviderServices
    {
        private static readonly ILog _log = LogManager.GetLogger("GoldserveGenerator");
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        Dictionary<GoalServeSportType, Dictionary<GoalServeDataAttributes, string>> AttribuetsMapper;
        private GoalserveLeagueMatchDumper dumper;
        private GoalServeProviderServices()
        {
            Initialize();
        }
        private void Initialize()
        {
            if (!Directory.Exists(BgBettingDataConstants.RepositoryPath))
                Directory.CreateDirectory(BgBettingDataConstants.RepositoryPath);
            if (!Directory.Exists(BgBettingDataConstants.ResultPath))
                Directory.CreateDirectory(BgBettingDataConstants.ResultPath);
            InitMappers();
        }
        #region Mappers
        private void InitMappers()
        {
            this.AttribuetsMapper = new Dictionary<GoalServeSportType, Dictionary<GoalServeDataAttributes, string>>();
            var soccerAttributes = new Dictionary<GoalServeDataAttributes, string>();
            soccerAttributes.Add(GoalServeDataAttributes.LeagueId, "@gid");
            soccerAttributes.Add(GoalServeDataAttributes.LeagueName, "@name");
            soccerAttributes.Add(GoalServeDataAttributes.CountryId, "@id");
            soccerAttributes.Add(GoalServeDataAttributes.CountryName, "@file_group");
            soccerAttributes.Add(GoalServeDataAttributes.IsCup, "@iscup");
            soccerAttributes.Add(GoalServeDataAttributes.MatchId, "@id");
            soccerAttributes.Add(GoalServeDataAttributes.MatchDate, "@formatted_date");
            soccerAttributes.Add(GoalServeDataAttributes.MatchStatus, "@status");
            soccerAttributes.Add(GoalServeDataAttributes.MatchTime, "@time");
            soccerAttributes.Add(GoalServeDataAttributes.MatchTeamName, "@name");
            soccerAttributes.Add(GoalServeDataAttributes.MatchTeamId, "@id");
            soccerAttributes.Add(GoalServeDataAttributes.MatchTeamTotalScore, "@goals");
            soccerAttributes.Add(GoalServeDataAttributes.localTeam, "localteam");
            soccerAttributes.Add(GoalServeDataAttributes.VisitorTeam, "visitorteam");
            this.AttribuetsMapper.Add(GoalServeSportType.Soccer, soccerAttributes);

            var basketballAttributes = new Dictionary<GoalServeDataAttributes, string>();
            basketballAttributes.Add(GoalServeDataAttributes.LeagueId, "id");
            basketballAttributes.Add(GoalServeDataAttributes.LeagueName, "name");
            basketballAttributes.Add(GoalServeDataAttributes.CountryName, "file_group");
            basketballAttributes.Add(GoalServeDataAttributes.MatchId, "id");
            basketballAttributes.Add(GoalServeDataAttributes.MatchDate, "date");
            basketballAttributes.Add(GoalServeDataAttributes.MatchStatus, "status");
            basketballAttributes.Add(GoalServeDataAttributes.MatchTime, "time");
            basketballAttributes.Add(GoalServeDataAttributes.MatchTeamName, "name");
            basketballAttributes.Add(GoalServeDataAttributes.MatchTeamId, "id");
            basketballAttributes.Add(GoalServeDataAttributes.MatchTeamTotalScore, "totalscore");
            basketballAttributes.Add(GoalServeDataAttributes.localTeam, "localteam");
            basketballAttributes.Add(GoalServeDataAttributes.VisitorTeam, "awayteam");
            basketballAttributes.Add(GoalServeDataAttributes.Q1Score, "q1");
            basketballAttributes.Add(GoalServeDataAttributes.Q2Score, "q2");
            basketballAttributes.Add(GoalServeDataAttributes.Q3Score, "q3");
            basketballAttributes.Add(GoalServeDataAttributes.Q4Score, "q4");
            this.AttribuetsMapper.Add(GoalServeSportType.Basketball, basketballAttributes);

            var tennisAttributes = new Dictionary<GoalServeDataAttributes, string>();
            tennisAttributes.Add(GoalServeDataAttributes.LeagueId, "@id");
            tennisAttributes.Add(GoalServeDataAttributes.LeagueName, "@name");
            tennisAttributes.Add(GoalServeDataAttributes.MatchId, "@id");
            tennisAttributes.Add(GoalServeDataAttributes.MatchDate, "@date");
            tennisAttributes.Add(GoalServeDataAttributes.MatchStatus, "@status");
            tennisAttributes.Add(GoalServeDataAttributes.MatchTime, "@time");
            tennisAttributes.Add(GoalServeDataAttributes.MatchTeamName, "@name");
            tennisAttributes.Add(GoalServeDataAttributes.MatchTeamId, "@id");
            tennisAttributes.Add(GoalServeDataAttributes.MatchTeamTotalScore, "@totalscore");
            this.AttribuetsMapper.Add(GoalServeSportType.Tennis, tennisAttributes);

            var handballAttributes = new Dictionary<GoalServeDataAttributes, string>();
            handballAttributes.Add(GoalServeDataAttributes.LeagueId, "id");
            handballAttributes.Add(GoalServeDataAttributes.LeagueName, "name");
            handballAttributes.Add(GoalServeDataAttributes.CountryName, "file_group");
            handballAttributes.Add(GoalServeDataAttributes.MatchId, "id");
            handballAttributes.Add(GoalServeDataAttributes.MatchDate, "date");
            handballAttributes.Add(GoalServeDataAttributes.MatchStatus, "status");
            handballAttributes.Add(GoalServeDataAttributes.MatchTime, "time");
            handballAttributes.Add(GoalServeDataAttributes.MatchTeamName, "name");
            handballAttributes.Add(GoalServeDataAttributes.MatchTeamId, "id");
            handballAttributes.Add(GoalServeDataAttributes.MatchTeamTotalScore, "totalscore");
            handballAttributes.Add(GoalServeDataAttributes.localTeam, "localteam");
            handballAttributes.Add(GoalServeDataAttributes.VisitorTeam, "awayteam");
            this.AttribuetsMapper.Add(GoalServeSportType.Handball, handballAttributes);

            var footballAttributes = new Dictionary<GoalServeDataAttributes, string>();
            footballAttributes.Add(GoalServeDataAttributes.LeagueId, "id");
            footballAttributes.Add(GoalServeDataAttributes.LeagueName, "name");
            footballAttributes.Add(GoalServeDataAttributes.MatchId, "id");
            footballAttributes.Add(GoalServeDataAttributes.MatchDate, "date");
            footballAttributes.Add(GoalServeDataAttributes.MatchStatus, "status");
            footballAttributes.Add(GoalServeDataAttributes.MatchTime, "time");
            footballAttributes.Add(GoalServeDataAttributes.MatchTeamName, "name");
            footballAttributes.Add(GoalServeDataAttributes.MatchTeamId, "id");
            footballAttributes.Add(GoalServeDataAttributes.MatchTeamTotalScore, "totalscore");
            footballAttributes.Add(GoalServeDataAttributes.localTeam, "localteam");
            footballAttributes.Add(GoalServeDataAttributes.VisitorTeam, "awayteam");
            this.AttribuetsMapper.Add(GoalServeSportType.Football, footballAttributes);

            var baseballAttributes = new Dictionary<GoalServeDataAttributes, string>();
            baseballAttributes.Add(GoalServeDataAttributes.LeagueId, "@id");
            baseballAttributes.Add(GoalServeDataAttributes.LeagueName, "@name");
            baseballAttributes.Add(GoalServeDataAttributes.MatchId, "@id");
            baseballAttributes.Add(GoalServeDataAttributes.MatchDate, "@date");
            baseballAttributes.Add(GoalServeDataAttributes.MatchStatus, "@status");
            baseballAttributes.Add(GoalServeDataAttributes.MatchTime, "@time");
            baseballAttributes.Add(GoalServeDataAttributes.MatchTeamName, "@name");
            baseballAttributes.Add(GoalServeDataAttributes.MatchTeamId, "@id");
            baseballAttributes.Add(GoalServeDataAttributes.MatchTeamTotalScore, "@totalscore");
            baseballAttributes.Add(GoalServeDataAttributes.localTeam, "localteam");
            baseballAttributes.Add(GoalServeDataAttributes.VisitorTeam, "awayteam");
            this.AttribuetsMapper.Add(GoalServeSportType.Baseball, baseballAttributes);

        }
        #endregion
        public void ResetDumper()
        {
            try
            {
                ConfigurationManager.RefreshSection("appSettings");
                var dumpLeague = ConfigurationManager.AppSettings["DumpLeague"];
                var dumpMatch = ConfigurationManager.AppSettings["DumpMatch"];
                if (string.IsNullOrEmpty(dumpMatch) || string.IsNullOrEmpty(dumpLeague))
                    this.dumper = new GoalserveLeagueMatchDumper(false);
                else
                {
                    this.dumper = new GoalserveLeagueMatchDumper(int.Parse(dumpLeague), int.Parse(dumpMatch));
                    Console.WriteLine($">> Dumper is active for leagueId {dumpLeague}, MatchId {dumpMatch}");
                    _log.Info($">> Dumper is active for leagueId {dumpLeague}, MatchId {dumpMatch}");
                }
            }
            catch (Exception ex)
            {
                _log.Error($"An error occurred trying to create league,match dumper", ex);
                this.dumper = new GoalserveLeagueMatchDumper(false);
            }
        }
        #region Fetchers
        public string FetchRawData(BgGoalServeFetchRequest request)
        {
            try
            {
                var jsonResponse = ExternalServices.Instance.FetchDataFromUrl(request.Url);
                this.dumper.DumpJson(request.LeagueId, request.CashFilePath, jsonResponse);
                //File.WriteAllText(request.CashFilePath, jsonResponse);
                //if (request.UseCach)
                //{
                //    if (!File.Exists(request.CashFilePath))
                //    {
                //        jsonResponse = ExternalServices.Instance.FetchDataFromUrl(request.Url);
                //        File.WriteAllText(request.CashFilePath, jsonResponse);
                //    }
                //    else
                //    {
                //        jsonResponse = File.ReadAllText(request.CashFilePath);
                //    }
                //}
                //else
                //{
                //    jsonResponse = ExternalServices.Instance.FetchDataFromUrl(request.Url);
                //}
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch data with request {request.ToString()}", ex);
            }
        }

        #endregion

        #region Parsers

        public BgGoalServeRepository ParseCategories(dynamic machesJson, BgGoalServeFetchRequest request, List<string> skippedInfo)
        {
            var matchesData = WebApiInvoker.JsonDeserializeDynamic(machesJson);
            if (matchesData != null && 
                matchesData.ContainsKey("scores") && 
                matchesData["scores"] != null && 
                matchesData["scores"].ContainsKey("category") && 
                matchesData["scores"]["category"] != null)
            {
                var categories = matchesData["scores"]["category"];
                var categoryIndex = 0;
                var matches = new Dictionary<MatchKey, IBgGoalServeMatch>();
                var leagues = new Dictionary<int, BgGoalServeLeague>();
                var allOdds = new Dictionary<int, List<BgGoalServeBookmaker>>();
                var oddsFeedLastUpdate = new Dictionary<SportLeagueKey, long>();
                foreach (var category in categories)
                {
                    try
                    {
                        var leagueName = BettingTipsServicesHelpers.DynamicResolver<string>(category, null, GoalServeDataAttributes.LeagueName, ResolveEnum.STRING, this.AttribuetsMapper[request.Sport]);
                        var leagueId = BettingTipsServicesHelpers.DynamicResolver<int>(category, null, GoalServeDataAttributes.LeagueId, ResolveEnum.INT, this.AttribuetsMapper[request.Sport]);
                        var country = BettingTipsServicesHelpers.DynamicResolver<string>(category, null, GoalServeDataAttributes.CountryName, ResolveEnum.STRING, this.AttribuetsMapper[request.Sport]);
                        var countryId = BettingTipsServicesHelpers.DynamicResolver<int?>(category, null, GoalServeDataAttributes.CountryId, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[request.Sport]);
                        var isCup = BettingTipsServicesHelpers.DynamicResolver<bool>(category, null, GoalServeDataAttributes.IsCup, ResolveEnum.BOOL, this.AttribuetsMapper[request.Sport]);
                        /*for debug
                                            if (leagueId == 18931) 
                                                _log.Debug("find");
                        */
                        if (!leagues.ContainsKey(leagueId))
                        {
                            //in Tennis there is no country
                            var countryName = string.IsNullOrEmpty(country)
                                ? string.Empty
                                : country;
                            leagues.Add(leagueId, new BgGoalServeLeague(leagueId, leagueName, countryName, countryId, isCup));
                        }

                        ParseMatches(request, skippedInfo, matches, category, leagueId);

                        if (request.Type >= GoalServeMatchDayFetch.Today)
                            ParseOdds(request, skippedInfo, ref allOdds, oddsFeedLastUpdate, leagueId, countryId);
                    }
                    catch (Exception ex)
                    {
                        _log.Fatal($"An error occurred trying to parse scores:category[{categoryIndex}]", ex);
                    }
                    categoryIndex++;
                }
                var repo = new BgGoalServeRepository(matches, allOdds, leagues, oddsFeedLastUpdate);
                return repo;
            }
            else
                _log.Info($"node scores:category could not be found...probabley no data availabale");
            return null;
        }

        public void ParseOdds(BgGoalServeFetchRequest request,
            List<string> skippedInfo,
            ref Dictionary<int, List<BgGoalServeBookmaker>> odds,
            Dictionary<SportLeagueKey, long> oddsFeedLastUpdate,
            dynamic leagueId,
            int? countryId)
        {
            var fetchOddRequest = new BgGoalServeFetchRequest(request.Sport,
                request.Type,
                GoalServeUrlType.Odds,
                request.PreviousRunRepository,
                leagueId);
            var oddsJson = FetchRawData(fetchOddRequest);
            var oddsParser = new BgGoalServeOddParser(oddsJson, request.Sport, leagueId, this.dumper);
            oddsParser.Parse();
            //if (oddsParser.MatchOddsCollection.Count == 0 && countryId.HasValue)
            if (oddsParser.MatchOddsCollection.Count == 0)
            {
                var leagueOrCountryId = countryId.HasValue
                    ? countryId.Value 
                    : leagueId;
                fetchOddRequest = new BgGoalServeFetchRequest(request.Sport,
                    request.Type,
                    GoalServeUrlType.Odds,
                    request.PreviousRunRepository,
                    leagueOrCountryId);
                oddsJson = FetchRawData(fetchOddRequest);
                oddsParser = new BgGoalServeOddParser(oddsJson, request.Sport, leagueOrCountryId, this.dumper);
                oddsParser.Parse();
            }
            oddsParser.JoinParserResults(ref odds, ref skippedInfo);
            var oddsFeedKey = new SportLeagueKey(request.Sport, leagueId);
            if (oddsFeedLastUpdate.ContainsKey(oddsFeedKey))
            {
                if (oddsFeedLastUpdate[oddsFeedKey] > oddsParser.FeedTimeStamp)
                    oddsFeedLastUpdate[oddsFeedKey] = oddsParser.FeedTimeStamp;
            }
            oddsFeedLastUpdate.Add(oddsFeedKey, oddsParser.FeedTimeStamp);
        }

        private void ParseMatches(BgGoalServeFetchRequest request, List<string> skippedInfo, Dictionary<MatchKey, IBgGoalServeMatch> matches, dynamic category, int leagueId)
        {
            var matchColl = request.Sport == GoalServeSportType.Soccer
                ? category["matches"]["match"]
                : category["match"];
            if (matchColl.GetType().IsArray)
            {
                var matchIndex = 0;
                foreach (var matchInfo in matchColl)
                {
                    var match = request.Sport == GoalServeSportType.Tennis
                        ? ParseOneTennisMatch(matchInfo, matchIndex, leagueId, skippedInfo, GoalServeSportType.Tennis, request.Date)
                        : ParseOneMatch(matchInfo, matchIndex, leagueId, skippedInfo, request.Sport, request.Date);
                    if (match != null)
                        matches.Add(new MatchKey(match.MatchId, match.LeagueId, match.LocalTeamId, match.VisitorTeamId), match);
                    matchIndex++;
                }
            }
            else
            {
                var match = ParseOneMatch(matchColl, 0, leagueId, skippedInfo, request.Sport, request.Date);
                if (match != null)
                    matches.Add(new MatchKey(match.MatchId, match.LeagueId, match.LocalTeamId, match.VisitorTeamId), match);
            }
            this.dumper.DumpAllMatches(leagueId, matches.Keys.ToList());
        }

        #region Parse Matches
        private BgGoalServeMatch ParseOneTennisMatch(dynamic matchInfo, int matchIndex, int leagueId, List<string> skippedInfo, GoalServeSportType sport, DateTime fetchDate)
        {
            var id = BettingTipsServicesHelpers.DynamicResolver<int>(matchInfo, null, GoalServeDataAttributes.MatchId, ResolveEnum.INT, this.AttribuetsMapper[sport]); //connection to getodds match
            var date = BettingTipsServicesHelpers.DynamicResolver<DateTime>(matchInfo, null, GoalServeDataAttributes.MatchDate, ResolveEnum.DATE2, this.AttribuetsMapper[sport]);
            var time = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, null, GoalServeDataAttributes.MatchTime, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
            var status = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, null, GoalServeDataAttributes.MatchStatus, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
            var players = matchInfo["player"];
            var playerColl = new List<(string, int, string)>();
            if (players.GetType().IsArray)
            {
                foreach (var player in players)
                {
                    var playerName = BettingTipsServicesHelpers.DynamicResolver<string>(player, null, GoalServeDataAttributes.MatchTeamName, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                    var playerId = BettingTipsServicesHelpers.DynamicResolver<int>(player, null, GoalServeDataAttributes.MatchTeamId, ResolveEnum.INT, this.AttribuetsMapper[sport]);
                    var playerScore = BettingTipsServicesHelpers.DynamicResolver<string>(player, null, GoalServeDataAttributes.MatchTeamTotalScore, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                    playerColl.Add((playerName, playerId, playerScore));
                }
            }
            IBgGoalServeMatchResult matchResult = new BgGoalServeMatchLocalVisitorResult(playerColl[0].Item3, playerColl[1].Item3);
            var goalserveMatch = new BgGoalServeMatch(playerColl[0].Item1, playerColl[0].Item2, playerColl[1].Item1, playerColl[1].Item2, status, leagueId, date, time, id, matchResult);
            return goalserveMatch;
        }
        private BgGoalServeMatch ParseOneMatch(dynamic matchInfo, int matchIndex, int leagueId, List<string> skippedInfo, GoalServeSportType sport, DateTime fetchDate)
        {
            try
            {
                var id = BettingTipsServicesHelpers.DynamicResolver<int>(matchInfo, null, GoalServeDataAttributes.MatchId, ResolveEnum.INT, this.AttribuetsMapper[sport]); //connection to getodds match
                var date = BettingTipsServicesHelpers.DynamicResolver<DateTime>(matchInfo, null, GoalServeDataAttributes.MatchDate, ResolveEnum.DATE2, this.AttribuetsMapper[sport]);
                var time = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, null, GoalServeDataAttributes.MatchTime, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                var status = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, null, GoalServeDataAttributes.MatchStatus, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                var localTeamName = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.MatchTeamName, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                var localTeamId = BettingTipsServicesHelpers.DynamicResolver<int>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.MatchTeamId, ResolveEnum.INT, this.AttribuetsMapper[sport]);
                var visitorTeamName = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.MatchTeamName, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                var visitorTeamId = BettingTipsServicesHelpers.DynamicResolver<int>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.MatchTeamId, ResolveEnum.INT, this.AttribuetsMapper[sport]);
                var localTeamScore = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.MatchTeamTotalScore, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                var visitorTeamScore = BettingTipsServicesHelpers.DynamicResolver<string>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.MatchTeamTotalScore, ResolveEnum.STRING, this.AttribuetsMapper[sport]);
                IBgGoalServeMatchResult matchResult = null;
                //if (fetchDate != date)
                //    throw new Exception($"Out of fetch date {fetchDate.ToString()} match encountered: id={id}, date={date}, time={time}, status={status}, localTeamId={localTeamId}, visitorTeamId={visitorTeamId}, localTeamName={localTeamName}, visitorTeamName ={visitorTeamName}");
                switch (sport)
                {
                    case GoalServeSportType.Soccer:
                    case GoalServeSportType.Handball:
                    case GoalServeSportType.Football:
                    case GoalServeSportType.Baseball:
                        matchResult = new BgGoalServeMatchLocalVisitorResult(localTeamScore, visitorTeamScore);
                        break;
                    case GoalServeSportType.Basketball:
                        var quatersScores = FetchBasketballQuartersScores(matchInfo);
                        matchResult = new BgGoalServeMatchBasketballResult(localTeamScore, visitorTeamScore, quatersScores);
                        break;
                }
                return new BgGoalServeMatch(localTeamName, localTeamId, visitorTeamName, visitorTeamId, status, leagueId, date, time, id, matchResult);
            }
            catch (Exception ex)
            {
                var msg = $"ParseMatches json path for leagueId={leagueId} scores:category:matches[match({matchIndex})]{Environment.NewLine}{ex}";
                skippedInfo.Add(msg);
            }
            return null;
        }

        private BasketballQuatersScores FetchBasketballQuartersScores(dynamic matchInfo)
        {
            List<int?> localTeamScores = new List<int?>()
            {
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.Q1Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.Q2Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.Q3Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.localTeam, GoalServeDataAttributes.Q4Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
            };
            List<int?> awayTeamScores = new List<int?>()
            {
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.Q1Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.Q2Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.Q3Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
                BettingTipsServicesHelpers.DynamicResolver<int?>(matchInfo, GoalServeDataAttributes.VisitorTeam, GoalServeDataAttributes.Q4Score, ResolveEnum.INT_NULLABLE, this.AttribuetsMapper[GoalServeSportType.Basketball]),
            };
            if (localTeamScores.Any(i => !i.HasValue) || awayTeamScores.Any(i => !i.HasValue))
                return null;
            var results = new BasketballQuatersScores(localTeamScores[0].Value, localTeamScores[1].Value, localTeamScores[2].Value, localTeamScores[3].Value,
                awayTeamScores[0].Value, awayTeamScores[1].Value, awayTeamScores[2].Value, awayTeamScores[3].Value);
            return results;
        }
        #endregion

        #endregion

        #region Dump CSV
        public void DumpToCSV(BgGoalServeRepository repository, BgGoalServeFetchRequest fetchRequest)
        {
            var csvData = new List<List<string>>();
            var filename = $"{DateTime.Now.Hour.ToString("D2")}{DateTime.Now.Minute.ToString("D2")}#{fetchRequest.Date.Day.ToString("D2")}_{fetchRequest.Date.Month.ToString("D2")}_{fetchRequest.Date.Year}_{fetchRequest.Sport.ToString()}_GoalServeRepository.csv";
            var filepath = Path.Combine(BgBettingDataConstants.ResultPath, filename);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath, false, System.Text.Encoding.UTF8))
            {
                var titles = new List<string>() { "Source", "Sport", "MatchId", "Status", "Country", "CountryId", "League", "LeagueId", "IsCup", "Game Date", "Game Time", "Home", "HomeId", "Away", "AwayId", "Home Odd", "Draw Odd", "Away Odd", "Final Result" };
                if (fetchRequest.Sport == GoalServeSportType.Basketball)
                {
                    var basketballQuartersScores = new List<string>() { "LocalQ1", "LocalQ2", "LocalQ3", "LocalQ4", "AwayQ1", "AwayQ2", "AwayQ3", "AwayQ4" };
                    titles.AddRange(basketballQuartersScores);
                }

                csvData.Add(titles);
                foreach (var match in repository.Matches.Keys)
                {
                    var line = new List<string>();
                    line.Add("goldserve");
                    line.Add(fetchRequest.Sport.ToString());
                    line.Add(repository.Matches[match].MatchId.ToString());
                    line.Add(repository.Matches[match].Status);
                    line.Add(repository.Leagues[repository.Matches[match].LeagueId].Country);
                    line.Add(repository.Leagues[repository.Matches[match].LeagueId].CountryId.ToString());
                    line.Add(repository.Leagues[repository.Matches[match].LeagueId].LeagueName);
                    line.Add(repository.Matches[match].LeagueId.ToString());
                    line.Add(repository.Leagues[repository.Matches[match].LeagueId].IsCup.ToString());
                    line.Add(repository.Matches[match].MatchDate.ToString("d"));
                    line.Add(repository.Matches[match].MatchTime);
                    line.Add(repository.Matches[match].LocalTeam);
                    line.Add(repository.Matches[match].LocalTeamId.ToString());
                    line.Add(repository.Matches[match].VisitorTeam);
                    line.Add(repository.Matches[match].VisitorTeamId.ToString());
                    var matchId = repository.Matches[match].MatchId;
                    if (repository.Odds.ContainsKey(matchId))
                    {
                        try
                        {
                            line.Add(repository.Odds[matchId].Take(3).Average(i => i.Odds["Home"]).ToString("0.00"));
                            line.Add(repository.Odds[matchId].Take(3).Average(i => i.Odds["Draw"]).ToString("0.00"));
                            line.Add(repository.Odds[matchId].Take(3).Average(i => i.Odds["Away"]).ToString("0.00"));
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        line.Add(string.Empty);
                        line.Add(string.Empty);
                        line.Add(string.Empty);
                    }
                    var finalResult = repository.Matches[match].MatchResult.LocalTeamScores == null || repository.Matches[match].MatchResult.VisitorTeamScores == null
                        ? string.Empty
                        : $"[{repository.Matches[match].MatchResult.LocalTeamScores}-{repository.Matches[match].MatchResult.VisitorTeamScores}]";
                    line.Add(finalResult);
                    if (!string.IsNullOrEmpty(finalResult) && fetchRequest.Sport == GoalServeSportType.Basketball)
                    {
                        var matchScoresResult = repository.Matches[match].MatchResult as BgGoalServeMatchBasketballResult;
                        if (matchScoresResult.QuartersScores != null)
                        {
                            line.Add(matchScoresResult.QuartersScores.LocalTeamQ1Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.LocalTeamQ2Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.LocalTeamQ3Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.LocalTeamQ4Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.AwayTeamQ1Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.AwayTeamQ2Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.AwayTeamQ3Score.ToString());
                            line.Add(matchScoresResult.QuartersScores.AwayTeamQ4Score.ToString());
                        }
                        else
                        {
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                            line.Add(string.Empty);
                        }
                    }
                    csvData.Add(line);
                }
                foreach (var csvLine in csvData)
                    file.WriteLine(string.Join(",", csvLine));
                csvData.Clear();
            }
        }
        #endregion

        #region AddOrUpdate
        public void AddOrUpdate(BgGoalServeRepository repository, GoalServeSportType Sport, HashSet<GoalserveMatch> feedExist)
        {
            var matchesToUpdate = new HashSet<GoalserveMatch>(new GoalserveMatchComparer());
            var matchesToAdd = new HashSet<GoalserveMatch>(new GoalserveMatchComparer());
            var countriesToAdd = new Dictionary<int, string>();          
            foreach (var match in repository.Matches.Keys)
            {
                try
                {
                    var oddsAndScores = ExtractOddsAndScores(repository, Sport, match);
                    var candidate = new GoalserveMatch(Sport, repository.Matches[match], repository.Leagues, oddsAndScores);
                    if (feedExist.TryGetValue(candidate, out var matchPersist))
                    {
                        if (matchPersist.Status != candidate.Status || !matchPersist.OddsAndScores.Equals(candidate.OddsAndScores))
                            matchesToUpdate.Add(candidate);
                    }
                    else
                    {
                        matchesToAdd.Add(candidate);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error($"Failed to upsert {Sport} match {repository.Matches[match].ToString()}", ex);
                }
            }
            BgMapperServices.Instance.AddOrUpdateMapping(matchesToAdd);
            BgMapperServices.Instance.AddOrUpdateMapping(matchesToUpdate); //REMOVE THIS
            if (matchesToUpdate.Count > 0)
            {
                persistance.GoalserveFeedUpdate(matchesToUpdate.ToList());
            }
            if (matchesToAdd.Count > 0)
            {
                persistance.GoalserveFeedInsert(matchesToAdd.ToList());
            }
            if (Sport == GoalServeSportType.Soccer && repository.Odds.Count > 0)
                PersistMatchesOdds(Sport, repository.Odds);
        }

        private void PersistMatchesOdds(GoalServeSportType Sport,IReadOnlyDictionary<int, List<BgGoalServeBookmaker>> odds)
        {
            try
            {
                List<GoalserveMatchOdds> flattenOdds = new List<GoalserveMatchOdds>();
                foreach (var matchId in odds.Keys) 
                {
                    foreach (var bookmaker in odds[matchId])
                        try
                        {
                            flattenOdds.Add(new GoalserveMatchOdds((int)Sport, matchId, bookmaker.Name, bookmaker.Odds["Home"], bookmaker.Odds["Draw"], bookmaker.Odds["Away"]));
                        }
                        catch (Exception) { }
                }
                if (flattenOdds.Count() > 0)
                    persistance.MergeGoalserveFeedOdds(flattenOdds);
            }
            catch(Exception)
            { }
        }

        private OddsAndScores ExtractOddsAndScores(BgGoalServeRepository repository,
            GoalServeSportType sport,
            MatchKey match)
        {
            decimal? homeOdd, drawOdd, awayOdd;
            int? ltScores, vtScores;
            homeOdd = null;
            drawOdd = null;
            awayOdd = null;
            if (repository.Odds.ContainsKey(match.MatchId))
            {
                try
                {
                    homeOdd = Math.Round(repository.Odds[match.MatchId].Take(3).Average(i => i.Odds["Home"]), 2);
                    drawOdd = sport != GoalServeSportType.Tennis
                        ? Math.Round(repository.Odds[match.MatchId].Take(3).Average(i => i.Odds["Draw"]), 2)
                        : (decimal?)null;
                    awayOdd = Math.Round(repository.Odds[match.MatchId].Take(3).Average(i => i.Odds["Away"]), 2);
                }
                catch (Exception)
                { }
            }
            ltScores = null;
            if (int.TryParse(repository.Matches[match].MatchResult.LocalTeamScores, out var localTeamScores))
                ltScores = localTeamScores;
            vtScores = null;
            if (int.TryParse(repository.Matches[match].MatchResult.VisitorTeamScores, out var visitorTeamScores))
                vtScores = visitorTeamScores;
            BasketballQuatersScores bbQuarters = null;
            if (repository.Matches[match].MatchResult is BgGoalServeMatchBasketballResult)
            {
                var bbMatchResult = repository.Matches[match].MatchResult as BgGoalServeMatchBasketballResult;
                bbQuarters = bbMatchResult.QuartersScores;
            }
            var oddsAndScores = new OddsAndScores(homeOdd, drawOdd, awayOdd, ltScores, vtScores, bbQuarters);
            return oddsAndScores;

        }
        #endregion
        #region GetPersistanceMatches
        public HashSet<GoalserveMatch> GetPersistanceMatches(GoalServeSportType sport, List<int> matchIds)
        {
            var persistanceFeed = persistance.GoalserveMatchesOnlyGet((int)sport, matchIds)
                .ConvertAll(f => new GoalserveMatch(f.SportId,
                            f.MatchId,
                            f.Status,
                            f.CountryId,
                            f.Country,
                            f.League,
                            f.LeagueId,
                            f.IsCup,
                            f.GameStart,
                            f.Home,
                            f.HomeId,
                            f.Away,
                            f.AwayId,
                            new OddsAndScores(f.HomeOdd.HasValue ? Convert.ToDecimal(f.HomeOdd) : (decimal?)null,
                                f.DrawOdd.HasValue ? Convert.ToDecimal(f.DrawOdd) : (decimal?)null,
                                f.AwayOdd.HasValue ? Convert.ToDecimal(f.AwayOdd) : (decimal?)null,
                                f.HomeScore,
                                f.AwayScore,
                                f.HomeQ1.HasValue
                                ? new BasketballQuatersScores(f.HomeQ1.Value,
                                    f.HomeQ2.Value,
                                    f.HomeQ3.Value,
                                    f.HomeQ4.Value,
                                    f.AwayQ1.Value,
                                    f.AwayQ2.Value,
                                    f.AwayQ3.Value,
                                    f.AwayQ4.Value)
                                : null)
                        ));
            var feed = new HashSet<GoalserveMatch>(persistanceFeed, new GoalserveMatchComparer());
            return feed;
        }
        #endregion
        #region GetPersistanceFeed
        public HashSet<GoalserveMatch> GetPersistanceFeed(GoalServeSportType sport, int daysAdd)
        {
            var persistanceFeed = persistance.GoalserveFeedGet((int)sport, daysAdd)
                .ConvertAll(f => new GoalserveMatch(f.SportId,
                            f.MatchId,
                            f.Status,
                            f.CountryId,
                            f.Country,
                            f.League,
                            f.LeagueId,
                            f.IsCup,
                            f.GameStart,
                            f.Home,
                            f.HomeId,
                            f.Away,
                            f.AwayId,
                            new OddsAndScores(f.HomeOdd.HasValue ? Convert.ToDecimal(f.HomeOdd) : (decimal?)null,
                                f.DrawOdd.HasValue ? Convert.ToDecimal(f.DrawOdd) : (decimal?)null,
                                f.AwayOdd.HasValue ? Convert.ToDecimal(f.AwayOdd) : (decimal?)null,
                                f.HomeScore,
                                f.AwayScore,
                                null))
                        );
            var feed = new HashSet<GoalserveMatch>(persistanceFeed, new GoalserveMatchComparer());
            return feed;
        }
        #endregion

        #region GetMonitorFeed
        public List<GoalserveMatchFeedMonitor> GetMonitorFeed(GoalServeSportType sport, int daysAdd)
        {
            var feed = persistance.GoalserveFeedGet((int)sport, daysAdd)
                .ConvertAll(f => new GoalserveMatchFeedMonitor((GoalServeSportType)f.SportId,
                            f.MatchId,
                            f.Status,
                            f.LastUpdate,
                            f.CountryId,
                            f.Country,
                            f.League,
                            f.LeagueId,
                            f.IsCup,
                            f.GameStart,
                            f.Home,
                            f.HomeId,
                            f.Away,
                            f.AwayId,
                            f.HomeOdd.HasValue ? Convert.ToDecimal(f.HomeOdd) : (decimal?)null,
                            f.DrawOdd.HasValue ? Convert.ToDecimal(f.DrawOdd) : (decimal?)null,
                            f.AwayOdd.HasValue ? Convert.ToDecimal(f.AwayOdd) : (decimal?)null,
                            f.HomeScore,
                            f.AwayScore,
                            f.HomeQ1,
                            f.AwayQ1,
                            f.HomeQ2,
                            f.AwayQ2,
                            f.HomeQ3,
                            f.AwayQ3,
                            f.HomeQ4,
                            f.AwayQ4
                            )
                        );
            return feed;
        }
        #endregion
        #region GetRawFeed
        public GoldserveMatchRepository GetRawFeed(GoalServeSportType sport, DateTime feedDate)
        {
            var days = feedDate.Date - DateTime.Today;
            var feed = persistance.GoalserveFeedGet((int)sport, days.Days)
                .ConvertAll(f => new GoalserveMatchFeed((GoalServeSportType)f.SportId,
                            f.MatchId,
                            f.Status,
                            f.CountryId,
                            f.Country,
                            f.League,
                            f.LeagueId,
                            f.IsCup,
                            f.GameStart,
                            f.Home,
                            f.HomeId,
                            f.Away,
                            f.AwayId,
                            f.LastUpdate,
                            f.HomeOdd.HasValue ? Convert.ToDecimal(f.HomeOdd) : (decimal?)null,
                            f.DrawOdd.HasValue ? Convert.ToDecimal(f.DrawOdd) : (decimal?)null,
                            f.AwayOdd.HasValue ? Convert.ToDecimal(f.AwayOdd) : (decimal?)null,
                            f.HomeScore,
                            f.AwayScore,
                            f.HomeQ1,
                            f.AwayQ1,
                            f.HomeQ2,
                            f.AwayQ2,
                            f.HomeQ3,
                            f.AwayQ3,
                            f.HomeQ4,
                            f.AwayQ4
                            )
                        );
            var repo = new GoldserveMatchRepository(feed);
            return repo;
        }
        #endregion    
    }
}