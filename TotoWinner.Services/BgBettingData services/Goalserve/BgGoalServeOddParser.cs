﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class BgGoalServeOddParser
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string OddsJson { get; }
        public int LeagueId { get; }
        public GoalServeSportType Sport { get; }
        public List<string> SkippedInfo { get; }
        public long FeedTimeStamp { get; private set; }
        public List<Dictionary<int, List<BgGoalServeBookmaker>>> MatchOddsCollection { get; }
        private IReadOnlyDictionary<GoalServeSportType, string> OddProviders { get; }
        private GoalserveLeagueMatchDumper Dumper { get; }
        public BgGoalServeOddParser(string oddsJson,
            GoalServeSportType sport,
            int leagueId,
            GoalserveLeagueMatchDumper goalserveLeagueMatchDumper)
        {
            try
            {
                this.OddsJson = oddsJson;
                this.Sport = sport;
                this.LeagueId = leagueId;
                this.Dumper = goalserveLeagueMatchDumper;
                this.SkippedInfo = new List<string>();
                this.MatchOddsCollection = new List<Dictionary<int, List<BgGoalServeBookmaker>>>();
                this.OddProviders = new Dictionary<GoalServeSportType, string>() {
                    { GoalServeSportType.Soccer, "Match Winner" },
                    { GoalServeSportType.Basketball,"3Way Result"},
                    { GoalServeSportType.Tennis,"Home/Away"},
                    { GoalServeSportType.Handball,"3Way Result"},
                    { GoalServeSportType.Football,"3Way Result"},
                    { GoalServeSportType.Baseball,"3Way Result"},
                };
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occured trying to construct type {typeof(BgGoalServeOddParser)}", ex);
            }
        }
        public override string ToString()
        {
            var str = $"OddsJson={this.OddsJson}, LeagueId={this.LeagueId}";
            return str;
        }
        #region Parse Odds
        public void Parse()
        {
            var oddsData = WebApiInvoker.JsonDeserializeDynamic(this.OddsJson);
            var oddsMatchesNodes = oddsData["scores"]["categories"];
            this.FeedTimeStamp = long.Parse(oddsData["scores"]["ts"]);
            foreach (var oddsMatchesNode in oddsMatchesNodes)
            {
                var oddsMatchIndex = 0;
                foreach (var oddsMatchNode in oddsMatchesNode["matches"])
                {
                    try
                    {
                        Dictionary<int, List<BgGoalServeBookmaker>> matchOdds = ParseOddsMatch(oddsMatchNode);
                        this.MatchOddsCollection.Add(matchOdds);
                        oddsMatchIndex++;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"An error occurred trying to parse scores:category:matches[{oddsMatchIndex}] for leagueId [{this.LeagueId}]", ex);
                    }
                }
            }
            this.Dumper.DumpAllMatchesFromOdds(this.LeagueId, this.MatchOddsCollection);
        }
        private Dictionary<int, List<BgGoalServeBookmaker>> ParseOddsMatch(dynamic oddsMatchNode)
        {
            int id = -1;
            var matchesIndex = 0;
            var odds = new Dictionary<int, List<BgGoalServeBookmaker>>();
            try
            {
                //staticId = BettingTipsServicesHelpers.DynamicResolver<int>(oddsMatchNode, null, "static_id", ResolveEnum.INT); //connection to getodds match
                id = BettingTipsServicesHelpers.DynamicResolver<int>(oddsMatchNode, null, "id", ResolveEnum.INT); //connection to getodds match
                foreach (var oddType in oddsMatchNode["odds"])
                {
                    var bookmakers = ParseOddTypeNode(oddType);
                    if (bookmakers.Count > 0)
                    {
                        this.Dumper.DumpBookmakers(this.LeagueId, id, bookmakers);
                        odds.Add(id, bookmakers);
                    }
                    matchesIndex++;
                }
            }
            catch (Exception ex)
            {
                var msg = $"ParseOddsMatch leagueId={this.LeagueId} json path scores:categories:matches({matchesIndex})[id={id}]{Environment.NewLine}{ex}";
                this.SkippedInfo.Add(msg);
            }
            return odds;
        }

        private List<BgGoalServeBookmaker> ParseOddTypeNode(dynamic oddType)
        {
            var typeValue = string.Empty;
            var bookmakers = new List<BgGoalServeBookmaker>();
            try
            {
                typeValue = BettingTipsServicesHelpers.DynamicResolver<string>(oddType, null, "value", ResolveEnum.STRING);
                if (typeValue == this.OddProviders[this.Sport])
                {
                    foreach (var bookmaker in oddType["bookmakers"])
                    {
                        var bookmakerInfo = ParseBookmakerNode(bookmaker);
                        if (bookmakerInfo != null)
                            bookmakers.Add(bookmakerInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to parse odd of type {typeValue}", ex);
            }
            return bookmakers;
        }

        private BgGoalServeBookmaker ParseBookmakerNode(dynamic bookmaker)
        {
            var bookmakerName = string.Empty;
            try
            {
                bookmakerName = BettingTipsServicesHelpers.DynamicResolver<string>(bookmaker, null, "name", ResolveEnum.STRING);
                var odds = new Dictionary<string, decimal>();
                foreach (var odd in bookmaker["odds"])
                {
                    var oddName = BettingTipsServicesHelpers.DynamicResolver<string>(odd, null, "name", ResolveEnum.STRING);
                    var oddValue = BettingTipsServicesHelpers.DynamicResolver<decimal>(odd, null, "value", ResolveEnum.DECIMAL);
                    odds.Add(oddName, oddValue);
                }
                var bookmakerInfo = new BgGoalServeBookmaker(bookmakerName, odds);
                if (!ValidateBookmaker(bookmakerInfo))
                    return null;
                return bookmakerInfo;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to parse bookmaker {bookmakerName}", ex);
            }
        }

        private bool ValidateBookmaker(BgGoalServeBookmaker bookmakerInfo)
        {
            var valid = this.Sport == GoalServeSportType.Tennis
                ? bookmakerInfo.Odds.Count == 2
                : bookmakerInfo.Odds.Count == 3;
            return valid;
        }
        #endregion

        public void JoinParserResults(ref Dictionary<int, List<BgGoalServeBookmaker>> odds, ref List<string> skippedInfo)
        {
            //odds = odds.Union(matchOdds).ToDictionary(pair => pair.Key, pair => pair.Value);
            foreach (var matchOddItem in this.MatchOddsCollection)
            {
                foreach (var matchOddId in matchOddItem.Keys)
                {
                    if (!odds.ContainsKey(matchOddId))
                        odds.Add(matchOddId, matchOddItem[matchOddId]);
                }
            }
            skippedInfo.AddRange(this.SkippedInfo);
        }
    }
}
