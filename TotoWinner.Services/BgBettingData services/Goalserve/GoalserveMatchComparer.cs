﻿using System.Collections.Generic;
using TotoWinner.Data.BgBettingData.Goalserve;

namespace TotoWinner.Services
{
    public class GoalserveMatchComparer : IEqualityComparer<GoalserveMatch>
    {
        public bool Equals(GoalserveMatch x, GoalserveMatch y)
        {
            return x.SportId == y.SportId
                && x.MatchId == y.MatchId
                && x.LeagueId == y.LeagueId
                && x.HomeId == y.HomeId
                && x.AwayId == y.AwayId;
                //&& x.GameStart == y.GameStart;
        }

        public int GetHashCode(GoalserveMatch obj)
        {
            return obj.SportId.GetHashCode()
                + obj.MatchId.GetHashCode()
                +obj.LeagueId.GetHashCode()
                + obj.HomeId.GetHashCode()
                + obj.AwayId.GetHashCode();
                //+ obj.GameStart.GetHashCode();
        }
    }
}
