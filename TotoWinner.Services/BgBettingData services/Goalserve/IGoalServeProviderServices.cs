﻿using System.Collections.Generic;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;

namespace TotoWinner.Services
{
    public interface IGoalServeProviderServices
    {
        void AddOrUpdate(BgGoalServeRepository repository, GoalServeSportType sport, HashSet<GoalserveMatch> feedExist);
        HashSet<GoalserveMatch> GetPersistanceFeed(GoalServeSportType sport, int daysAdd);
        List<GoalserveMatchFeedMonitor> GetMonitorFeed(GoalServeSportType sport, int daysAdd);
        void ResetDumper();
    }
}