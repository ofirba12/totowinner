﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class BgDataBlackFilterServices : Singleton<BgDataBlackFilterServices>, IBgDataBlackFilterServices
    {
        private static readonly ILog _log = LogManager.GetLogger("BgDataGenerator");
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        private BgDataBlackFilterServices()
        {
        }
        public List<BgTeamBlackFilter> GetAllBgTeamsBlackFilters()
        {
            var mapper = BgMapperServices.Instance.GetBgMapper();
            var list = persistance.GetAllBgTeamsBlackFilters()
                .ConvertAll<BgTeamBlackFilter>(f => new BgTeamBlackFilter(
                    mapper.BgIdBgMap[f.TeamId].First().Sport,
                    f.TeamId,
                    BgDataServices.Instance.ExtractBgNameFromBgId(mapper.BgIdBgMap[f.TeamId].First().Sport, BgMapperType.Teams, f.TeamId, mapper),
                    (BgMapperType)f.FilterBgType,
                    f.FilterBgId,
                    f.FilterBgType == (int)BgMapperType.Countries
                        ? BgDataServices.Instance.ExtractBgNameFromBgId(mapper.BgIdBgMap[f.TeamId].First().Sport, BgMapperType.Countries, f.FilterBgId, mapper)
                        : BgDataServices.Instance.ExtractBgNameFromBgId(mapper.BgIdBgMap[f.TeamId].First().Sport, BgMapperType.Leagues, f.FilterBgId, mapper)
                    ));
            return list;
        }
        public void AddFilter(BgTeamBlackFilter filter)
        {
            persistance.MergeBgTeamsBlackFilter(filter.TeamId, filter.FilterBgType, filter.FilterBgId);
        }
        public void DeleteFilter(BgTeamBlackFilter filter)
        {
            persistance.DeleteBgTeamsBlackFilter(filter.TeamId, filter.FilterBgType);
        }

        public FeedPersistanceRepository CleanByBlackFilter(FeedPersistanceRepository finalFeed)
        {
            var filters = GetAllBgTeamsBlackFilters();
            foreach (var filter in filters)
            {
                var blackCountries = finalFeed.BgFeedAddOrUpdate
                    .RemoveAll(m => (m.HomeId == filter.TeamId || m.AwayId == filter.TeamId) && filter.FilterBgType == BgMapperType.Countries && m.CountryId == filter.FilterBgId);
                var blackLeagues = finalFeed.BgFeedAddOrUpdate
                    .RemoveAll(m => (m.HomeId == filter.TeamId || m.AwayId == filter.TeamId) && filter.FilterBgType == BgMapperType.Leagues && m.LeagueId == filter.FilterBgId);

                if (filter.FilterBgType == BgMapperType.Countries)
                    _log.Info($"Black list cleaned {blackCountries} countries [{filter.FilterBgName}:{filter.FilterBgId}]");
                if (filter.FilterBgType == BgMapperType.Leagues)
                    _log.Info($"Black list cleaned {blackLeagues} leagues [{filter.FilterBgName}:{filter.FilterBgId}]");
            };
            return finalFeed;
        }
    }
}
