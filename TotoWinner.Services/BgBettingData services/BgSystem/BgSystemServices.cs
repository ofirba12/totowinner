﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class BgSystemServices : Singleton<BgSystemServices>, IBgSystemServices
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        private BgSystemServices()
        {
        }
        public void SetSystemField(BgSystemField field, DateTime date)
        {
            persistance.SetBgSystemItem(field.ToString(), date.ToString("dd/MM/yyyy HH:mm:ss.fff"), BgSystemFieldType.DateTime);
        }

        public BgSystemRepository GetBgSystemRepository()
        {
            try
            {
                var systemItems = persistance.GetBgSystemItems();
                DateTime? manualProviderStartGeneration = null;
                DateTime? manualProviderFinishGeneration = null;
                DateTime? bgFeedStartGeneration = null;
                DateTime? bgFeedFinishGeneration = null, bgDataWatcherStartAnalysis = null, bgDataWatcherFinishAnalysis = null;
                foreach (var item in systemItems)
                {
                    if (!Enum.TryParse<BgSystemFieldType>(item.ValueType.ToString(), out var itemType))
                        throw new Exception($"Failed to parse BgSystemFieldType {item.ValueType}");
                    if (!Enum.TryParse<BgSystemField>(item.Name, out var itemName))
                        throw new Exception($"Failed to parse BgSystemField {item.Name}");
                    DateTime? dateTime = null;
                    switch(itemType)
                    {
                        case BgSystemFieldType.DateTime:
                            try
                            {
                                dateTime = DateTime.ParseExact(item.Value, "dd/MM/yyyy HH:mm:ss.fff", new CultureInfo("en-US"),
                                            DateTimeStyles.None);
                                //dateTime = Convert.ToDateTime(item.Value, new CultureInfo("en-US"));
                            }
                            catch(Exception ex)
                            {
                                throw new Exception($"Failed to convert {item.Name} value {item.Value} to DateTime", ex);
                            }
                            //if (!DateTime.TryParse(item.Value, out var dateTimeFieldValue))
                            //    throw new Exception($"Failed to convert {item.Name} value {item.Value} to DateTime");
                            //dateTime = dateTimeFieldValue;
                            break;
                        default:
                            throw new Exception($"System fields type {itemType} in not supported");
                    }

                    switch (itemName)
                    {
                        case BgSystemField.ManualProviderStartGeneration:
                            manualProviderStartGeneration = dateTime;
                            break;
                        case BgSystemField.ManualProviderFinishGeneration:
                            manualProviderFinishGeneration = dateTime;
                            break;
                        case BgSystemField.BgFeedStartGeneration:
                            bgFeedStartGeneration = dateTime;
                            break;
                        case BgSystemField.BgFeedFinishGeneration:
                            bgFeedFinishGeneration = dateTime;
                            break;
                        case BgSystemField.BgDataWatcherStartAnalysis:
                            bgDataWatcherStartAnalysis = dateTime;
                            break;
                        case BgSystemField.BgDataWatcherFinishAnalysis:
                            bgDataWatcherFinishAnalysis = dateTime;
                            break;
                    }
                }
                var repo = new BgSystemRepository(manualProviderStartGeneration, 
                    manualProviderFinishGeneration, 
                    bgFeedStartGeneration, 
                    bgFeedFinishGeneration,
                    bgDataWatcherStartAnalysis,
                    bgDataWatcherFinishAnalysis);
                return repo;
            }
            catch(Exception ex)
            {
                throw new Exception("An error occurred trying to get bgSystem repository", ex);
            }
        }
    }
}
