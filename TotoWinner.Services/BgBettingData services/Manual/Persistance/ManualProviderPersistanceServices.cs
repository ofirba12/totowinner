﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services.Persistance.BgData;

namespace TotoWinner.Services
{
    public class ManualProviderPersistanceServices : Singleton<ManualProviderPersistanceServices>, IManualProviderPersistanceServices
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<ManualMatch> MatchCollection = new List<ManualMatch>();
        private ManualProviderPersistanceServices()
        {}
        public List<Feed_Manual_FetchByLastUpdate_Result> FetchMatchesByLastUpdate(DateTime lastUpdate)
        {
            try
            {
                List<Feed_Manual_FetchByLastUpdate_Result> matches = null;
                using (var ent = new BgDataEntities())
                {
                    matches = ent.Feed_Manual_FetchByLastUpdate(lastUpdate).ToList();
                }
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying fetch manual matches by last update [{lastUpdate}]", ex);
            }
        }
        public List<Feed_Manual_FetchByMatchDate_Result> FetchMatchesByDate(DateTime feedDate)
        {
            try
            {
                List<Feed_Manual_FetchByMatchDate_Result> matches = null;
                using (var ent = new BgDataEntities())
                {
                    matches = ent.Feed_Manual_FetchByMatchDate(feedDate).ToList();
                }
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying fetch manual matches by date [{feedDate}]", ex);
            }
        }
        public List<Feed_Manual_Fetch_Result> FetchMatchesForSync(DateTime? lastUpdate) //when lastUpdate is null, fetch all 
        {
            try
            {
                List<Feed_Manual_Fetch_Result> matches = null;
                using (var ent = new BgDataEntities())
                {
                    matches = ent.Feed_Manual_Fetch(lastUpdate).ToList();
                }
                return matches;
            }
            catch (Exception ex)
            {
                var msg = lastUpdate.HasValue
                    ? $"An error occurred trying fetch manual matches for sync last updated [{lastUpdate}]"
                    : $"An error occurred trying fetch all manual matches";

                throw new Exception(msg, ex);
            }
        }
        public void AddOrUpdateMatch(ManualMatch match)
        {
            try
            {
                var matchId = -1;
                using (var ent = new BgDataEntities())
                {
                    var result = ent.Feed_Manual_Merge((int?)match?.MatchId,
                        (int)match.Sport,
                        match.Status,
                        match.CountryId,
                        match.Country,
                        match.League,
                        match.LeagueId,
                        match.IsCup,
                        match.GameStart,
                        match.Home,
                        match.HomeId,
                        match.Away,
                        match.AwayId,
                        (double?)match.OddsAndScores?.HomeOdd,
                        (double?)match.OddsAndScores?.DrawOdd,
                        (double?)match.OddsAndScores?.AwayOdd,
                        match.OddsAndScores?.HomeScore,
                        match.OddsAndScores?.AwayScore,
                        match.LastUpdate,
                        match.Ignore,
                        match.OddsAndScores?.BasketballQuatersScores?.LocalTeamQ1Score,
                        match.OddsAndScores?.BasketballQuatersScores?.AwayTeamQ1Score,
                        match.OddsAndScores?.BasketballQuatersScores?.LocalTeamQ2Score,
                        match.OddsAndScores?.BasketballQuatersScores?.AwayTeamQ2Score,
                        match.OddsAndScores?.BasketballQuatersScores?.LocalTeamQ3Score,
                        match.OddsAndScores?.BasketballQuatersScores?.AwayTeamQ3Score,
                        match.OddsAndScores?.BasketballQuatersScores?.LocalTeamQ4Score,
                        match.OddsAndScores?.BasketballQuatersScores?.AwayTeamQ4Score
                        );
                    matchId = result.First().Value;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add or update manual match", ex);
            }
        }
        public Feed_Manual_FetchByKey_Result GetMatch(int countryId, int leagueId, int homeId, int awayId, DateTime gameTime)
        {
            try
            {
                Feed_Manual_FetchByKey_Result match = null;
                using (var ent = new BgDataEntities())
                {
                    var result = ent.Feed_Manual_FetchByKey(countryId, leagueId, homeId, awayId, gameTime);
                    match = result.FirstOrDefault();
                }
                return match;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch manual match countryId {countryId}, leagueId {leagueId}, homeId {homeId}, awayId {awayId}, gameTime {gameTime}", ex);
            }
        }
    }
}
