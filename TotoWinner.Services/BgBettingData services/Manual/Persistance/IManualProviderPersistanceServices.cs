﻿using System;
using System.Collections.Generic;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services.Persistance.BgData;

namespace TotoWinner.Services
{
    public interface IManualProviderPersistanceServices
    {
        void AddOrUpdateMatch(ManualMatch match);
        List<Feed_Manual_FetchByLastUpdate_Result> FetchMatchesByLastUpdate(DateTime lastUpdate);
    }
}
