﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class ManualProviderServices : Singleton<ManualProviderServices>, IManualProviderServices
    {
        private static readonly ILog _log = LogManager.GetLogger("ManualGenerator");
        ManualProviderPersistanceServices persistance = ManualProviderPersistanceServices.Instance;
        public List<ManualMatch> MatchCollection = new List<ManualMatch>();
        private ManualProviderServices()
        { }
        public ManualMatch GetMatch(int countryId, int leagueId, int homeId, int awayId, DateTime gameTime)
        {
            try
            {
                ManualMatch match = null;
                var pMatch = persistance.GetMatch(countryId, leagueId, homeId, awayId, gameTime);
                if (pMatch != null)
                {
                    var quarters = new List<int?>() { pMatch.HomeQ1, pMatch.HomeQ2, pMatch.HomeQ3, pMatch.HomeQ4, pMatch.AwayQ1, pMatch.AwayQ2, pMatch.AwayQ3, pMatch.AwayQ4 };
                    BasketballQuatersScores quartersScores = quarters.All(q => q.HasValue)
                        ? new BasketballQuatersScores(pMatch.HomeQ1.Value, pMatch.HomeQ2.Value, pMatch.HomeQ3.Value, pMatch.HomeQ4.Value,
                                                pMatch.AwayQ1.Value, pMatch.AwayQ2.Value, pMatch.AwayQ3.Value, pMatch.AwayQ4.Value)
                        : null;
                    match = new ManualMatch((BgSport)pMatch.SportId,
                        pMatch.Status,
                        pMatch.CountryId,
                        pMatch.Country,
                        pMatch.League,
                        pMatch.LeagueId,
                        pMatch.IsCup,
                        pMatch.GameStart,
                        pMatch.Home,
                        pMatch.HomeId,
                        pMatch.Away,
                        pMatch.AwayId,
                        pMatch.Ignore,
                        pMatch.MatchId,
                        new OddsAndScores((decimal?)pMatch.HomeOdd, (decimal?)pMatch.DrawOdd, (decimal?)pMatch.AwayOdd, pMatch.HomeScore, pMatch.AwayScore, quartersScores),
                        pMatch.LastUpdate);
                }
                return match;
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occurred trying to get manual match countryId {countryId}, leagueId {leagueId}, homeId {homeId}, awayId {awayId}, gameTime {gameTime}", ex);
            }
        }
        public List<ManualMatch> FetchManualMatchesByLastUpdate(DateTime lastUpdate)
        {
            try
            {
                var matches = persistance.FetchMatchesByLastUpdate(lastUpdate)
                    .ConvertAll<ManualMatch>(m =>
                    {
                        var quarters = new List<int?>() { m.HomeQ1, m.HomeQ2, m.HomeQ3, m.HomeQ4, m.AwayQ1, m.AwayQ2, m.AwayQ3, m.AwayQ4 };
                        BasketballQuatersScores quartersScores = quarters.All(q => q.HasValue)
                            ? new BasketballQuatersScores(m.HomeQ1.Value, m.HomeQ2.Value, m.HomeQ3.Value, m.HomeQ4.Value,
                                                    m.AwayQ1.Value, m.AwayQ2.Value, m.AwayQ3.Value, m.AwayQ4.Value)
                            : null;
                        return new ManualMatch(
                        (BgSport)m.SportId,
                        m.Status,
                        m.CountryId,
                        m.Country,
                        m.League,
                        m.LeagueId,
                        m.IsCup,
                        m.GameStart,
                        m.Home,
                        m.HomeId,
                        m.Away,
                        m.AwayId,
                        m.Ignore,
                        m.MatchId,
                        new OddsAndScores((decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd, m.HomeScore, m.AwayScore, quartersScores),
                        m.LastUpdate);
                    });
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch all manual matches", ex);
            }
        }
        public List<ManualMatch> FetchManualMatchesByDate(DateTime feedDate)
        {
            try
            {
                var matches = persistance.FetchMatchesByDate(feedDate)
                    .ConvertAll<ManualMatch>(m =>
                    {
                        var quarters = new List<int?>() { m.HomeQ1, m.HomeQ2, m.HomeQ3, m.HomeQ4, m.AwayQ1, m.AwayQ2, m.AwayQ3, m.AwayQ4 };
                        BasketballQuatersScores quartersScores = quarters.All(q => q.HasValue)
                            ? new BasketballQuatersScores(m.HomeQ1.Value, m.HomeQ2.Value, m.HomeQ3.Value, m.HomeQ4.Value,
                                                    m.AwayQ1.Value, m.AwayQ2.Value, m.AwayQ3.Value, m.AwayQ4.Value)
                            : null;
                        return new ManualMatch(
                        (BgSport)m.SportId,
                        m.Status,
                        m.CountryId,
                        m.Country,
                        m.League,
                        m.LeagueId,
                        m.IsCup,
                        m.GameStart,
                        m.Home,
                        m.HomeId,
                        m.Away,
                        m.AwayId,
                        m.Ignore,
                        m.MatchId,
                        new OddsAndScores((decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd, m.HomeScore, m.AwayScore, quartersScores),
                        m.LastUpdate);
                    });
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch all manual matches", ex);
            }
        }
        public List<ManualMatch> FetchManualMatchesForProccessing(DateTime? lastUpdate)
        {
            try
            {
                var matches = persistance.FetchMatchesForSync(lastUpdate)
                    .ConvertAll<ManualMatch>(m =>
                    {
                        var quarters = new List<int?>() { m.HomeQ1, m.HomeQ2, m.HomeQ3, m.HomeQ4, m.AwayQ1, m.AwayQ2, m.AwayQ3, m.AwayQ4 };
                        BasketballQuatersScores quartersScores = quarters.All(q => q.HasValue)
                            ? new BasketballQuatersScores(m.HomeQ1.Value, m.HomeQ2.Value, m.HomeQ3.Value, m.HomeQ4.Value,
                                                    m.AwayQ1.Value, m.AwayQ2.Value, m.AwayQ3.Value, m.AwayQ4.Value)
                            : null;
                        return new ManualMatch(
                        (BgSport)m.SportId,
                        m.Status,
                        m.CountryId,
                        m.Country,
                        m.League,
                        m.LeagueId,
                        m.IsCup,
                        m.GameStart,
                        m.Home,
                        m.HomeId,
                        m.Away,
                        m.AwayId,
                        m.Ignore,
                        m.MatchId,
                        new OddsAndScores((decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd, m.HomeScore, m.AwayScore, quartersScores),
                        m.LastUpdate);
                    });
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to fetch all manual matches", ex);
            }
        }

        public void UpdateManualFeedWithBgIds(List<ManualMatch> matches)
        {
            try
            {
                if (matches.All(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue))
                    return;
                var existingMapping = BgMapperServices.Instance.GetBgMapper();
                foreach (var match in matches)
                {
                    try
                    {
                        var bgSport = match.Sport;
                        if (!match.CountryId.HasValue && !string.IsNullOrEmpty(match.Country))
                        {
                            var providerData = new ProviderData(null, bgSport, BgMapperType.Countries, match.Country);
                            if (existingMapping.ManualCountries.ContainsKey(providerData))
                            {
                                var bgId = existingMapping.ManualCountries[providerData].ProviderId;
                                match.UpdateCountry(existingMapping.BgIdBgMap[bgId].First().BgName, bgId);
                            }
                        }
                        if (!match.LeagueId.HasValue && !string.IsNullOrEmpty(match.League))
                        {
                            var providerData = new ProviderData(null, bgSport, BgMapperType.Leagues, match.League);
                            if (existingMapping.ManualLeagues.ContainsKey(providerData))
                            {
                                var bgId = existingMapping.ManualLeagues[providerData].ProviderId;
                                match.UpdateLeague(existingMapping.BgIdBgMap[bgId].First().BgName, bgId);
                            }
                        }
                        if (!match.HomeId.HasValue && !string.IsNullOrEmpty(match.Home))
                        {
                            var providerData = new ProviderData(null, bgSport, BgMapperType.Teams, match.Home);
                            if (existingMapping.ManualTeams.ContainsKey(providerData))
                            {
                                var bgId = existingMapping.ManualTeams[providerData].ProviderId;
                                match.UpdateHomeTeam(existingMapping.BgIdBgMap[bgId].First().BgName, bgId);
                            }
                        }
                        if (!match.AwayId.HasValue && !string.IsNullOrEmpty(match.Away))
                        {
                            var providerData = new ProviderData(null, bgSport, BgMapperType.Teams, match.Away);
                            if (existingMapping.ManualTeams.ContainsKey(providerData))
                            {
                                var bgId = existingMapping.ManualTeams[providerData].ProviderId;
                                match.UpdateAwayTeam(existingMapping.BgIdBgMap[bgId].First().BgName, bgId);
                            }
                        }
                        persistance.AddOrUpdateMatch(match);
                    }
                    catch(Exception ex)
                    {
                        _log.Error($"Failed to update manual match matchId={match.MatchId}", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to update or add one or more manual matches out of {matches.Count()} matches", ex);
            }
        }

        public void AddOrUpdateMatch(ManualMatch match)
        {
            try
            {
                persistance.AddOrUpdateMatch(match);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to update or add match {match.ToString()}", ex);
            }
        }
    }
}
