﻿using log4net;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls.WebParts;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class BgDataLeaguesWhitelistServices : Singleton<BgDataLeaguesWhitelistServices>, IBgDataLeaguesWhitelistServices
    {
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        private BgDataLeaguesWhitelistServices()
        {
        }
        public List<int> GetAllBgLeaguesIdsWhitelist()
        {
            var list = persistance.GetAllBgLeaguesWhitelist().ToList();
            return list;
        }
        public List<BgLeague> GetAllBgLeaguesWhitelist(BgSport sport, MapperRespository mapper)
        {
            var prk = new ProviderKey(0, sport, BgMapperType.Leagues);
            var list = persistance.GetAllBgLeaguesWhitelist()
                .ConvertAll<BgLeague>(f =>
                {
                    prk.ProviderId = f;
                    if (mapper.BgIdItemName.ContainsKey(prk))
                    {
                        return new BgLeague(
                        f,
                        BgDataServices.Instance.ExtractBgNameFromBgId(sport, BgMapperType.Leagues, f, mapper));
                    }
                    return null;
                })
                .Where(l => l != null)
                .ToList();
            return list;
        }
        public void AddLeague(int leagueId)
        {
            persistance.MergeBgLeaguesWhitelist(leagueId);
        }
        public void DeleteFilter(int leagueId)
        {
            persistance.DeleteBgLeaguesWhitelist(leagueId);
        }

    }
}
