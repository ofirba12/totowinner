﻿using log4net;
using NPOI.SS.Formula.Functions;
using NPOI.SS.Formula.Udf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Requests.Abstractions;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.BgDataApi;

namespace TotoWinner.Services
{
    public class BgDataScoresServices : Singleton<BgDataScoresServices>, IBgDataScoresServices
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;
        private static Dictionary<int, string> _flags = new Dictionary<int, string>();
        private BgDataScoresServices()
        {
            #region Flags
            _flags.Add(101, "al");
            _flags.Add(351, "al");
            _flags.Add(222, "dz");
            _flags.Add(102, "ad");
            _flags.Add(256, "ao");
            _flags.Add(103, "ar");
            _flags.Add(179, "ar");
            _flags.Add(104, "am");
            _flags.Add(105, "aw");
            _flags.Add(106, "au");
            _flags.Add(180, "au");
            _flags.Add(107, "at");
            _flags.Add(181, "at");
            _flags.Add(108, "az");
            _flags.Add(237, "bh");
            _flags.Add(280, "bh");
            _flags.Add(212, "bd");
            _flags.Add(229, "by");
            _flags.Add(299, "by");
            _flags.Add(109, "be");
            _flags.Add(234, "be");
            _flags.Add(330, "bj");
            _flags.Add(257, "bm");
            _flags.Add(165, "world");
            _flags.Add(166, "bo");
            _flags.Add(335, "bo");
            _flags.Add(238, "ba");
            _flags.Add(274, "ba");
            _flags.Add(110, "br");
            _flags.Add(182, "br");
            _flags.Add(111, "bg");
            _flags.Add(209, "bg");
            _flags.Add(270, "bf");
            _flags.Add(239, "bn");
            _flags.Add(302, "kh");
            _flags.Add(311, "cm");
            _flags.Add(219, "ca");
            _flags.Add(316, "ca");
            _flags.Add(329, "cv");
            _flags.Add(112, "cl");
            _flags.Add(295, "cl");
            _flags.Add(281, "cn");
            _flags.Add(332, "cn");
            _flags.Add(221, "co");
            _flags.Add(323, "co");
            _flags.Add(235, "cr");
            _flags.Add(113, "hr");
            _flags.Add(183, "hr");
            _flags.Add(258, "cu");
            _flags.Add(114, "cy");
            _flags.Add(268, "cy");
            _flags.Add(115, "cz");
            _flags.Add(184, "cz");
            _flags.Add(116, "dk");
            _flags.Add(185, "dk");
            _flags.Add(305, "do");
            _flags.Add(334, "do");
            _flags.Add(255, "cd");
            _flags.Add(168, "eg");
            _flags.Add(117, "sv");
            _flags.Add(118, "england");
            _flags.Add(167, "ec");
            _flags.Add(240, "ee");
            _flags.Add(312, "ee");
            _flags.Add(169, "et");
            _flags.Add(223, "eu");
            _flags.Add(358, "eu");
            _flags.Add(186, "eu");
            _flags.Add(284, "fo");
            _flags.Add(119, "fj");
            _flags.Add(120, "fi");
            _flags.Add(230, "fi");
            _flags.Add(121, "fr");
            _flags.Add(187, "fr");
            _flags.Add(328, "ga");
            _flags.Add(216, "gm");
            _flags.Add(241, "ge");
            _flags.Add(296, "ge");
            _flags.Add(122, "de");
            _flags.Add(188, "de");
            _flags.Add(271, "gh");
            _flags.Add(272, "gi");
            _flags.Add(123, "gr");
            _flags.Add(189, "gr");
            _flags.Add(259, "gt");
            _flags.Add(275, "gn");
            _flags.Add(139, "nl");
            _flags.Add(267, "nl");
            _flags.Add(273, "hn");
            _flags.Add(242, "hk");
            _flags.Add(124, "hu");
            _flags.Add(190, "hu");
            _flags.Add(125, "is");
            _flags.Add(191, "is");
            _flags.Add(126, "in");
            _flags.Add(127, "id");
            _flags.Add(293, "id");
            _flags.Add(170, "ir");
            _flags.Add(289, "ir");
            _flags.Add(128, "iq");
            _flags.Add(243, "ie");
            _flags.Add(265, "ie");
            _flags.Add(129, "il");
            _flags.Add(192, "il");
            _flags.Add(130, "it");
            _flags.Add(193, "it");
            _flags.Add(132, "jm");
            _flags.Add(133, "jp");
            _flags.Add(266, "jp");
            _flags.Add(252, "jo");
            _flags.Add(291, "kz");
            _flags.Add(303, "kz");
            _flags.Add(253, "ke");
            _flags.Add(176, "kw");
            _flags.Add(300, "kg");
            _flags.Add(306, "la");
            _flags.Add(304, "lv");
            _flags.Add(321, "lv");
            _flags.Add(134, "lb");
            _flags.Add(278, "ls");
            _flags.Add(283, "ir");
            _flags.Add(285, "ly");
            _flags.Add(308, "li");
            _flags.Add(194, "lt");
            _flags.Add(260, "lt");
            _flags.Add(135, "lu");
            _flags.Add(254, "lu");
            _flags.Add(318, "mo");
            _flags.Add(309, "mw");
            _flags.Add(292, "my");
            _flags.Add(213, "my");
            _flags.Add(277, "ml");
            _flags.Add(136, "mt");
            _flags.Add(244, "mr");
            _flags.Add(355, "mu");
            _flags.Add(137, "mx");
            _flags.Add(282, "mx");
            _flags.Add(279, "md");
            _flags.Add(314, "mn");
            _flags.Add(207, "me");
            _flags.Add(232, "me");
            _flags.Add(138, "ma");
            _flags.Add(325, "mz");
            _flags.Add(357, "na");
            _flags.Add(324, "nz");
            _flags.Add(353, "nz");
            _flags.Add(233, "ni");
            _flags.Add(171, "ng");
            _flags.Add(140, "mk");
            _flags.Add(195, "mk");
            _flags.Add(196, "no");
            _flags.Add(269, "no");
            _flags.Add(141, "om");
            _flags.Add(225, "ps");
            _flags.Add(142, "pa");
            _flags.Add(245, "py");
            _flags.Add(347, "py");
            _flags.Add(143, "pe");
            _flags.Add(197, "ph");
            _flags.Add(307, "ph");
            _flags.Add(144, "pl");
            _flags.Add(198, "pl");
            _flags.Add(145, "pt");
            _flags.Add(199, "pt");
            _flags.Add(319, "pr");
            _flags.Add(146, "qa");
            _flags.Add(210, "qa");
            _flags.Add(288, "cd");
            _flags.Add(313, "re");
            _flags.Add(147, "ro");
            _flags.Add(214, "ro");
            _flags.Add(200, "ru");
            _flags.Add(261, "ru");
            _flags.Add(172, "rw");
            _flags.Add(148, "sm");
            _flags.Add(173, "sa");
            _flags.Add(297, "sa");
            _flags.Add(149, "scotland");
            _flags.Add(150, "sn");
            _flags.Add(151, "cs");
            _flags.Add(201, "cs");
            _flags.Add(262, "sc");
            _flags.Add(215, "sl");
            _flags.Add(246, "sg");
            _flags.Add(152, "sk");
            _flags.Add(217, "sk");
            _flags.Add(153, "si");
            _flags.Add(220, "si");
            _flags.Add(336, "so");
            _flags.Add(155, "kr");
            _flags.Add(286, "kr");
            _flags.Add(154, "za");
            _flags.Add(156, "es");
            _flags.Add(202, "es");
            _flags.Add(247, "sd");
            _flags.Add(157, "se");
            _flags.Add(203, "se");
            _flags.Add(158, "ch");
            _flags.Add(236, "ch");
            _flags.Add(159, "sy");
            _flags.Add(294, "tw");
            _flags.Add(322, "tw");
            _flags.Add(315, "tj");
            _flags.Add(174, "tz");
            _flags.Add(160, "th");
            _flags.Add(341, "th");
            _flags.Add(331, "tg");
            _flags.Add(161, "tn");
            _flags.Add(162, "tr");
            _flags.Add(204, "tr");
            _flags.Add(349, "tm");
            _flags.Add(287, "ug");
            _flags.Add(248, "ua");
            _flags.Add(250, "ua");
            _flags.Add(163, "ae");
            _flags.Add(205, "united-kingdom");
            _flags.Add(218, "uy");
            _flags.Add(231, "uy");
            _flags.Add(164, "us");
            _flags.Add(206, "us");
            _flags.Add(276, "uz");
            _flags.Add(226, "ve");
            _flags.Add(343, "ve");
            _flags.Add(249, "vn");
            _flags.Add(339, "vn");
            _flags.Add(263, "wales");
            _flags.Add(264, "zm");
            _flags.Add(175, "zw");
            #endregion
        }
        #region private Helpers
        private string getCountryFlag(int? countryId)
        {
            var flag = countryId.HasValue && _flags.ContainsKey(countryId.Value)
                ? _flags[countryId.Value]
                : "il";
            return flag;
        }
        private string getBetResult(BgMatch match)
        {
            // Server side logic: if status == 'FT' need to set betResult with 'home'/'draw'/'away'
            if ((match.Status.ToUpper() == "FT" || match.Status.ToUpper() == "FINISHED")
                && match.HomeScore.HasValue && match.AwayScore.HasValue)
            {
                var res = match.HomeScore.Value - match.AwayScore.Value;
                if (res == 0)
                    return "draw";
                if (res > 0)
                    return "home";
                if (res < 0)
                    return "away";
            }
            return "";
        }
        private string getWDL(BgSport sport, string status, string wdl)
        {
            var specialDrawStatus = false;
            switch (sport)
            {
                case BgSport.Soccer:
                    specialDrawStatus = BgDataAnalysisServices.Instance.SoccerSpecialDrawStatuses.Contains(status.ToLower());// match.Status.ToLower() == "pen." || match.Status.ToLower() == "after over" || match.Status.ToLower() == "aet";
                    break;
                case BgSport.Basketball:
                    specialDrawStatus = false;
                    break;
            }
            if (specialDrawStatus)
                return $"{wdl}#Special";
            return wdl;
        }
        private string oppositeWDL(string wdl)
        {
            if (wdl == "W")
                return "L";
            else if (wdl == "L")
                return "W";
            return wdl;
        }
        private string getBetStatus(string status, string gameTime)
        {
            // Server side logic: all statuses must be converted to up to 3 characters, e.g. After Over => AFT
            if (!string.IsNullOrEmpty(status))
            {
                var newStatus = status.Count() > 3
                    ? status.Substring(0, 3)
                    : status;
                switch (status.ToUpper())
                {
                    case "AFTER OVER":
                        newStatus = "AFT";
                        break;
                    case "FT":
                    case "FINISHED":
                        newStatus = string.Empty;
                        break;
                    case "POSTP.":
                    case "POSTPOND":
                        newStatus = "POSP";
                        break;
                    case "ABAN.":
                        newStatus = "ABAN";
                        break;
                    case "CANCL.":
                    case "CANCELLED":
                        newStatus = "CANL";
                        break;
                    case "PEN.":
                        newStatus = "PEN";
                        break;
                    case "AET":
                    case "AFTER OVER TIME":
                        newStatus = "AET";
                        break;
                    case "P":
                        newStatus = "P";
                        break;
                    case "HT":
                    case "HALF TIME":
                        newStatus = "HT";
                        break;
                    case "AWARDED":
                        newStatus = "AWRD";
                        break;
                    case "1ST QUARTER":
                        newStatus = "Q1";
                        break;
                    case "2ND QUARTER":
                        newStatus = "Q2";
                        break;
                    case "3RD QUARTER":
                        newStatus = "Q3";
                        break;
                    case "4TH QUARTER":
                        newStatus = "Q4";
                        break;
                }
                if (status.Contains(":") || status.ToUpper() == "NOT STARTED")
                    newStatus = gameTime;
                return newStatus;
            }
            return "";
        }
        #endregion
        #region PrepareScoreData
        public List<BgDataLeague> PrepareScoreData(BgFeedFullRepository repo,
            Dictionary<long, BgMatchImpliedStatistics> impliedStatistics,
            BgSport sport)
        {
            try
            {
                var leagues = repo.AllMatches
                    .OrderBy(m => m.League)
                    .GroupBy(m => m.LeagueId).ToDictionary(m => m.Key, m => m.ToList());
                var result = new List<BgDataLeague>();
                foreach (var league in leagues)
                {
                    var matches = league.Value
                        .OrderBy(m => m.GameStart)
                        .ToList()
                        .ConvertAll<BgDataMatch>(m =>
                            new BgDataMatch(m.MatchId,
                                sport,
                                m.HomeId,
                                m.Home,
                                m.HomeId.ToString(),
                                m.AwayId,
                                m.Away,
                                m.AwayId.ToString(),
                                m.GameStart.ToString("HH:mm"),
                                getBetStatus(m.Status, m.GameStart.ToString("HH:mm")),
                                m.HomeOdd.HasValue
                                    ? Decimal.ToDouble(m.HomeOdd.Value)
                                    : default(double),
                                m.DrawOdd.HasValue
                                    ? Decimal.ToDouble(m.DrawOdd.Value)
                                    : default(double),
                                m.AwayOdd.HasValue
                                    ? Decimal.ToDouble(m.AwayOdd.Value)
                                    : default(double),
                                m.HomeScore.HasValue
                                    ? m.HomeScore.Value.ToString()
                                    : string.Empty,
                                m.AwayScore.HasValue
                                    ? m.AwayScore.Value.ToString()
                                    : string.Empty,
                                getBetResult(m),
                                getBetTips(m.MatchId, impliedStatistics)
                            )
                       );
                    result.Add(new BgDataLeague(league.Value.First().Country,
                        league.Value.First().CountryId.Value,
                        getCountryFlag(league.Value.First().CountryId),
                        league.Value.First().LeagueId,
                        league.Value.First().League,
                        matches
                        ));
                }
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred trying to prepare score data", ex);
            }
        }

        private BgDataBetTips getBetTips(long matchId, Dictionary<long, BgMatchImpliedStatistics> impliedStatistics)
        {
            BgDataBetTips tips = null;
            if (impliedStatistics.ContainsKey(matchId))
            {
                var web1x2 = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_1], "Home Win"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_X], "Draw"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_2], "Away Win"),
                }.OrderByDescending(x => x.Item1).First();
                var webOverUnder1_5 = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_Over1_5], "Over 1.5"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_Under1_5], "Under 1.5")
                }.OrderByDescending(x => x.Item1).First();
                var webOverUnder2_5 = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_Over2_5], "Over 2.5"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_Under2_5], "Under 2.5")
                }.OrderByDescending(x => x.Item1).First();
                var webOverUnder3_5 = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_Over3_5], "Over 3.5"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_Under3_5], "Under 3.5")
                }.OrderByDescending(x => x.Item1).First();
                var webRange = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_0_1], "Range 0-1"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_2_3], "Range 2-3"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_4Plus], "Range 4+"),
                }.OrderByDescending(x => x.Item1).First();
                var webBothScore = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_BothScore], "Both Score"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_BothNoScore], "No b.Score")
                }.OrderByDescending(x => x.Item1).First();
                var webDouble = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_1X], "Double 1X"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_12], "Double 12"),
                    new Tuple<int, string>(impliedStatistics[matchId].Items[BgImpliedStatisticType.Web_2X], "Double 2X"),
                }.OrderByDescending(x => x.Item1).First();
                Func<int, int> getQuality = (betValue) =>
                {
                    if (betValue >= 55 && betValue < 65)
                        return 3;
                    else if (betValue >= 65 && betValue < 75)
                        return 4;
                    else if (betValue >= 75)
                        return 5;
                    return 0;
                };
                Func<int, int> getStars = (betValue) =>
                {
                    if (betValue >= 0 && betValue < 41)
                        return 1;
                    else if (betValue >= 41 && betValue < 51)
                        return 2;
                    else if (betValue >= 51 && betValue < 61)
                        return 3;
                    else if (betValue >= 61 && betValue < 76)
                        return 4;
                    else if (betValue >= 76 && betValue < 91)
                        return 5;
                    else if (betValue >= 91)
                        return 6;
                    return 0;
                };
                tips = new BgDataBetTips(
                    web1x2.Item2,
                    web1x2.Item1,
                    getQuality(web1x2.Item1),
                    getStars(web1x2.Item1),
                    webOverUnder1_5.Item2,
                    webOverUnder1_5.Item1,
                    getQuality(webOverUnder1_5.Item1),
                    getStars(webOverUnder1_5.Item1),
                    webOverUnder2_5.Item2,
                    webOverUnder2_5.Item1,
                    getQuality(webOverUnder2_5.Item1),
                    getStars(webOverUnder2_5.Item1),
                    webOverUnder3_5.Item2,
                    webOverUnder3_5.Item1,
                    getQuality(webOverUnder3_5.Item1),
                    getStars(webOverUnder3_5.Item1),
                    webRange.Item2,
                    webRange.Item1,
                    getQuality(webRange.Item1),
                    getStars(webRange.Item1),
                    webBothScore.Item2,
                    webBothScore.Item1,
                    getQuality(webBothScore.Item1),
                    getStars(webBothScore.Item1),
                    webDouble.Item2,
                    webDouble.Item1,
                    getQuality(webDouble.Item1),
                    getStars(webDouble.Item1));
            }
            return tips;
        }


        #endregion

        #region PrepareMatchSummaryDetails
        public BgSummaryMatchDetails PrepareMatchSummaryDetails(long matchId,
            int timezoneOffset,
            MapperRespository mapper)
        {
            try
            {
                var match = BgDataServices.Instance.GetMatch(matchId, mapper);
                var info = GetMatchInfo(match, timezoneOffset);
                var bgTeamsAnalysisRepository = BgDataAnalysisServices.Instance.PrepareTeamsRepository(info.HomeTeamId, info.AwayTeamId);
                var analysis = GetSummaryAnalysis(bgTeamsAnalysisRepository, match.GameStart, timezoneOffset);
                var digest = DigestSummaryData(analysis);
                var summaryOverview = GetSummaryOverview(info, analysis, digest);
                var tips = GetMatchTips(match, bgTeamsAnalysisRepository);

                var summary = new BgSummaryMatchSummary(summaryOverview, digest);
                var response = new BgSummaryMatchDetails(info, summary, analysis, tips);
                return response;
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to prepare match summary data for matchId {matchId}", ex);
                throw new Exception($"An error occurred trying to prepare match summary data for matchId {matchId}", ex);
            }
        }
        private MatchTipsDescriptions PrepareTipsDescriptions(BgMatch match, BgDataBetTips tips,
            BgTeamsAnalysisRepository bgTeamsAnalysisRepository)
        {
            var maxRows = 6;
            var bgTeamsStatisitcsRepo = analysisServices.PrepareTeamsStatisitcsRepository(bgTeamsAnalysisRepository, maxRows);
            MatchTipsDescriptions descriptions = null;
            if (match.HomeOdd.HasValue && match.AwayOdd.HasValue)
            {
                var odds = new BgMatchOdds(match.Sport,
                        match.HomeId,
                        match.AwayId,
                        match.HomeOdd.Value,
                        match.DrawOdd.Value,
                        match.AwayOdd.Value);
                var fullTimeBetData = analysisServices.GetFullTimeBetData(odds, bgTeamsStatisitcsRepo);
                var overUnder15BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder1_5, odds, bgTeamsStatisitcsRepo);
                var overUnder25BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder2_5, odds, bgTeamsStatisitcsRepo);
                var overUnder35BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder3_5, odds, bgTeamsStatisitcsRepo);
                var rangeBetData = analysisServices.GetRangeBetData(odds, bgTeamsStatisitcsRepo);
                var bothScoreBetData = analysisServices.GetBothScoreBetData(odds, bgTeamsStatisitcsRepo);
                var winDrawLoseBetData = analysisServices.GetWinDrawLoseBetData(odds, bgTeamsStatisitcsRepo);
                List<string> fullTimeDescription = null;
                List<string> overUnder15Description = null;
                List<string> overUnder25Description = null;
                List<string> overUnder35Description = null;
                List<string> rangeDescription = null;
                List<string> bothScoreDescription = null;
                List<string> doubleDescription = null;
                switch (tips.FullTimeTitle.ToUpper())
                {
                    case "HOME WIN":
                        fullTimeDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_1, match.Home, match.Away, fullTimeBetData);
                        break;
                    case "DRAW":
                        fullTimeDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_X, match.Home, match.Away, fullTimeBetData);
                        break;
                    case "AWAY WIN":
                        fullTimeDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_2, match.Home, match.Away, fullTimeBetData);
                        break;
                };
                switch (tips.OverUnder1_5Title.ToUpper())
                {
                    case "OVER 1.5":
                        overUnder15Description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Over1_5, match.Home, match.Away, overUnder15BetData);
                        break;
                    case "UNDER 1.5":
                        overUnder15Description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Under1_5, match.Home, match.Away, overUnder15BetData);
                        break;
                };
                switch (tips.OverUnder2_5Title.ToUpper())
                {
                    case "OVER 2.5":
                        overUnder25Description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Over2_5, match.Home, match.Away, overUnder25BetData);
                        break;
                    case "UNDER 2.5":
                        overUnder25Description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Under2_5, match.Home, match.Away, overUnder25BetData);
                        break;
                };
                switch (tips.OverUnder3_5Title.ToUpper())
                {
                    case "OVER 3.5":
                        overUnder35Description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Over3_5, match.Home, match.Away, overUnder35BetData);
                        break;
                    case "UNDER 3.5":
                        overUnder35Description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Under3_5, match.Home, match.Away, overUnder35BetData);
                        break;
                };
                switch (tips.RangeTitle.ToUpper())
                {
                    case "RANGE 0-1":
                        rangeDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_0_1, match.Home, match.Away, rangeBetData);
                        break;
                    case "RANGE 2-3":
                        rangeDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_2_3, match.Home, match.Away, rangeBetData);
                        break;
                    case "RANGE 4+":
                        rangeDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_4Plus, match.Home, match.Away, rangeBetData);
                        break;
                }
                switch (tips.BothScoreTitle.ToUpper())
                {
                    case "BOTH SCORE":
                        bothScoreDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_BothScore, match.Home, match.Away, bothScoreBetData);
                        break;
                    case "NO B.SCORE":
                        bothScoreDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_BothNoScore, match.Home, match.Away, bothScoreBetData);
                        break;
                }
                switch (tips.DoubleTitle.ToUpper())
                {
                    case "DOUBLE 1X":
                        doubleDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_1X, match.Home, match.Away, winDrawLoseBetData);
                        break;
                    case "DOUBLE 12":
                        doubleDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_12, match.Home, match.Away, winDrawLoseBetData);
                        break;
                    case "DOUBLE 2X":
                        doubleDescription = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_2X, match.Home, match.Away, winDrawLoseBetData);
                        break;
                }
                descriptions = new MatchTipsDescriptions(fullTimeDescription.Where(i => !string.IsNullOrEmpty(i)).ToList(),
                    overUnder15Description.Where(i => !string.IsNullOrEmpty(i)).ToList(),
                    overUnder25Description.Where(i => !string.IsNullOrEmpty(i)).ToList(),
                    overUnder35Description.Where(i => !string.IsNullOrEmpty(i)).ToList(),
                    rangeDescription.Where(i => !string.IsNullOrEmpty(i)).ToList(),
                    bothScoreDescription.Where(i => !string.IsNullOrEmpty(i)).ToList(),
                    doubleDescription.Where(i => !string.IsNullOrEmpty(i)).ToList()
                    );
            }
            return descriptions;
        }
        public BgDataBetTips GetMatchTips(BgMatch match, BgTeamsAnalysisRepository bgTeamsAnalysisRepository)
        {
            var matchItem = new Dictionary<long, BgMatch>() { { match.MatchId, match } };
            var statistics = BgDataServices.Instance.GetStatisticsTotals(matchItem.Values.ToList());
            var statiticsTotals = new Dictionary<long, List<BgMatchStatistics>>() { { match.MatchId, statistics } };
            var statisticsDetails = BgDataServices.Instance.GetStatisticsDetails(matchItem.Values.ToList());
            var impliedStatistics = BgDataServices.Instance.CreateImpliedStatistics(matchItem, statiticsTotals, statisticsDetails);
            var tips = getBetTips(match.MatchId, impliedStatistics);
            if (tips != null)
            {
                var tipsDescriptions = PrepareTipsDescriptions(match, tips, bgTeamsAnalysisRepository);
                tips.FullTimeTipDescription = tipsDescriptions.FullTimeTipDescription;
                tips.OverUnder1_5TipDescription = tipsDescriptions.OverUnder1_5TipDescription;
                tips.OverUnder2_5TipDescription = tipsDescriptions.OverUnder2_5TipDescription;
                tips.OverUnder3_5TipDescription = tipsDescriptions.OverUnder3_5TipDescription;
                tips.RangeTipDescription = tipsDescriptions.RangeTipDescription;
                tips.BothScoreTipDescription = tipsDescriptions.BothScoreTipDescription;
                tips.DoubleTipDescription = tipsDescriptions.DoubleTipDescription;
            }
            return tips;
        }

        #region Overview
        private BgMatchSummaryOverview GetSummaryOverview(BgSummaryMatchInfo info,
            BgSummaryMatchAnalysis analysis,
            SummaryDigestData digest)
        {
            var sentences = new List<string>();
            sentences.Add($"{info.GameDayOfWeek}, {info.GameDate} at {info.GameTime}");
            sentences.Add($"{info.LeagueName}");
            sentences.Add($"{info.HomeTeam} Vs {info.AwayTeam}");
            var overview = new BgMatchSummaryOverviewParagraph(sentences);
            BgMatchSummaryOverviewParagraph oddsParagraph = null;
            BgMatchSummaryOverviewParagraph homeLastGameParagraph = null;
            BgMatchSummaryOverviewParagraph homeLast3GameParagraph = null;
            BgMatchSummaryOverviewParagraph awayLastGameParagraph = null;
            BgMatchSummaryOverviewParagraph awayLast3GameParagraph = null;
            BgMatchSummaryOverviewParagraph h2hGameParagraph = null;
            BgMatchSummaryOverviewParagraph homeStatisticsParagraph = null;
            BgMatchSummaryOverviewParagraph homePlayedAtHomeStatisticsParagraph = null;
            BgMatchSummaryOverviewParagraph awayStatisticsParagraph = null;
            BgMatchSummaryOverviewParagraph awayPlayedAwayStatisticsParagraph = null;
            if (info.HomeOdd.HasValue && info.AwayOdd.HasValue)
            {
                oddsParagraph = PrepareParagraphOddsAnalysis(info);
                homeLastGameParagraph = PrepareParagraphLastGame(info, info.HomeTeamId, info.HomeTeam, analysis.LastGamesHome.All.FirstOrDefault());
                if (analysis.LastGamesHome.All.Count > 2)
                    homeLast3GameParagraph = PrepareParagraphLast3Games(analysis.LastGamesHome.All.Take(3).ToList());
                awayLastGameParagraph = PrepareParagraphLastGame(info, info.AwayTeamId, info.AwayTeam, analysis.LastGamesAway.All.FirstOrDefault());
                if (analysis.LastGamesAway.All.Count > 2)
                    awayLast3GameParagraph = PrepareParagraphLast3Games(analysis.LastGamesAway.All.Take(3).ToList());
                h2hGameParagraph = PrepareParagraphHead2Head(info, analysis.Head2HeadGamesHome.All.FirstOrDefault());
                homeStatisticsParagraph = PrepareParagraphStatisticsAllGames(info.HomeTeamId, info.HomeTeam, analysis.LastGamesHome.All.Take(6).ToList(), false, null);
                homePlayedAtHomeStatisticsParagraph = PrepareParagraphStatisticsAllGames(info.HomeTeamId, info.HomeTeam, analysis.LastGamesHome.Home.Take(6).ToList(), true, "home");
                awayStatisticsParagraph = PrepareParagraphStatisticsAllGames(info.AwayTeamId, info.AwayTeam, analysis.LastGamesAway.All.Take(6).ToList(), false, null);
                awayPlayedAwayStatisticsParagraph = PrepareParagraphStatisticsAllGames(info.AwayTeamId, info.AwayTeam, analysis.LastGamesAway.Away.Take(6).ToList(), true, "away");
            }
            var all = new BgMatchSummaryOverview(overview,
                oddsParagraph,
                homeLastGameParagraph,
                homeLast3GameParagraph,
                awayLastGameParagraph,
                awayLast3GameParagraph,
                h2hGameParagraph,
                homeStatisticsParagraph,
                homePlayedAtHomeStatisticsParagraph,
                awayStatisticsParagraph,
                awayPlayedAwayStatisticsParagraph
                );
            return all;
        }

        private BgMatchSummaryOverviewParagraph PrepareParagraphStatisticsAllGames(int teamId, string teamName,
            List<BgCategoryMatch> lastGames, bool playedAtSide, string side)
        {
            var sentence = new List<string>();
            Func<List<BgCategoryMatch>, int, string> translate = (matches, currTeamId) =>
            {
                var scores = "(";
                foreach (var win in matches)
                {
                    var otherTeam = win.HomeTeamId == teamId
                        ? win.AwayTeam
                        : win.HomeTeam;
                    scores = $"{scores}{otherTeam} {win.HomeScore}-{win.AwayScore},";
                }
                scores = scores.Substring(0, scores.LastIndexOf(','));
                scores = $"{scores})";
                return scores;
            };
            if (lastGames.Count > 0)
            {
                var wins = lastGames.Where(g => g.WdlResult == "W");
                var losts = lastGames.Where(g => g.WdlResult == "L");
                var draws = lastGames.Where(g => g.WdlResult[0] == 'D');
                var homeAwayItem = string.Empty;
                switch (side)
                {
                    case "home":
                        homeAwayItem = "at home";
                        break;
                    case "away":
                        homeAwayItem = "away";
                        break;
                }
                sentence.Add($"In ");
                if (playedAtSide)
                {
                    sentence.Add($"{teamName}");
                    sentence.Add($"'s last {lastGames.Count} games when they played {homeAwayItem} they");
                }
                else
                {
                    sentence.Add($"her last {lastGames.Count} games ");
                    sentence.Add($"{teamName}");
                }
                //var part1 = playedAtSide
                //    ? $"{teamName}'s last {lastGames.Count} games when they played {homeAwayItem} they"
                //    : $"her last {lastGames.Count} games {teamName}";
                var part2 = string.Empty;
                var part3 = string.Empty;
                var part4 = string.Empty;

                if (wins.Count() > 0)
                    part2 = $" Win {wins.Count()} games {translate(wins.ToList(), teamId)}";
                if (losts.Count() > 0)
                    part3 = wins.Count() > 0
                        ? $", {losts.Count()} defeated {translate(losts.ToList(), teamId)}"
                        : $" Defeated in {losts.Count()} games {translate(losts.ToList(), teamId)}";
                if (draws.Count() > 0)
                    part4 = losts.Count() > 0 || wins.Count() > 0
                        ? $" and {draws.Count()} ended in draw {translate(draws.ToList(), teamId)}"
                        : $" {draws.Count()} games ended in draw {translate(draws.ToList(), teamId)}";

                sentence.Add($"{part2}{part3}{part4}");
            }
            var paragraph = new BgMatchSummaryOverviewParagraph(sentence);
            return paragraph;
        }

        private BgMatchSummaryOverviewParagraph PrepareParagraphHead2Head(BgSummaryMatchInfo info, BgCategoryMatch head2HeadGame)
        {
            var sentences = new List<string>();
            if (head2HeadGame != null)
            {
                if (!DateTime.TryParseExact(info.GameDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime matchDate))
                    throw new Exception($"Invalid match date found {info.GameDate}");
                if (!DateTime.TryParseExact(head2HeadGame.GameDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime lastH2HMatchDate))
                    throw new Exception($"Invalid match date found {head2HeadGame.GameDate}");

                var daysAgo = (matchDate - lastH2HMatchDate).TotalDays;
                var matchHomeResult = "draw";
                switch (head2HeadGame.WdlResult[0])
                {
                    case 'W':
                        matchHomeResult = "win";
                        break;
                    case 'L':
                        matchHomeResult = "lost";
                        break;
                }
                if (daysAgo <= 365)
                {
                    if (matchHomeResult != "draw")
                        sentences.Add($"The previous fixture between the teams saw {info.HomeTeam} {matchHomeResult} {head2HeadGame.HomeScore}-{head2HeadGame.AwayScore}");
                    else
                        sentences.Add($"The previous fixture between the teams ended in a draw {head2HeadGame.HomeScore}-{head2HeadGame.AwayScore}");
                }
                else
                {
                    if (matchHomeResult != "draw")
                        sentences.Add($"The previous fixture between the teams was more than a year. In this match {info.HomeTeam} {matchHomeResult} {head2HeadGame.HomeScore}-{head2HeadGame.AwayScore}");
                    else
                        sentences.Add($"The previous fixture between the teams was more than a year and ended in a draw {matchHomeResult} {head2HeadGame.HomeScore}-{head2HeadGame.AwayScore}");
                }
            }
            var paragraph = new BgMatchSummaryOverviewParagraph(sentences);
            return paragraph;
        }

        private BgMatchSummaryOverviewParagraph PrepareParagraphLast3Games(List<BgCategoryMatch> lastGames)
        {
            var sentences = new List<string>();
            if (lastGames.Count > 0)
            {
                var allWdl = string.Join("", lastGames.Select(g => g.WdlResult[0]));
                switch (allWdl)
                {
                    case "WWW":
                        sentences.Add("They have win their last three matches in a row");
                        break;
                    case "DDD":
                        sentences.Add("In the last three games, have a three match ended in a draw streak");
                        break;
                    case "LLL":
                        sentences.Add("They have lost their last three matches in a row");
                        break;
                    case "DWW":
                    case "WWD":
                    case "WDW":
                        sentences.Add("In the last three games, have two matches win and one match ended in a draw");
                        break;
                    case "LWW":
                        sentences.Add("In the last three games, have two matches win and one match defeated");
                        break;
                    case "WLW":
                    case "WWL":
                        sentences.Add("In the last three games, have two matches win and one match lose");
                        break;
                    case "WDD":
                    case "DDW":
                    case "DWD":
                        sentences.Add("In the last three games, have a three match unbeaten streak");
                        break;
                    case "LWD":
                    case "LDW":
                    case "DLW":
                    case "WLD":
                    case "WDL":
                    case "DWL":
                        sentences.Add("In the last three games, have Instability result when in their last three games one match have win, one defeated and one ended in a draw");
                        break;
                    case "LLW":
                    case "LWL":
                    case "WLL":
                        sentences.Add("In the last three games, have only won one of their last three games");
                        break;
                    case "LDD":
                    case "DDL":
                    case "DLD":
                        sentences.Add("In the last three games, have one match defeated and two matches ended in a draw");
                        break;
                    case "DLL":
                    case "LLD":
                    case "LDL":
                        sentences.Add("In the last three games, have two matches defeated and one match ended in a draw");
                        break;
                }
            }
            var paragraph = new BgMatchSummaryOverviewParagraph(sentences);
            return paragraph;
        }

        private BgMatchSummaryOverviewParagraph PrepareParagraphLastGame(BgSummaryMatchInfo info, int teamId, string team, BgCategoryMatch lastGame)
        {
            var sentence = new List<string>();
            if (lastGame != null)
            {
                if (!DateTime.TryParseExact(info.GameDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime matchDate))
                    throw new Exception($"Invalid match date found {info.GameDate}");
                if (!DateTime.TryParseExact(lastGame.GameDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime lastHomeMatchDate))
                    throw new Exception($"Invalid match date found {lastGame.GameDate}");

                var daysAgo = (matchDate - lastHomeMatchDate).TotalDays;
                var otherTeam = lastGame.HomeTeamId == teamId
                    ? lastGame.AwayTeam
                    : lastGame.HomeTeam;
                sentence.Add($"{team}");
                var daysAgoSentence = $" last match was {daysAgo} days ago";
                sentence.Add($"{daysAgoSentence} ");
                var homeResult = lastGame.WdlResult;
                switch (lastGame.WdlResult)
                {
                    case "W":
                        if (lastGame.HomeOdd >= 1 && lastGame.HomeOdd <= (decimal)1.4)
                            sentence.Add($"won {lastGame.HomeScore}-{lastGame.AwayScore} a game in which she was a clear favorite over {otherTeam}");
                        else if (lastGame.HomeOdd > (decimal)1.4 && lastGame.HomeOdd <= (decimal)1.7)
                            sentence.Add($"won {lastGame.HomeScore}-{lastGame.AwayScore} a game in which she was favorite over {otherTeam}");
                        else if (lastGame.HomeOdd > (decimal)1.7 && lastGame.HomeOdd <= (decimal)2.5)
                            sentence.Add($"match saw them triumph {lastGame.HomeScore}-{lastGame.AwayScore} over {otherTeam}");
                        else if (lastGame.HomeOdd > (decimal)2.5 && lastGame.HomeOdd <= (decimal)4)
                            sentence.Add($"Surprisingly beat {otherTeam} {lastGame.HomeScore}-{lastGame.AwayScore}");
                        else if (lastGame.HomeOdd > (decimal)4)
                            sentence.Add($"won a very surprising result {lastGame.HomeScore}-{lastGame.AwayScore} over {otherTeam}");
                        break;
                    case "L":
                        if (lastGame.AwayOdd >= 1 && lastGame.AwayOdd <= (decimal)1.4)
                            sentence.Add($"defeated in a very surprising result {lastGame.HomeScore}-{lastGame.AwayScore} by {otherTeam}");
                        else if (lastGame.AwayOdd > (decimal)1.4 && lastGame.AwayOdd <= (decimal)1.7)
                            sentence.Add($"surprisingly defeated by {otherTeam} {lastGame.HomeScore}-{lastGame.AwayScore}");
                        else if (lastGame.AwayOdd > (decimal)1.7 && lastGame.AwayOdd <= (decimal)2.9)
                            sentence.Add($"defeated {lastGame.HomeScore}-{lastGame.AwayScore} by {otherTeam}");
                        else if (lastGame.AwayOdd > (decimal)2.9)
                            sentence.Add($"expected loss of {lastGame.HomeScore}-{lastGame.AwayScore} by {otherTeam}");
                        break;
                    default:
                        sentence.Add($"end in a draw {lastGame.HomeScore}-{lastGame.AwayScore} with {lastGame.AwayTeam}");
                        break;
                }
            }
            var paragraph = new BgMatchSummaryOverviewParagraph(sentence);
            return paragraph;
        }

        private BgMatchSummaryOverviewParagraph PrepareParagraphOddsAnalysis(BgSummaryMatchInfo info)
        {
            var sentences = new List<string>();
            if (info.HomeOdd >= 1 && info.HomeOdd <= 1.4)
                sentences.Add($"In this match any result other than a victory for {info.HomeTeam} will be considered a complete surprise");
            else if (info.HomeOdd > 1.4 && info.HomeOdd.Value <= 1.7)
                sentences.Add($"In this match {info.HomeTeam} is favorite to win the game");
            if (info.AwayOdd >= 1 && info.AwayOdd <= 1.4)
                sentences.Add($"In this match even though {info.AwayTeam} is playing away, any result other than a victory for {info.AwayTeam} will be considered a complete surprise");
            else if (info.AwayOdd > 1.4 && info.AwayOdd.Value <= 1.7)
                sentences.Add($"In this match, even though {info.AwayTeam} is playing away, she is a favorite to win");
            else
                sentences.Add($"This match is between two evenly matched teams");
            var paragraph = new BgMatchSummaryOverviewParagraph(sentences);
            return paragraph;
        }

        #endregion
        #region Digest Data
        private Dictionary<string, SoccerWinDrawLossScores> DigestAggregator(List<BgCategoryMatch> population)
        {
            Func<List<BgCategoryMatch>, SoccerWinDrawLossScores> digest = (data) =>
            {
                var wins = data.Count(d => d.WdlResult == "W");
                var draws = data.Count(d => d.WdlResult == "D" || d.WdlResult == "D#Special");
                var losses = data.Count(d => d.WdlResult == "L");
                var noGoals = data.Count(d => d.HomeScore == 0 && d.AwayScore == 0);
                var goals1 = data.Count(d => (d.HomeScore + d.AwayScore) == 1);
                var goals2 = data.Count(d => (d.HomeScore + d.AwayScore) == 2);
                var goals3 = data.Count(d => (d.HomeScore + d.AwayScore) == 3);
                var goals4Plus = data.Count(d => (d.HomeScore + d.AwayScore) > 3);
                var oddsPower = data.Select(d => new OddsPowerDetails(d.BetIndexes.OddPower, d.WdlResult, d.GameDate)).ToList();
                return new SoccerWinDrawLossScores(wins, draws, losses, noGoals, goals1, goals2, goals3, goals4Plus, oddsPower);
            };
            var res = new Dictionary<string, SoccerWinDrawLossScores>()
            {
                { "Last5Games", digest(population.Take(5).OrderBy(p => p.ActualGameDate).ToList()) },
                { "Last10Games", digest(population.Take(10).OrderBy(p => p.ActualGameDate).ToList()) },
                { "Last15Games", digest(population.Take(15).OrderBy(p => p.ActualGameDate).ToList()) },
                { "Last20Games", digest(population.Take(20).OrderBy(p => p.ActualGameDate).ToList()) },
            };
            return res;
        }
        private SummaryDigestData DigestSummaryData(BgSummaryMatchAnalysis analysis)
        {
            var home = new DigestData(DigestAggregator(analysis.LastGamesHome.All),
                DigestAggregator(analysis.LastGamesHome.Favorite),
                DigestAggregator(analysis.LastGamesHome.Netral),
                DigestAggregator(analysis.LastGamesHome.Underdog),
                DigestAggregator(analysis.LastGamesHome.Home),
                DigestAggregator(analysis.LastGamesHome.HomeFavorite),
                DigestAggregator(analysis.LastGamesHome.HomeNetral),
                DigestAggregator(analysis.LastGamesHome.HomeUnderdog),
                DigestAggregator(analysis.LastGamesHome.Away),
                DigestAggregator(analysis.LastGamesHome.AwayFavorite),
                DigestAggregator(analysis.LastGamesHome.AwayNetral),
                DigestAggregator(analysis.LastGamesHome.AwayUnderdog));
            var away = new DigestData(DigestAggregator(analysis.LastGamesAway.All),
                DigestAggregator(analysis.LastGamesAway.Favorite),
                DigestAggregator(analysis.LastGamesAway.Netral),
                DigestAggregator(analysis.LastGamesAway.Underdog),
                DigestAggregator(analysis.LastGamesAway.Home),
                DigestAggregator(analysis.LastGamesAway.HomeFavorite),
                DigestAggregator(analysis.LastGamesAway.HomeNetral),
                DigestAggregator(analysis.LastGamesAway.HomeUnderdog),
                DigestAggregator(analysis.LastGamesAway.Away),
                DigestAggregator(analysis.LastGamesAway.AwayFavorite),
                DigestAggregator(analysis.LastGamesAway.AwayNetral),
                DigestAggregator(analysis.LastGamesAway.AwayUnderdog));
            var res = new SummaryDigestData(home, away);
            return res;
        }
        #endregion

        #region Analysis
        private BgDataMatchIndexes getBetIndexes(BgMatchIndexes allIndexes)
        {
            BgDataMatchIndexes indexes = null;
            if (allIndexes != null)
            {
                indexes = new BgDataMatchIndexes(allIndexes.OverUnder1_5Index.ToString(),
                    allIndexes.OverUnder2_5Index.ToString(),
                    allIndexes.OverUnder3_5Index.ToString(),
                    ((BgRangeIndex)allIndexes.RangeIndex).GetDescription<BgRangeIndex>(),
                    allIndexes.BothScoreIndex ? "Yes" : "No",
                    allIndexes.DrawWinIndex ? "Yes" : "No",
                    allIndexes.NoDrawIndex ? "Yes" : "No",
                    allIndexes.LoseDrawIndex ? "Yes" : "No",
                    allIndexes.OddPower
                    );
            }
            return indexes;
        }
        private BgSummaryMatchAnalysis GetSummaryAnalysis(BgTeamsAnalysisRepository repo,
            DateTime gameStart,
            int gmtOffset)
        {
            Func<List<BgMatchWithIndexes>, bool, List<BgCategoryMatch>> convert = (matches, switchWdl) =>
            {
                var lst = new List<BgCategoryMatch>();
                var pastMatches = matches.Where(m => m.GameStart.AddHours(-gmtOffset) < gameStart)
                    .Take(100)
                    .ToList();
                for (var index = 1; index <= pastMatches.Count; index++)
                {
                    var item = pastMatches[index - 1];
                    var actualGameStart = pastMatches[index - 1].GameStart.AddHours(-gmtOffset);
                    var wdl = switchWdl
                        ? oppositeWDL(getWDL(item.Sport, item.Status, item.Indexes.WDL.ToString()))
                        : getWDL(item.Sport, item.Status, item.Indexes.WDL.ToString());
                    lst.Add(new BgCategoryMatch(actualGameStart,
                        actualGameStart.ToString("dd.MM.yyyy"),
                        index,
                        item.MatchId,
                        item.LeagueId,
                        item.League,
                        getCountryFlag(pastMatches[index - 1].CountryId),
                        item.HomeId,
                        item.Home,
                        item.HomeId.ToString(),
                        item.AwayId,
                        item.Away,
                        item.AwayId.ToString(),
                        item.HomeScore.Value,
                        item.AwayScore.Value,
                        wdl,
                        item.HomeOdd.Value,
                        item.DrawOdd.Value,
                        item.AwayOdd.Value,
                        item.HQ1,
                        item.AQ1,
                        item.HQ2,
                        item.AQ2,
                        item.HQ3,
                        item.AQ3,
                        item.HQ4,
                        item.AQ4,
                        getBetIndexes(item.Indexes)
                    ));
                }
                return lst;
            };
            var lastGamesHome = new CategorizedMatches(
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.All_All], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.All_Favorite], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.All_Netral], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.All_Underdog], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Home_All], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Home_Favorite], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Home_Netral], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Home_Underdog], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Away_All], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Away_Favorite], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Away_Netral], false),
                convert(repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.Away_Underdog], false)
                );
            var lastGamesAway = new CategorizedMatches(
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.All_All], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.All_Favorite], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.All_Netral], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.All_Underdog], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Home_All], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Home_Favorite], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Home_Netral], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Home_Underdog], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Away_All], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Away_Favorite], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Away_Netral], false),
                convert(repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.Away_Underdog], false)
                );
            var lastGamesHomeH2H = new CategorizedMatches(
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_All], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_Favorite], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_Netral], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_Underdog], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Home_All], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Home_Favorite], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Home_Netral], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Home_Underdog], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_All], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Favorite], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Netral], false),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Underdog], false)
                );
            var lastGamesAwayH2H = new CategorizedMatches(
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_All], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_Underdog], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_Netral], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.All_Favorite], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_All], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Underdog], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Netral], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Favorite], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Home_All], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Home_Underdog], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Netral], true),
                convert(repo.H2hFilteredWithIndexes[BgTeamsFilter.Away_Favorite], true)
                );
            var analysis = new BgSummaryMatchAnalysis(lastGamesHome, lastGamesAway, lastGamesHomeH2H, lastGamesAwayH2H);
            return analysis;
        }
        #endregion

        #region Info
        private BgSummaryMatchInfo GetMatchInfo(BgMatch match, int gmtOffset)
        {
            var gameTimeOffset = match.GameStart.AddHours(-gmtOffset);
            var status = match.Status;
            if (match.Status.Contains(":"))
                status = gameTimeOffset.ToString("HH:mm");
            var info = new BgSummaryMatchInfo(match.MatchId,
                match.Sport,
                match.Country,
                match.CountryId.Value,
                getCountryFlag(match.CountryId),
                match.LeagueId,
                match.League,
                match.HomeId,
                match.Home,
                match.HomeId.ToString(),
                match.AwayId,
                match.Away,
                match.AwayId.ToString(),
                gameTimeOffset.Date.DayOfWeek.ToString(),
                gameTimeOffset.Date.ToString("dd.MM.yyyy"),
                gameTimeOffset.ToString("HH:mm"),
                status,
                (double?)match.HomeOdd,
                (double?)match.DrawOdd,
                (double?)match.AwayOdd,
                match.HomeScore.ToString(),
                match.AwayScore.ToString(),
                getBetResult(match));
            return info;
        }
        #endregion
        #endregion

        #region GetMatchesWithBets
        public List<BgDataMatch> GetMatchesWithBets(BgFeedFullRepository repo,
            Dictionary<long, BgMatchImpliedStatistics> impliedStatistics,
            BgSport sport)
        {
            var openBets = repo.AllMatches
                .ConvertAll<BgDataMatch>(m =>
                            new BgDataMatch(m.MatchId,
                                sport,
                                m.HomeId,
                                m.Home,
                                m.HomeId.ToString(),
                                m.AwayId,
                                m.Away,
                                m.AwayId.ToString(),
                                m.GameStart.ToString("HH:mm"),
                                getBetStatus(m.Status, m.GameStart.ToString("HH:mm")),
                                m.HomeOdd.HasValue
                                    ? Decimal.ToDouble(m.HomeOdd.Value)
                                    : default(double),
                                m.DrawOdd.HasValue
                                    ? Decimal.ToDouble(m.DrawOdd.Value)
                                    : default(double),
                                m.AwayOdd.HasValue
                                    ? Decimal.ToDouble(m.AwayOdd.Value)
                                    : default(double),
                                m.HomeScore.HasValue
                                    ? m.HomeScore.Value.ToString()
                                    : string.Empty,
                                m.AwayScore.HasValue
                                    ? m.AwayScore.Value.ToString()
                                    : string.Empty,
                                getBetResult(m),
                                getBetTips(m.MatchId, impliedStatistics)
                            ));
            return openBets;
        }
        #endregion
    }
}
