﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;

namespace TotoWinner.Services
{
    public class BgMapperServices : Singleton<BgMapperServices>, IBgMapperServices
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        private BgMapperServices()
        {
        }
        public MapperRespository GetBgMapper()
        {
            var mapper = persistance.GetBgMappers();
            var bgIdBgMap = new Dictionary<int, List<BgMapperItem>>();
            var bgIdItemName = new Dictionary<ProviderKey, string>();
            foreach (var item in mapper)
            {
                if (bgIdBgMap.ContainsKey(item.BgId))
                    bgIdBgMap[item.BgId].Add(item);
                else
                    bgIdBgMap[item.BgId] = new List<BgMapperItem>() { item };

                var pk = new ProviderKey(item.BgId, item.Sport, item.BgType);
                if (!bgIdItemName.ContainsKey(pk))
                    bgIdItemName.Add(pk, item.BgName);
            }
            var goldserveDataBgKey = mapper
                .Where(g => !string.IsNullOrEmpty(g.GoalserveName))
                .ToDictionary(m => new ProviderData(m.GoalserveId, m.Sport, m.BgType, m.GoalserveName), m => new ProviderKey(m.BgId, m.Sport, m.BgType));
            /**In Case of manual duplication **/
            //var population = mapper.Where(g => !string.IsNullOrEmpty(g.ManualName));
            //var manualDataBgKey = new Dictionary<ProviderData, ProviderKey>();
            //foreach (var m in population)
            //{
            //    var key = new ProviderData(null, m.Sport, m.BgType, m.ManualName);
            //    if (manualDataBgKey.ContainsKey(key))
            //        continue;
            //    manualDataBgKey.Add(key, new ProviderKey(m.BgId, m.Sport, m.BgType));
            //}
            /****/
            var manualDataBgKey = mapper
                .Where(g => !string.IsNullOrEmpty(g.ManualName))
                .ToDictionary(m => new ProviderData(null, m.Sport, m.BgType, m.ManualName), m => new ProviderKey(m.BgId, m.Sport, m.BgType));

            var repository = new MapperRespository(bgIdBgMap, goldserveDataBgKey, manualDataBgKey, bgIdItemName);
            return repository;
        }
        public void AddOrUpdateMapping(List<ManualMatch> matches)
        {
            if (matches.All(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue))
                return;
            var mapper = GetBgMapper();
            var bgMappers = new List<BgMapper>();
            var bgManualMapper = new List<BgManualMapper>();
            foreach (var match in matches)
            {
                var bgSport = match.Sport;
                if (!match.CountryId.HasValue && !string.IsNullOrEmpty(match.Country))
                    MergeManualMapping(bgMappers, bgManualMapper, bgSport, BgMapperType.Countries, match.Country, mapper.BgCountriesNames, mapper.ManualCountries, mapper.NextBgCountryId);
                if (!match.LeagueId.HasValue && !string.IsNullOrEmpty(match.League))
                    MergeManualMapping(bgMappers, bgManualMapper, bgSport, BgMapperType.Leagues, match.League, mapper.BgLeaguesNames, mapper.ManualLeagues, mapper.NextBgLeagueId);
                if (!match.HomeId.HasValue && !string.IsNullOrEmpty(match.Home))
                    MergeManualMapping(bgMappers, bgManualMapper, bgSport, BgMapperType.Teams, match.Home, mapper.BgTeamsNames, mapper.ManualTeams, mapper.NextBgTeamId);
                if (!match.AwayId.HasValue && !string.IsNullOrEmpty(match.Away))
                    MergeManualMapping(bgMappers, bgManualMapper, bgSport, BgMapperType.Teams, match.Away, mapper.BgTeamsNames, mapper.ManualTeams, mapper.NextBgTeamId);
            }
            persistance.MergeMappers(bgMappers, bgManualMapper);
        }
        //private void MergeCountryMapping(List<BgMapper> bgMappers,
        //    List<BgManualMapper> bgManualMapper,
        //    BgSport bgSport,
        //    string country,
        //    MapperRespository respository)
        //{
        //    try
        //    {
        //        var countryNameKey = new ProviderName(bgSport, country);
        //        var providerData = new ProviderData(null, bgSport, country);
        //        if (respository.BgCountriesNames.ContainsKey(countryNameKey))
        //        {
        //            var bgCountryId = respository.BgCountriesNames[countryNameKey];
        //            if (!respository.ManualCountries.ContainsKey(providerData))
        //            {
        //                bgManualMapper.Add(new BgManualMapper(bgCountryId, country));
        //                respository.ManualCountries.Add(providerData, bgCountryId);
        //            }
        //        }
        //        else if (!respository.ManualCountries.ContainsKey(providerData)) //in case BgName != Provider Name, manual change to be ignored
        //        {
        //            bgMappers.Add(new BgMapper(respository.NextBgCountryId, bgSport, BgMapperType.Countries, country));
        //            respository.BgCountriesNames.Add(countryNameKey, respository.NextBgCountryId);
        //            bgManualMapper.Add(new BgManualMapper(respository.NextBgCountryId, country));
        //            respository.ManualCountries.Add(providerData, respository.NextBgCountryId);
        //            respository.NextBgCountryId++;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"Failed to merge manual country {country}", ex);
        //    }
        //}
        private void MergeManualMapping(List<BgMapper> bgMappers,
            List<BgManualMapper> bgManualMapper,
            BgSport bgSport,
            BgMapperType bgMapperType,
            string objectName,
            Dictionary<ProviderName, int> bgObjectsNames,
            Dictionary<ProviderData, ProviderKey> manualObjects,
            BgObjectId nextBgId)
        {
            try
            {
                var pnKey = new ProviderName(bgSport, objectName);
                var providerData = new ProviderData(null, bgSport, bgMapperType, objectName);
                if (bgObjectsNames.ContainsKey(pnKey))
                {
                    var bgId = bgObjectsNames[pnKey];
                    if (!manualObjects.ContainsKey(providerData))
                    {
                        bgManualMapper.Add(new BgManualMapper(bgId, objectName));
                        manualObjects.Add(providerData, new ProviderKey(bgId, bgSport, bgMapperType));
                    }
                }
                else if (!manualObjects.ContainsKey(providerData)) //in case BgName != Provider Name, manual change to be ignored
                {
                    bgMappers.Add(new BgMapper(nextBgId.NextId, bgSport, bgMapperType, objectName));
                    bgObjectsNames.Add(pnKey, nextBgId.NextId);
                    bgManualMapper.Add(new BgManualMapper(nextBgId.NextId, objectName));
                    manualObjects.Add(providerData, new ProviderKey(nextBgId.NextId, bgSport, bgMapperType));
                    nextBgId++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to merge manual {bgMapperType} {objectName}", ex);
            }
        }
        public void AddOrUpdateMapping(HashSet<GoalserveMatch> matchesToAdd)
        {
            var mapper = GetBgMapper();
            var bgMappers = new List<BgMapper>();
            var bgGoalserveMapper = new List<BgGoalserveMapper>();

            foreach (var match in matchesToAdd)
            {
                var bgSport = (BgSport)match.SportId;
                if (match.CountryId.HasValue)
                    MergeGoldserveMapping(bgMappers, bgGoalserveMapper, bgSport, BgMapperType.Countries, match.Country, match.CountryId, mapper.BgCountriesNames, mapper.GoldserveCountries, mapper.NextBgCountryId);
                MergeGoldserveMapping(bgMappers, bgGoalserveMapper, bgSport, BgMapperType.Leagues, match.League, match.LeagueId, mapper.BgLeaguesNames, mapper.GoldserveLeaguesIds, mapper.NextBgLeagueId);
                MergeGoldserveMapping(bgMappers, bgGoalserveMapper, bgSport, BgMapperType.Teams, match.Home, match.HomeId, mapper.BgTeamsNames, mapper.GoldserveTeamsIds, mapper.NextBgTeamId);
                MergeGoldserveMapping(bgMappers, bgGoalserveMapper, bgSport, BgMapperType.Teams, match.Away, match.AwayId, mapper.BgTeamsNames, mapper.GoldserveTeamsIds, mapper.NextBgTeamId);
            }
            persistance.MergeMappers(bgMappers, bgGoalserveMapper);
        }
        private void MergeGoldserveMapping(List<BgMapper> bgMappers,
            List<BgGoalserveMapper> bgGoalserveMapper,
            BgSport bgSport,
            BgMapperType bgMapperType,
            string objectName,
            int? objectId,
            Dictionary<ProviderName, int> bgObjectsNames,
            Dictionary<ProviderData, int?> providerObjects,
            BgObjectId nextBgId)
        {
            try
            {
                var providerData = new ProviderData(objectId, bgSport, bgMapperType, objectName);
                var objectNameKey = new ProviderName(bgSport, objectName);
                if (bgObjectsNames.ContainsKey(objectNameKey)) //perfomance::filter bgObjectsNames by sport before 
                {
                    var bgObjectId = bgObjectsNames[objectNameKey];
                    if (!providerObjects.ContainsKey(providerData))
                    {
                        bgGoalserveMapper.Add(new BgGoalserveMapper(bgObjectId, objectId, objectName));
                        providerObjects.Add(providerData, bgObjectId);
                    }
                }
                else if (!providerObjects.ContainsKey(providerData)) //in case BgName != Provider Name, manual change to be ignored
                {
                    bgMappers.Add(new BgMapper(nextBgId.NextId, bgSport, bgMapperType, objectName));
                    bgObjectsNames.Add(objectNameKey, nextBgId.NextId);
                    bgGoalserveMapper.Add(new BgGoalserveMapper(nextBgId.NextId, objectId, objectName));
                    providerObjects.Add(providerData, nextBgId.NextId);
                    nextBgId++;
                }
            }
            catch (Exception ex)
            {
                var objectIdMsg = objectId.HasValue
                    ? $"{objectId.Value}"
                    : "No object Id";
                throw new Exception($"Failed to merge {bgMapperType} {objectName}, {objectIdMsg}", ex);
            }
        }

        //var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed(BgDataServices.Instance.BgSportToGoalserveSport(sport), date);
        public GoalserveMatchFeed GetGoalserveMatch(int bgHomeId, 
            int bgAwayId, 
            GoldserveMatchRepository goalserveFeed, 
            MapperRespository mapper)
        {
            if (!mapper.BgIdBgMap.TryGetValue(bgHomeId, out var homeList) ||
                !mapper.BgIdBgMap.TryGetValue(bgAwayId, out var awayList))
            {
                return null;
            }
            // Generate unique home-away ID pairs
            var permutations = homeList
                .Where(home => home.GoalserveId.HasValue)
                .SelectMany(home => awayList
                    .Where(away => away.GoalserveId.HasValue)
                    .Select(away => (home.GoalserveId.Value, away.GoalserveId.Value)))
                .ToHashSet();

            var goalserveMatch = permutations
                .Select(permutation => goalserveFeed.Feed
                    .FirstOrDefault(m => m.HomeId == permutation.Item1 && m.AwayId == permutation.Item2))
                .FirstOrDefault(match => match != null);
            return goalserveMatch;
        }
    }
}
