﻿using TotoWinner.Common;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace TotoWinner.Services
{
    public class BgMapperRepository
    {
        public ICacheProvider Cashe { get; set; }
        private int CacheExpirationInMin;
        public BgMapperRepository(int cacheExpirationInMin = 30) : this(new CacheProvider())
        {
            this.CacheExpirationInMin = cacheExpirationInMin;
        }
        private BgMapperRepository(ICacheProvider casheProvider)
        {
            this.Cashe = casheProvider;
        }

        public MapperRespository GetBgMapper()
        {
            var mapper = this.Cashe.Get("BgMapper") as MapperRespository;
            if (mapper == null)
            {
                mapper = BgMapperServices.Instance.GetBgMapper();
                this.Cashe.Set("BgMapper", mapper, this.CacheExpirationInMin);
            }
            return mapper;
        }
        public void ClearCache()
        {
            this.Cashe.Invalidate("BgMapper");
        }
    }
}
