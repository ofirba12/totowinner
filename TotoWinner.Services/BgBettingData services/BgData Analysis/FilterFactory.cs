﻿using System;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public static class FilterFactory
    {
        public static bool IsFilterSatisfied(BgMatchOdds m, int teamId, BgTeamsFilter filterType)
        {
            var isFilterSatisfied = false;
            switch (filterType)
            {
                case BgTeamsFilter.All_All:
                    isFilterSatisfied = true;
                    break;
                case BgTeamsFilter.All_Favorite:
                    if (m.HomeId == teamId && m.HomeOdd >= (decimal)1 && m.HomeOdd <= (decimal)1.79 && m.AwayOdd >= (decimal)1.8)
                        isFilterSatisfied = true;
                    if (m.AwayId == teamId && m.AwayOdd >= (decimal)1 && m.AwayOdd <= (decimal)1.79 && m.HomeOdd >= (decimal)1.8)
                        isFilterSatisfied = true;
                    break;
                case BgTeamsFilter.All_Netral:
                    if (m.HomeOdd >= (decimal)1 && m.HomeOdd <= (decimal)1.79 && m.AwayOdd >= (decimal)1 && m.AwayOdd <= (decimal)1.79)
                        isFilterSatisfied = true;
                    if (m.HomeOdd >= (decimal)1.8 && m.AwayOdd >= (decimal)1.8)
                        isFilterSatisfied = true;
                    break;
                case BgTeamsFilter.All_Underdog:
                    if (m.HomeId == teamId && m.AwayOdd >= (decimal)1 && m.AwayOdd <= (decimal)1.79 && m.HomeOdd >= (decimal)1.8)
                        isFilterSatisfied = true;
                    if (m.AwayId == teamId && m.HomeOdd >= (decimal)1 && m.HomeOdd <= (decimal)1.79 && m.AwayOdd >= (decimal)1.8)
                        isFilterSatisfied = true;
                    break;
                case BgTeamsFilter.Home_All:
                    isFilterSatisfied = m.HomeId == teamId;
                    break;
                case BgTeamsFilter.Away_All:
                    isFilterSatisfied = m.AwayId == teamId;
                    break;
            };
            return isFilterSatisfied;
        }
        public static Func<BgMatchForAnalysis, bool> GetFilter(int teamId, BgTeamsFilter filterType)
        {
            Func<BgMatchForAnalysis, bool> filter = (m) =>
            {
                if (m.HomeOdd != null && m.DrawOdd != null && m.AwayOdd != null)
                {
                    var matchOdds = new BgMatchOdds(m.Sport, m.HomeId, m.AwayId, m.HomeOdd.Value, m.DrawOdd.Value, m.AwayOdd.Value);
                    return IsFilterSatisfied(matchOdds, teamId, filterType);
                }
                return true;
            };

            //switch (filterType)
            //{
            //    case BgTeamsFilter.All_All:
            //        filter = (m) =>
            //        {
            //            return true;
            //        };
            //        break;
            //    case BgTeamsFilter.All_Favorite:
            //        filter = (m) =>
            //        {
            //            if (m.HomeId == teamId && m.HomeOdd >= (decimal)1 && m.HomeOdd <= (decimal)1.79 && m.AwayOdd >= (decimal)1.8)
            //                return true;
            //            if (m.AwayId == teamId && m.AwayOdd >= (decimal)1 && m.AwayOdd <= (decimal)1.79 && m.HomeOdd >= (decimal)1.8)
            //                return true;
            //            return false;
            //        };
            //        break;
            //    case BgTeamsFilter.All_Netral:
            //        filter = (m) =>
            //        {
            //            if (m.HomeOdd >= (decimal)1 && m.HomeOdd <= (decimal)1.79 && m.AwayOdd >= (decimal)1 && m.AwayOdd <= (decimal)1.79)
            //                return true;
            //            if (m.HomeOdd >= (decimal)1.8 && m.AwayOdd >= (decimal)1.8)
            //                return true;
            //            return false;
            //        };
            //        break;
            //    case BgTeamsFilter.All_Underdog:
            //        filter = (m) =>
            //        {
            //            if (m.HomeId == teamId && m.AwayOdd >= (decimal)1 && m.AwayOdd <= (decimal)1.79 && m.HomeOdd >= (decimal)1.8)
            //                return true;
            //            if (m.AwayId == teamId && m.HomeOdd >= (decimal)1 && m.HomeOdd <= (decimal)1.79 && m.AwayOdd >= (decimal)1.8)
            //                return true;
            //            return false;
            //        };
            //        break;
            //    case BgTeamsFilter.Home_All:
            //        filter = (m) =>
            //        {
            //            if (m.HomeId == teamId)
            //                return true;
            //            return false;
            //        };
            //        break;
            //    case BgTeamsFilter.Away_All:
            //        filter = (m) =>
            //        {
            //            if (m.AwayId == teamId)
            //                return true;
            //            return false;
            //        };
            //        break;
            //}
            return filter;
        }
    }
}
