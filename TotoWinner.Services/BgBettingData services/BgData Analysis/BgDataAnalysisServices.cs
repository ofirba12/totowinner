﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public partial class BgDataAnalysisServices : Singleton<BgDataAnalysisServices>, IBgDataServices
    {
        private BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        public IReadOnlyList<string> SoccerSpecialDrawStatuses;
        public IReadOnlyDictionary<BgSport, List<string>> StatisticsStatusesFilter;
        private BgDataAnalysisServices()
        {
            var specialSoccerDrawStatuses = new List<string>() { "pen.", "after over", "aet" };
            SoccerSpecialDrawStatuses = specialSoccerDrawStatuses.AsReadOnly();
            var statisticsStatusesFilterDictionary = new Dictionary<BgSport, List<string>>();
            statisticsStatusesFilterDictionary.Add(BgSport.Soccer, new List<string>() { "postp.", "cancl.", "aban.", "awarded" });
            statisticsStatusesFilterDictionary.Add(BgSport.Basketball, new List<string>() { "abandoned", "not started", "postponed", "awarded" });
            StatisticsStatusesFilter = new ReadOnlyDictionary<BgSport, List<string>>(statisticsStatusesFilterDictionary);
        }
        #region GetTeamMatchesForAnalysis
        public List<BgMatchForAnalysis> GetTeamMatchesForAnalysis(int? bgTeamId)
        {
            if (!bgTeamId.HasValue)
                return null;
            var mapper = BgMapperServices.Instance.GetBgMapper();
            var pickMaxDate = DateTime.Today.Date.AddDays(-1);
            Func<BgSport, string, bool> notfilterFunc = (sport, status) =>
            {
                if (sport == BgSport.Soccer)
                    return status.ToLower() != "postp." && status.ToLower() != "cancl." && status.ToLower() != "wo";
                if (sport == BgSport.Basketball)
                    return status.ToLower() != "postponed" && status.ToLower() != "cancelled";
                return true;
            };

            var matches = persistance.GetAllTeamMatches(bgTeamId.Value)
                .Where(m => m.GameStart <= pickMaxDate && notfilterFunc((BgSport)m.SportId, m.Status))
                .ToList()
                .ConvertAll<BgMatchForAnalysis>(m => new BgMatchForAnalysis(
                    (BgSourceProvider)m.Source,
                    (BgSport)m.SportId,
                    m.MatchId,
                    m.Status,
                    m.CountryId,
                    BgDataServices.Instance.ExtractBgNameFromBgId((BgSport)m.SportId, BgMapperType.Countries, m.CountryId.Value, mapper),
                    BgDataServices.Instance.ExtractBgNameFromBgId((BgSport)m.SportId, BgMapperType.Leagues, m.LeagueId, mapper),
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart,
                    BgDataServices.Instance.ExtractBgNameFromBgId((BgSport)m.SportId, BgMapperType.Teams, m.HomeId, mapper),
                    m.HomeId,
                    BgDataServices.Instance.ExtractBgNameFromBgId((BgSport)m.SportId, BgMapperType.Teams, m.AwayId, mapper),
                    m.AwayId,
                    m.LastUpdate,
                    (decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd,
                    m.HomeScore, m.AwayScore,
                    m.HomeQ1, m.AwayQ1,
                    m.HomeQ2, m.AwayQ2,
                    m.HomeQ3, m.AwayQ3,
                    m.HomeQ4, m.AwayQ4
                    ));
            return matches;
        }
        #endregion

        #region GetBgTeamAnalysis
        public List<BgTeamAnalysis> GetBgTeamAnalysis(BgSport sport)
        {
            var teamsAnalysis = persistance.GetBgTeamsWatcher(sport)
                ?.ConvertAll<BgTeamAnalysis>(t => new BgTeamAnalysis(
                    t.TeamId,
                    t.TotalShows,
                    t.MissingOdds,
                    t.MissingResult));
            return teamsAnalysis;
        }
        #endregion

        #region GetBgTeamAnalysisMonitor
        public List<BgTeamAnalysisMonitor> GetBgTeamAnalysisMonitor(BgSport sport)
        {
            var mapper = BgMapperServices.Instance.GetBgMapper();
            var teamsAnalysis = persistance.GetBgTeamsWatcher(sport)
                ?.ConvertAll<BgTeamAnalysisMonitor>(t => new BgTeamAnalysisMonitor(
                    t.TeamId,
                    BgDataServices.Instance.ExtractBgNameFromBgId(sport, BgMapperType.Teams, t.TeamId, mapper),
                    t.TotalShows,
                    t.MissingOdds,
                    t.MissingResult,
                    t.LastUpdate));
            return teamsAnalysis;
        }
        #endregion

        #region ApplyFilter
        public List<BgMatchForAnalysis> ApplyFilter(List<BgMatchForAnalysis> teamMatchesAllAll, BgTeamsFilter filter, int? teamId)
        {
            if (!teamId.HasValue)
                return null;
            var filteredCollection = teamMatchesAllAll;
            switch (filter)
            {
                case BgTeamsFilter.All_All:
                case BgTeamsFilter.All_Favorite:
                case BgTeamsFilter.All_Netral:
                case BgTeamsFilter.All_Underdog:
                case BgTeamsFilter.Home_All:
                case BgTeamsFilter.Away_All:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, filter).Invoke(m))
                        .ToList();
                    break;
                case BgTeamsFilter.Home_Favorite:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.Home_All).Invoke(m)
                                && FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.All_Favorite).Invoke(m))
                        .ToList();
                    break;
                case BgTeamsFilter.Home_Netral:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.Home_All).Invoke(m)
                                && FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.All_Netral).Invoke(m))
                        .ToList();
                    break;
                case BgTeamsFilter.Home_Underdog:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.Home_All).Invoke(m)
                                && FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.All_Underdog).Invoke(m))
                        .ToList();
                    break;
                case BgTeamsFilter.Away_Favorite:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.Away_All).Invoke(m)
                                && FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.All_Favorite).Invoke(m))
                        .ToList();
                    break;
                case BgTeamsFilter.Away_Netral:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.Away_All).Invoke(m)
                                && FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.All_Netral).Invoke(m))
                        .ToList();
                    break;
                case BgTeamsFilter.Away_Underdog:
                    filteredCollection = teamMatchesAllAll?
                        .Where(m => FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.Away_All).Invoke(m)
                                && FilterFactory.GetFilter(teamId.Value, BgTeamsFilter.All_Underdog).Invoke(m))
                        .ToList();
                    break;
            }
            return filteredCollection;
        }
        #endregion

        #region CreateMatchIndexes
        public BgMatchIndexes CreateMatchIndexes(BgSport sport, bool isHome, string status, int? homeScore, int? awayScore, decimal? homeOdd, decimal? drawOdd, decimal? awayOdd)
        {
            try
            {
                var inputs = new List<decimal?>()
                    {(decimal?)homeScore, (decimal?)awayScore , homeOdd, drawOdd, awayOdd};
                if (inputs.Any(i => !i.HasValue))
                    throw new Exception("all inputs must have values in order to create indexes");
                var wdl = isHome
                    ? homeScore - awayScore
                    : awayScore - homeScore;
                if (sport == BgSport.Soccer && this.SoccerSpecialDrawStatuses.Contains(status.ToLower()))// (status.ToLower() == "pen." || status.ToLower() == "after over" || status.ToLower() == "aet"))
                    wdl = 0;
                BgWDLIndex WDL;
                decimal OddPower;
                if (wdl == 0)
                {
                    WDL = BgWDLIndex.D;
                    OddPower = isHome
                        ? homeOdd.Value - drawOdd.Value
                        : awayOdd.Value - drawOdd.Value;
                }
                else if (wdl < 0)
                {
                    WDL = BgWDLIndex.L;
                    OddPower = isHome
                        ? -awayOdd.Value
                        : -homeOdd.Value;
                }
                else
                {
                    WDL = BgWDLIndex.W;
                    OddPower = isHome
                        ? homeOdd.Value
                        : awayOdd.Value;
                }

                var totalScores = homeScore + awayScore;
                var overUnder3_5Index = totalScores > (decimal)3.5
                    ? BgOverUnderIndex.Over
                    : BgOverUnderIndex.Under;
                var overUnder2_5Index = totalScores > (decimal)2.5
                    ? BgOverUnderIndex.Over
                    : BgOverUnderIndex.Under;
                var overUnder1_5Index = totalScores > (decimal)1.5
                    ? BgOverUnderIndex.Over
                    : BgOverUnderIndex.Under;
                var rangeIndex = BgRangeIndex.Range_4Plus;
                if (totalScores >= 0 && totalScores <= (decimal)1)
                    rangeIndex = BgRangeIndex.Range_0_1;
                else if (totalScores >= 2 && totalScores <= (decimal)3)
                    rangeIndex = BgRangeIndex.Range_2_3;

                var bothScoreIndex = homeScore > 0 && awayScore > 0;
                var drawWinIndex = WDL != BgWDLIndex.L;
                var noDrawIndex = WDL != BgWDLIndex.D;
                var loseDrawIndex = WDL != BgWDLIndex.W;

                var indexes = new BgMatchIndexes(WDL,
                    OddPower,
                    overUnder1_5Index,
                    overUnder2_5Index,
                    overUnder3_5Index,
                    rangeIndex,
                    bothScoreIndex,
                    drawWinIndex,
                    noDrawIndex,
                    loseDrawIndex);
                return indexes;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to create match indexes homeScore={homeScore}, awayScore={awayScore}, homeOdd={homeOdd}, drawOdd={drawOdd}, awayOdd={awayOdd}", ex);
            }
        }
        #endregion

        #region PrepareTeamsRepository
        public BgTeamsAnalysisRepository PrepareTeamsRepository(int? teamAId, int? teamBId)
        {
            var analysisServices = BgDataAnalysisServices.Instance;
            var teamAmatchesAllAll = analysisServices.GetTeamMatchesForAnalysis(teamAId)
                        .OrderByDescending(m => m.GameStart)
                        .ToList();
            var teamBmatchesAllAll = analysisServices.GetTeamMatchesForAnalysis(teamBId)?
                        .OrderByDescending(m => m.GameStart)
                        .ToList();
            var repo = PrepareTeamsRepository(teamAId, teamBId, teamAmatchesAllAll, teamBmatchesAllAll);
            return repo;
        }
        public BgTeamsAnalysisRepository PrepareTeamsRepository(int? teamAId, int? teamBId,
            List<BgMatchForAnalysis> teamAmatchesAllAll,
            List<BgMatchForAnalysis> teamBmatchesAllAll)
        {
            var analysisServices = BgDataAnalysisServices.Instance;
            List<BgMatchForAnalysis> h2hMatches = null;
            if (teamAmatchesAllAll != null && teamBmatchesAllAll != null && teamAmatchesAllAll.Count > 0 && teamBmatchesAllAll.Count > 0)
            {
                h2hMatches = teamAmatchesAllAll.Where(m => (m.HomeId == teamAId.Value && m.AwayId == teamBId.Value)
                || (m.HomeId == teamBId.Value && m.AwayId == teamAId.Value))
                    .OrderByDescending(m => m.GameStart)
                    .ToList();
            }

            Func<int?, int?, decimal?, decimal?, decimal?, string, BgSport, bool> isValid = (homeScore, awayScore, homeOdd, drawOdd, awayOdd, status, sport) =>
            {
                var inputs = new List<decimal?>()
                    {(decimal?)homeScore, (decimal?)awayScore , homeOdd, drawOdd, awayOdd};
                return !StatisticsStatusesFilter[sport].Contains(status.ToLower()) && !(inputs.Any(i => !i.HasValue));
            };

            var teamAmatchesFiltered = new Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>>();
            var teamAmatchesFilteredWithIndexes = new Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>>();
            var teamBmatchesFiltered = new Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>>();
            var teamBmatchesFilteredWithIndexes = new Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>>();
            var h2hFiltered = new Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>>();
            var h2hFilteredWithIndexes = new Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>>();
            foreach (var filter in Enum.GetValues(typeof(BgTeamsFilter)))
            {
                teamAmatchesFiltered.Add((BgTeamsFilter)filter, analysisServices.ApplyFilter(teamAmatchesAllAll, (BgTeamsFilter)filter, teamAId)
                        .OrderByDescending(m => m.GameStart)
                        .ToList()
                        );
                var indexesTeamA = teamAmatchesFiltered[(BgTeamsFilter)filter]
                    .Where(m => isValid(m.HomeScore, m.AwayScore, m.HomeOdd, m.DrawOdd, m.AwayOdd, m.Status, m.Sport))
                    .ToList()
                    .ConvertAll<BgMatchWithIndexes>(f => new BgMatchWithIndexes(f,
                         analysisServices.CreateMatchIndexes(f.Sport, f.HomeId == teamAId.Value, f.Status, f.HomeScore, f.AwayScore, f.HomeOdd, f.DrawOdd, f.AwayOdd)));
                teamAmatchesFilteredWithIndexes.Add((BgTeamsFilter)filter, indexesTeamA);

                teamBmatchesFiltered.Add((BgTeamsFilter)filter, analysisServices.ApplyFilter(teamBmatchesAllAll, (BgTeamsFilter)filter, teamBId)?.OrderByDescending(m => m.GameStart)
                        .ToList()
                        );
                var indexesTeamB = teamBmatchesFiltered[(BgTeamsFilter)filter]?
                    .Where(m => isValid(m.HomeScore, m.AwayScore, m.HomeOdd, m.DrawOdd, m.AwayOdd, m.Status, m.Sport))
                    .ToList()
                    .ConvertAll<BgMatchWithIndexes>(f => new BgMatchWithIndexes(f,
                         analysisServices.CreateMatchIndexes(f.Sport, f.HomeId == teamBId.Value, f.Status, f.HomeScore, f.AwayScore, f.HomeOdd, f.DrawOdd, f.AwayOdd)));
                teamBmatchesFilteredWithIndexes.Add((BgTeamsFilter)filter, indexesTeamB);

                h2hFiltered.Add((BgTeamsFilter)filter, analysisServices.ApplyFilter(h2hMatches, (BgTeamsFilter)filter, teamAId)?
                        .OrderByDescending(m => m.GameStart)
                        .ToList()
                        );
                var indexesH2h = h2hFiltered[(BgTeamsFilter)filter]?
                    .Where(m => isValid(m.HomeScore, m.AwayScore, m.HomeOdd, m.DrawOdd, m.AwayOdd, m.Status, m.Sport))
                    .ToList()
                    .ConvertAll<BgMatchWithIndexes>(f => new BgMatchWithIndexes(f,
                         analysisServices.CreateMatchIndexes(f.Sport, f.HomeId == teamAId.Value, f.Status, f.HomeScore, f.AwayScore, f.HomeOdd, f.DrawOdd, f.AwayOdd)));
                h2hFilteredWithIndexes.Add((BgTeamsFilter)filter, indexesH2h);

            }
            var repo = new BgTeamsAnalysisRepository(teamAmatchesAllAll,
                teamBmatchesAllAll,
                h2hMatches,
                teamAmatchesFiltered,
                teamAmatchesFilteredWithIndexes,
                teamBmatchesFiltered,
                teamBmatchesFilteredWithIndexes,
                h2hFiltered,
                h2hFilteredWithIndexes);
            return repo;
        }
        #endregion

        #region PrepareTeamsStatisitcsRepository
        private ITeamStatistics GetTeamStatistics(BgBetType bgBetType, IEnumerable<BgMatchWithIndexes> indexes)
        {
            ITeamStatistics result = null;
            switch (bgBetType)
            {
                case BgBetType.FullTime:
                    result = indexes.Count() > 0
                        ? new BgTeamFullTimeStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.WDL == BgWDLIndex.W).Count(),
                            indexes.Where(m => m.Indexes.WDL == BgWDLIndex.D).Count(),
                            indexes.Where(m => m.Indexes.WDL == BgWDLIndex.L).Count(),
                            indexes.Sum(m => m.Indexes.OddPower))
                        : null;
                    break;
                case BgBetType.OverUnder3_5:
                    result = indexes.Count() > 0
                        ? new BgTeamOverUnderStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.OverUnder3_5Index == BgOverUnderIndex.Over).Count(),
                            indexes.Where(m => m.Indexes.OverUnder3_5Index == BgOverUnderIndex.Under).Count()
                            )
                        : null;
                    break;
                case BgBetType.OverUnder2_5:
                    result = indexes.Count() > 0
                        ? new BgTeamOverUnderStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.OverUnder2_5Index == BgOverUnderIndex.Over).Count(),
                            indexes.Where(m => m.Indexes.OverUnder2_5Index == BgOverUnderIndex.Under).Count()
                            )
                        : null;
                    break;
                case BgBetType.OverUnder1_5:
                    result = indexes.Count() > 0
                        ? new BgTeamOverUnderStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.OverUnder1_5Index == BgOverUnderIndex.Over).Count(),
                            indexes.Where(m => m.Indexes.OverUnder1_5Index == BgOverUnderIndex.Under).Count()
                            )
                        : null;
                    break;
                case BgBetType.Range:
                    result = indexes.Count() > 0
                        ? new BgTeamRangeStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.RangeIndex == BgRangeIndex.Range_0_1).Count(),
                            indexes.Where(m => m.Indexes.RangeIndex == BgRangeIndex.Range_2_3).Count(),
                            indexes.Where(m => m.Indexes.RangeIndex == BgRangeIndex.Range_4Plus).Count()
                            )
                        : null;
                    break;
                case BgBetType.BothTeamScore:
                    result = indexes.Count() > 0
                        ? new BgTeamYesNoStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.BothScoreIndex).Count(),
                            indexes.Where(m => !m.Indexes.BothScoreIndex).Count()
                            )
                        : null;
                    break;
                case BgBetType.DrawWin:
                    result = indexes.Count() > 0
                        ? new BgTeamYesNoStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.DrawWinIndex).Count(),
                            indexes.Where(m => !m.Indexes.DrawWinIndex).Count()
                            )
                        : null;
                    break;
                case BgBetType.NoDraw:
                    result = indexes.Count() > 0
                        ? new BgTeamYesNoStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.NoDrawIndex).Count(),
                            indexes.Where(m => !m.Indexes.NoDrawIndex).Count()
                            )
                        : null;
                    break;
                case BgBetType.DrawLose:
                    result = indexes.Count() > 0
                        ? new BgTeamYesNoStatistics(indexes.Count(),
                            indexes.Where(m => m.Indexes.LoseDrawIndex).Count(),
                            indexes.Where(m => !m.Indexes.LoseDrawIndex).Count()
                            )
                        : null;
                    break;
            }
            return result;
        }
        public BgTeamsStatisticsRepository PrepareTeamsStatisitcsRepository(BgTeamsAnalysisRepository teamRepository, int maxRows)
        {
            var TeamA = new Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>>();
            var TeamB = new Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>>();
            var H2H = new Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>>();
            foreach (BgBetType betType in Enum.GetValues(typeof(BgBetType)))
            {
                foreach (var filter in teamRepository.TeamAmatchesFilteredWithIndexes.Keys)
                {
                    var selectedRows = teamRepository.TeamAmatchesFilteredWithIndexes[filter].Take(maxRows);
                    var statisticsA = GetTeamStatistics(betType, selectedRows);
                    if (!TeamA.ContainsKey(betType))
                        TeamA.Add(betType, new Dictionary<BgTeamsFilter, ITeamStatistics>());
                    TeamA[betType].Add(filter, statisticsA);

                    selectedRows = teamRepository.TeamBmatchesFilteredWithIndexes[filter].Take(maxRows);
                    var statisticsB = GetTeamStatistics(betType, selectedRows);
                    if (!TeamB.ContainsKey(betType))
                        TeamB.Add(betType, new Dictionary<BgTeamsFilter, ITeamStatistics>());
                    TeamB[betType].Add(filter, statisticsB);

                    selectedRows = teamRepository.H2hFilteredWithIndexes[filter].Take(maxRows);
                    var statisticsH2H = GetTeamStatistics(betType, selectedRows);
                    if (!H2H.ContainsKey(betType))
                        H2H.Add(betType, new Dictionary<BgTeamsFilter, ITeamStatistics>());
                    H2H[betType].Add(filter, statisticsH2H);
                }
            }
            var repo = new BgTeamsStatisticsRepository(TeamA,
                TeamB,
                H2H);

            return repo;
        }
        #endregion

        #region PrepareTeamsBettingDataRepository
        public BgTeamsBettingDataRepository PrepareTeamsBettingDataRepository(BgTeamsAnalysisRepository repo, int maxRows)
        {
            var totalGamesA = repo.TeamAmatchesAllAll.Count();
            var totalGamesB = repo.TeamBmatchesAllAll.Count();
            var totalH2H = repo.H2hMatches != null
                ? repo.H2hMatches.Count()
                : 0;
            var totalRedLineA = totalGamesA - repo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.All_All].Count();
            var totalRedLineB = totalGamesB - repo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.All_All].Count();
            var btRepo = new BgTeamsBettingDataRepository(totalGamesA,
                totalGamesB,
                totalH2H,
                totalRedLineA,
                totalRedLineB);
            return btRepo;
        }
        #endregion

        #region GetTeamClasification
        public BgTeamClasification GetTeamClasification(BgBetTypeSide side, BgMatchOdds match)
        {
            var teamId = side == BgBetTypeSide.Home || side == BgBetTypeSide.H2H
                ? match.HomeId
                : match.AwayId;
            var clasification = BgTeamClasification.Netral;
            if (FilterFactory.IsFilterSatisfied(match, teamId, BgTeamsFilter.All_Favorite))
                clasification = BgTeamClasification.Favorite;
            else if (FilterFactory.IsFilterSatisfied(match, teamId, BgTeamsFilter.All_Netral))
                clasification = BgTeamClasification.Netral;
            else if (FilterFactory.IsFilterSatisfied(match, teamId, BgTeamsFilter.All_Underdog))
                clasification = BgTeamClasification.Underdog;
            else
                throw new Exception($"Could not determine team {teamId} clasification for match [{match.ToString()}]");
            return clasification;
        }

        #endregion

        #region GetFullTimeBetData
        public BgFullTimeBetData GetFullTimeBetData(BgMatchOdds match, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            var homeCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeHomeWinAwayLose, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            var awayCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeHomeWinAwayLose, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var homeAwayTotals = GetBetTotals(BgBetType.FullTime, BgBetTypeComponent.FullTimeHomeWinAwayLose, homeCandidates, awayCandidates);
            var homeWinAwayLose = new BgFullTimeHomeAwayBetData(homeCandidates, awayCandidates, homeAwayTotals);

            var h2hCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeHomeWinAwayLose, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var homeWinH2H = new BgFullTimeH2HBetData(h2hCandidates);

            homeCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeDraw, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeDraw, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            homeAwayTotals = GetBetTotals(BgBetType.FullTime, BgBetTypeComponent.FullTimeDraw, homeCandidates, awayCandidates);
            var homeDrawAwayDraw = new BgFullTimeHomeAwayBetData(homeCandidates, awayCandidates, homeAwayTotals);

            h2hCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeDraw, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var drawH2H = new BgFullTimeH2HBetData(h2hCandidates);

            homeCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeAwayWinHomeLose, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeAwayWinHomeLose, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            homeAwayTotals = GetBetTotals(BgBetType.FullTime, BgBetTypeComponent.FullTimeAwayWinHomeLose, homeCandidates, awayCandidates);
            var awayWinHomeLose = new BgFullTimeHomeAwayBetData(homeCandidates, awayCandidates, homeAwayTotals);

            h2hCandidates = GetBetPopulation(BgBetType.FullTime, BgBetTypeComponent.FullTimeAwayWinHomeLose, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var awayWinH2H = new BgFullTimeH2HBetData(h2hCandidates);

            var fullTimeData = new BgFullTimeBetData(homeWinAwayLose, homeWinH2H, homeDrawAwayDraw, drawH2H, awayWinHomeLose, awayWinH2H);
            return fullTimeData;
        }

        private List<BgBetTotal> GetBetTotals(BgBetType betType, BgBetTypeComponent component, List<ITeamStatistics> homeCandidates, List<ITeamStatistics> awayCandidates)
        {
            var totals = new List<BgBetTotal>();
            switch (component)
            {
                case BgBetTypeComponent.FullTimeHomeWinAwayLose:
                case BgBetTypeComponent.FullTimeDraw:
                case BgBetTypeComponent.FullTimeAwayWinHomeLose:
                    var homeCandidatesFullTime = homeCandidates.ConvertAll<BgTeamFullTimeStatisticsEx>(i => (BgTeamFullTimeStatisticsEx)i);
                    var awayCandidatesFullTime = awayCandidates.ConvertAll<BgTeamFullTimeStatisticsEx>(i => (BgTeamFullTimeStatisticsEx)i);
                    totals = GetBetTotals(betType, component, homeCandidatesFullTime, awayCandidatesFullTime);
                    break;
                case BgBetTypeComponent.Over3_5HomeAway:
                case BgBetTypeComponent.Under3_5HomeAway:
                case BgBetTypeComponent.Over2_5HomeAway:
                case BgBetTypeComponent.Under2_5HomeAway:
                case BgBetTypeComponent.Over1_5HomeAway:
                case BgBetTypeComponent.Under1_5HomeAway:
                    var homeCandidatesOverUnder = homeCandidates.ConvertAll<BgTeamOverUnderStatisticsEx>(i => (BgTeamOverUnderStatisticsEx)i);
                    var awayCandidatesOverUnder = awayCandidates.ConvertAll<BgTeamOverUnderStatisticsEx>(i => (BgTeamOverUnderStatisticsEx)i);
                    totals = GetBetTotals(betType, component, homeCandidatesOverUnder, awayCandidatesOverUnder);
                    break;
                case BgBetTypeComponent.Range0_1HomeAway:
                case BgBetTypeComponent.Range2_3HomeAway:
                case BgBetTypeComponent.Range4PlusHomeAway:
                    var homeCandidatesRange = homeCandidates.ConvertAll<BgTeamRangeStatisticsEx>(i => (BgTeamRangeStatisticsEx)i);
                    var awayCandidatesRange = awayCandidates.ConvertAll<BgTeamRangeStatisticsEx>(i => (BgTeamRangeStatisticsEx)i);
                    totals = GetBetTotals(betType, component, homeCandidatesRange, awayCandidatesRange);
                    break;
                case BgBetTypeComponent.BothScoreYes:
                case BgBetTypeComponent.BothScoreNo:
                case BgBetTypeComponent.WinOrDraw:
                case BgBetTypeComponent.HomeAwayNoDraw:
                case BgBetTypeComponent.DrawOrLose:
                    var homeCandidatesBothScore = homeCandidates.ConvertAll<BgTeamYesNoStatisticsEx>(i => (BgTeamYesNoStatisticsEx)i);
                    var awayCandidatesBothScore = awayCandidates.ConvertAll<BgTeamYesNoStatisticsEx>(i => (BgTeamYesNoStatisticsEx)i);
                    totals = GetBetTotals(betType, component, homeCandidatesBothScore, awayCandidatesBothScore);
                    break;
            }
            return totals;
        }
        private List<BgBetTotal> GetBetTotals(BgBetType betType, BgBetTypeComponent component, List<BgTeamFullTimeStatisticsEx> homeCandidates, List<BgTeamFullTimeStatisticsEx> awayCandidates)
        {
            var totals = new List<BgBetTotal>();
            switch (component)
            {
                case BgBetTypeComponent.FullTimeHomeWinAwayLose:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalIndexPercentW + awayCandidates[index].TotalIndexPercentL) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
                case BgBetTypeComponent.FullTimeDraw:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalIndexPercentD + awayCandidates[index].TotalIndexPercentD) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
                case BgBetTypeComponent.FullTimeAwayWinHomeLose:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalIndexPercentL + awayCandidates[index].TotalIndexPercentW) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
            }
            return totals;

        }
        private List<BgBetTotal> GetBetTotals(BgBetType betType, BgBetTypeComponent component, List<BgTeamOverUnderStatisticsEx> homeCandidates, List<BgTeamOverUnderStatisticsEx> awayCandidates)
        {
            var totals = new List<BgBetTotal>();
            switch (component)
            {
                case BgBetTypeComponent.Over3_5HomeAway:
                case BgBetTypeComponent.Over2_5HomeAway:
                case BgBetTypeComponent.Over1_5HomeAway:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalOverPercent + awayCandidates[index].TotalOverPercent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
                case BgBetTypeComponent.Under3_5HomeAway:
                case BgBetTypeComponent.Under2_5HomeAway:
                case BgBetTypeComponent.Under1_5HomeAway:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalUnderPercent + awayCandidates[index].TotalUnderPercent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
            }
            return totals;
        }
        private List<BgBetTotal> GetBetTotals(BgBetType betType, BgBetTypeComponent component, List<BgTeamRangeStatisticsEx> homeCandidates, List<BgTeamRangeStatisticsEx> awayCandidates)
        {
            var totals = new List<BgBetTotal>();
            switch (component)
            {
                case BgBetTypeComponent.Range0_1HomeAway:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalBetween0and1Percent + awayCandidates[index].TotalBetween0and1Percent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
                case BgBetTypeComponent.Range2_3HomeAway:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].TotalBetween2and3Percent + awayCandidates[index].TotalBetween2and3Percent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
                case BgBetTypeComponent.Range4PlusHomeAway:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].Total4PlusPercent + awayCandidates[index].Total4PlusPercent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
            }
            return totals;
        }
        private List<BgBetTotal> GetBetTotals(BgBetType betType, BgBetTypeComponent component, List<BgTeamYesNoStatisticsEx> homeCandidates, List<BgTeamYesNoStatisticsEx> awayCandidates)
        {
            var totals = new List<BgBetTotal>();
            switch (component)
            {
                case BgBetTypeComponent.BothScoreYes:
                case BgBetTypeComponent.WinOrDraw:
                case BgBetTypeComponent.HomeAwayNoDraw:
                case BgBetTypeComponent.DrawOrLose:

                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].YesPercent + awayCandidates[index].YesPercent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
                case BgBetTypeComponent.BothScoreNo:
                    for (var index = 0; index < homeCandidates.Count; index++)
                    {
                        var avg = Decimal.Round((homeCandidates[index].NoPercent + awayCandidates[index].NoPercent) / 2, 0);
                        totals.Add(new BgBetTotal(homeCandidates[index].TotalRows + awayCandidates[index].TotalRows, avg));
                    }
                    break;
            }
            return totals;
        }
        private List<ITeamStatistics> GetBetPopulation(BgBetType betType,
            BgBetTypeComponent component,
            BgBetTypeSide side,
            BgMatchOdds match,
            BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            var clasification = GetTeamClasification(side, match);
            List<ITeamStatistics> population = null;
            switch (side)
            {
                case BgBetTypeSide.Home:
                    population = GetHomePopulation(betType, bgTeamsStatisitcsRepo, clasification);
                    break;
                case BgBetTypeSide.Away:
                    population = GetAwayPopulation(betType, bgTeamsStatisitcsRepo, clasification);
                    break;
                case BgBetTypeSide.H2H:
                    population = GetH2HPopulation(betType, bgTeamsStatisitcsRepo, clasification);
                    break;
            }
            List<ITeamStatistics> candidates = GetPopulationCandidates(betType, component, side, population);
            return candidates;
        }
        private List<ITeamStatistics> GetPopulationCandidates(BgBetType betType, BgBetTypeComponent component, BgBetTypeSide side, List<ITeamStatistics> population)
        {
            List<ITeamStatistics> candidates = null;
            switch (betType)
            {
                case BgBetType.FullTime:
                    var fullTimePopulation = population.ConvertAll<BgTeamFullTimeStatisticsEx>(i => (BgTeamFullTimeStatisticsEx)i);
                    candidates = GetPopulationCandidates(component, side, fullTimePopulation);
                    break;
                case BgBetType.OverUnder3_5:
                case BgBetType.OverUnder2_5:
                case BgBetType.OverUnder1_5:
                    var overUnderPopulation = population.ConvertAll<BgTeamOverUnderStatisticsEx>(i => (BgTeamOverUnderStatisticsEx)i);
                    candidates = GetPopulationCandidates(component, side, overUnderPopulation);
                    break;
                case BgBetType.Range:
                    var rangePopulation = population.ConvertAll<BgTeamRangeStatisticsEx>(i => (BgTeamRangeStatisticsEx)i);
                    candidates = GetPopulationCandidates(component, side, rangePopulation);
                    break;
                case BgBetType.BothTeamScore:
                case BgBetType.DrawWin:
                case BgBetType.NoDraw:
                case BgBetType.DrawLose:
                    var bothScorePopulation = population.ConvertAll<BgTeamYesNoStatisticsEx>(i => (BgTeamYesNoStatisticsEx)i);
                    candidates = GetPopulationCandidates(component, side, bothScorePopulation);
                    break;
            }
            return candidates;
        }
        private List<ITeamStatistics> GetPopulationCandidates(BgBetTypeComponent component, BgBetTypeSide side, List<BgTeamOverUnderStatisticsEx> population)
        {
            List<BgTeamOverUnderStatisticsEx> candidates = null;
            switch (component)
            {
                case BgBetTypeComponent.Over3_5HomeAway:
                case BgBetTypeComponent.Over2_5HomeAway:
                case BgBetTypeComponent.Over1_5HomeAway:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.TotalOverPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    var splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.TotalOverPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
                case BgBetTypeComponent.Under3_5HomeAway:
                case BgBetTypeComponent.Under2_5HomeAway:
                case BgBetTypeComponent.Under1_5HomeAway:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.TotalUnderPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.TotalUnderPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
            }
            var all = candidates.ConvertAll<ITeamStatistics>(i => (ITeamStatistics)i);
            return all;
        }
        private List<ITeamStatistics> GetPopulationCandidates(BgBetTypeComponent component, BgBetTypeSide side, List<BgTeamFullTimeStatisticsEx> population)
        {
            List<BgTeamFullTimeStatisticsEx> candidates = null;
            switch (component)
            {
                case BgBetTypeComponent.FullTimeHomeWinAwayLose:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => side == BgBetTypeSide.Home || side == BgBetTypeSide.H2H
                                ? i.TotalIndexPercentW
                                : i.TotalIndexPercentL)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    var splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => side == BgBetTypeSide.Home || side == BgBetTypeSide.H2H
                                ? i.TotalIndexPercentW
                                : i.TotalIndexPercentL)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
                case BgBetTypeComponent.FullTimeDraw:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.TotalIndexPercentD)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.TotalIndexPercentD)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
                case BgBetTypeComponent.FullTimeAwayWinHomeLose:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => side == BgBetTypeSide.Home || side == BgBetTypeSide.H2H
                                ? i.TotalIndexPercentL
                                : i.TotalIndexPercentW)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => side == BgBetTypeSide.Home || side == BgBetTypeSide.H2H
                                ? i.TotalIndexPercentL
                                : i.TotalIndexPercentW)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
            }
            var all = candidates.ConvertAll<ITeamStatistics>(i => (ITeamStatistics)i);
            return all;
        }
        private List<ITeamStatistics> GetPopulationCandidates(BgBetTypeComponent component, BgBetTypeSide side, List<BgTeamRangeStatisticsEx> population)
        {
            List<BgTeamRangeStatisticsEx> candidates = null;
            switch (component)
            {
                case BgBetTypeComponent.Range0_1HomeAway:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.TotalBetween0and1Percent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    var splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.TotalBetween0and1Percent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
                case BgBetTypeComponent.Range2_3HomeAway:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.TotalBetween2and3Percent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.TotalBetween2and3Percent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
                case BgBetTypeComponent.Range4PlusHomeAway:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.Total4PlusPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.Total4PlusPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
            }
            var all = candidates.ConvertAll<ITeamStatistics>(i => (ITeamStatistics)i);
            return all;
        }
        private List<ITeamStatistics> GetPopulationCandidates(BgBetTypeComponent component, BgBetTypeSide side, List<BgTeamYesNoStatisticsEx> population)
        {
            List<BgTeamYesNoStatisticsEx> candidates = null;
            switch (component)
            {
                case BgBetTypeComponent.BothScoreYes:
                case BgBetTypeComponent.HomeAwayNoDraw:
                case BgBetTypeComponent.WinOrDraw:
                case BgBetTypeComponent.DrawOrLose:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.YesPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    var splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.YesPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
                case BgBetTypeComponent.BothScoreNo:
                    candidates = population
                        .Where(i => i.TotalRows > 2)
                        .OrderByDescending(i => i.NoPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    splitCandidates = population
                        .Where(i => i.TotalRows < 3)
                        .OrderByDescending(i => i.NoPercent)
                        .ThenByDescending(i => i.TotalRows)
                        .ToList();
                    candidates.AddRange(splitCandidates);
                    break;
            }
            var all = candidates.ConvertAll<ITeamStatistics>(i => (ITeamStatistics)i);
            return all;
        }
        private List<ITeamStatistics> GetHomePopulation(BgBetType betType, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo, BgTeamClasification clasification)
        {
            var homeWinPopulation = new List<ITeamStatistics>();
            SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.All_All], BgTeamsFilter.All_All);
            SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.Home_All], BgTeamsFilter.Home_All);
            switch (clasification)
            {
                case BgTeamClasification.Favorite:
                    SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.All_Favorite], BgTeamsFilter.All_Favorite);
                    SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.Home_Favorite], BgTeamsFilter.Home_Favorite);
                    break;
                case BgTeamClasification.Netral:
                    SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.All_Netral], BgTeamsFilter.All_Netral);
                    SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.Home_Netral], BgTeamsFilter.Home_Netral);
                    break;
                case BgTeamClasification.Underdog:
                    SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.All_Underdog], BgTeamsFilter.All_Underdog);
                    SafeAddPopulation(betType, homeWinPopulation, bgTeamsStatisitcsRepo.TeamA[betType][BgTeamsFilter.Home_Underdog], BgTeamsFilter.Home_Underdog);
                    break;
            }

            return homeWinPopulation;
        }
        private List<ITeamStatistics> GetAwayPopulation(BgBetType betType, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo, BgTeamClasification clasification)
        {
            var awayLosePopulation = new List<ITeamStatistics>();
            SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.All_All], BgTeamsFilter.All_All);
            SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.Away_All], BgTeamsFilter.Away_All);
            switch (clasification)
            {
                case BgTeamClasification.Favorite:
                    SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.All_Favorite], BgTeamsFilter.All_Favorite);
                    SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.Away_Favorite], BgTeamsFilter.Away_Favorite);
                    break;
                case BgTeamClasification.Netral:
                    SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.All_Netral], BgTeamsFilter.All_Netral);
                    SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.Away_Netral], BgTeamsFilter.Away_Netral);
                    break;
                case BgTeamClasification.Underdog:
                    SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.All_Underdog], BgTeamsFilter.All_Underdog);
                    SafeAddPopulation(betType, awayLosePopulation, bgTeamsStatisitcsRepo.TeamB[betType][BgTeamsFilter.Away_Underdog], BgTeamsFilter.Away_Underdog);
                    break;
            }

            return awayLosePopulation;
        }
        private List<ITeamStatistics> GetH2HPopulation(BgBetType betType, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo, BgTeamClasification clasification)
        {
            var h2hPopulation = new List<ITeamStatistics>();
            SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.All_All], BgTeamsFilter.All_All);
            SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.Home_All], BgTeamsFilter.Home_All);
            switch (clasification)
            {
                case BgTeamClasification.Favorite:
                    SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.All_Favorite], BgTeamsFilter.All_Favorite);
                    SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.Home_Favorite], BgTeamsFilter.Home_Favorite);
                    break;
                case BgTeamClasification.Netral:
                    SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.All_Netral], BgTeamsFilter.All_Netral);
                    SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.Home_Netral], BgTeamsFilter.Home_Netral);
                    break;
                case BgTeamClasification.Underdog:
                    SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.All_Underdog], BgTeamsFilter.All_Underdog);
                    SafeAddPopulation(betType, h2hPopulation, bgTeamsStatisitcsRepo.H2H[betType][BgTeamsFilter.Home_Underdog], BgTeamsFilter.Home_Underdog);
                    break;
            }

            return h2hPopulation;
        }
        private void SafeAddPopulation(BgBetType betType, List<ITeamStatistics> population, ITeamStatistics item, BgTeamsFilter filter)
        {
            switch (betType)
            {
                case BgBetType.FullTime:
                    if (item != null)
                        population.Add(new BgTeamFullTimeStatisticsEx(item, filter));
                    else
                        population.Add(new BgTeamFullTimeStatisticsEx(filter));
                    break;
                case BgBetType.OverUnder3_5:
                case BgBetType.OverUnder2_5:
                case BgBetType.OverUnder1_5:
                    if (item != null)
                        population.Add(new BgTeamOverUnderStatisticsEx(item, filter));
                    else
                        population.Add(new BgTeamOverUnderStatisticsEx(filter));
                    break;
                case BgBetType.Range:
                    if (item != null)
                        population.Add(new BgTeamRangeStatisticsEx(item, filter));
                    else
                        population.Add(new BgTeamRangeStatisticsEx(filter));
                    break;
                case BgBetType.BothTeamScore:
                case BgBetType.DrawWin:
                case BgBetType.NoDraw:
                case BgBetType.DrawLose:
                    if (item != null)
                        population.Add(new BgTeamYesNoStatisticsEx(item, filter));
                    else
                        population.Add(new BgTeamYesNoStatisticsEx(filter));
                    break;
            }
        }

        #endregion

        #region GetOverUnderBetData
        public BgOverUnderBetData GetOverUnderBetData(BgBetType betType, BgMatchOdds match, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            BgBetTypeComponent overHomeAwayComponent = BgBetTypeComponent.Over3_5HomeAway;
            BgBetTypeComponent underHomeAwayComponent = BgBetTypeComponent.Under3_5HomeAway;
            switch (betType)
            {
                case BgBetType.OverUnder3_5:
                    overHomeAwayComponent = BgBetTypeComponent.Over3_5HomeAway;
                    underHomeAwayComponent = BgBetTypeComponent.Under3_5HomeAway;
                    break;
                case BgBetType.OverUnder2_5:
                    overHomeAwayComponent = BgBetTypeComponent.Over2_5HomeAway;
                    underHomeAwayComponent = BgBetTypeComponent.Under2_5HomeAway;
                    break;
                case BgBetType.OverUnder1_5:
                    overHomeAwayComponent = BgBetTypeComponent.Over1_5HomeAway;
                    underHomeAwayComponent = BgBetTypeComponent.Under2_5HomeAway;
                    break;
            }
            var homeCandidates = GetBetPopulation(betType, overHomeAwayComponent, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            var awayCandidates = GetBetPopulation(betType, overHomeAwayComponent, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var overTotals = GetBetTotals(betType, overHomeAwayComponent, homeCandidates, awayCandidates);
            var overBetData = new BgOverUnderHomeAwayBetData(homeCandidates, awayCandidates, overTotals);

            homeCandidates = GetBetPopulation(betType, underHomeAwayComponent, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(betType, underHomeAwayComponent, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var underTotals = GetBetTotals(betType, underHomeAwayComponent, homeCandidates, awayCandidates);
            var underBetData = new BgOverUnderHomeAwayBetData(homeCandidates, awayCandidates, underTotals);

            var h2hCandidates = GetBetPopulation(betType, overHomeAwayComponent, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var overH2H = new BgOverUnderH2HBetData(h2hCandidates);

            h2hCandidates = GetBetPopulation(betType, underHomeAwayComponent, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var underH2H = new BgOverUnderH2HBetData(h2hCandidates);

            var overUnderBetData = new BgOverUnderBetData(overBetData, underBetData, overH2H, underH2H);
            return overUnderBetData;
        }
        #endregion

        #region GetRangeBetData
        public BgRangeBetData GetRangeBetData(BgMatchOdds match, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            var homeCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range0_1HomeAway, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            var awayCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range0_1HomeAway, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var rangeTotals = GetBetTotals(BgBetType.Range, BgBetTypeComponent.Range0_1HomeAway, homeCandidates, awayCandidates);
            var range01BetData = new BgRangeHomeAwayBetData(homeCandidates, awayCandidates, rangeTotals);

            homeCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range2_3HomeAway, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range2_3HomeAway, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            rangeTotals = GetBetTotals(BgBetType.Range, BgBetTypeComponent.Range2_3HomeAway, homeCandidates, awayCandidates);
            var range23BetData = new BgRangeHomeAwayBetData(homeCandidates, awayCandidates, rangeTotals);

            homeCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range4PlusHomeAway, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range4PlusHomeAway, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            rangeTotals = GetBetTotals(BgBetType.Range, BgBetTypeComponent.Range4PlusHomeAway, homeCandidates, awayCandidates);
            var range4PlusBetData = new BgRangeHomeAwayBetData(homeCandidates, awayCandidates, rangeTotals);

            var h2hCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range0_1HomeAway, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var range01H2H = new BgRangeH2HBetData(h2hCandidates);

            h2hCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range2_3HomeAway, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var range23H2H = new BgRangeH2HBetData(h2hCandidates);

            h2hCandidates = GetBetPopulation(BgBetType.Range, BgBetTypeComponent.Range4PlusHomeAway, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var range4PlusH2H = new BgRangeH2HBetData(h2hCandidates);

            var rangeBetData = new BgRangeBetData(range01BetData, range23BetData, range4PlusBetData, range01H2H, range23H2H, range4PlusH2H);
            return rangeBetData;
        }
        #endregion

        #region GetBothScoreBetData
        public BgBothScoreBetData GetBothScoreBetData(BgMatchOdds match, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            var homeCandidates = GetBetPopulation(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreYes, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            var awayCandidates = GetBetPopulation(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreYes, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var bothYesTotals = GetBetTotals(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreYes, homeCandidates, awayCandidates);
            var bothYesBetData = new BgYesNoHomeAwayBetData(homeCandidates, awayCandidates, bothYesTotals);

            homeCandidates = GetBetPopulation(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreNo, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreNo, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var bothNoTotals = GetBetTotals(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreNo, homeCandidates, awayCandidates);
            var bothNoBetData = new BgYesNoHomeAwayBetData(homeCandidates, awayCandidates, bothNoTotals);

            var h2hCandidates = GetBetPopulation(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreYes, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var bothScoreYesH2H = new BgYesNoH2HBetData(h2hCandidates);

            h2hCandidates = GetBetPopulation(BgBetType.BothTeamScore, BgBetTypeComponent.BothScoreNo, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var bothScoreNoH2H = new BgYesNoH2HBetData(h2hCandidates);

            var bothScoreBetData = new BgBothScoreBetData(bothYesBetData, bothNoBetData, bothScoreYesH2H, bothScoreNoH2H);
            return bothScoreBetData;
        }
        #endregion

        #region GetWinDrawLose
        public BgWinDrawLoseBetData GetWinDrawLoseBetData(BgMatchOdds match, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            var homeCandidates = GetBetPopulation(BgBetType.DrawWin, BgBetTypeComponent.WinOrDraw, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            var awayCandidates = GetBetPopulation(BgBetType.DrawLose, BgBetTypeComponent.DrawOrLose, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var winDrawTotals = GetBetTotals(BgBetType.DrawWin, BgBetTypeComponent.WinOrDraw, homeCandidates, awayCandidates);
            var winDrawBetData = new BgYesNoHomeAwayBetData(homeCandidates, awayCandidates, winDrawTotals);

            var h2hCandidates = GetBetPopulation(BgBetType.DrawWin, BgBetTypeComponent.WinOrDraw, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var winDrawBetDataH2H = new BgYesNoH2HBetData(h2hCandidates);

            homeCandidates = GetBetPopulation(BgBetType.NoDraw, BgBetTypeComponent.HomeAwayNoDraw, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.NoDraw, BgBetTypeComponent.HomeAwayNoDraw, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var noDrawTotals = GetBetTotals(BgBetType.NoDraw, BgBetTypeComponent.HomeAwayNoDraw, homeCandidates, awayCandidates);
            var noDrawBetData = new BgYesNoHomeAwayBetData(homeCandidates, awayCandidates, noDrawTotals);

            h2hCandidates = GetBetPopulation(BgBetType.NoDraw, BgBetTypeComponent.HomeAwayNoDraw, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var noDrawBetDataH2H = new BgYesNoH2HBetData(h2hCandidates);

            homeCandidates = GetBetPopulation(BgBetType.DrawLose, BgBetTypeComponent.DrawOrLose, BgBetTypeSide.Home, match, bgTeamsStatisitcsRepo);
            awayCandidates = GetBetPopulation(BgBetType.DrawWin, BgBetTypeComponent.WinOrDraw, BgBetTypeSide.Away, match, bgTeamsStatisitcsRepo);
            var drawLoseTotals = GetBetTotals(BgBetType.DrawLose, BgBetTypeComponent.DrawOrLose, homeCandidates, awayCandidates);
            var drawLoseBetData = new BgYesNoHomeAwayBetData(homeCandidates, awayCandidates, drawLoseTotals);

            h2hCandidates = GetBetPopulation(BgBetType.DrawLose, BgBetTypeComponent.DrawOrLose, BgBetTypeSide.H2H, match, bgTeamsStatisitcsRepo);
            var drawLoseBetDataH2H = new BgYesNoH2HBetData(h2hCandidates);

            var winDrawLoseBetData = new BgWinDrawLoseBetData(winDrawBetData, noDrawBetData, drawLoseBetData, winDrawBetDataH2H, noDrawBetDataH2H, drawLoseBetDataH2H);
            return winDrawLoseBetData;
        }
        #endregion

        #region PrepareAllBetsRepository
        public BgAllBetsRepository PrepareAllBetsRepository(BgMatchOdds match, BgTeamsStatisticsRepository bgTeamsStatisitcsRepo)
        {
            var fullTimeBetData = GetFullTimeBetData(match, bgTeamsStatisitcsRepo);
            var overUnder35BetData = GetOverUnderBetData(BgBetType.OverUnder3_5, match, bgTeamsStatisitcsRepo);
            var overUnder25BetData = GetOverUnderBetData(BgBetType.OverUnder2_5, match, bgTeamsStatisitcsRepo);
            var overUnder15BetData = GetOverUnderBetData(BgBetType.OverUnder1_5, match, bgTeamsStatisitcsRepo);
            var rangeBetData = GetRangeBetData(match, bgTeamsStatisitcsRepo);
            var bothScoreBetData = GetBothScoreBetData(match, bgTeamsStatisitcsRepo);
            var winDrawLoseBetData = GetWinDrawLoseBetData(match, bgTeamsStatisitcsRepo);
            
            var allBets = new BgAllBetsRepository(fullTimeBetData,
                overUnder35BetData,
                overUnder25BetData,
                overUnder15BetData,
                rangeBetData,
                bothScoreBetData,
                winDrawLoseBetData);
            return allBets;
        }
        #endregion

        #region WhyThisBet
        private string GetFilterTypeDescription(string filter)
        {
            var item = filter = !string.IsNullOrEmpty(filter)
                        ? $"{filter}, "
                        : string.Empty;
            return item;
        }
        public List<string> WhyThisBetDescription(BgImpliedStatisticType type, string home, string away, BgFullTimeBetData fullTimeBetData)
        {
            var homeDescription = string.Empty;
            var awayAllDescription = string.Empty;
            var awayDescription = string.Empty;
            var homeAllDescription = string.Empty;
            var h2hDescription = string.Empty;
            switch (type)
            {
                case BgImpliedStatisticType.Web_1:
                    var homeItem = fullTimeBetData.HomeWinAwayLose.HomeCandidates[0];
                    var awayItem = fullTimeBetData.HomeWinAwayLose.AwayCandidates[0];
                    var allAllHome = fullTimeBetData.HomeWinAwayLose.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var allAllAway = fullTimeBetData.HomeWinAwayLose.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var h2h = fullTimeBetData.HomeWinH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    var homeFilterTypeDescription = GetFilterTypeDescription(GenerateHomeStatisticsFilterSentence(homeItem.Filter, home));
                    var awayFilterTypeDescription = GetFilterTypeDescription(GenerateAwayStatisticsFilterSentence(awayItem.Filter, away));
                    //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, Forentina has a win rate of 67%
                    homeDescription = $"Out of the last {homeItem.TotalRows} games played by {home}, {homeFilterTypeDescription}{home} has a win rate of {homeItem.TotalIndexPercentW}%.";
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? $"Out of the last {homeItem.TotalRows} games played by {home}, {home} has a win rate of {allAllHome.TotalIndexPercentW}%."
                        : string.Empty;
                    awayDescription = $"Out of the last {awayItem.TotalRows} games played by {away}, {awayFilterTypeDescription}{away} has a loss rate of {awayItem.TotalIndexPercentL}%.";
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? $"Out of the last {awayItem.TotalRows} games played by {away}, {away} has a loss rate of {allAllAway.TotalIndexPercentL}%."
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.TotalIndexPercentW}% of the matches {home} won."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_X:
                    homeItem = fullTimeBetData.HomeDrawAwayDraw.HomeCandidates[0];
                    awayItem = fullTimeBetData.HomeDrawAwayDraw.AwayCandidates[0];
                    allAllHome = fullTimeBetData.HomeDrawAwayDraw.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = fullTimeBetData.HomeDrawAwayDraw.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = fullTimeBetData.DrawH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GetFilterTypeDescription(GenerateHomeStatisticsFilterSentence(homeItem.Filter, home));
                    awayFilterTypeDescription = GetFilterTypeDescription(GenerateAwayStatisticsFilterSentence(awayItem.Filter, away));
                    homeDescription = $"Out of the last {homeItem.TotalRows} games played by {home}, {homeFilterTypeDescription}{homeItem.TotalIndexPercentD}% of them resulted in a draw.";
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? $"Out of the last {homeItem.TotalRows} games played by {home} {allAllHome.TotalIndexPercentD}% of them resulted in a draw."
                        : string.Empty;
                    awayDescription = $"Out of the last {awayItem.TotalRows} games played by {away}, {awayFilterTypeDescription}{awayItem.TotalIndexPercentD}% of them resulted in a draw.";
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? $"Out of the last {awayItem.TotalRows} games played by {away} {allAllAway.TotalIndexPercentD}% of them resulted in a draw."
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                        ? $"Out of the last {h2h.TotalRows} head-to-head matches between {home} and {away}, {h2h.TotalIndexPercentD}% of them concluded in a draw."
                        : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_2:
                    homeItem = fullTimeBetData.AwayWinHomeLose.HomeCandidates[0];
                    awayItem = fullTimeBetData.AwayWinHomeLose.AwayCandidates[0];
                    allAllHome = fullTimeBetData.AwayWinHomeLose.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = fullTimeBetData.AwayWinHomeLose.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = fullTimeBetData.HomeWinH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GetFilterTypeDescription(GenerateHomeStatisticsFilterSentence(homeItem.Filter, home));
                    awayFilterTypeDescription = GetFilterTypeDescription(GenerateAwayStatisticsFilterSentence(awayItem.Filter, away));
                    homeDescription = $"Out of the last {homeItem.TotalRows} games played by {home}, {homeFilterTypeDescription}{home} has a loss rate of {homeItem.TotalIndexPercentL}%.";
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? $"Out of the last {homeItem.TotalRows} games played by {home}, {home} has a loss rate of {allAllHome.TotalIndexPercentL}%."
                        : string.Empty;
                    awayDescription = $"Out of the last {awayItem.TotalRows} games played by {away}, {awayFilterTypeDescription}{away} has a win rate of {awayItem.TotalIndexPercentW}%.";
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? $"Out of the last {awayItem.TotalRows} games played by {away}, {away} has a win rate of {allAllAway.TotalIndexPercentW}%."
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.TotalIndexPercentW}% of the matches {home} lost."
                            : string.Empty;
                    break;
            };
            var description = new List<string>() { homeDescription, awayDescription, homeAllDescription, awayAllDescription, h2hDescription };
            return description;
        }
        public List<string> WhyThisBetDescription(BgImpliedStatisticType type, string home, string away, BgOverUnderBetData overUnderBetData)
        {
            var homeDescription = string.Empty;
            var awayAllDescription = string.Empty;
            var awayDescription = string.Empty;
            var homeAllDescription = string.Empty;
            var h2hDescription = string.Empty;
            var range = string.Empty;
            switch (type)
            {
                case BgImpliedStatisticType.Web_Under1_5:
                case BgImpliedStatisticType.Web_Over1_5:
                    range = "1.5";
                    break;
                case BgImpliedStatisticType.Web_Under2_5:
                case BgImpliedStatisticType.Web_Over2_5:
                    range = "2.5";
                    break;
                case BgImpliedStatisticType.Web_Under3_5:
                case BgImpliedStatisticType.Web_Over3_5:
                    range = "3.5";
                    break;
            }
            Func<int, string, string, decimal, string, string> builder = (totalRows, team, filterText, percent, rangeTitle) =>
            {
                var filterTextItem = string.IsNullOrEmpty(filterText)
                    ? string.Empty
                    : $"{filterText}, ";

                var sentence = totalRows > 0
                    ? $"Out of the last {totalRows} games played by {team}, {filterTextItem}{percent}% of the matches ended with {rangeTitle} {range} goals."
                    : string.Empty;

                return sentence;
            };
            switch (type)
            {
                case BgImpliedStatisticType.Web_Under1_5:
                case BgImpliedStatisticType.Web_Under2_5:
                case BgImpliedStatisticType.Web_Under3_5:
                    var homeItem = overUnderBetData.HomeUnderAwayUnder.HomeCandidates[0];
                    var awayItem = overUnderBetData.HomeUnderAwayUnder.AwayCandidates[0];
                    var allAllHome = overUnderBetData.HomeUnderAwayUnder.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var allAllAway = overUnderBetData.HomeUnderAwayUnder.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var h2h = overUnderBetData.UnderH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    var homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    var awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches ended with less than 1.5 goals.
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.TotalUnderPercent, "less than");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.TotalUnderPercent, "less than")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.TotalUnderPercent, "less than");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.TotalUnderPercent, "less than")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.TotalUnderPercent}% of the matches ended with less than {range} goals."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_Over1_5:
                case BgImpliedStatisticType.Web_Over2_5:
                case BgImpliedStatisticType.Web_Over3_5:
                    homeItem = overUnderBetData.HomeOverAwayOver.HomeCandidates[0];
                    awayItem = overUnderBetData.HomeOverAwayOver.AwayCandidates[0];
                    allAllHome = overUnderBetData.HomeOverAwayOver.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = overUnderBetData.HomeOverAwayOver.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = overUnderBetData.OverH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67 % of the matches ended with over than 1.5 goals.
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.TotalOverPercent, "over than");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.TotalOverPercent, "over than")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.TotalOverPercent, "over than");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.TotalOverPercent, "over than")
                        : string.Empty;

                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.TotalOverPercent}% of the matches ended with over than {range} goals."
                            : string.Empty;
                    break;
            }
            var description = new List<string>() { homeDescription, awayDescription, homeAllDescription, awayAllDescription, h2hDescription };
            return description;
        }
        //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches ended with range of 0-1 goals
        //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches ended with range of 2-3 goals
        //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches ended with at least 4 goals were scored in each match
        public List<string> WhyThisBetDescription(BgImpliedStatisticType type, string home, string away, BgRangeBetData rangeBetData)
        {
            var homeDescription = string.Empty;
            var awayAllDescription = string.Empty;
            var awayDescription = string.Empty;
            var homeAllDescription = string.Empty;
            var h2hDescription = string.Empty;

            Func<int, string, string, decimal, string, string> builder = (totalRows, team, filterText, percent, rangeTitle) =>
            {
                var filterTextItem = string.IsNullOrEmpty(filterText)
                    ? string.Empty
                    : $"{filterText}, ";

                var sentence = totalRows > 0
                    ? $"Out of the last {totalRows} games played by {team}, {filterTextItem}{percent}% of the matches ended with {rangeTitle}."
                    : string.Empty;

                return sentence;
            };
            switch (type)
            {
                case BgImpliedStatisticType.Web_0_1:
                    var homeItem = rangeBetData.RangeBetween0and1.HomeCandidates[0];
                    var awayItem = rangeBetData.RangeBetween0and1.AwayCandidates[0];
                    var allAllHome = rangeBetData.RangeBetween0and1.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var allAllAway = rangeBetData.RangeBetween0and1.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var h2h = rangeBetData.RangeBetween0and1H2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    var homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    var awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.TotalBetween0and1Percent, "range of 0-1 goals");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.TotalBetween0and1Percent, "range of 0-1 goals")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.TotalBetween0and1Percent, "range of 0-1 goals");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.TotalBetween0and1Percent, "range of 0-1 goals")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.TotalBetween0and1Percent}% of the matches ended with range of 0-1 goals."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_2_3:
                    homeItem = rangeBetData.RangeBetween2and3.HomeCandidates[0];
                    awayItem = rangeBetData.RangeBetween2and3.AwayCandidates[0];
                    allAllHome = rangeBetData.RangeBetween2and3.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = rangeBetData.RangeBetween2and3.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = rangeBetData.RangeBetween2and3H2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.TotalBetween2and3Percent, "range of 2-3 goals");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.TotalBetween2and3Percent, "range of 2-3 goals")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.TotalBetween2and3Percent, "range of 2-3 goals");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.TotalBetween2and3Percent, "range of 2-3 goals")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.TotalBetween2and3Percent}%  of the matches ended with range of 2-3 goals."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_4Plus:
                    homeItem = rangeBetData.Range4Plus.HomeCandidates[0];
                    awayItem = rangeBetData.Range4Plus.AwayCandidates[0];
                    allAllHome = rangeBetData.Range4Plus.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = rangeBetData.Range4Plus.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = rangeBetData.Range4PlusH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.Total4PlusPercent, "at least 4 goals that were scored in each match.");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.Total4PlusPercent, "at least 4 goals were that scored in each match.")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.Total4PlusPercent, "at least 4 goals that were scored in each match.");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.Total4PlusPercent, "at least 4 goals that were scored in each match.")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.Total4PlusPercent}% of the matches ended with at least 4 goals that were scored in each match."
                            : string.Empty;
                    break;
            }
            var description = new List<string>() { homeDescription, awayDescription, homeAllDescription, awayAllDescription, h2hDescription };
            return description;
        }
        //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches both teams scored
        //Out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches at least one team not scoring for each match
        public List<string> WhyThisBetDescription(BgImpliedStatisticType type, string home, string away, BgBothScoreBetData bothScoreBetData)
        {
            var homeDescription = string.Empty;
            var awayAllDescription = string.Empty;
            var awayDescription = string.Empty;
            var homeAllDescription = string.Empty;
            var h2hDescription = string.Empty;

            Func<int, string, string, decimal, string, string> builder = (totalRows, team, filterText, percent, rangeTitle) =>
            {
                var filterTextItem = string.IsNullOrEmpty(filterText)
                    ? string.Empty
                    : $"{filterText}, ";

                var sentence = totalRows > 0
                    ? $"Out of the last {totalRows} games played by {team}, {filterTextItem}{percent}% of the matches {rangeTitle}."
                    : string.Empty;

                return sentence;
            };
            switch (type)
            {
                case BgImpliedStatisticType.Web_BothScore:
                    var homeItem = bothScoreBetData.BothScoreYes.HomeCandidates[0];
                    var awayItem = bothScoreBetData.BothScoreYes.AwayCandidates[0];
                    var allAllHome = bothScoreBetData.BothScoreYes.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var allAllAway = bothScoreBetData.BothScoreYes.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var h2h = bothScoreBetData.BothScoreYesH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    var homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    var awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.YesPercent, "both teams scored");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.YesPercent, "both teams scored")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.YesPercent, "both teams scored");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.YesPercent, "both teams scored")
                        : string.Empty;
                    h2hDescription = bothScoreBetData.BothScoreYesH2H.H2HCandidates[0].TotalRows > 0
                            ? $"Out of the last {bothScoreBetData.BothScoreYesH2H.H2HCandidates[0].TotalRows} games played by {home} with head-to-head matches against {away}, {bothScoreBetData.BothScoreYesH2H.H2HCandidates[0].YesPercent}% of the matches both teams scored."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_BothNoScore:
                    homeItem = bothScoreBetData.BothScoreNo.HomeCandidates[0];
                    awayItem = bothScoreBetData.BothScoreNo.AwayCandidates[0];
                    allAllHome = bothScoreBetData.BothScoreNo.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = bothScoreBetData.BothScoreNo.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = bothScoreBetData.BothScoreNoH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.NoPercent, "at least one team not scoring for each match");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.NoPercent, "at least one team not scoring for each match")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.NoPercent, "at least one team not scoring for each match");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.NoPercent, "at least one team not scoring for each match")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.NoPercent}%  of the matches at least one team not scoring for each match."
                            : string.Empty;
                    break;
            }
            var description = new List<string>() { homeDescription, awayDescription, homeAllDescription, awayAllDescription, h2hDescription };
            return description;
        }

        //out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches  Fiorentina either won or ended in a draw.
        //ut of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches  Fiorentina either lose  or ended in a draw.
        //out of the last 6 games played by Fiorentina, when there is no favorite team to win, 67% of the matches did not end in a draw.
        public List<string> WhyThisBetDescription(BgImpliedStatisticType type, string home, string away, BgWinDrawLoseBetData winDrawLoseBetData)
        {
            var homeDescription = string.Empty;
            var awayAllDescription = string.Empty;
            var awayDescription = string.Empty;
            var homeAllDescription = string.Empty;
            var h2hDescription = string.Empty;

            Func<int, string, string, decimal, string, string> builder = (totalRows, team, filterText, percent, rangeTitle) =>
            {
                var filterTextItem = string.IsNullOrEmpty(filterText)
                    ? string.Empty
                    : $"{filterText}, ";

                var sentence = totalRows > 0
                    ? $"Out of the last {totalRows} games played by {team}, {filterTextItem}{percent}% of the matches {rangeTitle}."
                    : string.Empty;

                return sentence;
            };
            switch (type)
            {
                case BgImpliedStatisticType.Web_1X:
                    var homeItem = winDrawLoseBetData.WinDraw.HomeCandidates[0];
                    var awayItem = winDrawLoseBetData.WinDraw.AwayCandidates[0];
                    var allAllHome = winDrawLoseBetData.WinDraw.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var allAllAway = winDrawLoseBetData.WinDraw.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    var h2h = winDrawLoseBetData.WinDrawH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    var homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    var awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.YesPercent, $"{home} either won or ended in a draw");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.YesPercent, $"{home} either won or ended in a draw")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.YesPercent, $"{away} either lose or ended in a draw");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.YesPercent, $"{away} either lose or ended in a draw")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.YesPercent}% of the matches either won or ended in a draw."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_12:
                    homeItem = winDrawLoseBetData.NoDraw.HomeCandidates[0];
                    awayItem = winDrawLoseBetData.NoDraw.AwayCandidates[0];
                    allAllHome = winDrawLoseBetData.NoDraw.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = winDrawLoseBetData.NoDraw.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = winDrawLoseBetData.NoDrawH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.YesPercent, $"did not end in a draw");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.YesPercent, $"did not end in a draw")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.YesPercent, $"did not end in a draw");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.YesPercent, $"did not end in a draw")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.YesPercent}% of the matches did not end in a draw."
                            : string.Empty;
                    break;
                case BgImpliedStatisticType.Web_2X:
                    homeItem = winDrawLoseBetData.DrawLose.HomeCandidates[0];
                    awayItem = winDrawLoseBetData.DrawLose.AwayCandidates[0];
                    allAllHome = winDrawLoseBetData.DrawLose.HomeCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    allAllAway = winDrawLoseBetData.DrawLose.AwayCandidates.First(a => a.Filter == BgTeamsFilter.All_All);
                    h2h = winDrawLoseBetData.DrawLoseH2H.H2HCandidates.First(a => a.Filter == BgTeamsFilter.All_All);

                    homeFilterTypeDescription = GenerateHomeStatisticsFilterSentence(homeItem.Filter, home);
                    awayFilterTypeDescription = GenerateAwayStatisticsFilterSentence(awayItem.Filter, away);
                    homeDescription = builder(homeItem.TotalRows, home, homeFilterTypeDescription, homeItem.YesPercent, $"{home} either lose or ended in a draw");
                    homeAllDescription = homeItem.Filter != BgTeamsFilter.All_All
                        ? builder(homeItem.TotalRows, home, string.Empty, allAllHome.YesPercent, $"{home} either lose or ended in a draw")
                        : string.Empty;
                    awayDescription = builder(awayItem.TotalRows, away, awayFilterTypeDescription, awayItem.YesPercent, $"{away} either won or ended in a draw");
                    awayAllDescription = awayItem.Filter != BgTeamsFilter.All_All
                        ? builder(awayItem.TotalRows, away, string.Empty, allAllAway.YesPercent, $"{away} either won or ended in a draw")
                        : string.Empty;
                    h2hDescription = h2h.TotalRows > 0
                            ? $"Out of the last {h2h.TotalRows} games played by {home} with head-to-head matches against {away}, {h2h.YesPercent}% of the matches either lose or ended in a draw."
                            : string.Empty;
                    break;
            }
            var description = new List<string>() { homeDescription, awayDescription, homeAllDescription, awayAllDescription, h2hDescription };
            return description;
        }
        private string GenerateHomeStatisticsFilterSentence(BgTeamsFilter filter, string home)
        {
            var homeFilterTypeDescription = string.Empty;
            if (filter == BgTeamsFilter.All_Favorite)
                homeFilterTypeDescription = $"when they are favored to win";
            else if (filter == BgTeamsFilter.All_Netral)
                homeFilterTypeDescription = $"when there is no favorite team to win";
            else if (filter == BgTeamsFilter.All_Underdog)
                homeFilterTypeDescription = $"When {home} is no favorite to win";
            else if (filter == BgTeamsFilter.Home_All)
                homeFilterTypeDescription = $"when {home} played at home";
            else if (filter == BgTeamsFilter.Home_Favorite)
                homeFilterTypeDescription = $"when {home} played at home and was favorite to win";
            else if (filter == BgTeamsFilter.Home_Netral)
                homeFilterTypeDescription = $"when {home} played at home and there was no favorite team to win";
            else if (filter == BgTeamsFilter.Home_Underdog)
                homeFilterTypeDescription = $"when {home} played at home and was not favorite to win";
            return homeFilterTypeDescription;
        }
        private string GenerateAwayStatisticsFilterSentence(BgTeamsFilter filter, string away)
        {
            var awayFilterTypeDescription = string.Empty;
            if (filter == BgTeamsFilter.All_Favorite)
                awayFilterTypeDescription = $"when they are favored to win";
            else if (filter == BgTeamsFilter.All_Netral)
                awayFilterTypeDescription = $"when there is no favorite team to win";
            else if (filter == BgTeamsFilter.All_Underdog)
                awayFilterTypeDescription = $"when {away} is no favorite to win";
            else if (filter == BgTeamsFilter.Away_All)
                awayFilterTypeDescription = $"when {away} played away";
            else if (filter == BgTeamsFilter.Away_Favorite)
                awayFilterTypeDescription = $"when {away} played away and was favorite to win";
            else if (filter == BgTeamsFilter.Away_Netral)
                awayFilterTypeDescription = $"when {away} played away and there was no favorite team to win";
            else if (filter == BgTeamsFilter.Away_Underdog)
                awayFilterTypeDescription = $"when {away} played away and was not favorite to win";
            return awayFilterTypeDescription;
        }

        #endregion
    }
}