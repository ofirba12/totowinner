﻿using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Services
{
    public class WinnerDataServices : Singleton<WinnerDataServices>, IWinnerDataServices
    {
        BgDataPersistanceServices bgPersistance = BgDataPersistanceServices.Instance;
        BgDataWinnerPersistanceServices winPersistance = BgDataWinnerPersistanceServices.Instance;
        private WinnerDataServices() { }
        public SportIds BgSportToWinnerSport(BgSport sport)
        {
            var winSport = SportIds.Soccer;
            if (sport == BgSport.Basketball)
                winSport = SportIds.Basketball;
            if (sport == BgSport.Tennis)
                winSport = SportIds.Tennis;
            if (sport == BgSport.Handball)
                winSport = SportIds.Handball;
            if (sport == BgSport.Baseball)
                winSport = SportIds.Baseball;
            if (sport == BgSport.Football)
                winSport = SportIds.Football;
            return winSport;
        }
        public GoalServeSportType BgSportToGoalserveSport(BgSport sport)
        {
            var glSport = GoalServeSportType.Soccer;
            if (sport == BgSport.Basketball)
                glSport = GoalServeSportType.Basketball;
            if (sport == BgSport.Tennis)
                glSport = GoalServeSportType.Tennis;
            if (sport == BgSport.Handball)
                glSport = GoalServeSportType.Handball;
            if (sport == BgSport.Baseball)
                glSport = GoalServeSportType.Baseball;
            if (sport == BgSport.Football)
                glSport = GoalServeSportType.Football;
            return glSport;
        }
        public BgWinnerMapperRepository GetWinnerMapper()
        {
            var rawMap = winPersistance.GetWinnerMapper();
            var dic = rawMap.ToDictionary(m => new WinnerMapperKey(m.Sport, m.WinnerId), m => m);
            var repo = new BgWinnerMapperRepository(rawMap, dic);
            return repo;
        }
        public List<BgWinnerFeed> GetWinnerDataFathersOnly(BgSport bgSport, DateTime fetchDate, BgWinnerMapperRepository mapper)
        {
            var data = GetWinnerData(bgSport, fetchDate, mapper)
                        .Where(m => m.SportId == SportIds.Soccer
                                 ? m.BetTypeId == BetTypes.BetSoccer1x2
                                 : m.BetTypeId == BetTypes.BetBasketballAdvance || m.BetTypeId == BetTypes.BetBasketballAdvanceWithoutDraw);

            return data.ToList();
        }
        public List<BgWinnerFeed> GetWinnerData(BgSport bgSport, DateTime fetchDate, BgWinnerMapperRepository mapper)
        {
            var sport = BgSportToWinnerSport(bgSport);
            var daysFromToday = fetchDate - DateTime.Today;
            var feed = winPersistance.FetchWinnerFeed(sport, daysFromToday.Days)
                .ConvertAll<BgWinnerFeed>(m => new BgWinnerFeed(
                    m.EventId,
                    m.ProgramId,
                    sport,
                    m.TotoId,
                    m.HomeId,
                    mapper.Mapper[new WinnerMapperKey(sport, m.HomeId)].WinnerName,
                    m.GuestId,
                    mapper.Mapper[new WinnerMapperKey(sport, m.GuestId)].WinnerName,
                    (BetTypes)m.BetTypeId,
                    m.Place,
                    m.Name,
                    m.GameStart,
                    m.Status,
                    (decimal?)m.HomeOdd,
                    (decimal?)m.DrawOdd,
                    (decimal?)m.AwayOdd,
                    m.Country,
                    m.League,
                    m.LastUpdate)
                    );
            return feed;
        }

        public List<WinnerBgCombinedFeed> CombineBgAndWinnerFeeds(BgSport bgSport,
            DateTime gameDate,
            SportIds winnerSport,
            BgWinnerMapperRepository winnerMapper,
            IEnumerable<BgWinnerFeed> winnerData,
            Dictionary<BgFeedExactKey, BgMatch> bgMatches,
            Dictionary<long, BgMatchImpliedStatistics> impliedStatistics)
        {
            var combined = new List<WinnerBgCombinedFeed>();
            foreach (var data in winnerData)
            {
                var homeKey = new WinnerMapperKey(winnerSport, data.HomeId);
                var awayKey = new WinnerMapperKey(winnerSport, data.GuestId);
                if (winnerMapper.Mapper.ContainsKey(homeKey) && winnerMapper.Mapper.ContainsKey(awayKey))
                {
                    if (winnerMapper.Mapper[homeKey].BgId.HasValue && winnerMapper.Mapper[awayKey].BgId.HasValue)
                    {
                        var bgKey = new BgFeedExactKey(bgSport,
                            gameDate,
                            winnerMapper.Mapper[homeKey].BgId.Value,
                            winnerMapper.Mapper[awayKey].BgId.Value);
                        if (bgMatches.ContainsKey(bgKey))
                        {
                            var bgMatch = bgMatches[bgKey];
                            var bets = impliedStatistics.ContainsKey(bgMatch.MatchId)
                                ? impliedStatistics[bgMatch.MatchId]
                                : null;
                            combined.Add(new WinnerBgCombinedFeed(data, bgMatch, bets));
                        }
                        else
                        {
                            combined.Add(new WinnerBgCombinedFeed(data, null, null));
                        }
                    }
                }
            }

            return combined;
        }
        public Dictionary<long, WinnerBgCombinedFeed> BgWinnerMatcher(BgWinnerMapperRepository winnerMapper, 
            BgSport sport, 
            DateTime feedDate, 
            Dictionary<BgFeedKey, BgMatch> generatedFeed)
        {
            var winnerSport = WinnerDataServices.Instance.BgSportToWinnerSport(sport);
            var winnerData = WinnerDataServices.Instance.GetWinnerDataFathersOnly(sport, feedDate, winnerMapper);
            var bgMatches = new Dictionary<BgFeedExactKey, BgMatch>();
            foreach (var f in generatedFeed)
            {
                var key = new BgFeedExactKey(sport, feedDate, f.Key.HomeTeamId, f.Key.AwayTeamId);
                if (!bgMatches.ContainsKey(key))
                    bgMatches.Add(key, f.Value);
            }
            var combined = WinnerDataServices.Instance.CombineBgAndWinnerFeeds(sport,
                feedDate,
                winnerSport,
                winnerMapper,
                winnerData,
                bgMatches,
                new Dictionary<long, BgMatchImpliedStatistics>()).Where(m => m.BgMatch != null);
            var bgWinnerLookup = new Dictionary<long, WinnerBgCombinedFeed>();
            //As the same event/match can be in different programs, we need to take only one
            foreach (var c in combined)
            {
                if (!bgWinnerLookup.ContainsKey(c.BgMatch.MatchId))
                    bgWinnerLookup.Add(c.BgMatch.MatchId, c);
            }

            return bgWinnerLookup;
        }

        public BgWinnerFeed FindTeamInWinnerFeed(int winnerTeamId, BgWinnerMapperRepository mapper)
        {
            var game = winPersistance.FetchLastTeamGame(winnerTeamId);
            var sport = (SportIds)game.SportId;
            var result = new BgWinnerFeed(
                    game.EventId,
                    game.ProgramId,
                    sport,
                    game.TotoId,
                    game.HomeId,
                    mapper.Mapper[new WinnerMapperKey(sport, game.HomeId)].WinnerName,
                    game.GuestId,
                    mapper.Mapper[new WinnerMapperKey(sport, game.GuestId)].WinnerName,
                    (BetTypes)game.BetTypeId,
                    game.Place,
                    game.Name,
                    game.GameStart,
                    game.Status,
                    (decimal?)game.HomeOdd,
                    (decimal?)game.DrawOdd,
                    (decimal?)game.AwayOdd,
                    game.Country,
                    game.League,
                    game.LastUpdate
                    );
            return result;
        }
    }
}
