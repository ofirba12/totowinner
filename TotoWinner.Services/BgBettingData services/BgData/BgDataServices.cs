﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;

namespace TotoWinner.Services
{
    public class BgDataServices : Singleton<BgDataServices>, IBgDataServices
    {
        private static readonly ILog _log = LogManager.GetLogger("BgDataGenerator");
        BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;
        private BgDataServices()
        {
        }
        public BgSport GoalserveSportToBgSport(GoalServeSportType sport)
        {
            switch (sport)
            {
                case GoalServeSportType.Soccer:
                    return BgSport.Soccer;
                case GoalServeSportType.Baseball:
                    return BgSport.Baseball;
                case GoalServeSportType.Tennis:
                    return BgSport.Tennis;
                case GoalServeSportType.Basketball:
                    return BgSport.Basketball;
                case GoalServeSportType.Handball:
                    return BgSport.Handball;
                case GoalServeSportType.Football:
                    return BgSport.Football;
                default:
                    throw new Exception($"Goalserve Sport {sport} is not supported in BG");
            }
        }
        public GoalServeSportType BgSportToGoalserveSport(BgSport sport)
        {
            switch (sport)
            {
                case BgSport.Soccer:
                    return GoalServeSportType.Soccer;
                case BgSport.Baseball:
                    return GoalServeSportType.Baseball;
                case BgSport.Tennis:
                    return GoalServeSportType.Tennis;
                case BgSport.Basketball:
                    return GoalServeSportType.Basketball;
                case BgSport.Handball:
                    return GoalServeSportType.Handball;
                case BgSport.Football:
                    return GoalServeSportType.Football;
                default:
                    throw new Exception($"BG Sport {sport} is not supported in Goalserve");
            }

        }

        public FeedPersistanceRepository OverrideFeedWithManual(Dictionary<BgFeedKey, BgMatch> fData,
            List<ManualMatch> manualData)
        {
            var bgFeedRemove = new List<BgMatch>();
            foreach (var item in manualData)
            {
                var bgFeedKey = new BgFeedKey(item.Sport, item.GameStart, item.HomeId.Value, item.AwayId.Value);
                var match = new BgMatch(
                        BgSourceProvider.Manual,
                        item.Sport,
                        item.MatchId.Value,
                        item.Status,
                        item.CountryId,
                        item.Country,
                        item.League,
                        item.LeagueId.Value,
                        item.IsCup,
                        item.GameStart,
                        item.Home,
                        item.HomeId.Value,
                        item.Away,
                        item.AwayId.Value,
                        item.LastUpdate,
                        item.OddsAndScores.HomeOdd, item.OddsAndScores.DrawOdd, item.OddsAndScores.AwayOdd,
                        item.OddsAndScores.HomeScore, item.OddsAndScores.AwayScore,
                        item.OddsAndScores.BasketballQuatersScores?.LocalTeamQ1Score,
                        item.OddsAndScores.BasketballQuatersScores?.AwayTeamQ1Score,
                        item.OddsAndScores.BasketballQuatersScores?.LocalTeamQ2Score,
                        item.OddsAndScores.BasketballQuatersScores?.AwayTeamQ2Score,
                        item.OddsAndScores.BasketballQuatersScores?.LocalTeamQ3Score,
                        item.OddsAndScores.BasketballQuatersScores?.AwayTeamQ3Score,
                        item.OddsAndScores.BasketballQuatersScores?.LocalTeamQ4Score,
                        item.OddsAndScores.BasketballQuatersScores?.AwayTeamQ4Score
                        );
                if (!fData.ContainsKey(bgFeedKey) && !item.Ignore)
                    fData.Add(bgFeedKey, match);
                else if (fData.ContainsKey(bgFeedKey) && !item.Ignore)
                    fData[bgFeedKey] = match;
                else if (fData.ContainsKey(bgFeedKey) && item.Ignore)
                {
                    fData.Remove(bgFeedKey);
                    bgFeedRemove.Add(match);
                }
            }
            var bgFeedAddOrUpdate = fData.Values.ToList();
            var feedPersistanceRepository = new FeedPersistanceRepository(bgFeedAddOrUpdate, bgFeedRemove);
            return feedPersistanceRepository;
        }
        #region Generate
        public List<BgMatch> GenerateData(List<GoalserveMatchFeed> matchesProviderFeed, MapperRespository mapper)
        {
            var data = new List<BgMatch>();
            foreach (var goldserveItem in matchesProviderFeed)
            {
                try
                {
                    var bgCountry = goldserveItem.Sport == GoalServeSportType.Soccer || 
                                    goldserveItem.Sport == GoalServeSportType.Basketball 
                        ? ExtractBgData(goldserveItem, BgMapperItems.Country, mapper)
                        : new ProviderData(-1, GoalserveSportToBgSport(goldserveItem.Sport), BgMapperType.Countries, string.Empty);
                    var bgLeague = ExtractBgData(goldserveItem, BgMapperItems.League, mapper);
                    var bgHomeTeam = ExtractBgData(goldserveItem, BgMapperItems.HomeTeam, mapper);
                    var bgAwayTeam = ExtractBgData(goldserveItem, BgMapperItems.AwayTeam, mapper);
                    var bgMatch = new BgMatch(BgSourceProvider.Goldserve,
                        GoalserveSportToBgSport(goldserveItem.Sport),
                        goldserveItem.MatchId,
                        goldserveItem.Status,
                        bgCountry.ProviderId,
                        bgCountry.Data,
                        bgLeague.Data,
                        bgLeague.ProviderId.Value,
                        goldserveItem.IsCup,
                        goldserveItem.GameStart,
                        bgHomeTeam.Data,
                        bgHomeTeam.ProviderId.Value,
                        bgAwayTeam.Data,
                        bgAwayTeam.ProviderId.Value,
                        goldserveItem.LastUpdate,
                        goldserveItem.HomeOdd, goldserveItem.DrawOdd, goldserveItem.AwayOdd,
                        goldserveItem.HomeScore, goldserveItem.AwayScore,
                        goldserveItem.HQ1, goldserveItem.AQ1,
                        goldserveItem.HQ2, goldserveItem.AQ2,
                        goldserveItem.HQ3, goldserveItem.AQ3,
                        goldserveItem.HQ4, goldserveItem.AQ4
                        );
                    data.Add(bgMatch);
                }
                catch(Exception ex)
                {
                    _log.Error($"An error occured trying to create bgMatch from goalserve item [{goldserveItem.ToString()}]", ex);
                }
            }

            return data;
        }

        private ProviderData ExtractBgData(GoalserveMatchFeed goldserveItem, BgMapperItems mappingType, MapperRespository mapper)
        {
            try
            {
                ProviderData providerData = null;
                switch (mappingType)
                {
                    case BgMapperItems.Country:
                        providerData = new ProviderData(goldserveItem.CountryId, GoalserveSportToBgSport(goldserveItem.Sport), BgMapperType.Countries, goldserveItem.Country);
                        break;
                    case BgMapperItems.League:
                        providerData = new ProviderData(goldserveItem.LeagueId, GoalserveSportToBgSport(goldserveItem.Sport), BgMapperType.Leagues, goldserveItem.League);
                        break;
                    case BgMapperItems.HomeTeam:
                        providerData = new ProviderData(goldserveItem.HomeId, GoalserveSportToBgSport(goldserveItem.Sport), BgMapperType.Teams, goldserveItem.Home);
                        break;
                    case BgMapperItems.AwayTeam:
                        providerData = new ProviderData(goldserveItem.AwayId, GoalserveSportToBgSport(goldserveItem.Sport), BgMapperType.Teams, goldserveItem.Away);
                        break;
                }
                var bgData = mapper.GoldserveDataBgKey[providerData];
                var bgKey = new ProviderKey(bgData.ProviderId, bgData.Sport, bgData.ItemType);
                var bgName = mapper.BgIdItemName[bgKey];
                var bgItem = new ProviderData(bgKey.ProviderId, bgData.Sport, bgData.ItemType, bgName);
                return bgItem;
            }
            catch(Exception ex)
            {
                throw new Exception($"Failed to extract Bg Data for {mappingType} from [{goldserveItem.ToString()}]", ex);
            }
        }
        #endregion
        public string ExtractBgNameFromBgId(BgSport sport, BgMapperType itemType, int bgId, MapperRespository mapper)
        {
            try
            {
                var bgKey = new ProviderKey(bgId, sport, itemType);
                var bgName = itemType == BgMapperType.Countries && bgId == -1
                    ? string.Empty 
                    : mapper.BgIdItemName[bgKey];
                return bgName;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get {itemType} bg id for sport={sport}, bgId={bgId} ", ex);
            }
        }
        #region Handle Duplicates
        public Dictionary<BgFeedKey, BgMatch> MergeDuplicates(List<BgMatch> bgData)
        {
            var fData = new Dictionary<BgFeedKey, BgMatch>();
            foreach (var item in bgData)
            {
                var bgFeedKey = new BgFeedKey(item.Sport, item.GameStart, item.HomeId, item.AwayId);
                if (!fData.ContainsKey(bgFeedKey))
                    fData.Add(bgFeedKey, item);
                else
                {
                    var current = fData[bgFeedKey];
                    var scores = HasScores(current, item);
                    var odds = HasOdds(current, item);

                    if (takeLastUpdate(scores, odds))
                        fData[bgFeedKey] = current.LastUpdate > item.LastUpdate
                            ? current
                            : item;
                    //else if (takeCurrent(scores, odds))
                    //    fData[bgFeedKey] = current;
                    else if (takeItem(scores, odds))
                        fData[bgFeedKey] = item;
                    else if (takeCurrentScoreItemOdds(scores, odds))
                        fData[bgFeedKey].SetOdds(item.HomeOdd.Value, item.DrawOdd.Value, item.AwayOdd.Value);
                    else if (takeCurrentOddsItemScores(scores, odds))
                        fData[bgFeedKey].SetScores(item.HomeScore.Value, item.AwayScore.Value);
                }
            }
            //var cleanFeed = fData.Values.ToList();
            return fData;
        }

        private bool takeLastUpdate(bool[] scores, bool[] odds)
        {
            var s1 = !scores[0] && !odds[0] && !scores[1] && !odds[1];
            var s2 = scores[0] && !odds[0] && scores[1] && !odds[1];
            var s3 = !scores[0] && odds[0] && !scores[1] && odds[1];
            var s4 = scores[0] && odds[0] && scores[1] && odds[1];
            return s1 || s2 || s3 || s4;
        }

        private bool takeCurrentOddsItemScores(bool[] scores, bool[] odds)
        {
            var s1 = !scores[0] && odds[0] && scores[1] && !odds[1];
            return s1;
        }

        private bool takeCurrentScoreItemOdds(bool[] scores, bool[] odds)
        {
            var s1 = scores[0] && !odds[0] && !scores[1] && odds[1];
            return s1;
        }

        private bool takeItem(bool[] scores, bool[] odds)
        {
            var s1 = !scores[0] && !odds[0] && scores[1] && !odds[1];
            var s2 = !scores[0] && !odds[0] && !scores[1] && odds[1];
            var s3 = !scores[0] && !odds[0] && scores[1] && odds[1];
            var s4 = scores[0] && !odds[0] && scores[1] && odds[1];
            var s5 = !scores[0] && odds[0] && scores[1] && odds[1];
            return s1 || s2 || s3 || s4 || s5;
        }

        private bool takeCurrent(bool[] scores, bool[] odds)
        {
            var s1 = scores[0] && !odds[0] && !scores[1] && !odds[1];
            var s2 = !scores[0] && odds[0] && !scores[1] && !odds[1];
            var s3 = scores[0] && odds[0] && !scores[1] && !odds[1];
            var s4 = scores[0] && odds[0] && scores[1] && !odds[1];
            var s5 = scores[0] && odds[0] && !scores[1] && odds[1];
            return s1 || s2 || s3 || s4 || s5;
        }

        public bool[] HasScores(BgMatch current, BgMatch item)
        {
            var arr = new bool[2]
            {
                current.HomeScore.HasValue && current.AwayScore.HasValue,
                item.HomeScore.HasValue && item.AwayScore.HasValue
            };
            return arr;
        }
        public bool[] HasOdds(BgMatch current, BgMatch item)
        {
            var arr = new bool[2]
            {
                current.HomeOdd.HasValue && current.DrawOdd.HasValue && current.AwayOdd.HasValue,
                item.HomeOdd.HasValue && item.DrawOdd.HasValue && item.AwayOdd.HasValue,
            };
            return arr;
        }
        #endregion
        public List<BgRawMatch> GetBgRawFeedWithFilter(BgSport sport,
            DateTime fetchDate,
            int gmtOffset,
            int? countryId,
            int? leagueId)
        {
            var daysFromToday = fetchDate - DateTime.Today;
            var bgMatches = persistance.FetchFilterBgFeed(sport, daysFromToday.Days, -gmtOffset, countryId, leagueId)
                .ConvertAll<BgRawMatch>(m => new BgRawMatch(
                    (BgSourceProvider)m.Source,
                    sport,
                    m.MatchId,
                    m.Status,
                    m.CountryId,
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart.AddHours(gmtOffset),
                    m.HomeId,
                    m.AwayId,
                    m.LastUpdate,
                    (decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd,
                    m.HomeScore, m.AwayScore,
                    m.HomeQ1, m.AwayQ1,
                    m.HomeQ2, m.AwayQ2,
                    m.HomeQ3, m.AwayQ3,
                    m.HomeQ4, m.AwayQ4
                    ));
            return bgMatches;
        }
        public BgFeedFullRepository GetBgMatchesFeedWithFilter(BgSport sport,
            DateTime fetchDate,
            int gmtOffset,
            List<int> whitelistLeagueIds,
            MapperRespository mapper)
        {
            var daysFromToday = fetchDate - DateTime.Today;
            var bgMatches = persistance.FetchFilterBgFeed(sport, daysFromToday.Days, gmtOffset, null, null)
                .Where(m => whitelistLeagueIds.Contains(m.LeagueId))
                .Where(m =>
                {
                    if (sport == BgSport.Soccer)
                        return (m.HomeOdd.HasValue && m.DrawOdd.HasValue && m.AwayOdd.HasValue);
                    return true;
                })
                .ToList()
                .ConvertAll<BgMatch>(m => new BgMatch(
                    (BgSourceProvider)m.Source,
                    sport,
                    m.MatchId,
                    m.Status,
                    m.CountryId,
                    ExtractBgNameFromBgId(sport, BgMapperType.Countries, m.CountryId.Value, mapper),
                    ExtractBgNameFromBgId(sport, BgMapperType.Leagues, m.LeagueId, mapper),
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart.AddHours(-gmtOffset),
                    ExtractBgNameFromBgId(sport, BgMapperType.Teams, m.HomeId, mapper),
                    m.HomeId,
                    ExtractBgNameFromBgId(sport, BgMapperType.Teams, m.AwayId, mapper),
                    m.AwayId,
                    m.LastUpdate,
                    (decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd,
                    m.HomeScore, m.AwayScore,
                    m.HomeQ1, m.AwayQ1,
                    m.HomeQ2, m.AwayQ2,
                    m.HomeQ3, m.AwayQ3,
                    m.HomeQ4, m.AwayQ4
                    ));
            var statistics = GetStatisticsTotals(bgMatches);
            var statisticsDetails = GetStatisticsDetails(bgMatches);
            var repo = new BgFeedFullRepository(bgMatches, statistics, statisticsDetails, null);
            //var matchesWithStatistics = bgMatches.Where(m => repo.Statistics.ContainsKey(m.MatchId) || m.Sport == BgSport.Basketball)
            //    .ToList();
            //return matchesWithStatistics;
            return repo;
        }
        public BgFeedFullRepository GetBgFeedWithFilter(BgSport sport,
        DateTime fetchDate,
        int gmtOffset,
        int? countryId,
        int? leagueId,
        MapperRespository mapper)
        {
            var daysFromToday = fetchDate - DateTime.Today;
            var bgMatches = persistance.FetchFilterBgFeed(sport, daysFromToday.Days, -gmtOffset, countryId, leagueId)
                .ConvertAll<BgMatch>(m => new BgMatch(
                    (BgSourceProvider)m.Source,
                    sport,
                    m.MatchId,
                    m.Status,
                    m.CountryId,
                    ExtractBgNameFromBgId(sport, BgMapperType.Countries, m.CountryId.Value, mapper),
                    ExtractBgNameFromBgId(sport, BgMapperType.Leagues, m.LeagueId, mapper),
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart.AddHours(gmtOffset),
                    ExtractBgNameFromBgId(sport, BgMapperType.Teams, m.HomeId, mapper),
                    m.HomeId,
                    ExtractBgNameFromBgId(sport, BgMapperType.Teams, m.AwayId, mapper),
                    m.AwayId,
                    m.LastUpdate,
                    (decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd,
                    m.HomeScore, m.AwayScore,
                    m.HomeQ1, m.AwayQ1,
                    m.HomeQ2, m.AwayQ2,
                    m.HomeQ3, m.AwayQ3,
                    m.HomeQ4, m.AwayQ4
                    ));
            var statistics = GetStatisticsTotals(bgMatches);
            var impliedStatistics = GetStatisticsDetails(bgMatches);

            var info = persistance.GetBgFeedInfo(bgMatches.Select(m => m.MatchId).ToList())
                .ConvertAll<BgMatchInfo>(m => new BgMatchInfo(m.MatchId,
                    m.BadMatches,
                    m.HomeMatches,
                    m.AwayMatches,
                    m.Head2HeadMatches,
                    m.LastUpdate
                    ));

            var repo = new BgFeedFullRepository(bgMatches, statistics, impliedStatistics, info);
            return repo;
        }

        public Dictionary<long, List<BgMatchStatisticsDetails>> GetStatisticsDetails(List<BgMatch> bgMatches)
        {
            var details = new Dictionary<long, List<BgMatchStatisticsDetails>>();
            details = persistance.GetBgFeedStatisticsDetails(bgMatches.Select(m => m.MatchId).ToList())
                .GroupBy(m => m.MatchId)
                .ToDictionary(m => m.Key,
                        m => m.ToList().ConvertAll<BgMatchStatisticsDetails>(n => new BgMatchStatisticsDetails(
                            n.MatchId,
                            (BgStatisticsDetailType)n.Type,
                            n.StatisticIndex,
                            (BgTeamsFilter)n.Population,
                            n.Lines,
                            n.Percent,
                            n.LastUpdate)));
            return details;
        }

        public List<BgMatchStatistics> GetStatisticsTotals(List<BgMatch> bgMatches)
        {
            return persistance.GetBgFeedStatistics(bgMatches.Select(m => m.MatchId).ToList())
                .ConvertAll<BgMatchStatistics>(m => new BgMatchStatistics(m.MatchId,
                    m.StatisticIndex,
                    m.FullTimeHomeWin,
                    m.FullTimeDraw,
                    m.FullTimeAwayWin,
                    m.FullTimeHomeWinH2H,
                    m.FullTimeDrawH2H,
                    m.FullTimeAwayWinH2H,
                    m.Over3_5,
                    m.Under3_5,
                    m.Over3_5H2H,
                    m.Under3_5H2H,
                    m.Over2_5,
                    m.Under2_5,
                    m.Over2_5H2H,
                    m.Under2_5H2H,
                    m.Over1_5,
                    m.Under1_5,
                    m.Over1_5H2H,
                    m.Under1_5H2H,
                    m.Range0_1,
                    m.Range2_3,
                    m.Range4Plus,
                    m.Range0_1H2H,
                    m.Range2_3H2H,
                    m.Range4PlusH2H,
                    m.BothScore,
                    m.NoBothScore,
                    m.BothScoreH2H,
                    m.NoBothScoreH2H,
                    m.HomeDraw,
                    m.NoDraw,
                    m.DrawAway,
                    m.HomeDrawH2H,
                    m.NoDrawH2H,
                    m.DrawAwayH2H,
                    Convert.ToDecimal(m.HomePower),
                    Convert.ToDecimal(m.AwayPower),
                    m.LastUpdate
                    ));
        }
        public BgMatch GetMatch(long matchId, MapperRespository mapper)
        {
            var pMatch = persistance.FetchMatch(matchId);
            var match = new BgMatch(
                    (BgSourceProvider)pMatch.Source,
                    (BgSport)pMatch.SportId,
                    pMatch.MatchId,
                    pMatch.Status,
                    pMatch.CountryId,
                    ExtractBgNameFromBgId((BgSport)pMatch.SportId, BgMapperType.Countries, pMatch.CountryId.Value, mapper),
                    ExtractBgNameFromBgId((BgSport)pMatch.SportId, BgMapperType.Leagues, pMatch.LeagueId, mapper),
                    pMatch.LeagueId,
                    pMatch.IsCup,
                    pMatch.GameStart,
                    ExtractBgNameFromBgId((BgSport)pMatch.SportId, BgMapperType.Teams, pMatch.HomeId, mapper),
                    pMatch.HomeId,
                    ExtractBgNameFromBgId((BgSport)pMatch.SportId, BgMapperType.Teams, pMatch.AwayId, mapper),
                    pMatch.AwayId,
                    pMatch.LastUpdate,
                    (decimal?)pMatch.HomeOdd, (decimal?)pMatch.DrawOdd, (decimal?)pMatch.AwayOdd,
                    pMatch.HomeScore, pMatch.AwayScore,
                    pMatch.HomeQ1, pMatch.AwayQ1,
                    pMatch.HomeQ2, pMatch.AwayQ2,
                    pMatch.HomeQ3, pMatch.AwayQ3,
                    pMatch.HomeQ4, pMatch.AwayQ4
                    );
            return match;
        }
        public BgDataRepository GetBgData(BgSport sport, DateTime fetchDate, MapperRespository mapper)
        {
            var daysFromToday = fetchDate - DateTime.Today;
            var bgMatches = persistance.FetchBgFeed(sport, daysFromToday.Days)
                .ConvertAll<BgMatch>(m => new BgMatch(
                    (BgSourceProvider)m.Source,
                    sport,
                    m.MatchId,
                    m.Status,
                    m.CountryId,
                    ExtractBgNameFromBgId(sport, BgMapperType.Countries, m.CountryId.Value, mapper),
                    ExtractBgNameFromBgId(sport, BgMapperType.Leagues, m.LeagueId, mapper),
                    m.LeagueId,
                    m.IsCup,
                    m.GameStart,
                    ExtractBgNameFromBgId(sport, BgMapperType.Teams, m.HomeId, mapper),
                    m.HomeId,
                    ExtractBgNameFromBgId(sport, BgMapperType.Teams, m.AwayId, mapper),
                    m.AwayId,
                    m.LastUpdate,
                    (decimal?)m.HomeOdd, (decimal?)m.DrawOdd, (decimal?)m.AwayOdd,
                    m.HomeScore, m.AwayScore,
                    m.HomeQ1, m.AwayQ1,
                    m.HomeQ2, m.AwayQ2,
                    m.HomeQ3, m.AwayQ3,
                    m.HomeQ4, m.AwayQ4
                    ));
            var noDuplicateFeed = new Dictionary<BgFeedKey, BgMatch>();
            var counter = new Dictionary<BgFeedKey, List<BgMatch>>();
            foreach (var item in bgMatches)
            {
                var bgFeedKey = new BgFeedKey(item.Sport, item.GameStart, item.HomeId, item.AwayId);
                if (!noDuplicateFeed.ContainsKey(bgFeedKey))
                    noDuplicateFeed.Add(bgFeedKey, item);
                if (!counter.ContainsKey(bgFeedKey))
                    counter.Add(bgFeedKey, new List<BgMatch>() { item });
                else
                    counter[bgFeedKey].Add(item);
            }
            var duplicates = counter.Where(i => i.Value.Count > 1).ToDictionary(m => m.Key, m => m.Value);
            var all = new List<BgMatch>();
            foreach (var i in counter)
                all.AddRange(i.Value);
            var repository = new BgDataRepository(noDuplicateFeed, duplicates, all);
            return repository;
        }
        private bool IsIdenticalBgMatch(BgMatch a, BgMatch b)
        {
            return
                a.Sport == b.Sport &&
                a.Source == b.Source &&
                a.Status == b.Status &&
                a.CountryId == b.CountryId &&
                a.LeagueId == b.LeagueId &&
                a.IsCup == b.IsCup &&
                a.GameStart == b.GameStart &&
                a.HomeId == b.HomeId &&
                a.AwayId == b.AwayId &&
                a.HomeOdd == b.HomeOdd &&
                a.DrawOdd == b.DrawOdd &&
                a.AwayOdd == b.DrawOdd &&
                a.HomeScore == b.HomeScore &&
                a.AwayScore == b.AwayScore &&
                a.HQ1 == b.HQ1 &&
                a.AQ1 == b.AQ1 &&
                a.HQ2 == b.HQ2 &&
                a.AQ2 == b.AQ2 &&
                a.HQ3 == b.HQ3 &&
                a.AQ3 == b.AQ3 &&
                a.HQ4 == b.HQ4 &&
                a.AQ4 == b.AQ4;
        }
        public FeedPersistanceRepository MergeAllFeeds(Dictionary<BgFeedKey, BgMatch> generatedFeed,
                    BgDataRepository persistanceFeed,
                    List<ManualMatch> manualDataByDate)
        {
            var generatedMergePersistance = new Dictionary<BgFeedKey, BgMatch>();
            foreach (var genItem in generatedFeed)//??? there are no duplicates in persistance feed
            {
                BgMatch match = null;
                if (persistanceFeed.FeedWithNoDuplicates.ContainsKey(genItem.Key))
                {
                    var beforeMergeMatches = new List<BgMatch>();
                    beforeMergeMatches.Add(persistanceFeed.FeedWithNoDuplicates[genItem.Key]); //must be first //
                    beforeMergeMatches.Add(genItem.Value);
                    var singleMatch = BgDataServices.Instance.MergeDuplicates(beforeMergeMatches);
                    var matchMerged = singleMatch.First().Value;
                    var lastUpdate = IsIdenticalBgMatch(beforeMergeMatches[0], beforeMergeMatches[1])
                        ? beforeMergeMatches[0].LastUpdate
                        : beforeMergeMatches[1].LastUpdate;
                    match = new BgMatch(
                        matchMerged.Source,
                        matchMerged.Sport,
                        matchMerged.MatchId,
                        matchMerged.Status,
                        genItem.Value.CountryId,
                        matchMerged.Country,
                        matchMerged.League,
                        genItem.Value.LeagueId,
                        matchMerged.IsCup,
                        matchMerged.GameStart,
                        matchMerged.Home,
                        matchMerged.HomeId,
                        matchMerged.Away,
                        matchMerged.AwayId,
                        lastUpdate,
                        matchMerged.HomeOdd, matchMerged.DrawOdd, matchMerged.AwayOdd,
                        matchMerged.HomeScore, matchMerged.AwayScore,
                        matchMerged.HQ1,
                        matchMerged.AQ1,
                        matchMerged.HQ2,
                        matchMerged.AQ2,
                        matchMerged.HQ3,
                        matchMerged.AQ3,
                        matchMerged.HQ4,
                        matchMerged.AQ4
                    );
                }
                else
                {
                    match = genItem.Value;
                }
                generatedMergePersistance.Add(new BgFeedKey(match.Sport, match.GameStart, match.HomeId, match.AwayId), match);
            }
            var manualPersistanceKeysData = persistanceFeed.FeedWithNoDuplicates.Keys.Except(generatedMergePersistance.Keys);
            foreach (var key in manualPersistanceKeysData)
                generatedMergePersistance.Add(key, persistanceFeed.FeedWithNoDuplicates[key]);
            var finalFeed = BgDataServices.Instance.OverrideFeedWithManual(generatedMergePersistance, manualDataByDate); // Manual feed must be last provider
            return finalFeed;
        }

        public Dictionary<long, BgMatchStatistics> FilterMatchStatistics(Dictionary<long, List<BgMatchStatistics>> statistics)
        {
            var result = new Dictionary<long, BgMatchStatistics>();
            foreach (var match in statistics.Keys)
            {
                var lines = statistics[match].OrderBy(i => i.StatisticIndex).ToList();
                decimal? fullTimeHomeWin = null, fullTimeDraw = null, fullTimeAwayWin = null, fullTimeHomeWinH2H = null, fullTimeDrawH2H = null, fullTimeAwayWinH2H = null;
                decimal? over3_5 = null, under3_5 = null, over3_5H2H = null, under3_5H2H = null;
                decimal? over2_5 = null, under2_5 = null, over2_5H2H = null, under2_5H2H = null;
                decimal? over1_5 = null, under1_5 = null, over1_5H2H = null, under1_5H2H = null;
                decimal? range0_1 = null, range2_3 = null, range4Plus = null, range0_1H2H = null, range2_3H2H = null, range4PlusH2H = null;
                decimal? bothScore = null, noBothScore = null, bothScoreH2H = null, noBothScoreH2H = null;
                decimal? homeDraw = null, noDraw = null, drawAway = null, homeDrawH2H = null, noDrawH2H = null, drawAwayH2H = null;
                if (lines.Count > 0)
                {
                    #region FullTimeHomeWin
                    if (lines.First().FullTimeHomeWin.HasValue &&
                        lines.First().FullTimeHomeWin.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.FullTimeHomeWin.Value >= 50))
                    {
                        fullTimeHomeWin = lines.First().FullTimeHomeWin.Value;
                    }
                    #endregion
                    #region FullTimeDraw
                    if (lines.First().FullTimeDraw.HasValue &&
                        lines.First().FullTimeDraw.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.FullTimeDraw.Value >= 50))
                    {
                        fullTimeDraw = lines.First().FullTimeDraw.Value;
                    }
                    #endregion
                    #region FullTimeAwayWin
                    if (lines.First().FullTimeAwayWin.HasValue &&
                        lines.First().FullTimeAwayWin.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.FullTimeAwayWin.Value >= 50))
                    {
                        fullTimeAwayWin = lines.First().FullTimeAwayWin.Value;
                    }
                    #endregion
                    #region FullTimeHomeWinH2H
                    if (lines.First().FullTimeHomeWinH2H.HasValue &&
                        lines.First().FullTimeHomeWinH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.FullTimeHomeWinH2H.Value >= 50))
                    {
                        fullTimeHomeWinH2H = lines.First().FullTimeHomeWinH2H.Value;
                    }
                    #endregion
                    #region FullTimeDrawH2H
                    if (lines.First().FullTimeDrawH2H.HasValue &&
                        lines.First().FullTimeDrawH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.FullTimeDrawH2H.Value >= 50))
                    {
                        fullTimeDrawH2H = lines.First().FullTimeDrawH2H.Value;
                    }
                    #endregion
                    #region FullTimeAwayWinH2H
                    if (lines.First().FullTimeAwayWinH2H.HasValue &&
                        lines.First().FullTimeAwayWinH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.FullTimeAwayWinH2H.Value >= 50))
                    {
                        fullTimeAwayWinH2H = lines.First().FullTimeAwayWinH2H.Value;
                    }
                    #endregion

                    #region Over3_5
                    if (lines.First().Over3_5.HasValue &&
                        lines.First().Over3_5.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Over3_5.Value >= 50))
                    {
                        over3_5 = lines.First().Over3_5.Value;
                    }
                    #endregion
                    #region Under3_5
                    if (lines.First().Under3_5.HasValue &&
                        lines.First().Under3_5.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Under3_5.Value >= 50))
                    {
                        under3_5 = lines.First().Under3_5.Value;
                    }
                    #endregion
                    #region Over3_5H2H
                    if (lines.First().Over3_5H2H.HasValue &&
                        lines.First().Over3_5H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Over3_5H2H.Value >= 50))
                    {
                        over3_5H2H = lines.First().Over3_5H2H.Value;
                    }
                    #endregion
                    #region Under3_5H2H
                    if (lines.First().Under3_5H2H.HasValue &&
                        lines.First().Under3_5H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Under3_5H2H.Value >= 50))
                    {
                        under3_5H2H = lines.First().Under3_5H2H.Value;
                    }
                    #endregion

                    #region Over2_5
                    if (lines.First().Over2_5.HasValue &&
                        lines.First().Over2_5.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Over2_5.Value >= 50))
                    {
                        over2_5 = lines.First().Over2_5.Value;
                    }
                    #endregion
                    #region Under2_5
                    if (lines.First().Under2_5.HasValue &&
                        lines.First().Under2_5.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Under2_5.Value >= 50))
                    {
                        under2_5 = lines.First().Under2_5.Value;
                    }
                    #endregion
                    #region Over2_5H2H
                    if (lines.First().Over2_5H2H.HasValue &&
                        lines.First().Over2_5H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Over2_5H2H.Value >= 50))
                    {
                        over2_5H2H = lines.First().Over2_5H2H.Value;
                    }
                    #endregion
                    #region Under2_5H2H
                    if (lines.First().Under2_5H2H.HasValue &&
                        lines.First().Under2_5H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Under2_5H2H.Value >= 50))
                    {
                        under2_5H2H = lines.First().Under2_5H2H.Value;
                    }
                    #endregion

                    #region Over1_5
                    if (lines.First().Over1_5.HasValue &&
                        lines.First().Over1_5.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Over1_5.Value >= 50))
                    {
                        over1_5 = lines.First().Over1_5.Value;
                    }
                    #endregion
                    #region Under1_5
                    if (lines.First().Under1_5.HasValue &&
                        lines.First().Under1_5.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Under1_5.Value >= 50))
                    {
                        under1_5 = lines.First().Under1_5.Value;
                    }
                    #endregion
                    #region Over1_5H2H
                    if (lines.First().Over1_5H2H.HasValue &&
                        lines.First().Over1_5H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Over1_5H2H.Value >= 50))
                    {
                        over1_5H2H = lines.First().Over1_5H2H.Value;
                    }
                    #endregion
                    #region Under1_5H2H
                    if (lines.First().Under1_5H2H.HasValue &&
                        lines.First().Under1_5H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Under1_5H2H.Value >= 50))
                    {
                        under1_5H2H = lines.First().Under1_5H2H.Value;
                    }
                    #endregion

                    #region Range0_1
                    if (lines.First().Range0_1.HasValue &&
                        lines.First().Range0_1.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Range0_1.Value >= 50))
                    {
                        range0_1 = lines.First().Range0_1.Value;
                    }
                    #endregion
                    #region Range2_3
                    if (lines.First().Range2_3.HasValue &&
                        lines.First().Range2_3.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Range2_3.Value >= 50))
                    {
                        range2_3 = lines.First().Range2_3.Value;
                    }
                    #endregion
                    #region Range4Plus
                    if (lines.First().Range4Plus.HasValue &&
                        lines.First().Range4Plus.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Range4Plus.Value >= 50))
                    {
                        range4Plus = lines.First().Range4Plus.Value;
                    }
                    #endregion
                    #region Range0_1H2H
                    if (lines.First().Range0_1H2H.HasValue &&
                        lines.First().Range0_1H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Range0_1H2H.Value >= 50))
                    {
                        range0_1H2H = lines.First().Range0_1H2H.Value;
                    }
                    #endregion
                    #region Range2_3H2H
                    if (lines.First().Range2_3H2H.HasValue &&
                        lines.First().Range2_3H2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Range2_3H2H.Value >= 50))
                    {
                        range2_3H2H = lines.First().Range2_3H2H.Value;
                    }
                    #endregion
                    #region Range4PlusH2H
                    if (lines.First().Range4PlusH2H.HasValue &&
                        lines.First().Range4PlusH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.Range4PlusH2H.Value >= 50))
                    {
                        range4PlusH2H = lines.First().Range4PlusH2H.Value;
                    }
                    #endregion

                    #region BothScore
                    if (lines.First().BothScore.HasValue &&
                        lines.First().BothScore.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.BothScore.Value >= 50))
                    {
                        bothScore = lines.First().BothScore.Value;
                    }
                    #endregion
                    #region NoBothScore
                    if (lines.First().NoBothScore.HasValue &&
                        lines.First().NoBothScore.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.NoBothScore.Value >= 50))
                    {
                        noBothScore = lines.First().NoBothScore.Value;
                    }
                    #endregion
                    #region BothScoreH2H
                    if (lines.First().BothScoreH2H.HasValue &&
                        lines.First().BothScoreH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.BothScoreH2H.Value >= 50))
                    {
                        bothScoreH2H = lines.First().BothScoreH2H.Value;
                    }
                    #endregion
                    #region NoBothScoreH2H
                    if (lines.First().NoBothScoreH2H.HasValue &&
                        lines.First().NoBothScoreH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.NoBothScoreH2H.Value >= 50))
                    {
                        noBothScoreH2H = lines.First().NoBothScoreH2H.Value;
                    }
                    #endregion

                    #region HomeDraw
                    if (lines.First().HomeDraw.HasValue &&
                        lines.First().HomeDraw.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.HomeDraw.Value >= 50))
                    {
                        homeDraw = lines.First().HomeDraw.Value;
                    }
                    #endregion
                    #region NoDraw
                    if (lines.First().NoDraw.HasValue &&
                        lines.First().NoDraw.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.NoDraw.Value >= 50))
                    {
                        noDraw = lines.First().NoDraw.Value;
                    }
                    #endregion
                    #region DrawAway
                    if (lines.First().DrawAway.HasValue &&
                        lines.First().DrawAway.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.DrawAway.Value >= 50))
                    {
                        drawAway = lines.First().DrawAway.Value;
                    }
                    #endregion
                    #region HomeDrawH2H
                    if (lines.First().HomeDrawH2H.HasValue &&
                        lines.First().HomeDrawH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.HomeDrawH2H.Value >= 50))
                    {
                        homeDrawH2H = lines.First().HomeDrawH2H.Value;
                    }
                    #endregion
                    #region NoDrawH2H
                    if (lines.First().NoDrawH2H.HasValue &&
                        lines.First().NoDrawH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.NoDrawH2H.Value >= 50))
                    {
                        noDrawH2H = lines.First().NoDrawH2H.Value;
                    }
                    #endregion
                    #region DrawAwayH2H
                    if (lines.First().DrawAwayH2H.HasValue &&
                        lines.First().DrawAwayH2H.Value > 80 &&
                        lines.Where(i => i.StatisticIndex > 0).All(i => i.DrawAwayH2H.Value >= 50))
                    {
                        drawAwayH2H = lines.First().DrawAwayH2H.Value;
                    }
                    #endregion

                }
                result.Add(match, new BgMatchStatistics(match, 0,
                    fullTimeHomeWin,
                    fullTimeDraw,
                    fullTimeAwayWin,
                    fullTimeHomeWinH2H,
                    fullTimeDrawH2H,
                    fullTimeAwayWinH2H,
                    over3_5,
                    under3_5,
                    over3_5H2H,
                    under3_5H2H,
                    over2_5,
                    under2_5,
                    over2_5H2H,
                    under2_5H2H,
                    over1_5,
                    under1_5,
                    over1_5H2H,
                    under1_5H2H,
                    range0_1,
                    range2_3,
                    range4Plus,
                    range0_1H2H,
                    range2_3H2H,
                    range4PlusH2H,
                    bothScore,
                    noBothScore,
                    bothScoreH2H,
                    noBothScoreH2H,
                    homeDraw,
                    noDraw,
                    drawAway,
                    homeDrawH2H,
                    noDrawH2H,
                    drawAwayH2H,
                    lines.First().HomePower,
                    lines.First().AwayPower,
                    lines.First().LastUpdate
                    ));
            }
            return result;
        }

        public Dictionary<long, BgMatchImpliedStatistics> CreateImpliedStatistics(Dictionary<long, BgMatch> matches,
            Dictionary<long, List<BgMatchStatistics>> statisticsTotals,
            Dictionary<long, List<BgMatchStatisticsDetails>> statisticsDetails)
        {
            var result = new Dictionary<long, BgMatchImpliedStatistics>();
            #region Funcs
            Func<BgMatchStatisticsDetails, BgMatchStatisticsDetails, int> smartCalc = (a, b) =>
            {
                if (a != null && a.Percent.HasValue && b != null && b.Percent.HasValue)
                    return (a.Percent.Value + b.Percent.Value) / 2;
                else if (b != null && b.Percent.HasValue)
                    return b.Percent.Value;
                else if (a != null && a.Percent.HasValue)
                    return a.Percent.Value;
                return 0;
            };
            Func<List<int?>, int> smartAvgCalc = (list) =>
            {
                if (list.Any(i => i.HasValue))
                {
                    var avg = Convert.ToInt16(list
                            .Where(i => i.HasValue)
                            .Average());
                    return avg;
                }
                return 0;
            };
            Func<List<int>, int, int> normalized = (list, avgItem) =>
            {
                var sum = list.Sum();
                var normalizedResult = sum > 0
                    ? Convert.ToInt16(avgItem * 100 / list.Sum())
                    : 0;
                return normalizedResult;
            };
            #endregion
            foreach (var f in matches.Values)
            {
                var implied = new Dictionary<BgImpliedStatisticType, int>();
                var matchTotalStatistics = statisticsTotals.ContainsKey(f.MatchId)
                    ? statisticsTotals[f.MatchId].FirstOrDefault()
                    : null;
                if (statisticsDetails.ContainsKey(f.MatchId))
                {
                    #region Implied
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.FullTime__HomeWin__HomeWin,
                        BgStatisticsDetailType.FullTime__HomeWin__AwayLoose,
                        BgImpliedStatisticType.All_All_1);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.FullTime__Draw__HomeDraw,
                        BgStatisticsDetailType.FullTime__Draw__AwayDraw,
                        BgImpliedStatisticType.All_All_X);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.FullTime__AwayWin__AwayWin,
                        BgStatisticsDetailType.FullTime__AwayWin__HomeLoose,
                        BgImpliedStatisticType.All_All_2);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.OverUnder__Under1_5__HomeUnder,
                        BgStatisticsDetailType.OverUnder__Under1_5__AwayUnder,
                        BgImpliedStatisticType.All_All_Under1_5);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.OverUnder__Over1_5__HomeOver,
                        BgStatisticsDetailType.OverUnder__Over1_5__AwayOver,
                        BgImpliedStatisticType.All_All_Over1_5);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.OverUnder__Under2_5__HomeUnder,
                        BgStatisticsDetailType.OverUnder__Under2_5__AwayUnder,
                        BgImpliedStatisticType.All_All_Under2_5);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.OverUnder__Over2_5__HomeOver,
                        BgStatisticsDetailType.OverUnder__Over2_5__AwayOver,
                        BgImpliedStatisticType.All_All_Over2_5);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.OverUnder__Under3_5__HomeUnder,
                        BgStatisticsDetailType.OverUnder__Under3_5__AwayUnder,
                        BgImpliedStatisticType.All_All_Under3_5);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.OverUnder__Over3_5__HomeOver,
                        BgStatisticsDetailType.OverUnder__Over3_5__AwayOver,
                        BgImpliedStatisticType.All_All_Over3_5);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.Range__InRange0_1__Home,
                        BgStatisticsDetailType.Range__InRange0_1__Away,
                        BgImpliedStatisticType.All_All_0_1);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.Range__InRange2_3__Home,
                        BgStatisticsDetailType.Range__InRange2_3__Away,
                        BgImpliedStatisticType.All_All_2_3);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.Range__InRange4Pplus__Home,
                        BgStatisticsDetailType.Range__InRange4Pplus__Away,
                        BgImpliedStatisticType.All_All_4Plus);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.BothScore__Yes__Home,
                        BgStatisticsDetailType.BothScore__Yes__Away,
                        BgImpliedStatisticType.All_All_BothScore);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.BothScore__No__Home,
                        BgStatisticsDetailType.BothScore__No__Away,
                        BgImpliedStatisticType.All_All_NoBothScore);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.WinDrawLose__WinDraw__HomeWinDraw,
                        BgStatisticsDetailType.WinDrawLose__WinDraw__AwayDrawLose,
                        BgImpliedStatisticType.All_All_1X);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.WinDrawLose__NoDraw__Home,
                        BgStatisticsDetailType.WinDrawLose__NoDraw__Away,
                        BgImpliedStatisticType.All_All_12);
                    AddImplied(statisticsDetails, smartCalc, f, implied,
                        BgStatisticsDetailType.WinDrawLose__DrawLose__Home,
                        BgStatisticsDetailType.WinDrawLose__DrawLose__Away,
                        BgImpliedStatisticType.All_All_2X);
                    #endregion

                    #region AverageImplied
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_1);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_X);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_2);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_Under1_5);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_Over1_5);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_Under2_5);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_Over2_5);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_Under3_5);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_Over3_5);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_0_1);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_2_3);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_4Plus);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_BothScore);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_BothNoScore);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_1X);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_12);
                    AddAverageImplied(matchTotalStatistics, statisticsDetails, smartAvgCalc, f, implied, BgImpliedStatisticType.Avg_2X);
                    #endregion

                    #region NormalizeImplied
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_1);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_X);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_2);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_Under1_5);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_Over1_5);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_Under2_5);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_Over2_5);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_Under3_5);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_Over3_5);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_0_1);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_2_3);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_4Plus);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_BothScore);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_BothNoScore);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_1X);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_12);
                    AddWebImplied(normalized, f, implied, BgImpliedStatisticType.Web_2X);
                    #endregion

                    result.Add(f.MatchId, new BgMatchImpliedStatistics(implied));
                }
            }
            return result;
        }

        private void AddWebImplied(Func<List<int>, int, int> normalized,
            BgMatch f,
            Dictionary<BgImpliedStatisticType, int> implied,
            BgImpliedStatisticType webType)
        {
            var list = new List<int>();
            var item = 0;
            switch (webType)
            {
                case BgImpliedStatisticType.Web_1:
                case BgImpliedStatisticType.Web_X:
                case BgImpliedStatisticType.Web_2:
                    list = new List<int>() {
                        implied[BgImpliedStatisticType.Avg_1],
                        implied[BgImpliedStatisticType.Avg_X],
                        implied[BgImpliedStatisticType.Avg_2] };
                    switch (webType)
                    {
                        case BgImpliedStatisticType.Web_1:
                            item = implied[BgImpliedStatisticType.Avg_1];
                            break;
                        case BgImpliedStatisticType.Web_X:
                            item = implied[BgImpliedStatisticType.Avg_X];
                            break;
                        case BgImpliedStatisticType.Web_2:
                            item = implied[BgImpliedStatisticType.Avg_2];
                            break;
                    }
                    break;
                case BgImpliedStatisticType.Web_Under1_5:
                case BgImpliedStatisticType.Web_Over1_5:
                    list = new List<int>() {
                        implied[BgImpliedStatisticType.Avg_Under1_5],
                        implied[BgImpliedStatisticType.Avg_Over1_5]};
                    switch (webType)
                    {
                        case BgImpliedStatisticType.Web_Under1_5:
                            item = implied[BgImpliedStatisticType.Avg_Under1_5];
                            break;
                        case BgImpliedStatisticType.Web_Over1_5:
                            item = implied[BgImpliedStatisticType.Avg_Over1_5];
                            break;
                    }
                    break;
                case BgImpliedStatisticType.Web_Under2_5:
                case BgImpliedStatisticType.Web_Over2_5:
                    list = new List<int>() {
                        implied[BgImpliedStatisticType.Avg_Under2_5],
                        implied[BgImpliedStatisticType.Avg_Over2_5]};
                    switch (webType)
                    {
                        case BgImpliedStatisticType.Web_Under2_5:
                            item = implied[BgImpliedStatisticType.Avg_Under2_5];
                            break;
                        case BgImpliedStatisticType.Web_Over2_5:
                            item = implied[BgImpliedStatisticType.Avg_Over2_5];
                            break;
                    }
                    break;
                case BgImpliedStatisticType.Web_Under3_5:
                case BgImpliedStatisticType.Web_Over3_5:
                    list = new List<int>() {
                        implied[BgImpliedStatisticType.Avg_Under3_5],
                        implied[BgImpliedStatisticType.Avg_Over3_5]};
                    switch (webType)
                    {
                        case BgImpliedStatisticType.Web_Under3_5:
                            item = implied[BgImpliedStatisticType.Avg_Under3_5];
                            break;
                        case BgImpliedStatisticType.Web_Over3_5:
                            item = implied[BgImpliedStatisticType.Avg_Over3_5];
                            break;
                    }
                    break;
                case BgImpliedStatisticType.Web_0_1:
                case BgImpliedStatisticType.Web_2_3:
                case BgImpliedStatisticType.Web_4Plus:
                    list = new List<int>() {
                        implied[BgImpliedStatisticType.Avg_0_1],
                        implied[BgImpliedStatisticType.Avg_2_3],
                        implied[BgImpliedStatisticType.Avg_4Plus] };
                    switch (webType)
                    {
                        case BgImpliedStatisticType.Web_0_1:
                            item = implied[BgImpliedStatisticType.Avg_0_1];
                            break;
                        case BgImpliedStatisticType.Web_2_3:
                            item = implied[BgImpliedStatisticType.Avg_2_3];
                            break;
                        case BgImpliedStatisticType.Web_4Plus:
                            item = implied[BgImpliedStatisticType.Avg_4Plus];
                            break;
                    }
                    break;
                case BgImpliedStatisticType.Web_BothScore:
                case BgImpliedStatisticType.Web_BothNoScore:
                    list = new List<int>() {
                        implied[BgImpliedStatisticType.Avg_BothScore],
                        implied[BgImpliedStatisticType.Avg_BothNoScore]};
                    switch (webType)
                    {
                        case BgImpliedStatisticType.Web_BothScore:
                            item = implied[BgImpliedStatisticType.Avg_BothScore];
                            break;
                        case BgImpliedStatisticType.Web_BothNoScore:
                            item = implied[BgImpliedStatisticType.Avg_BothNoScore];
                            break;
                    }
                    break;
            }
            if (webType != BgImpliedStatisticType.Web_1X &&
                webType != BgImpliedStatisticType.Web_12 &&
                webType != BgImpliedStatisticType.Web_2X)
            {
                implied.Add(webType, normalized(list, item));
            }
            else
            {
                var special = 0;
                switch (webType)
                {
                    case BgImpliedStatisticType.Web_1X:
                        special = implied[BgImpliedStatisticType.Web_1] + implied[BgImpliedStatisticType.Web_X];
                        break;
                    case BgImpliedStatisticType.Web_12:
                        special = implied[BgImpliedStatisticType.Web_1] + implied[BgImpliedStatisticType.Web_2];
                        break;
                    case BgImpliedStatisticType.Web_2X:
                        special = implied[BgImpliedStatisticType.Web_2] + implied[BgImpliedStatisticType.Web_X];
                        break;
                }
                implied.Add(webType, special);
            }
        }

        private void AddAverageImplied(BgMatchStatistics statisticsTotals,
            Dictionary<long, List<BgMatchStatisticsDetails>> statisticsDetails,
            Func<List<int?>, int> smartAvgCalc,
            BgMatch f,
            Dictionary<BgImpliedStatisticType, int> implied,
            BgImpliedStatisticType impliedType)
        {
            var list = new List<int?>();
            switch (impliedType)
            {
                case BgImpliedStatisticType.Avg_1:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_1],
                        statisticsTotals?.FullTimeHomeWin,
                        statisticsTotals?.FullTimeHomeWinH2H };
                    break;
                case BgImpliedStatisticType.Avg_X:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_X],
                        statisticsTotals?.FullTimeDraw,
                        statisticsTotals?.FullTimeDrawH2H };
                    break;
                case BgImpliedStatisticType.Avg_2:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_2],
                        statisticsTotals?.FullTimeAwayWin,
                        statisticsTotals?.FullTimeAwayWinH2H };
                    break;
                case BgImpliedStatisticType.Avg_Under1_5:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_Under1_5],
                        statisticsTotals?.Under1_5,
                        statisticsTotals?.Under1_5H2H };
                    break;
                case BgImpliedStatisticType.Avg_Over1_5:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_Over1_5],
                        statisticsTotals?.Over1_5,
                        statisticsTotals?.Over1_5H2H };
                    break;
                case BgImpliedStatisticType.Avg_Under2_5:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_Under2_5],
                        statisticsTotals?.Under2_5,
                        statisticsTotals?.Under2_5H2H };
                    break;
                case BgImpliedStatisticType.Avg_Over2_5:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_Over2_5],
                        statisticsTotals?.Over2_5,
                        statisticsTotals?.Over2_5H2H };
                    break;
                case BgImpliedStatisticType.Avg_Under3_5:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_Under3_5],
                        statisticsTotals?.Under3_5,
                        statisticsTotals?.Under3_5H2H };
                    break;
                case BgImpliedStatisticType.Avg_Over3_5:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_Over3_5],
                        statisticsTotals?.Over3_5,
                        statisticsTotals?.Over3_5H2H };
                    break;
                case BgImpliedStatisticType.Avg_0_1:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_0_1],
                        statisticsTotals?.Range0_1,
                        statisticsTotals?.Range0_1H2H };
                    break;
                case BgImpliedStatisticType.Avg_2_3:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_2_3],
                        statisticsTotals?.Range2_3,
                        statisticsTotals?.Range2_3H2H };
                    break;
                case BgImpliedStatisticType.Avg_4Plus:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_4Plus],
                        statisticsTotals?.Range4Plus,
                        statisticsTotals?.Range4PlusH2H };
                    break;
                case BgImpliedStatisticType.Avg_BothScore:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_BothScore],
                        statisticsTotals?.BothScore,
                        statisticsTotals?.BothScoreH2H };
                    break;
                case BgImpliedStatisticType.Avg_BothNoScore:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_NoBothScore],
                        statisticsTotals?.NoBothScore,
                        statisticsTotals?.NoBothScoreH2H };
                    break;
                case BgImpliedStatisticType.Avg_1X:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_1X],
                        statisticsTotals?.HomeDraw,
                        statisticsTotals?.HomeDrawH2H };
                    break;
                case BgImpliedStatisticType.Avg_12:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_12],
                        statisticsTotals?.NoDraw,
                        statisticsTotals?.NoDrawH2H };
                    break;
                case BgImpliedStatisticType.Avg_2X:
                    list = new List<int?>() {
                        implied[BgImpliedStatisticType.All_All_2X],
                        statisticsTotals?.DrawAway,
                        statisticsTotals?.DrawAwayH2H };
                    break;
            }

            implied.Add(impliedType, smartAvgCalc(list));
        }

        private static void AddImplied(Dictionary<long, List<BgMatchStatisticsDetails>> statisticsDetails,
            Func<BgMatchStatisticsDetails, BgMatchStatisticsDetails, int> smartCalc,
            BgMatch f,
            Dictionary<BgImpliedStatisticType, int> implied,
            BgStatisticsDetailType typeI1,
            BgStatisticsDetailType typeI2,
            BgImpliedStatisticType impliedType)
        {
            var i1 = statisticsDetails[f.MatchId]
                .FirstOrDefault(s => s.Type == typeI1 && s.Population == BgTeamsFilter.All_All);
            var i2 = statisticsDetails[f.MatchId]
                .FirstOrDefault(s => s.Type == typeI2 && s.Population == BgTeamsFilter.All_All);
            implied.Add(impliedType, smartCalc(i1, i2));
        }
    }
}
