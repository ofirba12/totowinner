﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public interface IExternalServices
    {
        string FetchDataFromUrl(string url);
    }
}
