﻿using System;
using System.IO;
using System.Net;
using TotoWinner.Common.Infra;

namespace TotoWinner.Services
{
    public class ExternalServices : Singleton<ExternalServices>, IExternalServices
    {
        private ExternalServices() { }

        public string FetchDataFromUrl(string url)
        {
            try
            {
                var jsonString = string.Empty;
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    jsonString = reader.ReadToEnd();
                }
                return jsonString;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying fetch data from URL {url}", ex);
            }
        }
    }
}
