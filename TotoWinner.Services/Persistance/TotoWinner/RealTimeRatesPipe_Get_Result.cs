//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TotoWinner.Services.Persistance.TotoWinner
{
    using System;
    
    public partial class RealTimeRatesPipe_Get_Result
    {
        public byte ProgramType { get; set; }
        public byte GameIndex { get; set; }
        public double Rate1 { get; set; }
        public double RateX { get; set; }
        public double Rate2 { get; set; }
    }
}
