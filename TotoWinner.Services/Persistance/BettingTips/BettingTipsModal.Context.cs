﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TotoWinner.Services.Persistance.BettingTips
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class BettingTipsEntities : DbContext
    {
        public BettingTipsEntities()
            : base("name=BettingTipsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<SystemState> SystemStates { get; set; }
        public virtual DbSet<Subscriber> Subscribers { get; set; }
        public virtual DbSet<SubscribersUrl> SubscribersUrls { get; set; }
        public virtual DbSet<BroadcastSchedule> BroadcastSchedules { get; set; }
        public virtual DbSet<Program> Programs { get; set; }
        public virtual DbSet<BotButton> BotButtons { get; set; }
        public virtual DbSet<Tip> Tips { get; set; }
    
        public virtual int EraseAllData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("EraseAllData");
        }
    
        public virtual int Programs_Merge(Nullable<long> programId, Nullable<System.DateTime> programDate)
        {
            var programIdParameter = programId.HasValue ?
                new ObjectParameter("ProgramId", programId) :
                new ObjectParameter("ProgramId", typeof(long));
    
            var programDateParameter = programDate.HasValue ?
                new ObjectParameter("ProgramDate", programDate) :
                new ObjectParameter("ProgramDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Programs_Merge", programIdParameter, programDateParameter);
        }
    
        public virtual int AuditTip(Nullable<int> programId, Nullable<int> gameIndex, Nullable<System.DateTime> auditTS, string auditDetails)
        {
            var programIdParameter = programId.HasValue ?
                new ObjectParameter("ProgramId", programId) :
                new ObjectParameter("ProgramId", typeof(int));
    
            var gameIndexParameter = gameIndex.HasValue ?
                new ObjectParameter("GameIndex", gameIndex) :
                new ObjectParameter("GameIndex", typeof(int));
    
            var auditTSParameter = auditTS.HasValue ?
                new ObjectParameter("AuditTS", auditTS) :
                new ObjectParameter("AuditTS", typeof(System.DateTime));
    
            var auditDetailsParameter = auditDetails != null ?
                new ObjectParameter("AuditDetails", auditDetails) :
                new ObjectParameter("AuditDetails", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AuditTip", programIdParameter, gameIndexParameter, auditTSParameter, auditDetailsParameter);
        }
    
        public virtual int Tips_Audit(Nullable<long> programId, Nullable<int> gameIndex, Nullable<System.DateTime> auditTS, string auditDetails)
        {
            var programIdParameter = programId.HasValue ?
                new ObjectParameter("ProgramId", programId) :
                new ObjectParameter("ProgramId", typeof(long));
    
            var gameIndexParameter = gameIndex.HasValue ?
                new ObjectParameter("GameIndex", gameIndex) :
                new ObjectParameter("GameIndex", typeof(int));
    
            var auditTSParameter = auditTS.HasValue ?
                new ObjectParameter("AuditTS", auditTS) :
                new ObjectParameter("AuditTS", typeof(System.DateTime));
    
            var auditDetailsParameter = auditDetails != null ?
                new ObjectParameter("AuditDetails", auditDetails) :
                new ObjectParameter("AuditDetails", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Tips_Audit", programIdParameter, gameIndexParameter, auditTSParameter, auditDetailsParameter);
        }
    
        public virtual int Tips_UpdateResults(Nullable<long> programId, Nullable<int> gameIndex, string tipOfficialResult, string tipOfficialMark, string tipManualResult, string tipManualMark)
        {
            var programIdParameter = programId.HasValue ?
                new ObjectParameter("ProgramId", programId) :
                new ObjectParameter("ProgramId", typeof(long));
    
            var gameIndexParameter = gameIndex.HasValue ?
                new ObjectParameter("GameIndex", gameIndex) :
                new ObjectParameter("GameIndex", typeof(int));
    
            var tipOfficialResultParameter = tipOfficialResult != null ?
                new ObjectParameter("TipOfficialResult", tipOfficialResult) :
                new ObjectParameter("TipOfficialResult", typeof(string));
    
            var tipOfficialMarkParameter = tipOfficialMark != null ?
                new ObjectParameter("TipOfficialMark", tipOfficialMark) :
                new ObjectParameter("TipOfficialMark", typeof(string));
    
            var tipManualResultParameter = tipManualResult != null ?
                new ObjectParameter("TipManualResult", tipManualResult) :
                new ObjectParameter("TipManualResult", typeof(string));
    
            var tipManualMarkParameter = tipManualMark != null ?
                new ObjectParameter("TipManualMark", tipManualMark) :
                new ObjectParameter("TipManualMark", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Tips_UpdateResults", programIdParameter, gameIndexParameter, tipOfficialResultParameter, tipOfficialMarkParameter, tipManualResultParameter, tipManualMarkParameter);
        }
    
        public virtual int Tips_UpdateStatus(Nullable<long> programId, Nullable<int> gameIndex, string status)
        {
            var programIdParameter = programId.HasValue ?
                new ObjectParameter("ProgramId", programId) :
                new ObjectParameter("ProgramId", typeof(long));
    
            var gameIndexParameter = gameIndex.HasValue ?
                new ObjectParameter("GameIndex", gameIndex) :
                new ObjectParameter("GameIndex", typeof(int));
    
            var statusParameter = status != null ?
                new ObjectParameter("Status", status) :
                new ObjectParameter("Status", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Tips_UpdateStatus", programIdParameter, gameIndexParameter, statusParameter);
        }
    
        public virtual int SystemState_Merge(string name, string value)
        {
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SystemState_Merge", nameParameter, valueParameter);
        }
    
        public virtual int SubscribersUrls_Visit(Nullable<System.Guid> urlGuid)
        {
            var urlGuidParameter = urlGuid.HasValue ?
                new ObjectParameter("UrlGuid", urlGuid) :
                new ObjectParameter("UrlGuid", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SubscribersUrls_Visit", urlGuidParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> Subscribers_Merge(Nullable<int> id, Nullable<long> subscriberChatId, string firstName, string lastName, string cellNumber, string mail, Nullable<System.DateTime> registeredDate, string remarks, Nullable<System.DateTime> blockedDate, string blockageReason)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));
    
            var subscriberChatIdParameter = subscriberChatId.HasValue ?
                new ObjectParameter("SubscriberChatId", subscriberChatId) :
                new ObjectParameter("SubscriberChatId", typeof(long));
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var cellNumberParameter = cellNumber != null ?
                new ObjectParameter("CellNumber", cellNumber) :
                new ObjectParameter("CellNumber", typeof(string));
    
            var mailParameter = mail != null ?
                new ObjectParameter("Mail", mail) :
                new ObjectParameter("Mail", typeof(string));
    
            var registeredDateParameter = registeredDate.HasValue ?
                new ObjectParameter("RegisteredDate", registeredDate) :
                new ObjectParameter("RegisteredDate", typeof(System.DateTime));
    
            var remarksParameter = remarks != null ?
                new ObjectParameter("Remarks", remarks) :
                new ObjectParameter("Remarks", typeof(string));
    
            var blockedDateParameter = blockedDate.HasValue ?
                new ObjectParameter("BlockedDate", blockedDate) :
                new ObjectParameter("BlockedDate", typeof(System.DateTime));
    
            var blockageReasonParameter = blockageReason != null ?
                new ObjectParameter("BlockageReason", blockageReason) :
                new ObjectParameter("BlockageReason", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("Subscribers_Merge", idParameter, subscriberChatIdParameter, firstNameParameter, lastNameParameter, cellNumberParameter, mailParameter, registeredDateParameter, remarksParameter, blockedDateParameter, blockageReasonParameter);
        }
    
        public virtual int SubscribersUrls_Insert(Nullable<System.Guid> urlGuid, Nullable<int> tipId, Nullable<long> subscriberId)
        {
            var urlGuidParameter = urlGuid.HasValue ?
                new ObjectParameter("UrlGuid", urlGuid) :
                new ObjectParameter("UrlGuid", typeof(System.Guid));
    
            var tipIdParameter = tipId.HasValue ?
                new ObjectParameter("TipId", tipId) :
                new ObjectParameter("TipId", typeof(int));
    
            var subscriberIdParameter = subscriberId.HasValue ?
                new ObjectParameter("SubscriberId", subscriberId) :
                new ObjectParameter("SubscriberId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SubscribersUrls_Insert", urlGuidParameter, tipIdParameter, subscriberIdParameter);
        }
    
        public virtual ObjectResult<Tips_GetAll_Result> Tips_GetAll()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Tips_GetAll_Result>("Tips_GetAll");
        }
    
        public virtual ObjectResult<Tips_GetCloseForBetCandidates_Result> Tips_GetCloseForBetCandidates()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Tips_GetCloseForBetCandidates_Result>("Tips_GetCloseForBetCandidates");
        }
    
        public virtual ObjectResult<Tippers_GetAll_Result> Tippers_GetAll()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Tippers_GetAll_Result>("Tippers_GetAll");
        }
    
        public virtual ObjectResult<Nullable<int>> Tippers_Merge(Nullable<int> tipperId, string tipperName)
        {
            var tipperIdParameter = tipperId.HasValue ?
                new ObjectParameter("TipperId", tipperId) :
                new ObjectParameter("TipperId", typeof(int));
    
            var tipperNameParameter = tipperName != null ?
                new ObjectParameter("TipperName", tipperName) :
                new ObjectParameter("TipperName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("Tippers_Merge", tipperIdParameter, tipperNameParameter);
        }
    
        public virtual int Tipper_Delete(Nullable<int> tipperId)
        {
            var tipperIdParameter = tipperId.HasValue ?
                new ObjectParameter("TipperId", tipperId) :
                new ObjectParameter("TipperId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Tipper_Delete", tipperIdParameter);
        }
    
        public virtual ObjectResult<Tips_GetAllWithNoResults_Result> Tips_GetAllWithNoResults()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Tips_GetAllWithNoResults_Result>("Tips_GetAllWithNoResults");
        }
    
        public virtual ObjectResult<Nullable<int>> Tips_Merge(Nullable<long> programId, Nullable<int> gameIndex, Nullable<int> sportId, Nullable<int> totoId, Nullable<System.DateTime> gameStart, Nullable<bool> tipType, Nullable<byte> powerTip, Nullable<System.DateTime> insertedTS, Nullable<System.DateTime> updatedTS, string betName, string betLeague, string country, Nullable<bool> betParentChild, string betType, Nullable<bool> isSingle, Nullable<double> betRate, string status, string tipTime, string tipContent, string tipMarks, string tipUrl, Nullable<int> tipperId)
        {
            var programIdParameter = programId.HasValue ?
                new ObjectParameter("ProgramId", programId) :
                new ObjectParameter("ProgramId", typeof(long));
    
            var gameIndexParameter = gameIndex.HasValue ?
                new ObjectParameter("GameIndex", gameIndex) :
                new ObjectParameter("GameIndex", typeof(int));
    
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var totoIdParameter = totoId.HasValue ?
                new ObjectParameter("TotoId", totoId) :
                new ObjectParameter("TotoId", typeof(int));
    
            var gameStartParameter = gameStart.HasValue ?
                new ObjectParameter("GameStart", gameStart) :
                new ObjectParameter("GameStart", typeof(System.DateTime));
    
            var tipTypeParameter = tipType.HasValue ?
                new ObjectParameter("TipType", tipType) :
                new ObjectParameter("TipType", typeof(bool));
    
            var powerTipParameter = powerTip.HasValue ?
                new ObjectParameter("PowerTip", powerTip) :
                new ObjectParameter("PowerTip", typeof(byte));
    
            var insertedTSParameter = insertedTS.HasValue ?
                new ObjectParameter("InsertedTS", insertedTS) :
                new ObjectParameter("InsertedTS", typeof(System.DateTime));
    
            var updatedTSParameter = updatedTS.HasValue ?
                new ObjectParameter("UpdatedTS", updatedTS) :
                new ObjectParameter("UpdatedTS", typeof(System.DateTime));
    
            var betNameParameter = betName != null ?
                new ObjectParameter("BetName", betName) :
                new ObjectParameter("BetName", typeof(string));
    
            var betLeagueParameter = betLeague != null ?
                new ObjectParameter("BetLeague", betLeague) :
                new ObjectParameter("BetLeague", typeof(string));
    
            var countryParameter = country != null ?
                new ObjectParameter("Country", country) :
                new ObjectParameter("Country", typeof(string));
    
            var betParentChildParameter = betParentChild.HasValue ?
                new ObjectParameter("BetParentChild", betParentChild) :
                new ObjectParameter("BetParentChild", typeof(bool));
    
            var betTypeParameter = betType != null ?
                new ObjectParameter("BetType", betType) :
                new ObjectParameter("BetType", typeof(string));
    
            var isSingleParameter = isSingle.HasValue ?
                new ObjectParameter("IsSingle", isSingle) :
                new ObjectParameter("IsSingle", typeof(bool));
    
            var betRateParameter = betRate.HasValue ?
                new ObjectParameter("BetRate", betRate) :
                new ObjectParameter("BetRate", typeof(double));
    
            var statusParameter = status != null ?
                new ObjectParameter("Status", status) :
                new ObjectParameter("Status", typeof(string));
    
            var tipTimeParameter = tipTime != null ?
                new ObjectParameter("TipTime", tipTime) :
                new ObjectParameter("TipTime", typeof(string));
    
            var tipContentParameter = tipContent != null ?
                new ObjectParameter("TipContent", tipContent) :
                new ObjectParameter("TipContent", typeof(string));
    
            var tipMarksParameter = tipMarks != null ?
                new ObjectParameter("TipMarks", tipMarks) :
                new ObjectParameter("TipMarks", typeof(string));
    
            var tipUrlParameter = tipUrl != null ?
                new ObjectParameter("TipUrl", tipUrl) :
                new ObjectParameter("TipUrl", typeof(string));
    
            var tipperIdParameter = tipperId.HasValue ?
                new ObjectParameter("TipperId", tipperId) :
                new ObjectParameter("TipperId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("Tips_Merge", programIdParameter, gameIndexParameter, sportIdParameter, totoIdParameter, gameStartParameter, tipTypeParameter, powerTipParameter, insertedTSParameter, updatedTSParameter, betNameParameter, betLeagueParameter, countryParameter, betParentChildParameter, betTypeParameter, isSingleParameter, betRateParameter, statusParameter, tipTimeParameter, tipContentParameter, tipMarksParameter, tipUrlParameter, tipperIdParameter);
        }
    }
}
