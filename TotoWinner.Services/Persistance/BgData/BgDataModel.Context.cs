﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TotoWinner.Services.Persistance.BgData
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class BgDataEntities : DbContext
    {
        public BgDataEntities()
            : base("name=BgDataEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Feed_Goalserve> Feed_Goalserve { get; set; }
        public virtual DbSet<Feed_Goalserve_BasketballQuarters> Feed_Goalserve_BasketballQuarters { get; set; }
        public virtual DbSet<BgSystem> BgSystems { get; set; }
        public virtual DbSet<Feed_Bg_Info> Feed_Bg_Info { get; set; }
        public virtual DbSet<Feed_Bg_Statistics> Feed_Bg_Statistics { get; set; }
        public virtual DbSet<Feed_Bg_StatisticsDetails> Feed_Bg_StatisticsDetails { get; set; }
    
        public virtual ObjectResult<Feed_Goalserve_Fetch_Result> Feed_Goalserve_Fetch(Nullable<int> sportId, Nullable<int> daysAdd)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var daysAddParameter = daysAdd.HasValue ?
                new ObjectParameter("DaysAdd", daysAdd) :
                new ObjectParameter("DaysAdd", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Feed_Goalserve_Fetch_Result>("Feed_Goalserve_Fetch", sportIdParameter, daysAddParameter);
        }
    
        public virtual int Mapper_UpdateBgName(Nullable<int> bgId, string bgName)
        {
            var bgIdParameter = bgId.HasValue ?
                new ObjectParameter("BgId", bgId) :
                new ObjectParameter("BgId", typeof(int));
    
            var bgNameParameter = bgName != null ?
                new ObjectParameter("BgName", bgName) :
                new ObjectParameter("BgName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Mapper_UpdateBgName", bgIdParameter, bgNameParameter);
        }
    
        public virtual int BgSystem_Merge(string name, string value, Nullable<byte> valueType)
        {
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            var valueTypeParameter = valueType.HasValue ?
                new ObjectParameter("ValueType", valueType) :
                new ObjectParameter("ValueType", typeof(byte));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BgSystem_Merge", nameParameter, valueParameter, valueTypeParameter);
        }
    
        public virtual ObjectResult<Feed_Manual_Fetch_Result> Feed_Manual_Fetch(Nullable<System.DateTime> lastFetched)
        {
            var lastFetchedParameter = lastFetched.HasValue ?
                new ObjectParameter("LastFetched", lastFetched) :
                new ObjectParameter("LastFetched", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Feed_Manual_Fetch_Result>("Feed_Manual_Fetch", lastFetchedParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> Feed_Manual_Merge(Nullable<int> matchId, Nullable<int> sportId, string status, Nullable<int> countryId, string country, string league, Nullable<int> leagueId, Nullable<bool> isCup, Nullable<System.DateTime> gameStart, string home, Nullable<int> homeId, string away, Nullable<int> awayId, Nullable<double> homeOdd, Nullable<double> drawOdd, Nullable<double> awayOdd, Nullable<int> homeScore, Nullable<int> awayScore, Nullable<System.DateTime> lastUpdate, Nullable<bool> ignore, Nullable<int> homeQ1, Nullable<int> awayQ1, Nullable<int> homeQ2, Nullable<int> awayQ2, Nullable<int> homeQ3, Nullable<int> awayQ3, Nullable<int> homeQ4, Nullable<int> awayQ4)
        {
            var matchIdParameter = matchId.HasValue ?
                new ObjectParameter("MatchId", matchId) :
                new ObjectParameter("MatchId", typeof(int));
    
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var statusParameter = status != null ?
                new ObjectParameter("Status", status) :
                new ObjectParameter("Status", typeof(string));
    
            var countryIdParameter = countryId.HasValue ?
                new ObjectParameter("CountryId", countryId) :
                new ObjectParameter("CountryId", typeof(int));
    
            var countryParameter = country != null ?
                new ObjectParameter("Country", country) :
                new ObjectParameter("Country", typeof(string));
    
            var leagueParameter = league != null ?
                new ObjectParameter("League", league) :
                new ObjectParameter("League", typeof(string));
    
            var leagueIdParameter = leagueId.HasValue ?
                new ObjectParameter("LeagueId", leagueId) :
                new ObjectParameter("LeagueId", typeof(int));
    
            var isCupParameter = isCup.HasValue ?
                new ObjectParameter("IsCup", isCup) :
                new ObjectParameter("IsCup", typeof(bool));
    
            var gameStartParameter = gameStart.HasValue ?
                new ObjectParameter("GameStart", gameStart) :
                new ObjectParameter("GameStart", typeof(System.DateTime));
    
            var homeParameter = home != null ?
                new ObjectParameter("Home", home) :
                new ObjectParameter("Home", typeof(string));
    
            var homeIdParameter = homeId.HasValue ?
                new ObjectParameter("HomeId", homeId) :
                new ObjectParameter("HomeId", typeof(int));
    
            var awayParameter = away != null ?
                new ObjectParameter("Away", away) :
                new ObjectParameter("Away", typeof(string));
    
            var awayIdParameter = awayId.HasValue ?
                new ObjectParameter("AwayId", awayId) :
                new ObjectParameter("AwayId", typeof(int));
    
            var homeOddParameter = homeOdd.HasValue ?
                new ObjectParameter("HomeOdd", homeOdd) :
                new ObjectParameter("HomeOdd", typeof(double));
    
            var drawOddParameter = drawOdd.HasValue ?
                new ObjectParameter("DrawOdd", drawOdd) :
                new ObjectParameter("DrawOdd", typeof(double));
    
            var awayOddParameter = awayOdd.HasValue ?
                new ObjectParameter("AwayOdd", awayOdd) :
                new ObjectParameter("AwayOdd", typeof(double));
    
            var homeScoreParameter = homeScore.HasValue ?
                new ObjectParameter("HomeScore", homeScore) :
                new ObjectParameter("HomeScore", typeof(int));
    
            var awayScoreParameter = awayScore.HasValue ?
                new ObjectParameter("AwayScore", awayScore) :
                new ObjectParameter("AwayScore", typeof(int));
    
            var lastUpdateParameter = lastUpdate.HasValue ?
                new ObjectParameter("LastUpdate", lastUpdate) :
                new ObjectParameter("LastUpdate", typeof(System.DateTime));
    
            var ignoreParameter = ignore.HasValue ?
                new ObjectParameter("Ignore", ignore) :
                new ObjectParameter("Ignore", typeof(bool));
    
            var homeQ1Parameter = homeQ1.HasValue ?
                new ObjectParameter("HomeQ1", homeQ1) :
                new ObjectParameter("HomeQ1", typeof(int));
    
            var awayQ1Parameter = awayQ1.HasValue ?
                new ObjectParameter("AwayQ1", awayQ1) :
                new ObjectParameter("AwayQ1", typeof(int));
    
            var homeQ2Parameter = homeQ2.HasValue ?
                new ObjectParameter("HomeQ2", homeQ2) :
                new ObjectParameter("HomeQ2", typeof(int));
    
            var awayQ2Parameter = awayQ2.HasValue ?
                new ObjectParameter("AwayQ2", awayQ2) :
                new ObjectParameter("AwayQ2", typeof(int));
    
            var homeQ3Parameter = homeQ3.HasValue ?
                new ObjectParameter("HomeQ3", homeQ3) :
                new ObjectParameter("HomeQ3", typeof(int));
    
            var awayQ3Parameter = awayQ3.HasValue ?
                new ObjectParameter("AwayQ3", awayQ3) :
                new ObjectParameter("AwayQ3", typeof(int));
    
            var homeQ4Parameter = homeQ4.HasValue ?
                new ObjectParameter("HomeQ4", homeQ4) :
                new ObjectParameter("HomeQ4", typeof(int));
    
            var awayQ4Parameter = awayQ4.HasValue ?
                new ObjectParameter("AwayQ4", awayQ4) :
                new ObjectParameter("AwayQ4", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("Feed_Manual_Merge", matchIdParameter, sportIdParameter, statusParameter, countryIdParameter, countryParameter, leagueParameter, leagueIdParameter, isCupParameter, gameStartParameter, homeParameter, homeIdParameter, awayParameter, awayIdParameter, homeOddParameter, drawOddParameter, awayOddParameter, homeScoreParameter, awayScoreParameter, lastUpdateParameter, ignoreParameter, homeQ1Parameter, awayQ1Parameter, homeQ2Parameter, awayQ2Parameter, homeQ3Parameter, awayQ3Parameter, homeQ4Parameter, awayQ4Parameter);
        }
    
        public virtual ObjectResult<BgMapper_Fetch_Result> BgMapper_Fetch()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<BgMapper_Fetch_Result>("BgMapper_Fetch");
        }
    
        public virtual ObjectResult<Feed_Manual_FetchByMatchDate_Result> Feed_Manual_FetchByMatchDate(Nullable<System.DateTime> feedDate)
        {
            var feedDateParameter = feedDate.HasValue ?
                new ObjectParameter("FeedDate", feedDate) :
                new ObjectParameter("FeedDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Feed_Manual_FetchByMatchDate_Result>("Feed_Manual_FetchByMatchDate", feedDateParameter);
        }
    
        public virtual ObjectResult<FeedBg_Fetch_Result> FeedBg_Fetch(Nullable<int> sportId, Nullable<int> daysAdd)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var daysAddParameter = daysAdd.HasValue ?
                new ObjectParameter("DaysAdd", daysAdd) :
                new ObjectParameter("DaysAdd", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FeedBg_Fetch_Result>("FeedBg_Fetch", sportIdParameter, daysAddParameter);
        }
    
        public virtual ObjectResult<Feed_Manual_FetchByKey_Result> Feed_Manual_FetchByKey(Nullable<int> countryId, Nullable<int> leagueId, Nullable<int> homeId, Nullable<int> awayId, Nullable<System.DateTime> gameTime)
        {
            var countryIdParameter = countryId.HasValue ?
                new ObjectParameter("CountryId", countryId) :
                new ObjectParameter("CountryId", typeof(int));
    
            var leagueIdParameter = leagueId.HasValue ?
                new ObjectParameter("LeagueId", leagueId) :
                new ObjectParameter("LeagueId", typeof(int));
    
            var homeIdParameter = homeId.HasValue ?
                new ObjectParameter("HomeId", homeId) :
                new ObjectParameter("HomeId", typeof(int));
    
            var awayIdParameter = awayId.HasValue ?
                new ObjectParameter("AwayId", awayId) :
                new ObjectParameter("AwayId", typeof(int));
    
            var gameTimeParameter = gameTime.HasValue ?
                new ObjectParameter("GameTime", gameTime) :
                new ObjectParameter("GameTime", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Feed_Manual_FetchByKey_Result>("Feed_Manual_FetchByKey", countryIdParameter, leagueIdParameter, homeIdParameter, awayIdParameter, gameTimeParameter);
        }
    
        public virtual ObjectResult<FeedBg_FilterFetch_Result> FeedBg_FilterFetch(Nullable<int> sportId, Nullable<int> daysAdd, Nullable<int> gmtOffset, Nullable<int> countryId, Nullable<int> leagueId)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var daysAddParameter = daysAdd.HasValue ?
                new ObjectParameter("DaysAdd", daysAdd) :
                new ObjectParameter("DaysAdd", typeof(int));
    
            var gmtOffsetParameter = gmtOffset.HasValue ?
                new ObjectParameter("GmtOffset", gmtOffset) :
                new ObjectParameter("GmtOffset", typeof(int));
    
            var countryIdParameter = countryId.HasValue ?
                new ObjectParameter("CountryId", countryId) :
                new ObjectParameter("CountryId", typeof(int));
    
            var leagueIdParameter = leagueId.HasValue ?
                new ObjectParameter("LeagueId", leagueId) :
                new ObjectParameter("LeagueId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FeedBg_FilterFetch_Result>("FeedBg_FilterFetch", sportIdParameter, daysAddParameter, gmtOffsetParameter, countryIdParameter, leagueIdParameter);
        }
    
        public virtual ObjectResult<Feed_Manual_FetchByLastUpdate_Result> Feed_Manual_FetchByLastUpdate(Nullable<System.DateTime> updateDate)
        {
            var updateDateParameter = updateDate.HasValue ?
                new ObjectParameter("UpdateDate", updateDate) :
                new ObjectParameter("UpdateDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Feed_Manual_FetchByLastUpdate_Result>("Feed_Manual_FetchByLastUpdate", updateDateParameter);
        }
    
        public virtual ObjectResult<TeamBg_GetAllMatches_Result> TeamBg_GetAllMatches(Nullable<int> teamId)
        {
            var teamIdParameter = teamId.HasValue ?
                new ObjectParameter("TeamId", teamId) :
                new ObjectParameter("TeamId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<TeamBg_GetAllMatches_Result>("TeamBg_GetAllMatches", teamIdParameter);
        }
    
        public virtual int BgTeams_Watcher_Delete(Nullable<int> teamId)
        {
            var teamIdParameter = teamId.HasValue ?
                new ObjectParameter("TeamId", teamId) :
                new ObjectParameter("TeamId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BgTeams_Watcher_Delete", teamIdParameter);
        }
    
        public virtual ObjectResult<BgTeams_Watcher_Get_Result> BgTeams_Watcher_Get(Nullable<byte> sportId)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(byte));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<BgTeams_Watcher_Get_Result>("BgTeams_Watcher_Get", sportIdParameter);
        }
    
        public virtual int BgTeams_BlackFilter_Delete(Nullable<int> teamId, Nullable<byte> filterBgType)
        {
            var teamIdParameter = teamId.HasValue ?
                new ObjectParameter("TeamId", teamId) :
                new ObjectParameter("TeamId", typeof(int));
    
            var filterBgTypeParameter = filterBgType.HasValue ?
                new ObjectParameter("FilterBgType", filterBgType) :
                new ObjectParameter("FilterBgType", typeof(byte));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BgTeams_BlackFilter_Delete", teamIdParameter, filterBgTypeParameter);
        }
    
        public virtual ObjectResult<BgTeams_BlackFilter_GetAll_Result> BgTeams_BlackFilter_GetAll()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<BgTeams_BlackFilter_GetAll_Result>("BgTeams_BlackFilter_GetAll");
        }
    
        public virtual int BgTeams_BlackFilter_Merge(Nullable<int> teamId, Nullable<byte> filterBgType, Nullable<int> filterBgId)
        {
            var teamIdParameter = teamId.HasValue ?
                new ObjectParameter("TeamId", teamId) :
                new ObjectParameter("TeamId", typeof(int));
    
            var filterBgTypeParameter = filterBgType.HasValue ?
                new ObjectParameter("FilterBgType", filterBgType) :
                new ObjectParameter("FilterBgType", typeof(byte));
    
            var filterBgIdParameter = filterBgId.HasValue ?
                new ObjectParameter("FilterBgId", filterBgId) :
                new ObjectParameter("FilterBgId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BgTeams_BlackFilter_Merge", teamIdParameter, filterBgTypeParameter, filterBgIdParameter);
        }
    
        public virtual int BgLeagues_Whitelist_Delete(Nullable<int> leagueId)
        {
            var leagueIdParameter = leagueId.HasValue ?
                new ObjectParameter("LeagueId", leagueId) :
                new ObjectParameter("LeagueId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BgLeagues_Whitelist_Delete", leagueIdParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> BgLeagues_Whitelist_GetAll()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("BgLeagues_Whitelist_GetAll");
        }
    
        public virtual int BgLeagues_Whitelist_Merge(Nullable<int> leagueId)
        {
            var leagueIdParameter = leagueId.HasValue ?
                new ObjectParameter("LeagueId", leagueId) :
                new ObjectParameter("LeagueId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BgLeagues_Whitelist_Merge", leagueIdParameter);
        }
    
        public virtual ObjectResult<FeedBg_FetchByMatchId_Result> FeedBg_FetchByMatchId(Nullable<long> matchId)
        {
            var matchIdParameter = matchId.HasValue ?
                new ObjectParameter("MatchId", matchId) :
                new ObjectParameter("MatchId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FeedBg_FetchByMatchId_Result>("FeedBg_FetchByMatchId", matchIdParameter);
        }
    
        public virtual int MapperWinner_UpdateBgId(Nullable<byte> sport, Nullable<int> winnerId, Nullable<int> bgId)
        {
            var sportParameter = sport.HasValue ?
                new ObjectParameter("Sport", sport) :
                new ObjectParameter("Sport", typeof(byte));
    
            var winnerIdParameter = winnerId.HasValue ?
                new ObjectParameter("WinnerId", winnerId) :
                new ObjectParameter("WinnerId", typeof(int));
    
            var bgIdParameter = bgId.HasValue ?
                new ObjectParameter("BgId", bgId) :
                new ObjectParameter("BgId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("MapperWinner_UpdateBgId", sportParameter, winnerIdParameter, bgIdParameter);
        }
    
        public virtual ObjectResult<FeedWinner_Fetch_Result> FeedWinner_Fetch(Nullable<int> sportId, Nullable<int> daysAdd)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var daysAddParameter = daysAdd.HasValue ?
                new ObjectParameter("DaysAdd", daysAdd) :
                new ObjectParameter("DaysAdd", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FeedWinner_Fetch_Result>("FeedWinner_Fetch", sportIdParameter, daysAddParameter);
        }
    
        public virtual ObjectResult<FeedWinner_FetchLastTeamGame_Result> FeedWinner_FetchLastTeamGame(Nullable<int> teamId)
        {
            var teamIdParameter = teamId.HasValue ?
                new ObjectParameter("TeamId", teamId) :
                new ObjectParameter("TeamId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FeedWinner_FetchLastTeamGame_Result>("FeedWinner_FetchLastTeamGame", teamIdParameter);
        }
    
        public virtual ObjectResult<FeedBg_Tips_FetchFull_Result> FeedBg_Tips_FetchFull(Nullable<int> sportId, Nullable<int> daysAdd)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var daysAddParameter = daysAdd.HasValue ?
                new ObjectParameter("DaysAdd", daysAdd) :
                new ObjectParameter("DaysAdd", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FeedBg_Tips_FetchFull_Result>("FeedBg_Tips_FetchFull", sportIdParameter, daysAddParameter);
        }
    
        public virtual ObjectResult<BgWinnerMapper_Fetch_Result> BgWinnerMapper_Fetch()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<BgWinnerMapper_Fetch_Result>("BgWinnerMapper_Fetch");
        }
    
        public virtual int MapperWinner_UpdateGoalserveId(Nullable<byte> sportId, Nullable<int> winnerId, Nullable<int> goalserveId)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(byte));
    
            var winnerIdParameter = winnerId.HasValue ?
                new ObjectParameter("WinnerId", winnerId) :
                new ObjectParameter("WinnerId", typeof(int));
    
            var goalserveIdParameter = goalserveId.HasValue ?
                new ObjectParameter("GoalserveId", goalserveId) :
                new ObjectParameter("GoalserveId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("MapperWinner_UpdateGoalserveId", sportIdParameter, winnerIdParameter, goalserveIdParameter);
        }
    
        public virtual ObjectResult<Feed_Goalserve_Odds_Fetch_Result> Feed_Goalserve_Odds_Fetch(Nullable<int> sportId, Nullable<int> goalserveHomeId, Nullable<int> goalserveAwayId, Nullable<System.DateTime> gameStart)
        {
            var sportIdParameter = sportId.HasValue ?
                new ObjectParameter("SportId", sportId) :
                new ObjectParameter("SportId", typeof(int));
    
            var goalserveHomeIdParameter = goalserveHomeId.HasValue ?
                new ObjectParameter("GoalserveHomeId", goalserveHomeId) :
                new ObjectParameter("GoalserveHomeId", typeof(int));
    
            var goalserveAwayIdParameter = goalserveAwayId.HasValue ?
                new ObjectParameter("GoalserveAwayId", goalserveAwayId) :
                new ObjectParameter("GoalserveAwayId", typeof(int));
    
            var gameStartParameter = gameStart.HasValue ?
                new ObjectParameter("GameStart", gameStart) :
                new ObjectParameter("GameStart", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Feed_Goalserve_Odds_Fetch_Result>("Feed_Goalserve_Odds_Fetch", sportIdParameter, goalserveHomeIdParameter, goalserveAwayIdParameter, gameStartParameter);
        }
    }
}
