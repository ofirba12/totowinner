//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TotoWinner.Services.Persistance.BgData
{
    using System;
    
    public partial class BgTeams_Watcher_Get_Result
    {
        public int TeamId { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int TotalShows { get; set; }
        public int MissingOdds { get; set; }
        public int MissingResult { get; set; }
    }
}
