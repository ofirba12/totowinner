﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TotoWinner.Common.Infra;
using TotoWinner.Data.BettingTips;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services.Persistance.BettingTips;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public partial class TelegramBotPersistanceServices : Singleton<TelegramBotPersistanceServices>, ITelegramBotPersistanceServices
    {
        private TelegramBotPersistanceServices()
        {
        }

        public List<Subscriber> GetSubscribers()
        {
            try
            {
                List<Subscriber> subscribers = null;
                using (var ent = new BettingTipsEntities())
                {
                    subscribers = ent.Subscribers.ToList();
                }
                return subscribers;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all subscribers", ex);
            }
        }
        public void InsertSubscriberTip(Guid tipGuid, int tipId, long subscriberId)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.SubscribersUrls_Insert(tipGuid, tipId, subscriberId);
                    //var rec = new SubscribersUrl()
                    //{
                    //    UrlGuid = tipGuid,
                    //    TipId = tipId,
                    //    SubscriberId = subscriberId,
                    //    VisitUrlTS = null,
                    //    PickedUpByChatServer = null
                    //};
                    //ent.SubscribersUrls.Add(rec);
                    //ent.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to insert subscriber tip url: guid={tipGuid}, tipId={tipId}, subscriberId={subscriberId}", ex);
            }
        }

        public List<PushTipTime> GetBroadcastSchedules()
        {
            try
            {
                List<PushTipTime> times = null;
                using (var ent = new BettingTipsEntities())
                {
                    times = ent.BroadcastSchedules.ToList()
                        .Select(b => new PushTipTime(b.Hour, b.Minute))
                        .ToList();
                }
                return times;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all broadcast schedules", ex);
            }
        }

        public void UpdateBroadcastSchedules(List<PushTipTime> times)
        {
            try
            {
                var tableName = "BroadcastSchedules";
                using (var ent = new BettingTipsEntities())
                {
                    ent.Database.ExecuteSqlCommand("TRUNCATE TABLE [" + tableName + "]");
                    foreach (var t  in times)
                    {
                        var rec = new BroadcastSchedule()
                        {
                            Hour = t.Hour,
                            Minute = t.Minute
                        };
                        ent.BroadcastSchedules.Add(rec);
                    }
                    ent.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying update broadcast schedules", ex);
            }
        }

        public void VisitSubscriberUrl(Guid tipGuid)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.SubscribersUrls_Visit(tipGuid);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to visit subscriber tip url: guid={tipGuid}", ex);
            }
        }
        public Persistance.BettingTips.Tip GetTipFromGuid(Guid tipGuid)
        {
            try
            {
                Persistance.BettingTips.Tip tip = null;
                using (var ent = new BettingTipsEntities())
                {
                    var tipId = ent.SubscribersUrls.Where(s => s.UrlGuid == tipGuid).FirstOrDefault()?.TipId;
                    if (tipId.HasValue)
                        tip = ent.Tips.Where(t => t.TipId == tipId.Value).FirstOrDefault();
                }
                return tip;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying get tip from guid={tipGuid}", ex);
            }
        }
        public void SetActiveTip(int tipId, bool isActive)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    var result = (from p in ent.Tips
                                  where p.TipId == tipId
                                  select p).SingleOrDefault();

                    result.Active = isActive;
                    ent.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying set active={isActive} to tipId={tipId}", ex);
            }
        }
        public bool VerifyRegistrationCode(int registrationId, long chatId)
        {
            try
            {
                var validCode = false;
                using (var ent = new BettingTipsEntities())
                {
                    var subscriber = (from p in ent.Subscribers
                                      where (p.Id == registrationId && p.SubscriberChatId == chatId) || (p.Id == registrationId && p.SubscriberChatId == null)
                                      select p).SingleOrDefault();
                    if (subscriber != null)
                        validCode = true;
                }
                return validCode;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to verify registrationId {registrationId} for chatId {chatId}", ex);
            }
        }
        public bool RegisterSubscriber(int registrationId, long chatId)
        {
            try
            {
                var succ = false;
                using (var ent = new BettingTipsEntities())
                {
                    var subscriber = (from p in ent.Subscribers
                                      where p.Id == registrationId
                                      select p).SingleOrDefault();
                    if (subscriber != null)
                    {
                        subscriber.SubscriberChatId = chatId;
                        ent.SaveChanges();
                        succ = true;
                    }
                }
                return succ;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to register client registrationId {registrationId} with chatId {chatId}", ex);
            }
        }

        public List<SubscribersUrl> DumpSubscribersUrl()
        {
            List<SubscribersUrl> urls = null;
            using (var ent = new BettingTipsEntities())
            {
                urls = ent.SubscribersUrls.ToList();
            }
            return urls;
        }

        public void DeleteSubscriber(int registrationId)
        {
            using (var ent = new BettingTipsEntities())
            {
                var subscriber = ent.Subscribers.Where(s => s.Id == registrationId).FirstOrDefault();
                if (subscriber != null)
                {
                    ent.Subscribers.Remove(subscriber);
                    ent.SaveChanges();
                }
            }
        }

        public void DeleteSubscriberUrlHistory(long subscriberId)
        {
            using (var ent = new BettingTipsEntities())
            {
                var utls = ent.SubscribersUrls.Where(u => u.SubscriberId == subscriberId);
                if (utls.Any())
                {
                    ent.SubscribersUrls.RemoveRange(utls);
                    ent.SaveChanges();
                }
            }
        }
        public List<SubscriberdUrl> GetSubscribersUrls()
        {
            try
            {
                List<SubscriberdUrl> urls = null;
                using (var ent = new BettingTipsEntities())
                {
                    urls = ent.SubscribersUrls.ToList()
                        .Select(s => new SubscriberdUrl(s.UrlId, s.SubscriberId, s.TipId, s.VisitUrlTS))
                        .ToList();
                }
                return urls;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all visited subscribers urls", ex);
            }
        }
        public List<SubscriberVisitedUrl> GetVisitedSubscribersUrls()
        {
            try
            {
                List<SubscriberVisitedUrl> subscribers = null;
                using (var ent = new BettingTipsEntities())
                {
                    var urls = ent.SubscribersUrls
                        .Where(u => u.PickedUpByChatServer == null && u.VisitUrlTS != null)
                        .ToList();
                    subscribers = urls.Select(s => new SubscriberVisitedUrl(s.UrlId, s.SubscriberId)).ToList();
                }
                return subscribers;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all visited subscribers urls", ex);
            }
        }
        public void UpdateVisitedUrlPickedByChatServer(int urlId)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    var result = (from p in ent.SubscribersUrls
                                  where p.UrlId == urlId
                                  select p).SingleOrDefault();

                    result.PickedUpByChatServer = DateTime.Now;
                    ent.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to Update visited url picked by chat server urlId={urlId}", ex);
            }
        }

        public int InsertSubscriber(string firstName, string lastName, string cell, string mail, string remarks)
        {
            try
            {
                var registrationNumber = 0;
                using (var ent = new BettingTipsEntities())
                {
                    var result = ent.Subscribers_Merge(null, null, firstName, lastName, cell, mail, DateTime.Now, remarks, null, null);
                    registrationNumber = result.First().Value;
                }
                return registrationNumber;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add new subscriber: firstName={firstName}, lastName={lastName}, cell={cell}, mail={mail}, remarks={remarks}", ex);
            }
        }
        public void UpdateSubscriber(int registrationId, long chatId, string firstName, string lastName, string cell, string mail, string remarks, DateTime? blockedDate, string blockageReason)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    ent.Subscribers_Merge(registrationId, chatId, firstName, lastName, cell, mail, DateTime.Now, remarks, blockedDate, blockageReason);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add new subscriber: chatId={chatId}, firstName={firstName}, lastName={lastName}, cell={cell}, mail={mail}, remarks={remarks}, blockedDate={blockedDate}, blockageReason={blockageReason},", ex);
            }
        }
        public Subscriber GetSubscriberByRegistarionId(int registrationId)
        {
            try
            {
                Subscriber subscriber = null;
                using (var ent = new BettingTipsEntities())
                {
                    subscriber = ent.Subscribers.FirstOrDefault(s => s.Id == registrationId);
                }
                return subscriber;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get subscriber: registrationId={registrationId}", ex);
            }
        }
        public Subscriber GetSubscriberByChatId(long chatId)
        {
            try
            {
                Subscriber subscriber = null;
                using (var ent = new BettingTipsEntities())
                {
                    subscriber = ent.Subscribers.FirstOrDefault(s => s.SubscriberChatId == chatId);
                }
                return subscriber;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get subscriber: chatId={chatId}", ex);
            }
        }
        public void SetButtonStatistics(BotButtonType button)
        {
            try
            {
                using (var ent = new BettingTipsEntities())
                {
                    var rec = (from p in ent.BotButtons
                                  where p.Id == (int)button
                                  select p).SingleOrDefault();

                    if (rec != null)
                    {
                        rec.Counter++;
                        ent.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to set bot button statistics for button={button}", ex);
            }
        }
        public List<BotButton> GetButtonStatistics()
        {
            try
            {
                List<BotButton> buttons = null;
                using (var ent = new BettingTipsEntities())
                {
                    buttons = ent.BotButtons.ToList();
                }
                return buttons;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get buttons statistics", ex);
            }
        }
    }
}