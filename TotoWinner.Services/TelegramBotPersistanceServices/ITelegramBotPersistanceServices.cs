﻿using System;
using System.Collections.Generic;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services.Persistance.BettingTips;

namespace TotoWinner.Services
{
    public interface ITelegramBotPersistanceServices
    {
        void InsertSubscriberTip(Guid tipGuid, int tipId, long subscriberId);
        void VisitSubscriberUrl(Guid tipGuid);
        Tip GetTipFromGuid(Guid tipGuid);
        List<Subscriber> GetSubscribers();
        void DeleteSubscriber(int registrationId);
        void DeleteSubscriberUrlHistory(long subscriberId);
        void SetActiveTip(int tipId, bool isActive);
        List<SubscriberVisitedUrl> GetVisitedSubscribersUrls();
        void UpdateVisitedUrlPickedByChatServer(int tipId);
        List<SubscribersUrl> DumpSubscribersUrl();
        bool RegisterSubscriber(int registrationId, long chatId);
        int InsertSubscriber(string firstName, string lastName, string cell, string mail, string remarks);
        void UpdateSubscriber(int registrationId, long chatId, string firstName, string lastName, string cell, string mail, string remarks, DateTime? blockedDate, string blockageReason);
        Subscriber GetSubscriberByRegistarionId(int registrationId);
        Subscriber GetSubscriberByChatId(long chatId);
        void UpdateBroadcastSchedules(List<PushTipTime> times);
        List<PushTipTime> GetBroadcastSchedules();
        void SetButtonStatistics(BotButtonType button);
        List<BotButton> GetButtonStatistics();
    }
}
