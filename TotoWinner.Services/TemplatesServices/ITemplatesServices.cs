﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public interface ITemplatesServices
    {
        int AddTemplate(Template template);
        void DeleteTemplate(Template template);
        List<Template> GetAllTemplate();
        Template GetTemplate(int templateId);

        void PrepareGroupsData(string groupPath);
        AnalyzeTemplateResult AnalyzeTemplate(string template);
        BacktestingDefinitions CreateBacktestingDefinitions(List<string> bPattern, List<string> dPattern);
        void TemplateWatcher(Template template, ProgramWithGames program, Dictionary<int, WatchTemplatesBetsResult> result);

        string ConvertTemaplteBetForToToBetForStr(string from);
    }
}
