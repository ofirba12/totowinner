﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Services.Algo;
using TotoWinner.Services.Persistance;

namespace TotoWinner.Services
{
    public class TemplatesServices : Singleton<TemplatesServices>, ITemplatesServices
    {
        private PersistanceServices _persistanceSrv = PersistanceServices.Instance;
        private const string groupsDataFilename = @"TWN-GROUPS.txt";
        private Dictionary<string, GroupData> groups = new Dictionary<string, GroupData>();
        public List<string> BankersGroupKeys { get; private set; } // { "A","B","C","D","E","F","G","H","I","J","K","L" };
        public List<string> DoublesGroupKeys { get; private set; }
        private BacktestingAlgo backtestingAlgo = new BacktestingAlgo();

        private TemplatesServices()
        {
            if (this.groups.Count == 0)
            {
                this.BankersGroupKeys = new List<string>();
                this.DoublesGroupKeys = new List<string>();
                var groupPath = HttpContext.Current != null
                    ? HttpContext.Current.Server.MapPath($"..\\..\\bin\\{groupsDataFilename}")
                    : groupsDataFilename;
                this.PrepareGroupsData(groupPath);
            }
        }
        public void TemplateWatcher(Template template, ProgramWithGames program,
            Dictionary<int, WatchTemplatesBetsResult> result)
        {
            AnalyzeTemplateResult analyzeTemplate = null;
            try
            {
                analyzeTemplate = TemplatesServices.Instance.AnalyzeTemplate(template.Value);
            }
            catch(Exception)
            {
                result.Add(template.Id, new WatchTemplatesBetsResult()
                {
                    BaseBets = null,
                    Status = TemplateBaseBetsStatus.InvalidTemplate
                });
                return;
            }
            var definitions = TemplatesServices.Instance.CreateBacktestingDefinitions(analyzeTemplate.BankerPattern,
                analyzeTemplate.DoublePattern);
            definitions.BankersLimits = new MinMaxLimits() { Min = analyzeTemplate.BankerSize, Max = analyzeTemplate.BankerSize };
            definitions.DoublesLimits = new MinMaxLimits() { Min = analyzeTemplate.DoubleSize, Max = analyzeTemplate.DoubleSize };
            var res = backtestingAlgo.GenerateBaseBets(program, definitions);
            if (res.Count == 0)
            {
                result.Add(template.Id, new WatchTemplatesBetsResult()
                {
                    BaseBets = null,
                    Status = TemplateBaseBetsStatus.NoBaseFound
                });
            }
            else
            {
                result.Add(template.Id, new WatchTemplatesBetsResult()
                {
                    BaseBets = res,
                    Status = TemplateBaseBetsStatus.Success
                });
            }
        }
        public AnalyzeTemplateResult AnalyzeTemplate(string template)
        {
            var bPattern = new List<string>();
            var dPattern = new List<string>();
            var bankerSize = 0;
            var doubleSize = 0;
            var splitTemplate = template.Split('-');
            foreach (var item in splitTemplate)
            {
                if (item.Length == 2)
                {
                    var groupKey = item.Substring(0, 1);
                    if (this.groups.ContainsKey(groupKey))
                    {
                        var limit = Convert.ToInt32(item.Substring(1));
                        bankerSize += limit;
                        bPattern.Add(item);
                    }
                    else
                    {
                        throw new Exception($"Template banker item {item} is invalid, Group key {groupKey} does not exists");
                    }
                }
                else if (item.Length == 3)
                {
                    var groupKey = item.Substring(0, 2);
                    if (this.groups.ContainsKey(groupKey))
                    {
                        var limit = Convert.ToInt32(item.Substring(2));
                        doubleSize += limit;
                        dPattern.Add(item);
                    }
                    else
                    {
                        throw new Exception($"Template double item {item} is invalid, Group key {groupKey} does not exists");
                    }
                }
                else
                {
                    throw new Exception($"Template Item {item} is invalid. Item length is {item.Length}, should be [2,3]");
                }
            }
            var result = new AnalyzeTemplateResult(bPattern, bankerSize, dPattern, doubleSize);
            return result;
        }
        public void PrepareGroupsData(string groupPath)
        {
            string line;            
            //System.IO.StreamReader file = new System.IO.StreamReader(groupsDataFilename);
            System.IO.StreamReader file = new System.IO.StreamReader(groupPath);
            while ((line = file.ReadLine()) != null)
            {
                if (line.StartsWith("ISIN"))
                    continue;
                string[] items = line.Split("\t".ToCharArray());
                switch (items[0].Length)
                {
                    case 1:
                        this.BankersGroupKeys.Add(items[0]);
                        this.groups.Add(items[0], new GroupData(items[1], items[2], items[3], items[4]));
                        break;
                    case 2:
                        this.DoublesGroupKeys.Add(items[0]);
                        this.groups.Add(items[0], new GroupData(items[1], items[2], items[3], items[4], items[5]));
                        break;
                }
            }
            file.Close();
        }

        public BacktestingDefinitions CreateBacktestingDefinitions(List<string> bPattern, List<string> dPattern)
        {
            var definitions = new BacktestingDefinitions();
            //work.Definitions.ProgramType = this.programType;
            //work.Definitions.FromDate = this.FromDate;
            //work.Definitions.ToDate = this.ToDate;
            //work.Definitions.BankersLimits = new MinMaxLimits() { Min = this.groupsRepository.BankersSize, Max = this.groupsRepository.BankersSize };
            //work.Definitions.DoublesLimits = new MinMaxLimits() { Min = this.groupsRepository.DoublesSize, Max = this.groupsRepository.DoublesSize };
            definitions.GamesDefinitions = new List<GamesDefinition>();
            foreach (var banker in bPattern)
            {
                var groupKey = banker.Substring(0, 1);
                var groupData = this.groups[groupKey];
                var limit = Convert.ToInt32(banker.Substring(1));
                definitions.GamesDefinitions.Add(new GamesDefinition()
                {
                    BetForType = ConvertTemaplteBetForToToBetForStr(groupData.GameType),
                    BetType = "רגיל",
                    Id = 1,
                    RateFrom = groupData.RangeFrom,
                    RateTo = groupData.RangeTo,
                    From = limit,
                    To = limit,
                    RateType = ConvertTemplateRateToRateStr(groupData.TemplateType),
                    RowType = BtGameDefinitionType.Banker
                });
            }
            foreach (var d in dPattern)
            {
                var groupKey = d.Substring(0, 2);
                var groupData = this.groups[groupKey];
                var limit = Convert.ToInt32(d.Substring(2));
                definitions.GamesDefinitions.Add(new GamesDefinition()
                {
                    BetForType = ConvertTemaplteBetForToToBetForStr(groupData.GameType),
                    BetType = ConvertTemplateDoubleToDoubleBetStr(groupData.DoubleShape),
                    Id = 1,
                    RateFrom = groupData.RangeFrom,
                    RateTo = groupData.RangeTo,
                    From = limit,
                    To = limit,
                    RateType = ConvertTemplateRateToRateStr(groupData.TemplateType),
                    RowType = BtGameDefinitionType.Double
                });
            }
            return definitions;
        }

        private string ConvertTemplateRateToRateStr(string from)
        {
            switch (from)
            {
                case "low":
                    return "נמוך";
                case "high":
                    return "גבוה";
                default:
                    throw new Exception($"{from} can not be converted to RateType in hebrew");
            }
        }
        public string ConvertTemaplteBetForToToBetForStr(string from)
        {
            switch (from)
            {
                case "home":
                    return "קבוצה בית";
                case "draw":
                    return "תיקו";
                case "away":
                    return "קבוצה חוץ";
                case "free":
                    return "חופשי";
                default:
                    throw new Exception($"{from} can not be converted to BtWinType in hebrew");
            }
        }
        private string ConvertTemplateDoubleToDoubleBetStr(string from)
        {
            switch (from)
            {
                case "1X":
                case "12":
                case "2X":
                    return from;
                case "free":
                    return "חופשי";
                default:
                    throw new Exception($"{from} can not be converted to BtDoubleBetType in hebrew");
            }
        }

        public int AddTemplate(Template template)
        {
            try
            {
                var id = _persistanceSrv.TemplateUpsert(template);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add template {template.ToString()}", ex);
            }

        }

        public void DeleteTemplate(Template template)
        {
            try
            {
                if (template.Id == 0)
                    throw new Exception("template id is not set");
                var dbServices = Services.PersistanceServices.Instance;
                dbServices.TemplateDelete(template.Id);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete template {template.ToString()}", ex);
            }
        }
        public List<Template> GetAllTemplate()
        {
            try
            {
                var templates = _persistanceSrv.TemplateGet(null);
                return templates;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all templates", ex);
            }

        }
        public Template GetTemplate(int templateId)
        {
            try
            {
                var template = _persistanceSrv.TemplateGet(templateId);
                return template.First();
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get template with id {templateId}", ex);
            }

        }
    }
}
