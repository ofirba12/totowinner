﻿using log4net;
using System;
using System.Collections.Generic;
using TotoWinner.Common.Infra;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class ShrinkDrillerServices : Singleton<ShrinkDrillerServices>, IShrinkDrillerServices
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private PersistanceServices _persistanceSrv = PersistanceServices.Instance;
        private ProgramServices _programSrv = ProgramServices.Instance;
        private const string templatesDataFilename = @"SHRINK-DRILLER-GROUPS.txt";
        public Dictionary<string, Dictionary<int, MinMax>> IsinIndexFromToRepository { get; }
        public Dictionary<string, DrillerShrinkTemplateType> IsinTypeRepository { get; }
        public Dictionary<string, ShrinkDrillerGradesGroup> IsinTypeGradesRepository { get; }

        private ShrinkDrillerServices()
        {
            this.IsinIndexFromToRepository = new Dictionary<string, Dictionary<int, MinMax>>();
            this.IsinTypeRepository = new Dictionary<string, DrillerShrinkTemplateType>();
            this.IsinTypeGradesRepository = new Dictionary<string, ShrinkDrillerGradesGroup>();
            this.GenerateTemplatesRepository();
        }

        private void GenerateTemplatesRepository()
        {
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(templatesDataFilename);
            while ((line = file.ReadLine()) != null)
            {
                if (line.StartsWith("ISIN"))
                    continue;
                string[] items = line.Split("\t".ToCharArray());
                var isin = items[0];
                Enum.TryParse(items[1], out DrillerShrinkTemplateType shrinkType);
                var firstFrom = Convert.ToInt32(items[2]);
                var firstTo = Convert.ToInt32(items[3]);
                var maxloop = Convert.ToInt32(items[4]);
                var counter = Convert.ToInt32(items[5]);
                var strongRange = items[6].Split("-".ToCharArray());
                var mediumRange = items[7].Split("-".ToCharArray());
                var lowRange = items[8].Split("-".ToCharArray());
                var from = firstFrom;
                var to = firstTo;
                for (var index = 1; index <= counter; index++)
                {
                    if (!this.IsinIndexFromToRepository.ContainsKey(isin))
                    {
                        this.IsinTypeRepository.Add(isin, shrinkType);
                        this.IsinIndexFromToRepository.Add(isin, new Dictionary<int, MinMax>());
                    }
                    this.IsinIndexFromToRepository[isin].Add(index, new MinMax(from, to));
                    if (!this.IsinTypeGradesRepository.ContainsKey(isin))
                    {
                        this.IsinTypeGradesRepository.Add(isin, new ShrinkDrillerGradesGroup(
                            new MinMax(Convert.ToInt32(strongRange[0]), Convert.ToInt32(strongRange[1])),
                            new MinMax(Convert.ToInt32(mediumRange[0]), Convert.ToInt32(mediumRange[1])),
                            new MinMax(Convert.ToInt32(lowRange[0]), Convert.ToInt32(lowRange[1])))
                        );
                    }

                    //}
                    //else
                    //{
                    //    this.IsinIndexFromToRepository[isin].Add(index, new MinMax(from, to));
                    //}
                    if (to < maxloop)
                    {
                        to++;
                        from++;
                    }
                    else
                    {
                        from = firstFrom;
                        to = ++firstTo;
                    }

                }
            }
            file.Close();
        }

        public bool TemplateExists(string templateKey)
        {
            var res = this.IsinTypeRepository.ContainsKey(templateKey);
            return res;
        }
    }
}
