﻿namespace TotoWinner.Services
{
    public interface IShrinkDrillerServices
    {
        bool TemplateExists(string templateKey);
    }
}
