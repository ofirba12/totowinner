﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public class ResultsToExcelSaver
    {
        public static string SaveToFile(IEnumerable<CombinedResult> combinedResultsToPrint, string excelFileName, string excelTemplatePath)
        {
            if (combinedResultsToPrint.Count() == 0) return null;


        //    string excelConfigFileName = "OutputTemplate.xlsx";
            var outputExcelConfig = new OutputExcelConfig(excelTemplatePath);


            if (outputExcelConfig == null) return null;
            IWorkbook wb = new XSSFWorkbook();
            int sheetId = 1;

            int currentResultInSheetId = 0;

            ISheet sheet = null;
            foreach (var combinedResult in combinedResultsToPrint)
            {
                if (sheet == null)
                {
                    sheet = wb.CreateSheet(sheetId.ToString());
                    SetSheetStyle(sheet, outputExcelConfig);
                    sheet.GetRow(0).GetCell(0).SetCellValue(sheetId);

                    sheetId++;
                }

                for (int resultId = 0; resultId < combinedResult.Result.Length; resultId++)
                {
                    var rowId = outputExcelConfig.AllowedRows[outputExcelConfig.AllowedRows.Length - (currentResultInSheetId * 3 + combinedResult.Result[resultId]) - 1];
                    var row = sheet.GetRow(rowId);

                    var cellId = outputExcelConfig.AllowedCols[outputExcelConfig.AllowedCols.Length - resultId - 1];
                    var cell = row.GetCell(cellId);

                    cell.SetCellValue("x");
                }

                if (currentResultInSheetId == 9)
                {
                    sheet = null;
                    currentResultInSheetId = 0;
                }
                else
                {
                    currentResultInSheetId++;
                }

            }

            var dirName = Path.GetDirectoryName(excelFileName);
            if (!string.IsNullOrWhiteSpace(dirName) && !Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);
            var file = File.OpenWrite(excelFileName);
            var fullFilename = file.Name;
            wb.Write(file);
            file.Close();
            return fullFilename;
        }

        private static void SetSheetStyle(ISheet sheet, OutputExcelConfig outputExcelConfig)
        {
            foreach (var columnData in outputExcelConfig.ColsWidths)
            {
                sheet.SetColumnWidth(columnData.Key, columnData.Value);
            }

            foreach (var rowData in outputExcelConfig.RowsHeights)
            {
                var newRow = sheet.CreateRow(rowData.Key);
                newRow.Height = rowData.Value;
            }

            var hFont = sheet.Workbook.CreateFont();

            hFont.FontHeightInPoints = 8;
            var hStyle = sheet.Workbook.CreateCellStyle();
            hStyle.SetFont(hFont);
            hStyle.Alignment = HorizontalAlignment.Center;
            hStyle.VerticalAlignment = VerticalAlignment.Center;

            foreach (var columnData in outputExcelConfig.ColsWidths)
            {
                foreach (var rowData in outputExcelConfig.RowsHeights)
                {
                    var row = sheet.GetRow(rowData.Key);
                    var cell = row.CreateCell(columnData.Key);

                    cell.CellStyle = hStyle;
                }
            }
        }
    }
}
