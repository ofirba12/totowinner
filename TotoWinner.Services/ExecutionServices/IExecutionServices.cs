﻿using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Services
{
    public interface IExecutionServices
    {
        int AddBacktestingExecution(BacktestingDefinitions backtestingDefinitions);
        List<ProgramBetData> FetchBacktestingBaseBets(int executionId, int programId, ProgramWithGames program);
        BacktestingResults GetBacktestingResults(int executionId, int fromIndex, bool useInMemoryPersistance = false);
        ExecutionState GetBacktestingExecutionStatus(int executionId);

        int AddOptimizerExecution(ExecutionOptimizerParameters parameters);
        void UpdateExecutionStatus<T>(int executionId, SystemType systemType, ExecutionStatus status, T results, bool useInMemoryPersistance = false);
        ExecutionState GetExecutionStatus(int executionId);
        void SetOptimizedBetResult(int executionId, OptimizedBet result);
        List<OptimizedBet> GetOptimizedBetResults(int executionId);
        void SetOptimizedConditionResult(int executionId, OptimizedCondition result);
        List<OptimizedCondition> GetOptimizedConditionResults(int executionId);
        void SetOptimizedAdvancedConditionResult(int executionId, OptimizedAdvancedCondition result);
        List<OptimizedAdvancedCondition> GetOptimizedAdvancedConditionResults(int executionId);
        SearchResults GetOptimizerExecutionShrinkResult(int executionId);
    }
}
