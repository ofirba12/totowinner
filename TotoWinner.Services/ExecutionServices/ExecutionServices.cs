﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Services.Persistance.TotoWinner;

namespace TotoWinner.Services
{
    public class ExecutionServices : Singleton<ExecutionServices>, IExecutionServices
    {
        private PersistanceServices _persistanceSrv = PersistanceServices.Instance;
        private InMemoryBacktestingPersistanceServices _inMemoryBtPersistanceSrv = InMemoryBacktestingPersistanceServices.Instance;

        private ProgramServices _programSrv = ProgramServices.Instance;
        private ExecutionServices() { }
        #region Backtesting
        public ExecutionState GetBacktestingExecutionStatus(int executionId)
        {
            try
            {
                var executionStatus = _persistanceSrv.ExecutionGet(executionId);
                var status = new ExecutionState(executionStatus.Status, null);
                return status;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get backtesting execution status: executionId: {executionId}", ex);
            }
        }

        public BacktestingResults GetBacktestingResults(int executionId, int fromIndex, bool useInMemoryPersistance = false)
        {
            try
            {
                var programResults = _persistanceSrv.GetBacktestingProgramResults(executionId, useInMemoryPersistance);
                Dictionary<int, Dictionary<short, int>> programsStrikesCounters = new Dictionary<int, Dictionary<short, int>>();
                foreach (var result in programResults)
                {
                    if (!programsStrikesCounters.ContainsKey(result.ProgramId))
                        programsStrikesCounters.Add(result.ProgramId, new Dictionary<short, int>());
                    if (!programsStrikesCounters[result.ProgramId].ContainsKey(result.BetStrike))
                        programsStrikesCounters[result.ProgramId].Add(result.BetStrike, 0);
                    programsStrikesCounters[result.ProgramId][result.BetStrike] = result.Counter;
                }

                var programs = new List<BacktestingProgramResult>();
                int[] totalsArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                var notValidProgramCounter = 0;
                var noBaseBetsFoundCounter = 0;
                var programsWithTooManyBetsCounter = 0;
                foreach (var programId in programsStrikesCounters.Keys)
                {
                    int[] counters = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    foreach (var strike in programsStrikesCounters[programId].Keys)
                    {
                        counters[strike] = programsStrikesCounters[programId][strike];
                    }
                    var programStatus = BtProgramResultStatus.OK;
                    for (var ind = 0; ind < counters.Length; ind++)
                    {
                        totalsArr[ind] += counters[ind] > 0? counters[ind] : 0; //When program is invalid last game has a minus as a flag
                        if (counters[ind] < 0)
                        {
                            switch (counters[ind])
                            {
                                case -1:
                                    programStatus = BtProgramResultStatus.InvalidProgram;
                                    break;
                                case -2:
                                    programStatus = BtProgramResultStatus.TooManyBets;
                                    break;
                            }
                        }
                    }
                    var program = _programSrv.GetPersistanceProgramsWithGamesData(new List<int>() { programId }, useInMemoryPersistance).First();
                    var baseBetsColl = _persistanceSrv.FetchBacktestingBaseBets(executionId, programId, useInMemoryPersistance);
                    //bool isInvalidProgram = program.Games.Values.Any(game => game.Validations.Count > 0);
                    //if (isInvalidProgram)
                    if (programStatus == BtProgramResultStatus.InvalidProgram)
                    {
                        programs.Add(new BacktestingProgramResult(program.Number, programId, "ת.ל.ת", program.EndDate, 0, 0, 0, 0, new List<int>(), 0));
                        notValidProgramCounter++;
                    }
                    else if (programStatus == BtProgramResultStatus.TooManyBets)
                    {
                        programs.Add(new BacktestingProgramResult(program.Number, programId, "גדול מ-15 מליון", program.EndDate, 0, 0, 0, 0, new List<int>(), 1));
                        //notValidProgramCounter++;
                        programsWithTooManyBetsCounter++;
                    }
                    else if (baseBetsColl.Count() == 0)
                    {
                        programs.Add(new BacktestingProgramResult(program.Number, programId, "אין טור", program.EndDate, 0, 0, 0, 0, new List<int>(), 0));
                        noBaseBetsFoundCounter++;
                    }
                    else
                    {
                        LookforBankersDoublesCounters(baseBetsColl, out int bankersCounter, out int doublesCounter, out int trippleCounter);
                        programs.Add(new BacktestingProgramResult(program.Number, programId, "", program.EndDate,
                            trippleCounter, doublesCounter, bankersCounter, counters.Sum(), counters.ToList(), 1));
                    }
                }
                var programWithBaseBetsCounter = programs.Count - noBaseBetsFoundCounter - notValidProgramCounter - programsWithTooManyBetsCounter;
                BacktestingTotalResult totals = new BacktestingTotalResult(totalsArr.Sum(), totalsArr.ToList(), programWithBaseBetsCounter);

                //programs.Add(new BacktestingProgramResult(191301, "", 2, 5, 10, 288, new List<int>() { 0, 0, 4, 4, 0, 0, 20, 80, 60, 50, 35, 25, 0, 1, 5, 3, 1 }));
                //programs.Add(new BacktestingProgramResult(191201, "", 5, 6, 4, 5184, new List<int>() { 30, 4, 17, 235, 180, 18, 900, 880, 262, 852, 953, 529, 239, 80, 5, 0, 0 }));
                //programs.Add(new BacktestingProgramResult(191101, "", 7, 0, 9, 2187, new List<int>() { 29, 15, 172, 25, 152, 125, 585, 529, 269, 256, 15, 8, 5, 0, 1, 1, 0 }));
                //programs.Add(new BacktestingProgramResult(191100, "אין טור", 0, 0, 0, 0, new List<int>()));
                //programs.Add(new BacktestingProgramResult(191099, "ת.ל.ת", 0, 0, 0, 0, new List<int>()));
                programs.RemoveRange(0, fromIndex);
                BacktestingSummaryResult summary = new BacktestingSummaryResult(programsStrikesCounters.Keys.Count, notValidProgramCounter, noBaseBetsFoundCounter);

                return new BacktestingResults(programs, totals, summary);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get backtesting results for execution {executionId}", ex);
            }
        }

        private void LookforBankersDoublesCounters(List<TWBacktestingBaseBet> baseBetsColl, out int bankersCounter, out int doublesCounter, out int trippleCounter)
        {
            bankersCounter = 0;
            doublesCounter = 0;
            trippleCounter = 0;
            foreach (var bet in baseBetsColl)
            {
                var sum = Convert.ToInt32(bet.Win_1) + Convert.ToInt32(bet.Win_X) + Convert.ToInt32(bet.Win_2);
                switch (sum)
                {
                    case 1:
                        bankersCounter++;
                        break;
                    case 2:
                        doublesCounter++;
                        break;
                    case 3:
                        trippleCounter++;
                        break;
                    default:
                        throw new Exception($"Illegal bets found for programId {bet.ProgramId}, {bet.Win_1},{bet.Win_X},{bet.Win_2}");
                }
            }
        }

        public int AddBacktestingExecution(BacktestingDefinitions backtestingDefinitions)
        {
            try
            {
                var json = JsonConvert.SerializeObject(backtestingDefinitions);
                var executionId = _persistanceSrv.ExecutionStatusUpsert(null, SystemType.Backtesting, ExecutionStatus.Queue, json);
                return executionId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add optimizer execution: backtestingDefinitions: {backtestingDefinitions.ToString()}", ex);
            }
        }

        public List<ProgramBetData> FetchBacktestingBaseBets(int executionId, int programId, ProgramWithGames program)
        {
            try
            {
                var coll = _persistanceSrv.FetchBacktestingBaseBets(executionId, programId);
                var baseBetData = new List<ProgramBetData>();
                foreach (var bet in coll)
                {
                    if (program.Games.TryGetValue(bet.GameIndex, out var orgGame))
                    {
                        baseBetData.Add(new ProgramBetData(
                            new List<float>() { (float)orgGame.Rate1, (float)orgGame.RateX, (float)orgGame.Rate2 },
                            new List<bool>() { bet.Win_1, bet.Win_X, bet.Win_2 }));
                    }
                    else
                        throw new Exception($"The specified game index {bet.GameIndex} was not found in program Id={program.Id}, number={program.Number}");
                }
                return baseBetData;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get backtesting base bets results: executionId={executionId} | programId={programId}", ex);
            }
        }
        #endregion

        #region Optimizer
        public int AddOptimizerExecution(ExecutionOptimizerParameters parameters)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                var jsonParams = JsonConvert.SerializeObject(parameters);
                var executionId = perSrv.ExecutionStatusUpsert(null, SystemType.Optimizer, ExecutionStatus.Queue, jsonParams);
                return executionId;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add optimizer execution: parameters: {parameters.ToString()}", ex);
            }
        }
        public ExecutionState GetExecutionStatus(int executionId)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                var executionStatus = perSrv.ExecutionGet(executionId);
                var bets = perSrv.ExecutionOptimizedBetsGet(executionId);
                var conditions = perSrv.ExecutionOptimizedConditionsGet(executionId);
                var advancedConditions = perSrv.ExecutionOptimizedAdvancedConditionsGet(executionId);
                var parentBetsCounter = 0;
                var shrinkBetsCounter = 0;
                if (executionStatus.Results != null)
                {
                    SearchResults results = JsonConvert.DeserializeObject<SearchResults>(executionStatus.Results);
                    parentBetsCounter = CountOptimizerFinalBets(bets);
                    shrinkBetsCounter = results.ResultsStatistics.Found;
                }
                var result = new ExecutionOptimizerData(bets,
                    conditions.Count == 0 ? null : conditions,
                    advancedConditions.Count == 0 ? null : advancedConditions,
                    parentBetsCounter, shrinkBetsCounter);
                var status = new ExecutionState(executionStatus.Status, result);
                return status;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get execution status: executionId: {executionId}", ex);
            }
        }

        private int CountOptimizerFinalBets(List<OptimizedBet> bets)
        {
            var trianglesCounter = 0;
            var doublesCounter = 0;
            foreach (var bet in bets)
            {
                if (bet.Bet.Trim().Length == 2)
                    doublesCounter++;
                else if (bet.Bet.Trim().Length == 3)
                    trianglesCounter++;
            }
            var betsCounterNumber = Math.Max(1, Math.Pow(3, trianglesCounter))
                * Math.Max(1, Math.Pow(2, doublesCounter));

            return Convert.ToInt32(betsCounterNumber);
        }

        public void SetOptimizedBetResult(int executionId, OptimizedBet result)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                perSrv.ExecutionOptimizedBetsUpsert(executionId, result.GameIndex, result.Bet, result.IsFinalBet);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to set optimized bet result: executionId: {executionId} |  optimizedBet: {result.ToString()}", ex);
            }
        }

        public List<OptimizedBet> GetOptimizedBetResults(int executionId)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                var results = perSrv.ExecutionOptimizedBetsGet(executionId);
                if (results.Count == 0)
                    return null;
                return results;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get optimized bet results: executionId: {executionId}", ex);
            }
        }

        public void SetOptimizedConditionResult(int executionId, OptimizedCondition result)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                perSrv.ExecutionOptimizedConditionUpsert(executionId, result.Title, result.Type,
                    Decimal.ToDouble(result.Condition.Min), Decimal.ToDouble(result.Condition.Max));
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to set optimized condition result: executionId: {executionId} |  optimizedCondition: {result.ToString()}", ex);
            }
        }
        public List<OptimizedCondition> GetOptimizedConditionResults(int executionId)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                var results = perSrv.ExecutionOptimizedConditionsGet(executionId);
                if (results.Count == 0)
                    return null;
                return results;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get optimized conditions results: executionId: {executionId}", ex);
            }
        }
        public void SetOptimizedAdvancedConditionResult(int executionId, OptimizedAdvancedCondition result)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                perSrv.ExecutionOptimizedAdvancedConditionUpsert(executionId, result.Type, result.IndexPerType,
                    Decimal.ToDouble(result.Condition.Min), Decimal.ToDouble(result.Condition.Max));
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to set optimized advanced condition result: executionId: {executionId} |  optimizedAdvancedCondition: {result.ToString()}", ex);
            }
        }
        public List<OptimizedAdvancedCondition> GetOptimizedAdvancedConditionResults(int executionId)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                var results = perSrv.ExecutionOptimizedAdvancedConditionsGet(executionId);
                if (results.Count == 0)
                    return null;
                return results;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get optimized advanced conditions results: executionId: {executionId}", ex);
            }
        }

        public SearchResults GetOptimizerExecutionShrinkResult(int executionId)
        {
            try
            {
                var perSrv = PersistanceServices.Instance;
                var execRecord = perSrv.ExecutionGet(executionId);
                var searchRes = execRecord.Results != null
                    ? JsonConvert.DeserializeObject<SearchResults>(execRecord.Results)
                    : null;
                return searchRes;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get optimized shrink results: executionId: {executionId}", ex);
            }
        }
        #endregion

        public void UpdateExecutionStatus<T>(int executionId, SystemType systemType, ExecutionStatus status, T results, bool useInMemoryPersistance = false)
        {
            try
            {
                if (useInMemoryPersistance)
                {
                    _inMemoryBtPersistanceSrv.UpdateExecutionStatus(executionId, status);
                }
                else
                {
                    var strRes = results != null ? JsonConvert.SerializeObject(results) : null;
                    _persistanceSrv.ExecutionStatusUpsert(executionId, systemType, status, null, strRes);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update execution status: executionId: {executionId} | status: {status}", ex);
            }
        }
    }
}
