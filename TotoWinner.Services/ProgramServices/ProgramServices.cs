﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using TotoWinner.Common;
using TotoWinner.Common.Infra;
using TotoWinner.Data;
using TotoWinner.Service;
using TotoWinner.Services.Persistance.TotoWinner;

namespace TotoWinner.Services
{
    public class ProgramServices : Singleton<ProgramServices>, IProgramServices
    {
        private PersistanceServices _persistanceSrv = PersistanceServices.Instance;
        private InMemoryBacktestingPersistanceServices _inMemoryBtPersistanceSrv = InMemoryBacktestingPersistanceServices.Instance;

        private ProgramServices() { }
        #region Persistance Programs
        public List<ProgramWithGames> GetPersistanceProgramsWithGamesData(ProgramType programType, DateTime fromEndDate, DateTime toEndDate,
            bool useInMemoryPersistance = false)
        {
            try
            {
                if (useInMemoryPersistance &&
                    _inMemoryBtPersistanceSrv.ProgramWithGamesRepository != null &&
                    _inMemoryBtPersistanceSrv.ProgramWithGamesRepository.Count > 0)
                    return _inMemoryBtPersistanceSrv.ProgramWithGamesRepository;

                var onlyPrograms = _persistanceSrv.GetProgramsData(programType, fromEndDate, toEndDate);
                var sortedPrograms = onlyPrograms.OrderBy(p => p.EndDate).ToList();
                var repository = PrepareProgramCollection(sortedPrograms);
                if (useInMemoryPersistance)
                    _inMemoryBtPersistanceSrv.AddProgramWithGamesRepository(repository);
                return repository;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to get persistance programs with games data for programType={programType} | fromEndDate={fromEndDate} | toEndDate={toEndDate}.", ex);
            }
        }

        public List<ProgramWithGames> GetPersistanceProgramsWithGamesDataByProgramNumber(List<int> programsNumbers)
        {
            try
            {
                var onlyPrograms = _persistanceSrv.GetProgramsDataByProgramNumbers(programsNumbers);
                var sortedPrograms = onlyPrograms.OrderBy(p => p.EndDate).ToList();
                var repository = PrepareProgramCollection(sortedPrograms);
                return repository;
            }
            catch (Exception ex)
            {
                var progInputStr = string.Join(",", programsNumbers);
                throw new Exception($"An error occured trying to get persistance programs with games data for program numbers {progInputStr}.", ex);
            }
        }
        public List<ProgramWithGames> GetPersistanceProgramsWithGamesData(List<int> programsIds, bool useInMemoryPersistance = false)
        {
            try
            {
                List<ProgramWithGames> repository = null;
                if (useInMemoryPersistance)
                {
                    repository = _inMemoryBtPersistanceSrv.ProgramWithGamesRepository.Where(i => programsIds.Contains(i.Id)).ToList();
                }
                else
                {
                    var onlyPrograms = _persistanceSrv.GetProgramsData(programsIds);
                    repository = PrepareProgramCollection(onlyPrograms);
                }
                return repository;
            }
            catch (Exception ex)
            {
                var progInputStr = string.Join(",", programsIds);
                throw new Exception($"An error occured trying to get persistance programs with games data for program Ids {progInputStr}.", ex);
            }
        }
        public List<ProgramWithGames> PrepareProgramCollection(List<Program> onlyPrograms)
        {
            var repository = new List<ProgramWithGames>();
            foreach (var program in onlyPrograms)
            {
                var games = _persistanceSrv.GetGames(program.Id);
                var validations = _persistanceSrv.GetGamesValidations(program.Id);
                var gvColl = new Dictionary<int, GameWithValidations>();
                foreach (var game in games)
                {
                    var gv = validations.Where(i => i.Index == game.Index).Select(v => (ProgramValidationStatus)v.Validation).ToList();
                    gvColl.Add(game.Index, new GameWithValidations(game.Index, game.Description, game.Rate1,
                        game.RateX, game.Rate2, game.Won, game.GameResult, game.OpenForBets, gv));
                }
                var item = new ProgramWithGames(program.Id, program.Number, program.EndDate, gvColl);
                repository.Add(item);
            }
            return repository;
        }

        public List<ProgramBetData> PrepareProgramBetsCollection(ProgramWithGames program)
        {
            try
            {
                var betsData = new List<ProgramBetData>();
                foreach (var game in program.Games.Values)
                {
                    List<bool> boolWin = null;
                    switch (game.Won)
                    {
                        case "1":
                            boolWin = new List<bool>() { true, false, false };
                            break;
                        case "X":
                            boolWin = new List<bool>() { false, true, false };
                            break;
                        case "2":
                            boolWin = new List<bool>() { false, false, true };
                            break;
                    }
                    betsData.Add(new ProgramBetData(
                        new List<float>() { (float)game.Rate1, (float)game.RateX, (float)game.Rate2 },
                        boolWin));
                }
                return betsData;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to prepare backtesting program bets data for program Id={program.Id} | number={program.Number}", ex);
            }
        }
        public void UpsertProgramData(ProgramDataResult games, ProgramType programType, DateTime endDate, int totoRoundId)
        {
            try
            {
                var programId = _persistanceSrv.ProgramUpsert(programType,
                    (short)endDate.Year,
                    totoRoundId,
                    endDate);
                UpsertGames(endDate, programType, games, programId);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to upsert program data, programType={programType}, endDate={endDate}, totoRoundId={totoRoundId}", ex);
            }
        }
        public void UpsertGames(DateTime endDate, ProgramType programType, ProgramDataResult games, int programId)
        {
            try
            {
                foreach (var game in games.Details.Keys)
                {
                    var item = games.Details[game];
                    char won = ' ';
                    var wonItem = item.Bets.Details.FirstOrDefault(i => i.Value.Won == true);
                    if (!wonItem.Equals(default(KeyValuePair<BetOutputType, BetDetails>)))
                    {
                        switch (wonItem.Key)
                        {
                            case BetOutputType.Bet1:
                                won = '1';
                                break;
                            case BetOutputType.BetX:
                                won = 'X';
                                break;
                            case BetOutputType.Bet2:
                                won = '2';
                                break;
                            default:
                                throw new Exception($"bet of type {wonItem.Key} is not supported");
                        }
                    }
                    _persistanceSrv.GameUpsert(programId, (byte)item.GameIndex,
                        item.GameTime,
                        item.GameName,
                        item.LeagueName,
                        item.GameResult,
                        item.Bets.OpenForBets,
                        item.BetColor,
                        item.Bets.Details[BetOutputType.Bet1].Rate,
                        item.Bets.Details[BetOutputType.BetX].Rate,
                        item.Bets.Details[BetOutputType.Bet2].Rate,
                        won != ' ' ? won.ToString() : null,
                        item.Validations.Keys.ToList());
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to upsert games of program programId={programId}, programType={programType}, endDate={endDate}", ex);
            }
        }

        #endregion

        #region Get External Programs
        public ProgramsResult GetAllPrograms()
        {
            try
            {
                Dictionary<ProgramUniqueId, ProgramMetaData> collection = new Dictionary<ProgramUniqueId, ProgramMetaData>();
                for (int fromYear = 2017; fromYear <= DateTime.Today.Date.Year; fromYear++)
                {
                    DateTime n = new DateTime(fromYear + 1, 1, 1);
                    var endDate = n.AddDays(-1);
                    var newCollection = GetProgramsByEndDate(endDate);
                    collection = collection.Concat(newCollection).ToDictionary(x => x.Key, x => x.Value);
                }
                return new ProgramsResult(collection);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to get all programs.", ex);
            }
        }
        public ProgramsResult GetAllLatestPrograms()
        {
            try
            {
                DateTime n = new DateTime(DateTime.Today.Date.Year + 1, 1, 1);
                var endDate = n.AddDays(-1);
                var collection = GetProgramsByEndDate(endDate);
                return new ProgramsResult(collection);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to get all programs.", ex);
            }
        }
        private Dictionary<ProgramUniqueId, ProgramMetaData> GetProgramsByEndDate(DateTime endDate)
        {
            try
            {
                Dictionary<ProgramUniqueId, ProgramMetaData> collection = new Dictionary<ProgramUniqueId, ProgramMetaData>();

                var win16Coll = GetPrograms(endDate, ProgramType.Winner16);
                collection = collection.Concat(win16Coll.Collection).ToDictionary(x => x.Key, x => x.Value);
                var winHalfColl = GetPrograms(endDate, ProgramType.WinnerHalf);
                collection = collection.Concat(winHalfColl.Collection).ToDictionary(x => x.Key, x => x.Value);
                var winWorldColl = GetPrograms(endDate, ProgramType.WinnerWorld);
                collection = collection.Concat(winWorldColl.Collection).ToDictionary(x => x.Key, x => x.Value);

                return collection;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to get programs by end date {endDate}.", ex);
            }
        }

        public ProgramsResult GetPrograms(DateTime programsEndDate, ProgramType programsType)
        {
            try
            {
                var pType = programsType == ProgramType.WinnerHalf ? ProgramType.Winner16 : programsType;
                var totoWinnerUrl = $"https://www.bankerim.co.il/api/totoGames/getPrograms?date={programsEndDate.ToString("yyyy-MM-dd")}&gameType={(int)pType}";
                WebRequest request = WebRequest.Create(totoWinnerUrl);
                WebResponse response = request.GetResponse();
                var jsonString = string.Empty;
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    jsonString = reader.ReadToEnd();
                }
                var details = new Dictionary<int, ProgramDetails>();
                var programs = WebApiInvoker.JsonDeserializeDynamicArray(jsonString);
                Dictionary<ProgramUniqueId, ProgramMetaData> collection = new Dictionary<ProgramUniqueId, ProgramMetaData>();
                foreach (var dynamicProgram in programs)
                {
                    dynamicProgram.TryGetValue("beginDate", out dynamic beginDateStr);
                    dynamicProgram.TryGetValue("endDate", out dynamic endDateStr);
                    dynamicProgram.TryGetValue("id", out dynamic idStr);
                    dynamicProgram.TryGetValue("toto_round_id", out dynamic totoRoundIdStr);
                    if (!Int32.TryParse(totoRoundIdStr, out int totoRoundId))
                        throw new Exception($"toto_round_id={totoRoundIdStr} could not be parsed as int");
                    var key = new ProgramUniqueId(totoRoundId, programsType);

                    if (!DateTime.TryParse(beginDateStr, out DateTime beginDate))
                        throw new Exception($"beginDate={beginDateStr} could not be parsed as date");
                    if (!DateTime.TryParse(endDateStr, out DateTime endDate))
                        throw new Exception($"endDate={endDateStr} could not be parsed as date");
                    if (!Int32.TryParse(idStr, out int id))
                        throw new Exception($"id={idStr} could not be parsed as int");

                    var data = new ProgramMetaData(beginDate, endDate, id);
                    collection.Add(key, data);
                }
                return new ProgramsResult(collection);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to get programs data for {programsType} | {programsEndDate}.", ex);
            }

        }
        public ProgramDataResult GetProgramData(DateTime programEndDate, ProgramType programType)
        {
            try
            {
                var totoWinnerUrl = $"https://www.bankerim.co.il/api/totoGames/getProgram?date={programEndDate.ToString("yyyy-MM-dd")}&gameType={(int)programType}";
                WebRequest request = WebRequest.Create(totoWinnerUrl);
                WebResponse response = request.GetResponse();
                var jsonString = string.Empty;
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    jsonString = reader.ReadToEnd();
                }
                var details = new Dictionary<int, ProgramDetails>();
                var programs = WebApiInvoker.JsonDeserializeDynamicArray(jsonString);
                foreach (var dynamicProgram in programs)
                {
                    dynamicProgram.TryGetValue("round", out dynamic roundData);
                    roundData.TryGetValue("place", out dynamic gameIndexDynamic);
                    dynamicProgram.TryGetValue("status", out dynamic statusData);
                    statusData.TryGetValue("color", out dynamic betStatus);
                    statusData.TryGetValue("description", out dynamic gameTime);
                    dynamicProgram.TryGetValue("league", out dynamic leagueData);
                    leagueData.TryGetValue("name", out dynamic leagueName);
                    dynamicProgram.TryGetValue("result", out dynamic gameResult);
                    dynamicProgram.TryGetValue("bet", out dynamic betData);
                    betData.TryGetValue("description", out dynamic betDescriptionData);
                    betDescriptionData.TryGetValue("value", out dynamic gameName);
                    betData.TryGetValue("openForBets", out dynamic openForBets);
                    fetchBets(betData, out double rateBet1, out bool wonBet1,
                        out double rateBetX, out bool wonBetX,
                        out double rateBet2, out bool wonBet2);
                    int gameIndex = Convert.ToInt32(gameIndexDynamic);
                    var betOpenForBets = Convert.ToBoolean(openForBets);
                    var betDetails = new Dictionary<BetOutputType, BetDetails>()
                    {
                        { BetOutputType.Bet1, new BetDetails(rateBet1, wonBet1) },
                        { BetOutputType.BetX, new BetDetails(rateBetX, wonBetX) },
                        { BetOutputType.Bet2, new BetDetails(rateBet2, wonBet2) },
                    };
                    BetsRepository bets = new BetsRepository(betOpenForBets, betDetails);
                    gameResult = gameResult ?? string.Empty;
                    details.Add(gameIndex, new ProgramDetails(gameIndex,
                        betStatus, gameTime, leagueName, gameName, gameResult,
                        bets));
                }
                ValidateAmendGamesData(details, programType);
                var finalResult = CalculateFinalResult(details, programEndDate);
                return new ProgramDataResult(details, finalResult);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to get program details for {programType} | {programEndDate}.", ex);
            }
        }

        private void fetchBets(dynamic betData, out double rateBet1, out bool wonBet1,
            out double rateBetX, out bool wonBetX,
            out double rateBet2, out bool wonBet2)
        {
            betData.TryGetValue("1", out dynamic bet1Data);
            bet1Data.TryGetValue("current", out dynamic dynamicRateBet1);
            bet1Data.TryGetValue("won", out dynamic dynamicWonBet1);
            rateBet1 = Convert.ToDouble(dynamicRateBet1);
            wonBet1 = Convert.ToBoolean(dynamicWonBet1);

            betData.TryGetValue("X", out dynamic betXData);
            betXData.TryGetValue("current", out dynamic dynamicRateBetX);
            betXData.TryGetValue("won", out dynamic dynamicWonBetX);
            rateBetX = Convert.ToDouble(dynamicRateBetX);
            wonBetX = Convert.ToBoolean(dynamicWonBetX);

            betData.TryGetValue("2", out dynamic bet2Data);
            bet2Data.TryGetValue("current", out dynamic dynamicRateBet2);
            bet2Data.TryGetValue("won", out dynamic dynamicWonBet2);
            rateBet2 = Convert.ToDouble(dynamicRateBet2);
            wonBet2 = Convert.ToBoolean(dynamicWonBet2);
        }

        private double? CalculateFinalResult(Dictionary<int, ProgramDetails> details, DateTime programEndDate)
        {
            try
            {
                double? finalResult = null;
                var isClosedProgram = details.All(item => item.Value.Bets.OpenForBets == false);
                if (isClosedProgram)
                {
                    finalResult = details.Sum(item =>
                    {
                        if (item.Value.Bets.OpenForBets == false)
                        {
                            var wonBet = item.Value.Bets.Details.FirstOrDefault(bet => bet.Value.Won == true);
                            if (wonBet.Value.Won == true)
                                return wonBet.Value.Rate;
                        }
                        return 0;
                    });
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to calculate final resultfor {programEndDate}.", ex);
            }
        }

        private void ValidateAmendGamesData(Dictionary<int, ProgramDetails> details, ProgramType programType)
        {
            var pipes = _persistanceSrv.GetRealTimeRatesPipe(programType);
            var numberOfGames = 0;
            switch (programType)
            {
                case ProgramType.Winner16:
                    numberOfGames = 16;
                    break;
                case ProgramType.WinnerHalf:
                    numberOfGames = 14;
                    break;
                case ProgramType.WinnerWorld:
                    numberOfGames = 15;
                    break;
            }
            var programIsOpen = details.Values.Any(p => p.Bets.OpenForBets == true);
            for (int i = 1; i <= numberOfGames; i++)
            {
                try
                {
                    if (!details.ContainsKey(i))
                    {
                        details.Add(i, new ProgramDetails(i, "black", "משחק דמה", "ליגה", "קבוצה א' - קבוצה ב'", "",
                            new BetsRepository(false,
                                new Dictionary<BetOutputType, BetDetails>()
                                {
                                { BetOutputType.Bet1, new BetDetails(1, false) },
                                { BetOutputType.BetX, new BetDetails(1, true) },
                                { BetOutputType.Bet2, new BetDetails(1, false) }
                                },
                                true)));
                        addValidation(details[i].Validations, ProgramValidationStatus.DummyGame);
                        addValidation(details[i].Validations, ProgramValidationStatus.GameResultMissing);
                    }
                    else
                    {
                        var noWinner = details[i].Bets.Details.Values.All(bet => bet.Won == false);
                        var noRates = details[i].Bets.Details.Values.All(bet => bet.Rate == 1);

                        if (!details[i].Bets.OpenForBets)
                        {
                            if (noWinner)
                            {
                                details[i].Bets.Details[BetOutputType.BetX].Won = true;
                                if (details[i].GameTime == "נדחה")
                                    addValidation(details[i].Validations, ProgramValidationStatus.GamePostponed);
                                else//Game Canceled
                                    addValidation(details[i].Validations, ProgramValidationStatus.NoWinnerFound);
                            }
                            if (noRates)
                            {
                                //                                details[i].Bets.Details[BetOutputType.Bet1].Won = true;
                                addValidation(details[i].Validations, ProgramValidationStatus.MissingRates);
                            }
                            if (string.IsNullOrEmpty(details[i].GameResult))
                                addValidation(details[i].Validations, ProgramValidationStatus.GameResultMissing);
                        }
                        else if (programIsOpen && noRates)
                        {
                            var overrideRates = pipes.First(r => r.GameIndex == i);
                            if (overrideRates.Rate1 + overrideRates.RateX + overrideRates.Rate2 > 3)
                            {
                                details[i].Bets.Details[BetOutputType.Bet1] = new BetDetails(overrideRates.Rate1, details[i].Bets.Details[BetOutputType.Bet1].Won);
                                details[i].Bets.Details[BetOutputType.BetX] = new BetDetails(overrideRates.RateX, details[i].Bets.Details[BetOutputType.BetX].Won);
                                details[i].Bets.Details[BetOutputType.Bet2] = new BetDetails(overrideRates.Rate2, details[i].Bets.Details[BetOutputType.Bet2].Won);
                                details[i].SetBetColor("blue");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"An error occured trying to fix game {i}.", ex);
                }
            }
        }

        private void addValidation(Dictionary<ProgramValidationStatus, string> validations, ProgramValidationStatus status)
        {
            try
            {
                if (validations.ContainsKey(ProgramValidationStatus.ValidForCalculation))
                    validations.Remove(ProgramValidationStatus.ValidForCalculation);
                if (!validations.ContainsKey(status))
                    validations.Add(status, status.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying add validation {status.ToString()}.", ex);
            }
        }
        #endregion 
    }
}
