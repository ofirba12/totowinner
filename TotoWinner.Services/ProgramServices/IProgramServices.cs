﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Service
{
    public interface IProgramServices
    {
        #region Persistance Programs
        List<ProgramWithGames> GetPersistanceProgramsWithGamesData(ProgramType programType, DateTime fromEndDate, DateTime toEndDate, bool useInMemoryPersistance = false);
        List<ProgramWithGames> GetPersistanceProgramsWithGamesDataByProgramNumber(List<int> programsNumbers);
        List<ProgramWithGames> GetPersistanceProgramsWithGamesData(List<int> programsIds, bool useInMemoryPersistance = false);
        List<ProgramBetData> PrepareProgramBetsCollection(ProgramWithGames program);
        void UpsertProgramData(ProgramDataResult games, ProgramType programType, DateTime endDate, int totoRoundId);
        void UpsertGames(DateTime endDate, ProgramType programType, ProgramDataResult games, int programId);
        #endregion

        #region Get External Programs
        ProgramDataResult GetProgramData(DateTime programEndDate, ProgramType programType);
        ProgramsResult GetPrograms(DateTime programsEndDate, ProgramType programsType);
        ProgramsResult GetAllPrograms();
        ProgramsResult GetAllLatestPrograms();
        #endregion
    }
}
