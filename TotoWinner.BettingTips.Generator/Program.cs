﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Generator
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private static BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;

        static void Main(string[] args)
        {
            _log.Info("BettingTips Generator activated");
            try
            {
                Initialize();
                var menu = new Menu().Show();
                Console.WriteLine($"Updating database with programs");
                _btTipsSrv.SetPrograms(new List<long>() { menu.RoundId }, menu.WinnerLineDate);
                Console.WriteLine($"Fetching Data from Bankerim...");
                var sportsToParse = new List<SportIds>() { SportIds.Soccer, SportIds.Basketball };
                var repository = _btTipsSrv.ParseWinnerLineResponse(menu.MarketData, menu.RoundId, menu.WinnerLineDate, false, sportsToParse);
                if (repository.SkippedEventsInfo.Count > 0)
                {
                    foreach (var warning in repository.SkippedEventsInfo)
                        _log.Warn(warning);
                }

                var allData = _btTipsSrv.CollectData(repository, 4);
                if (allData.Errors.Count > 0)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Attention: some events were skipped as bad data encounter, please refer to log");
                    foreach (var e in allData.Errors)
                        _log.Error(e);
                }
                Console.ForegroundColor = ConsoleColor.Green;
                _btPersistanceSrv.CsvDump(allData, repository);
                Console.WriteLine($"Pushing to database...");
                _btTipsSrv.PersistProgramAndTips(allData, repository);
                Console.WriteLine($"DONE");
                Console.WriteLine($"{Environment.NewLine}FinishTipGeneration set to true, update tips wharehouse is ready to start");
                _btTipsSrv.FinishTipGeneration();
                _log.Info("FinishTipGeneration set to true, update tips wharehouse is ready to start");

            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to activate BettingTips Generator", ex);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"{Environment.NewLine}{Environment.NewLine}Press any key to close");
                Console.ReadKey();
            }
        }
        private static void Initialize()
        {
            //Console.WriteLine("Clearing database...");
            //_btPersistanceSrv.EraseAllDatabase();
            Console.Write($"DONE{Environment.NewLine}");
            Console.ForegroundColor = ConsoleColor.Blue;

            if (!Directory.Exists(BettingTipsConstants.RepositoryPath))
                Directory.CreateDirectory(BettingTipsConstants.RepositoryPath);
            if (!Directory.Exists(BettingTipsConstants.ResultPath))
                Directory.CreateDirectory(BettingTipsConstants.ResultPath);
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1255"); //for hebrew
        }
    }
}
