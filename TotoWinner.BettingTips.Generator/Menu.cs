﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Generator
{
    internal class Menu
    {
        private BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;

        internal dynamic MarketData { get; private set; }
        internal DateTime WinnerLineDate { get; private set; }
        internal long RoundId { get; private set; }
        internal int Momentum { get; private set; }
        internal bool Verbose { get; private set; }
        internal List<long> ProgramIds { get; private set; }

        internal Menu Show()
        {
            var correct = true;
            do
            {
                correct = AskForWinnerLineDate();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("date chosen is not in an acceptable format!");
                }
            }
            while (!correct);
            Console.ForegroundColor = ConsoleColor.White;
            var winnerLineJson = _btTipsSrv.GetWinnerLineResponse(this.WinnerLineDate, false);
            this.MarketData = WebApiInvoker.JsonDeserializeDynamic(winnerLineJson);
            do
            {
                correct = AskForRoundId();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please choose as instruction below!");
                }
            }
            while (!correct);
            do
            {
                correct = AskForTotalMomentum();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please choose as instruction below!");
                }
            }
            while (!correct);
            do
            {
                correct = AskForVerbose();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please choose as instruction below!");
                }
            }
            while (!correct);
            Console.ForegroundColor = ConsoleColor.White;
            return this;
        }
        private bool AskForVerbose()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            this.Verbose = false;
            Console.ForegroundColor = ConsoleColor.White;
            return true;
        }
        private bool AskForTotalMomentum()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            this.Momentum = 4;
            Console.WriteLine($"Default Momentum Chosen: {this.Momentum}");
            return true;
        }
        private bool AskForWinnerLineDate()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Please choose Date (e.g 26-05-2021):");
            var input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                this.WinnerLineDate = DateTime.Today;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"Default Winner Line Date Chosen: {this.WinnerLineDate}");
                return true;
            }
            else
            {
                var enUS = new CultureInfo("en-US");
                if (DateTime.TryParseExact(input, "dd-MM-yyyy", enUS,
                                     DateTimeStyles.None, out var dateValue))
                {
                    this.WinnerLineDate = dateValue;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"Winner Line Date: {this.WinnerLineDate}");
                    return true;
                }
            }
            return false;
        }
        private bool AskForRoundId()
        {
            var dRounds = this.MarketData["rounds"];
            this.ProgramIds = new List<long>();
            Console.WriteLine("Rounds found:");
            var index = 1;
            var rounds = new Dictionary<long, int>(); //#roundid, #games
            var roundsMenu = new Dictionary<int, long>(); //#menu index, #roundid
            var okToAddRounds = new List<long>(); // #roundid
            Console.ForegroundColor = ConsoleColor.Blue;
            var allProgram = _btTipsSrv.GetAllPrograms();
            foreach (var dRound in dRounds)
            {
                var id = BettingTipsServicesHelpers.DynamicResolver<long>(dRound, null, "id", ResolveEnum.LONG);
                var games = BettingTipsServicesHelpers.DynamicResolver<int>(dRound, null, "games", ResolveEnum.INT);
                var okToAdd = allProgram.Any(p => p.ProgramId == id && p.ProgramDate == this.WinnerLineDate) ||
                    allProgram.All(p => p.ProgramId != id);
                var okToAddStr = "[EXISTS]";
                if (okToAdd)
                {
                    okToAddStr = "[OK to add]";
                    okToAddRounds.Add(id);
                }
                Console.WriteLine($"{index}. {id} has {games} games {okToAddStr}");
                roundsMenu.Add(index, id);
                rounds.Add(id, games);
                if (okToAdd)
                    this.ProgramIds.Add(id);
                index++;
            }
            Console.WriteLine($"Please choose round (1-{index-1}):");
            var input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                var maxGames = rounds.Max(g => g.Value);
                var roundChosen = rounds.Where(r => r.Value == maxGames).First().Key;
                this.RoundId = roundChosen;
                Console.WriteLine($"Chosen Round Id: {roundChosen} with {maxGames} games");
                if (!okToAddRounds.Contains(roundChosen))
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Can not add existing program {roundChosen}, please choose [OK to add] Only");
                    return false;
                }
                return true;
            }
            else if (int.TryParse(input, out int roundIndexChosen) && roundIndexChosen >= 1 && roundIndexChosen < index)
            {
                this.RoundId = roundsMenu[roundIndexChosen];
                Console.WriteLine($"Chosen Round Id: {roundsMenu[roundIndexChosen]} with {rounds[this.RoundId]} games");
                if (!okToAddRounds.Contains(roundsMenu[roundIndexChosen]))
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Can not add existing program {roundsMenu[roundIndexChosen]}, please choose [OK to add] Only");
                    return false;
                }

                return true;
            }

            Console.ForegroundColor = ConsoleColor.White;
            return false;
        }
    }
}
