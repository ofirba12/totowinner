﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.DataMonitor
{
    public class AllEvents
    {
        internal Dictionary<EventKey, MainEvent> MainEvents = new Dictionary<EventKey, MainEvent>();
        internal Dictionary<SportEnum, List<Tuple<int, int>>> MarketTypeIdsPeriodsIds = new Dictionary<SportEnum, List<Tuple<int, int>>>();
        internal List<SportEventData> AllEventsData = new List<SportEventData>();
        internal List<SummaryEvent> Summary { get; private set; }
        internal SummaryEventData SummaryEventData { get; set; }

        public AllEvents()
        {
            this.Summary = new List<SummaryEvent>();
            this.MarketTypeIdsPeriodsIds.Add(SportEnum.Soccer, new List<Tuple<int, int>>()
            {
                //100 - 90Min, 102 - "First Half", 955 -?
                new Tuple<int, int>(1, 100),//1X2
                new Tuple<int, int>(110123, 100),//הימור יתרון
                new Tuple<int, int>(110100, 100),//סך הכל שערים
                new Tuple<int, int>(3007, 100),//מעל מתחת שערים
                new Tuple<int, int>(13002, 100),// "סה\"כ קרנות טווחים"
                new Tuple<int, int>(13002, 102),// "סה\"כ קרנות טווחים"
                new Tuple<int, int>(110124, 100),// "יותר קרנות עם יתרון"
                new Tuple<int, int>(28, 100),// "תוצאה מדויקת"
                new Tuple<int, int>(28, 102),// "תוצאה מדויקת"
                new Tuple<int, int>(27, 100),// "מחצית/סיום" TBD
                new Tuple<int, int>(29, 100),// "שער ראשון במשחק"            
                new Tuple<int, int>(1, 102),//מחצית ראשונה
                new Tuple<int, int>(1, 103),//מחצית שניה
                new Tuple<int, int>(3007, 102),//מעל מתחת 1.5 שעריים מחצית
                new Tuple<int, int>(13005, 100),//מעל מתחת 9.5 קרנות
                new Tuple<int, int>(110131, 100),//מאצאפ
                new Tuple<int, int>(481, 100),//שער ראשון במשחק
                new Tuple<int, int>(1000, 100),//באיזו מחצית יהיו יותר שערים
                new Tuple<int, int>(350, 100),//האם כל קבוצה תקביע
                new Tuple<int, int>(110115, 100),//קבוצת חוץ תשמור על שער נקי
                new Tuple<int, int>(110114, 100),//קבוצת בית תשמור על שער נקי
                new Tuple<int, int>(100000020, 955),//איזו קבוצה תקביע עד דקה 15
                new Tuple<int, int>(100000025, 955),//מעל מתחת 0.5 שערים עד דקה 15
                new Tuple<int, int>(100000024, 955),//קרנות עד דקה 15
                new Tuple<int, int>(100000023, 100),//ראשונה ל-3 קרנות
                new Tuple<int, int>(100000065, 100),//ראשונה ל-5 קרנות
                new Tuple<int, int>(13750, 100),//קרן ראשונה לאחד
            });
            this.MarketTypeIdsPeriodsIds.Add(SportEnum.Basketball, new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(110121, 1195),//אבא הימור יתרון תוספת זוגי
                new Tuple<int, int>(110121, 213),//הימור יתרון רבע תוספת זוגי
                new Tuple<int, int>(110121, 211),//הימור יתרון מחצית ראשונה תוספת זוגי
                new Tuple<int, int>(110095, 1195),//מעל מתחת נקודות תוספת זוגי
                new Tuple<int, int>(3059, 209),//מנצחת סוף משחק כולל הארכה
                new Tuple<int, int>(15, 1195),//מעל מתחת נקודות קבוצה בית
                //new Tuple<int, int>(15, 1195),//מעל מתחת נקודות קבוצה חוץ /////בעייתי לדבר עם בן
                new Tuple<int, int>(15, 1195),//מעל מתחת נקודות קבוצה חוץ
                new Tuple<int, int>(15, 211),//מעל מתחת נקודות מחצית
                new Tuple<int, int>(110094, 209),//סל ראשון
                new Tuple<int, int>(1253, 209),//ראשונה ל-10 נקודות
                new Tuple<int, int>(1254, 209),//ראשונה ל-20 נקודות
                new Tuple<int, int>(110092, 209),//מעל מתחת שלשות
                new Tuple<int, int>(110128, 209),//מאצאפ שחקנים
                new Tuple<int, int>(13920, 1195),//רבע עם סך נקודות הכי גבוה
                new Tuple<int, int>(13810, 1195),//מחצית/סיום
                new Tuple<int, int>(1257, 209),//אבא הימור עם תוספת אי זוגי
                new Tuple<int, int>(1257, 213),//הימוררבע אי זוגי
                new Tuple<int, int>(1257, 211),//הימור מחצית אי זוגי
                new Tuple<int, int>(15, 209),//מעל מתחת נקודות אי זוגי  
                new Tuple<int, int>(15, 213),//מעל מתחת נקודות רבע אי זוגי  
                new Tuple<int, int>(15, 211),//מעל מתחת נקודות מחצית אי זוגי  
            });
            this.MarketTypeIdsPeriodsIds.Add(SportEnum.Tennis, new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(68, 113),//אבא
                new Tuple<int, int>(67, 113),//יתרון משחוקנים לשחקן
                new Tuple<int, int>(69, 113),//מעל מתחת משחקונים
                new Tuple<int, int>(110101, 113),//תוצאה מדוייקת מערכות
                new Tuple<int, int>(10047, 114),//תוצאה מדויקת מערכה ראשונה
            });
            this.MarketTypeIdsPeriodsIds.Add(SportEnum.Handball, new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(232, 1131),//אבא עם הימור יתרון
                new Tuple<int, int>(233, 1131),//מעל מתחת שערים
                new Tuple<int, int>(232, 140),//הימור יתרון מחצית
                new Tuple<int, int>(233, 140),//מעל מתחת שערים מחצית
            });
            this.MarketTypeIdsPeriodsIds.Add(SportEnum.Football, new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(1103, 119),//אבא עם הימור יתרון
                new Tuple<int, int>(10, 119),//מעל מתחת נקודות
                new Tuple<int, int>(11, 119),//מנצחת בסוף משחק
                new Tuple<int, int>(13930, 120),//רבע עם הכי הרבה נקודות
                new Tuple<int, int>(13820, 120),//מחצית/סיום
            });
        }

        public void Clear()
        {
            this.MainEvents.Clear();
            this.AllEventsData.Clear();
            this.Summary.Clear();
        }
    }
}
