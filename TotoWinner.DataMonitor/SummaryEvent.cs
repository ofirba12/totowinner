﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.DataMonitor
{
    public class SummaryEvent : MainEvent
    {
        public string SportType { get; set; }

        public SummaryEvent(MainEvent evnt)
        {
            this.CountryName = evnt.CountryName;
            this.Description = evnt.Description;
            this.EventDate = evnt.EventDate;
            this.EventId = evnt.EventId;
            this.LaunderResult = evnt.LaunderResult;
            this.MarketTypeId = evnt.MarketTypeId;
            this.MarketTypeName = evnt.MarketTypeName;
            this.OrdinalPositionTitles = evnt.OrdinalPositionTitles;
            this.PeriodTypeName = evnt.PeriodTypeName;
            this.Rates = evnt.Rates;
            this.LeagueName = evnt.LeagueName;
            this.SportId = evnt.SportId;
            this.PeriodId = evnt.PeriodId;
        }
    }
}
