﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.DataMonitor
{
    public class MainEvent 
    {
        public SportEnum SportId { get; set; }
        public string CountryName { get; set; }
        public string LeagueName { get; set; }
        public string Description { get; set; }
        public DateTime EventDate { get; set; }
        public decimal LaunderResult { get; set; }
        public int EventId { get; set; }
        public int MarketTypeId { get; set; }
        public string MarketTypeName { get; set; }
        public int PeriodId { get; set; }
        public string PeriodTypeName { get; set; }
        public Dictionary<string, decimal> Rates { get; set; }
        public Dictionary<int, string> OrdinalPositionTitles { get; set; }
        public Dictionary<int, int> OrdinalPositionOutcomeId { get; set; }
        public Dictionary<string, string> FieldNameSyntheticData { get; set; }
        public Dictionary<int, Dictionary<string, string>> RobotIdSpecialPicks { get; set; }
}
}
