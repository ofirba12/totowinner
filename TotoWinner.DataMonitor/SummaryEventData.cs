﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TotoWinner.DataMonitor
{
    internal class SummaryEventData : IDisposable
    {
        public System.Windows.Forms.TabPage TabPage { get; private set; }
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn SportId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SportType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn League;
        private System.Windows.Forms.DataGridViewTextBoxColumn BetName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BetDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BetTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Formula;
        internal SummaryEventData(IEnumerable<SummaryEvent> events)
        {
            events = events.OrderBy(i => i.LaunderResult);
            InitializeComponent();
            foreach (var data in events)
            {
                var row = (DataGridViewRow)this.dataGridView.Rows[0].Clone();
                row.Cells[0].Value = data.SportId;
                row.Cells[1].Value = data.SportType;
                row.Cells[2].Value = data.CountryName;
                row.Cells[3].Value = data.LeagueName;
                row.Cells[4].Value = data.Description;
                row.Cells[5].Value = data.EventDate.Date.ToString("d");
                row.Cells[6].Value = data.EventDate.ToString("HH:mm");
                row.Cells[7].Value = data.LaunderResult.ToString("#.####");
                this.dataGridView.Rows.Add(row);
            }

        }



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabPage = new System.Windows.Forms.TabPage();
            this.dataGridView = new System.Windows.Forms.DataGridView();

            this.SportId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SportType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.League = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BetName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BetDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BetTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Formula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            // 
            // tabPage1
            // 
            this.TabPage.Controls.Add(this.dataGridView);
            this.TabPage.Location = new System.Drawing.Point(4, 22);
            this.TabPage.Name = "SummaryEventData";
            this.TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage.Size = new System.Drawing.Size(1051, 648);
            this.TabPage.TabIndex = 0;
            this.TabPage.Text = "SummaryEventData";
            this.TabPage.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                this.SportId,
                this.SportType,
                this.Country,
                this.League,
                this.BetName,
                this.BetDate,
                this.BetTime,
                this.Formula});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 3);
            this.dataGridView.Name = "dataGridView1";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(1045, 642);
            this.dataGridView.TabIndex = 0;
            // 
            // Sport
            // 
            this.SportId.HeaderText = "Sport";
            this.SportId.Name = "Sport";
            this.SportId.ReadOnly = true;
            // 
            // Type
            // 
            this.SportType.HeaderText = "Type";
            this.SportType.Name = "Type";
            this.SportType.ReadOnly = true;
            // 
            // Country
            // 
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            // 
            // League
            // 
            this.League.HeaderText = "League";
            this.League.Name = "League";
            this.League.ReadOnly = true;
            // 
            // BetName
            // 
            this.BetName.HeaderText = "Bet Name";
            this.BetName.Name = "BetName";
            this.BetName.ReadOnly = true;
            // 
            // BetDate
            // 
            this.BetDate.HeaderText = "Bet Date";
            this.BetDate.Name = "BetDate";
            this.BetDate.ReadOnly = true;
            // 
            // BetTime
            // 
            this.BetTime.HeaderText = "Bet Time";
            this.BetTime.Name = "BetTime";
            this.BetTime.ReadOnly = true;
            // 
            // Formula
            // 
            this.Formula.HeaderText = "Formula";
            this.Formula.Name = "Formula";
            this.Formula.ReadOnly = true;
            // 
            // Monitor
            // 
            this.TabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
        }

        #endregion
        public void Dispose()
        {
            foreach (var column in this.dataGridView.Columns)
            {
                var obj = column as DataGridViewColumn;
                obj.Dispose();
            }
            this.dataGridView.Columns.Clear();
            this.dataGridView.Dispose();
            this.TabPage.Controls.Clear();
        }
    }
}
