﻿using System.Linq;

namespace TotoWinner.DataMonitor
{
    partial class Monitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.buttonMonitor = new System.Windows.Forms.Button();
            this.checkBoxFootball = new System.Windows.Forms.CheckBox();
            this.checkBoxBasketball = new System.Windows.Forms.CheckBox();
            this.buttonApplyFilter = new System.Windows.Forms.Button();
            this.checkBoxTennis = new System.Windows.Forms.CheckBox();
            this.checkBoxHandball = new System.Windows.Forms.CheckBox();
            this.checkBoxSoccer = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelLastUpdate = new System.Windows.Forms.Label();
            this.labelLastUpdateDate = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1077, 651);
            this.tabControl1.TabIndex = 0;
            // 
            // buttonMonitor
            // 
            this.buttonMonitor.Location = new System.Drawing.Point(1083, 68);
            this.buttonMonitor.Name = "buttonMonitor";
            this.buttonMonitor.Size = new System.Drawing.Size(121, 23);
            this.buttonMonitor.TabIndex = 14;
            this.buttonMonitor.Text = "Start Monitor";
            this.buttonMonitor.UseVisualStyleBackColor = true;
            this.buttonMonitor.Click += new System.EventHandler(this.buttonMonitor_Click);
            // 
            // checkBoxFootball
            // 
            this.checkBoxFootball.AutoSize = true;
            this.checkBoxFootball.Checked = true;
            this.checkBoxFootball.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFootball.Location = new System.Drawing.Point(1086, 202);
            this.checkBoxFootball.Name = "checkBoxFootball";
            this.checkBoxFootball.Size = new System.Drawing.Size(63, 17);
            this.checkBoxFootball.TabIndex = 15;
            this.checkBoxFootball.Text = "Football";
            this.checkBoxFootball.UseVisualStyleBackColor = true;
            this.checkBoxFootball.CheckedChanged += new System.EventHandler(this.checkBoxFootball_CheckedChanged);
            // 
            // checkBoxBasketball
            // 
            this.checkBoxBasketball.AutoSize = true;
            this.checkBoxBasketball.Checked = true;
            this.checkBoxBasketball.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBasketball.Location = new System.Drawing.Point(1087, 133);
            this.checkBoxBasketball.Name = "checkBoxBasketball";
            this.checkBoxBasketball.Size = new System.Drawing.Size(75, 17);
            this.checkBoxBasketball.TabIndex = 16;
            this.checkBoxBasketball.Text = "Basketball";
            this.checkBoxBasketball.UseVisualStyleBackColor = true;
            this.checkBoxBasketball.CheckedChanged += new System.EventHandler(this.checkBoxBasketball_CheckedChanged);
            // 
            // buttonApplyFilter
            // 
            this.buttonApplyFilter.Location = new System.Drawing.Point(1086, 226);
            this.buttonApplyFilter.Name = "buttonApplyFilter";
            this.buttonApplyFilter.Size = new System.Drawing.Size(75, 23);
            this.buttonApplyFilter.TabIndex = 17;
            this.buttonApplyFilter.Text = "Apply";
            this.buttonApplyFilter.UseVisualStyleBackColor = true;
            this.buttonApplyFilter.Click += new System.EventHandler(this.buttonApplyFilter_Click);
            // 
            // checkBoxTennis
            // 
            this.checkBoxTennis.AutoSize = true;
            this.checkBoxTennis.Checked = true;
            this.checkBoxTennis.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTennis.Location = new System.Drawing.Point(1087, 156);
            this.checkBoxTennis.Name = "checkBoxTennis";
            this.checkBoxTennis.Size = new System.Drawing.Size(58, 17);
            this.checkBoxTennis.TabIndex = 18;
            this.checkBoxTennis.Text = "Tennis";
            this.checkBoxTennis.UseVisualStyleBackColor = true;
            this.checkBoxTennis.CheckedChanged += new System.EventHandler(this.checkBoxTennis_CheckedChanged);
            // 
            // checkBoxHandball
            // 
            this.checkBoxHandball.AutoSize = true;
            this.checkBoxHandball.Checked = true;
            this.checkBoxHandball.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHandball.Location = new System.Drawing.Point(1087, 179);
            this.checkBoxHandball.Name = "checkBoxHandball";
            this.checkBoxHandball.Size = new System.Drawing.Size(68, 17);
            this.checkBoxHandball.TabIndex = 19;
            this.checkBoxHandball.Text = "Handball";
            this.checkBoxHandball.UseVisualStyleBackColor = true;
            this.checkBoxHandball.CheckedChanged += new System.EventHandler(this.checkBoxHandball_CheckedChanged);
            // 
            // checkBoxSoccer
            // 
            this.checkBoxSoccer.AutoSize = true;
            this.checkBoxSoccer.Checked = true;
            this.checkBoxSoccer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSoccer.Location = new System.Drawing.Point(1087, 110);
            this.checkBoxSoccer.Name = "checkBoxSoccer";
            this.checkBoxSoccer.Size = new System.Drawing.Size(60, 17);
            this.checkBoxSoccer.TabIndex = 20;
            this.checkBoxSoccer.Text = "Soccer";
            this.checkBoxSoccer.UseVisualStyleBackColor = true;
            this.checkBoxSoccer.CheckedChanged += new System.EventHandler(this.checkBoxSoccer_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelLastUpdate
            // 
            this.labelLastUpdate.AutoSize = true;
            this.labelLastUpdate.Location = new System.Drawing.Point(1087, 13);
            this.labelLastUpdate.Name = "labelLastUpdate";
            this.labelLastUpdate.Size = new System.Drawing.Size(65, 13);
            this.labelLastUpdate.TabIndex = 21;
            this.labelLastUpdate.Text = "Last Update";
            // 
            // labelLastUpdateDate
            // 
            this.labelLastUpdateDate.AutoSize = true;
            this.labelLastUpdateDate.Location = new System.Drawing.Point(1087, 41);
            this.labelLastUpdateDate.Name = "labelLastUpdateDate";
            this.labelLastUpdateDate.Size = new System.Drawing.Size(0, 13);
            this.labelLastUpdateDate.TabIndex = 22;
            // 
            // Monitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1235, 663);
            this.Controls.Add(this.labelLastUpdateDate);
            this.Controls.Add(this.labelLastUpdate);
            this.Controls.Add(this.checkBoxSoccer);
            this.Controls.Add(this.checkBoxHandball);
            this.Controls.Add(this.checkBoxTennis);
            this.Controls.Add(this.buttonApplyFilter);
            this.Controls.Add(this.checkBoxBasketball);
            this.Controls.Add(this.checkBoxFootball);
            this.Controls.Add(this.buttonMonitor);
            this.Controls.Add(this.tabControl1);
            this.Name = "Monitor";
            this.Text = "Monitor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button buttonMonitor;
        private System.Windows.Forms.CheckBox checkBoxFootball;
        private System.Windows.Forms.CheckBox checkBoxBasketball;
        private System.Windows.Forms.Button buttonApplyFilter;
        private System.Windows.Forms.CheckBox checkBoxTennis;
        private System.Windows.Forms.CheckBox checkBoxHandball;
        private System.Windows.Forms.CheckBox checkBoxSoccer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelLastUpdate;
        private System.Windows.Forms.Label labelLastUpdateDate;
    }
}

