﻿using System.Collections.Generic;

namespace TotoWinner.DataMonitor
{
    internal class RobotAlertKey : IEqualityComparer<RobotAlertKey>
    {
        public int RobotId { get; }
        public EventKey Event { get; }
        public RobotAlertKey(int robotId, EventKey eventKey)
        {
            this.RobotId = robotId;
            this.Event = eventKey;
        }
        public bool Equals(RobotAlertKey x, RobotAlertKey y)
        {
            return (x.RobotId == y.RobotId &&
                x.Event.Equals(y.Event));
        }

        public int GetHashCode(RobotAlertKey obj)
        {
            return this.Event.GetHashCode() * this.RobotId;
        }
        public override bool Equals(object obj)
        {
            return this.Equals(this, (RobotAlertKey)obj);
        }
        public override int GetHashCode()
        {
            return this.GetHashCode(this);
        }
    }
}
