﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.DataMonitor
{
    internal class EventKey : IEqualityComparer<EventKey>
    {
        public int EventId { get; }
        public SportEnum SportId { get; }
        public int MarketTypeId { get; }
        public int PeriodId { get; }
        public EventKey(SportEnum sportId,int eventId, int marketTypeId, int periodId)
        {
            this.SportId = sportId;
            this.EventId = eventId;
            this.MarketTypeId = marketTypeId;
            this.PeriodId = periodId;
        }
        public bool Equals(EventKey x, EventKey y)
        {
            return (x.EventId == y.EventId &&
                x.MarketTypeId == y.MarketTypeId &&
                x.PeriodId == y.PeriodId &&
                x.SportId == y.SportId);
        }

        public int GetHashCode(EventKey obj)
        {
            return this.EventId.GetHashCode() ^ (this.MarketTypeId + this.PeriodId + (int)this.SportId);
        }
        public override bool Equals(object obj)
        {
            return this.Equals(this, (EventKey)obj);
        }
        public override int GetHashCode()
        {
            return this.GetHashCode(this);
        }
        public override string ToString()
        {
            return $"EventId={this.EventId} SportId={this.SportId} MarketTypeId={this.MarketTypeId} PeriodId={this.PeriodId}";
        }
    }
}
