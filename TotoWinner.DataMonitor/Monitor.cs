﻿using HtmlAgilityPack;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using TotoWinner.Common;

namespace TotoWinner.DataMonitor
{
    public partial class Monitor : Form
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string formulaForamt = "#.####";
        private decimal trigger1 = (decimal)1;//for test 1.22
        private decimal trigger2 = (decimal)1.5;
        private decimal trigger3 = (decimal)1.5;
        private decimal trigger4 = (decimal)1; //for test 1.18 
        private decimal trigger5 = (decimal)1; //for test 1.44
        private decimal trigger6 = (decimal)1; //for test 1.5
        private decimal trigger7 = (decimal)1; //for test 1.32
        private decimal trigger8 = (decimal)1; //for test 1.32
        private decimal trigger9 = (decimal)1; //for test 1.27
        private decimal trigger10 = (decimal)1;//for test 1.333
        private decimal trigger11 = (decimal)1;//for test 1.27
        private decimal trigger12 = (decimal)1;//for test 1.28
        private decimal trigger13 = (decimal)1;//for test 1.28
        private decimal trigger14 = (decimal)1;//for test 1.29
        internal AllEvents AllSportEvents { get; }
        private Dictionary<SportEnum, bool> VisibleSportData;
        public Monitor()
        {
            this.AllSportEvents = new AllEvents();
            this.VisibleSportData = new Dictionary<SportEnum, bool>();
            this.VisibleSportData.Add(SportEnum.Soccer, true);
            this.VisibleSportData.Add(SportEnum.Basketball, true);
            this.VisibleSportData.Add(SportEnum.Tennis, true);
            this.VisibleSportData.Add(SportEnum.Handball, true);
            this.VisibleSportData.Add(SportEnum.Football, true);

            var jsonString = FetchDataFromUrl();
            LoadData(jsonString);
            InitializeComponent();
            buttonMonitor_Click(null, null);
        }
        private string FetchDataFromUrl()
        {
            try
            {
                var jsonString = string.Empty;
                var totoWinnerUrl = $"https://affiliates-xml.winner.co.il/xmls/RPMInternetProgramm_1.json";
                WebRequest request = WebRequest.Create(totoWinnerUrl);
                WebResponse response = request.GetResponse();
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    jsonString = reader.ReadToEnd();
                }
                return jsonString;
            }
            catch
            { }
            return null;
        }
        private void LoadData(string jsonString)
        {
            try
            {
                if (string.IsNullOrEmpty(jsonString))
                    return;
                //var programs = WebApiInvoker.JsonDeserializeDynamic(jsonString);
                //var filePath = @"C:\Users\ofirba\Projects\TotoWinner\TotoWinner.UnitTests\bin\Debug\RPMInternetProgramm_1.json";
                //File.WriteAllText(filePath, jsonString);
                //jsonString = File.ReadAllText(filePath);
                var jss = new JavaScriptSerializer();
                jss.MaxJsonLength = 2147483647;
                var dict = jss.Deserialize<dynamic>(jsonString);
                var events = dict["events"];
                foreach (var evnt in events)
                {
                    var countryName = evnt["countryName"];
                    var leagueName = evnt["leagueName"];
                    var description = evnt["description"];
                    var homeTeamName = evnt["homeTeamName"];
                    var awayTeamName = evnt["awayTeamName"];
                    var eventDate = DateTime.Parse(evnt["eventDate"]);
                    var eventId = evnt["id"];
                    var sportId = evnt["sportId"];
                    string periodTypeName = string.Empty;
                    var sportType = (SportEnum)sportId;
                    if (sportType == SportEnum.Basketball ||
                        sportType == SportEnum.Football ||
                        sportType == SportEnum.Tennis ||
                        sportType == SportEnum.Handball ||
                        sportType == SportEnum.Soccer)
                    {
                        foreach (var market in evnt["markets"])
                        {
                            var status = market["status"];
                            if (status == "OPEN")
                            {
                                market.TryGetValue("description", out dynamic marketDescription);
                                market.TryGetValue("periodTypeName", out dynamic dynPeriodTypeName);
                                market.TryGetValue("marketTypeId", out dynamic dynMarketTypeId);
                                market.TryGetValue("marketTypeName", out dynamic dynMarketTypeName);
                                market.TryGetValue("periodId", out dynamic dynPeriodId);
                                var key = new EventKey(sportType, eventId, dynMarketTypeId, dynPeriodId);
                                var scenarioIdentifier = new Tuple<int, int>(dynMarketTypeId, dynPeriodId);
                                if (this.AllSportEvents.MarketTypeIdsPeriodsIds[sportType].Contains(scenarioIdentifier))
                                {
                                    var rates = new Dictionary<string, decimal>();
                                    var ordinalPositionTitles = new Dictionary<int, string>();
                                    var ordinalPositionOutcomeId = new Dictionary<int, int>();
                                    foreach (var outcome in market["outcomes"])
                                    {
                                        var rate = outcome["internetOdd"];
                                        var rateTitle = outcome["description"];
                                        var ordinalPosition = outcome["ordinalPosition"];
                                        var id = outcome["id"];
                                        rates.Add(rateTitle, rate);
                                        ordinalPositionTitles.Add(ordinalPosition, rateTitle);
                                        ordinalPositionOutcomeId.Add(ordinalPosition, id);
                                    }
                                    var specialPicks = GetSpecialPicks(key, ordinalPositionOutcomeId.First().Value);
                                    if (rates.Count > 0 && !this.AllSportEvents.MainEvents.ContainsKey(key) && rates.All(r => r.Value > 0))
                                    {
                                        this.AllSportEvents.MainEvents.Add(key, new MainEvent()
                                        {
                                            CountryName = countryName,
                                            EventId = eventId,
                                            MarketTypeId = key.MarketTypeId,
                                            MarketTypeName = dynMarketTypeName,
                                            Description = description,
                                            PeriodTypeName = dynPeriodTypeName,
                                            EventDate = eventDate,
                                            LeagueName = leagueName,
                                            SportId = (SportEnum)sportId,
                                            OrdinalPositionTitles = ordinalPositionTitles,
                                            PeriodId = key.PeriodId,
                                            OrdinalPositionOutcomeId = ordinalPositionOutcomeId,
                                            RobotIdSpecialPicks = specialPicks
                                        });
                                        this.AllSportEvents.MainEvents[key].Rates = rates;
                                        this.AllSportEvents.MainEvents[key].LaunderResult = rates.Sum(r => 1 / r.Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"Unexpected failure occurred", ex);
            }
        }

        private Dictionary<int, Dictionary<string, string>> GetSpecialPicks(EventKey key, int outcomeId)
        {
            Dictionary<int, Dictionary<string, string>> robotIdCollection = null;
            try
            {
                // Since https://www.winner.co.il/ is not reachable on amazon server, better not to use this, as it stuck the process
#if DEBUG
                if (key.MarketTypeId == 110123 && key.PeriodId == 100) //for alert#4
                {
                    var totoWinnerUrl = $"https://www.winner.co.il/line/{key.EventId}";
                    var web = new HtmlWeb();
                    var htmlDoc = web.Load(totoWinnerUrl);
                    var nodes = htmlDoc.DocumentNode.SelectNodes("//div[@data-id='110123|100']");
                    var takeChildHome = true;
                    if (nodes != null && nodes.Count == 1)
                    {
                        var node = nodes[0];
                        var outcome1 = node.SelectSingleNode($".//td[contains(@class, 'outcome_{outcomeId}')]");
                        var titleNode1 = outcome1.SelectSingleNode(".//div[contains(@class, 'title')]");
                        var title1 = titleNode1.Attributes["title"].Value;
                        takeChildHome = title1.Contains("+"); //true - child home with parent away //false  -child away with parent home
                        robotIdCollection = new Dictionary<int, Dictionary<string, string>>();
                        robotIdCollection.Add(4, new Dictionary<string, string>() {
                        { "Child+", takeChildHome
                                    ? "home"
                                    : "away"}
                    });

                        var outcome2 = node.SelectSingleNode($".//td[contains(@class, 'ordpos_2')]");
                        var titleNode2 = outcome2.SelectSingleNode(".//div[contains(@class, 'title')]");
                        var outcome3 = node.SelectSingleNode($".//td[contains(@class, 'ordpos_3')]");
                        var titleNode3 = outcome3.SelectSingleNode(".//div[contains(@class, 'title')]");
                        robotIdCollection[4].Add("SiteInfo", $"{Environment.NewLine}{titleNode1.InnerText}{Environment.NewLine}{titleNode2.InnerText}{Environment.NewLine}{titleNode3.InnerText}");

                        var span1 = outcome1.SelectSingleNode(".//span[contains(@class, 'formatted_price')]");
                        var rate1 = span1.InnerText;
                        robotIdCollection[4].Add("Home Rate", rate1);

                        var span3 = outcome3.SelectSingleNode(".//span[contains(@class, 'formatted_price')]");
                        var rate3 = span3.InnerText;
                        robotIdCollection[4].Add("Away Rate", rate3);
                    }
                }
#endif
            }
            catch (Exception ex)
            {
                _log.Error($"An error occurred trying to get special picks for alert#4 EVENT: {key.ToString()} ; outcomeId={outcomeId}", ex);
            }
            return robotIdCollection;
        }

        private void buttonMonitor_Click(object sender, EventArgs e)
        {
            try
            {
                this.buttonMonitor.Enabled = false;
                ClearMemory();
                this.AllSportEvents.Clear();
                var jsonString = FetchDataFromUrl();
                LoadData(jsonString);
                this.labelLastUpdateDate.Text = DateTime.Now.ToString();
                this.labelLastUpdateDate.BackColor = Color.GreenYellow;
                PrepareSyntheticRobotsData();
                foreach (var sportKPV in this.VisibleSportData.Where(t => t.Value == true))
                {
                    if (sportKPV.Key == SportEnum.Soccer)
                    {
                        AddNewTab(SportEnum.Soccer, this.AllSportEvents.MarketTypeIdsPeriodsIds[sportKPV.Key][0]);
                        AddNewTab(SportEnum.Soccer, this.AllSportEvents.MarketTypeIdsPeriodsIds[sportKPV.Key][2]);
                        AddNewTab(SportEnum.Soccer, this.AllSportEvents.MarketTypeIdsPeriodsIds[sportKPV.Key][9]);
                        AddNewTab(SportEnum.Soccer, this.AllSportEvents.MarketTypeIdsPeriodsIds[sportKPV.Key][8]);
                        for (var index = 0; index < this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer].Count; index++)
                        {
                            if (index == 0 || index == 2 || index == 8 || index == 9)
                                continue;
                            AddNewTab(SportEnum.Soccer, this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][index]);
                        }
                    }
                    else
                        this.AllSportEvents.MarketTypeIdsPeriodsIds[sportKPV.Key].ForEach(t => AddNewTab(sportKPV.Key, t));
                }
                buttonApplyFilter_Click(null, null);

                this.buttonMonitor.Enabled = true;
            }
            catch (System.ComponentModel.Win32Exception exwin)
            {
                _log.Fatal($"Unexpected failure occurred", exwin);
            }
            catch (Exception ex)
            {
                _log.Fatal($"Unexpected failure occurred", ex);
            }
        }
        private void PrepareSyntheticRobotsData()
        {
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][0], //1, 100 1X2
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][1], //110123, 100 הימור יתרון
                4);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][2], //110100, 100 סך הכל שערים
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][3], //3007, 100 מעל מתחת שערים
                5);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][2], //110100, 100 סך הכל שערים
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][3], //3007, 100 מעל מתחת שערים
                6);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][9], //27, 100 מחצית סיום
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][0], //1, 100 1X2
                7);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][9], //27, 100 מחצית סיום
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][0], //1, 100 1X2
                8);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][9], //27, 100 מחצית סיום
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][11], //1, 102 מחצית ראשונה
                9);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][9], //27, 100 מחצית סיום
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][11], //1, 102 מחצית ראשונה
                10);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][9], //27, 100 מחצית סיום
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][11], //1, 102 מחצית ראשונה
                11);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][8], //28, 102 תוצאה מדויקת מחצית ראשונה
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][11], //1, 102 מחצית ראשונה
                12);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][8], //28, 102 תוצאה מדויקת מחצית ראשונה
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][11], //1, 102 מחצית ראשונה
                13);
            PrepareSyntheticData(
                SportEnum.Soccer,
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][8], //28, 102 תוצאה מדויקת מחצית ראשונה
                this.AllSportEvents.MarketTypeIdsPeriodsIds[SportEnum.Soccer][11], //1, 102 מחצית ראשונה
                14);

        }
        private void PrepareSyntheticData(SportEnum spotType, Tuple<int, int> baseIdentifiers, Tuple<int, int> synthticIdentifiers,
            int robotId)
        {
            try
            {
                var pickSyntheticEvents = this.AllSportEvents.MainEvents.Where(kp => kp.Key.SportId == spotType && kp.Key.MarketTypeId == synthticIdentifiers.Item1
                                && kp.Key.PeriodId == synthticIdentifiers.Item2);
                var pickBaseEvents = this.AllSportEvents.MainEvents.Where(kp => kp.Key.SportId == spotType && kp.Key.MarketTypeId == baseIdentifiers.Item1
                                    && kp.Key.PeriodId == baseIdentifiers.Item2);

                foreach (var pbe in pickBaseEvents)
                {
                    var synEvent = pickSyntheticEvents.Where(e => e.Value.EventDate == pbe.Value.EventDate &&
                        e.Value.CountryName == pbe.Value.CountryName && e.Value.LeagueName == pbe.Value.LeagueName &&
                        e.Value.Description == pbe.Value.Description);
                    if (synEvent.Count() == 1)
                    {
                        if (pbe.Value.FieldNameSyntheticData == null)
                            pbe.Value.FieldNameSyntheticData = new Dictionary<string, string>();
                        var syncEventData = synEvent.First().Value;
                        switch (robotId)
                        {
                            case 4:
                                var parentOppositeIndex = 0;
                                var childIndesx = 0;
                                var childPlusRateFromSite = (decimal)0.0;
                                if (syncEventData.RobotIdSpecialPicks != null)
                                {
                                    if (syncEventData.RobotIdSpecialPicks[4]["Child+"] == "away")
                                    {
                                        parentOppositeIndex = 1;
                                        childIndesx = 3;
                                        if (!Decimal.TryParse(syncEventData.RobotIdSpecialPicks[4]["Away Rate"], out childPlusRateFromSite))
                                            continue;
                                        //throw new Exception($"{syncEventData.Description}/{syncEventData.LeagueName}, Can not convert to decimal: {syncEventData.RobotIdSpecialPicks[4]["Away Rate"]}");
                                        //childPlusRateFromSite = Decimal.Parse(syncEventData.RobotIdSpecialPicks[4]["Away Rate"]);
                                    }
                                    else
                                    {
                                        parentOppositeIndex = 3;
                                        childIndesx = 1;
                                        if (!Decimal.TryParse(syncEventData.RobotIdSpecialPicks[4]["Home Rate"], out childPlusRateFromSite))
                                            continue;
                                        //throw new Exception($"{syncEventData.Description}/{syncEventData.LeagueName}, Can not convert to decimal: {syncEventData.RobotIdSpecialPicks[4]["Home Rate"]}");
                                        //childPlusRateFromSite = Decimal.Parse(syncEventData.RobotIdSpecialPicks[4]["Home Rate"]);
                                    }
                                    decimal parentOpposite;
                                    decimal synValue;

                                    try
                                    {
                                        parentOpposite = pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[parentOppositeIndex]];
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception("", ex);
                                    }
                                    try
                                    {
                                        synValue = syncEventData.Rates[syncEventData.OrdinalPositionTitles[childIndesx]];
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception("", ex);
                                    }
                                    var syntheticRates4 = new Dictionary<string, decimal>()
                                    {
                                        {"Child+", synValue },
                                        {"Opposite",  parentOpposite }
                                    };
                                    pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Child+", synValue.ToString());
                                    pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Opposite", parentOpposite.ToString());
                                    var formula4 = syntheticRates4.Values.Sum(r => 1 / r);
                                    pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula4.ToString(formulaForamt));
                                    var childPlusRateFromApi = Decimal.Parse(synValue.ToString());
                                    var syntheticRates4_2 = new Dictionary<string, decimal>()
                                    {
                                        {"Child+", childPlusRateFromSite },
                                        {"Opposite",  parentOpposite }
                                    };
                                    var formula4_2 = syntheticRates4_2.Values.Sum(r => 1 / r);
                                    if ((formula4 < trigger4 && childPlusRateFromApi == childPlusRateFromSite) ||
                                        formula4_2 < trigger4)
                                        AlertsManager.HandleAlert(4, "הימור אבא  עם נגדי + 1", pbe.Value, formula4, syntheticRates4,
                                            syncEventData.RobotIdSpecialPicks[4]["SiteInfo"]);
                                }
                                break;
                            case 5:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]]
                                //};
                                var syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[1], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]] },
                                    {pbe.Value.OrdinalPositionTitles[2], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]] },
                                    {syncEventData.OrdinalPositionTitles[1], syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]] },
                                };
                                var formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} {syncEventData.OrdinalPositionTitles[1]}", syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger5)
                                    AlertsManager.HandleAlert(5, "שלושה הימורי שערים", pbe.Value, formula, syntheticRates);
                                break;
                            case 6:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[2], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]] },
                                    {pbe.Value.OrdinalPositionTitles[3], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} {syncEventData.OrdinalPositionTitles[2]}", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger6)
                                    AlertsManager.HandleAlert(6, "שלושה הימורי שערים", pbe.Value, formula, syntheticRates);
                                break;
                            case 7:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[4]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[1], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]] },
                                    {pbe.Value.OrdinalPositionTitles[2], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]] },
                                    {pbe.Value.OrdinalPositionTitles[3], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]] },
                                    {pbe.Value.OrdinalPositionTitles[4], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[4]] },
                                    {pbe.Value.OrdinalPositionTitles[7], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                    {syncEventData.OrdinalPositionTitles[3], syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger7)
                                    AlertsManager.HandleAlert(7, "אבא סינטטי עבור הימור 1", pbe.Value, formula, syntheticRates);
                                break;
                            case 8:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[6]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[8]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[9]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[3], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]] },
                                    {pbe.Value.OrdinalPositionTitles[6], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[6]] },
                                    {pbe.Value.OrdinalPositionTitles[7], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]] },
                                    {pbe.Value.OrdinalPositionTitles[8], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[8]] },
                                    {pbe.Value.OrdinalPositionTitles[9], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[9]] },
                                    {syncEventData.OrdinalPositionTitles[1], syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger8)
                                    AlertsManager.HandleAlert(8, "אבא סינטטי עבור הימור 2", pbe.Value, formula, syntheticRates);
                                break;
                            case 9:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[1], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]] },
                                    {pbe.Value.OrdinalPositionTitles[2], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]] },
                                    {pbe.Value.OrdinalPositionTitles[3], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                    {syncEventData.OrdinalPositionTitles[3], syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger9)
                                    AlertsManager.HandleAlert(9, "אבא הימור מחצית עבור הימור", pbe.Value, formula, syntheticRates);
                                break;
                            case 10:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[4]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[5]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[6]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[4], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[4]] },
                                    {pbe.Value.OrdinalPositionTitles[5], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[5]] },
                                    {pbe.Value.OrdinalPositionTitles[6], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[6]] },
                                    {syncEventData.OrdinalPositionTitles[1], syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]] },
                                    {syncEventData.OrdinalPositionTitles[3], syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger10)
                                    AlertsManager.HandleAlert(10, "אבא הימור מחצית עבור הימור", pbe.Value, formula, syntheticRates);
                                break;
                            case 11:
                                //    var synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[8]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[9]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[7], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]] },
                                    {pbe.Value.OrdinalPositionTitles[8], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[8]] },
                                    {pbe.Value.OrdinalPositionTitles[9], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[9]] },
                                    {syncEventData.OrdinalPositionTitles[1], syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger11)
                                    AlertsManager.HandleAlert(11, "אבא הימור מחצית עבור הימור", pbe.Value, formula, syntheticRates);
                                break;
                            case 12:
                                //    synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[1], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[1]] },
                                    {pbe.Value.OrdinalPositionTitles[2], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[2]] },
                                    {pbe.Value.OrdinalPositionTitles[3], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[3]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                    {syncEventData.OrdinalPositionTitles[3], syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger12)
                                    AlertsManager.HandleAlert(12, "אבא הימור מחצית עבור הימור תוצאה מדויקת מחצית ראשונה", pbe.Value, formula, syntheticRates);
                                break;
                            case 13:
                                //    synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[4]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[5]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[6]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[4], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[4]] },
                                    {pbe.Value.OrdinalPositionTitles[5], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[5]] },
                                    {pbe.Value.OrdinalPositionTitles[6], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[6]] },
                                    {syncEventData.OrdinalPositionTitles[1], syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]] },
                                    {syncEventData.OrdinalPositionTitles[3], syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", syncEventData.Rates[syncEventData.OrdinalPositionTitles[3]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger13)
                                    AlertsManager.HandleAlert(13, "אבא הימור מחצית עבור הימור תוצאה מדויקת מחצית ראשונה", pbe.Value, formula, syntheticRates);
                                break;
                            case 14:
                                //    synRates = new List<decimal>()
                                //{
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[8]],
                                //    pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[9]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]],
                                //    syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]]
                                //};
                                syntheticRates = new Dictionary<string, decimal>()
                                {
                                    {pbe.Value.OrdinalPositionTitles[7], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[7]] },
                                    {pbe.Value.OrdinalPositionTitles[8], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[8]] },
                                    {pbe.Value.OrdinalPositionTitles[9], pbe.Value.Rates[pbe.Value.OrdinalPositionTitles[9]] },
                                    {syncEventData.OrdinalPositionTitles[1], syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]] },
                                    {syncEventData.OrdinalPositionTitles[2], syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]] },
                                };
                                formula = syntheticRates.Values.Sum(r => 1 / r);
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", syncEventData.Rates[syncEventData.OrdinalPositionTitles[1]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", syncEventData.Rates[syncEventData.OrdinalPositionTitles[2]].ToString());
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", formula.ToString(formulaForamt));
                                if (formula < trigger14)
                                    AlertsManager.HandleAlert(14, "אבא הימור מחצית עבור הימור תוצאה מדויקת מחצית ראשונה", pbe.Value, formula, syntheticRates);
                                break;
                        }
                    }
                    else
                    {
                        if (pbe.Value.FieldNameSyntheticData == null)
                            pbe.Value.FieldNameSyntheticData = new Dictionary<string, string>();

                        switch (robotId)
                        {
                            case 9:
                            case 12:
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", "-");
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", "-");
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", "-");
                                break;
                            case 10:
                            case 13:
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", "-");
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 2", "-");
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", "-");
                                break;
                            case 11:
                            case 14:
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} 1", "-");
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} X", "-");
                                pbe.Value.FieldNameSyntheticData.Add($"#{robotId} Formula", "-");
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.labelLastUpdateDate.BackColor = Color.Orange;
                _log.Fatal($"Unexpected failure occurred", ex);
            }
        }

        private void PrepareSummary()
        {
            this.AllSportEvents.Summary.Clear();
            foreach (var sportKPV in this.VisibleSportData.Where(t => t.Value == true))
            {
                this.AllSportEvents.MarketTypeIdsPeriodsIds[sportKPV.Key].ForEach(t =>
                {
                    var pickEvent = this.AllSportEvents.MainEvents.Where(kp => kp.Key.SportId == sportKPV.Key && kp.Key.MarketTypeId == t.Item1
                            && kp.Key.PeriodId == t.Item2)
                        .Select(kp => kp.Value).ToList().OrderBy(m => m.LaunderResult).FirstOrDefault();
                    if (pickEvent != null)
                    {
                        var summaryEvnt = new SummaryEvent(pickEvent);
                        summaryEvnt.SportType = $"{t.Item1},{t.Item2} {pickEvent.MarketTypeName}";
                        this.AllSportEvents.Summary.Add(summaryEvnt);
                    }
                });
            }
            this.AllSportEvents.SummaryEventData = new SummaryEventData(this.AllSportEvents.Summary);
        }
        private void AddNewTab(SportEnum sport, Tuple<int, int> identifier)
        {
            var events = this.AllSportEvents.MainEvents.Where(kp => kp.Key.MarketTypeId == identifier.Item1
                && kp.Key.PeriodId == identifier.Item2)
                .Select(kp => kp.Value).ToList();
            if (events.Count() > 0)
            {
                var title = $"{identifier.Item1},{identifier.Item2} {events.First().MarketTypeName}";
                var md = new SportEventData(sport, title, events);
                this.AllSportEvents.AllEventsData.Add(md);
            }
        }

        private void checkBoxFootball_CheckedChanged(object sender, EventArgs e)
        {
            this.VisibleSportData[SportEnum.Football] = ((CheckBox)sender).Checked;
        }

        private void checkBoxBasketball_CheckedChanged(object sender, EventArgs e)
        {
            this.VisibleSportData[SportEnum.Basketball] = ((CheckBox)sender).Checked;
        }

        private void checkBoxTennis_CheckedChanged(object sender, EventArgs e)
        {
            this.VisibleSportData[SportEnum.Tennis] = ((CheckBox)sender).Checked;
        }
        private void checkBoxHandball_CheckedChanged(object sender, EventArgs e)
        {
            this.VisibleSportData[SportEnum.Handball] = ((CheckBox)sender).Checked;
        }
        private void checkBoxSoccer_CheckedChanged(object sender, EventArgs e)
        {
            this.VisibleSportData[SportEnum.Soccer] = ((CheckBox)sender).Checked;
        }

        private void buttonApplyFilter_Click(object sender, EventArgs e)
        {
            PrepareSummary();
            CheckForAlerts();
            this.tabControl1.Controls.Add(this.AllSportEvents.SummaryEventData.TabPage);

            foreach (var sportKPV in this.VisibleSportData.Where(t => t.Value == true))
            {
                foreach (var tab in this.AllSportEvents.AllEventsData.Where(i => i.SportType == sportKPV.Key))
                    this.tabControl1.Controls.Add(tab.TabPage);
            }
        }
        private void ClearMemory()
        {
            foreach (var sportKPV in this.VisibleSportData)
            {
                foreach (var tab in this.AllSportEvents.AllEventsData.Where(i => i.SportType == sportKPV.Key))
                    tab.Dispose();
            }
            this.AllSportEvents.SummaryEventData?.Dispose();
            this.tabControl1.Controls.Clear();
        }
        private void CheckForAlerts()
        {
            try
            {
                foreach (var data in this.AllSportEvents.Summary
                    .Where(d => d.SportId == SportEnum.Soccer && d.LaunderResult < trigger1))
                {
                    AlertsManager.HandleAlert(1, "SUMMARY EVENT DATA", (MainEvent)data);
                }
                foreach (var data in this.AllSportEvents.MainEvents
                    .Where(d => d.Value.SportId == SportEnum.Soccer && d.Value.MarketTypeId == 100000025))
                {
                    if (data.Value.Rates.Last().Value > trigger2)
                        AlertsManager.HandleAlert(2, "מעל או מתחת 0.5 שערים עד דקה 15", data.Value);
                }
                foreach (var data in this.AllSportEvents.MainEvents
                    .Where(d => d.Value.SportId == SportEnum.Soccer && d.Value.MarketTypeId == 100000020))
                {
                    var xTitle = data.Value.OrdinalPositionTitles[1];
                    if (data.Value.Rates[xTitle] > trigger3)
                        AlertsManager.HandleAlert(3, "איזו קבוצה תבקיע יותר שערים עד דקה 15", data.Value);
                }
            }
            catch (Exception ex)
            {
                this.labelLastUpdateDate.BackColor = Color.Orange;
                _log.Fatal($"Unexpected failure occurred", ex);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timer1.Enabled = false;
            this.buttonMonitor_Click(sender, e);
            this.timer1.Enabled = true;
        }
    }
}
