﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TotoWinner.DataMonitor
{
    internal class SportEventData : IDisposable
    {
        public System.Windows.Forms.TabPage TabPage { get; private set; }
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn League;
        private System.Windows.Forms.DataGridViewTextBoxColumn BetName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BetDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BetTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn[] RatesTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn Formula;
        private System.Windows.Forms.DataGridViewTextBoxColumn[] SyntheticTitles;
        private string marketDataType;
        public SportEnum SportType { get; }
        internal SportEventData(SportEnum sportType, string dataType, IEnumerable<MainEvent> events)
        {
            this.SportType = sportType;
            this.marketDataType = dataType;
            events = events.OrderBy(i => i.LaunderResult);
            var outcomes = new List<DataGridViewTextBoxColumn>();
            foreach (var title in events.First().OrdinalPositionTitles)
            {
                var outcome = SetRatesTitles(events.First(), title);
                outcomes.Add(outcome);
            }
            this.RatesTitle = outcomes.ToArray();
            var synthetics = new List<DataGridViewTextBoxColumn>();
            try
            {
                var maxFields = events.Where(e => e.FieldNameSyntheticData != null).Count() > 0 ?
                    events.Where(e => e.FieldNameSyntheticData != null).Max(i => i.FieldNameSyntheticData.Count())
                    : 0;

                if (maxFields> 0)
                {
                    if (events.Where(e => e.FieldNameSyntheticData != null && e.FieldNameSyntheticData.Count == maxFields).Count() > 0)
                    {
                        foreach (var title in events.First(e => e.FieldNameSyntheticData != null && e.FieldNameSyntheticData.Count == maxFields).FieldNameSyntheticData.Keys)
                        {
                            synthetics.Add(new DataGridViewTextBoxColumn()
                            {
                                HeaderText = title,
                                Name = title
                            });
                        }
                    }
                }
            }
            catch(Exception)
            { }
            this.SyntheticTitles = synthetics.ToArray();
            InitializeComponent();
            foreach (var data in events)
            {
                var row = (DataGridViewRow)this.dataGridView.Rows[0].Clone();
                row.Cells[0].Value = data.CountryName;
                row.Cells[1].Value = data.LeagueName;
                row.Cells[2].Value = data.Description;
                row.Cells[3].Value = data.EventDate.Date.ToString("d");
                row.Cells[4].Value = data.EventDate.ToString("HH:mm");
                row.Cells[5].Value = data.LaunderResult.ToString("#.####");
                var ordinal = data.OrdinalPositionTitles.First().Key;//1;
                var index = 6;
                for (; index < this.RatesTitle.Count() + 6; index++)
                {
                    var rateTitle = data.OrdinalPositionTitles[ordinal];
                    row.Cells[index].Value = data.Rates[rateTitle];
                    ordinal++;
                }
                if (data.FieldNameSyntheticData != null)
                {
                    try
                    {
                        foreach (var extras in data.FieldNameSyntheticData)
                        {
                            row.Cells[index].Value = extras.Value;
                            index++;
                        }
                    }
                    catch(Exception ex)
                    { }
                }
                this.dataGridView.Rows.Add(row);
            }
        }
        
        private DataGridViewTextBoxColumn SetRatesTitles(MainEvent mainEvent, KeyValuePair<int, string> title)
        {
            var outcome = new DataGridViewTextBoxColumn();

            switch (mainEvent.MarketTypeId)
            {
                case 1:
                case 110123:
                case 110131:
                case 110121:
                    var columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "1";
                            break;
                        case 2:
                            columnTitle = "X";
                            break;
                        case 3:
                            columnTitle = "2";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 100000020:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 0:
                            columnTitle = "1";
                            break;
                        case 1:
                            columnTitle = "X";
                            break;
                        case 2:
                            columnTitle = "2";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 110124:
                case 3059:
                case 1253:
                case 1254:
                case 1257:
                case 67:
                case 68:
                case 1103:
                case 11:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "1";
                            break;
                        case 2:
                            columnTitle = "2";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 27:
                case 13810:
                case 13820:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "Home-Home";
                            break;
                        case 2:
                            columnTitle = "X-Home";
                            break;
                        case 3:
                            columnTitle = "Away-Home";
                            break;
                        case 4:
                            columnTitle = "Home-X";
                            break;
                        case 5:
                            columnTitle = "X-X";
                            break;
                        case 6:
                            columnTitle = "Away-X";
                            break;
                        case 7:
                            columnTitle = "Home-Away";
                            break;
                        case 8:
                            columnTitle = "X-Away";
                            break;
                        case 9:
                            columnTitle = "Away-Away";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 28:
                    var reversedTitle = new string(title.Value.Reverse().ToArray());
                    outcome.HeaderText = reversedTitle;
                    outcome.Name = reversedTitle;
                    break;
                case 29:
                case 110094:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "Home";
                            break;
                        case 2:
                            columnTitle = title.Value;
                            break;
                        case 3:
                            columnTitle = "Away";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 100000023:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 0:
                            columnTitle = "Home";
                            break;
                        case 1:
                            columnTitle = title.Value;
                            break;
                        case 2:
                            columnTitle = "Away";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 100000065:
                case 13750:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "Home";
                            break;
                        case 2:
                            columnTitle = title.Value;
                            break;
                        case 3:
                            columnTitle = "Away";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 110128:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "Home(+1)";
                            break;
                        case 2:
                            columnTitle = title.Value;
                            break;
                        case 3:
                            columnTitle = "Away(-1)";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;
                case 232:
                    columnTitle = "TBD";
                    switch (title.Key)
                    {
                        case 1:
                            columnTitle = "Home";
                            break;
                        case 2:
                            columnTitle = "Away";
                            break;
                    }
                    outcome.HeaderText = $"{columnTitle}";
                    outcome.Name = $"{columnTitle}";
                    break;

                default:
                    outcome.HeaderText = title.Value;
                    outcome.Name = title.Value;
                    break;
            }
            outcome.ReadOnly = true;
            return outcome;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabPage = new System.Windows.Forms.TabPage();
            this.dataGridView = new System.Windows.Forms.DataGridView();

            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.League = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BetName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BetDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BetTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Formula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            // 
            // tabPage1
            // 
            this.TabPage.Controls.Add(this.dataGridView);
            this.TabPage.Location = new System.Drawing.Point(4, 22);
            this.TabPage.Name = this.marketDataType;
            this.TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage.Size = new System.Drawing.Size(1051, 648);
            this.TabPage.TabIndex = 0;
            this.TabPage.Text = this.marketDataType;
            this.TabPage.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Country,
            this.League,
            this.BetName,
            this.BetDate,
            this.BetTime,
            this.Formula});
            this.dataGridView.Columns.AddRange(this.RatesTitle);
            this.dataGridView.Columns.AddRange(this.SyntheticTitles);
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 3);
            this.dataGridView.Name = "dataGridView1";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(1045, 642);
            this.dataGridView.TabIndex = 0;
            // 
            // Type
            // 

            // 
            // Country
            // 
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            // 
            // League
            // 
            this.League.HeaderText = "League";
            this.League.Name = "League";
            this.League.ReadOnly = true;
            // 
            // BetName
            // 
            this.BetName.HeaderText = "Bet Name";
            this.BetName.Name = "BetName";
            this.BetName.ReadOnly = true;
            // 
            // BetDate
            // 
            this.BetDate.HeaderText = "Bet Date";
            this.BetDate.Name = "BetDate";
            this.BetDate.ReadOnly = true;
            // 
            // BetTime
            // 
            this.BetTime.HeaderText = "Bet Time";
            this.BetTime.Name = "BetTime";
            this.BetTime.ReadOnly = true;
            // 
            // Formula
            // 
            this.Formula.HeaderText = "Formula";
            this.Formula.Name = "Formula";
            this.Formula.ReadOnly = true;
            // 
            // Monitor
            // 
            this.TabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
        }

        #endregion
        public void Dispose()
        {
            foreach (var column in this.dataGridView.Columns)
            {
                var obj = column as DataGridViewColumn;
                obj.Dispose();
            }
            this.dataGridView.Columns.Clear();
            this.dataGridView.Dispose();
            this.TabPage.Controls.Clear();
        }
    }
}
