﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Telegram.Bot;

namespace TotoWinner.DataMonitor
{
    public static class AlertsManager
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Dictionary<RobotAlertKey, EventKey> MailSent = new Dictionary<RobotAlertKey, EventKey>();
        private static TelegramBotClient _botClient;
        private static bool _sendNotification;
        static AlertsManager()
        {
            var botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
            _botClient = new TelegramBotClient(botToken);
            _sendNotification = bool.Parse(ConfigurationManager.AppSettings["SendNotification"]);

        }
        internal static void HandleAlert(int robotId, string subject, MainEvent data)
        {
            try
            {
                var eventKey = new EventKey(data.SportId, data.EventId, data.MarketTypeId, data.PeriodId);
                var key = new RobotAlertKey(robotId, eventKey);
                if (!MailSent.ContainsKey(key))
                {
                    var body = $"{data.SportId} {data.MarketTypeId},{data.PeriodId} {Environment.NewLine}{data.MarketTypeName}{Environment.NewLine}{data.Description}{Environment.NewLine}{data.LeagueName}{Environment.NewLine}BET TIME {data.EventDate}{Environment.NewLine}FORMULA {data.LaunderResult.ToString("#.####")}{Environment.NewLine}{Environment.NewLine}{Environment.MachineName}{Environment.NewLine}{Environment.NewLine}";
                    SendMessage($"Alert#{robotId}: {subject}", body);
                    MailSent.Add(key, eventKey);
                }
            }
            catch(Exception ex)
            {
                _log.Fatal($"Robot {robotId} failed to sent mail", ex);
                throw new Exception($"Robot {robotId} failed to sent mail", ex);
            }
        }
        internal static void HandleAlert(int robotId, string subject, MainEvent data, decimal formula, Dictionary<string, decimal> syntheticRates, string comment="")
        {
            try
            {
                var eventKey = new EventKey(data.SportId, data.EventId, data.MarketTypeId, data.PeriodId);
                var key = new RobotAlertKey(robotId, eventKey);
                if (!MailSent.ContainsKey(key))
                {
                    var body = $"{data.SportId} {data.MarketTypeId},{data.PeriodId} {Environment.NewLine}{data.MarketTypeName}{Environment.NewLine}{data.Description}{Environment.NewLine}{data.LeagueName}{Environment.NewLine}BET TIME {data.EventDate}{Environment.NewLine}FORMULA {formula.ToString("#.####")}{Environment.NewLine}{Environment.NewLine}{Environment.MachineName}{Environment.NewLine}{Environment.NewLine}";
                    var syntheticRatesBody = string.Empty;
                    foreach (var rate in syntheticRates)
                        syntheticRatesBody = $"{syntheticRatesBody}{rate.Key} = {rate.Value}{Environment.NewLine}";
                    if (!string.IsNullOrEmpty(comment))
                        comment = $"Real Time Data from winner site:{Environment.NewLine}{comment}";
                    SendMessage($"Alert#{robotId}: {subject}", $"{body}{syntheticRatesBody}{Environment.NewLine}{comment}");
                    MailSent.Add(key, eventKey);
                }
            }
            catch(Exception ex)
            {
                _log.Fatal($"Robot {robotId} failed to sent mail", ex);
                throw new Exception($"Robot {robotId} failed to sent mail", ex);
            }
        }
        private static void SendMessage(string subject, string message)
        {
            if (_sendNotification == true)
            {
                SendMessage(957002288, subject, message);
                SendMessage(585508035, subject, message);
            }
        }
        private static async void SendMessage(long chatId, string subject, string message)
        {
            try
            {
                Task.Delay(1000).Wait();
                var alert = $"{subject}{Environment.NewLine}{message}";
                await _botClient.SendTextMessageAsync(chatId, alert, Telegram.Bot.Types.Enums.ParseMode.Markdown);
            }
            catch(Exception ex)
            {
                _log.Fatal($"Failed to send alert: {subject} ::: {message}", ex);
            }
        }
        //private static void SendMail(string subject, string body)
        //{
        //    var fromAddress = new MailAddress("ofirba.eloi@gmail.com", "Win16 Data Monitor");
        //    var toAddress = new MailAddress("ofirba@gmail.com", "Ofir Ben Avraham");
        //    const string fromPassword = "Ofir!ba12$";

        //    var smtp = new SmtpClient
        //    {
        //        Host = "smtp.gmail.com",
        //        Port = 587,
        //        EnableSsl = true,
        //        DeliveryMethod = SmtpDeliveryMethod.Network,
        //        UseDefaultCredentials = false,
        //        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
        //    };
        //    using (var message = new MailMessage(fromAddress, toAddress)
        //    {
        //        Subject = subject,
        //        Body = body
        //    })
        //    {
        //        message.To.Add(new MailAddress("gabi9200@gmail.com", "Ben Gabizon"));
        //        smtp.Send(message);
        //    }
        //}
    }
}
