﻿using System;
using System.Linq;

namespace TotoWinner.BettingTips
{
    internal static class Helpers
    {
        internal static T DynamicResolver<T>(dynamic parent, string child, string attribute, ResolveEnum type)
        {
            try
            {
                var dNode = string.IsNullOrEmpty(child)
                    ? parent
                    : parent[child];
                dNode.TryGetValue(attribute, out dynamic dAttribute);
                switch (type)
                {
                    case ResolveEnum.INT:
                        return int.Parse(dAttribute.ToString());
                    case ResolveEnum.BOOL:
                        return bool.Parse(dAttribute.ToString());
                    case ResolveEnum.DATE:
                        return DateTime.Parse(dAttribute.ToString());
                    case ResolveEnum.DECIMAL:
                        return decimal.Parse(dAttribute);
                    case ResolveEnum.STRING:
                        return dAttribute.ToString();
                }
                throw new Exception($"{child}:{attribute} with type {type} is not supported");
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to resolve {child}:{attribute} with type {type}", ex);
            }
        }
        internal static string Hebrew(string message)
        {
            return new string(message.Reverse().ToArray());
        }
    }
}
