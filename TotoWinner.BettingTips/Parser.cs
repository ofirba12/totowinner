﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TotoWinner.Common;
using TotoWinner.Services;

namespace TotoWinner.BettingTips
{
    internal class Parser
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal Dictionary<int, BettingTipsParentEvent> ParseWinnerLineResponse(dynamic dWinnerLineResponse, int roundIdInput, List<int> sportIdsInput)
        {
            var result = new Dictionary<int, BettingTipsParentEvent>();
            var events = dWinnerLineResponse["events"];
            foreach (var evnt in events)
            {
                var sportId = Helpers.DynamicResolver<int>(evnt, "sport", "id", ResolveEnum.INT); //2 כדורגל
                if (!sportIdsInput.Contains(sportId))
                    continue;
                var roundId = Helpers.DynamicResolver<int>(evnt, "round", "id", ResolveEnum.INT);
                if (roundId != roundIdInput)
                    continue;
                var place = Helpers.DynamicResolver<int>(evnt, "round", "place", ResolveEnum.INT);
                var eventId = Helpers.DynamicResolver<int>(evnt, null, "id", ResolveEnum.INT);
                var league = Helpers.DynamicResolver<string>(evnt, "league", "name", ResolveEnum.STRING);
                var isFather = Helpers.DynamicResolver<bool>(evnt, null, "isFather", ResolveEnum.BOOL);
                var time = Helpers.DynamicResolver<DateTime>(evnt, null, "time", ResolveEnum.DATE);
                var status = Helpers.DynamicResolver<string>(evnt, "status", "description", ResolveEnum.STRING);
                var bet = ParseBet(evnt["bet"], sportId);
                var homeId = Helpers.DynamicResolver<int>(evnt, "home", "id", ResolveEnum.INT);
                var homeTeamName = Helpers.DynamicResolver<string>(evnt, "home", "name", ResolveEnum.STRING);
                var guestId = Helpers.DynamicResolver<int>(evnt, "guest", "id", ResolveEnum.INT);
                var guestTeamName = Helpers.DynamicResolver<string>(evnt, "guest", "name", ResolveEnum.STRING);
                var eventInfoJson = GetEventInfo(eventId);

                var eventInfoDict = WebApiInvoker.JsonDeserializeDynamic(eventInfoJson);
                var lastGames = ParseLastGames(eventInfoDict, homeId, homeTeamName, guestId, guestTeamName);
                var h2h = ParseH2H(eventInfoDict);
                var btParentEvent = new BettingTipsParentEvent(eventId,
                    sportId,
                    roundId,
                    place,
                    league,
                    isFather,
                    time,
                    status,
                    homeId,
                    homeTeamName,
                    guestId,
                    guestTeamName,
                    bet, lastGames, h2h);
                result.Add(eventId, btParentEvent);
            }
            return result;
        }

        private List<BettingTipsHeadeToHead> ParseH2H(dynamic eventInfoDict)
        {
            eventInfoDict.TryGetValue("h2h", out dynamic dH2h);
            var collection = new List<BettingTipsHeadeToHead>();
            foreach (var dh2hLine in dH2h)
            {
                try
                {
                    var date = Helpers.DynamicResolver<DateTime>(dh2hLine, null, "date", ResolveEnum.DATE);
                    var home = Helpers.DynamicResolver<string>(dh2hLine, null, "home", ResolveEnum.STRING);
                    var guest = Helpers.DynamicResolver<string>(dh2hLine, null, "guest", ResolveEnum.STRING);
                    var score = Helpers.DynamicResolver<string>(dh2hLine, null, "score", ResolveEnum.STRING);
                    var league = Helpers.DynamicResolver<string>(dh2hLine, null, "league", ResolveEnum.STRING);
                    var h2hLine = new BettingTipsHeadeToHead(date, home, guest, score, league);
                    collection.Add(h2hLine);
                }
                catch(Exception ex)
                {
                    _log.Warn("Head 2 Head game found invalid and will be skipped", ex);
                }
            }
            return collection;
        }

        private BettingTipsLastGames ParseLastGames(dynamic eventInfoDict, int homeId, string homeTeamName, int guestId, string guestTeamName)
        {
            var homeGames = GetLastGames(eventInfoDict, "home", homeTeamName);
            var guestGames = GetLastGames(eventInfoDict, "guest", guestTeamName);
            var result = new BettingTipsLastGames(homeId, guestId, homeGames, guestGames);
            return result;
        }
        private List<BettingTipsGameInfo> GetLastGames(dynamic eventInfoDict, string team, string teamName)
        {
            var dTeam = eventInfoDict[team];
            dTeam.TryGetValue("lastGames", out dynamic dlastGames);
            var games = new List<BettingTipsGameInfo>();
            foreach (var game in dlastGames)
            {
                try
                {
                    var date = Helpers.DynamicResolver<DateTime>(game, null, "date", ResolveEnum.DATE);
                    var place = Helpers.DynamicResolver<int>(game, null, "place", ResolveEnum.INT);
                    var opponent = Helpers.DynamicResolver<string>(game, null, "opponent", ResolveEnum.STRING);
                    var status = Helpers.DynamicResolver<int>(game, null, "status", ResolveEnum.INT);
                    var league = Helpers.DynamicResolver<string>(game, null, "league", ResolveEnum.STRING);
                    var score = Helpers.DynamicResolver<string>(game, null, "score", ResolveEnum.STRING);
                    var gameInfo = new BettingTipsGameInfo(date, place, opponent, status, league, score, teamName);
                    games.Add(gameInfo);
                }
                catch (Exception ex)
                {
                    _log.Warn("Last game found invalid and will be skipped", ex);
                }
            }
            return games;
        }
        private BettingTipsBet ParseBet(dynamic bet, int sportId)
        {
            bet["1"].TryGetValue("current", out dynamic dhomeBet);
            bet["X"].TryGetValue("current", out dynamic dDrawBet);
            bet["2"].TryGetValue("current", out dynamic dAwayBet);
            bet["description"].TryGetValue("value", out dynamic dBetName);
            bet["description"].TryGetValue("prefix", out dynamic dBetNamePrefix);
            bet["period"].TryGetValue("name", out dynamic dPeriod);
            var homeBet = decimal.Parse(dhomeBet);
            var drawBet = decimal.Parse(dDrawBet);
            var awayBet = decimal.Parse(dAwayBet);
            var betName = $"{dBetNamePrefix?.ToString()}{dBetName.ToString()}";
            decimal? barrierHome = null;
            decimal? barrierGuest = null;
            if (sportId == 5)
            {
                var teams = betName.Split('-');
                barrierHome = GetBarrierFromName(teams[0]);
                barrierGuest = GetBarrierFromName(teams[1]);
            }
            var result = new BettingTipsBet(homeBet, drawBet, awayBet, betName, dPeriod.ToString(), barrierHome, barrierGuest);
            return result;
        }

        private decimal? GetBarrierFromName(string betName)
        {
            decimal? result = null;
            string[] numbers = Regex.Split(betName, @"\D+");
            var numberStr = string.Empty;
            if (numbers.ToList().Any(n => !string.IsNullOrEmpty(n)))
            {
                foreach (var number in numbers)
                {
                    if (!string.IsNullOrEmpty(number))
                        numberStr = string.IsNullOrEmpty(numberStr)
                            ? $"{number}"
                            : $"{numberStr}.{number}";
                }
                result = decimal.Parse(numberStr);
            }
            return result;
        }

        private string GetEventInfo(int eventId)
        {
            var eventJson = string.Empty;
            var filePath = Path.Combine(Constants.RepositoryPath, $"Event_{eventId}.json");
            if (!File.Exists(filePath))
            {
                var eventInfoUrl = $"https://www.bankerim.co.il/api/winnerLine/getEventInfo?eventId={eventId}";
                eventJson = ExternalServices.Instance.FetchDataFromUrl(eventInfoUrl);
                File.WriteAllText(filePath, eventJson);
            }
            else
            {
                eventJson = File.ReadAllText(filePath);
            }
            return eventJson;
        }
    }
}
