﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TotoWinner.Common;
using TotoWinner.Services;

namespace TotoWinner.BettingTips
{
    internal class Menu
    {
        internal dynamic MarketDate { get; private set; }
        internal DateTime WinnerLineDate { get; private set; }
        internal int RoundId { get; private set; }
        internal int Momentum { get; private set; }
        internal bool Verbose { get; private set; }

        internal Menu Show()
        {
            var correct = true;
            do
            {
                correct = AskForWinnerLineDate();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("date chosen is not in an acceptable format!");
                }
            }
            while (!correct);
            Console.ForegroundColor = ConsoleColor.White;
            var winnerLineJson = GetWinnerLineResponse(this.WinnerLineDate);
            this.MarketDate = WebApiInvoker.JsonDeserializeDynamic(winnerLineJson);
            do
            {
                correct = AskForRoundId();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please choose as instruction below!");
                }
            }
            while (!correct);
            do
            {
                correct = AskForTotalMomentum();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please choose as instruction below!");
                }
            }
            while (!correct);
            do
            {
                correct = AskForVerbose();
                if (!correct)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please choose as instruction below!");
                }
            }
            while (!correct);
            Console.ForegroundColor = ConsoleColor.White;
            return this;
        }
        private bool AskForVerbose()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Use Full Data (Y/N):");
            var input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                this.Verbose = true;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"Default Full Data Chosen: {this.Verbose}");
                return true;
            }
            else 
            {
                var testInput = input?.ToUpper();
                if (testInput != null)
                {
                    if (testInput == "Y")
                        this.Verbose = true;
                    else if (testInput == "N")
                        this.Verbose = false;
                    else
                        return false;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"Full Data: {this.Verbose}");
                    return true;
                }
            }
            return false;
        }
        private bool AskForTotalMomentum()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Please choose Momentum (1-10):");
            var input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                this.Momentum = 3;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"Default Momentum Chosen: {this.Momentum}");
                return true;
            }
            else if (int.TryParse(input, out int momentum))
            {
                if (momentum > 0 && momentum <= 10)
                {
                    this.Momentum = momentum;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"Momentum: {this.WinnerLineDate}");
                    return true;
                }
            }
            return false;
        }
        private bool AskForWinnerLineDate()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Please choose Date (e.g 26-05-2021):");
            var input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                this.WinnerLineDate = DateTime.Parse("26-05-2021");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"Default Winner Line Date Chosen: {this.WinnerLineDate}");
                return true;
            }
            else
            {
                var enUS = new CultureInfo("en-US");
                if (DateTime.TryParseExact(input, "dd-MM-yyyy", enUS,
                                     DateTimeStyles.None, out var dateValue))
                {
                    this.WinnerLineDate = dateValue;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"Winner Line Date: {this.WinnerLineDate}");
                    return true;
                }
            }
            return false;
        }
        private bool AskForRoundId()
        {
            var dRounds = this.MarketDate["rounds"];
            Console.WriteLine("Rounds found:");
            var index = 1;
            var rounds = new Dictionary<int, int>();
            Console.ForegroundColor = ConsoleColor.Blue;
            foreach (var dRound in dRounds)
            {
                var id = Helpers.DynamicResolver<int>(dRound, null, "id", ResolveEnum.INT);
                var games = Helpers.DynamicResolver<int>(dRound, null, "games", ResolveEnum.INT);
                Console.WriteLine($"{index}. {id} has {games} games");
                rounds.Add(index, id);
                index++;
            }
            Console.WriteLine($"Please choose round (1-{index-1}):");
            Console.ForegroundColor = ConsoleColor.White;
            var input = Console.ReadLine();
            if (int.TryParse(input, out int roundIndexChosen) && roundIndexChosen >= 1 && roundIndexChosen < index)
            {
                this.RoundId = rounds[roundIndexChosen];
                Console.WriteLine($"Chosen Round Id: {rounds[roundIndexChosen]}");
                return true;
            }
            return false;
        }
        private string GetWinnerLineResponse(DateTime wnDate)
        {
            var winnerLineJson = string.Empty;
            var filePath = Path.Combine(Constants.RepositoryPath, $"WinnerLine_{wnDate.Day}_{wnDate.Month}_{wnDate.Year}.json");
            if (!File.Exists(filePath))
            {
                var wnDateStr = wnDate.ToString("yyyy-MM-dd 12:00:00");
                var winnerLineUrl = $"https://www.bankerim.co.il/api/winnerLine/getDate?date={wnDateStr}";
                winnerLineJson = ExternalServices.Instance.FetchDataFromUrl(winnerLineUrl);
                File.WriteAllText(filePath, winnerLineJson);
            }
            else
            {
                winnerLineJson = File.ReadAllText(filePath);
            }
            return winnerLineJson;
        }
    }
}
