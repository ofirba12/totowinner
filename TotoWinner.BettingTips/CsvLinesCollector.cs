﻿using System.Collections.Generic;

namespace TotoWinner.BettingTips
{
    internal class CsvLinesCollector
    {
        private int Momentum;

        public CsvLinesCollector(int momentum)
        {
            this.Momentum = momentum;
        }

        internal BettingTipsTotalsGamesContainer PrepareLastGames(List<BettingTipsGameInfo> lastGames, BettingTipsBet bet, GameType gameType, FileType fileType)
        {
            var verboseHome = new List<int>();
            var verboseDraw = new List<int>();
            var verboseAway = new List<int>();
            var verboseExactHome = new List<int>();
            var verboseExactDraw = new List<int>();
            var verboseExactAway = new List<int>();
            foreach (var game in lastGames)
            {
                CollectLastGamesExact(gameType, verboseExactHome, verboseExactDraw, verboseExactAway, game, bet, fileType);
                CollectLastGames(gameType, verboseHome, verboseDraw, verboseAway, game, bet, fileType);
            }
            var totals = new BettingTipsTotalsGamesContainer(this.Momentum,
                verboseHome.ToArray(),
                verboseDraw.ToArray(),
                verboseAway.ToArray(),
                verboseExactHome.ToArray(),
                verboseExactDraw.ToArray(),
                verboseExactAway.ToArray());
            return totals;
        }

        internal void CollectLastGamesExact(GameType gameType, List<int> verboseExactHome, List<int> verboseExactDraw, List<int> verboseExactAway, BettingTipsGameInfo game, BettingTipsBet bet, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BasketBallFatherExtraPoints:
                    var teamScore = game.TeamScore + (bet.BarrierHome ?? 0);
                    var OpponentScore = game.OpponentScore + (bet.BarrierGuest ?? 0);
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {
                        if (teamScore > OpponentScore)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.BasketballBarrier:
                    var totalScore = game.TeamScore + game.OpponentScore;
                    var barrier = (bet.BarrierHome ?? 0) + (bet.BarrierGuest ?? 0);
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {
                        if (totalScore > barrier)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (totalScore == barrier)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    var gols = game.TeamScore + game.OpponentScore;
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {
                        if (gols <= 1)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (gols <= 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols >= 4)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChile2point5Gols:
                    gols = game.TeamScore + game.OpponentScore;
                    if (game.Place == GameLocation.HomeGame && gameType == GameType.Home ||
                        (game.Place == GameLocation.Outside && gameType == GameType.Guest))
                    {

                        if (gols >= 3)
                        {
                            verboseExactHome.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols < 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                default://FileType.SoccerFather
                    {
                        if (gameType == GameType.Home)
                        {
                            if ((game.Place == GameLocation.HomeGame && game.Status == GameResult.Win))
                            {
                                verboseExactHome.Add(1);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(0);
                            }
                            else if ((game.Place == GameLocation.HomeGame && game.Status == GameResult.Draw))
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(1);
                                verboseExactAway.Add(0);
                            }
                            else if ((game.Place == GameLocation.HomeGame && game.Status == GameResult.Lost))
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(1);
                            }
                        }
                        else if (gameType == GameType.Guest)
                        {
                            if (game.Place == GameLocation.Outside && game.Status == GameResult.Lost)
                            {
                                verboseExactHome.Add(1);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(0);
                            }
                            else if (game.Place == GameLocation.Outside && game.Status == GameResult.Draw)
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(1);
                                verboseExactAway.Add(0);
                            }
                            else if (game.Place == GameLocation.Outside && game.Status == GameResult.Win)
                            {
                                verboseExactHome.Add(0);
                                verboseExactDraw.Add(0);
                                verboseExactAway.Add(1);
                            }
                        }
                        break;
                    }
            }
        }
        internal void CollectLastGames(GameType gameType, List<int> verboseHome, List<int> verboseDraw, List<int> verboseAway, BettingTipsGameInfo game, BettingTipsBet bet, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BasketBallFatherExtraPoints:
                    decimal teamScore;
                    decimal OpponentScore;
                    if (gameType == GameType.Home)
                    {
                        teamScore = game.TeamScore + (bet.BarrierHome ?? 0);
                        OpponentScore = game.OpponentScore + (bet.BarrierGuest ?? 0); 
                    }
                    else
                    {
                        teamScore = game.TeamScore + (bet.BarrierGuest ?? 0);
                        OpponentScore = game.OpponentScore + (bet.BarrierHome ?? 0);
                    }
                    if (teamScore > OpponentScore)
                    {
                        verboseHome.Add(gameType == GameType.Home ? 1 : 0);
                        verboseDraw.Add(0);
                        verboseAway.Add(gameType == GameType.Home ? 0 : 1);
                    }
                    else if (teamScore == OpponentScore)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else
                    {
                        verboseHome.Add(gameType == GameType.Home ? 0 : 1);
                        verboseDraw.Add(0);
                        verboseAway.Add(gameType == GameType.Home? 1 : 0);
                    }
                    break;
                case FileType.BasketballBarrier:
                    var totalScore = game.TeamScore + game.OpponentScore;
                    var barrier = (bet.BarrierHome ?? 0) + (bet.BarrierGuest ?? 0);
                    if (totalScore > barrier)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (totalScore == barrier)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    var gols = game.TeamScore + game.OpponentScore;
                    if (gols <= 1)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (gols <= 3)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols >= 4)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    break;
                case FileType.SoccerChile2point5Gols:
                    gols = game.TeamScore + game.OpponentScore;
                    if (gols >= 3)
                    {
                        verboseHome.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols < 3)
                    {
                        verboseHome.Add(0);
                        verboseAway.Add(1);
                    }
                    break;
                default://FileType.SoccerFather
                    {
                        if ((gameType == GameType.Home && game.Status == GameResult.Win) ||
                            (gameType == GameType.Guest && game.Status == GameResult.Lost))
                        {
                            verboseHome.Add(1);
                            verboseDraw.Add(0);
                            verboseAway.Add(0);
                        }
                        else if ((gameType == GameType.Home && game.Status == GameResult.Draw) ||
                                 (gameType == GameType.Guest && game.Status == GameResult.Draw))
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(1);
                            verboseAway.Add(0);
                        }
                        if ((gameType == GameType.Home && game.Status == GameResult.Lost) ||
                            (gameType == GameType.Guest && game.Status == GameResult.Win))
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(0);
                            verboseAway.Add(1);
                        }
                    }
                    break;
            }
        }

        internal BettingTipsTotalsGamesContainer PrepareHead2Head(string homeTeamName, string guestTeamName, List<BettingTipsHeadeToHead> head2Head, BettingTipsBet bet, FileType fileType)
        {
            var verboseHome = new List<int>();
            var verboseDraw = new List<int>();
            var verboseAway = new List<int>();
            var verboseExactHome = new List<int>();
            var verboseExactDraw = new List<int>();
            var verboseExactAway = new List<int>();

            foreach (var game in head2Head)
            {
                CollectHead2Head(homeTeamName, guestTeamName, verboseHome, verboseDraw, verboseAway, verboseExactHome, verboseExactDraw, verboseExactAway, game, bet, fileType);
            }
            var totals = new BettingTipsTotalsGamesContainer(this.Momentum,
                verboseHome.ToArray(),
                verboseDraw.ToArray(),
                verboseAway.ToArray(),
                verboseExactHome.ToArray(),
                verboseExactDraw.ToArray(),
                verboseExactAway.ToArray());
            return totals;
        }

        private static void CollectHead2Head(string homeTeamName, string guestTeamName, List<int> verboseHome, List<int> verboseDraw, List<int> verboseAway, List<int> verboseExactHome, List<int> verboseExactDraw, List<int> verboseExactAway, BettingTipsHeadeToHead game, BettingTipsBet bet, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BasketBallFatherExtraPoints:
                    decimal teamScore;
                    decimal OpponentScore;
                    if (guestTeamName == game.Home)
                    {
                        teamScore = game.HomeScore + (bet.BarrierGuest ?? 0);
                        OpponentScore = game.GuestScore + (bet.BarrierHome ?? 0);
                        if (teamScore > OpponentScore)
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(0);
                            verboseAway.Add(1);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(1);
                            verboseAway.Add(0);
                        }
                        else
                        {
                            verboseHome.Add(1);
                            verboseDraw.Add(0);
                            verboseAway.Add(0);
                        }
                    }
                    else
                    {
                        teamScore = game.HomeScore + (bet.BarrierHome ?? 0);
                        OpponentScore = game.GuestScore + (bet.BarrierGuest ?? 0);
                        if (teamScore > OpponentScore)
                        {
                            verboseHome.Add(1);
                            verboseDraw.Add(0);
                            verboseAway.Add(0);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(1);
                            verboseAway.Add(0);
                        }
                        else
                        {
                            verboseHome.Add(0);
                            verboseDraw.Add(0);
                            verboseAway.Add(1);
                        }
                    }

                    if (game.Home == homeTeamName)
                    {
                        if (teamScore > OpponentScore)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (teamScore == OpponentScore)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.BasketballBarrier:
                    var totalScore = game.HomeScore + game.GuestScore;
                    var barrier = (bet.BarrierHome ?? 0) + (bet.BarrierGuest ?? 0);
                    if (totalScore > barrier)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (totalScore == barrier)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    if (game.Home == homeTeamName)
                    {
                        if (totalScore > barrier)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (totalScore == barrier)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChild23Gols:
                    var gols = game.HomeScore + game.GuestScore;
                    if (gols <= 1)
                    {
                        verboseHome.Add(1);
                        verboseDraw.Add(0);
                        verboseAway.Add(0);
                    }
                    else if (gols <= 3)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols >= 4)
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(0);
                        verboseAway.Add(1);
                    }
                    if (game.Home == homeTeamName)
                    {
                        if (gols <= 1)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (gols <= 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols >= 4)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                case FileType.SoccerChile2point5Gols:
                    gols = game.HomeScore + game.GuestScore;
                    if (gols >= 3)
                    {
                        verboseHome.Add(1);
                        verboseAway.Add(0);
                    }
                    else if (gols < 3)
                    {
                        verboseHome.Add(0);
                        verboseAway.Add(1);
                    }
                    if (game.Home == homeTeamName)
                    {
                        if (gols >= 3)
                        {
                            verboseExactHome.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else if (gols < 3)
                        {
                            verboseExactHome.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
                default://FileType.SoccerFather.
                    if (game.HomeScore > game.GuestScore)
                    {
                        verboseHome.Add(game.Home == homeTeamName ? 1 : 0);
                        verboseDraw.Add(0);
                        verboseAway.Add(game.Home == guestTeamName ? 1 : 0);
                    }
                    else if (game.HomeScore < game.GuestScore)
                    {
                        verboseHome.Add(game.Guest == homeTeamName ? 1 : 0);
                        verboseDraw.Add(0);
                        verboseAway.Add(game.Guest == guestTeamName ? 1 : 0);
                    }
                    else
                    {
                        verboseHome.Add(0);
                        verboseDraw.Add(1);
                        verboseAway.Add(0);
                    }

                    if (game.Home == homeTeamName)
                    {
                        if (game.HomeScore > game.GuestScore)
                        {
                            verboseExactHome.Add(1);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(0);
                        }
                        else if (game.HomeScore == game.GuestScore)
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(1);
                            verboseExactAway.Add(0);
                        }
                        else
                        {
                            verboseExactHome.Add(0);
                            verboseExactDraw.Add(0);
                            verboseExactAway.Add(1);
                        }
                    }
                    break;
            }
        }
    }
}
