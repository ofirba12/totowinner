﻿namespace TotoWinner.BettingTips
{
    internal static class Constants
    {
        internal const string RepositoryPath = "Repository";
        internal const string ResultPath = "Result";
    }
    internal enum ResolveEnum
    {
        INT,
        BOOL,
        DATE,
        DECIMAL,
        STRING
    }
    internal enum GameResult
    {
        Win = 0,
        Draw = 1,
        Lost = 2,
    }
    internal enum FileType
    {
        SoccerFather,
        SoccerChild23Gols,
        SoccerChile2point5Gols,
        BasketballBarrier,
        BasketBallFatherExtraPoints
    }
    internal enum GameType
    {
        Home,
        Guest
    }
    internal enum GameLocation
    {
        HomeGame = 0,
        Outside
    }
}
