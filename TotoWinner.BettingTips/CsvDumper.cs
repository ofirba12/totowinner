﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace TotoWinner.BettingTips
{
    internal class CsvDumper
    {
        private Dictionary<int, BettingTipsParentEvent> RoundItems;
        private int RoundId;
        private int Momentum;
        private bool Verbose;
        private CsvLinesCollector Collector;
        private const string seperator = "  ";

        internal CsvDumper(Dictionary<int, BettingTipsParentEvent> roundItems, int roundId, int momentum, bool verbose)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            this.RoundItems = (roundItems as Dictionary<int, BettingTipsParentEvent>);
            this.RoundId = roundId;
            this.Momentum = momentum;
            this.Verbose = verbose;
            this.Collector = new CsvLinesCollector(momentum);
        }
        internal CsvDumper DumpSoccerFatherRounds()
        {
            var soccerFatherRounds = this.RoundItems.Values
                .Where(i => i.SportId == 2 && i.IsFather == true)
                .ToList();
            var filename = DumpResult(soccerFatherRounds, this.RoundId, FileType.SoccerFather);
            Console.WriteLine($"File {Helpers.Hebrew(filename.Replace(".csv", ""))} CSV has been created under {Constants.ResultPath} folder.");
            return this;
        }
        internal CsvDumper DumpSoccerChild23Rounds()
        {
            var soccerChild23Rounds = this.RoundItems.Values
                .Where(i => i.SportId == 2 && i.IsFather == false && i.Bet.Name.Contains("(2-3)"))
                .ToList();
            var filename = DumpResult(soccerChild23Rounds, this.RoundId, FileType.SoccerChild23Gols);
            Console.WriteLine($"File {Helpers.Hebrew(filename.Replace(".csv", ""))} CSV has been created under {Constants.ResultPath} folder.");
            return this;
        }
        internal CsvDumper DumpSoccerChild25Rounds()
        {
            var soccerChild25Rounds = this.RoundItems.Values
                .Where(i => i.SportId == 2 && i.IsFather == false && i.Bet.Name.Contains(@"מעל/מתחת שערים (2.5)"))
                .ToList();
            var filename = DumpResult(soccerChild25Rounds, this.RoundId, FileType.SoccerChile2point5Gols);
            Console.WriteLine($"File {Helpers.Hebrew(filename.Replace(".csv", ""))} CSV has been created under {Constants.ResultPath} folder.");
            return this;
        }
        internal CsvDumper DumpBasketballBarrierRounds()
        {
            var basketballBarrierRounds = this.RoundItems.Values
                .Where(i => i.SportId == 5 && i.IsFather == false && 
                    i.Bet.Name.Contains(@"מעל/מתחת נקודות") && 
                    !i.Bet.Period.Contains("מחצית") &&
                    !i.Bet.Period.Contains("רבע"))
                .ToList();
            var filename = DumpResult(basketballBarrierRounds, this.RoundId, FileType.BasketballBarrier);
            Console.WriteLine($"File {Helpers.Hebrew(filename.Replace(".csv", ""))} CSV has been created under {Constants.ResultPath} folder.");
            return this;
        }
        internal CsvDumper DumpBasketBallFatherExtraPointsRounds()
        {
            var basketBallFatherExtraPointsRounds = this.RoundItems.Values
                .Where(i => i.SportId == 5 && i.IsFather == true && i.Bet.Name.Contains(@"+)"))
                .ToList();
            var filename = DumpResult(basketBallFatherExtraPointsRounds, this.RoundId, FileType.BasketBallFatherExtraPoints);
            Console.WriteLine($"File {Helpers.Hebrew(filename.Replace(".csv", ""))} CSV has been created under {Constants.ResultPath} folder.");
            return this;
        }
        private string DumpResult(List<BettingTipsParentEvent> roundItems, int roundId, FileType fileType)
        {
            var filename = string.Empty;
            switch (fileType)
            {
                case FileType.SoccerFather:
                    filename = $"כדורגל אבא.csv";
                    break;
                case FileType.SoccerChild23Gols:
                    filename = $"כדורגל 2_3 שערים.csv";
                    break;
                case FileType.SoccerChile2point5Gols:
                    filename = $"כדורגל 2 וחצי שערים.csv";
                    break;
                case FileType.BasketballBarrier:
                    filename = $"כדורסל מעל מתחת נקודות.csv";
                    break;
                case FileType.BasketBallFatherExtraPoints:
                    filename = $"כדורסל אבא תוספת נקודות.csv";
                    break;
            }
            var filepath = Path.Combine(Constants.ResultPath, $"{roundId}_{filename}");
            var csvColumnTitles = GetAllTitles();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(filepath, false, System.Text.Encoding.UTF8))
            {
                file.WriteLine(csvColumnTitles);
                foreach (var item in roundItems)
                {
                    var lines = GenerateCsvLine(item, fileType);
                    foreach (var csvLine in lines)
                        file.WriteLine(string.Join(",", csvLine));
                    if (lines.Count > 0)
                        file.WriteLine("");
                }
            }
            return filename;
        }
        private string GetAllTitles()
        {
            var columnSeperator = !this.Verbose ? $",{seperator}" : "";
            var homeLastGameDataTitles = $"HomeLastGame,HomeLastGame Momentum{columnSeperator}";
            if (this.Verbose)
                homeLastGameDataTitles = $"{homeLastGameDataTitles},lastGameHome#1,lastGameHome#2,lastGameHome#3,lastGameHome#4,lastGameHome#5,lastGameHome#6,lastGameHome#7,lastGameHome#8,lastGameHome#9,lastGameHome#10";

            var guestLastGameDataTitles = $"GuestLastGame,GuestLastGame Momentum{columnSeperator}";
            if (this.Verbose)
                guestLastGameDataTitles = $"{guestLastGameDataTitles},lastGameGuest#1,lastGameGuest#2,lastGameGuest#3,lastGameGuest#4,lastGameGuest#5,lastGameGuest#6,lastGameGuest#7,lastGameGuest#8,lastGameGuest#9,lastGameGuest#10";

            var homeLastGameExactDataTitles =$"HomeLastGameExact,HomeLastGameExact Momentum{columnSeperator}";
            if (this.Verbose)
                homeLastGameExactDataTitles = $"{homeLastGameExactDataTitles},lastGameHomeExact#1,lastGameHomeExact#2,lastGameHomeExact#3,lastGameHomeExact#4,lastGameHomeExact#5,lastGameHomeExact#6,lastGameHomeExact#7,lastGameHomeExact#8,lastGameHomeExact#9,lastGameHomeExact#10";

            var guestLastGameExactDataTitles = $"GuestLastGameExact,GuestLastGameExact Momentum{columnSeperator}";
            if (this.Verbose)
                guestLastGameExactDataTitles = $"{guestLastGameExactDataTitles},lastGameGuestExact#1,lastGameGuestExact#2,lastGameGuestExact#3,lastGameGuestExact#4,lastGameGuestExact#5,lastGameGuestExact#6,lastGameGuestExact#7,lastGameGuestExact#8,lastGameGuestExact#9,lastGameGuestExact#10";

            var h2hDataTitles = $"Head,Head Momentum{columnSeperator}";
            if (this.Verbose)
                h2hDataTitles = $"{h2hDataTitles},headTable#1,headTable#2,headTable#3,headTable#4,headTable#5,headTable#6,headTable#7,headTable#8,headTable#9,headTable#10";

            var h2hExactDataTitles = $"HeadExact,HeadExact Momentum";
            if (this.Verbose)
                h2hExactDataTitles = $"{h2hExactDataTitles},headTableExact#1,headTableExact#2,headTableExact#3,headTableExact#4,headTableExact#5,headTableExact#6,headTableExact#7,headTableExact#8,headTableExact#9,headTableExact#10";

            return $"bet number, time, status, bet name,league,bet type, direction, odd,{homeLastGameDataTitles},{guestLastGameDataTitles},{homeLastGameExactDataTitles},{guestLastGameExactDataTitles},{h2hDataTitles},{h2hExactDataTitles}";
        }

        private List<List<string>> GenerateCsvLine(BettingTipsParentEvent bettingTipsParentEvent, FileType fileType)
        {
            var baseValue = new List<string>();
            var lines = new List<List<string>>();
            baseValue.Add(bettingTipsParentEvent.Place.ToString());
            baseValue.Add(bettingTipsParentEvent.Time.ToString("t"));
            baseValue.Add(bettingTipsParentEvent.Status);
            baseValue.Add(bettingTipsParentEvent.Bet.Name.Replace(',', ';'));
            baseValue.Add(bettingTipsParentEvent.LeagueName.Replace(',', ';'));
            baseValue.Add(bettingTipsParentEvent.IsFather ? "אבא" : "בן");

            var homeLastGameTotals = this.Collector.PrepareLastGames(bettingTipsParentEvent.LastGames.Games[bettingTipsParentEvent.HomeId], bettingTipsParentEvent.Bet, GameType.Home, fileType);
            var guestLastGameTotals = this.Collector.PrepareLastGames(bettingTipsParentEvent.LastGames.Games[bettingTipsParentEvent.GuestId], bettingTipsParentEvent.Bet, GameType.Guest, fileType);
            var h2h = this.Collector.PrepareHead2Head(bettingTipsParentEvent.HomeTeamName, bettingTipsParentEvent.GuestTeamName, bettingTipsParentEvent.Head2Head, bettingTipsParentEvent.Bet, fileType);

            var summaryLine = SummaryLine(baseValue.Count, homeLastGameTotals, guestLastGameTotals, h2h);
            var homeLine = HomeLine(baseValue, bettingTipsParentEvent.Bet.Home, homeLastGameTotals, guestLastGameTotals, h2h);
            var drawLine = DrawLine(baseValue, bettingTipsParentEvent.Bet.Draw, homeLastGameTotals, guestLastGameTotals, h2h);
            var awayLine = AwayLine(baseValue, bettingTipsParentEvent.Bet.Away, homeLastGameTotals, guestLastGameTotals, h2h);

            lines.Add(summaryLine);
            lines.Add(homeLine);
            if (bettingTipsParentEvent.Bet.Draw != -1)
                lines.Add(drawLine);
            lines.Add(awayLine);

            return lines;
        }

        private void GenerateGroupLine(List<string> line, decimal winPercent, decimal momentumWinPercent, List<int> verbose, bool isSummaryLine = false)
        {
            var percent = !isSummaryLine ? "%" : "";
            line.Add($"{Math.Round(winPercent, 0).ToString()}{percent}");
            line.Add($"{Math.Round(momentumWinPercent, 0).ToString()}{percent}");
            if (!this.Verbose)
                line.Add($"{seperator}");

            if (this.Verbose)
            {
                verbose.ForEach(i => line.Add(isSummaryLine
                    ? ""
                    : i.ToString()));
                for (var i = 0; i < 10 - verbose.Count(); i++)
                    line.Add("");
            }
        }
        private List<string> HomeLine(List<string> baseValue, decimal bet, BettingTipsTotalsGamesContainer homeLastGameTotals, BettingTipsTotalsGamesContainer guestLastGameTotals, BettingTipsTotalsGamesContainer h2h)
        {
            var line = new List<string>(baseValue);
            line.Add("Home");
            line.Add(bet.ToString());

            GenerateGroupLine(line, homeLastGameTotals.HomeWinPercent, homeLastGameTotals.HomeMomentumWinPercent, homeLastGameTotals.VerboseHome.ToList());
            GenerateGroupLine(line, guestLastGameTotals.HomeWinPercent, guestLastGameTotals.HomeMomentumWinPercent, guestLastGameTotals.VerboseHome.ToList());
            GenerateGroupLine(line, homeLastGameTotals.HomeExactWinPercent, homeLastGameTotals.HomeExactMomentumWinPercent, homeLastGameTotals.VerboseExactHome.ToList());
            GenerateGroupLine(line, guestLastGameTotals.HomeExactWinPercent, guestLastGameTotals.HomeExactMomentumWinPercent, guestLastGameTotals.VerboseExactHome.ToList());
            GenerateGroupLine(line, h2h.HomeWinPercent, h2h.HomeMomentumWinPercent, h2h.VerboseHome.Take(10).ToList());
            GenerateGroupLine(line, h2h.HomeExactWinPercent, h2h.HomeExactMomentumWinPercent, h2h.VerboseExactHome.Take(10).ToList());

            return line;
        }

        private List<string> DrawLine(List<string> baseValue, decimal bet, BettingTipsTotalsGamesContainer homeLastGameTotals, BettingTipsTotalsGamesContainer guestLastGameTotals, BettingTipsTotalsGamesContainer h2h)
        {
            var line = new List<string>(baseValue);
            line.Add("Draw");
            line.Add(bet.ToString());

            GenerateGroupLine(line, homeLastGameTotals.DrawWinPercent, homeLastGameTotals.DrawMomentumWinPercent, homeLastGameTotals.VerboseDraw.ToList());
            GenerateGroupLine(line, guestLastGameTotals.DrawWinPercent, guestLastGameTotals.DrawMomentumWinPercent, guestLastGameTotals.VerboseDraw.ToList());
            GenerateGroupLine(line, homeLastGameTotals.DrawExactWinPercent, homeLastGameTotals.DrawExactMomentumWinPercent, homeLastGameTotals.VerboseExactDraw.ToList());
            GenerateGroupLine(line, guestLastGameTotals.DrawExactWinPercent, guestLastGameTotals.DrawExactMomentumWinPercent, guestLastGameTotals.VerboseExactDraw.ToList());
            GenerateGroupLine(line, h2h.DrawWinPercent, h2h.DrawMomentumWinPercent, h2h.VerboseDraw.Take(10).ToList());
            GenerateGroupLine(line, h2h.DrawExactWinPercent, h2h.DrawExactMomentumWinPercent, h2h.VerboseExactDraw.Take(10).ToList());

            return line;
        }

        private List<string> AwayLine(List<string> baseValue, decimal bet, BettingTipsTotalsGamesContainer homeLastGameTotals, BettingTipsTotalsGamesContainer guestLastGameTotals, BettingTipsTotalsGamesContainer h2h)
        {
            var line = new List<string>(baseValue);
            line.Add("Away");
            line.Add(bet.ToString());


            GenerateGroupLine(line, homeLastGameTotals.AwayWinPercent, homeLastGameTotals.AwayMomentumWinPercent, homeLastGameTotals.VerboseAway.ToList());
            GenerateGroupLine(line, guestLastGameTotals.AwayWinPercent, guestLastGameTotals.AwayMomentumWinPercent, guestLastGameTotals.VerboseAway.ToList());
            GenerateGroupLine(line, homeLastGameTotals.AwayExactWinPercent, homeLastGameTotals.AwayExactMomentumWinPercent, homeLastGameTotals.VerboseExactAway.ToList());
            GenerateGroupLine(line, guestLastGameTotals.AwayExactWinPercent, guestLastGameTotals.AwayExactMomentumWinPercent, guestLastGameTotals.VerboseExactAway.ToList());
            GenerateGroupLine(line, h2h.AwayWinPercent, h2h.AwayMomentumWinPercent, h2h.VerboseAway.Take(10).ToList());
            GenerateGroupLine(line, h2h.AwayExactWinPercent, h2h.AwayExactMomentumWinPercent, h2h.VerboseExactAway.Take(10).ToList());

            return line;
        }

        private List<string> SummaryLine(int emptyCounter , BettingTipsTotalsGamesContainer homeLastGameTotals, BettingTipsTotalsGamesContainer guestLastGameTotals, BettingTipsTotalsGamesContainer h2h)
        {
            var line = new List<string>();
            for (var i = 0; i < emptyCounter + 2; i++)
                line.Add("");

            GenerateGroupLine(line, homeLastGameTotals.VerboseHome.Count(), this.Momentum, homeLastGameTotals.VerboseHome.ToList(), true);
            GenerateGroupLine(line, homeLastGameTotals.VerboseAway.Count(), this.Momentum, homeLastGameTotals.VerboseAway.ToList(), true);
            GenerateGroupLine(line, homeLastGameTotals.VerboseExactHome.Count(), this.Momentum, homeLastGameTotals.VerboseExactHome.ToList(), true);
            GenerateGroupLine(line, homeLastGameTotals.VerboseExactAway.Count(), this.Momentum, homeLastGameTotals.VerboseExactAway.ToList(), true);
            GenerateGroupLine(line, h2h.VerboseHome.Count(), this.Momentum, h2h.VerboseHome.Take(10).ToList(), true);
            GenerateGroupLine(line, h2h.VerboseExactHome.Count(), this.Momentum, h2h.VerboseExactHome.Take(10).ToList(), true);

            return line;
        }
    }
}
