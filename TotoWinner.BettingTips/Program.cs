﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TotoWinner.BettingTips
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            try
            {
                Initialize();
                var menu = new Menu().Show();
                var roundItems = new Parser().ParseWinnerLineResponse(menu.MarketDate, menu.RoundId, new List<int>() { 2, 5 });
                var csvDumper = new CsvDumper(roundItems, menu.RoundId, menu.Momentum, menu.Verbose)
                    .DumpSoccerFatherRounds()
                    .DumpSoccerChild23Rounds()
                    .DumpSoccerChild25Rounds()
                    .DumpBasketballBarrierRounds()
                    .DumpBasketBallFatherExtraPointsRounds();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                _log.Fatal("An unexpected error occurred.", ex);
            }
            finally
            {
                Console.WriteLine($"{Environment.NewLine}Press any key to exist:");
                Console.ReadLine();
            }
        }

        private static void Initialize()
        {
            if (!Directory.Exists(Constants.RepositoryPath))
                Directory.CreateDirectory(Constants.RepositoryPath);
            if (!Directory.Exists(Constants.ResultPath))
                Directory.CreateDirectory(Constants.ResultPath);
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1255"); //for hebrew
        }
    }
}
