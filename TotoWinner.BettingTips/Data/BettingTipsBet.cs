﻿using System.Text.RegularExpressions;

namespace TotoWinner.BettingTips
{
    internal class BettingTipsBet
    {
        internal decimal Home;
        internal decimal Draw;
        internal decimal Away;
        internal string Name;
        internal string Period;
        internal decimal? BarrierHome;
        internal decimal? BarrierGuest;

        public BettingTipsBet(decimal homeBet, decimal drawBet, decimal awayBet, string name, string period, decimal? barrierHome, decimal? barrierGuest)
        {
            this.Home = homeBet;
            this.Draw = drawBet;
            this.Away = awayBet;
            this.Name = name;
            this.Period = period;
            this.BarrierHome = barrierHome;
            this.BarrierGuest = barrierGuest;
        }
    }
}