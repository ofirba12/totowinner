﻿using System;
using System.Linq;

namespace TotoWinner.BettingTips
{
    internal class BettingTipsTotalsGamesContainer
    {
        internal int Home { get; }
        internal int HomeMomentum { get; }
        internal decimal HomeWinPercent { get; }
        internal decimal HomeMomentumWinPercent { get; }
        internal int[] VerboseHome { get; }

        internal int Draw { get; }
        internal int DrawMomentum { get; }
        internal decimal DrawWinPercent { get; }
        internal decimal DrawMomentumWinPercent { get; }
        internal int[] VerboseDraw { get; }

        internal int Away { get; }
        internal int AwayMomentum { get; }
        internal decimal AwayWinPercent { get; }
        internal decimal AwayMomentumWinPercent { get; }
        internal int[] VerboseAway { get; }

        internal int HomeExact { get; }
        internal int HomeExactMomentum { get; }
        internal decimal HomeExactWinPercent { get; }
        internal decimal HomeExactMomentumWinPercent { get; }
        internal int[] VerboseExactHome { get; }

        internal int DrawExact { get; }
        internal int DrawExactMomentum { get; }
        internal decimal DrawExactWinPercent { get; }
        internal decimal DrawExactMomentumWinPercent { get; }
        internal int[] VerboseExactDraw { get; }

        internal int AwayExact { get; }
        internal int AwayExactMomentum { get; }
        internal decimal AwayExactWinPercent { get; }
        internal decimal AwayExactMomentumWinPercent { get; }
        internal int[] VerboseExactAway { get; }

        public BettingTipsTotalsGamesContainer(int momentum, 
            int[] verboseHome, int[] verboseDraw, int[] verboseAway, 
            int[] verboseExactHome, int[] verboseExactDraw, int[] verboseExactAway)
        {
            this.Home = verboseHome.Sum();
            this.HomeMomentum = verboseHome.Take(momentum).Sum();
            this.VerboseHome = verboseHome;
            this.HomeWinPercent = verboseHome.Count() > 0 
                ? (decimal)this.Home / verboseHome.Count() * 100
                : 0;
            var actualMomentum = Math.Min(momentum, verboseHome.Take(momentum).Count());
            this.HomeMomentumWinPercent = (decimal)this.HomeMomentum / Math.Max(1,actualMomentum) * 100;

            this.Draw = verboseDraw.Sum();
            this.DrawMomentum = verboseDraw.Take(momentum).Sum();
            this.VerboseDraw = verboseDraw;
            this.DrawWinPercent = verboseDraw.Count() > 0
                ? (decimal)this.Draw / verboseDraw.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseDraw.Take(momentum).Count());
            this.DrawMomentumWinPercent = (decimal)this.DrawMomentum / Math.Max(1, actualMomentum) * 100;

            this.Away = verboseAway.Sum();
            this.AwayMomentum = verboseAway.Take(momentum).Sum();
            this.VerboseAway = verboseAway;
            this.AwayWinPercent = verboseAway.Count() > 0
                ? (decimal)this.Away / verboseAway.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseAway.Take(momentum).Count());
            this.AwayMomentumWinPercent = (decimal)this.AwayMomentum / Math.Max(1, actualMomentum) * 100;

            this.HomeExact = verboseExactHome.Sum();
            this.HomeExactMomentum = verboseExactHome.Take(momentum).Sum();
            this.VerboseExactHome = verboseExactHome;
            this.HomeExactWinPercent = verboseExactHome.Count() > 0
                ? (decimal)this.HomeExact / verboseExactHome.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseExactHome.Take(momentum).Count());
            this.HomeExactMomentumWinPercent = (decimal)this.HomeExactMomentum / Math.Max(1, actualMomentum) * 100;

            this.DrawExact = verboseExactDraw.Sum();
            this.DrawExactMomentum = verboseExactDraw.Take(momentum).Sum();
            this.VerboseExactDraw = verboseExactDraw;
            this.DrawExactWinPercent = verboseExactDraw.Count() > 0
                ? (decimal)this.DrawExact / verboseExactDraw.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseExactDraw.Take(momentum).Count());
            this.DrawExactMomentumWinPercent = (decimal)this.DrawExactMomentum / Math.Max(1, actualMomentum) * 100;

            this.AwayExact = verboseExactAway.Sum();
            this.AwayExactMomentum = verboseExactAway.Take(momentum).Sum();
            this.VerboseExactAway = verboseExactAway;
            this.AwayExactWinPercent = verboseExactAway.Count() > 0
                ? (decimal)this.AwayExact / verboseExactAway.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseExactAway.Take(momentum).Count());
            this.AwayExactMomentumWinPercent = (decimal)this.AwayExactMomentum / Math.Max(1, actualMomentum) * 100;
        }

    }
}
