﻿using System.Collections.Generic;

namespace TotoWinner.BettingTips
{
    internal class BettingTipsLastGames
    {
        internal Dictionary<int, List<BettingTipsGameInfo>> Games;

        public BettingTipsLastGames(int homeId, int guestId, 
            List<BettingTipsGameInfo> homeGames, 
            List<BettingTipsGameInfo> guestGames)
        {
            this.Games = new Dictionary<int, List<BettingTipsGameInfo>>();
            this.Games.Add(homeId, homeGames);
            this.Games.Add(guestId, guestGames);
        }
    }
}