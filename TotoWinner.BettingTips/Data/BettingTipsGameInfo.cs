﻿using System;

namespace TotoWinner.BettingTips
{
    internal class BettingTipsGameInfo
    {
        internal DateTime Date { get; }
        internal GameLocation Place { get; }
        internal string Opponent { get; }
        internal GameResult Status { get; }
        internal string League { get; }
        internal int TeamScore { get; }
        internal int OpponentScore { get; }

        public BettingTipsGameInfo(DateTime date, int place, string opponent, int status, string league, string score, string teamName)
        {
            try
            {
                this.Date = date;
                this.Place = (GameLocation)place;
                this.Opponent = opponent;
                this.Status = Enum.IsDefined(typeof(GameResult), status)
                    ? (GameResult)status
                    : throw new Exception($"status can not be converted to GameResult, must be 0,1,2");
                this.League = league;
                var scores = score.Split(':');
                var opponentScore = this.Place == GameLocation.Outside
                    ? scores[0]
                    : scores[1];
                var teamScore = this.Place == GameLocation.Outside
                    ? scores[1]
                    : scores[0];
                this.TeamScore = int.Parse(teamScore);
                this.OpponentScore = int.Parse(opponentScore);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to create BettingTipsGameInfo for {date.ToString("t")} place: {place}, opponent: {opponent}, status: {status}, league:{league}, score: {score}, teamName: {teamName} ", ex);
            }
        }
    }
}