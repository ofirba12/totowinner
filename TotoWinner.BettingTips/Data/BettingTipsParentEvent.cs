﻿using System;
using System.Collections.Generic;

namespace TotoWinner.BettingTips
{
    internal class BettingTipsParentEvent
    {
        internal int EventId { get; }
        internal int SportId { get; }
        internal int RoundId { get; }
        internal int Place { get; }
        internal string LeagueName { get; }
        internal bool IsFather { get; }
        internal DateTime Time { get; }
        internal string Status { get; }
        internal int HomeId { get; }
        internal string HomeTeamName { get; }
        internal int GuestId { get; }
        internal string GuestTeamName { get; }
        internal BettingTipsBet Bet { get; }
        internal BettingTipsLastGames LastGames { get; }
        internal List<BettingTipsHeadeToHead> Head2Head { get; }

        public BettingTipsParentEvent(
            int eventId,
            int sportId,
            int roundId,
            int place,
            string leagueName,
            bool isFather,
            DateTime time,
            string status,
            int homeId,
            string homeTeamName,
            int guestId,
            string guestTeamName,
            BettingTipsBet bet,
            BettingTipsLastGames lastGames,
            List<BettingTipsHeadeToHead> h2h)
        {
            this.EventId = eventId;
            this.SportId = sportId;
            this.RoundId = roundId;
            this.Place = place;
            this.LeagueName = leagueName;
            this.IsFather = isFather;
            this.Time = time;
            this.Status = status;
            this.HomeId = homeId;
            this.HomeTeamName = homeTeamName;
            this.GuestId = guestId;
            this.GuestTeamName = guestTeamName;
            this.Bet = bet;
            this.LastGames = lastGames;
            this.Head2Head = h2h;
        }
    }
}