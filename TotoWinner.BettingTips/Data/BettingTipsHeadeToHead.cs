﻿using System;

namespace TotoWinner.BettingTips
{
    internal class BettingTipsHeadeToHead
    {
        internal DateTime Date;
        internal string Home;
        internal string Guest;
        internal int HomeScore;
        internal int GuestScore;
        internal string League;

        public BettingTipsHeadeToHead(DateTime date, string home, string guest, string score, string league)
        {
            try
            {
                this.Date = date;
                this.Home = home;
                this.Guest = guest;
                var scores = score.Split(':');
                this.HomeScore = int.Parse(scores[0]);
                this.GuestScore = int.Parse(scores[1]);
                this.League = league;
            }
            catch(Exception ex)
            {
                throw new Exception($"Error occured for date={date}, home={guest}, score={score}, league={league}", ex);
            }
        }
    }
}