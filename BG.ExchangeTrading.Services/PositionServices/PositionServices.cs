﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TotoWinner.Common.Infra;

namespace BG.ExchangeTrading.Services
{
    public class PositionServices : Singleton<PositionServices>, IPositionServices
    {
        private PositionPersistanceServices persistance = PositionPersistanceServices.Instance;
        private PositionServices()
        { }

        public void AddPortfolioAsset(PortfolioAsset asset)
        {
            try
            {
                persistance.AddPortfolioAsset(asset.PortfolioId,
                    asset.TickerId,
                    asset.TickerExpiry,
                    asset.IsCall,
                    asset.Strike,
                    asset.IsLong,
                    asset.Premium,
                    asset.Size,
                    asset.Fee);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add profolio asset: {asset}", ex);
            }
        }

        public List<Portfolio> GetAllPortfolios()
        {
            try
            {
                var portfolios = persistance.GetAllPorfolios()
                    .ConvertAll<Portfolio>(p => new Portfolio(p.PortfolioId, p.Expiry));
                return portfolios;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all portfolios", ex);
            }
        }

        public Dictionary<TickerIdExpiry, double> GetLastEodSpotPerExpiry(List<TickerIdExpiry> tickers)
        {
            try
            {
                var distinctTickers = tickers.Distinct(new TickerIdExpiryComparer()).ToList();
                var result = persistance.GetTickersDataPerExpiry(distinctTickers);
                var tickerDict = result.ToDictionary(x => new TickerIdExpiry( x.TickerId, x.Expiry), x => x.ClosePrice);
                return tickerDict;
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occurred trying to get last EOD for spot per expiry", ex);
            }
        }

        public List<PortfolioAsset> GetPortfolioAssets(int portfolioId)
        {
            try
            {
                var tickers = TickersPersistanceServices.Instance.GetExisitingTickers();
                var assets = persistance.GetPortfolioAssets(portfolioId).
                    ConvertAll<PortfolioAsset>(p =>
                    {
                        var ticker = tickers.First(t => t.TickerId == p.TickerId);
                        return new PortfolioAsset(p.PortfolioId,
                            p.AssetId,
                            p.TickerId,
                            ticker.Symbol,
                            ticker.FullName,
                            ticker.Type,
                            ticker.Group,
                            p.TickerExpiry,
                            p.IsCall.Value,
                            p.Strike.Value,
                            p.IsLong.Value,
                            p.Premium.Value,
                            p.Size.Value,
                            p.Fee.Value);
                        });
                return assets;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get portfolio assets for porfolioId {portfolioId}", ex);
            }
        }

        public int CalculateFairValue(bool isCall, bool isLong, double strike, int size, double? lastSpot)
        {
            var fv = 0;
            if (lastSpot.HasValue)
            {
                if (!isCall && !isLong)
                    fv = (int)(strike <= lastSpot.Value 
                        ? 0
                        : -(strike-lastSpot.Value)*size*100);
                else if (!isCall && isLong)
                    fv = (int)(strike <= lastSpot.Value
                        ? 0
                        : (strike - lastSpot.Value) * size * 100);
                else if (isCall && !isLong)
                    fv = (int)(strike >= lastSpot.Value
                        ? 0
                        : -(lastSpot.Value - strike) * size * 100);
                else if (isCall && isLong)
                    fv = (int)(strike >= lastSpot.Value
                        ? 0
                        : (lastSpot.Value - strike) * size * 100);
            }

            return fv;
        }
    }
}
