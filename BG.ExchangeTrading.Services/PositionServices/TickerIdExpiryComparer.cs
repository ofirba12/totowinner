﻿using BG.ExchangeTrading.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Services
{
    public class TickerIdExpiryComparer : IEqualityComparer<TickerIdExpiry>
    {
        public bool Equals(TickerIdExpiry x, TickerIdExpiry y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(TickerIdExpiry obj)
        {
            return obj.GetHashCode();
        }
    }
}
