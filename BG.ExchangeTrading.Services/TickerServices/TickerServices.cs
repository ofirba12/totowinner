﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services.Persistance;
using EOD;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TotoWinner.Common.Infra;
using ExTradingData = BG.ExchangeTrading.Data;

namespace BG.ExchangeTrading.Services
{
    public class TickerServices : Singleton<TickerServices>, ITickerServices
    {
        public readonly API EODHistoricalDataAPI;
        private readonly API _apiProxy;
        private readonly API _apiSource;
        private TickersPersistanceServices persistance = TickersPersistanceServices.Instance;

        private TickerServices()
        {
#if DEBUG
            //string apiKey = "64d25a5e75cba8.49610783";//ofirba@gmail.com
            string apiKey = "64f7759043f422.85886633";//gabi9200@gmail.com
#else
            string apiKey = "64f7759043f422.85886633";//gabi9200@gmail.com
#endif
            System.Net.WebProxy proxy = new WebProxy("localhost:80");
            EODHistoricalDataAPI = new API(apiKey);
            _apiProxy = new API(apiKey, proxy);
            _apiSource = new API(apiKey, null, "EODHistoricalData.Downloader");
        }

        public ExTradingData.Ticker ConvertShortcutToTicker(string shortcut)
        {
            try
            {
                if (!string.IsNullOrEmpty(shortcut))
                {
                    var splited = shortcut.Split(':');
                    var ticker = new ExTradingData.Ticker(splited[0].Trim().ToUpper(), splited[1].Trim(), null, null, null, null, null);
                    return ticker;
                }
                throw new Exception($"This following {shortcut} can not be converted to Ticker");
            }
            catch (Exception ex)
            {
                throw new Exception($"An error ocurred trying to convert {shortcut}", ex);
            }
        }
        public List<ExTradingData.Ticker> GetAvailableTickers()
        {
            try
            {
                var tickers = new List<ExTradingData.Ticker>();
                string[] lines = File.ReadAllLines("tickers.csv");

                // Iterate over the lines and create a Ticker object for each line.
                for (int i = 1; i < lines.Length; i++)
                {
                    string[] columns = lines[i].Split(',');

                    var symbol = columns[0];
                    var fullname = columns[1];
                    var exchange = columns[2];
                    var type = columns[3];
                    //var spot = double.Parse(columns[4]);
                    var index = columns[5];
                    var marketCap = !string.IsNullOrEmpty(columns[6])
                        ? double.Parse(columns[6])
                        : (double?)null;
                    var options = columns[7];
                    var ticker = new ExTradingData.Ticker(symbol, fullname, exchange, type, index, marketCap, options);

                    tickers.Add(ticker);
                }
                return tickers;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred trying to get available tickers from csv file", ex);
            }
        }
        //public int AddTicker(string symbol, string fullName)
        //{
        //    try
        //    {
        //        int tickerId = -1;
        //        using (var db = new BgExchangeTradingEntities())
        //        {
        //            var result = db.Tickers_Merge(symbol, fullName);
        //            tickerId = result.First().Value;
        //        }
        //        return tickerId;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"An error occurred trying to add ticker symbol={symbol}, full name={fullName}", ex);
        //    }
        //}
        public List<BGTicker> GetExistingTickersList()
        {
            try
            {
                List<BGTicker> tickers = null;
                using (var db = new BgExchangeTradingEntities())
                {
                    tickers = db.Tickers
                        .ToList()
                        .ConvertAll<BGTicker>(t => new BGTicker(t.TickerId,
                            t.Symbol,
                            t.FullName,
                            t.Type,
                            t.Group,
                            t.StockRating,
                            t.LastUpdateStockRating,
                            t.Cap,
                            t.Floor,
                            t.Url
                            ));
                }
                return tickers;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred trying to get existing tickers list", ex);
            }
        }
        public List<BGTickerDetails> GetExistingTickersWithDetails()
        {
            try
            {
                List<BGTickerDetails> tickers = null;
                using (var db = new BgExchangeTradingEntities())
                {
                    tickers = db.Tickers_GetAll()
                        .ToList()
                        .ConvertAll<BGTickerDetails>(t => new BGTickerDetails(t.TickerId,
                            t.Symbol,
                            t.FullName,
                            t.Type,
                            t.Group,
                            t.StockRating,
                            t.LastUpdateStockRating,
                            t.Cap,
                            t.Floor,
                            t.Spot,
                            t.SpotLastUpdate,
                            t.ClosePrice,
                            t.Date,
                            t.Buffer,
                            t.MA50CapPercent,
                            t.MA50FloorPercent,
                            t.MA50,
                            t.MA75CapPercent,
                            t.MA75FloorPercent,
                            t.MA75,
                            t.MA100CapPercent,
                            t.MA100FloorPercent,
                            t.MA100,
                            t.MA125CapPercent,
                            t.MA125FloorPercent,
                            t.MA125,
                            t.MA150CapPercent,
                            t.MA150FloorPercent,
                            t.MA150,
                            t.MA175CapPercent,
                            t.MA175FloorPercent,
                            t.MA175,
                            t.MA200CapPercent,
                            t.MA200FloorPercent,
                            t.MA200,
                            t.Url
                            ));
                }
                return tickers;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred trying to get existing tickers list", ex);
            }
        }
        public async Task<List<StockPrice>> FetchEofStockPrice(string symbol, DateTime from, DateTime to)
        {
            try
            {
                var fetcher = new StockPriceFetcher(this.EODHistoricalDataAPI, symbol, from, to);
                var stockPrices = await fetcher.FetchEndOfDayData();
                var historicalStockPrice = stockPrices.ConvertAll<StockPrice>(s => new StockPrice(s.Date, s.Adjusted_open, s.Adjusted_close, s.Adjusted_high, s.Adjusted_low, s.Volume));
                return historicalStockPrice;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get eof data for symbol {symbol}", ex);
            }
        }
        public async Task<double?> FetchLiveStockPrice(string symbol)
        {
            try
            {
                var fetcher = new StockPriceFetcher(this.EODHistoricalDataAPI, symbol);
                var stockPrice = await fetcher.FetchLiveData();
                //var historicalStockPrice = stockPrices.ConvertAll<StockPrice>(s => new StockPrice(s.Date, s.Adjusted_open, s.Adjusted_close, s.Adjusted_high, s.Adjusted_low, s.Volume));
                return stockPrice.Close;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get live data for symbol {symbol}", ex);
            }
        }
        public double? CalculateMovingAverage(List<StockPrice> data, int maRulerValue)
        {
            var daysSelected = data
            .OrderByDescending(d => d.Date)
            .Take(maRulerValue);
            var movingAverage = daysSelected.Count() == maRulerValue
                ? daysSelected
                    .Select(d => d.ClosePrice)
                    .Average()
                : (double?)null;

            return movingAverage;
        }
        public double CalculateDailyChange(double currentTicker, double? previousTicker)
        {
            if (previousTicker != null)
                return (currentTicker / previousTicker.Value - 1) * 100;
            return 0;
        }
        public List<TickerEodData> GetTickerData(int tickerId)
        {
            try
            {
                var data = persistance.GetTickerData(tickerId)
                     .OrderByDescending(d => d.Date)
                     .ToList();
                var eofData = data.ConvertAll<TickerEodData>(d => new TickerEodData(
                    d.TickerId,
                    d.Date,
                    d.OpenPrice,
                    d.ClosePrice,
                    CalculateDailyChange(d.ClosePrice, data.Where(d1 => d1.Date < d.Date).FirstOrDefault()?.ClosePrice),
                    d.High,
                    d.Low,
                    d.Volume,
                    d.MA150,
                    //CalculateMovingAverage(data.Where(d1 => d1.Date <= d.Date).ToList().ConvertAll<StockPrice>(p => new StockPrice(p.Date, p.OpenPrice, p.ClosePrice, p.High, p.Low, p.Volume)), 150),
                    d.LastUpdate
                    ));
                return eofData;

            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get data for ticker [{tickerId}]", ex);
            }
        }
        public List<StockEodData> CalculateEofData(List<StockPrice> tickerData)
        {
            var data = tickerData
                 .OrderByDescending(d => d.Date)
                 .ToList();
            var eofData = data.ConvertAll<StockEodData>(d => new StockEodData(
                d.Date.Value,
                d.OpenPrice.Value,
                d.ClosePrice.Value,
                CalculateDailyChange(d.ClosePrice.Value, data.Where(d1 => d1.Date < d.Date).FirstOrDefault()?.ClosePrice),
                d.High.Value,
                d.Low.Value,
                d.Volume.Value,
                new MovingAverageItem(50, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 50), d.ClosePrice),
                new MovingAverageItem(75, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 75), d.ClosePrice),
                new MovingAverageItem(100, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 100), d.ClosePrice),
                new MovingAverageItem(125, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 125), d.ClosePrice),
                new MovingAverageItem(150, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 150), d.ClosePrice),
                new MovingAverageItem(175, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 175), d.ClosePrice),
                new MovingAverageItem(200, CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= d.Date.Value).ToList(), 200), d.ClosePrice)
                ));
            return eofData;
        }
        public int AddTickerWithHistoricalData(string symbol, string fullName, string type, string group, string url, List<StockPrice> tickerData)
        {
            var eofData = CalculateEofData(tickerData);
            var tickerId = persistance.AddTickerWithHistoricalData(symbol, fullName, type, group, url, eofData);
            return tickerId;
        }

        public List<TickerMovingAverageData> GenerateHeatMapData(List<StockPrice> tickerData, List<int> movingAverage)
        {
            try
            {
                var data = tickerData
                 .OrderByDescending(d => d.Date)
                 .ToList();
                var result = new List<TickerMovingAverageData>();
                foreach (var row in tickerData)
                {
                    if (row.ClosePrice.HasValue)
                    {
                        var maItems = new List<MovingAverageData>();
                        foreach (var ma in movingAverage)
                        {
                            var maValue = CalculateMovingAverage(data.Where(d1 => d1.Date.Value <= row.Date.Value).ToList(), ma);
                            if (maValue.HasValue)
                            {
                                var spread = (row.ClosePrice.Value / maValue.Value - 1) * 100;
                                maItems.Add(new MovingAverageData(ma, maValue.Value, spread));
                            }
                        }
                        result.Add(new TickerMovingAverageData(row.Date.Value, row.ClosePrice.Value, maItems));
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to generate heatmap data for {tickerData.Count} ticker data with ma ruler", ex);
            }
        }
        #region GenerateHeatMapRepository
        public HeatMapRepository GenerateHeatMapRepository(List<TickerMovingAverageData> heatmapData,
            List<int> buyPoints,
            List<int> sellPoints,
            List<int> movingAverageRuler,
            int buffer)
        {
            var heatmapSummary = new List<EntryPointHeatMap>();
            //ma, <trigger, list>
            Dictionary<int,Dictionary<int, List<HeatmapEntriesRepository>>> buyMaTriggersRepo = new Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>>(); 
            Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>> sellMaTriggersRepo = new Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>>();
            foreach (var ma in movingAverageRuler)
            {
                var buyTriggersRepo = GenerateTriggerHeatmapData(heatmapData, buyPoints, buffer, ma, true);
                buyMaTriggersRepo.Add(ma, buyTriggersRepo);
                GenerateSummary(heatmapSummary, ma, buyTriggersRepo, true);
                var sellTriggersRepo = GenerateTriggerHeatmapData(heatmapData, sellPoints, buffer, ma, false);
                sellMaTriggersRepo.Add(ma, sellTriggersRepo);
                GenerateSummary(heatmapSummary, ma, sellTriggersRepo, false);
            }
            var heatmap = new HeatMapRepository(heatmapSummary,
                buyMaTriggersRepo,
                sellMaTriggersRepo);
            return heatmap;
        }

        private void GenerateSummary(List<EntryPointHeatMap> heatmap,
            int ma,
            Dictionary<int, List<HeatmapEntriesRepository>> triggersRepo,
            bool isBuyTrigger)
        {
            if (triggersRepo.Count > 0)
            {
                var summary = triggersRepo.Keys
                    .ToList()
                    .ConvertAll<EntryPointHeatMap>(i =>
                        {
                            return triggersRepo[i].Count() > 0
                                ? new EntryPointHeatMap(
                                    ma,
                                    isBuyTrigger,
                                    i,
                                    triggersRepo[i].Count(),
                                    triggersRepo[i].Where(j => j.Win).Count(),
                                    triggersRepo[i].Where(j => !j.Win).Count()
                                    )
                                : null;
                        })
                   .Where(j => j != null);
                if (summary.Count() > 0)
                    heatmap.AddRange(summary);
            }
        }

        private Dictionary<int, List<HeatmapEntriesRepository>> GenerateTriggerHeatmapData(List<TickerMovingAverageData> heatmapData,
            List<int> triggerPoints,
            int buffer,
            int ma,
            bool isBuyTrigger)
        {
            var repo = new Dictionary<int, List<HeatmapEntriesRepository>>();
            var sortedData = heatmapData
                .Where(i => i.MovingAverageItems.Where(j => j.MaDays == ma).Count() > 0)
                .OrderBy(d => d.Date)
                .Select((item, index) => new { Key = index + 1, Value = item })
                .ToDictionary(item => item.Key, item => item.Value);
            var result = new Dictionary<int, List<HeatmapEntriesRepository>>();
            foreach (var trigger in triggerPoints)
            {
                var entries = new Dictionary<int, TickerMovingAverageData>();
                Recurse(ma, trigger, sortedData, 1, entries, isBuyTrigger);
                var heatMapRepo = entries.Keys.ToList().ConvertAll<HeatmapEntriesRepository>(i =>
                    new HeatmapEntriesRepository(entries[i].Date,
                        entries[i].MovingAverageItems.First(j => j.MaDays == ma).SpreadValue,
                        entries[i].ClosePrice,
                        isBuyTrigger
                        ? sortedData[i].ClosePrice * (1 - 0.01 * buffer)
                        : sortedData[i].ClosePrice * (1 + 0.01 * buffer),
                        sortedData[i + 20].Date,
                        sortedData[i + 20].ClosePrice,
                        isBuyTrigger
                        ? sortedData[i + 20].ClosePrice > sortedData[i].ClosePrice * (1 - 0.01 * buffer)
                        : sortedData[i + 20].ClosePrice < sortedData[i].ClosePrice * (1 + 0.01 * buffer)
                ));
                repo.Add(trigger, heatMapRepo);
            }
            return repo;
        }

        private void Recurse(int ma,
            int tigger,
            Dictionary<int, TickerMovingAverageData> sortedData,
            int index,
            Dictionary<int, TickerMovingAverageData> entries,
            bool isBuyTrigger)
        {
            if (sortedData.Count > index + 20)
            {
                var maItem = sortedData[index].MovingAverageItems.First(m => m.MaDays == ma);
                var condition = isBuyTrigger
                    ? maItem.SpreadValue <= tigger
                    : maItem.SpreadValue >= tigger;
                if (condition)
                {
                    entries.Add(index, sortedData[index]);
                    index += 20;
                }
                else
                    index++;
                Recurse(ma, tigger, sortedData, index, entries, isBuyTrigger);
            }
        }

        public List<MovingAverageSuccessRate> CalulateMAs(int tickerId,
            StockEodData stockEodData, 
            List<EntryPointHeatMap> summary, 
            List<int> movingAverageRuler)
        {
            Func<bool, int, double, bool> IsTriggerHit = (isBuy, trigger, spread) =>
            {
                var hit = !isBuy
                    ? trigger <= spread
                    : trigger >= spread;
                return hit;
            };
            var result = new List<MovingAverageSuccessRate>();
            foreach (var mv in movingAverageRuler)
            {
                if (!stockEodData.MA50.Spread.HasValue)
                    continue;
                var spread = stockEodData.MA50.Spread.Value;
                switch (mv)
                {
                    case 50:
                        spread = stockEodData.MA50.Spread.Value;
                        break;
                    case 75:
                        spread = stockEodData.MA75.Spread.Value;
                        break;
                    case 100:
                        spread = stockEodData.MA100.Spread.Value;
                        break;
                    case 125:
                        spread = stockEodData.MA125.Spread.Value;
                        break;
                    case 150:
                        spread = stockEodData.MA150.Spread.Value;
                        break;
                    case 175:
                        spread = stockEodData.MA175.Spread.Value;
                        break;
                    case 200:
                        spread = stockEodData.MA200.Spread.Value;
                        break;
                }
                var isBuy = spread < 0;
                var triggers = summary.Where(s => s.IsBuyPoint == isBuy && 
                        s.MovingAverage == mv && IsTriggerHit(isBuy, s.Trigger, spread))
                    .ToList();
                var w = triggers.Sum(t => t.TotalPositions * t.Success);
                var totalPosition = triggers.Sum(t => t.TotalPositions);
                var successRate = totalPosition > 0
                    ? (double)w / totalPosition
                    : 0;
                var maSuccRate = new MovingAverageSuccessRate(tickerId, mv, isBuy, successRate, totalPosition);
                result.Add(maSuccRate);
            }
            return result;
        }
        #endregion
        #region GetFullSymbol
        public string GetFullSymbol(string symbol)
        {
            var fullSymbol = symbol == "VIX"
            ? $"{symbol}.INDX"
            : $"{symbol}.US";
            return fullSymbol;
        }
        #endregion
    }
}
