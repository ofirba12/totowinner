﻿using BG.ExchangeTrading.Data;
using EOD;
using EOD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EOD.API;

namespace BG.ExchangeTrading.Services
{
    internal class StockPriceFetcher
    {
        public string Symbol { get; }
        private DateTime From;
        private DateTime To;
        private readonly API _api;
        public StockPriceFetcher(API api, string symbol)
        {
            if (string.IsNullOrEmpty(symbol))
                throw new ArgumentNullException($"The specified symbol is empty");
            this.Symbol = symbol;
            this._api = api;
        }
        public StockPriceFetcher(API api, string symbol, DateTime from, DateTime to) : this(api, symbol) 
        {
            this.From = from;
            this.To = to;
        }

        public async Task<List<HistoricalStockPrice>> FetchEndOfDayData()
        {
            try
            {
                var stockPrices = await _api.GetEndOfDayHistoricalStockPriceAsync(this.Symbol,
                    From,
                    To,
                    HistoricalPeriod.Daily);
                return stockPrices;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get historical data for {this.Symbol} from [{From}] till [{To}]", ex);
            }
        }
        public async Task<LiveStockPrice> FetchLiveData()
        {
            try
            {
                var stockPrice = await _api.GetLiveStockPricesAsync(this.Symbol);
                return stockPrice;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get live data for {this.Symbol}", ex);
            }
        }
    }
}
