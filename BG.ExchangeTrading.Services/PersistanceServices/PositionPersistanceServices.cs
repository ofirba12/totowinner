﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services.Persistance;
using EntityFrameworkExtras.EF6;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Common.Infra;
using TotoWinner.Services;

namespace BG.ExchangeTrading.Services
{
    public class PositionPersistanceServices : Singleton<PositionPersistanceServices>, IPositionPersistanceServices
    {
        private PositionPersistanceServices() { }

        #region AddPortfolio
        public int AddPortfolio(DateTime expiry)
        {
            try
            {
                int? portfolioId = null;
                using (var ent = new BgExchangeTradingEntities())
                {
                    var result = ent.Portfolios_Merge(null, expiry);
                    portfolioId = result.First().Value;
                }
                return portfolioId.Value;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add porfolio with expiry {expiry}", ex);
            }
        }
        #endregion

        #region DeletePortfolio
        public void DeletePortfolio(int portfolioId)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    ent.Portfolios_Delete(portfolioId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add delete portfolio with Id {portfolioId}", ex);
            }
        }
        #endregion

        #region GetAllPorfolios
        public List<Portfolios_GetAll_Result> GetAllPorfolios()
        {
            try
            {
                List<Portfolios_GetAll_Result> portfolios = null;
                using (var ent = new BgExchangeTradingEntities())
                {
                    portfolios = ent.Portfolios_GetAll().ToList();
                }
                return portfolios;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get all portfolios", ex);
            }
        }
        #endregion

        #region AddPortfolioAsset
        public void AddPortfolioAsset(int portfolioId,
            int tickerId,
            DateTime tickerExpiry,
            bool isCall,
            double strike,
            bool isLong,
            double premium,
            int size,
            double fee)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    ent.PortfolioAssets_Merge(null,
                        portfolioId,
                        tickerId,
                        tickerExpiry,
                        isCall,
                        strike,
                        isLong,
                        premium,
                        size,
                        fee);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add portfolio asset for porfolioId {portfolioId} with tickerId {tickerId}", ex);
            }
        }
        #endregion

        #region UpdatePortfolioAsset
        public void UpdatePortfolioAsset(int assetId,
            int portfolioId,
            int tickerId,
            DateTime tickerExpiry,
            bool isCall,
            double strike,
            bool isLong,
            double premium,
            int size,
            double fee)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    ent.PortfolioAssets_Merge(assetId,
                        portfolioId,
                        tickerId,
                        tickerExpiry,
                        isCall,
                        strike,
                        isLong,
                        premium,
                        size,
                        fee);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update portfolio asset for porfolioId {portfolioId} with tickerId {tickerId}", ex);
            }
        }
        #endregion

        #region GetPortfolioAssets
        public List<PortfolioAssets_Get_Result> GetPortfolioAssets(int portfolioId)
        {
            try
            {
                List<PortfolioAssets_Get_Result> assets = null;
                using (var ent = new BgExchangeTradingEntities())
                {
                    assets = ent.PortfolioAssets_Get(portfolioId).ToList();
                }
                return assets;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get portfolio assets for porfolioId {portfolioId}", ex);
            }
        }
        #endregion

        #region DeletePortfolioAsset
        public void DeletePortfolioAsset(int assetId)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    ent.PortfolioAsset_Delete(assetId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add delete portfolio assetId {assetId}", ex);
            }
        }
        #endregion

        #region GetTickersDataPerExpiry

        private class PersistanceTickerSpotData
        {
            public int TickerId { get; set; }
            public DateTime Expiry { get; set; }
            public double ClosePrice { get; set; }
        }
        public List<TickerIdExpirySpot> GetTickersDataPerExpiry(List<TickerIdExpiry> collection)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    var parameterTable = new DataTable();
                    parameterTable.Columns.Add("TickerId", typeof(int));
                    parameterTable.Columns.Add("Expiry", typeof(DateTime));
                    foreach(var input in collection)                    
                        parameterTable.Rows.Add(input.TickerId, input.Expiry);

                    var parameters = new SqlParameter("@Collection", SqlDbType.Structured)
                    {
                        TypeName = "dbo.TickerIdExpiries_UDT",
                        Value = parameterTable
                    };

                    var output = ent.Database.SqlQuery<PersistanceTickerSpotData>(
                        "EXEC dbo.TickersDataPerExpiry_Get @Collection", parameters).ToList();

                    var result = output.ConvertAll<TickerIdExpirySpot>(e => new TickerIdExpirySpot(e.TickerId, e.Expiry, e.ClosePrice));
                    return result;  
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to tickers data per expiry", ex);
            }
        }
        #endregion

        #region GetActivePortfolioAssets
        public List<PortfolioAssets_GetActive_Result> GetActivePortfolioAssets()
        {
            try
            {
                List<PortfolioAssets_GetActive_Result> assets = null;
                using (var ent = new BgExchangeTradingEntities())
                {
                    assets = ent.PortfolioAssets_GetActive().ToList();
                }
                return assets;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get active portfolio assets", ex);
            }
        }
        #endregion
    }
}
