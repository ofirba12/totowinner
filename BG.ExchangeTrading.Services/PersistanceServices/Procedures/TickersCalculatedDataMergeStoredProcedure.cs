﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("TickersCalculatedData_Merge")]
    public class TickersCalculatedDataMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<TickersCalculatedDataUDT> Collection { get; set; }
    }
}
