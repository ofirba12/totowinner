﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("TickersData_Merge")]
    public class TickersDataMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<TickersDataUDT> Collection { get; set; }
    }
}
