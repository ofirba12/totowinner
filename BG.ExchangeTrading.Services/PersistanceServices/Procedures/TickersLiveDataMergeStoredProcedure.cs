﻿using EntityFrameworkExtras.EF6;
using System.Data;
using System.Collections.Generic;

namespace TotoWinner.Services
{
    [StoredProcedure("TickersLiveData_Merge")]
    public class TickersLiveDataMergeStoredProcedure
    {
        [StoredProcedureParameter(SqlDbType.Udt)]
        public List<TickersLiveDataUDT> Collection { get; set; }
    }
}
