﻿using EntityFrameworkExtras.EF6;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TotoWinner.Services
{
    [UserDefinedTableType("TickersData_UDT")]
    public class TickersDataUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int TickerId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public DateTime Date { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public decimal OpenPrice { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public decimal ClosePrice { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public decimal High { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public decimal Low { get; set; }
        [UserDefinedTableTypeColumn(7)]
        public long Volume { get; set; }
        [UserDefinedTableTypeColumn(8)]
        public decimal? MA50 { get; set; }
        [UserDefinedTableTypeColumn(9)]
        public decimal? MA75 { get; set; }
        [UserDefinedTableTypeColumn(10)]
        public decimal? MA100 { get; set; }
        [UserDefinedTableTypeColumn(11)]
        public decimal? MA125 { get; set; }
        [UserDefinedTableTypeColumn(12)]
        public decimal? MA150 { get; set; }
        [UserDefinedTableTypeColumn(13)]
        public decimal? MA175 { get; set; }
        [UserDefinedTableTypeColumn(14)]
        public decimal? MA200 { get; set; }
        [UserDefinedTableTypeColumn(15)]
        public DateTime LastUpdate { get; set; }
        public TickersDataUDT() { }
    }
}

