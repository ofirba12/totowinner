﻿using EntityFrameworkExtras.EF6;
using System;

namespace TotoWinner.Services
{
    [UserDefinedTableType("TickersCalculatedData_UDT")]
    public class TickersCalculatedDataUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int TickerId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public int MV { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public bool IsBuy { get; set; }
        [UserDefinedTableTypeColumn(4)]
        public decimal SuccessRate { get; set; }
        [UserDefinedTableTypeColumn(5)]
        public int TotalPosition { get; set; }
        [UserDefinedTableTypeColumn(6)]
        public DateTime LastUpdate { get; set; }
        public TickersCalculatedDataUDT() { }
    }
}

