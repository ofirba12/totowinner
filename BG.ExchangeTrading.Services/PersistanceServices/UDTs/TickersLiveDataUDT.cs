﻿using EntityFrameworkExtras.EF6;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TotoWinner.Services
{
    [UserDefinedTableType("TickersLiveData_UDT")]
    public class TickersLiveDataUDT
    {
        [UserDefinedTableTypeColumn(1)]
        public int TickerId { get; set; }
        [UserDefinedTableTypeColumn(2)]
        public decimal Spot { get; set; }
        [UserDefinedTableTypeColumn(3)]
        public DateTime LastUpdate { get; set; }
        public TickersLiveDataUDT() { }
    }
}

