﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services.Persistance;
using EntityFrameworkExtras.EF6;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common.Infra;
using TotoWinner.Services;

namespace BG.ExchangeTrading.Services
{
    public class TickersPersistanceServices : Singleton<TickersPersistanceServices>, ITickersPersistanceServices
    {
        private TickersPersistanceServices() { }

        public List<Persistance.Ticker> GetExisitingTickers()
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    var data = ent.Tickers.ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get exsiting tickers", ex);
            }
        }
        public Persistance.Ticker GetTickerInfo(string symbol)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    var ticker = ent.Tickers.Where(t => t.Symbol == symbol.ToUpper()).FirstOrDefault();
                    return ticker;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get ticker [{symbol}] data", ex);
            }
        }
        public List<TickersData> GetTickerData(int tickerId)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    var data = ent.TickersDatas.Where(t => t.TickerId == tickerId).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get data for ticker [{tickerId}]", ex);
            }
        }
        public void UpdateTickerInfo(string symbol,
            string type,
            string group,
            double? stockRating,
            DateTime? lastStockRatingDate,
            double? buffer,
            double? cap,
            double? floor,
            double? ma50CapPercent,
            double? ma50FloorPercent,
            double? ma75CapPercent,
            double? ma75FloorPercent,
            double? ma100CapPercent,
            double? ma100FloorPercent,
            double? ma125CapPercent,
            double? ma125FloorPercent,
            double? ma150CapPercent,
            double? ma150FloorPercent,
            double? ma175CapPercent,
            double? ma175FloorPercent,
            double? ma200CapPercent,
            double? ma200FloorPercent,
            string url
        )
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    var update = DateTime.Now;
                    var result = ent.Tickers_Merge(symbol, null, type,
                        group,
                        stockRating,
                        buffer,
                        lastStockRatingDate,
                        cap,
                        floor,
                        ma50CapPercent,
                        ma50FloorPercent,
                        ma75CapPercent,
                        ma75FloorPercent,
                        ma100CapPercent,
                        ma100FloorPercent,
                        ma125CapPercent,
                        ma125FloorPercent,
                        ma150CapPercent,
                        ma150FloorPercent,
                        ma175CapPercent,
                        ma175FloorPercent,
                        ma200CapPercent,
                        ma200FloorPercent,
                        url
                        );

                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update ticker {symbol} information", ex);
            }
        }
        public int AddTickerWithHistoricalData(string symbol, string fullName, string type, string group, string url, List<StockEodData> tickerData)
        {
            try
            {
                int? tickerId = null;
                using (var ent = new BgExchangeTradingEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var update = DateTime.Now;
                            var result = ent.Tickers_Merge(symbol, fullName, type, group,
                                null,null,null,null,null,null, null,
                                null,null, null,null,null, null,null,
                                null, null,null,null, null, url);
                            tickerId = result.First().Value;

                            var proc = new TickersDataMergeStoredProcedure();
                            proc.Collection = tickerData.ConvertAll(m => new TickersDataUDT()
                            {
                                TickerId = tickerId.Value,
                                Date = m.Date,
                                OpenPrice = (decimal)m.OpenPrice,
                                ClosePrice = (decimal)m.ClosePrice,
                                High = (decimal)m.High,
                                Low = (decimal)m.Low,
                                Volume = m.Volume,
                                MA50 = (decimal?)m.MA50.Value,
                                MA75 = (decimal?)m.MA75.Value,
                                MA100 = (decimal?)m.MA100.Value,
                                MA125 = (decimal?)m.MA125.Value,
                                MA150 = (decimal?)m.MA150.Value,
                                MA175 = (decimal?)m.MA175.Value,
                                MA200 = (decimal?)m.MA200.Value,
                                LastUpdate = update
                            }); ;
                            ent.Database.ExecuteStoredProcedure<TickersDataUDT>(proc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to add ticker with historical data, transaction has been rolledback", ex);
                        }
                    }
                }
                return tickerId.Value;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add ticker with historical data", ex);
            }
        }
        public void AddHistoricalData(int tickerId, List<StockEodData> tickerData)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var update = DateTime.Now;

                            var proc = new TickersDataMergeStoredProcedure();
                            proc.Collection = tickerData.ConvertAll(m => new TickersDataUDT()
                            {
                                TickerId = tickerId,
                                Date = m.Date,
                                OpenPrice = (decimal)m.OpenPrice,
                                ClosePrice = (decimal)m.ClosePrice,
                                High = (decimal)m.High,
                                Low = (decimal)m.Low,
                                Volume = m.Volume,
                                MA50 = (decimal?)m.MA50.Value,
                                MA75 = (decimal?)m.MA75.Value,
                                MA100 = (decimal?)m.MA100.Value,
                                MA125 = (decimal?)m.MA125.Value,
                                MA150 = (decimal?)m.MA150.Value,
                                MA175 = (decimal?)m.MA175.Value,
                                MA200 = (decimal?)m.MA200.Value,
                                LastUpdate = update
                            }); ;
                            ent.Database.ExecuteStoredProcedure<TickersDataUDT>(proc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to add historical data to ticker {tickerId}, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add historical data to ticker {tickerId}", ex);
            }
        }
        public void AddCalculatedData(List<MovingAverageSuccessRate> movingAverageSuccessRate)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var update = DateTime.Now;

                            var proc = new TickersCalculatedDataMergeStoredProcedure();
                            proc.Collection = movingAverageSuccessRate.ConvertAll(m => new TickersCalculatedDataUDT()
                            {
                                TickerId = m.TickerId,
                                MV = m.Mv,
                                IsBuy = m.IsBuy,
                                SuccessRate = (decimal)m.SuccessRate,
                                TotalPosition = m.TotalPosition,
                                LastUpdate = update
                            }); ;
                            ent.Database.ExecuteStoredProcedure<TickersCalculatedDataUDT>(proc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to add calculated data, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to add calculated data", ex);
            }
        }
        public void DeleteTickerAndData(int tickerId)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.TickersCalculatedData_Delete(tickerId);
                            ent.TickersData_Delete(tickerId);
                            ent.TickersLiveData_Delete(tickerId);
                            ent.Tickers_Delete(tickerId);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to add historical data to ticker {tickerId}, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to delete ticker with historical data to ticker {tickerId}", ex);
            }
        }
        public void UpdateLiveData(List<StockLiveData> tickerLiveData)
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    using (var transaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var proc = new TickersLiveDataMergeStoredProcedure();
                            proc.Collection = tickerLiveData.ConvertAll(m => new TickersLiveDataUDT()
                            {
                                TickerId = m.TickerId,
                                Spot = (decimal)m.Spot,
                                LastUpdate = m.LastUpdate
                            }); ;
                            ent.Database.ExecuteStoredProcedure<TickersLiveDataUDT>(proc);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception($"Failed to update live data, transaction has been rolledback", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to update live data", ex);
            }
        }
        public List<MovingAverageSuccessRate> GetTickersCalculatedData()
        {
            try
            {
                using (var ent = new BgExchangeTradingEntities())
                {
                    var data = ent.TickersCalculatedData_Get()
                        .ToList()
                        .ConvertAll<MovingAverageSuccessRate>(m => 
                            new MovingAverageSuccessRate(m.TickerId, m.MV, m.IsBuy, m.SuccessRate, m.TotalPosition));
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to get calculated data for all tickers", ex);
            }
        }
    }
}