import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secondsToTime'
})
export class SecondsToTimePipe implements PipeTransform {

  transform(seconds: any, args?: any): any {
    let formattedTimeString: string = '';
    let ss : number = Math.floor(seconds % 60);
    let mm : number = Math.floor(seconds / 60) % 60;
    let hh : number = Math.floor(seconds / 3600) % 60;
    formattedTimeString =  hh.toString().padStart(2,"0") + ':'
      + mm.toString().padStart(2,"0") + ':'
      + ss.toString().padStart(2,"0");
    return formattedTimeString;
  }
}
