import { Component, OnInit, OnDestroy, TemplateRef, ChangeDetectionStrategy } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig, ConfirmationService, SelectItem } from 'primeng/api';
import { TemplatesService, AppHelperService } from 'src/app/_services';
import { Store, select } from '@ngrx/store';
import * as fromApp from './../../../state';
import * as appActions from './../../../state/app.actions';
import { Subscription, Observable } from 'rxjs';
import { ProgramType } from 'src/app/state/app';
import { TemplatesState, TemplateRequest } from 'src/app/model/interfaces/templates.module';

@Component({
  selector: 'app-templates-manager',
  templateUrl: './templates-manager.component.html',
  styleUrls: ['./templates-manager.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplatesManagerComponent implements OnInit, OnDestroy {
  templates$: Observable<TemplatesState>;
  templateName: string;
  templateValue: string;
  programsType: SelectItem[];
  selectedProgramType: SelectItem;
  // templatesSubscription$: Subscription;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private helper: AppHelperService,
    private confirmationService: ConfirmationService,
    private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.templates$ = this.store.pipe(select(fromApp.getTemplates));
    this.programsType = this.helper.programTypesForUI;
    this.selectedProgramType = this.programsType[0];
  }
  addTemplate(): void {
    var payload : TemplateRequest = { 
      templateInfo: {
        id: 0,
        type: this.selectedProgramType.value,
        name: this.templateName,
        value: this.templateValue,
      }
     };
    this.store.dispatch(new appActions.AddTemplate(payload));
  } 
  reload(){
    this.store.dispatch(new appActions.GetAllTemplates());
  }
  ngOnDestroy() {
    // if (this.templatesSubscription$)
    //   this.templatesSubscription$.unsubscribe();
  }
}
