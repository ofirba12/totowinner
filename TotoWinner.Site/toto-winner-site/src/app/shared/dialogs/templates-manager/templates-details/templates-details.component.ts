import { Component, OnInit, Input } from '@angular/core';
import { ProgramType } from 'src/app/state/app';
import { ConfirmationService, DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../state';
import * as appActions from './../../../../state/app.actions';
import { Template } from 'src/app/model/interfaces/templates.module';
@Component({
  selector: 'templates-details',
  templateUrl: './templates-details.component.html',
  styleUrls: ['./templates-details.component.css']
})
export class TemplatesDetailsComponent implements OnInit {
  @Input() collection: Template[];
  rowGroupMetadata: any;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private confirmationService: ConfirmationService,
    private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.updateRowGroupMetaData();
  }
  onSort() {
    this.updateRowGroupMetaData();
  }
  sort2Level(one: Template, two: Template): number {
    if (one.name == null && two.name == null)
      return (one.value > two.value ? 1 : -1);
    if (one.name == null && two.name != null)
      return -1;
    if (one.name != null && two.name == null)
      return 1;
    return (one.name > two.name ? 1 : -1)
  }
  updateRowGroupMetaData() {
    var self = this;
    this.rowGroupMetadata = {};
    this.collection.sort(function (a, b) { return b.type - a.type; }); //sort desc
    var winner16Ar = this.collection.filter(item => item.type == 96)
      .sort((one, two) => {
        return self.sort2Level(one, two);
      });// sort asc
    var winnerWorldAr = this.collection.filter(item => item.type == 87)
      .sort((one, two) => {
        return self.sort2Level(one, two);
      });// sort asc
    var winnerHalfAr = this.collection.filter(item => item.type == 82)
      .sort((one, two) => {
        return self.sort2Level(one, two);
      });// sort asc
    this.collection = winner16Ar.concat(winnerWorldAr).concat(winnerHalfAr);
    if (this.collection) {
      for (let i = 0; i < this.collection.length; i++) {
        let rowData = this.collection[i];
        let type = rowData.type;
        if (i == 0) {
          this.rowGroupMetadata[type] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.collection[i - 1];
          let previousRowGroup = previousRowData.type;
          if (type === previousRowGroup)
            this.rowGroupMetadata[type].size++;
          else
            this.rowGroupMetadata[type] = { index: i, size: 1 };
        }
      }
    }
  }
  showProgramTypeLabel(type: ProgramType): string {
    switch (type) {
      case 96:
        return "ווינר 16";
        break;
      case 87:
        return "ווינר עולמי";
        break;
      case 82:
        return "ווינר מחצית";
        break;
    }
  }
  deleteTemplate(template: Template) {
    this.confirmationService.confirm({
      header: 'התראת מחיקת מערכת',
      message: `טמפלט ${template.name} נבחרה למחיקה. האם ברצונך להמשיך?`,
      accept: () => {
        //this.ref.close({ selected: template, deleteAction: true });
        this.store.dispatch(new appActions.DeleteTemplate({
          templateInfo: template
        }));
      }
    });
  }
}
