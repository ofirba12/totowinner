import { Component, OnInit, ViewChild } from '@angular/core';
import { AppHelperService, StatisticalDataService } from 'src/app/_services';
import { SelectItem } from 'primeng/api';
import { GetStatisticalDataRequest, GetStatisticalDataResponse } from 'src/app/model/interfaces/statisical-data.module';
import { ProgramType } from 'src/app/state/app';
import { ProgramSelectorComponent } from '../../directives';

@Component({
  selector: 'statisical-data',
  templateUrl: './statisical-data.component.html',
  styleUrls: ['./statisical-data.component.css'],
  styles: [`
        .loading-text {
            display: block;
            background-color: #f1f1f1;
            min-height: 19px;
            animation: pulse 1s infinite ease-in-out;
            text-indent: -99999px;
            overflow: hidden;
        }
    `]
})
export class StatisicalDataComponent implements OnInit {
  @ViewChild(ProgramSelectorComponent, { static: true }) programSelectorComponent: ProgramSelectorComponent;
  data: GetStatisticalDataResponse;
  constructor(private helper: AppHelperService,
    private dataService: StatisticalDataService) {
  }

  ngOnInit() {
    this.programSelectorComponent.selectedProgramYear = this.helper.getYearsForUI()[this.helper.getYearsForUI().length - 1];
    this.programSelectorComponent.selectedProgramType = this.helper.programTypesForUI[0];
    this.onChangeSelection();
  }
  onChangeSelection() {
    var self = this;
    var request: GetStatisticalDataRequest = {
      year: this.programSelectorComponent.selectedProgramYear.value,
      type: this.programSelectorComponent.selectedProgramType.value
    };
    this.dataService.getData(request)
      .subscribe((res: GetStatisticalDataResponse) => {
        //console.warn(res);
        self.data = res;
      });
  }
  getDecimalClass(item: number){
    return item > 0 
    ? "relation-data-positive-class" 
    : "relation-data-negative-class";
  }
}
