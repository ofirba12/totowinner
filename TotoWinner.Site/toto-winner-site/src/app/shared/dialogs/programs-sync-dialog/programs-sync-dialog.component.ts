import { Component, OnInit, ViewChild } from '@angular/core';
import { ProgramSelectorComponent } from '../../directives';
import { SelectItem } from 'primeng/api';
import { TotoGamesService, ProgramsService } from 'src/app/_services';
import { collectExternalReferences } from '@angular/compiler';
import { baseResponse } from 'src/app/_services/http.utils';

@Component({
  selector: 'app-programs-sync-dialog',
  templateUrl: './programs-sync-dialog.component.html',
  styleUrls: ['./programs-sync-dialog.component.css']
})
export class ProgramsSyncDialogComponent implements OnInit {
  @ViewChild(ProgramSelectorComponent, { static: true }) programSelectorComponent: ProgramSelectorComponent;
  selectedProgram : SelectItem;
  syncResult : baseResponse;
  constructor(private programsService: ProgramsService) { }

  ngOnInit() {
    this.selectedProgram  =undefined;
  }
  getLabel() : string {
    var label = `סנכרן תוכניה ${this.programSelectorComponent.selectedProgram.value.toto_round_id}`;
    return label;
  }
  onChangeSelection() : void{
    this.syncResult = undefined;
  }
  syncProgram(): void {
    var self = this;
    this.programsService.syncProgram_Api(this.programSelectorComponent.selectedProgram.value.endDate,
      this.programSelectorComponent.selectedProgramType.value,
      this.programSelectorComponent.selectedProgram.value.toto_round_id)
      .subscribe((res : baseResponse)=> {
        self.syncResult = res;
      });
  }
}
