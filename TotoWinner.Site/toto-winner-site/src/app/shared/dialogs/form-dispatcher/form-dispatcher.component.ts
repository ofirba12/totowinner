import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig, ConfirmationService } from 'primeng/api';
import { TotoGamesService, DispatchFormService, AppHelperService } from 'src/app/_services';
import { SystemType, SystemParameters, SystemParametersWithFinalResult, GetAllSystemsWithFinalResultResponse, UnionSystemsResponse, Container1X2, FormTypeGeneration, GenerateFormToDownloadResponse } from 'src/app/model/interfaces/shared.module';
import { ProgramType } from 'src/app/state/app';
import { ProgramSelectorComponent } from '../../directives';

@Component({
  selector: 'form-dispatcher-dialog',
  templateUrl: './form-dispatcher.component.html',
  styleUrls: ['./form-dispatcher.component.css']
})
export class FormDispatcherComponent implements OnInit {
  @ViewChild(ProgramSelectorComponent, { static: true }) programSelectorComponent: ProgramSelectorComponent;
  systems: SystemParametersWithFinalResult[];
  cols: any[];
  selectedSystems: SystemParameters[];
  unionSystemsData: UnionSystemsResponse;
  counterCoverage: Container1X2[];
  playerDownloadUrl: string = null;
  excelDownloadUrl: string = null;
  smartDownloadUrl: string = null;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private confirmationService: ConfirmationService,
    private helper: AppHelperService,
    private dispatchFormService: DispatchFormService) { }
  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'שם' },
      { field: 'lastUpdate', header: 'נכון לתאריך' },
      { field: 'finalResult', header: 'מספר טורים' },
    ];
    this.programSelectorComponent.selectedProgramType = this.helper.programTypesForUI[0];
    this.onChangeProgramType();
  }
  onChangeProgramType(){
    var programType: ProgramType = this.programSelectorComponent.selectedProgramType.value;
    this.dispatchFormService.GetAllSystemsForUnion(programType)
      .subscribe((response: GetAllSystemsWithFinalResultResponse) => {
        if (response.systems) {
          this.systems = response.systems;
        }
      })
  }
  onRowSelectionChanged() {
    var self = this;
    self.playerDownloadUrl = null;
    self.excelDownloadUrl = null;
    self.smartDownloadUrl = null;
  }
  generatePlayerFile() {
    var self = this;
    self.dispatchFormService.generateUnionSystemFormToDownload(self.unionSystemsData.unionResultId, FormTypeGeneration.Player)
      .subscribe((response: GenerateFormToDownloadResponse) => {
        if (response.isSuccess) {
          self.playerDownloadUrl = response.outputFile;
        }
        else {
          //SHOW ERROR MESSAGE
          console.error("generateUnionSystemFormToDownload", response.errorMessage)
        }
      });
  }
  generateXlsFile() {
    var self = this;
    self.dispatchFormService.generateUnionSystemFormToDownload(self.unionSystemsData.unionResultId, FormTypeGeneration.Excel)
      .subscribe((response: GenerateFormToDownloadResponse) => {
        if (response.isSuccess) {
          self.excelDownloadUrl = response.outputFile;
        }
        else {
          //SHOW ERROR MESSAGE
          console.error("generateUnionSystemFormToDownload", response.errorMessage)
        }
      });
  }
  generateSmartFile() {
    var self = this;
    self.dispatchFormService.generateUnionSystemFormToDownload(self.unionSystemsData.unionResultId, FormTypeGeneration.SMART)
      .subscribe((response: GenerateFormToDownloadResponse) => {
        if (response.isSuccess) {
          self.smartDownloadUrl = response.outputFile;
        }
        else {
          //SHOW ERROR MESSAGE
          console.error("generateUnionSystemFormToDownload", response.errorMessage)
        }
      });
  }
  unionSystems(): void {
    var self = this;
    var systemIds = self.selectedSystems.map(s => s.systemId);
    self.dispatchFormService.unionSystems(systemIds)
      .subscribe((response: UnionSystemsResponse) => {
        if (response) {
          console.warn(response);
          self.unionSystemsData = response;
          //Convert Dictionary<int, Container1X2> to array Container1X2[]
          self.unionSystemsData.counterCoverage = Object.entries(response.counterCoverage)
            .map(dictionary => dictionary[1]);
          self.unionSystemsData.counterPercentCoverage = Object.entries(response.counterPercentCoverage)
            .map(dictionary => dictionary[1]);
        }
      }
      )
  }
  getStyle(item: number): any {
    if (item <= 5)
      return { 'color': 'red' };
    if (item >= 90)
      return { 'color': 'green' };
  }
}
