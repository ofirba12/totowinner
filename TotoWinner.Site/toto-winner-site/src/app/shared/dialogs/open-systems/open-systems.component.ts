import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef, ConfirmationService } from 'primeng/api';
import { TotoGamesService } from 'src/app/_services';
import { GetAllSystemsResponse, SystemParameters } from 'src/app/model/interfaces/shared.module';

@Component({
  selector: 'app-open-systems',
  templateUrl: './open-systems.component.html',
  styleUrls: ['./open-systems.component.css']//,encapsulation: ViewEncapsulation.None
})
export class OpenSystemsComponent implements OnInit {
  systems : SystemParameters[]; 
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private totoGamesService: TotoGamesService,
    private confirmationService: ConfirmationService) { }
  ngOnInit() {
    var type = this.config.data.systemType;
    this.totoGamesService.getAllSystems(type).subscribe((response: GetAllSystemsResponse) =>
    { 
      if (response.systems){
        this.systems = response.systems.filter(sys => sys.name != 'צימצומים זמנית');;  
      }
    }
  )
  }
  selectSystem(system: any){
    this.ref.close({selected: system, deleteAction: false});
  }
  deleteSystem(system: any){
    this.confirmationService.confirm({
      header: 'התראת מחיקת מערכת',
      message: `תוכנית ${system.name} נבחרה למחיקה. האם ברצונך להמשיך?`,
      accept: () => {
        this.ref.close({selected: system, deleteAction: true});
      }
    });
  }
}
