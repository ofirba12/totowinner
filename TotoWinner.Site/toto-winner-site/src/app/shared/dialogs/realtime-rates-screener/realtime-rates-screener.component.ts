import { Component, OnInit, ViewChild } from '@angular/core';
import { ProgramSelectorComponent } from '../../directives';
import { AppHelperService, ProgramsService } from 'src/app/_services';
import { ProgramType } from 'src/app/state/app';
import { RealTimePipeGetResponse, RatesPipe } from 'src/app/model/interfaces/program.module';
import { ResponseBase } from 'src/app/model/interfaces/shared.module';

@Component({
  selector: 'app-realtime-rates-screener',
  templateUrl: './realtime-rates-screener.component.html',
  styleUrls: ['./realtime-rates-screener.component.css']
})
export class RealtimeRatesScreenerComponent implements OnInit {
  @ViewChild(ProgramSelectorComponent, { static: true }) programSelectorComponent: ProgramSelectorComponent;
  rates: RatesPipe[];
  updateResponse: ResponseBase;

  constructor(private helper: AppHelperService,
    private programService: ProgramsService) { }

  ngOnInit() {
    this.programSelectorComponent.selectedProgramType = this.helper.programTypesForUI[0];
    this.onChangeProgramType();
  }
  onChangeProgramType() {
    var programType: ProgramType = this.programSelectorComponent.selectedProgramType.value;
    this.programService.getProgramRatesScreener(programType)
      .subscribe((response: RealTimePipeGetResponse) => {
        if (response) {
          this.rates = response.rates;
        }
        else {
          console.warn('getProgramRatesScreener failed', response.errorMessage)
        }
      })
  }
  updateRates(): void {
    var programType: ProgramType = this.programSelectorComponent.selectedProgramType.value;
    this.programService.updateProgramRatesScreener(programType, this.rates)
      .subscribe((response: ResponseBase) => {
        this.updateResponse = response;
        if (!response.isSuccess) {
          console.warn('updateProgramRatesScreener failed', response.errorMessage)
        }
      })
  }
}
