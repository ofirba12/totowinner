import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { TotoGamesService } from 'src/app/_services';
import { GetAllSystemsResponse, SystemParameters } from 'src/app/model/interfaces/shared.module';

@Component({
  selector: 'app-save-system',
  templateUrl: './save-system.component.html',
  styleUrls: ['./save-system.component.css']//,encapsulation: ViewEncapsulation.None
})
export class SaveSystemComponent implements OnInit {
  systems: SystemParameters[];
  error: string;
  systemName = '';
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private totoGamesService: TotoGamesService) { }

  ngOnInit() {
    this.systemName = this.config.data.systemName;
    var type = this.config.data.systemType;
    this.totoGamesService.getAllSystems(type).subscribe((response: GetAllSystemsResponse) => {
      this.systems = response.systems;
    }
    )
  }
  saveSystem(systemName: string, override: boolean) {
    var existing = this.systems.filter(s => s.name == systemName);
    if (override == false && existing.length > 0) {
      this.error =`מערכת ${systemName} כבר קיימת. האם לשמור באותו שם?`;
    }
    else {
      this.ref.close(systemName);
    }
  }
  handleNo($event){
    this.error='';
  }
}
