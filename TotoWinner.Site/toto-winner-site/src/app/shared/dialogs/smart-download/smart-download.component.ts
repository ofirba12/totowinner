import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { DispatchFormService, AuthenticationService } from 'src/app/_services';
import { GetSmartFilesResponse, SmartFileContainer, DeleteSmartFileRequest, ResponseBase } from 'src/app/model/interfaces/shared.module';

@Component({
  selector: 'smart-download',
  templateUrl: './smart-download.component.html',
  styleUrls: ['./smart-download.component.css']
})
export class SmartDownloadComponent implements OnInit {
  cols: any[];
  files: SmartFileContainer[];
  isAdmin : boolean;
  constructor(private confirmationService: ConfirmationService,
    private dispatchFormService: DispatchFormService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.isAdmin = this.authenticationService.isAdminUser();
    this.cols = [
      { field: 'filename', header: 'שם' },
      { field: 'lastUpdate', header: 'נכון לתאריך' },
      { field: 'downloadUrl', header: '' }
    ];
    this.dispatchFormService.getSmartFiles()
      .subscribe((response: GetSmartFilesResponse) => {
        this.files = response.files;
      }
      )
  }
  deleteFile(id : number) :void {
    var self = this;
    this.dispatchFormService.deleteSmartFile(id)
      .subscribe((response: ResponseBase) => {
        if (response.isSuccess)
          self.files = self.files.filter(file => file.id != id);
        else
          console.warn(response.errorMessage);
        
      }
      )
  }
}
