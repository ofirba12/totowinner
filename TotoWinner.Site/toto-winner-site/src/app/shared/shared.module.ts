import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DecimalRangeComponent } from '../systems/bets-shrink-system/components/_directives';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimeNgModule } from '../primeng/primeng.module';
import { SecondsToTimePipe } from './pipes';
import { MinmaxComponent, DecimalRangeReduxComponent, ClockSpinnerComponent } from './directives';
import { TemplatesDetailsComponent } from './dialogs/templates-manager/templates-details/templates-details.component';
import { ProgramSelectorComponent } from './directives/program-selector/program-selector.component';

@NgModule({
  declarations: [
    MinmaxComponent,
    DecimalRangeComponent,
    ClockSpinnerComponent,
    SecondsToTimePipe,
    DecimalRangeReduxComponent,
    TemplatesDetailsComponent,
    ProgramSelectorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PrimeNgModule
  ],
  exports: [
    MinmaxComponent,
    DecimalRangeComponent,
    ClockSpinnerComponent,
    SecondsToTimePipe,
    DecimalRangeReduxComponent,
    TemplatesDetailsComponent,
    ProgramSelectorComponent
  ]
})
export class SharedModule { }
