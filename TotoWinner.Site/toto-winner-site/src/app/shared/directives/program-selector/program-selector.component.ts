import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { SelectItem } from 'primeng/api';
import * as fromApp from '../../../state';
import * as appActions from '../../../state/app.actions';
import { Store, select } from '@ngrx/store';
import { ProgramType, ShrinkState, ToolbarState } from 'src/app/state/app';
import { Observable, Subscription } from 'rxjs';
import { TotoGamesService, AppHelperService, ProgramsService } from 'src/app/_services';

@Component({
  selector: 'program-selector',
  templateUrl: './program-selector.component.html',
  styleUrls: ['./program-selector.component.css']
})
export class ProgramSelectorComponent implements OnInit {
  @Input() initializer: boolean;
  @Input() showProgramsDp: boolean;
  @Input() showYearsDp: boolean;
  @Output() onChangeSelection: EventEmitter<any> = new EventEmitter();
  programsType: SelectItem[];
  programsYear: SelectItem[];
  programs: SelectItem[];
  selectedProgramType: SelectItem;
  selectedProgramYear: SelectItem;
  selectedProgram: SelectItem;
  numberOfPrograms: number;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;
  shrinkToolbarState$: Observable<ToolbarState>;
  shrinkToolbarStateSubscription$: Subscription;
  state: ShrinkState;

  constructor(private programsService: ProgramsService,
    private helper: AppHelperService,
    private store: Store<fromApp.AppState>) {
    this.programsType = this.helper.programTypesForUI;
    this.programsYear = helper.getYearsForUI();
    this.numberOfPrograms = this.programsType.length * this.programsYear.length;
  }

  ngOnInit() {
    var self = this;
    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        self.state = state;
        if (self.initializer == false && self.state.toolbar.programsRepository.length > 0) {
          self.selectedProgramYear = self.programsYear[self.programsYear.length - 1];//self.programsYear[0];
          self.selectedProgramType = self.programsType[0];
          if (self.showProgramsDp) {
            self.programs = self.state.toolbar.programsRepository
              .find(item => item.type == self.selectedProgramType.value
                && item.year == self.selectedProgramYear.value)
              .programs;
            self.selectedProgram = self.programs[0];
          }
        }
      })
    this.shrinkToolbarState$ = this.store.pipe(select(fromApp.getShrinkToolbar));
    if (self.initializer == true) {
      self.initialize();
    }
  }
  initialize(): void {
    var self = this;
    if (self.state.toolbar.programsRepository.length != self.numberOfPrograms) {
      for (let programType of self.programsType) {
        for (let year of self.programsYear) {
          self.fetchProgramsList(programType.value, year.value)
        }
      }
    }

    this.shrinkToolbarStateSubscription$ = this.shrinkToolbarState$
      .subscribe(data => {
        var programsRepo = data.programsRepository.find(item => item.type == data.keys.selectedType
          && item.year == data.keys.selectedYear);
        if (programsRepo) {
          self.programs = programsRepo.programs;
          self.selectedProgramType = self.programsType.find(item => item.value === data.keys.selectedType);
          self.selectedProgramYear = self.programsYear.find(item => item.value === data.keys.selectedYear);
          if (self.showProgramsDp && data.selectedProgram == undefined) {
            self.selectedProgram = self.programs[0];
            this.store.dispatch(new appActions.SetToolbarSelectedProgram(self.selectedProgram));
          }
          else if (self.showProgramsDp)
            self.selectedProgram = data.selectedProgram;
        }
        else {
          self.fetchProgramsList(data.keys.selectedType, data.keys.selectedYear)
        }
      });
  }
  OnChangeProgramType($event) {
    this.OnChangeProgramYear(null);
  }
  OnChangeProgramYear($event) {
    //this.clearFormSelections();
    //    this.initToolbar();
    var self = this;
    this.onChangeSelection.emit();
    if (self.initializer == true) {
      this.store.dispatch(new appActions.SetToolbarSelectedTypeYear({
        selectedType: this.selectedProgramType.value,
        selectedYear: this.selectedProgramYear.value,
      }));
    }
    else {
      self.programs = self.state.toolbar.programsRepository
        .find(item => item.type == self.selectedProgramType.value
          && item.year == self.selectedProgramYear.value)
        .programs;
      self.selectedProgram = self.programs[0];
    }
  }
  OnChangeProgram($event) {
    //  this.clearFormSelections();
    //this.initToolbar();
    var self = this;
    this.onChangeSelection.emit();
    if (self.initializer == true) {
      this.store.dispatch(new appActions.SetToolbarSelectedProgram(this.selectedProgram));
    }
  }
  fetchProgramsList(selectedType: ProgramType, selectedYear: number) {
    var programType = selectedType == ProgramType.WinnerHalf ? ProgramType.Winner16 : selectedType;
    this.programsService.getPrograms_BankrimAPI(programType.toString(), selectedYear)
      .subscribe(programsData => {
        var programs: SelectItem[] = [];
        var programTypeLabel = this.programsType.find(item => item.value == selectedType).label;
        for (let entry of programsData) {
          var endDate = new Date(entry.endDate);
          var endDateLabel = this.helper.getFormatedDate(endDate, "DD.MM.YYYY");
          var roundTitle = entry.toto_round_id + ' (' + endDateLabel + ')';
          var programTitle = [programTypeLabel, "תוכניה מס' " + entry.toto_round_id, endDateLabel].join('|');
          entry.uniqueId = [entry.toto_round_id, selectedType.toString()].join('#');
          programs.push({ label: roundTitle, value: entry, title: programTitle });
        }

        this.store.dispatch(new appActions.SetPrograms({
          type: selectedType,
          year: selectedYear,
          programs: programs
        }));
      })
  }

  ngOnDestroy(): void {
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
    if (this.shrinkToolbarStateSubscription$)
      this.shrinkToolbarStateSubscription$.unsubscribe();
  }
}
