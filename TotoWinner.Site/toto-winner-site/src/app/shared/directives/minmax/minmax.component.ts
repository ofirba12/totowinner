import { Component, Input, OnChanges, SimpleChange, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { DropDownValue } from '../../../model/interfaces/shared.module';

@Component({
  selector: 'wn-minmax',
  templateUrl: './minmax.component.html',
  styleUrls: ['./minmax.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MinmaxComponent implements OnChanges {
  @Input() id: string;
  @Input() min: number;
  @Input() max: number;
  @Input() selectedMin: number;
  @Input() selectedMax: number;  
  @Input() disabled: boolean;
  @Input() alignVertical: boolean;
  @Input() hideLabels: boolean;
  @Output() isDirty: EventEmitter<any> = new EventEmitter();
  @Output() minChanges: EventEmitter<number> = new EventEmitter();
  @Output() maxChanges: EventEmitter<number> = new EventEmitter();

  minValues: DropDownValue[];
  maxValues: DropDownValue[];
  selectedMaxValue: DropDownValue;
  selectedMinValue: DropDownValue;
  labelStyle :any  = {'display': 'none'};

  constructor() { }
  init(): void {
    this.maxValues = [];
    this.minValues = [];
    for (let i = 0; i <= this.max; i++) {
      this.minValues.push(
        { label: i.toString(), value: i }
      );
      this.maxValues.push(
        { label: i.toString(), value: i }
      );
    }
    //TODO: Check for range
    this.selectedMinValue = this.selectedMin? this.minValues[this.selectedMin]: this.minValues[0];
    this.selectedMaxValue = this.selectedMax? this.maxValues[this.selectedMax] : this.maxValues[this.maxValues.length - 1];
//    this.selectedMaxValue = this.maxValues[this.maxValues.length - 1];
    //this.selectedMinValue = this.minValues[0];
    this.labelStyle = this.hideLabels? {'display': 'none'}: '';
  }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes &&
        changes.max && changes.max.currentValue) {
        this.init();
    }
  }
  OnChangeMinValue($event) {
    this.maxValues = [];
    for (let i = this.selectedMinValue.value; i <= this.max; i++) {
      this.maxValues.push(
        { label: i.toString(), value: i }
      );
    }
    this.minChanges.emit(this.selectedMinValue.value);
    if ($event)
      this.isDirty.emit(null);
  }
  OnChangeMaxValue($event) {
    this.minValues = [];
    for (let i = 0; i <= this.selectedMaxValue.value; i++) {
      this.minValues.push(
        { label: i.toString(), value: i }
      );
    }
    this.maxChanges.emit(this.selectedMaxValue.value);
    if ($event)
      this.isDirty.emit(null);
  }
  clearSelection(): void {
    this.init();
    // this.selectedMaxValue = { label: this.max.toString(), value: this.max };//Values[this.maxValues.length - 1];
    // this.selectedMinValue = { label: this.min.toString(), value: this.min };//this.minValues[0];
  }
  disableComponent(state: boolean) {
    this.disabled = state;
  }
  loadValues(minVal: number, maxVal: number) : void {
    this.selectedMinValue = { label: minVal.toString(), value: minVal};
    this.selectedMaxValue = { label: maxVal.toString(), value: maxVal};
    this.OnChangeMinValue(null);
    this.OnChangeMaxValue(null);
  }
}
