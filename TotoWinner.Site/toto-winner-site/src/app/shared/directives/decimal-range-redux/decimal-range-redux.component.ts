import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, OnChanges } from '@angular/core';

@Component({
  selector: 'decimal-range-redux',
  templateUrl: './decimal-range-redux.component.html',
  styleUrls: ['./decimal-range-redux.component.css']
})
export class DecimalRangeReduxComponent implements OnInit, OnChanges {
  @Input() min: number;
  @Input() max: number;
  @Input() showInlineError: boolean;
  @Input() allowNegativeValues: boolean;
  @Output() onError: EventEmitter<boolean> = new EventEmitter(true);
  @Output() onErrorMessage: EventEmitter<string> = new EventEmitter();
  @Output() onErrorMessageClear: EventEmitter<null> = new EventEmitter();
  @Output() exposeValues: EventEmitter<{ from: number, to: number }> = new EventEmitter();

  private invalidRangeMsg: string = "טווח לא חוקי, עד<מ";
  private outOfRangeMsg: string = "מספר לא בטווח";
  private illegalFromMsg: string = "ערך מ- אינו חוקי";
  private illegalToMsg: string = "ערך עד- אינו חוקי";

  decimal2PointsFromRegex: RegExp = /^-?\s*(?=.*[0-9])\d*(?:\.\d{1,2})?\s*$/;
  /* (?=.*[1-9])  # Assert that at least one digit >= 0 is present in the string */
  //decimal2PointsToRegex: RegExp = /^-?\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/; //Regex greater than zero with 2 decimal places
  decimal2PointsToRegex: RegExp = /^-?\s*(?=.*[0-9])\d*(?:\.\d{1,2})?\s*$/;
  /*Explanation: Regex with 2 decimal places (allow zero)
  ^            # Start of string
  -?           # Allow negative, ? is a quantifier meaning one or zero occurrences
  \s*          # Optional whitespace
  (?=.*[1-9])  # Assert that at least one digit > 0 is present in the string
  \d*          # integer part (optional)
  (?:          # decimal part:
   \.          # dot
   \d{1,2}     # plus one or two decimal digits
  )?           # (optional)
  \s*          # Optional whitespace
  $            # End of string  
  */
  decimalFrom: string;
  decimalTo: string;
  hasError: boolean = false;
  disableTo: boolean = false;
  disableFrom: boolean = false;
  errorMessage: string = "";
  constructor() { }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes) {
      if (changes.max || changes.min) {
        if (!isNaN(this.max))
          this.decimalTo = this.max.toFixed(2);
        if (!isNaN(this.min))
          this.decimalFrom = this.min.toFixed(2);
      }
    }
  }
  ngOnInit() {
  }
  isNegativeValue(value: string){
    if (!isNaN(+value) && +value < 0) {
      return true;
    }
  }
  onChangeFrom() {
    if (isNaN(+this.decimalFrom)) {
      this.broadcastError(true, "from", this.illegalFromMsg);
      return;
    }
    var regxFrom = this.decimal2PointsFromRegex.test(this.decimalFrom);
    if (!regxFrom) {
//      console.warn("regex");
      this.broadcastError(true, "from", this.illegalFromMsg);
      return;
    }
    this.broadcastError(false, "from", "");
  }
  onChangeTo() {
    if (isNaN(+this.decimalTo)) {
      this.broadcastError(true, "to", this.illegalToMsg);
      return;
    }
    var regxTo = this.decimal2PointsToRegex.test(this.decimalTo);
    if (!regxTo) {
      this.broadcastError(true, "to", this.illegalToMsg);
      return;
    }
    this.broadcastError(false, "to", "");
  }
  checkRange() {
    var from = +this.decimalFrom;
    var to = +this.decimalTo;
    if (isNaN(from) || isNaN(to))
      return;
    var isRangeValid: boolean = from <= to;
    if (!isRangeValid) {
      this.broadcastError(true, "range", this.invalidRangeMsg);
      return;
    }
    var inRangeFrom: boolean = this.allowNegativeValues
    ? true
    : from >= 0;//(+this.min.toFixed(2));
    var inRangeTo: boolean = to <= 99.99;//(+this.max.toFixed(2));
    if (!inRangeFrom || !inRangeTo) {
      this.broadcastError(true, "range", this.outOfRangeMsg);
      return;
    }
    if (!this.hasError)
      this.exposeValues.emit({ from: +this.decimalFrom, to: +this.decimalTo });
  }
  broadcastError(error: boolean, source: string, msg: string) {
    this.hasError = error;
    this.onError.emit(error);
    if (error) {
      this.errorMessage = msg;      
      this.onErrorMessage.emit(msg);
      switch (source) {
        case "from":
          this.disableTo = true;
          break;
        case "to":
          this.disableFrom = true;
          break;
      }
    }
    else {
      this.onErrorMessageClear.emit();
      this.disableTo = false;
      this.disableFrom = false;
    }
  }
}
