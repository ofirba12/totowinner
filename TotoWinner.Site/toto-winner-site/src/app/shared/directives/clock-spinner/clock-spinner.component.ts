import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs/internal/observable/timer';

@Component({
  selector: 'app-clock-spinner',
  templateUrl: './clock-spinner.component.html',
  styleUrls: ['./clock-spinner.component.css']
})
export class ClockSpinnerComponent implements OnInit {
  sec: number = 0;
  source: any;
  subscription$ : any;
  constructor() { }

  ngOnInit() {
  }
  start() {
    //output: 0,1,2,3,4,5......
    this.sec = 0;
    this.source = timer(0, 1000);
    this.subscription$ = this.source.subscribe(val => {
      this.sec = val;
    });
  }
  stop() {
    if (this.subscription$)
      this.subscription$.unsubscribe();
  }
}
