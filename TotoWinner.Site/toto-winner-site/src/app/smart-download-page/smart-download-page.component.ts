import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/api';
import { SmartDownloadComponent } from '../shared/dialogs';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'smart-download-page',
  templateUrl: './smart-download-page.component.html',
  styleUrls: ['./smart-download-page.component.css']
})
export class SmartDownloadPageComponent implements OnInit {

  constructor(public dialogService: DialogService) { }

  ngOnInit() {
    this.openSmartDownloadDialog();
  }
  openSmartDownloadDialog(){
    const ref = this.dialogService.open(SmartDownloadComponent, {
      data: {},
      header: 'הורדת טפסים',
      width: '40%',
//      height: '80%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .sync-program-dialog",
      contentStyle: { "font-size": "11px" }
    });
  }

}
