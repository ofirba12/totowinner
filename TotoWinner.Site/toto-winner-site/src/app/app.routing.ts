import { Routes, RouterModule } from '@angular/router';
import { OptimizerSystemComponent } from './systems/optimizer-system/optimizer-system.component'
import { AuthGuard } from './_guards';
import { LoginComponent } from './login/login.component';
import { BetsShrinkSystemComponent } from './systems/bets-shrink-system/bets-shrink-system.component';
import { SystemsLayoutComponent } from './_layouts/systems-layout/systems-layout.component';
import { SmartDownloadPageComponent } from './smart-download-page/smart-download-page.component';

const appRoutes: Routes = [
    {
        path: '',
        component: SystemsLayoutComponent,
        children: [
            { path: '', component: BetsShrinkSystemComponent, canActivate: [AuthGuard] },
            { path: 'optimizer', component: OptimizerSystemComponent },
            { path: 'backtesting', loadChildren: './systems/backtesting/backtesting.module#BacktestingModule' }
            //FOR V9          { path: 'backtesting', loadChildren: () => import('./systems/backtesting/backtesting.module').then(m => m.BacktestingModule) }
            //         { path: '', component: HomeComponent, pathMatch: 'full'},
            //   { path: 'about', component: AboutComponent },
            //   { path: 'test/:id', component: AboutComponent }
        ]
    },
    { path: 'download', component: SmartDownloadPageComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const Routing = RouterModule.forRoot(appRoutes, { useHash: true/*, enableTracing: true*/ })// , enableTracing: true }) // <-- debugging purposes only)