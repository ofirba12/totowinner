export interface IPricerData {
    bankersCounter : number;
    doublesCounter : number;
    trianglesCounter : number;
    betsCounter: string;
    betsCounterNumber: number;
    minRatesSum : number;
    maxRatesSum : number;
    averageRates : number;
    finalResult : number;
    sumAllRates : number;
    sumAllRates1 : number;
    sumAllRatesX : number;
    sumAllRates2 : number;    
}

export class PricerData implements IPricerData{
    bankersCounter : number = 0;
    doublesCounter : number = 0;
    trianglesCounter : number = 0;
    betsCounter: string = "0";
    betsCounterNumber: number = 0;
    minRatesSum : number = 0;
    maxRatesSum : number = 0;
    averageRates : number = 0;
    finalResult : number = 0;
    sumAllRates : number = 0;
    sumAllRates1 : number = 0;
    sumAllRatesX : number = 0;
    sumAllRates2 : number = 0;
    
    constructor(bankersCnt: number,
        doublesCnt: number,
        trianglesCnt: number,
        betsCnt: string,
        betCntNum : number,
        minRatesSm: number,
        maxRatesSm: number,
        avgRates: number,
        fnlResult: number,
        smAllRates: number,
        smAllRates1: number,
        smAllRatesX: number,
        smAllRates2: number
    ) {
        this.bankersCounter = bankersCnt;
        this.doublesCounter = doublesCnt;
        this.trianglesCounter = trianglesCnt;
        this.betsCounter = betsCnt;
        this.betsCounterNumber = betCntNum;
        this.minRatesSum = minRatesSm;
        this.maxRatesSum = maxRatesSm;
        this.averageRates = avgRates;
        this.finalResult = fnlResult;
        this.sumAllRates = smAllRates;
        this.sumAllRates1 = smAllRates1;
        this.sumAllRatesX = smAllRatesX;
        this.sumAllRates2 = smAllRates2;
    }
    init() {
        this.bankersCounter = this.doublesCounter = this.trianglesCounter = this.minRatesSum = 0;
        this.maxRatesSum = this.averageRates = 0; this.finalResult = 0; this.sumAllRates = 0;
        this.betsCounter = "0";
        this.betsCounterNumber = 0;
        this.sumAllRates1 = this.sumAllRatesX = this.sumAllRates2 = 0;
    }
}
