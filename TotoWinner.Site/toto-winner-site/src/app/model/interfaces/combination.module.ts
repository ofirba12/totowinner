//import { CombinationItem } from "./combination-item";
import { DropDownValue } from "./shared.module";

export interface CombinationGames {
    index: number;
    selected : boolean;
    games: CombinationItem[];
    min: number;
    max: number;
  }

export interface CombinationItem {
    index: number;
    combinationValues: DropDownValue[];
    selectedCombination : DropDownValue;
  }
