import { SelectItem } from "primeng/api";
import { CalculationStatus } from './shared.module';
export interface GameOptimizerData {
    index: number;
    name: string;
    rate_1: number;
    rate_X: number;
    rate_2: number;
    ui_rates: SelectItem[];
    bet1X2String_original: string;
    bet1X2String_optimized: string;
    calcProgress: string;
    finalBet: boolean;
}
export interface FromToItem{
    item: number;
    class: string;
}
export interface FromTo{
    from: FromToItem;
    to: FromToItem;
}
export enum AdvancedConditionType{
    Single1X2,
    SingleABC,
    Pair1X2,
    PairABC,
    Triplet1X2,
    TripletABC
}
export interface AdvancedConditionKey{
    type: AdvancedConditionType;
    index: number;
    titleType: string; //* 1X2 ABC
    titleShape: string; //* טור זוגות שלשות
}
export interface OptimizedFromTo{
    objectName: string;
    key?: AdvancedConditionKey;
    original: FromTo;
    optimized: FromTo;
}
export interface OptimizerConditions {    
    amount1X2 : OptimizedFromTo[]; //3 cells
    sequence1X2 : OptimizedFromTo[]; //3 cells
    breaks1X2: OptimizedFromTo[];//only 1 cell
    amountABC : OptimizedFromTo[]; //3 cells
    sequenceABC : OptimizedFromTo[]; //3 cells
    breaksABC: OptimizedFromTo[];//only 1 cell
    range: OptimizedFromTo[];//only 1 cell
    advancedObjects: OptimizedFromTo[]; //shapes and...
    optimizationStatus : CalculationStatus;
}
export interface OptimizerBets {
    optimizationStatus : CalculationStatus;
    gamesData: GameOptimizerData[];
}
export interface OptimizerData {
    programTitle: string;
    originalParentBetsCounter : number;
    optimizedParentBetsCounter: number;
    originalShrinkedBetsCounter : number;
    optimizedShrinkedBetsCounter: number;  
    bets: OptimizerBets;
    conditions: OptimizerConditions;
    shrinkSystemName: string;
}
export interface StartCalcOptimizerParam{
    targetBetsCounter: number;
    targetConditionCounter: number;
}
export interface StopExecutionReason{
    manualStop: boolean;
    status: CalculationStatus;
}