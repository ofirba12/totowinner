declare module BetsCalculation {

    export interface EventsBet {
        position: number;
        bet1: boolean;
        betX: boolean;
        bet2: boolean;
    }

    export interface EventsRate {
        position: number;
        rate1: number;
        rateX: number;
        rate2: number;
    }

    export interface MinMax {
        min: number;
        max: number;
    }

    export interface IncludeSettings {
        amountSum: MinMax;
        amount1: MinMax;
        amountX: MinMax;
        amount2: MinMax;
        sequence1: MinMax;
        sequenceX: MinMax;
        sequence2: MinMax;
        sequenceBreak: MinMax;
    }
    export interface ExcludeSettings {
        amountSum: MinMax;
        amount1: MinMax;
        amountX: MinMax;
        amount2: MinMax;
        sequence1: number;
        sequenceX: number;
        sequence2: number;
        sequenceBreak: MinMax;
        activateSumAmountFilter: boolean;
        activateOneAmountFilter: boolean;
        activateXAmountFilter: boolean;
        activateTwoAmountFilter: boolean;
        activateOneSequenceFilter: boolean;
        activateXSequenceFilter: boolean;
        activateTwoSequenceFilter: boolean;
        activateBreakFilter: boolean;
    }

    export interface Combination1X2Settings {
        combinations: string[];
        minMax: MinMax;
        isActive: boolean;
        type: number;
    }
    export interface LettersSetting {
        combinations: string[];
        minMax: MinMax;
        isActive: boolean;
        type: number;
    }
    export interface CombinationLettersSettings {
        lettersSettings: LettersSetting[];
        minMaxA: MinMax;
        minMaxB: MinMax;
        minMaxC: MinMax;
        minMaxSequenceLengthA?: MinMax;
        minMaxSequenceLengthB?: MinMax;
        minMaxSequenceLengthC?: MinMax;
        minMaxBreak?: MinMax;
    }
    export interface Combination { //same as MinMax????
        min: number;
        max: number;
    }

    export interface ShapesPattern {
        combinations: Combination[];
        minMax: MinMax;
        isActive: boolean;
        type: number;
    }

    export interface FormSettings {
        includeSettings: IncludeSettings;
        excludeSettings?: ExcludeSettings;
        combination1X2Settings?: Combination1X2Settings[];
        combinationLettersSettings?: CombinationLettersSettings;
        shapes2?: ShapesPattern[];
        shapes3?: ShapesPattern[];
        lettersShapes2?: ShapesPattern[];
        lettersShapes3?: ShapesPattern[];
    }

    export interface FormCreateRequest {
        eventsBets: EventsBet[];
        eventsRates: EventsRate[];
        formSettings: FormSettings;
        cleanOnDiff: boolean;
        generateOutputFile: boolean;
    }
}

