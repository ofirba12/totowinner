import { SelectItem } from "primeng/api";
import { GamesComponent } from "src/app/systems/bets-shrink-system/components/games/games.component";
import { FormComponent } from "src/app/systems/bets-shrink-system/components/form/form.component";
import { ProgramType } from 'src/app/state/app';

export interface Program {
    beginDate?;
    endDate?;
    id?;
    toto_round_id?;
}
export interface ProgramDataSyncRequest {
    ProgramEndDate: string;
    ProgramType: ProgramType;
    TotoRoundId: number;
}
export interface ProgramDataRequest {
    ProgramEndDate: string;
    ProgramType: ProgramType;
}
export interface ProgramData {
    programGames: ProgramDetails[];
    finalResult: number;
    openForBets: boolean;
    programDate: Date;
    programType: ProgramType;
    programTitle: string;
    uniqueId: string;
}
export interface BetProgramDetail {
    rate: number;
    won: boolean;
    rateLabel: string;
}

export interface ProgramDetails {
    gameIndex?;
    betStatus?;
    statusStyle?;
    gameTime?;
    leagueName?;
    gameName?;
    openForBets : boolean;
    bets?: SelectItem[];
    betsProgramDetail: BetProgramDetail[];
    gameResult?;
    bet1X2String?;
    betsSelection?;
    validations?;
    selectedRates?;
}
export interface ShrinkSystemInternalState {
    programData : ProgramData;
    isProgramSelectionDirty : boolean;
    calculationResult : any; //TODO Should i move this to interface???
    totalBetsAfterCalcLabel : string;
    programs: SelectItem[];
    selectedProgramType: SelectItem; 
    selectedProgramYear: SelectItem;
    selectedProgram: SelectItem;
    form: BetsCalculation.FormCreateRequest;
    systemName : string;
}
export interface RealTimePipeGetRequest{
    programType: ProgramType;
}
export interface RatesPipe{
    gameIndex: number;
    rate1 : number;
    rateX : number;
    rate2: number;
}
export interface RealTimePipeGetResponse{
    rates: RatesPipe[];
    isSuccess: boolean;
    errorMessage?: string;
}
export interface RealTimePipeSetRequest{
    programType: ProgramType;
    rates: RatesPipe[];
}