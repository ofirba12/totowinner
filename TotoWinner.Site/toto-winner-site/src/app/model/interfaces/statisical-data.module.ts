import { ProgramType } from 'src/app/state/app';

export interface GetStatisticalDataRequest {
    year: number;
    type: ProgramType;
}
export interface BasicData3 {
    programEndDate: Date;
    roundId: number;
    amount1: number;
    amount2: number;
    amount3: number;
}
export interface BasicData1 {
    programEndDate: Date;
    roundId: number;
    amount: number;
}
export interface Shapes2Data1X2 {
    programEndDate: Date;
    roundId: number;
    amount11: number;
    amount1X: number;
    amount12: number;
    amountX1: number;
    amountXX: number;
    amountX2: number;
    amount21: number;
    amount2X: number;
    amount22: number;
}
export interface Shapes2DataABC
{
    programEndDate: Date;
    roundId: number;
    amountAA: number;
    amountAB: number;
    amountAC: number;
    amountBA: number;
    amountBB: number;
    amountBC: number;
    amountCA: number;
    amountCB: number;
    amountCC: number;
}
export interface GroupsBasicData
{
    programEndDate: Date;
    roundId: number;
    amount0: number;
    amount1: number;
    amount2: number;
    amount3Plus: number;
}
export interface Shapes3Data1X2
{
    programEndDate: Date;
    roundId: number;
    amount1: Shapes2Data1X2;
    amountX: Shapes2Data1X2;
    amount2: Shapes2Data1X2;
    amount3Plus: number;
}
export interface Shapes3DataABC
{
    programEndDate: Date;
    roundId: number;
    amountA: Shapes2DataABC;
    amountB: Shapes2DataABC;
    amountC: Shapes2DataABC;
    amount3Plus: number;
}
export interface RelationsData
{
    programEndDate: Date;
    roundId: number;
    min: number;
    middle : number;
    max : number; 
    home : number;
    draft : number;
    away : number;
    sum : number; 
    finalResult : number;
    deltaMin : number;
    deltaMiddle : number;
    deltaMax : number; 
    deltaHome : number;
    deltaDraft : number;
    deltaAway : number;
} 
    
export interface GetStatisticalDataResponse {
    amount1X2: BasicData3[];
    amountABC: BasicData3[];
    sequenceLength1X2: BasicData3[];
    sequenceLengthABC: BasicData3[];
    breaks1X2: BasicData1[];
    breaksABC: BasicData1[];
    shapes2Data1X2: Shapes2Data1X2[];
    shapes2DataABC: Shapes2DataABC[];
    shapesGroups2Data1X2: GroupsBasicData[];
    shapesGroups2DataABC: GroupsBasicData[];
    shapes3Data1X2: Shapes3Data1X2[];
    shapes3DataABC: Shapes3DataABC[];
    shapesGroups3Data1X2: GroupsBasicData[];
    shapesGroups3DataABC: GroupsBasicData[];
    relationsData: RelationsData[];
    isSuccess: boolean;
    errorMessage?: string;
}