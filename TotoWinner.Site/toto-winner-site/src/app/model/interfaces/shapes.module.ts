//declare module ShapesModule {
  export interface ShapesData {
    index: number;
    selected: boolean;
    shapes_t1: ShapeDataItem[];
    shapes_t2?: ShapeDataItem[];
    shapes_t3?: ShapeDataItem[];
    fromMin: number;
    toMax: number;
    currentFrom: number;
    currentTo: number;
  }

  export interface ShapeDataItem {
    rowIndex: number; // 0 -from, 1- to
    dataValues: MinMaxData[];
  }
  export interface MinMaxData {
    currentValue?: number;
    minValue: number;
    maxValue: number;
    blinkClass: string;
  }
  export enum ShapesFromToRangeType{
    Default,
    ZeroZero,
    OneOne,
    TwoTwo,
    ThreePlus
  }
//}