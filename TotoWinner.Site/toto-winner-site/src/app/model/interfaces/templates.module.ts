import { SelectItem } from "primeng/api";
import { ProgramType } from 'src/app/state/app';

export interface Template {
    id: number;
    type: ProgramType;
    name: string;
    value: string;
}
export interface GetAllTemplatesResponse{
    templates: Template[];
}
export interface TemplatesState{
    templates: Template[],
    watcher: TemplateWatcherState,
    showBets: TemplateWatcher,
    error: string
}
export interface TemplateWatcherState{
    needsUpdate: boolean;
    collection: { [id: number] : TemplatesBaseBetsResponse; }
}
export interface TemplateRequest{
    templateInfo: Template;
}
export interface AddTemplateResponse{
    templateInfo: Template;
}
export interface DeleteTemplateResponse{
    templateId : number;
}
export interface TemplateWatcher{
    id: number;
    name: string;
    value: string;
    bets: Bet[];
    status: TemplateBaseBetsStatus;
    statusStyle: any;
}
export interface TemplateWatcherRepository{
    templatesBaseBets: { [id: number] : TemplatesBaseBetsResponse; };
}
export interface TemplateWatcherParameters {
    programType : ProgramType;
    bets: SelectItem[][];
    templates : Template[];
    watcherNeedsUpdate: boolean;
//    rateUpdateTimeStamp: Date;
}
export interface TemplateWatcherDialogData {
    rates: SelectItem[][];
    templateData: TemplateWatcher;
}
export interface RatesInfo{
    gameIndex : number;
    rate1 : number;
    rateX : number;
    rate2 : number;
}
export interface WatchTemplatesRequest{
    templates: Template[];
    rates: RatesInfo[];
}
export interface Bet {
    position: number;
    bet1: boolean;
    betX: boolean;
    bet2: boolean;
}
export interface TemplatesBaseBetsResponse{
    baseBets : { [id: number] : Bet; };
    status: TemplateBaseBetsStatus;
}
// export interface WatchTemplatesResponse{
//     templatesBaseBets: { [id: number] : Bet; }
// }
export interface WatchTemplatesResponse{
    templatesBaseBets: { [id: number] : TemplatesBaseBetsResponse; };
}
export enum TemplateBaseBetsStatus{
    Success = 1,
    InvalidTemplate = 2,
    NoBaseFound = 3
}