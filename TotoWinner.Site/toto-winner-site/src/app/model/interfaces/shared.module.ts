import { ProgramDetails, ProgramData } from "./program.module";
import { SelectItem } from 'primeng/api';
import { ProgramType, OptimizerExecutionState } from 'src/app/state/app';

export enum SystemType {
  Shrink = 1,
  Optimizer = 2,
  Backtesting = 3
//### DB Update:
//### delete TWSystems where Type=2 
//### update TWSystems set Type=2 where Type=3  
//  ShrinkToOptimizer = 2,
}
export enum SystemStateStatus {
  None = 0,
  OnLoad = 1,
  RefreshBrowser = 2,
  OpenNewShrinkSystem  = 3,
  LoadValuesStarted = 4,
  LoadValuesFinished = 5
}
export enum CalculationStatus {
  None = 0,
  Queue = 1,
  Running = 2,
  Stopped = 3,
  Finished = 4,
  Error = 5
}
export interface SystemParameters {
  systemId : number;
  type : number;
  programId : number;
  programType : ProgramType;
  name : string;
  systemPayload: string;
  referenceSystemId: string;
  lastUpdate: Date;
}
export interface SystemParametersWithFinalResult {
  systemId : number;
  type : number;
  programId : number;
  programType : ProgramType;
  name : string;
  systemPayload: string;
  referenceSystemId: string;
  lastUpdate: Date;
  finalResult: number;
}
export interface DropDownValue {
  label: string;
  value: number;
}
export interface LoadShrinkSystemRequest {
  systemId : number;
}
export interface GetAllSystemsRequest {
  type : string;
}
export interface GetAllSystemsForUnionRequest {
  type : ProgramType;
}
export interface GetAllSystemsResponse {
  systems: SystemParameters[];
}
export interface GetAllSystemsWithFinalResultResponse{
  systems: SystemParametersWithFinalResult[];
}
export interface ShrinkSystemData {
  uniqueId: string;
  systemType: SystemType;
  name: string;
  system: BetsCalculation.FormCreateRequest;
  games: ProgramDetails[];
  state: ShrinkSystemState;
  uiState: string; //string of ShrinkSystemUIState
}
export interface ShrinkSystemState {
  programTitle : string;
  betsCounter: number;
  shrinkedBetsCounter: number;
  finalResult: number;
  openForBets : boolean;
  programDate: Date;
  programType: string;
  calculationResult: any;
}
export interface ShrinkSystemUIState {
  isProgramSelectionDirty : boolean;
  selecedPrograms: SelectItem[];
  selectedProgramType : SelectItem;
  selectedProgramYear : SelectItem;
  selectedProgram: SelectItem;
  totalBetsAfterCalcLabel: string;
}
export interface ShrinkSystemDefinitions{
  programData: ProgramData,
  isProgramDataDirty: boolean,
  form: BetsCalculation.FormCreateRequest,
  isFormDirty: boolean,
}
export interface ShrinkSystemPayload{
  name: string,
  uniqueId: string,
  year: number,
//  programType: ProgramType,
  //games: ProgramDetails[],
  definitions: ShrinkSystemDefinitions;
  result: ShrinkResultModule.SeResults
}
export interface SaveShrinkSystemResponse{
  systemId : number
}
export interface ExecutionStateRequest {
  executionId : number;
}
export interface ExecutionOptimizerParameters{
  optimizedSystemId: number;
  targetBetsCounter: number;
  targetAdditionalConditionCounter: number;
}
export interface StartOptimizerRequest {
  parameters : ExecutionOptimizerParameters;
}
export interface StopOptimizerRequest {
  executionId : number;
}
export interface OptimizerResultsToExcelRequest {
  executionId : number;
  optimizerSystemId : number;
}

export interface SaveOptimizerSystemRequest {
  uniqueId: string;
  systemType: SystemType;
  name: string;
  optimizerData: string; 
  optimizedSystemId: number;
//  lastExecutionId: number;
  execution: OptimizerExecutionState;
}
export interface LoadOptimizerSystemResponse {
  systemData : SaveOptimizerSystemRequest
  optimizedSystemName : string;
}
export interface LoadOptimizerSystemRequest {
  systemId : number;
}
export interface UnionSystemsRequest {
  systemIds: number[];
}
export interface Container1X2 {
  item1: number;
  itemX: number;
  item2: number;
}
export interface UnionSystemsResponse {
  unionResultId: number;
  counterBeforeUnion: number;
  counterAfterUnion: number;
  counterCoverage: Container1X2[];
  counterPercentCoverage: Container1X2[];
  isSuccess: boolean;
  errorMessage?: any;  
}
export enum FormTypeGeneration
{
    None = 0,
    Player = 1,
    Excel = 2,
    SMART = 3
}
export interface GenerateFormToDownloadRequest{
  unionResultId: number;
  formType: FormTypeGeneration;
}
export interface GenerateFormToDownloadResponse{
  outputFile : string;
  isSuccess: boolean;
  errorMessage?: any;  
}
export interface DeleteSmartFileRequest{
  id: number;
}
export interface ResponseBase{
  isSuccess: boolean;
  errorMessage: string;
}
export interface GetSmartFilesResponse{
  files: SmartFileContainer[];
}
export interface SmartFileContainer{
  id: number;
  systemIds : string;
  lastUpdate : Date;
  downloadUrl: string;
  filename: string;
}