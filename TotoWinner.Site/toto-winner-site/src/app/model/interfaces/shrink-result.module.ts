declare module ShrinkResultModule {

    export interface FinalResult {
        _isSkipped: boolean;
        _isIncludedInResults: boolean;
        _resultColumn: string;
        _pointersToResults: any[];
        numOfPointers: number;
        result: string;
        isInMap: boolean;
        isSkipped: boolean;
        isIncludedInResults: boolean;
        testResult: number;
    }

    export interface TestsCounters {
        0: number;
        1: number;
        2: number;
        3: number;
        4: number;
        5: number;
        6: number;
        7: number;
        8: number;
        9: number;
        10: number;
        11: number;
        12: number;
        13: number;
        14: number;
        15: number;
        16: number;
    }

    export interface ResultsStatistics {
        found: number;
        testsCounters: TestsCounters;
    }

    export interface SeResults {
        finalResults: FinalResult[];
        resultsStatistics: ResultsStatistics;
    }

    // export interface RootObject {
    //     seResults: SeResults;
    //     outputFile: string;
    // }
}