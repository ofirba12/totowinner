import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BacktestingRoutingModule } from './backtesting-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BacktestingSystemComponent } from './components/backtesting-system/backtesting-system.component';
import { BacktestingShellComponent } from './containers/backtesting-shell/backtesting-shell.component';
import { PrimeNgModule } from '../../primeng/primeng.module';
// import { StoreModule } from '@ngrx/store';
// import { reducer } from './state/backtesting.reducer';
import { EffectsModule } from '@ngrx/effects';
import { BacktestingEffects } from './state/backtesting.effects';
import { BacktestingDefinitionsComponent } from './components/backtesting-definitions/backtesting-definitions.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BetsDialogsComponent } from './components/bets-dialogs/bets-dialogs.component';
import { ShrinkBasicComponent } from './components/backtesting-definitions/shrink-basic/shrink-basic.component';
import { ShrinkShapesComponent } from './components/backtesting-definitions/shrink-shapes/shrink-shapes.component';
import { ShrinkRatesComponent } from './components/backtesting-definitions/shrink-rates/shrink-rates.component';

@NgModule({
  declarations: [
    BacktestingSystemComponent, 
    BacktestingShellComponent, 
    BacktestingDefinitionsComponent, BetsDialogsComponent, ShrinkBasicComponent, ShrinkShapesComponent, ShrinkRatesComponent,
  ],
  imports: [
    CommonModule,
//    BrowserAnimationsModule,
    BacktestingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PrimeNgModule,
//    StoreModule.forFeature('backtesting', reducer),
    EffectsModule.forFeature(
      [ BacktestingEffects ]
    ),
  ]
})
export class BacktestingModule { }
