import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { BacktestingPastProgramSelected, BtProgramBetData } from '../../backtesting';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../state';
import * as appActions from './../../state/backtesting.actions';
import { SelectItem } from 'primeng/api';
import { ProgramState, ProgramType } from 'src/app/state/app';


@Component({
  selector: 'backtesting-bets-dialogs',
  templateUrl: './bets-dialogs.component.html',
  styleUrls: ['./bets-dialogs.component.css']
})
export class BetsDialogsComponent implements OnInit, OnChanges {
  @Input() program: BacktestingPastProgramSelected;
  @Input() programsRepository: ProgramState[];
  @Input() programType: ProgramType;
  show: boolean = false;
  title: string;
  uiBaseBetData: SelectItem[][] = [];
  uiWinBetData: SelectItem[][] = [];
  constructor(private store: Store<fromApp.AppState>) { }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes && changes.program && changes.program.currentValue) {
      this.fetchData();
      this.show = true;
    }
    else
      this.show = false;

  }
  hideDialog() {
    this.store.dispatch(new appActions.HideBacktestingPastProgram());
  }
  fetchData() {
    this.title = `מחזור- ${this.program.programNumber}`;
    var fList = this.programsRepository.filter(item => item.type == this.programType);
    if (fList.length > 0) {
      var all: SelectItem[] = fList[0].programs;
      for (let i = 1; i < fList.length; i++) {
        all = all.concat(fList[i].programs);
      }
      var pData = all.find(item => +item.value.toto_round_id == this.program.programNumber);
      if (pData)
        this.title = pData.title;// `מחזור- ${this.program.programId}`;// | ${this.program.programDate}`;
    }
    if (this.program.baseBetData != null) {
      this.uiBaseBetData = [];
      // this.program.baseBetData = [
      //   { rates: [1.1, 1.2, 1.3], selection: [true, false, false] },
      //   { rates: [2.1, 2.2, 2.3], selection: [true, true, false] },
      //   { rates: [Math.floor(Math.random() * 3) + 1, 3.2, 3.3], selection: [true, true, true] }
      // ];
      for (let row of this.program.baseBetData) {
        this.uiBaseBetData.push(this.createBetsItems(row, true));
      }
    }
    if (this.program.winBetData != null) {
      this.uiWinBetData = [];
      // this.program.winBetData = [
      //   { rates: [1.1, 1.2, 1.3], selection: [true, false, false] },
      //   { rates: [2.1, 2.2, 2.3], selection: [false, true, false] },
      //   { rates: [Math.floor(Math.random() * 3) + 1, 3.2, 3.3], selection: [false, false, true] }
      // ];
      for (let row of this.program.winBetData) {
        this.uiWinBetData.push(this.createBetsItems(row, false));
      }
    }
  }
  createBetsItems(betsDetails: BtProgramBetData, isBaseBet: boolean): SelectItem[] {
    var chosenStyle = isBaseBet ? "wn-bt-basebet-selected" : "wn-bt-winbet-selected";
    let betsArr: SelectItem[] = [
      { label: betsDetails.rates[0].toFixed(2), value: 1, disabled: true, styleClass: betsDetails.selection[0] ? chosenStyle : "" },
      { label: betsDetails.rates[1].toFixed(2), value: 2, disabled: true, styleClass: betsDetails.selection[1] ? chosenStyle : "" },
      { label: betsDetails.rates[2].toFixed(2), value: 3, disabled: true, styleClass: betsDetails.selection[2] ? chosenStyle : "" }];
    return betsArr;
  }

  ngOnInit() {
  }

}
