import { Component, OnInit, EventEmitter, Output, Input, SimpleChange, OnChanges, ViewChild } from '@angular/core';
import { RunBacktestingRequest, BacktestingDefinitions, BacktestingExecution, StopExecutionRequest, PollExecutionRequest, BtRowData, BtTotalRowData, BtSummaryRowData } from '../../backtesting';
import { CalculationStatus } from 'src/app/model/interfaces/shared.module';
import { ProgramType, NotificationType } from 'src/app/state/app';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../state';
import * as btActions from './../../state/backtesting.actions';
import * as appActions from './../../../../state/app.actions';
import { TotoGamesService, ExcelService, BacktestingService, AppHelperService, AppConnetorService } from 'src/app/_services';

@Component({
  selector: 'backtesting-system',
  templateUrl: './backtesting-system.component.html',
  styleUrls: ['./backtesting-system.component.css']
})
export class BacktestingSystemComponent implements OnInit, OnChanges {
  @Input() definitions: BacktestingDefinitions;
  @Input() execution: BacktestingExecution;
  @Output() runSystem = new EventEmitter<RunBacktestingRequest>();
  @Output() stopSystem = new EventEmitter<StopExecutionRequest>();
  @Output() stopOnException = new EventEmitter<any>();
  @Output() pollSystem = new EventEmitter<PollExecutionRequest>();
  
  //@ViewChild(ClockSpinnerComponent) clockSpinner: ClockSpinnerComponent;
  cols: any[];
  rows: BtRowData[];
  betsSize: number = 16;
  totals: BtTotalRowData;
  summary: BtSummaryRowData;
  showWaitingMessage: boolean;
  constructor(private store: Store<fromApp.AppState>,
    private backtestingService: BacktestingService,
    private totoGameService: TotoGamesService,
    private excelService: ExcelService,
    private helper: AppHelperService,
    private gatewayService: AppConnetorService) { }

  ngOnInit() {
    this.onProgramTypeChange(ProgramType.Winner16);
    this.gatewayService.registerBacktesting(this);
  }
  onProgramTypeChange(programType: ProgramType) {
    switch (+programType) {
      case ProgramType.Winner16:
        this.betsSize = 16;
        break;
      case ProgramType.WinnerWorld:
        this.betsSize = 15;
        break;
      case ProgramType.WinnerHalf:
        this.betsSize = 14;
        break;
    }
    this.cols = [
      { field: 'show', header: '' },
      { field: 'triplets', header: 'משולשים' },
      { field: 'doubles', header: 'כפולים' },
      { field: 'bankers', header: 'בנקרים' },
      { field: 'programDescription', header: 'מחזור' },
      { field: 'programEndDate', header: 'תאריך' },
      { field: 'bets', header: 'כמות טורים' }
    ];
    for (var result = this.betsSize; result >= 0; result--) {
      this.cols.push({ field: result, header: result })
    }
    this.initExecutionResults();
  }
  clearState() : void{
    this.store.dispatch(new btActions.ClearState());
  }
  initExecutionResults(): void {
    this.rows = []
    // { triplets: 2, doubles: 5, bankers: 10, program: '191301', bets: 288, results: [0, 0, 4, 4, 0, 0, 20, 80, 60, 50, 35, 25, 0, 1, 5, 3, 1] },
    // { triplets: 5, doubles: 6, bankers: 4, program: '191201', bets: 5184, results: [30, 4, 17, 235, 180, 18, 900, 880, 262, 852, 953, 529, 239, 80, 5, 0, 0] },
    // { triplets: 7, doubles: 0, bankers: 9, program: '191101', bets: 2187, results: [29, 15, 172, 25, 152, 125, 585, 529, 269, 256, 15, 8, 5, 0, 1, 1, 0] }
    //    ];
    //    this.totals = { bets: 7659, results: [59, 19, 193, 264, 332, 143, 1505, 1489, 591, 1158, 1003, 562, 244, 81, 11, 4, 1] };
    this.backtestingService.rowsExecutionData = this.rows;
    this.summary = { programsCounter: 0, notValidProgramCounter: 0, notExistsProgramCounter: 0 };
    this.totals = null;
  }
  setExecutionData() {
    this.rows = this.rows.concat(this.execution.results);
    this.backtestingService.rowsExecutionData = this.rows;
    this.totals = this.execution.totals;
    this.summary = this.execution.summary;
  }
  getColumnStyle(field: string) {
    switch (field) {
      case 'show':
        return "wn-show-col-class";
      case 'bets':
        return "wn-bets-col-class";
      case 'programDescription':
      case 'programEndDate':
        return "wn-program-col-class";
      case 'triplets':
      case 'doubles':
      case 'bankers':
        return "wn-counters-col-class";
    }
  }
  getProgramDescriptionStyle(fieldValue: string) {
    var check = +fieldValue;
    if (Number.isNaN(check)) {
      if (fieldValue == "אין טור")
        return "wn-bt-nobet";
      if (fieldValue == "ת.ל.ת" || fieldValue == "גדול מ-15 מליון")
        return "wn-bt-invalid-program";
    }
  }
  getResultStyle(field: string, data: number) {
    if (data > 0) {
      if (+field == this.betsSize)
        return "wn-first-winner";
      if (+field == this.betsSize - 1)
        return "wn-second-winner";
      if (+field == this.betsSize - 2)
        return "wn-third-winner";
      if (+field == this.betsSize - 3)
        return "wn-forth-winner";
      if (+field == this.betsSize - 4)
        return "wn-fifth-winner"
    }
  }
  isResultTitle(title: string) {
    var check = +title;
    if (Number.isNaN(check))
      return false;
    return true;
  }
  styleField(field: string, data: any) { //could be number or date
    if (field == 'bets' || this.isResultTitle(field))
      return data.toLocaleString();
    if (field == 'programEndDate') {
      var endDateLabel = this.helper.getFormatedDate(new Date(data), "DD.MM.YYYY");
      return endDateLabel;
    }
    return data;
  }
  showBets($event, rowData: BtRowData) {
    this.store.dispatch(new btActions.ShowBacktestingPastProgram({
      programId: rowData.programPersistanceId,
      programNumber: rowData.programNumber,
      executionId: this.execution.executionId
    }));
  }
  isValidateDefinitions(): boolean {
    // if (this.definitions.bankersLimits.max + this.definitions.doublesLimits.max > this.betsSize) {
    //   this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: `מבנה טור בסיס: סכום תנאי מקסימום גדול מ- ${this.betsSize}` }));
    //   return false;
    // }
    if (this.definitions.bankersLimits.min + this.definitions.doublesLimits.min > this.betsSize) {
      this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: `מבנה טור בסיס: סכום תנאי מינימום גדול מ- ${this.betsSize}` }));
      return false;
    }
    this.store.dispatch(new appActions.ClearMessage());
    return true;
  }
  run(): void {
    if (this.isValidateDefinitions() == true) {
      var request: RunBacktestingRequest = {
        definitions: this.definitions
      }
      this.showWaitingMessage = true;
      this.initExecutionResults();
      this.runSystem.emit(request);
    }
  }
  stop(): void {
    this.showWaitingMessage = false;
    var request: StopExecutionRequest = {
      executionId: this.execution.executionId
    }
    this.stopSystem.emit(request);
    //this.clockSpinner.stop();
  }
  handleRunningProcess(exeResult: BacktestingExecution) {
//    console.log("bt results:", exeResult);
    if (exeResult.isSuccess) {
      switch (exeResult.status) {
        case CalculationStatus.Finished:
          this.setExecutionData();
          this.stop();
          console.log("bt finished", exeResult.status);
          break;
        case CalculationStatus.Stopped:
          this.showWaitingMessage = false;
          console.log("bt stopped", exeResult.status);
          break;
        case CalculationStatus.Error:
          this.stop();
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: `אירעה שגיאת בריצה מספר ${this.execution.executionId}` }));
          console.log("bt error", exeResult.status);
          break;
        case CalculationStatus.Running:
          this.showWaitingMessage = false;
          console.log("bt running", exeResult.status);
          this.setExecutionData();
          break;
      }
    }
    else{
      this.showWaitingMessage = false;
      this.stopOnException.emit();
      this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: `אירעה תקלה לא צפוייה בהצגת נתוני הריצה` }));
      console.log("bt exception", exeResult.status);
    }
  }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes) {
      if (changes.execution && changes.execution.currentValue) {
        this.handleExecutionChanges(changes);
      }
      if (changes.definitions && changes.definitions.currentValue) {
        this.hadnleDefinitionChanges(changes);
      }
    }
  }
  hadnleDefinitionChanges(changes: { [propKey: string]: SimpleChange }): void {
    var current = changes.definitions.currentValue;
    var previous = changes.definitions.previousValue;
    if (current != null && previous != null
      && current.programType != previous.programType) {
      this.onProgramTypeChange(current.programType);
    }
  }
  handleExecutionChanges(changes: { [propKey: string]: SimpleChange }): void {
    var current = changes.execution.currentValue;
    var previous = changes.execution.previousValue;
    if ((current != null && previous == null)
      || (previous.executionId != null && current.executionId != previous.executionId)) {
      switch (current.status) {
        case CalculationStatus.Running:
        case CalculationStatus.Queue:
          var request: PollExecutionRequest = {
            executionId: this.execution.executionId,
            fromIndex: this.rows.length
          }
          this.rows = [];
          //this.clockSpinner.start();
          this.pollSystem.emit(request);
          break;
      }
    }
    else if (current != null && previous != null
      && current.executionId == previous.executionId) {
      this.handleRunningProcess(current);
    }
  }

  runButtonDisabled(): boolean {
    var disabled = (this.execution && this.execution.status == CalculationStatus.Running)
      || this.showWaitingMessage
      || (this.definitions && this.definitions.fromDate == null || this.definitions.toDate == null);

    return disabled;
  }
  exportToExcel(): void {
    var excelRows: any[] = [];
    for (let row of this.rows) {
      var data: any = {}
      var index = 0;
      for (let res of row.results) {
        data[index] = res;
        index++;
      }
      data["כמות טורים"] = row.bets;
      data["תאריך"] = this.helper.getFormatedDate(new Date(row.programEndDate), "DD.MM.YYYY");
      data["מחזור"] = row.programDescription;
      data["בנקרים"] = row.bankers;
      data["כפולים"] = row.doubles;
      data["משולשים"] = row.triplets;
      excelRows.push(data);
    }
    this.excelService.exportAsExcelFile(excelRows, 'bt_result');
  }
}
