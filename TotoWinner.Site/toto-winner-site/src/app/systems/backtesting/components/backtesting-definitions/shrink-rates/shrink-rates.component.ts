import { Component, OnInit } from '@angular/core';
import { RatesShrinkFieldType, BtShrinkRatesSelectItemPayload, BtShrinkRatesItemPayload, BtShrinkRatesValidItemPayload, DefaultMinMax } from '../../../backtesting';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../../state';
import * as appActions from './../../../state/backtesting.actions';

@Component({
  selector: 'backtesting-shrink-rates',
  templateUrl: './shrink-rates.component.html',
  styleUrls: ['./shrink-rates.component.css'],
  animations: [
    trigger('errorState', [
      state('hidden', style({
        opacity: 0
      })),
      state('visible', style({
        opacity: 1
      })),
      transition('visible => hidden', animate('400ms ease-in')),
      transition('hidden => visible', animate('400ms ease-out'))
    ])
  ],  
})
export class ShrinkRatesComponent implements OnInit {
  selectedRows: boolean[] = [
    false,
    false,
    false,
    false,
    false,
    false
  ];
  defaultRangeMin: number = 0;
  defaultRangeMax: number = 99;
  errorMessageArr: string[] = [
    null,
    null,
    null,
    null,
    null,
    null
  ];
  constructor(private store: Store<fromApp.AppState>) { }
  ngOnInit() {
    var range : DefaultMinMax = { min : this.defaultRangeMin, max: this.defaultRangeMax};
    this.store.dispatch(new appActions.InitializeRatesShrinkItems(range));
  }
  onSelect($event : boolean,fieldType: RatesShrinkFieldType){
    var item: BtShrinkRatesSelectItemPayload = {
      type: fieldType,
      value: $event
    }
    this.store.dispatch(new appActions.UpdateSelectedRatesShrinkItem(item));
  }
  setRates( $event: { from: number, to: number }, fieldType: RatesShrinkFieldType){
    var item: BtShrinkRatesItemPayload = {
      type: fieldType,
      from: $event.from,
      to: $event.to
    }
    this.store.dispatch(new appActions.UpdateRatesShrinkItem(item));
  }
  onErrorMessage($event: string, fieldType: RatesShrinkFieldType) {
    this.errorMessageArr[fieldType] = $event;
    var item: BtShrinkRatesValidItemPayload = {
      type: fieldType,
      value: false
    }
    this.store.dispatch(new appActions.ValidateRatesShrinkItem(item));
    this.selectedRows[fieldType] = false;
    this.onSelect(false, fieldType);
  }
  onErrorMessageClear(fieldType: RatesShrinkFieldType) {
    this.errorMessageArr[fieldType] = null;
    var item: BtShrinkRatesValidItemPayload = {
      type: fieldType,
      value: true
    }
    this.store.dispatch(new appActions.ValidateRatesShrinkItem(item));
  }
  CheckState(message, fieldType: RatesShrinkFieldType): boolean {
    if (this.errorMessageArr[fieldType]) {
      message.text = this.errorMessageArr[fieldType];
      return true;
    }
    return false;
  }
}
