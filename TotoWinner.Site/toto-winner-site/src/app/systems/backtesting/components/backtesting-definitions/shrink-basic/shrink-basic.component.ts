import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BasicShrinkFieldType, BtShrinkBasicItemPayload, DefaultMinMax } from '../../../backtesting';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../../state';
import * as appActions from './../../../state/backtesting.actions';

@Component({
  selector: 'backtesting-shrink-basic',
  templateUrl: './shrink-basic.component.html',
  styleUrls: ['./shrink-basic.component.css']
})
export class ShrinkBasicComponent implements OnInit, OnChanges {
  @Input() defaultMin: number;
  @Input() defaultMax: number;
  constructor(private store: Store<fromApp.AppState>) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes &&
      changes.defaultMax && changes.defaultMax.currentValue) {
        var minMax : DefaultMinMax = { 
          min: changes.defaultMin
            ? changes.defaultMin.currentValue
            : 0, 
          max: changes.defaultMax.currentValue};
        this.store.dispatch(new appActions.InitBasicShrinkCollection(minMax));
    }
  }

  ngOnInit() {
  }
  minChanges($value: number, fieldType: BasicShrinkFieldType) {
    var item: BtShrinkBasicItemPayload = {
      type: fieldType,
      value: $value
    }
    this.store.dispatch(new appActions.UpdateBasicShrinkFromField(item));
  }
  maxChanges($value: number, fieldType: BasicShrinkFieldType) {
    var item: BtShrinkBasicItemPayload = {
      type: fieldType,
      value: $value
    }
    this.store.dispatch(new appActions.UpdateBasicShrinkToField(item));
  }
}
