import { Component, OnInit, Input } from '@angular/core';
import { BacktestingDefinitions, BtGamesDefinitionRow, BtBetType, LimitType } from '../../backtesting';
import { SelectItem } from 'primeng/api';
import { ProgramType } from 'src/app/state/app';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../state';
import * as appActions from './../../state/backtesting.actions';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppHelperService } from 'src/app/_services';

@Component({
  selector: 'backtesting-definitions',
  templateUrl: './backtesting-definitions.component.html',
  styleUrls: ['./backtesting-definitions.component.css']
})
export class BacktestingDefinitionsComponent implements OnInit {
  @Input() definitions: BacktestingDefinitions;
  programsType: SelectItem[];
  selectedProgramType: SelectItem;
  defaultMin = 0;
  defaultMax = 16;
  defaultRangeMax = 99.99;
  gamesDefinitions: BtGamesDefinitionRow[];
  bankerBetTypes: SelectItem[];
  bankerRateTypes: SelectItem[];
  bankerBetForTypes: SelectItem[];
  doubleBetTypes: SelectItem[];
  doubleRateTypes: SelectItem[];
  doubleBetForTypes: SelectItem[];
  clonedGamesDefinitions: { [s: number]: BtGamesDefinitionRow; } = {};
  rangeFrom: Date;
  rangeTo: Date;
  minDate: Date;
  maxDate: Date;
  minDateRangeTo : Date;
  
  constructor(private store: Store<fromApp.AppState>,
    private helper: AppHelperService) { }
  onSelectDateFrom($event : Date){    
    this.store.dispatch(new appActions.SetBacktestingFromDate($event));
    this.minDateRangeTo = new Date($event);
    this.minDateRangeTo.setDate( this.minDateRangeTo.getDate() + 1 );
  }
  onSelectDateTo($event : Date){
    this.store.dispatch(new appActions.SetBacktestingToDate($event));
  }
  ngOnInit() {
    this.minDate = new Date("1 JAN 2017");
    this.maxDate = new Date();
    this.rangeFrom = new Date("1 JAN 2017");
    this.onSelectDateFrom(this.rangeFrom);
    this.rangeTo = new Date();
    this.onSelectDateTo(this.rangeTo);
    this.programsType = this.helper.programTypesForUI;
    this.selectedProgramType = this.programsType[0];
    this.store.dispatch(new appActions.SetBacktestingProgramType(+this.selectedProgramType.value));
    this.gamesDefinitions = [];
    this.onAddBankerRow();
    this.bankerBetTypes = [
      //{ label: 'תיקו', value: 'תיקו' },
      { label: 'רגיל', value: 'רגיל' },
    ];
    this.doubleBetTypes = [
      { label: '1X', value: '1X' },
      { label: '12', value: '12' },
      { label: '2X', value: '2X' },
      { label: 'חופשי', value: 'חופשי' }
    ];
    this.bankerRateTypes = [
      { label: 'נמוך', value: 'נמוך' },
      { label: 'גבוה', value: 'גבוה' },
    ];
    this.doubleRateTypes = [...this.bankerRateTypes];
    //this.doubleRateTypes.push({ label: 'אמצע', value: 'אמצע' })
    this.bankerBetForTypes = [
      { label: 'קבוצה בית', value: 'קבוצה בית' },
      { label: 'תיקו', value: 'תיקו' },
      { label: 'קבוצה חוץ', value: 'קבוצה חוץ' },
      { label: 'חופשי', value: 'חופשי' }
    ];
    this.doubleBetForTypes = [
      { label: 'קבוצה בית', value: 'קבוצה בית' },
      { label: 'תיקו', value: 'תיקו' },
      { label: 'קבוצה חוץ', value: 'קבוצה חוץ' },
      { label: 'חופשי', value: 'חופשי' }
    ];
  }
  getCalenderDateRange() : string {
    var range : string = `${this.minDate.getFullYear()}:${this.maxDate.getFullYear()+1}`;
    return range;
  }
  onRowEditInit(row: BtGamesDefinitionRow) {
    this.clonedGamesDefinitions[row.id] = { ...row };
  }

  onRowEditSave(row: BtGamesDefinitionRow) {
    delete this.clonedGamesDefinitions[row.id];
    // if (car.year > 0) {
    //   delete this.clonedCars[car.id];
    //   //this.messageService.add({severity:'success', summary: 'Success', detail:'Car is updated'});
    // }
    // else {
    //   //this.messageService.add({severity:'error', summary: 'Error', detail:'Year is required'});
    // }
  }

  onRowEditCancel(row: BtGamesDefinitionRow, index: number) {
    this.gamesDefinitions[index] = this.clonedGamesDefinitions[row.id];
    delete this.clonedGamesDefinitions[row.id];
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
  }
  onAddBankerRow() {
    var nextRowId = this.gamesDefinitions.length + 1;
    var row: BtGamesDefinitionRow = { id: nextRowId, rowType: BtBetType.Banker, betType: 'רגיל', rateType: 'נמוך', betForType: 'קבוצה בית', rateFrom: 0, rateTo: 99.99, from: 0, to: this.defaultMax, defaultFrom: 0, defaultTo: this.defaultMax, errorFound: false }
    this.gamesDefinitions.push(row);
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
    this.clonedGamesDefinitions[row.id] = { ...row };
  }
  onAddDoubleRow() {
    var nextRowId = this.gamesDefinitions.length + 1;
    var row: BtGamesDefinitionRow = { id: nextRowId, rowType: BtBetType.Double, betType: '1X', rateType: 'נמוך', betForType: 'קבוצה בית', rateFrom: 0, rateTo: 99.99, from: 0, to: this.defaultMax, defaultFrom: 0, defaultTo: this.defaultMax, errorFound: false }
    this.gamesDefinitions.push(row);
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
    this.clonedGamesDefinitions[row.id] = { ...row };
  }
  onRowReorder($event){
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
  }
  onRowDelete(row: BtGamesDefinitionRow, index: number) {
    this.gamesDefinitions.splice(index, 1);
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
  }
  setRowRateRange(index, rowData: BtGamesDefinitionRow, $event: { from: number, to: number }) {
    this.gamesDefinitions[index].rateFrom = $event.from;
    this.gamesDefinitions[index].rateTo = $event.to;
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
  }
  setRowRangeFrom(index, rowData: BtGamesDefinitionRow, $event: number) {
    this.gamesDefinitions[index].from = $event;
    this.gamesDefinitions[index].defaultFrom = $event;
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
  }
  setRowRangeTo(index, rowData: BtGamesDefinitionRow, $event: number) {
    this.gamesDefinitions[index].to = $event;
    this.gamesDefinitions[index].defaultTo = $event;
    this.store.dispatch(new appActions.UpdateGamesDefinition(this.gamesDefinitions));
  }
  onRowRangeError(ri, rowData, $event: boolean) {
    rowData.errorFound = $event;
  }
  onChangeProgramType($event) {
    switch (+this.selectedProgramType.value) {
      case ProgramType.Winner16:
        this.defaultMax = 16;
        break;
      case ProgramType.WinnerWorld:
        this.defaultMax = 15;
        break;
      case ProgramType.WinnerHalf:
        this.defaultMax = 14;
        break;
    }
    this.defaultMin = 0;
    this.store.dispatch(new appActions.SetBacktestingProgramType(+this.selectedProgramType.value));
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Banker, limitType: LimitType.Min, value: 0})); 
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Banker, limitType: LimitType.Max, value: this.defaultMax})); 
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Double, limitType: LimitType.Min, value: 0})); 
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Double, limitType: LimitType.Max, value: this.defaultMax})); 
  }
  bankersMinChanges($event: number) {
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Banker, limitType: LimitType.Min, value: $event})); 
  }
  bankersMaxChanges($event: number) {
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Banker, limitType: LimitType.Max, value: $event})); 
  }
  doublesMinChanges($event: number) {
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Double, limitType: LimitType.Min, value: $event})); 
  }
  doublesMaxChanges($event: number) {
    this.store.dispatch(new appActions.SetBacktestingDefinitionLimits({ betType: BtBetType.Double, limitType: LimitType.Max, value: $event})); 
  }
}
