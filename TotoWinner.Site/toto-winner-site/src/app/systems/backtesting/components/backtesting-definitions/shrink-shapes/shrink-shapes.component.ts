import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ShapesShrinkFieldType, BtShrinkShapesItemPayload, DefaultMinMax } from '../../../backtesting';
import { Store } from '@ngrx/store';
import * as fromApp from './../../../../../state';
import * as appActions from './../../../state/backtesting.actions';
@Component({
  selector: 'backtesting-shrink-shapes',
  templateUrl: './shrink-shapes.component.html',
  styleUrls: ['./shrink-shapes.component.css']
})
export class ShrinkShapesComponent implements OnInit, OnChanges {
  @Input() defaultMin: number;
  @Input() defaultMax: number;

  constructor(private store: Store<fromApp.AppState>) { }
 
  ngOnChanges(changes: SimpleChanges): void {
    if (changes &&
      changes.defaultMax && changes.defaultMax.currentValue) {
        var minMax : DefaultMinMax = { 
          min: changes.defaultMin
            ? changes.defaultMin.currentValue
            : 0, 
          max: changes.defaultMax.currentValue};
        this.store.dispatch(new appActions.InitShapesShrinkCollection(minMax));
    }
  }
  ngOnInit() {
  }
  minChanges($value: number, fieldType: ShapesShrinkFieldType) {
    var item: BtShrinkShapesItemPayload = {
      type: fieldType,
      value: $value
    }
    this.store.dispatch(new appActions.UpdateShapesShrinkFromField(item));
  }
  maxChanges($value: number, fieldType: ShapesShrinkFieldType) {
    var item: BtShrinkShapesItemPayload = {
      type: fieldType,
      value: $value
    }
    this.store.dispatch(new appActions.UpdateShapesShrinkToField(item));
  }

}
