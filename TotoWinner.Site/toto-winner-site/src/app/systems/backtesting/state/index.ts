import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../../state';
import * as fromBacktesting from './backtesting.reducer';

// Extends the app state to include the product feature.
// This is required because products are lazy loaded.
// So the reference to ProductState cannot be added to app.state.ts directly.
// export interface AppState extends fromRoot.AppState {
//     backtesting: fromBacktesting.BacktestingState;
// }

// Selector functions
// const getBacktestingFeatureState = createFeatureSelector<fromBacktesting.BacktestingState>('backtesting');

// export const getProgramType = createSelector(
//     getBacktestingFeatureState,
//     state => state.programType
// );
// export const getBacktestingDefinition = createSelector(
//     getBacktestingFeatureState,
//     state => state.definitions
// );
// export const getBacktestingExecution = createSelector(
//     getBacktestingFeatureState,
//     state => state.execution
// );
