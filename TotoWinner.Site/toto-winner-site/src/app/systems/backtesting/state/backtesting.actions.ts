 import { Action } from '@ngrx/store';
import { StopExecutionResponse, StopExecutionRequest, PollExecutionRequest, RunBacktestingResponse, RunBacktestingRequest, BtBetType, LimitType, BtGamesDefinitionRow, BacktestingExecution, ShowBacktestingPastProgramRequest, BacktestingPastProgramSelected, BtShrinkBasicItemPayload, DefaultMinMax, BtShrinkShapesItemPayload, BtShrinkRatesItemPayload, BtShrinkRatesValidItemPayload, BtShrinkRatesSelectItemPayload } from '../backtesting';
import { ProgramType } from 'src/app/state/app';

 export enum BacktestingActionTypes {
    SetBacktestingProgramType = '[Backtesting] Set Program Type',
    SetBacktestingFromDate = '[Backtesting] Set From Date',    
    SetBacktestingToDate = '[Backtesting] Set To Date',    
    SetBacktestingDefinitionLimits = '[Backtesting] Set Definition Limits',
    RunBacktesting = '[Backtesting] Run',
    RunBacktestingSuccess = '[Backtesting] Run Success',
    RunBacktestingFail = '[Backtesting] Run Fail',
    PollingBacktesting = '[Backtesting] Polling',
    PollingBacktestingSuccess = '[Backtesting] Polling Success',
    PollingBacktestingFail = '[Backtesting] Polling Fail',
    DeactivatePolling = '[Backtesting] Polling Deactivated',
    DeactivatePollingSuccess = '[Backtesting] Polling Deactivated Success',
    DeactivatePollingFail = '[Backtesting] Polling Deactivated Fail',
    ShowBacktestingPastProgram = '[Backtesting] Show Past Program Bets',
    ShowBacktestingPastProgramSuccess = '[Backtesting] Show Past Program Bets Success',
    ShowBacktestingPastProgramFail = '[Backtesting] Show Past Program Bets Fail',
    HideBacktestingPastProgram = '[Backtesting] Hide Past Program Bets',
    UpdateGamesDefinition = '[Backtesting] Update Games Definition',
    InitBasicShrinkCollection = '[Backtesting] Init Basic Shrink Definition Collection',
    UpdateBasicShrinkFromField = '[Backtesting] Update Basic Shrink From Field Definition',
    UpdateBasicShrinkToField = '[Backtesting] Update Basic Shrink To Field Definition',
    InitShapesShrinkCollection = '[Backtesting] Init Shapes Shrink Definition Collection',
    UpdateShapesShrinkFromField = '[Backtesting] Update Shapes Shrink From Field Definition',
    UpdateShapesShrinkToField = '[Backtesting] Update Shapes Shrink To Field Definition',
    InitializeRatesShrinkItems = '[Backtesting] Init Rates Shrink Definition Collection',
    UpdateSelectedRatesShrinkItem = '[Backtesting] Update Rates Shrink Item Selection',
    UpdateRatesShrinkItem = '[Backtesting] Update Rates Shrink Item Definition',
    ValidateRatesShrinkItem = '[Backtesting] Validate Rates Shrink Item',
    ClearState = '[Backtesting] Clear State'
 }
export class SetBacktestingProgramType implements Action {
    readonly type = BacktestingActionTypes.SetBacktestingProgramType;

    constructor(public payload: ProgramType) { }
}
export class SetBacktestingFromDate implements Action {
    readonly type = BacktestingActionTypes.SetBacktestingFromDate;

    constructor(public payload: Date) { }
}
export class SetBacktestingToDate implements Action {
    readonly type = BacktestingActionTypes.SetBacktestingToDate;

    constructor(public payload: Date) { }
}
export class SetBacktestingDefinitionLimits implements Action {
    readonly type = BacktestingActionTypes.SetBacktestingDefinitionLimits;

    constructor(public payload: {betType: BtBetType, limitType: LimitType, value: number}) { }
}
export class RunBacktesting implements Action {
    readonly type = BacktestingActionTypes.RunBacktesting;

    constructor(public payload: RunBacktestingRequest) { }
}
export class RunBacktestingSuccess implements Action {
    readonly type = BacktestingActionTypes.RunBacktestingSuccess;

    constructor(public payload: RunBacktestingResponse) { }
}
export class RunBacktestingFail implements Action {
    readonly type = BacktestingActionTypes.RunBacktestingFail;

    constructor(public payload: string) { }
}
export class PollingBacktesting implements Action {
    readonly type = BacktestingActionTypes.PollingBacktesting;

    constructor(public payload: PollExecutionRequest) { }
}
export class PollingBacktestingSuccess implements Action {
    readonly type = BacktestingActionTypes.PollingBacktestingSuccess;

    constructor(public payload: BacktestingExecution) { }
}
export class PollingBacktestingFail implements Action {
    readonly type = BacktestingActionTypes.PollingBacktestingFail;

    constructor(public payload: string) { }
}
export class DeactivatePolling implements Action {
    readonly type = BacktestingActionTypes.DeactivatePolling;

    constructor(public payload: StopExecutionRequest) { }
}
export class DeactivatePollingSuccess implements Action {
    readonly type = BacktestingActionTypes.DeactivatePollingSuccess;

    constructor(public payload: StopExecutionResponse) { }
}
export class DeactivatePollingFail implements Action {
    readonly type = BacktestingActionTypes.DeactivatePollingFail;

    constructor(public payload: string) { }
}
export class ShowBacktestingPastProgram implements Action {
    readonly type = BacktestingActionTypes.ShowBacktestingPastProgram;

    constructor(public payload: ShowBacktestingPastProgramRequest) { }
}
export class ShowBacktestingPastProgramSuccess implements Action {
    readonly type = BacktestingActionTypes.ShowBacktestingPastProgramSuccess;

    constructor(public payload: BacktestingPastProgramSelected) { }
}
export class ShowBacktestingPastProgramFail implements Action {
    readonly type = BacktestingActionTypes.ShowBacktestingPastProgramFail;

    constructor(public payload: string) { }
}
export class HideBacktestingPastProgram implements Action {
    readonly type = BacktestingActionTypes.HideBacktestingPastProgram;

    constructor() { }
}
export class UpdateGamesDefinition implements Action {
    readonly type = BacktestingActionTypes.UpdateGamesDefinition;

    constructor(public payload: BtGamesDefinitionRow[]) { }
}
export class UpdateBasicShrinkFromField implements Action {
    readonly type = BacktestingActionTypes.UpdateBasicShrinkFromField;

    constructor(public payload: BtShrinkBasicItemPayload) { }
}
export class UpdateBasicShrinkToField implements Action {
    readonly type = BacktestingActionTypes.UpdateBasicShrinkToField;

    constructor(public payload: BtShrinkBasicItemPayload) { }
}
export class InitBasicShrinkCollection implements Action {
    readonly type = BacktestingActionTypes.InitBasicShrinkCollection;

    constructor(public payload: DefaultMinMax) { }
}
export class UpdateShapesShrinkFromField implements Action {
    readonly type = BacktestingActionTypes.UpdateShapesShrinkFromField;

    constructor(public payload: BtShrinkShapesItemPayload) { }
}
export class UpdateShapesShrinkToField implements Action {
    readonly type = BacktestingActionTypes.UpdateShapesShrinkToField;

    constructor(public payload: BtShrinkShapesItemPayload) { }
}
export class InitShapesShrinkCollection implements Action {
    readonly type = BacktestingActionTypes.InitShapesShrinkCollection;

    constructor(public payload: DefaultMinMax) { }
}
export class InitializeRatesShrinkItems implements Action {
    readonly type = BacktestingActionTypes.InitializeRatesShrinkItems;

    constructor(public payload: DefaultMinMax) { }
}
export class UpdateSelectedRatesShrinkItem implements Action {
    readonly type = BacktestingActionTypes.UpdateSelectedRatesShrinkItem;

    constructor(public payload: BtShrinkRatesSelectItemPayload) { }
}
export class UpdateRatesShrinkItem implements Action {
    readonly type = BacktestingActionTypes.UpdateRatesShrinkItem;

    constructor(public payload: BtShrinkRatesItemPayload) { }
}
export class ValidateRatesShrinkItem implements Action {
    readonly type = BacktestingActionTypes.ValidateRatesShrinkItem;

    constructor(public payload: BtShrinkRatesValidItemPayload) { }
}
export class ClearState implements Action {
    readonly type = BacktestingActionTypes.ClearState;

    constructor() { }
}

export type BacktestingActions = 
| SetBacktestingProgramType
| SetBacktestingFromDate
| SetBacktestingToDate
| SetBacktestingDefinitionLimits
| RunBacktesting
| RunBacktestingSuccess
| RunBacktestingFail
| PollingBacktesting
| PollingBacktestingSuccess
| PollingBacktestingFail
| DeactivatePolling
| DeactivatePollingSuccess
| DeactivatePollingFail
| ShowBacktestingPastProgram
| ShowBacktestingPastProgramSuccess
| ShowBacktestingPastProgramFail
| HideBacktestingPastProgram
| UpdateGamesDefinition
| InitBasicShrinkCollection
| UpdateBasicShrinkFromField
| UpdateBasicShrinkToField
| InitShapesShrinkCollection
| UpdateShapesShrinkFromField
| UpdateShapesShrinkToField
| InitializeRatesShrinkItems
| UpdateSelectedRatesShrinkItem
| UpdateRatesShrinkItem
| ValidateRatesShrinkItem
| ClearState
;