import { BacktestingDefinitions, BacktestingExecution, BacktestingPastProgramSelected, BtBetType, LimitType, BtGamesDefinitionRow, BtShrinkBasicItem, BasicShrinkFieldType, BtShrinkShapesItem, ShapesShrinkFieldType, BtShrinkRatesItem, RatesShrinkFieldType } from '../backtesting';
import { BacktestingActions, BacktestingActionTypes } from './backtesting.actions';
import { CalculationStatus } from 'src/app/model/interfaces/shared.module';

export interface BacktestingState {
    definitions: BacktestingDefinitions;
    execution: BacktestingExecution | null;
    pastProgramSelected: BacktestingPastProgramSelected | null;
    error: string;
}
export const initialShrinkBasicDefinitionState: BtShrinkBasicItem[] = [
    { type: BasicShrinkFieldType.Amount1X2_1, from: 0, to: 16 },
    { type: BasicShrinkFieldType.Amount1X2_X, from: 0, to: 16 },
    { type: BasicShrinkFieldType.Amount1X2_2, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountABC_A, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountABC_B, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountABC_C, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountSeq1X2_1, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountSeq1X2_X, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountSeq1X2_2, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountSeqABC_A, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountSeqABC_B, from: 0, to: 16 },
    { type: BasicShrinkFieldType.AmountSeqABC_C, from: 0, to: 16 },
    { type: BasicShrinkFieldType.Break1X2, from: 0, to: 15 },
    { type: BasicShrinkFieldType.BreakABC, from: 0, to: 15 }
];
export const initialShrinkShapesDefinitionState: BtShrinkShapesItem[] = [
    { type: ShapesShrinkFieldType.Shapes2_1X2_0, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_1X2_1, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_1X2_2, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_1X2_3Plus, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_ABC_0, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_ABC_1, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_ABC_2, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes2_ABC_3Plus, from: 0, to: 9 },
    { type: ShapesShrinkFieldType.Shapes3_1X2_0, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_1X2_1, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_1X2_2, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_1X2_3Plus, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_ABC_0, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_ABC_1, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_ABC_2, from: 0, to: 27 },
    { type: ShapesShrinkFieldType.Shapes3_ABC_3Plus, from: 0, to: 27 }
];
export const initialShrinkRatesDefinitionState: BtShrinkRatesItem[] = [
    { type: RatesShrinkFieldType.Minimum, from: 0, to: 99, valid: true, selected: false },
    { type: RatesShrinkFieldType.Middle, from: 0, to: 99, valid: true, selected: false },
    { type: RatesShrinkFieldType.Maximum, from: 0, to: 99, valid: true, selected: false },
    { type: RatesShrinkFieldType.Home, from: 0, to: 99, valid: true, selected: false },
    { type: RatesShrinkFieldType.Draft, from: 0, to: 99, valid: true, selected: false },
    { type: RatesShrinkFieldType.Away, from: 0, to: 99, valid: true, selected: false }
];

export const initialState: BacktestingState = {
    definitions: {
        programType: null,
        fromDate: null,
        toDate: null,
        bankersLimits: { min: 0, max: 16 },
        doublesLimits: { min: 0, max: 16 },
        gamesDefinitions: [],
        shrinkDefinitions: {
            basic: {
                collection: initialShrinkBasicDefinitionState
            },
            shapes: {
                collection: initialShrinkShapesDefinitionState
            },
            rates: {
                collection: initialShrinkRatesDefinitionState
            }
        }
    },
    execution: null,
    pastProgramSelected: null,
    error: ''
};
export function backtestingReducer(state = initialState, action: BacktestingActions): BacktestingState {
    switch (action.type) {
        case BacktestingActionTypes.SetBacktestingProgramType:
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    programType: action.payload
                }
            };
        case BacktestingActionTypes.SetBacktestingFromDate:
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    fromDate: action.payload
                }
            };
        case BacktestingActionTypes.SetBacktestingToDate:
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    toDate: action.payload
                }
            };
        case BacktestingActionTypes.SetBacktestingDefinitionLimits:
            var newDef: BacktestingDefinitions = { ...state.definitions }
            var newLimitNode: { min: number, max: number };
            if (action.payload.betType == BtBetType.Banker)
                newLimitNode = newDef.bankersLimits;
            else
                newLimitNode = newDef.doublesLimits;
            if (action.payload.limitType == LimitType.Min)
                newLimitNode.min = action.payload.value;
            else
                newLimitNode.max = action.payload.value;
            return {
                ...state,
                definitions: newDef
            };
        case BacktestingActionTypes.RunBacktestingSuccess:
            return {
                ...state,
                execution: {
                    executionId: action.payload.executionId,
                    status: action.payload.status,
                    results: null,
                    summary: null,
                    totals: null,
                    isSuccess: null
                },
                pastProgramSelected: null,
                error: ''
            };
        case BacktestingActionTypes.RunBacktestingFail:
            return {
                ...state,
                definitions: state.definitions,
                execution: null,
                pastProgramSelected: null,
                error: action.payload
            };
        case BacktestingActionTypes.PollingBacktestingSuccess:
            var errorMsg = '';
            if (!action.payload.isSuccess)
                errorMsg = "אירעה תקלה לא צפויה בהצגת נתוני ריצה";
            return {
                ...state,
                execution: action.payload,
                pastProgramSelected: null,
                error: errorMsg
            };
        case BacktestingActionTypes.PollingBacktesting:
            return {
                ...state,
                execution: {
                    executionId: action.payload.executionId,
                    status: state.execution.status,
                    results: null,
                    summary: null,
                    totals: null,
                    isSuccess: null
                },
                pastProgramSelected: null,
                error: ''
            };
        case BacktestingActionTypes.DeactivatePolling:
            return {
                ...state,
                execution: {
                    ...state.execution,
                    executionId: action.payload.executionId,
                },
                pastProgramSelected: null,
                error: ''
            };
        case BacktestingActionTypes.DeactivatePollingSuccess:
            return {
                ...state,
                execution: {
                    ...state.execution,
                    executionId: state.execution.executionId,
                    status: action.payload.status
                },
                pastProgramSelected: null,
                error: ''
            };
        case BacktestingActionTypes.ShowBacktestingPastProgram:
            return {
                ...state,
                pastProgramSelected: {
                    programId: action.payload.programId,
                    programNumber: action.payload.programNumber,
                    baseBetData: null, winBetData: null
                }
            }
        case BacktestingActionTypes.ShowBacktestingPastProgramSuccess:
            return {
                ...state,
                pastProgramSelected: action.payload
            }
        case BacktestingActionTypes.HideBacktestingPastProgram:
            return {
                ...state,
                pastProgramSelected: null
            }
        case BacktestingActionTypes.UpdateGamesDefinition:
            var newArr = action.payload.slice();
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    gamesDefinitions: newArr
                }
            }
        case BacktestingActionTypes.InitBasicShrinkCollection:
            var initializedBasic: BtShrinkBasicItem[] = [];
            initialShrinkBasicDefinitionState.forEach(element => {
                var newItem: BtShrinkBasicItem = {
                    type: element.type,
                    from: element.from,
                    to: element.to
                };
                initializedBasic.push(newItem);
            });
            initializedBasic[BasicShrinkFieldType.Break1X2].to = action.payload.max - 1;
            initializedBasic[BasicShrinkFieldType.BreakABC].to = action.payload.max - 1;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        basic: {
                            collection: initializedBasic
                        }
                    }
                }
            }
        case BacktestingActionTypes.UpdateBasicShrinkFromField:
            var newColl = state.definitions.shrinkDefinitions.basic.collection.slice();
            var item = newColl.find(i => i.type == action.payload.type);
            item.from = action.payload.value;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        basic: {
                            collection: newColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.UpdateBasicShrinkToField:
            var newColl = state.definitions.shrinkDefinitions.basic.collection.slice();
            var item = newColl.find(i => i.type == action.payload.type);
            item.to = action.payload.value;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        basic: {
                            collection: newColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.InitShapesShrinkCollection:
            var initializedShapes: BtShrinkShapesItem[] = [];
            initialShrinkShapesDefinitionState.forEach(element => {
                var newItem: BtShrinkShapesItem = {
                    type: element.type,
                    from: element.from,
                    to: element.to
                };
                initializedShapes.push(newItem);
            });
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        shapes: {
                            collection: initializedShapes
                        }
                    }
                }
            }
        case BacktestingActionTypes.UpdateShapesShrinkFromField:
            var newShapesColl = state.definitions.shrinkDefinitions.shapes.collection.slice();
            var shapesItem = newShapesColl.find(i => i.type == action.payload.type);
            shapesItem.from = action.payload.value;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        shapes: {
                            collection: newShapesColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.UpdateShapesShrinkToField:
            var newShapesColl = state.definitions.shrinkDefinitions.shapes.collection.slice();
            var shapesItem = newShapesColl.find(i => i.type == action.payload.type);
            shapesItem.to = action.payload.value;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        shapes: {
                            collection: newShapesColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.InitializeRatesShrinkItems:
            var initializedRates: BtShrinkRatesItem[] = [];
            initialShrinkRatesDefinitionState.forEach(element => {
                var newItem: BtShrinkRatesItem = {
                    type: element.type,
                    from: action.payload.min,
                    to: action.payload.max,
                    valid: element.valid,
                    selected: element.selected
                };
                initializedRates.push(newItem);
            });
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        rates: {
                            collection: initializedRates
                        }
                    }
                }
            }
        case BacktestingActionTypes.UpdateSelectedRatesShrinkItem:
            var rateSelectionColl = state.definitions.shrinkDefinitions.rates.collection.slice();
            var rateSelectionItem = rateSelectionColl.find(i => i.type == action.payload.type);
            rateSelectionItem.selected = action.payload.value;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        rates: {
                            collection: rateSelectionColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.UpdateRatesShrinkItem:
            var ratesColl = state.definitions.shrinkDefinitions.rates.collection.slice();
            var ratesItem = ratesColl.find(i => i.type == action.payload.type);
            ratesItem.from = action.payload.from;
            ratesItem.to = action.payload.to;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        rates: {
                            collection: ratesColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.ValidateRatesShrinkItem:
            var ratesValidColl = state.definitions.shrinkDefinitions.rates.collection.slice();
            var ratesValidItem = ratesValidColl.find(i => i.type == action.payload.type);
            ratesValidItem.valid = action.payload.value;
            return {
                ...state,
                definitions: {
                    ...state.definitions,
                    shrinkDefinitions: {
                        ...state.definitions.shrinkDefinitions,
                        rates: {
                            collection: ratesValidColl
                        }
                    }
                }
            }
        case BacktestingActionTypes.ClearState:
            var initializedBasic: BtShrinkBasicItem[] = [];
            initialShrinkBasicDefinitionState.forEach(element => {
                var newItem: BtShrinkBasicItem = {
                    type: element.type,
                    from: element.from,
                    to: element.to
                };
                initializedBasic.push(newItem);
            });
            var initializedShapes: BtShrinkShapesItem[] = [];
            initialShrinkShapesDefinitionState.forEach(element => {
                var newItem: BtShrinkShapesItem = {
                    type: element.type,
                    from: element.from,
                    to: element.to
                };
                initializedShapes.push(newItem);
            });
            var initializedRates: BtShrinkRatesItem[] = [];
            initialShrinkRatesDefinitionState.forEach(element => {
                var newItem: BtShrinkRatesItem = {
                    type: element.type,
                    from: element.from,
                    to: element.to,
                    valid: element.valid,
                    selected: element.selected
                };
                initializedRates.push(newItem);
            });
            return {
                ...state,
                definitions: {
                    programType: null,
                    fromDate: null,
                    toDate: null,
                    bankersLimits: { min: 0, max: 16 },
                    doublesLimits: { min: 0, max: 16 },
                    gamesDefinitions: [],
                    shrinkDefinitions: {
                        basic: {
                            collection: initializedBasic
                        },
                        shapes: {
                            collection: initializedShapes
                        },
                        rates: {
                            collection: initializedRates
                        }
                    }
                },
                error: initialState.error,
                pastProgramSelected: initialState.pastProgramSelected,
                execution: {
                    results: null,
                    executionId: state.execution ? state.execution.executionId : null,
                    isSuccess: true,
                    status: CalculationStatus.None,
                    summary: null,
                    totals: null
                }
            }

        default:
            return state;
    }
}
function insertItem(array, action) {
    return [
        ...array.slice(0, action.index),
        action.item,
        ...array.slice(action.index)
    ]
}
function removeItem(array, action) {
    return [...array.slice(0, action.index), ...array.slice(action.index + 1)]
}
/*function insertItem(array, action) {
    let newArray = array.slice()
    newArray.splice(action.index, 0, action.item)
    return newArray
}

function removeItem(array, action) {
    let newArray = array.slice()
    newArray.splice(action.index, 1)
    return newArray
}
function removeItem(array, action) {
    return array.filter((item, index) => index !== action.index)
}
function updateObjectInArray(array, action) {
    return array.map((item, index) => {
        if (index !== action.index) {
            // This isn't the item we care about - keep it as-is
            return item
        }

        // Otherwise, this is the one we want - return an updated value
        return {
            ...item,
            ...action.item
        }
    })
}
*/
