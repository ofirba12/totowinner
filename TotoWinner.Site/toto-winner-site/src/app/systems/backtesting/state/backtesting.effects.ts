import { Injectable, OnInit } from '@angular/core';
import { Observable, of, timer } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
//import * as backtestingActions from './backtesting.actions';
import * as appActions from './../../backtesting/state/backtesting.actions';
import { RunBacktestingRequest, StopExecutionRequest, ShowBacktestingPastProgramRequest } from '../backtesting';
import { BacktestingService } from 'src/app/_services/backtesting.service';

@Injectable()
export class BacktestingEffects {
    constructor(private backtestingService: BacktestingService,
        private actions$: Actions) { }

    @Effect()
    runBacktesting$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.BacktestingActionTypes.RunBacktesting),
        map((action: appActions.RunBacktesting) => action.payload),
        mergeMap((runRequest: RunBacktestingRequest) =>
            this.backtestingService.runBacktesting(runRequest).pipe(
                map(runResponse => (new appActions.RunBacktestingSuccess(runResponse))),
                catchError(err => of(new appActions.RunBacktestingFail(err)))
            )
        )
    );

    @Effect()
    pollingBacktestingDeactivated$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.BacktestingActionTypes.DeactivatePolling),
        map((action: appActions.DeactivatePolling) => action.payload),
        mergeMap((stopRequest: StopExecutionRequest) =>
            this.backtestingService.stopBacktesting(stopRequest).pipe(
                map(stopResponse => (new appActions.DeactivatePollingSuccess(stopResponse))),
                catchError(err => of(new appActions.DeactivatePollingFail(err)))
            )
        )
    );
    @Effect()
    showBacktestingPastProgram$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.BacktestingActionTypes.ShowBacktestingPastProgram),
        map((action: appActions.ShowBacktestingPastProgram) => action.payload),
        mergeMap((request: ShowBacktestingPastProgramRequest) =>
            this.backtestingService.getPastProgram(request).pipe(
                map(response => (new appActions.ShowBacktestingPastProgramSuccess(response))),
                catchError(err => of(new appActions.ShowBacktestingPastProgramFail(err)))
            )
        )
    );
}

