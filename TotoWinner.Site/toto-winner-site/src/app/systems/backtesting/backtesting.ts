import { CalculationStatus } from '../../model/interfaces/shared.module';
import { SelectItem } from 'primeng/api';
import { ProgramType } from '../../state/app';
import { FromToItem } from 'src/app/model/interfaces/optimizer.module';

export enum LimitType {
    Min = 1,
    Max = 2
}
export enum BasicShrinkFieldType{
    Amount1X2_1,
    Amount1X2_X,
    Amount1X2_2,
    AmountABC_A,
    AmountABC_B,
    AmountABC_C,
    AmountSeq1X2_1,
    AmountSeq1X2_X,
    AmountSeq1X2_2,
    AmountSeqABC_A,
    AmountSeqABC_B,
    AmountSeqABC_C,
    Break1X2,
    BreakABC
}
export enum ShapesShrinkFieldType{
    Shapes2_1X2_0,
    Shapes2_1X2_1,
    Shapes2_1X2_2,
    Shapes2_1X2_3Plus,
    Shapes2_ABC_0,
    Shapes2_ABC_1,
    Shapes2_ABC_2,
    Shapes2_ABC_3Plus,
    Shapes3_1X2_0,
    Shapes3_1X2_1,
    Shapes3_1X2_2,
    Shapes3_1X2_3Plus,
    Shapes3_ABC_0,
    Shapes3_ABC_1,
    Shapes3_ABC_2,
    Shapes3_ABC_3Plus
}
export enum RatesShrinkFieldType{
    Minimum,
    Middle,
    Maximum,
    Home,
    Draft,
    Away
}
export interface DefaultMinMax {
    min: number;
    max: number;
}

export interface BacktestingDefinitions {
    programType: ProgramType
    fromDate: Date;
    toDate: Date;
    bankersLimits: { min: number, max: number }
    doublesLimits: { min: number, max: number }
    gamesDefinitions: BtGamesDefinitionRow[];
    shrinkDefinitions: BtShrinkDefinition;
}
export interface BtShrinkDefinition{
    basic : BtShrinkBasicDefinition;
    shapes : BtShrinkShapesDefinition;
    rates : BtShrinkRatesDefinition;
}
export interface BtShrinkBasicDefinition{
    collection : BtShrinkBasicItem[];
}

export interface BtShrinkBasicItem{
    type: BasicShrinkFieldType;
    from: number;
    to: number;
}
export interface BtShrinkBasicItemPayload{
    type: BasicShrinkFieldType;
    value: number;
}
export interface BtShrinkShapesDefinition{
    collection : BtShrinkShapesItem[];
}
export interface BtShrinkShapesItem{
    type: ShapesShrinkFieldType;
    from: number;
    to: number;
}
export interface BtShrinkShapesItemPayload{
    type: ShapesShrinkFieldType;
    value: number;
}
export interface BtShrinkRatesDefinition{
    collection : BtShrinkRatesItem[];
}
export interface BtShrinkRatesItem{
    type: RatesShrinkFieldType;
    from: number;
    to: number;
    valid: boolean;
    selected: boolean;
}

export interface BtShrinkRatesSelectItemPayload{
    type: RatesShrinkFieldType;
    value: boolean;
}
export interface BtShrinkRatesItemPayload{
    type: RatesShrinkFieldType;
    from: number;
    to: number;
}
export interface BtShrinkRatesValidItemPayload{
    type: RatesShrinkFieldType;
    value: boolean;
}
export enum BtBetType {
    Banker = 1,
    Double = 2
}
export interface BtGamesDefinitionRow {
    id: number;
    rowType: BtBetType;
    betType: string;
    rateType: string;
    rateFrom: number;
    rateTo: number;
    betForType: string;
    from: number;
    to: number;
    defaultFrom: number;
    defaultTo: number;
    errorFound: boolean;
}
export interface BtSummaryRowData {
    programsCounter: number;
    notValidProgramCounter: number;
    notExistsProgramCounter: number;
}
export interface BtTotalRowData {
    bets: number;
    results: number[];
}
export interface BtRowData {
    triplets: number;
    doubles: number;
    bankers: number;
    programDescription: string;
    programNumber: number;
    programPersistanceId : number;
    programEndDate: Date;
    bets: number;
    results: number[];
}
export interface BtProgramBetData {
    rates: number[];
    selection: boolean[];
}
export interface BacktestingPastProgramSelected {
    programId: number;
    programNumber: number;
    baseBetData: BtProgramBetData[];
    winBetData: BtProgramBetData[];
}
export interface ShowBacktestingPastProgramRequest{
    executionId: number;
    programId: number;    
    programNumber: number;
}
export interface RunBacktestingRequest {
    definitions: BacktestingDefinitions;
}
export interface RunBacktestingResponse {
    executionId: number;
    status: CalculationStatus;
}
export interface BacktestingExecution {
    executionId: number;
    status: CalculationStatus;
    results: BtRowData[];
    totals: BtTotalRowData;
    summary: BtSummaryRowData;
    isSuccess: boolean;
}
export interface BacktestingExecutionRequest {
    executionId: number;
}
export interface PollExecutionRequest {
    executionId: number;
    fromIndex: number;
}
export interface StopExecutionRequest {
    executionId: number;
}
export interface StopExecutionResponse extends BaseResponse {
    status: CalculationStatus;
}
export interface BaseResponse {
    isSuccess: boolean;
    errorMessage: string;
}
