import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BacktestingShellComponent } from './containers/backtesting-shell/backtesting-shell.component';

const routes: Routes = [
  {
    path:'',
    component: BacktestingShellComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BacktestingRoutingModule { }
