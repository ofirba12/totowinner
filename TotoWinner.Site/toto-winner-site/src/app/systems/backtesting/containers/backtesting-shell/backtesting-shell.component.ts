import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, of, BehaviorSubject, Subscription } from 'rxjs';
import { delay, tap, skip, concatMap, concat, takeUntil } from 'rxjs/operators';

import * as fromBacktesting from './../../state';
import * as appActions from './../../state/backtesting.actions';
import { RunBacktestingRequest, BacktestingDefinitions, BacktestingExecution, StopExecutionRequest, PollExecutionRequest, BacktestingPastProgramSelected } from '../../backtesting';
import { BacktestingService } from 'src/app/_services/backtesting.service';
import * as fromApp from './../../../../state';
import { ProgramState, ProgramType } from 'src/app/state/app';
@Component({
  selector: 'app-backtesting-shell',
  templateUrl: './backtesting-shell.component.html',
  styleUrls: ['./backtesting-shell.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BacktestingShellComponent implements OnInit, OnDestroy {
  definitions$: Observable<BacktestingDefinitions>;
  execution$: Observable<BacktestingExecution>;
  pastProgramSelected$: Observable<BacktestingPastProgramSelected>;
  programsRepository$: Observable<ProgramState[]>;
  programType$: Observable<ProgramType>;
  whenToRefresh$: Observable<any>;
  polling$: BehaviorSubject<any>;
  pollBacktesting$: Observable<BacktestingExecution>;
  pollExecutionObserver$: Observable<any>;
  subscription$: Subscription;
  constructor(private store: Store<fromApp.AppState>,
    private backtestingService: BacktestingService) { }

  ngOnInit() {
    this.definitions$ = this.store.pipe(select(fromApp.getBacktestingDefinition));
    this.execution$ = this.store.pipe(select(fromApp.getBacktestingExecution));
    this.pastProgramSelected$ = this.store.pipe(select(fromApp.getBacktestingPastProgramSelected));
    this.programsRepository$ = this.store.pipe(select(fromApp.getProgramsRepository));
    this.programType$ = this.store.pipe(select(fromApp.getBacktestingProgramType));
  }

  run(request: RunBacktestingRequest): void {
    this.store.dispatch(new appActions.RunBacktesting(request));
  }
  poll(request: PollExecutionRequest): void {
    this.store.dispatch(new appActions.PollingBacktesting(request));
    this.polling$ = new BehaviorSubject('');
    this.whenToRefresh$ = of('').pipe(
      delay(5000),
      tap(_ => {
        if (!this.polling$.isStopped) {
          this.polling$.next('')
        }
      }),
      skip(1)
    );
    this.pollBacktesting$ = this.backtestingService.pollBacktesting(request);
    this.pollExecutionObserver$ = this.polling$.pipe(
      //      concatMap(_ => this.pollBacktesting$.pipe(concat(this.whenToRefresh$))),
      concatMap(_ => this.dynamicPollFunction(request).pipe(concat(this.whenToRefresh$))),
    );
    this.subscription$ = this.pollExecutionObserver$.subscribe(res => {
      this.store.dispatch(new appActions.PollingBacktestingSuccess(res))
    });
  }
  dynamicPollFunction(request: PollExecutionRequest): Observable<BacktestingExecution> {
    request.fromIndex = this.backtestingService.rowsExecutionData.length;
    var $observ = this.backtestingService.pollBacktesting(request);
    return $observ;
  }
  deactivatePoll(request: StopExecutionRequest) {
    if (this.subscription$)
      this.subscription$.unsubscribe();

    this.store.dispatch(new appActions.DeactivatePolling(request));
  }
  deactivatePollWithNoEffect(){
    if (this.subscription$)
      this.subscription$.unsubscribe();
  }
  ngOnDestroy() {
    if (this.subscription$)
      this.subscription$.unsubscribe();
  }
}
