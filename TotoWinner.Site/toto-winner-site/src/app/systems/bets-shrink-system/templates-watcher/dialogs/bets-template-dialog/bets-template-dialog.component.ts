import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromApp from '../../../../../state';
import * as appActions from '../../../../../state/app.actions';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { TemplateWatcherDialogData, Bet } from 'src/app/model/interfaces/templates.module';
import { SelectItem, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'bets-template-dialog',
  templateUrl: './bets-template-dialog.component.html',
  styleUrls: ['./bets-template-dialog.component.css']
})
export class BetsTemplateDialogComponent implements OnInit, OnDestroy {
  data$: Observable<TemplateWatcherDialogData>;
  dataSubscription$: Subscription;
  uiBaseBetData: SelectItem[][] = [];
  show: boolean = false;
  title: string = "TBD";
  constructor(private store: Store<fromApp.AppState>,
    public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  createBetsItems(rates: SelectItem[], bet: Bet): SelectItem[] {
    var chosenStyle = "wn-template-basebet-selected";
    let rateArr: SelectItem[] = [
      { label: `${rates[0].label}`, value: 1, disabled: true, styleClass: bet.bet1 ? chosenStyle : "" },
      { label: `${rates[1].label}`, value: 2, disabled: true, styleClass: bet.betX ? chosenStyle : "" },
      { label: `${rates[2].label}`, value: 3, disabled: true, styleClass: bet.bet2 ? chosenStyle : "" }];
    return rateArr;
  }
  ngOnInit() {
    var self = this;
    var templateData = this.config.data.templateData;
    this.data$ = this.store.pipe(select(fromApp.getTemplateWatcherDialogData));
    this.dataSubscription$ = this.data$
      .subscribe(data => {
        if (data && data.templateData) {
          var index = 0;
          for (let row of data.rates) {
            self.uiBaseBetData.push(self.createBetsItems(row, data.templateData.bets[index]));
            index++;
          }
        }
      });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription$)
      this.dataSubscription$.unsubscribe();
  }
}
