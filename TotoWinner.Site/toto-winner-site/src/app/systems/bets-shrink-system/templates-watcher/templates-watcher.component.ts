import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { SelectItem, DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../state';
import * as appActions from '../../../state/app.actions';
import { ProgramType } from 'src/app/state/app';
import { ProgramDetails } from 'src/app/model/interfaces/program.module';
import { WatchTemplatesRequest, RatesInfo, TemplateWatcherParameters, TemplateWatcherRepository, Template, TemplateWatcher, Bet, TemplatesBaseBetsResponse, TemplateBaseBetsStatus, TemplateWatcherState } from 'src/app/model/interfaces/templates.module';
import { element } from 'protractor';
import { AuthenticationService } from 'src/app/_services';
import { BetsTemplateDialogComponent } from './dialogs/bets-template-dialog/bets-template-dialog.component';

@Component({
  selector: 'templates-watcher',
  templateUrl: './templates-watcher.component.html',
  styleUrls: ['./templates-watcher.component.css']
})
export class TemplatesWatcherComponent implements OnInit, OnDestroy {
  // params$: Observable<TemplateWatcherParameters>;
  // paramsSubscription$: Subscription;
  data$: Observable<TemplateWatcherState>;
  dataSubscription$: Subscription;
  templateWatcherParameters: TemplateWatcherParameters;
  watchData: TemplateWatcher[] = [];
  constructor(private store: Store<fromApp.AppState>,
    public dialogService: DialogService,
    public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.templateWatcherParameters = config.data.watcherParameters
  }

  isCollectionValid(watcherState: { [id: number]: TemplatesBaseBetsResponse; }): boolean {
    var length = Object.keys(watcherState).length;
    return length > 0;
  }
  // getCollection(watcherState: { [id: number]: TemplatesBaseBetsResponse; }) {
  //   this.prepareCollection(watcherState);
  //   return this.watchData;
  // }
  prepareCollection(watcherState: { [id: number]: TemplatesBaseBetsResponse; }) {
    var self = this;
    Object.entries(watcherState).forEach(function (item) {
      var key = +item[0];
      var watcherBets: Bet[] = item[1].baseBets
        ? Object.values(item[1].baseBets)
        : [];
      var betsStatus = item[1].status
      //Object.assign<any, Bet[]>(Object.entries(item[1]), watcherBets);
      //var elm = Object.values<Template>(self.stateTemplates).find(t => t.id === key);
      var elm = Object.values<Template>(self.templateWatcherParameters.templates).find(t => t.id === key);
      if (elm) {
        var style = {};
        switch (betsStatus) {
          case TemplateBaseBetsStatus.InvalidTemplate:
            style = { 'background-color': 'orange' };
            break;
          case TemplateBaseBetsStatus.NoBaseFound:
            style = { 'background-color': 'red' };
            break;
          default:
            style = { 'background-color': 'limegreen' };
            break;
        }
        self.watchData.push({ id: elm.id, value: elm.value, name: elm.name, bets: watcherBets, status: betsStatus, statusStyle: style })
      }
    });
    self.watchData.sort((one, two) => {
      if (one.name == null && two.name == null)
        return (one.value > two.value ? 1 : -1);
      if (one.name == null && two.name != null)
        return -1;
      if (one.name != null && two.name == null)
        return 1;
      return (one.name > two.name ? 1 : -1)
    });// sort asc
    // Object.keys(collection).forEach(element => {
    //   var elm = Object.values<Template>(this.stateTemplates).find(t => t.id === +element);
    //   watchData.push({id: elm.id, value: elm.value, name: elm.name, bets: null})
    // });
  }
  showBets(rowData: TemplateWatcher) {
    this.store.dispatch(new appActions.ShowTemplateBetsDialog(rowData));
    const ref = this.dialogService.open(BetsTemplateDialogComponent, {
      data: {
        templateData: rowData
      },
      header: `${rowData.name} ${rowData.value}`,
      width: '16em',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .save-system-header",
      contentStyle: { "font-size": "11px" }
    });
  }
  ngOnInit() {
    var self = this;
    this.data$ = this.store.pipe(select(fromApp.getTemplateWatcherData));
    this.dataSubscription$ = this.data$
      .subscribe(data => {
        if (data && self.isCollectionValid(data.collection)) {
          self.prepareCollection(data.collection)
        }
      });
  }
  ngOnDestroy(): void {
    // if (this.paramsSubscription$)
    //   this.paramsSubscription$.unsubscribe();
    if (this.dataSubscription$)
      this.dataSubscription$.unsubscribe();
  }
}
