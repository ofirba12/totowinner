import { Component, Input, OnChanges, SimpleChange, ViewEncapsulation, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { SelectItem, MessageService } from 'primeng/api';
import { TotoGamesService, AppConnetorService, ProgramsService } from '../../../../_services';
import { ProgramData } from '../../../../model/interfaces/program.module';
import { ProgramDetails, BetProgramDetail } from 'src/app/model/interfaces/program.module';
import { SystemStateStatus, SystemType } from 'src/app/model/interfaces/shared.module';
import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../../state';
import * as appActions from '../../../../state/app.actions';
import { ToolbarState, ProgramType, ShrinkState, StateMode, NotificationType } from 'src/app/state/app';
import { TemplateWatcherParameters, RatesInfo, WatchTemplatesRequest } from 'src/app/model/interfaces/templates.module';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GamesComponent implements OnInit, OnDestroy {
  @Input() SelectedProgram: any;
  selectedBets: string[][];
  programTitle: string;
  programData: ProgramData;
  gameStatusStyle: string;
  isValidForCalc: boolean;
  isProgramSelectionDirty: boolean = false;
  loading: boolean;
  selectedProgram$: Observable<SelectItem>;
  selectedProgramSubscription$: Subscription;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;
  currentProgramType: ProgramType;
  state: ShrinkState;
  params$: Observable<TemplateWatcherParameters>;
  paramsSubscription$: Subscription;
  templateWatcherParameters: TemplateWatcherParameters;
  constructor(private programsService: ProgramsService,
    private gatewayService: AppConnetorService,
    private store: Store<fromApp.AppState>) { }
  ngOnDestroy(): void {
    if (this.selectedProgramSubscription$)
      this.selectedProgramSubscription$.unsubscribe();
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
    if (this.paramsSubscription$)
      this.paramsSubscription$.unsubscribe();
  }
  ngOnInit(): void {
    var self = this;
    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        self.state = state;
        self.currentProgramType = state.toolbar.keys.selectedType;
        if (state.definitions.programData.programGames.length > 0) {
          this.setProgramData(state.toolbar.selectedProgram, state.definitions.programData)
          self.isProgramSelectionDirty = state.definitions.isProgramDataDirty;
        }
      })
    this.selectedProgram$ = this.store.pipe(select(fromApp.getShrinkToolbarSelectedProgram));
    this.selectedProgramSubscription$ = this.selectedProgram$
      .subscribe(program => {
        if (program) {
          self.programTitle = program.title;
          if (self.state.mode != StateMode.Loading) {
            var endDate = program.value.endDate;
            var programUniqueId = program.value.uniqueId;
            self.isValidForCalc = true;
            if (self.state.definitions.programData.programGames.length == 0)
              self.useWinnerSiteApi(endDate, self.currentProgramType, programUniqueId);
          }
        }
      });
    this.params$ = this.store.pipe(select(fromApp.getTemplateWatcherParameters));
    this.paramsSubscription$ = this.params$
      .subscribe(data => {
        self.templateWatcherParameters = data;
        if (data && data.watcherNeedsUpdate == true) {
          self.dispatchTemplateWatcher();
        }

      });
  }
  setProgramData(selectedProgram: SelectItem, programData: ProgramData) {
    var self = this;
    self.programData = programData;
    self.isProgramSelectionDirty = false
    if (self.state.mode == StateMode.Loading) {
      this.store.dispatch(new appActions.ShrinkSystemLoadFinished());
      self.gatewayService.sendProgramDetail(self.programData);
      this.store.dispatch(new appActions.ShrinkSystemUpdateDirtyProgramDataFlag(true));
    }
  }

  useWinnerSiteApi(endDate: Date, programType: ProgramType, programUniqueId: string) {
    this.programsService.getProgram(endDate, programType)
      .subscribe(pData => {
        this.onSuccessRetrieveProgram(pData, endDate, programType, programUniqueId);
      });
  }
  onSuccessRetrieveProgram(pData, endDate: Date, programType: ProgramType, programUniqueId: string) {
    try {
      var programsCol = Object.values<any>(pData.details);
      var programDetails: ProgramDetails[] = [];
      var programStillOpen: boolean = false;
      for (let entry of programsCol) {
        var betsDetails: BetProgramDetail[] = [
          { rate: entry.bets.details.bet1.rate, won: entry.bets.details.bet1.won, rateLabel: (Math.round(entry.bets.details.bet1.rate * 100) / 100).toFixed(2) },
          { rate: entry.bets.details.betX.rate, won: entry.bets.details.betX.won, rateLabel: (Math.round(entry.bets.details.betX.rate * 100) / 100).toFixed(2) },
          { rate: entry.bets.details.bet2.rate, won: entry.bets.details.bet2.won, rateLabel: (Math.round(entry.bets.details.bet2.rate * 100) / 100).toFixed(2) }
        ];
        if (entry.validations.validForCalculation === undefined)
          this.isValidForCalc = false;
        programStillOpen = programStillOpen || entry.bets.openForBets;
        programDetails.push({
          gameIndex: entry.gameIndex,
          betStatus: entry.betColor,
          statusStyle: { 'color': entry.betColor },
          gameTime: entry.gameTime,
          leagueName: entry.leagueName,
          gameName: entry.gameName,
          openForBets: entry.bets.openForBets,
          bets: this.createBetsItems(betsDetails, entry.bets.openForBets, entry.validations),
          betsProgramDetail: betsDetails,
          gameResult: entry.gameResult,
          bet1X2String: this.create1X2stringFromFinalResult(betsDetails),
          betsSelection: [false, false, false],
          validations: entry.validations
        });
      }
      if (!this.isValidForCalc)
        this.programTitle += '  ' + 'תוכניה לא שלמה';
      this.programData = {
        programGames: programDetails,
        programTitle: this.programTitle,
        finalResult: pData.finalResult,
        openForBets: programStillOpen,
        programDate: endDate,
        programType: programType,
        uniqueId: programUniqueId
      };
      this.store.dispatch(new appActions.SetShrinkSystemDataProgramGames(this.programData));
      this.gatewayService.sendProgramDetail(this.programData);
      this.isProgramSelectionDirty = false;
      //      this.store.dispatch(new appActions.ProgramRatesUpdated());
      this.dispatchTemplateWatcher();
    }
    catch (error) {
      this.onErrorRetrieveProgram(error);
    }
  }
  dispatchTemplateWatcher() {
    var self = this;
    if (self.templateWatcherParameters && self.templateWatcherParameters.bets.length > 0) {
      var ratesInfo: RatesInfo[] = [];
      var index: number = 1;
      self.templateWatcherParameters.bets.forEach(element => {
        var rates: RatesInfo = {
          gameIndex: index,
          rate1: +element[0].label,
          rateX: +element[1].label,
          rate2: +element[2].label,
        }
        ratesInfo.push(rates);
        index++;
      });
      //this.stateTemplates = self.templateWatcherParameters.templates;
      var payload: WatchTemplatesRequest = {
        templates: self.templateWatcherParameters.templates,
        rates: ratesInfo
      }
      this.store.dispatch(new appActions.WatchTemplates(payload));
      //        self.waitForData = true;
      //   }
    }
  }
  onErrorRetrieveProgram(error) {
    this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'לא קיימת תוכניה במאגר, אנא בחר תוכניה אחרת' }));
    // this.messageService.add({
    //   key: 'tc', severity: 'error',
    //   summary: 'תוכניה לא נמצאה',
    //   detail: 'לא קיימת תוכניה במאגר, אנא בחר תוכניה אחרת',
    //   life: 1500,
    //   closable: false
    // });
    this.programTitle += '  ' + 'לא קיימת תוכנית במאגר';
    this.programData = null;
    this.gatewayService.sendProgramDetail(this.programData);
    this.isProgramSelectionDirty = false;
    //this.store.dispatch(new appActions.ShrinkSystemUpdateDirtyProgramDataFlag(false));
  }
  createBetsItems(betsDetails: BetProgramDetail[], openForBets: boolean, validations: any): SelectItem[] {
    var betXStyleClass = betsDetails[1].won ? "wn-ui-button-won-bet" : "";
    if (validations.validForCalculation === undefined && validations.missingRates === undefined) {
      //DummyGame, NoWinnerFound=GameCanceled, GamePostponed, GameCanceled,GameResultMissing
      betXStyleClass = "wn-ui-button-won-bet-amended";
    }
    let betsArr: SelectItem[] = [
      { label: betsDetails[0].rateLabel, value: 1, disabled: !openForBets, styleClass: betsDetails[0].won ? "wn-ui-button-won-bet" : "" },
      { label: betsDetails[1].rateLabel, value: 2, disabled: !openForBets, styleClass: betXStyleClass },
      { label: betsDetails[2].rateLabel, value: 3, disabled: !openForBets, styleClass: betsDetails[2].won ? "wn-ui-button-won-bet" : "" }];
    return betsArr;
  }
  create1X2stringFromFinalResult(betsDetails: any): string {
    var betStr = ["", "", ""];
    betStr[0] = betsDetails[0].won ? "1" : "";
    betStr[1] = betsDetails[1].won ? "X" : "";
    betStr[2] = betsDetails[2].won ? "2" : "";
    return betStr.join("");
  }
  create1X2string(betsSelection: boolean[]): string[] {
    var betStr = ["", "", ""];
    betStr[0] = betsSelection[0] ? "1" : "";
    betStr[1] = betsSelection[1] ? "X" : "";
    betStr[2] = betsSelection[2] ? "2" : "";
    return betStr;
  }
  onChange($event, rowData) {
    var betStr = this.create1X2string(this.programData.programGames[rowData.gameIndex - 1].betsSelection);
    this.programData.programGames[rowData.gameIndex - 1].bet1X2String = betStr.join("");
    this.isProgramSelectionDirty = true;
    //this.store.dispatch(new appActions.ShrinkSystemUpdateDirtyProgramDataFlag(true));
    this.gatewayService.sendProgramDetail(this.programData);
  }
  onOptionClick($event, rowData) {
    this.programData.programGames[rowData.gameIndex - 1].betsSelection[$event.index] = !this.programData.programGames[rowData.gameIndex - 1].betsSelection[$event.index];

    this.store.dispatch(new appActions.SetShrinkSystemDataProgramGames(this.programData));
    // var clickData : BetsSelectionClickData = {
    //   gameIndex : rowData.gameIndex,
    //   betIndex : $event.index,
    //   betValue: this.programData.programGames[rowData.gameIndex - 1].betsSelection[$event.index]
    // }
    // this.store.dispatch(new appActions.SetShrinkSystemDataBetsSelection(clickData));
  }
  clearSelectedBets(): void {
    if (this.programData.openForBets) {
      for (let p of this.programData.programGames) {
        p.selectedRates = [];
        p.betsSelection = [false, false, false];
        p.bet1X2String = "";
      }
      this.gatewayService.sendProgramDetail(this.programData);
      this.store.dispatch(new appActions.ShrinkSystemUpdateDirtyProgramDataFlag(false));
    }
  }
  refreshData(): void {
    this.useWinnerSiteApi(this.programData.programDate, this.programData.programType, this.programData.uniqueId);
  }
  isDirty(): boolean {
    return this.programData.openForBets && this.isProgramSelectionDirty;
  }
  fillEvents(eventsBets: BetsCalculation.EventsBet[], eventsRates: BetsCalculation.EventsRate[]): void {
    for (let game of this.programData.programGames) {
      eventsBets.push({
        position: game.gameIndex - 1,
        bet1: game.bet1X2String.indexOf('1') != -1,
        betX: game.bet1X2String.indexOf('X') != -1,
        bet2: game.bet1X2String.indexOf('2') != -1
      });
      eventsRates.push({
        position: game.gameIndex - 1,
        rate1: +game.bets[0].label,
        rateX: +game.bets[1].label,
        rate2: +game.bets[2].label
      });
    }
  }
}
