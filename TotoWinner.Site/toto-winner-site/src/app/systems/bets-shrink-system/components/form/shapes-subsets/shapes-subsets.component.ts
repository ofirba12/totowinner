import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { ShapesPatternComponent } from '../../_directives';

@Component({
  selector: 'app-shapes-subsets',
  templateUrl: './shapes-subsets.component.html',
  styleUrls: ['./shapes-subsets.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ShapesSubsetsComponent implements OnInit {
  isDirtyComponent: boolean = false;
  @Output() isDirty: EventEmitter<any> = new EventEmitter();

  @ViewChildren(ShapesPatternComponent) shapesSettings: QueryList<ShapesPatternComponent>;

  constructor() { }

  ngOnInit() {
  }
  clearSelection(): void {
    if (this.shapesSettings == undefined)
      return;
    for (let shape of this.shapesSettings.toArray()) {
      shape.clearSelection();
    }
    this.isDirtyComponent = false;
  }
  setDirty(): void {
    this.isDirtyComponent = true;
    this.isDirty.emit(null);
  }
  fillValues(formSettings: BetsCalculation.FormSettings): void {
    for (let shape of this.shapesSettings.toArray()) {
      shape.fillValues(formSettings);
    }
  }
}
