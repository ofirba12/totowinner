import { Component, OnInit, ViewChildren, QueryList, Output, EventEmitter, OnDestroy } from '@angular/core';
//import { ProgramData } from '../../../model/program-data';
import { TotoGamesService, AppConnetorService } from '../../../../../_services';
import { ProgramData } from 'src/app/model/interfaces/program.module';
import { SystemStateStatus } from 'src/app/model/interfaces/shared.module';
import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../../../state';
import * as appActions from '../../../../../state/app.actions';
import { StateMode, ShrinkState } from 'src/app/state/app';
import { MinmaxComponent } from 'src/app/shared/directives';

@Component({
  selector: 'app-basic-subsets',
  templateUrl: './basic-subsets.component.html',
  styleUrls: ['./basic-subsets.component.css']
})
export class BasicSubsetsComponent implements OnInit, OnDestroy {
  @Output() isDirty: EventEmitter<any> = new EventEmitter();

  defaultMin: number;
  defaultMax: number;
  cleanOnDiff: boolean;
  isDirtyComponent: boolean = false;
  loading: boolean;
  @ViewChildren(MinmaxComponent) includeSettings: QueryList<MinmaxComponent>;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;
  constructor(private totoGamesService: TotoGamesService,
    private stateManagerService: AppConnetorService,
    private store: Store<fromApp.AppState>) { }
  ngOnDestroy(): void {
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
  }
  ngOnInit() {
    var self = this;
    self.loading = true;
    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        if (state.definitions.programData.programGames.length > 0) {
          self.defaultMin = 0;
          self.defaultMax = state.definitions.programData.programGames.length;
          if (state.definitions.form /*&& self.includeSettings*/ && (state.mode == StateMode.Loading || self.loading)) {
            self.cleanOnDiff = state.definitions.form.cleanOnDiff;
            self.loading = false;
            setTimeout(function () { self.loadValues(state.definitions.form.formSettings) }, 500);           
          }
        }
      })
  }
 
  clearSelection(): void {
    if (this.includeSettings == undefined)
      return;
    for (let settings of this.includeSettings.toArray()) {
      settings.clearSelection();
    }
    this.cleanOnDiff = false;
    this.isDirtyComponent = false;
  }
  setDirty(): void {
    if (!this.isDirtyComponent)
      this.isDirty.emit(null);
    this.isDirtyComponent = true;
  }
  fillValues(formSetting: BetsCalculation.FormSettings): void {
    if (formSetting.combinationLettersSettings == null) {
      formSetting.combinationLettersSettings = {
        lettersSettings: null,
        minMaxA: { min: 0, max: 16 },
        minMaxB: { min: 0, max: 16 },
        minMaxC: { min: 0, max: 16 },
        minMaxBreak: { min: 0, max: 15 },
        minMaxSequenceLengthA: { min: 0, max: 16 },
        minMaxSequenceLengthB: { min: 0, max: 16 },
        minMaxSequenceLengthC: { min: 0, max: 16 }
      }
    }
    for (let howMany of this.includeSettings.toArray()) {
      var minmax: BetsCalculation.MinMax =
        { min: howMany.selectedMinValue.value, max: howMany.selectedMaxValue.value };
      switch (howMany.id) {
        case 'minmax_include_amount1':
          formSetting.includeSettings.amount1 = minmax;
          break;
        case 'minmax_include_amountx':
          formSetting.includeSettings.amountX = minmax;
          break;
        case 'minmax_include_amount2':
          formSetting.includeSettings.amount2 = minmax;
          break;
        case 'minmax_include_sequence1':
          formSetting.includeSettings.sequence1 = minmax;
          break;
        case 'minmax_include_sequencex':
          formSetting.includeSettings.sequenceX = minmax;
          break;
        case 'minmax_include_sequence2':
          formSetting.includeSettings.sequence2 = minmax;
          break;
        case 'minmax_include_break1X2':
          formSetting.includeSettings.sequenceBreak = minmax;
          break;
        case 'minmax_include_amountA':
          formSetting.combinationLettersSettings.minMaxA = minmax;
          break;
        case 'minmax_include_amountB':
          formSetting.combinationLettersSettings.minMaxB = minmax;
          break;
        case 'minmax_include_amountC':
          formSetting.combinationLettersSettings.minMaxC = minmax;
          break;
        case 'minmax_include_sequenceA':
          formSetting.combinationLettersSettings.minMaxSequenceLengthA = minmax;
          break;
        case 'minmax_include_sequenceB':
          formSetting.combinationLettersSettings.minMaxSequenceLengthB = minmax;
          break;
        case 'minmax_include_sequenceC':
          formSetting.combinationLettersSettings.minMaxSequenceLengthC = minmax;
          break;
        case 'minmax_include_breakABC':
          formSetting.combinationLettersSettings.minMaxBreak = minmax;
          break;
      }
    }
  }
  loadValues(formSettingsState: BetsCalculation.FormSettings) {
    var self = this;
    for (let setting of self.includeSettings.toArray()) {
      switch (setting.id) {
        case 'minmax_include_amount1':
          setting.loadValues(formSettingsState.includeSettings.amount1.min, formSettingsState.includeSettings.amount1.max);
          break;
        case 'minmax_include_amountx':
          setting.loadValues(formSettingsState.includeSettings.amountX.min, formSettingsState.includeSettings.amountX.max);
          break;
        case 'minmax_include_amount2':
          setting.loadValues(formSettingsState.includeSettings.amount2.min, formSettingsState.includeSettings.amount2.max);
          break;
        case 'minmax_include_sequence1':
          setting.loadValues(formSettingsState.includeSettings.sequence1.min, formSettingsState.includeSettings.sequence1.max);
          break;
        case 'minmax_include_sequencex':
          setting.loadValues(formSettingsState.includeSettings.sequenceX.min, formSettingsState.includeSettings.sequenceX.max);
          break;
        case 'minmax_include_sequence2':
          setting.loadValues(formSettingsState.includeSettings.sequence2.min, formSettingsState.includeSettings.sequence2.max);
          break;
        case 'minmax_include_break1X2':
          setting.loadValues(formSettingsState.includeSettings.sequenceBreak.min, formSettingsState.includeSettings.sequenceBreak.max);
          break;
        case 'minmax_include_amountA':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxA.min, formSettingsState.combinationLettersSettings.minMaxA.max);
          break;
        case 'minmax_include_amountB':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxB.min, formSettingsState.combinationLettersSettings.minMaxB.max);
          break;
        case 'minmax_include_amountC':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxC.min, formSettingsState.combinationLettersSettings.minMaxC.max);
          break;
        case 'minmax_include_sequenceA':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxSequenceLengthA.min, formSettingsState.combinationLettersSettings.minMaxSequenceLengthA.max);
          break;
        case 'minmax_include_sequenceB':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxSequenceLengthB.min, formSettingsState.combinationLettersSettings.minMaxSequenceLengthB.max);
          break;
        case 'minmax_include_sequenceC':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxSequenceLengthC.min, formSettingsState.combinationLettersSettings.minMaxSequenceLengthC.max);
          break;
        case 'minmax_include_breakABC':
          setting.loadValues(formSettingsState.combinationLettersSettings.minMaxBreak.min, formSettingsState.combinationLettersSettings.minMaxBreak.max);
          break;
      }
    }
  }
}
