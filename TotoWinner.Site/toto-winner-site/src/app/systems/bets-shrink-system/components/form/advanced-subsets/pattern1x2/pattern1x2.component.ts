import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { CombinationPatternComponent } from 'src/app/systems/bets-shrink-system/components/_directives';

@Component({
  selector: 'app-pattern1x2',
  templateUrl: './pattern1x2.component.html',
  styleUrls: ['./pattern1x2.component.css']
})
export class Pattern1x2Component implements OnInit {
  @Output() isDirty: EventEmitter<any> = new EventEmitter();

  @ViewChild(CombinationPatternComponent, { static: true }) combinationSettings: CombinationPatternComponent;
  isDirtyComponent: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  fillValues(formSettings: BetsCalculation.FormSettings): void {
    var combination1X2Settings: BetsCalculation.Combination1X2Settings[] = [];
    for (let cmb of this.combinationSettings.combinations) {
      var patterns: string[] = [];
      for (let game of cmb.games) {
        patterns.push(game.selectedCombination ? game.selectedCombination.label : "");
      }
      var settings: BetsCalculation.Combination1X2Settings = {
        combinations: patterns,
        minMax: { min: cmb.min, max: cmb.max },
        isActive: cmb.selected,
        type: 0
      }
      combination1X2Settings.push(settings);
    }
    if (combination1X2Settings.length > 0)
      formSettings.combination1X2Settings = combination1X2Settings;
  }
  clearSelection(): void {
    this.isDirtyComponent = false;
    this.combinationSettings.clearSelection();
  }

  setDirty(): void {
    if (!this.isDirtyComponent) {
      this.isDirtyComponent = true;
      this.isDirty.emit(null);
    }
  }
}
