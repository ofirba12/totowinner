import { Component, OnInit, ViewChildren, QueryList, Output, EventEmitter, ViewChild, ViewEncapsulation, OnDestroy } from '@angular/core';
import { DecimalRangeComponent } from '../../../_directives';
import { TotoGamesService, AppConnetorService } from '../../../../../../_services';
import { PricerData } from '../../../../../../model/pricer-data';
//import { ProgramData } from '../../../../model/program-data';
import { DropDownValue, SystemStateStatus } from '../../../../../../model/interfaces/shared.module';
import { ProgramData } from 'src/app/model/interfaces/program.module';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../../../../state';
import * as appActions from '../../../../../../state/app.actions';
import { StateMode, ShrinkState } from 'src/app/state/app';
import { Observable, Subscription } from 'rxjs';
import { MinmaxComponent } from 'src/app/shared/directives';

@Component({
  selector: 'app-exclude',
  templateUrl: './exclude.component.html',
  styleUrls: ['./exclude.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ExcludeComponent implements OnInit, OnDestroy {
  @Output() onErrorExclude: EventEmitter<boolean> = new EventEmitter();
  @Output() isDirty: EventEmitter<any> = new EventEmitter();

  isDirtyComponent: boolean = false;
  defaultRangeMin: number;
  defaultRangeMax: number;
  defaultMin: number;
  defaultMax: number;
  notInRangeSwitch: boolean = false;
  notOccurance1Switch: boolean = false;
  notOccuranceXSwitch: boolean = false;
  notOccurance2Switch: boolean = false;
  notSequence1Switch: boolean = false;
  notSequenceXSwitch: boolean = false;
  notSequence2Switch: boolean = false;
  values: DropDownValue[];
  selectedNoSeq1: DropDownValue;
  selectedNoSeqX: DropDownValue;
  selectedNoSeq2: DropDownValue;
  notBreaksSwitch: boolean = false;
  selectedBreak: number;
  loading: boolean;
  @ViewChildren(DecimalRangeComponent) rangeSettings: QueryList<DecimalRangeComponent>;
  @ViewChildren(MinmaxComponent) minMaxSettings: QueryList<MinmaxComponent>;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;

  constructor(private totoGamesService: TotoGamesService,
    private gatewayService: AppConnetorService,
    private store: Store<fromApp.AppState>) { }
  ngOnDestroy(): void {
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
  }
  ngOnInit() {
    var self = this;
    self.loading = true;
    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        if (state.definitions.programData.programGames.length > 0) {
          self.defaultMin = 0;
          self.defaultMax = state.definitions.programData.programGames.length;
          if (state.definitions.form /*&& self.rangeSettings && self.minMaxSettings*/
            && (state.mode == StateMode.Loading || self.loading)) {
            self.loading = false;
            setTimeout(function () { 
              //console.warn("exclude loading", state.mode, state.definitions.form.formSettings)
              self.loadValues(state.definitions.form.formSettings) 
            }, 500);
          }
        }
      })
    this.gatewayService.getPricerDataSubject().subscribe(pricerData => {
      if (pricerData)
        this.fetchPricerData(pricerData)
    });
    this.gatewayService.getGameSelectionSubject().subscribe(gamesData => {
      if (gamesData)
        this.fetchGamesData(gamesData);
    });
  }
  fetchGamesData(gamesData: ProgramData) {
    this.defaultMin = 0;
    this.defaultMax = gamesData.programGames.length;
    this.values = [];
    for (let i = 0; i <= this.defaultMax; i++) {
      this.values.push(
        { label: i.toString(), value: i }
      );
    }
  }

  fetchPricerData(pricerData: PricerData) {
    var self = this;
    this.defaultRangeMin = pricerData.minRatesSum;
    this.defaultRangeMax = pricerData.maxRatesSum;
  }

  clearSelection(): void {
    if (this.rangeSettings == undefined || this.minMaxSettings == undefined || this.values == undefined)
      return;
    this.isDirtyComponent = false;
    this.notInRangeSwitch = false;
    this.notOccurance1Switch = this.notOccuranceXSwitch = this.notOccurance2Switch = false;
    for (let range of this.rangeSettings.toArray()) {
      range.clearSelection();
    }
    for (let set of this.minMaxSettings.toArray()) {
      set.clearSelection();
      set.disabled = true;
    }
    this.selectedNoSeq1 = this.selectedNoSeqX = this.selectedNoSeq2 = this.values[0];
    this.notSequence1Switch = this.notSequenceXSwitch = this.notSequence2Switch = false;
    this.notBreaksSwitch = false;
  }
  setDirty(): void {
    if (!this.isDirtyComponent) {
      this.isDirtyComponent = true;
      this.isDirty.emit(null);
    }
  }
  onError(state: boolean): void {
    this.onErrorExclude.emit(state);
  }
  fillValues(formSetting: BetsCalculation.FormSettings): void {
    for (let range of this.rangeSettings.toArray()) {
      var setting: BetsCalculation.MinMax = { min: +range.decimalFrom, max: +range.decimalTo }
      switch (range.id) {
        case 'minmax_include_range':
          formSetting.includeSettings.amountSum = setting;
          break;
        case 'minmax_exclude_amountsum':
          formSetting.excludeSettings.amountSum = setting;
          formSetting.excludeSettings.activateSumAmountFilter = this.notInRangeSwitch;
          break;
      }
    }
    var voidSetting: BetsCalculation.MinMax = { min: 0, max: 0 }
    for (let howMany of this.minMaxSettings.toArray()) {
      var setting: BetsCalculation.MinMax = { min: howMany.selectedMinValue.value, max: howMany.selectedMaxValue.value }
      switch (howMany.id) {
        case 'minmax_exclude_amount1':
          formSetting.excludeSettings.amount1 = setting;
          formSetting.excludeSettings.activateOneAmountFilter = this.notOccurance1Switch;
          break;
        case 'minmax_exclude_amountX':
          formSetting.excludeSettings.amountX = setting;
          formSetting.excludeSettings.activateXAmountFilter = this.notOccuranceXSwitch;
          break;
        case 'minmax_exclude_amount2':
          formSetting.excludeSettings.amount2 = setting;
          formSetting.excludeSettings.activateTwoAmountFilter = this.notOccurance2Switch;
          break;
        case 'minmax_exclude_breaks':
          formSetting.excludeSettings.sequenceBreak = setting;
          formSetting.excludeSettings.activateBreakFilter = this.notBreaksSwitch;
          break;
      }
    }
    formSetting.excludeSettings.activateOneSequenceFilter = this.notSequence1Switch;
    formSetting.excludeSettings.activateXSequenceFilter = this.notSequenceXSwitch;
    formSetting.excludeSettings.activateTwoSequenceFilter = this.notSequence2Switch;
    formSetting.excludeSettings.sequence1 = this.selectedNoSeq1 ? this.selectedNoSeq1.value : this.values[0].value;
    formSetting.excludeSettings.sequenceX = this.selectedNoSeqX ? this.selectedNoSeqX.value : this.values[0].value;
    formSetting.excludeSettings.sequence2 = this.selectedNoSeq2 ? this.selectedNoSeq2.value : this.values[0].value;
  }
  loadValues(formSettingsState: BetsCalculation.FormSettings) {
    var self = this;
    //#region sequence1X2 
    self.notSequence1Switch = formSettingsState.excludeSettings.activateOneSequenceFilter;
    self.notSequenceXSwitch = formSettingsState.excludeSettings.activateXSequenceFilter;
    self.notSequence2Switch = formSettingsState.excludeSettings.activateTwoSequenceFilter;
    self.selectedNoSeq1 = { label: formSettingsState.excludeSettings.sequence1.toString(), value: formSettingsState.excludeSettings.sequence1 };
    self.selectedNoSeqX = { label: formSettingsState.excludeSettings.sequenceX.toString(), value: formSettingsState.excludeSettings.sequenceX };
    self.selectedNoSeq2 = { label: formSettingsState.excludeSettings.sequence2.toString(), value: formSettingsState.excludeSettings.sequence2 };
    //#endregion    
    //#region range setting
    for (let setting of self.rangeSettings.toArray()) {
      switch (setting.id) {
        case 'minmax_include_range':
          setting.loadValues(formSettingsState.includeSettings.amountSum.min, formSettingsState.includeSettings.amountSum.max);
          break;
        case 'minmax_exclude_amountsum':
          self.notInRangeSwitch = formSettingsState.excludeSettings.activateSumAmountFilter;
          setting.loadValues(formSettingsState.excludeSettings.amountSum.min, formSettingsState.excludeSettings.amountSum.max);
          break;
      }
    }
    //#endregion
    //#region min max settings
    for (let setting of this.minMaxSettings.toArray()) {
      var defaultSetting: BetsCalculation.MinMax = { min: self.defaultMin, max: self.defaultMax };
      switch (setting.id) {
        case 'minmax_exclude_amount1':
          self.notOccurance1Switch = formSettingsState.excludeSettings.activateOneAmountFilter;
          setting.disabled = !self.notOccurance1Switch;
          setting.loadValues(formSettingsState.excludeSettings.amount1.min, formSettingsState.excludeSettings.amount1.max);
          break;
        case 'minmax_exclude_amountX':
          self.notOccuranceXSwitch = formSettingsState.excludeSettings.activateXAmountFilter;
          setting.disabled = !self.notOccuranceXSwitch;
          setting.loadValues(formSettingsState.excludeSettings.amountX.min, formSettingsState.excludeSettings.amountX.max);
          break;
        case 'minmax_exclude_amount2':
          self.notOccurance2Switch = formSettingsState.excludeSettings.activateTwoAmountFilter;
          setting.disabled = !self.notOccurance2Switch;
          setting.loadValues(formSettingsState.excludeSettings.amount2.min, formSettingsState.excludeSettings.amount2.max);
          break;
        case 'minmax_exclude_breaks':
          self.notBreaksSwitch = formSettingsState.excludeSettings.activateBreakFilter;
          setting.disabled = !self.notBreaksSwitch;
          setting.loadValues(formSettingsState.excludeSettings.sequenceBreak.min, formSettingsState.excludeSettings.sequenceBreak.max);
          break;
      }
    }
    //#endregion
  }

}
