import { Component, OnInit } from '@angular/core';
import { TotoGamesService, AppConnetorService } from 'src/app/_services';
//import { ProgramData } from '../../../model/program-data';
import { PricerData } from '../../../../../model/pricer-data';
import { ProgramData } from 'src/app/model/interfaces/program.module';

@Component({
  selector: 'app-pricer',
  templateUrl: './pricer.component.html',
  styleUrls: ['./pricer.component.css']
})
export class PricerComponent implements OnInit {
  public pricerData : PricerData = new PricerData(0,0,0,"0",0,0,0,0,0,0,0,0,0);
  constructor(private gatewayService: AppConnetorService) { }

  ngOnInit() {
    this.gatewayService.getGameSelectionSubject().subscribe(gamesData => {
      if (gamesData)
        this.aggregateData(gamesData)
    });
  }
  aggregateData(gamesData : ProgramData) {
    this.pricerData.init();
    for (let game of gamesData.programGames) {
      if (game.bet1X2String.length == 1)
        this.pricerData.bankersCounter++;
      if (game.bet1X2String.length == 2)
        this.pricerData.doublesCounter++;
      if (game.bet1X2String.length == 3)
        this.pricerData.trianglesCounter++;
      this.pricerData.minRatesSum += Math.min(game.betsProgramDetail[0].rate, game.betsProgramDetail[1].rate, game.betsProgramDetail[2].rate);
      this.pricerData.maxRatesSum += Math.max(game.betsProgramDetail[0].rate, game.betsProgramDetail[1].rate, game.betsProgramDetail[2].rate);
      this.pricerData.sumAllRates += game.betsProgramDetail[0].rate + game.betsProgramDetail[1].rate + game.betsProgramDetail[2].rate;
      this.pricerData.sumAllRates1 += game.betsProgramDetail[0].rate;
      this.pricerData.sumAllRatesX += game.betsProgramDetail[1].rate;
      this.pricerData.sumAllRates2 += game.betsProgramDetail[2].rate;
    }
    if (this.pricerData.trianglesCounter + this.pricerData.doublesCounter + this.pricerData.bankersCounter == 0)
      this.pricerData.betsCounter = "0";
    else
    {
      this.pricerData.betsCounterNumber = Math.max(1,Math.pow(3, this.pricerData.trianglesCounter)) * Math.max(1,Math.pow(2, this.pricerData.doublesCounter));
      this.pricerData.betsCounter = this.pricerData.betsCounterNumber.toLocaleString();
    }
    this.pricerData.averageRates = this.pricerData.sumAllRates / 3;
    this.pricerData.finalResult = gamesData.finalResult;
    this.gatewayService.sendPricerData(this.pricerData);
  }
}