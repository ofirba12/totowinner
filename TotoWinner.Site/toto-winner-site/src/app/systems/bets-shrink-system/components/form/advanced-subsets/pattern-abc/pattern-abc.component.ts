import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { CombinationPatternComponent } from 'src/app/systems/bets-shrink-system/components/_directives';

@Component({
  selector: 'app-pattern-abc',
  templateUrl: './pattern-abc.component.html',
  styleUrls: ['./pattern-abc.component.css']
})
export class PatternABCComponent implements OnInit {
  @Output() isDirty: EventEmitter<any> = new EventEmitter();
  @ViewChild(CombinationPatternComponent, { static: true }) combinationSettings: CombinationPatternComponent;
  isDirtyComponent: boolean = false;
  constructor() { }

  ngOnInit() {
  }
  clearSelection(): void {
    this.isDirtyComponent = false;
    this.combinationSettings.clearSelection();
  }
  setDirty(): void {
    if (!this.isDirtyComponent) {
      this.isDirtyComponent = true;
      this.isDirty.emit(null);
    }
  }
  fillValues(formSettings: BetsCalculation.FormSettings): void {
    var combinationLettersSettings: BetsCalculation.LettersSetting[] = [];
    for (let cmb of this.combinationSettings.combinations) {
      var patterns: string[] = [];
      for (let game of cmb.games) {
        patterns.push(game.selectedCombination ? game.selectedCombination.label : "");
      }
      var settings: BetsCalculation.LettersSetting = {
        combinations: patterns,
        minMax: { min: cmb.min, max: cmb.max },
        isActive: cmb.selected,
        type: 1
      }
      combinationLettersSettings.push(settings);
    }
    if (combinationLettersSettings.length > 0) {
      if (formSettings.combinationLettersSettings == null) {
        formSettings.combinationLettersSettings = {
          lettersSettings: combinationLettersSettings,
          minMaxA: { min: 0, max: 16 },
          minMaxB: { min: 0, max: 16 },
          minMaxC: { min: 0, max: 16 },
          minMaxBreak: { min: 0, max: 15 },
          minMaxSequenceLengthA: { min: 0, max: 16 },
          minMaxSequenceLengthB: { min: 0, max: 16 },
          minMaxSequenceLengthC: { min: 0, max: 16 }
        }
      }
      formSettings.combinationLettersSettings.lettersSettings = combinationLettersSettings;
    }
  }
}
