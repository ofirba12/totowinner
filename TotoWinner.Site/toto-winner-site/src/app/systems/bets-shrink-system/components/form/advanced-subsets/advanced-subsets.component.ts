import { Component, OnInit, ViewEncapsulation, ViewChildren, QueryList, ViewChild, Output, EventEmitter } from '@angular/core';
import { ExcludeComponent } from './exclude/exclude.component';
import { Pattern1x2Component } from './pattern1x2/pattern1x2.component';
import { PatternABCComponent } from './pattern-abc/pattern-abc.component';

@Component({
  selector: 'app-advanced-subsets',
  templateUrl: './advanced-subsets.component.html',
  styleUrls: ['./advanced-subsets.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AdvancedSubsetsComponent implements OnInit {
  isDirtyComponent: boolean = false;
  hasError: boolean = false;
  @Output() isDirty: EventEmitter<any> = new EventEmitter();
  @Output() errorExists: EventEmitter<any> = new EventEmitter();
  @ViewChild(ExcludeComponent, { static: true }) excludeSettings: ExcludeComponent;
  @ViewChild(Pattern1x2Component, { static: true }) pattern1X2Settings: Pattern1x2Component;
  @ViewChild(PatternABCComponent, { static: true }) patternABCSettings: PatternABCComponent;

  constructor() { }

  ngOnInit() {
  }

  clearSelection(): void {
    this.excludeSettings.clearSelection();
    this.pattern1X2Settings.clearSelection();
    this.patternABCSettings.clearSelection();
    this.isDirtyComponent = false;
  }
  setDirty(): void {
    this.isDirtyComponent = true;
    this.isDirty.emit(null);
  }
  onErrorExclude(state: boolean): void {
    this.hasError = state;
    this.errorExists.emit(state);

  }
  fillValues(formSettings: BetsCalculation.FormSettings): void {
    this.excludeSettings.fillValues(formSettings);
    this.pattern1X2Settings.fillValues(formSettings);
    this.patternABCSettings.fillValues(formSettings);
  }
}
