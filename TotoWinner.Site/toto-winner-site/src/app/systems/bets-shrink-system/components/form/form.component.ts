import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { PricerComponent } from './pricer/pricer.component';
import { BasicSubsetsComponent } from './basic-subsets/basic-subsets.component';
import { AdvancedSubsetsComponent } from './advanced-subsets/advanced-subsets.component';
import { ShapesSubsetsComponent } from './shapes-subsets/shapes-subsets.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Output() isDirty: EventEmitter<any> = new EventEmitter();
  @Output() errorExists: EventEmitter<any> = new EventEmitter();
  @ViewChild(PricerComponent, { static: true }) pricer: PricerComponent;
  @ViewChild(BasicSubsetsComponent, { static: true }) basicSubsets: BasicSubsetsComponent;
  @ViewChild(AdvancedSubsetsComponent, { static: true }) advancedSubsets: AdvancedSubsetsComponent;
  @ViewChild(ShapesSubsetsComponent, { static: true }) shapesSubsets: ShapesSubsetsComponent;
  
  isDirtyComponent : boolean = false;
  hasError : boolean = false;
  constructor() { }
  ngOnInit() {    
  }
  clearSelections() {
    this.basicSubsets.clearSelection();
    this.advancedSubsets.clearSelection();
    this.advancedSubsets.clearSelection();
    this.shapesSubsets  .clearSelection();
    this.isDirtyComponent = false;    
  }
  setDirty(): void {
    this.isDirtyComponent = true;
    this.isDirty.emit(null);
    // var flag : boolean =  this.basicSubsets.isDirtyComponent ||
    //   this.advancedSubsets.isDirty();
    // return flag;
  }
  onError(state: boolean) : void{
    this.hasError = state;
    this.errorExists.emit(state);
  }
  fillIncludeSettings(formSetting: BetsCalculation.FormSettings) : void {
    this.basicSubsets.fillValues(formSetting);
    this.advancedSubsets.fillValues(formSetting);
    this.shapesSubsets.fillValues(formSetting);
  }
}
