import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, OnChanges } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-decimal-range',
  templateUrl: './decimal-range.component.html',
  styleUrls: ['./decimal-range.component.css'],
  animations: [
    trigger('errorState', [
      state('hidden', style({
        opacity: 0
      })),
      state('visible', style({
        opacity: 1
      })),
      transition('visible => hidden', animate('400ms ease-in')),
      transition('hidden => visible', animate('400ms ease-out'))
    ])
  ],
})
export class DecimalRangeComponent implements OnInit, OnChanges {
  @Input() id: string;
  @Input() min: number;
  @Input() max: number;
  @Input() disabled: boolean;
  @Output() isDirty: EventEmitter<any> = new EventEmitter(true);
  @Output() onError: EventEmitter<boolean> = new EventEmitter(true);
  @Output() exposeValues: EventEmitter<{ from: number, to: number }> = new EventEmitter();

  decimal2PointsRegex: RegExp = /^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/;
  /*Explanation: Regex greater than zero with 2 decimal places
  ^            # Start of string
  \s*          # Optional whitespace
  (?=.*[1-9])  # Assert that at least one digit > 0 is present in the string
  \d*          # integer part (optional)
  (?:          # decimal part:
   \.          # dot
   \d{1,2}     # plus one or two decimal digits
  )?           # (optional)
  \s*          # Optional whitespace
  $            # End of string  
  */
  decimalFrom: string;
  decimalTo: string;
  isDirtyComponent: boolean = false;
  hasError: boolean = false;
  //disabled: boolean = false;

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    var self = this;
    if (changes) {
      if (changes.max || changes.min) {
        if (!isNaN(self.max))
          self.decimalTo = self.max.toFixed(2);
        if (!isNaN(self.min))
          self.decimalFrom = self.min.toFixed(2);
        //console.warn('ngOnChanges decimal-range', self.decimalFrom, "-", self.decimalTo);
      }
      else if (changes.disabled && changes.disabled.currentValue) {
        self.disabled = changes.disabled.currentValue;
      }
    }
  }
  loadValues(minVal: number, maxVal: number) {
    var self = this;
    //console.warn("loadValues", minVal, maxVal);
    self.decimalFrom = minVal.toFixed(2);
    self.decimalTo = maxVal.toFixed(2);
  }
  clearSelection() {
    this.decimalFrom = this.min.toFixed(2);
    this.decimalTo = this.max.toFixed(2);
    //console.warn("clearSelection", this.decimalFrom, this.decimalTo);
    this.isDirtyComponent = false;
  }
  CheckState(form, message): boolean {
    var self = this;
    var response: boolean = false;
    var isValidFrom: boolean = self.decimalFrom != undefined && self.decimalFrom != '';
    var isValidTo: boolean = self.decimalTo != undefined && self.decimalTo != '';
    if (!isValidFrom || !isValidTo)
      return false;
    
    var inRangeFrom: boolean = +self.decimalFrom >= (+self.min.toFixed(2));
    var inRangeTo: boolean = +self.decimalTo <= (+self.max.toFixed(2));
    var isRangeValid: boolean = +self.decimalFrom <= +self.decimalTo;
    var exValidation: boolean = true;
    //console.log(inRangeFrom + ' ' + inRangeTo + ' ' + (+this.decimalTo) + ' ' + (+this.max.toFixed(2)));
    if (!inRangeFrom || !inRangeTo) {
      // console.warn('check state decimal-range error', +self.decimalTo, "<=", (+self.max.toFixed(2)),inRangeTo);
      // console.warn('check state decimal-range error', self.decimalFrom, ">=", self.min.toFixed(2), inRangeFrom);
      message.text = "מספר לא בטווח";
      exValidation = false;
    }
    else if (!isRangeValid) {
      message.text = "טווח לא חוקי, עד<מ";
      exValidation = false;
    }
    else if (form.dirty && !form.valid) {
      message.text = "מספר לא חוקי";
      exValidation = false;
    }
    // else if (form.dirty){
    //   message.text = "אנא הזן נתונים";
    //   exValidation = false;
    // }
    response = !exValidation;
    if (self.hasError != response) {
      //console.warn('has error decimal-range', 'form dirty',form.dirty,' ' ,message.text, self.decimalFrom, "-", self.decimalTo);
      self.onError.emit(response);
    }
    self.hasError = response;
    if (!self.isDirtyComponent && form.dirty &&
      (
        (+self.decimalFrom != (+self.min.toFixed(2))) ||
        (+self.decimalTo != (+self.max.toFixed(2)))
      )) {
      self.isDirtyComponent = true;
      self.isDirty.emit(null);
    }
    this.exposeValues.emit({ from: +self.decimalFrom, to: +self.decimalTo });
    return response;
  }
}