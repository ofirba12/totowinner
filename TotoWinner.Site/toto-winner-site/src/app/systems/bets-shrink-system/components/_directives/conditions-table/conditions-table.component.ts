import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { OptimizedFromTo, AdvancedConditionKey, AdvancedConditionType } from 'src/app/model/interfaces/optimizer.module';

@Component({
  selector: 'conditions-table',
  templateUrl: './conditions-table.component.html',
  styleUrls: ['./conditions-table.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConditionsTableComponent implements OnInit {
  @Input() headers: any[];
  @Input() data: OptimizedFromTo[];
  @Input() isRange: boolean;
  constructor() { }

  ngOnInit() {
    //    console.warn("con data", this.data);
  }
  resetFields(): void {
    for (let condition of this.data) {
        condition.optimized.from.item = condition.original.from.item;
        condition.optimized.from.class = '';
        condition.optimized.to.item = condition.original.to.item;
        condition.optimized.to.class = '';
    }
  }
}
