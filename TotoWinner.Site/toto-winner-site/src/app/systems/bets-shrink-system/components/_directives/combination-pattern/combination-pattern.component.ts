import { Component, OnInit, Input, OnChanges, SimpleChange, ViewEncapsulation, Output, EventEmitter, ViewChild, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { TotoGamesService, AppConnetorService } from 'src/app/_services';
import { DropDownValue, SystemStateStatus } from 'src/app/model/interfaces/shared.module';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { CombinationItem, CombinationGames } from 'src/app/model/interfaces/combination.module';
import { ConfirmationService } from 'primeng/api';
import { ProgramData } from 'src/app/model/interfaces/program.module';
import { MinmaxComponent } from '../../../../../shared/directives/minmax/minmax.component';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../../../state';
import * as appActions from '../../../../../state/app.actions';
import { StateMode, ShrinkState } from 'src/app/state/app';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-combination-pattern',
  templateUrl: './combination-pattern.component.html',
  styleUrls: ['./combination-pattern.component.css'],
  animations: [
    trigger('animation', [
      state('visible', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('void => *', [
        style({ transform: 'translateX(50%)', opacity: 0 }),
        animate('300ms ease-out')
      ]),
      transition('* => void', [
        animate(('250ms ease-in'), style({
          height: 0,
          opacity: 0,
          transform: 'translateX(50%)'
        }))
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class CombinationPatternComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() disabled: boolean;
  @Output() isDirty: EventEmitter<any> = new EventEmitter(true);

  @ViewChildren(MinmaxComponent) minMaxSettings: QueryList<MinmaxComponent>;
  values: DropDownValue[];
  defaultMin: number;
  defaultMax: number;
  combinations: CombinationGames[];
  isDirtyComponent: boolean = false;
  currentUniqueProgramId: string = '';
  loading: boolean;
  state: ShrinkState;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;
  constructor(private totoGamesService: TotoGamesService,
    private confirmationService: ConfirmationService,
//    private gatewayService: AppConnetorService,
    private store: Store<fromApp.AppState>) { }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes && changes.type && changes.type.currentValue) {
      this.init(changes.type.currentValue);
    }
  }

  ngOnDestroy(): void {
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
  }

  ngOnInit() {
    var self = this;
    self.loading = true;
    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        self.state = state;
        // if (state.definitions.programData.programGames.length > 0) {
        //   self.defaultMin = 0;
        //   self.defaultMax = state.definitions.programData.programGames.length;
        if (self.state.definitions.form /*&& self.rangeSettings && self.minMaxSettings*/
          && (self.state.mode == StateMode.Loading || self.loading)) {
          self.loading = false;
          self.loadValues();
          self.currentUniqueProgramId = state.definitions.programData.uniqueId;
          //console.warn(state.mode, self.currentUniqueProgramId, state.definitions);
          // setTimeout(function () { 
          //   self.currentUniqueProgramId =  state.definitions.programData.uniqueId;
          //   self.loadValues() }, 500);
        }
        else
          self.fetch(self.state.definitions.programData);
      }
        //}
      )
    //this.loading = true;
    //in case of performance issue, better to unsubscribe when not needed anymore
    //a new subject of program change should be created and this subsription should be there
    // this.gatewayService.getGameSelectionSubject().subscribe(gamesData => {
    //   if (self.state.mode == StateMode.Loading && this.loading == true){
    //     self.loading = false;
    //     this.loadValues(self.state.definitions.form.formSettings);
    //   }
    //   else
    //   this.fetch(gamesData)
    // });

    //     this.stateManagerService.getShrinkSystemStateSubject()
    //       .subscribe(state => {
    //         //if (state == SystemStateStatus.OnLoad && this.loading == true)
    // //          this.loadValues();
    //       });
  }
  fetch(gamesData: ProgramData) {
    var self = this;
    if (gamesData) {
      if (this.currentUniqueProgramId != gamesData.uniqueId) {
        //console.warn("call fetch", gamesData);
        this.defaultMin = 0;
        this.defaultMax = gamesData.programGames.length;
        this.buildTable();
        this.currentUniqueProgramId = gamesData.uniqueId;
        //         self.loading = true;
        //         if (self.loading && this.stateManagerService.hasValidState("CombinationPatternComponent")) {
        //           setTimeout(function () { //TODO Should consider usage of proper angular lifecycle events
        //             //            console.log("timeout","CombinationPatternComponent");
        // //            self.loadValues();
        //             self.loading = false;
        //           }, 500);
        //        }
      }
    }
  }
  minChanges($event, combinationGames: CombinationGames) {
    combinationGames.min = $event;
  }
  maxChanges($event, combinationGames: CombinationGames) {
    combinationGames.max = $event;
  }
  loadValues(): void {
    var self = this;
    //var formSettingsState = self.stateManagerService.shrinkSystemInternalState.form.formSettings;
    self.defaultMin = 0;
    self.defaultMax = self.state.definitions.programData.programGames.length;
    if (this.combinations && this.combinations.length > 0)
      this.combinations.splice(0, 1); //* The first combination is created for a page with no loaded values bu default
    else
      this.buildTable();

    var combinations: any;
    var formSettingsState = self.state.definitions.form.formSettings;
    if (this.type == "1X2") {
      combinations = formSettingsState.combination1X2Settings;
    }
    else if (this.type == "ABC") {
      combinations = formSettingsState.combinationLettersSettings.lettersSettings;
    }
    var counter = 0;

    for (let combination of combinations) {
      if (self.combinations[counter] === undefined) {
        self.addCombination(combination.minMax.min, combination.minMax.max, combination.isActive);
      }
      else {
        self.updateCombination(counter, combination.minMax.min, combination.minMax.max, combination.isActive);
      }
      try {
        for (let i = 0; i < combination.combinations.length; i++) {
          var ddItem = self.values.find((item) => item.label == combination.combinations[i]);
          self.combinations[counter].games[i].selectedCombination = ddItem;
        }
      }
      catch (error) {
        console.error("combinations issue to resolve:", self.defaultMax, self.type, self.combinations[counter], error);
      }
      counter++;
    }
  }
  updateCombination(index: number, selectedMin: number, selectedMax: number, combinationSelected: boolean) {
    this.combinations[index].min = selectedMin;
    this.combinations[index].max = selectedMax;
    this.combinations[index].selected = combinationSelected;
  }
  addCombination(selectedMin: number, selectedMax: number, combinationSelected: boolean = null) {
    var combination: CombinationGames =
      { index: this.combinations.length, selected: combinationSelected != null ? combinationSelected : true, games: [], min: selectedMin, max: selectedMax };
    for (let i = 1; i <= this.defaultMax; i++) {
      combination.games.push({ index: i, combinationValues: this.values, selectedCombination: null });
    }
    this.combinations.push(combination);
    if (this.combinations.length > 1)
      this.setDirty();
  }
  deleteCombination($event, i: number) {
    this.confirmationService.confirm({
      header: 'מחיקת נתונים',
      message: 'בחרת למחוק טבלה מספר ' + (i + 1) + '. האם למחוק?',
      accept: () => {
        this.combinations.splice(i, 1);
      }
    });
  }
  setDirty() {
    if (!this.isDirtyComponent)
      this.isDirty.emit(null);
    this.isDirtyComponent = true;
  }
  clearSelection() {
    this.buildTable();
    this.isDirtyComponent = false;
  }
  // OnChangePattern($event, rowData: CombinationItem) {
  //   rowData.selectedCombination = $event.value;
  //   this.setDirty();
  // }

  buildTable(): void {
    this.combinations = [];
    this.addCombination(this.defaultMin, this.defaultMax, true);
  }
  init(type: string): void {
    this.values = [];
    if (type == "1X2") {
      this.values = [
        { label: "", value: 0 },
        { label: "1", value: 1 },
        { label: "X", value: 2 },
        { label: "2", value: 3 },
        { label: "X1", value: 4 },
        { label: "21", value: 5 },
        { label: "2X", value: 6 },
        { label: "2X1", value: 7 },
      ]
    }
    else if (type == "ABC") {
      this.values = [
        { label: "", value: 0 },
        { label: "A", value: 1 },
        { label: "B", value: 2 },
        { label: "C", value: 3 },
        { label: "AB", value: 4 },
        { label: "AC", value: 5 },
        { label: "BC", value: 6 },
        { label: "ABC", value: 7 },
      ]
    }
  }
}
