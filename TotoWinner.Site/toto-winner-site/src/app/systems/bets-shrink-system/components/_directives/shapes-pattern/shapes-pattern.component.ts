import { Component, Input, Output, EventEmitter, OnInit, ViewEncapsulation, OnChanges, SimpleChange, OnDestroy } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ConfirmationService } from 'primeng/api';
import { AppConnetorService } from 'src/app/_services';
import { ShapesFromToRangeType, ShapesData, MinMaxData } from 'src/app/model/interfaces/shapes.module'
import { SystemStateStatus } from 'src/app/model/interfaces/shared.module';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../../../state';
import * as appActions from '../../../../../state/app.actions';
import { StateMode, ShrinkState } from 'src/app/state/app';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-shapes-pattern',
  templateUrl: './shapes-pattern.component.html',
  styleUrls: ['./shapes-pattern.component.css'],
  animations: [
    trigger('animation', [
      state('visible', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('void => *', [
        style({ transform: 'translateX(50%)', opacity: 0 }),
        animate('300ms ease-out')
      ]),
      transition('* => void', [
        animate(('250ms ease-in'), style({
          height: 0,
          opacity: 0,
          transform: 'translateX(50%)'
        }))
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class ShapesPatternComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() disabled: boolean;
  @Input() useDropBoxMinMax: boolean;
  @Output() isDirty: EventEmitter<any> = new EventEmitter(true);

  shapesTables: ShapesData[] = [];
  is3Tables: boolean = false;
  shapesTitles_t1: string[][];
  shapesTitles_t2: string[][];
  shapesTitles_t3: string[][];
  isDirtyComponent: boolean = false;
  tablePanelClass: string = '';
  formSettingEnum: number;
  loading: boolean;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;

  constructor(private confirmationService: ConfirmationService,
    private stateManagerService: AppConnetorService,
    private store: Store<fromApp.AppState>) { }
  ngOnDestroy(): void {
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
  }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes && changes.type && changes.type.currentValue) {
      this.init(changes.type.currentValue);
    }
  }
  ngOnInit() {
    this.tablePanelClass = this.useDropBoxMinMax ? '' : 'wn-shapes-panel'; //no affect here
    this.init(this.type);
    var self = this;
    self.loading = true;
    //this.addShapeTable();
    this.addDefaultShapeTable()

    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        if (state.definitions.programData.programGames.length > 0) {
          if (state.definitions.form /*&& self.rangeSettings && self.minMaxSettings*/
            && (state.mode == StateMode.Loading || self.loading)) {
            self.loading = false;
            setTimeout(function () { self.loadValues(state.definitions.form.formSettings) }, 500);
          }
        }
      })
  }
  loadValues(formSettingsState: BetsCalculation.FormSettings) {
    var self = this;
    var shapes: BetsCalculation.ShapesPattern[];
    switch (self.type) {
      case '1X2_PAIR':
        shapes = formSettingsState.shapes2;
        break;
      case '1X2_TRIPLE':
        shapes = formSettingsState.shapes3;
        break;
      case 'ABC_PAIR':
        shapes = formSettingsState.lettersShapes2;
        break;
      case 'ABC_TRIPLE':
        shapes = formSettingsState.lettersShapes3;
        break;
      default:
        console.error("shapes type is undefined");
        break;
    }
    var counter = 0;
    this.shapesTables = [];
    for (let shape of shapes) {
      for (let i = 0; i < shape.combinations.length; i++) {
        if (self.shapesTables[counter] === undefined) {
          self.addShapeTable(shape.minMax.min, shape.minMax.max, shape.isActive, shape.combinations, ShapesFromToRangeType.Default);
        }
      }
      counter++;
    }
  }
  addHiglyUsedShapeTables() {
    this.addShapeTable(null, null, null, null, ShapesFromToRangeType.ZeroZero);
    this.addShapeTable(null, null, null, null, ShapesFromToRangeType.OneOne);
    this.addShapeTable(null, null, null, null, ShapesFromToRangeType.TwoTwo);
    this.addShapeTable(null, null, null, null, ShapesFromToRangeType.ThreePlus);
  }
  getShapeFromValues(type: ShapesFromToRangeType): MinMaxData {
    var cell_from: MinMaxData;
    switch (type) {
      case ShapesFromToRangeType.Default:
        cell_from = { minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: 0, blinkClass: '' };
        break;
      case ShapesFromToRangeType.ZeroZero:
        cell_from = { minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: 0, blinkClass: '' };
        break;
      case ShapesFromToRangeType.OneOne:
        cell_from = { minValue: 1, maxValue: this.is3Tables ? 14 : 15, currentValue: 1, blinkClass: '' };
        break;
      case ShapesFromToRangeType.TwoTwo:
        cell_from = { minValue: 2, maxValue: this.is3Tables ? 14 : 15, currentValue: 2, blinkClass: '' };
        break;
      case ShapesFromToRangeType.ThreePlus:
        cell_from = { minValue: 3, maxValue: this.is3Tables ? 14 : 15, currentValue: 3, blinkClass: '' };
        break;
    }
    return cell_from;
  }
  getShapeToValues(type: ShapesFromToRangeType): MinMaxData {
    var cell_to: MinMaxData;
    switch (type) {
      case ShapesFromToRangeType.Default:
        cell_to = { minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: this.is3Tables ? 14 : 15, blinkClass: '' };
        break;
      case ShapesFromToRangeType.ZeroZero:
        cell_to = { minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: 0, blinkClass: '' };
        break;
      case ShapesFromToRangeType.OneOne:
        cell_to = { minValue: 1, maxValue: this.is3Tables ? 14 : 15, currentValue: 1, blinkClass: '' };
        break;
      case ShapesFromToRangeType.TwoTwo:
        cell_to = { minValue: 2, maxValue: this.is3Tables ? 14 : 15, currentValue: 2, blinkClass: '' };
        break;
      case ShapesFromToRangeType.ThreePlus:
        cell_to = { minValue: 3, maxValue: this.is3Tables ? 14 : 15, currentValue: this.is3Tables ? 14 : 15, blinkClass: '' };
        break;
    }
    return cell_to;
  }
  addDefaultShapeTable() {
    this.addShapeTable(null, null, null, null, ShapesFromToRangeType.Default);
  }
  addShapeTable(shapeMin: number = null, shapeMax: number = null,
    shapeSelected: boolean = null,
    combinations: BetsCalculation.Combination[] = null,
    shapesType: ShapesFromToRangeType): void {
    var defaultMax = this.is3Tables ? 27 : this.shapesTitles_t1.length;
    var shape: ShapesData = {
      index: this.shapesTables.length,
      selected: shapeSelected != null ? shapeSelected : true,
      shapes_t1: [],
      fromMin: 0,
      toMax: defaultMax,
      currentFrom: shapeMin ? shapeMin : 0,
      currentTo: shapeMax ? shapeMax : defaultMax
    };
    // var cell_from: ShapesModule.MinMaxData = { minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: 0, blinkClass: '' };
    // var cell_to: ShapesModule.MinMaxData = { minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: this.is3Tables ? 14 : 15, blinkClass: '' };
    var cell_from: MinMaxData = this.getShapeFromValues(shapesType);//{ minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: 0, blinkClass: '' };
    var cell_to: MinMaxData = this.getShapeToValues(shapesType);//{ minValue: 0, maxValue: this.is3Tables ? 14 : 15, currentValue: this.is3Tables ? 14 : 15, blinkClass: '' };
    shape.shapes_t1 = [
      { rowIndex: 0, dataValues: [] },
      { rowIndex: 1, dataValues: [] },
    ]
    if (!this.is3Tables) {
      for (let i = 0; i < this.shapesTitles_t1.length; i++) {
        var cellFrom = { ...cell_from };
        var cellTo = { ...cell_to };
        if (combinations) {
          cellFrom.currentValue = combinations[i] ? combinations[i].min : cellFrom.currentValue;
          cellTo.currentValue = combinations[i] ? combinations[i].max : cellTo.currentValue;
        }
        shape.shapes_t1[0].dataValues.push(cellFrom);
        shape.shapes_t1[1].dataValues.push(cellTo);
      }
    }
    else {
      shape.shapes_t2 = [
        { rowIndex: 0, dataValues: [] },
        { rowIndex: 1, dataValues: [] },
      ]
      shape.shapes_t3 = [
        { rowIndex: 0, dataValues: [] },
        { rowIndex: 1, dataValues: [] },
      ]
      for (let i = 0; i < 9; i++) {
        var cellFrom_t1 = { ...cell_from };
        var cellTo_t1 = { ...cell_to };
        if (combinations) {
          cellFrom_t1.currentValue = combinations[i] ? combinations[i].min : cellFrom_t1.currentValue;
          cellTo_t1.currentValue = combinations[i] ? combinations[i].max : cellTo_t1.currentValue;
        }
        shape.shapes_t1[0].dataValues.push(cellFrom_t1);
        shape.shapes_t1[1].dataValues.push(cellTo_t1);

        var cellFrom_t2 = { ...cell_from };
        var cellTo_t2 = { ...cell_to };
        if (combinations) {
          cellFrom_t2.currentValue = combinations[i + 9] ? combinations[i + 9].min : cellFrom_t2.currentValue;
          cellTo_t2.currentValue = combinations[i + 9] ? combinations[i + 9].max : cellTo_t2.currentValue;
        }
        shape.shapes_t2[0].dataValues.push(cellFrom_t2);
        shape.shapes_t2[1].dataValues.push(cellTo_t2);

        var cellFrom_t3 = { ...cell_from };
        var cellTo_t3 = { ...cell_to };
        if (combinations) {
          cellFrom_t3.currentValue = combinations[i + 18] ? combinations[i + 18].min : cellFrom_t3.currentValue;
          cellTo_t3.currentValue = combinations[i + 18] ? combinations[i + 18].max : cellTo_t3.currentValue;
        }
        shape.shapes_t3[0].dataValues.push(cellFrom_t3);
        shape.shapes_t3[1].dataValues.push(cellTo_t3);
      }
      shape.currentTo = shapeMax ? shapeMax : 27;
    }
    this.shapesTables.push(shape);
    if (this.shapesTables.length != 1)
      this.setDirty();

  }
  clearSelection() {
    this.shapesTables = [];
    //this.addShapeTable();
    this.addDefaultShapeTable()
    this.isDirtyComponent = false;
  }
  deleteShape($event, i: number) {
    this.confirmationService.confirm({
      header: 'מחיקת נתונים',
      message: 'בחרת למחוק טבלה מספר ' + (i + 1) + '. האם למחוק?',
      accept: () => {
        this.shapesTables.splice(i, 1);
      }
    });
  }
  setDirty() {
    if (!this.isDirtyComponent)
      this.isDirty.emit(null);
    this.isDirtyComponent = true;
  }
  onChangeTableSpinner(tableIndex: number) {
    var length = this.is3Tables ? 27 : this.shapesTitles_t1.length;
    var from = this.shapesTables[tableIndex].currentFrom;
    if (from > length)
      this.shapesTables[tableIndex].currentFrom = length;
    if (from < 0)
      this.shapesTables[tableIndex].currentFrom = 0;
    var to = this.shapesTables[tableIndex].currentTo;
    console.warn("from=", from, "to=", to, "length=", length);
    if (to > length)
      this.shapesTables[tableIndex].currentTo = length;
    if (to < 0)
      this.shapesTables[tableIndex].currentTo = this.shapesTables[tableIndex].toMax;
    if (from > to)
      this.shapesTables[tableIndex].currentFrom = this.shapesTables[tableIndex].currentTo;
    //console.warn("B- currentFrom=", this.shapesTables[tableIndex].currentFrom, "currentTo=", this.shapesTables[tableIndex].currentTo);
    this.setDirty();
  }
  onChangeCellSpinner_t1($event, iCol, rowData, tableIndex) {
    var selectedRow = rowData.rowIndex;
    this.adjustSpinner(this.shapesTables[tableIndex].shapes_t1, iCol);
  }
  onChangeCellSpinner_t2($event, iCol, rowData, tableIndex) {
    var shapesTable = this.shapesTables[tableIndex].shapes_t2;
    this.adjustSpinner(this.shapesTables[tableIndex].shapes_t2, iCol);
  }
  onChangeCellSpinner_t3($event, iCol, rowData, tableIndex) {
    this.adjustSpinner(this.shapesTables[tableIndex].shapes_t3, iCol);
  }
  adjustSpinner(shapesTable: any, iCol: number): void {
    var from = shapesTable[0].dataValues[iCol].currentValue;
    var to = shapesTable[1].dataValues[iCol].currentValue;
    if (to < from) {
      shapesTable[0].dataValues[iCol].blinkClass = 'blink-class';
      shapesTable[1].dataValues[iCol].blinkClass = 'blink-class';
      setTimeout(function () {
        shapesTable[0].dataValues[iCol].blinkClass = '';
        shapesTable[1].dataValues[iCol].blinkClass = '';
      }, 1000);
    }
    if (to != undefined)
      shapesTable[0].dataValues[iCol].maxValue = to;
    if (from != undefined)
      shapesTable[1].dataValues[iCol].minValue = from;
    this.setDirty();
  }
  buildCombinations(shapeData: ShapesData): BetsCalculation.Combination[] {
    var combinations: BetsCalculation.Combination[] = [];
    for (let i = 0; i < shapeData.shapes_t1[0].dataValues.length; i++) {
      combinations.push({
        min: shapeData.shapes_t1[0].dataValues[i].currentValue,
        max: shapeData.shapes_t1[1].dataValues[i].currentValue
      });
    }
    if (this.is3Tables) {
      for (let i = 0; i < shapeData.shapes_t2[0].dataValues.length; i++) {
        combinations.push({
          min: shapeData.shapes_t2[0].dataValues[i].currentValue,
          max: shapeData.shapes_t2[1].dataValues[i].currentValue
        });
      }
      for (let i = 0; i < shapeData.shapes_t3[0].dataValues.length; i++) {
        combinations.push({
          min: shapeData.shapes_t3[0].dataValues[i].currentValue,
          max: shapeData.shapes_t3[1].dataValues[i].currentValue
        });
      }
    }
    return combinations;
  }
  fillValues(formSettings: BetsCalculation.FormSettings): void {
    var shapesSettings: BetsCalculation.ShapesPattern[] = [];
    for (let shapeData of this.shapesTables) {
      var combinations = this.buildCombinations(shapeData);
      var settings: BetsCalculation.ShapesPattern = {
        combinations: combinations,
        minMax: { min: shapeData.currentFrom, max: shapeData.currentTo },
        isActive: shapeData.selected,
        type: this.formSettingEnum
      }
      shapesSettings.push(settings);
    }
    switch (this.formSettingEnum) {
      case 0:
        formSettings.shapes2 = shapesSettings;
        break;
      case 1:
        formSettings.shapes3 = shapesSettings;
        break;
      case 2:
        formSettings.lettersShapes2 = shapesSettings;
        break;
      case 3:
        formSettings.lettersShapes3 = shapesSettings;
        break;
    }
  }
  init(type: string): void {
    this.is3Tables = this.type == "1X2_TRIPLE" || this.type == "ABC_TRIPLE";
    switch (type) {
      case '1X2_PAIR':
        this.formSettingEnum = 0; /*RegularShapes2*/
        this.shapesTitles_t1 = [
          ["1", "1"],
          ["1", "X"],
          ["1", "2"],
          ["X", "1"],
          ["X", "X"],
          ["X", "2"],
          ["2", "1"],
          ["2", "X"],
          ["2", "2"],
        ]
        break;
      case '1X2_TRIPLE':
        this.formSettingEnum = 1; /*RegularShapes3*/
        this.shapesTitles_t1 = [
          ["1", "1", "1"],
          ["1", "1", "X"],
          ["1", "1", "2"],
          ["1", "X", "1"],
          ["1", "X", "X"],
          ["1", "X", "2"],
          ["1", "2", "1"],
          ["1", "2", "X"],
          ["1", "2", "2"],
        ]
        this.shapesTitles_t2 = [
          ["X", "1", "1"],
          ["X", "1", "X"],
          ["X", "1", "2"],
          ["X", "X", "1"],
          ["X", "X", "X"],
          ["X", "X", "2"],
          ["X", "2", "1"],
          ["X", "2", "X"],
          ["X", "2", "2"],
        ]
        this.shapesTitles_t3 = [
          ["2", "1", "1"],
          ["2", "1", "X"],
          ["2", "1", "2"],
          ["2", "X", "1"],
          ["2", "X", "X"],
          ["2", "X", "2"],
          ["2", "2", "1"],
          ["2", "2", "X"],
          ["2", "2", "2"],
        ]
        break;
      case 'ABC_PAIR':
        this.formSettingEnum = 2; /*LetterShapes2*/
        this.shapesTitles_t1 = [
          ["A", "A"],
          ["A", "B"],
          ["A", "C"],
          ["B", "A"],
          ["B", "B"],
          ["B", "C"],
          ["C", "A"],
          ["C", "B"],
          ["C", "C"],
        ]
        break;
      case 'ABC_TRIPLE':
        this.formSettingEnum = 3; /*LetterShapes3*/
        this.shapesTitles_t1 = [
          ["A", "A", "A"],
          ["A", "A", "B"],
          ["A", "A", "C"],
          ["A", "B", "A"],
          ["A", "B", "B"],
          ["A", "B", "C"],
          ["A", "C", "A"],
          ["A", "C", "B"],
          ["A", "C", "C"],
        ]
        this.shapesTitles_t2 = [
          ["B", "A", "A"],
          ["B", "A", "B"],
          ["B", "A", "C"],
          ["B", "B", "A"],
          ["B", "B", "B"],
          ["B", "B", "C"],
          ["B", "C", "A"],
          ["B", "C", "B"],
          ["B", "C", "C"],
        ]
        this.shapesTitles_t3 = [
          ["C", "A", "A"],
          ["C", "A", "B"],
          ["C", "A", "C"],
          ["C", "B", "A"],
          ["C", "B", "B"],
          ["C", "B", "C"],
          ["C", "C", "A"],
          ["C", "C", "B"],
          ["C", "C", "C"],
        ]
        break;
    }
  }
}
