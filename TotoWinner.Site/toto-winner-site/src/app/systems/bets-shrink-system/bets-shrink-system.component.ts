import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TotoGamesService, AuthenticationService, AppConnetorService, ExcelService } from '../../_services';
import { SelectItem, MessageService, ConfirmationService, MenuItem, DialogService } from 'primeng/api';
import { environment } from '../../../environments/environment';
import { GamesComponent } from './components/games/games.component';
import { FormComponent } from './components/form/form.component';
import { Router } from '@angular/router';
import { OpenSystemsComponent, SaveSystemComponent } from '../../shared/dialogs';
import { ShrinkState, StateMode, ToolbarState, ProgramType, NotificationType } from '../../state/app';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../state';
import * as appActions from '../../state/app.actions';
import { delay, skip } from 'rxjs/operators';
import { ClockSpinnerComponent, ProgramSelectorComponent } from '../../shared/directives';
import { OverlayPanel } from 'primeng/overlaypanel';
import { TemplatesWatcherComponent } from './templates-watcher/templates-watcher.component';
import { TemplateWatcherParameters, TemplatesBaseBetsResponse, TemplateBaseBetsStatus, TemplateWatcherState } from 'src/app/model/interfaces/templates.module';

@Component({
  selector: 'app-bets-shrink-system',
  templateUrl: './bets-shrink-system.component.html',
  styleUrls: ['./bets-shrink-system.component.css']
})
export class BetsShrinkSystemComponent implements OnInit, OnDestroy {
  title = 'toto-winner-site';
  isAdminUser: boolean = false;
  offlineMode: boolean = false;
  totalBetsAfterCalcLabel: string;
  systemMenus: MenuItem[];
  systemName: string = '';
  calculateStarted: boolean = false;
  hasError: boolean = false;
  isFormDirty: boolean = false;
  outputFileUrl: string = '';
  playerFileUrl: string = '';
  calculationResult: ShrinkResultModule.SeResults = null; // seResult
  loading: boolean;
  systemSaved: boolean = false;
  @ViewChild(ClockSpinnerComponent, { static: true }) clockSpinner: ClockSpinnerComponent;
  @ViewChild(GamesComponent, { static: true }) gamesComponent: GamesComponent;
  @ViewChild(FormComponent, { static: true }) formComponent: FormComponent;
  @ViewChild(ProgramSelectorComponent, { static: true }) programSelectorComponent: ProgramSelectorComponent;
  shrinkState$: Observable<ShrinkState>;
  shrinkStateSubscription$: Subscription;
  systemActions$: Observable<appActions.AppActionTypes>;
  systemActionsSubscription$: Subscription;
  templateDataWatcher$: Observable<TemplateWatcherState>;
  templateDataWatcherSubscription$: Subscription;
  state: ShrinkState;
  templateButtonStyle: any = { 'background-color': 'orange' };
  templateButtonLabel : string = "טמפלטים";
  constructor(private router: Router,
    private totoGamesService: TotoGamesService,
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private gatewayService: AppConnetorService,
    private excelService: ExcelService,
    private authenticationService: AuthenticationService,
    private store: Store<fromApp.AppState>) {
  }
  ngOnDestroy(): void {
    if (this.shrinkStateSubscription$)
      this.shrinkStateSubscription$.unsubscribe();
    // if (this.shrinkToolbarStateSubscription$)
    //   this.shrinkToolbarStateSubscription$.unsubscribe();
    if (this.systemActionsSubscription$)
      this.systemActionsSubscription$.unsubscribe();
    if (this.templateDataWatcherSubscription$)
      this.templateDataWatcherSubscription$.unsubscribe();

    this.gatewayService.destroyActionSubject();
  }
  ngOnInit(): void {
    var self = this;
    this.isAdminUser = this.authenticationService.isAdminUser();
    this.shrinkState$ = this.store.pipe(select(fromApp.getShrink));
    this.shrinkStateSubscription$ = this.shrinkState$
      .subscribe(state => {
        self.state = state;
        self.isFormDirty = state.definitions.isFormDirty;
        self.calculationResult = state.result;
        if (self.calculationResult)
          self.totalBetsAfterCalcLabel = `טורים לאחר צימצום: ${this.calculationResult.resultsStatistics.found.toLocaleString()}`;
      })

    this.systemActions$ = this.gatewayService.getSystemActionSubject();
    this.systemActionsSubscription$ = this.systemActions$
      .subscribe(action => {
        if (action == appActions.AppActionTypes.SnapshotShrinkSystem) {
          var formSettings = this.totoGamesService.generateBetsFormCreateRequest(this, false);
          this.store.dispatch(new appActions.SnapshotShrinkSystem(formSettings));
        }
        else if (action == appActions.AppActionTypes.ShrinkSystemClearForm) {
          this.clearFormSelections();
          this.initToolbar();
        }
      })
    //this.templateDataWatcher$ = this.store.pipe(select(fromApp.getTemplateWatcherData));
    this.templateDataWatcher$ = this.store.pipe(select(fromApp.getTemplateWatcherDataWithSelectedType));
    this.templateDataWatcherSubscription$ = this.templateDataWatcher$
      .subscribe(data => {
        if (data && data.collection) {
          var successCounter = 0;
          var invalidTemplateCounter = 0;
          var noBaseCounter = 0;
          var total = 0;
          Object.entries(data.collection).forEach(function (item) {
            total++;
            switch (item[1].status as TemplateBaseBetsStatus) {
              case TemplateBaseBetsStatus.Success:
                successCounter++;
                break;
              case TemplateBaseBetsStatus.InvalidTemplate:
                  invalidTemplateCounter++;
                break;
              case TemplateBaseBetsStatus.NoBaseFound:
                  noBaseCounter++;
                break;
            }
          });
          if (total == 0 || invalidTemplateCounter > 0){
            self.templateButtonStyle = { 'background-color': 'orange' };
          }
          else if (total == noBaseCounter) {
            self.templateButtonStyle = { 'background-color': 'red' };
          }
          else if (successCounter > 0) {
            self.templateButtonStyle = { 'background-color': 'limegreen' };
          }
          else {
            self.templateButtonStyle = { 'background-color': 'orange' };
          }
          self.templateButtonLabel = `${successCounter}/${total} טמפלטים`;
        }
      });
    self.systemSaved = false;
  }
  //#region toolbar methods

  openTemplateWatcher() {
    //debugger;
    //op.toggle($event)
    var self = this;
    const ref = this.dialogService.open(TemplatesWatcherComponent, {
      data: {
        watcherParameters: self.gamesComponent.templateWatcherParameters
      },
      header: 'טמפלטים למעקב',
      width: 'auto',
      height: '90%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .save-system-header",
      contentStyle: { "font-size": "11px" }
    });

  }
  onProgramSelectorChange(){
    this.clearFormSelections();
    this.initToolbar();    
  }
  initToolbar(): void {
    this.calculationResult = null;
    this.totalBetsAfterCalcLabel = '';
    this.systemName = '';
  }

  setDirtyForm(): void {
    this.store.dispatch(new appActions.ShrinkSystemUpdateDirtyFormFlag(true));
  }
  clearFormSelections(): void {
    this.formComponent.clearSelections();
    this.store.dispatch(new appActions.ShrinkSystemUpdateDirtyFormFlag(false));
    this.systemSaved = false;
    //When on init first program does not exists
    if (!this.gamesComponent.programData)
      return;
    var formSettings = this.totoGamesService.generateBetsFormCreateRequest(this, false);
    this.store.dispatch(new appActions.ShrinkSystemClearForm(formSettings));
  }
  refreshData(): void {
    this.initToolbar();
    this.gamesComponent.refreshData();
  }
  onError(state: boolean) {
    this.hasError = state;
    this.systemSaved = false;
  }
  isLongCalculation(form: FormComponent): boolean {
    var longCalc1: boolean = form.pricer.pricerData.betsCounterNumber > 8000 && form.basicSubsets.cleanOnDiff;
    var longCalc2: boolean = form.pricer.pricerData.betsCounterNumber > 500000;
    var longCalc: boolean = longCalc1 || longCalc2;
    return longCalc;
  }
  calculate(games: GamesComponent, form: FormComponent) {
    this.totalBetsAfterCalcLabel = '';
    if (!games.programData.programGames) {
      this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'תוכניה לא נבחרה, נא בחר תוכניה' }));
    }
    else if (form.pricer.pricerData.bankersCounter +
      form.pricer.pricerData.doublesCounter +
      form.pricer.pricerData.trianglesCounter != games.programData.programGames.length) {
      this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Info, message: 'טור לא מלא, נא למלא את כל הטור לפני חישוב' }));
    }
    else {
      if (this.isLongCalculation(form)) {
        this.confirmationService.confirm({
          header: "התראת חישוב",
          message: 'זמן החישוב לצימצום המבוקש יהיה ארוך. האם להמשיך?',
          accept: () => {
            this.sendToCalc();
          }
        });
      }
      else {
        this.sendToCalc();
      }
    }
  }
  sendToCalc() {
    try {
      var formSettings = this.totoGamesService.generateBetsFormCreateRequest(this, false);
      this.store.dispatch(new appActions.ShrinkSystemUpdateForm(formSettings));
      this.calculateStarted = true;
      this.clockSpinner.start();
      this.outputFileUrl = '';
      this.playerFileUrl = '';
      this.totoGamesService.calculateBets(this, false)
        .subscribe(
          (response: any) => {
            this.calculationResult = response.seResults;
            this.store.dispatch(new appActions.ShrinkSystemUpdateCalcResult(this.calculationResult));
            this.totalBetsAfterCalcLabel = `טורים לאחר צימצום: ${this.calculationResult.resultsStatistics.found.toLocaleString()}`;
            this.calculateStarted = false;
            this.clockSpinner.stop();
          },
          (error: any) => {
            this.calculateStarted = false;
            this.calculationResult = null;
            this.clockSpinner.stop();
            this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'ארעה תקלה בחישוב, נסה שנית' }));
          })
    }
    catch (error) {
      this.calculateStarted = false;
      this.clockSpinner.stop();
    }
  }
  resultsToExcel(calculationResult, form) {
    try {
      if (this.calculationResult.resultsStatistics.found > 3000) {
        this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Info, message: 'לא ניתן לייצר טופס, עקב מספר טורים גבוה מידי' }));
      }
      else {
        this.calculateStarted = true;
        this.totoGamesService.BetsResultToExcel(calculationResult, form.basicSubsets.cleanOnDiff)
          .subscribe(
            (response: any) => {
              this.calculateStarted = false;
              if (response.isSuccess) {
                this.outputFileUrl = response.outputFile;
                this.playerFileUrl = response.playerFile;
              }
              else
                console.error(response.message);
            },
            (error: any) => {
              this.calculateStarted = false;
              this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'ארעה תקלה לא צפויה ביצירת טופס' }));
            })
      }
    }
    catch (error) {
      this.calculateStarted = false;
      console.error(error);
    }
  }
}
