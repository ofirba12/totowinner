import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { OptimizerData, OptimizerConditions, StartCalcOptimizerParam, OptimizedFromTo, AdvancedConditionType, OptimizerBets, GameOptimizerData, StopExecutionReason } from '../../model/interfaces/optimizer.module';
import { Observable, Subscription, BehaviorSubject, of } from 'rxjs';
import { delay, tap, skip, concatMap, concat } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { TotoGamesService, ExecutionService, AppConnetorService } from '../../_services';
import { ShrinkSystemData, SystemType, SaveOptimizerSystemRequest, CalculationStatus, ShrinkSystemPayload } from '../../model/interfaces/shared.module';
import { MessageService, DialogService } from 'primeng/api';
import { ConditionsOptimizerHeaderComponent } from './conditions/conditions-optimizer-header/conditions-optimizer-header.component';
import { GamesOptimizerHeaderComponent } from './games/games-optimizer-header/games-optimizer-header.component';
import { ConditionsOptimizerComponent } from './conditions/conditions-optimizer/conditions-optimizer.component';
import { SaveSystemComponent, OpenSystemsComponent } from '../../shared/dialogs';
import { OptimizerService } from '../../_services/optimizer.service';
import { OptimizerState, ShrinkForOptimizerState, StateMode, OptimizerExecutionState, NotificationType, OptimizerExecutionResultsState } from '../../state/app';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../state';
import * as appActions from '../../state/app.actions';
import { ProgramData } from '../../model/interfaces/program.module';
import { SelectorFlags } from '@angular/compiler/src/core';

@Component({
  selector: 'app-optimizer-system',
  templateUrl: './optimizer-system.component.html',
  styleUrls: ['./optimizer-system.component.css']
})
export class OptimizerSystemComponent implements OnInit, OnDestroy {
  optimizerData: OptimizerData;
  optimizedSystemId: number;
  optimizerId: number;
  execution: OptimizerExecutionState;
  polledExecutionData$: Observable<any>; //should be type response
  polling$: BehaviorSubject<any>;
  optimizer$: Observable<any>;
  whenToRefresh$: Observable<any>;
  subscription$: Subscription;
  polledPrice: number = 0;
  //optimizerStatus: CalculationStatus = CalculationStatus.None;
  gamesStatus: CalculationStatus = CalculationStatus.None;;
  conditionsStatus: CalculationStatus = CalculationStatus.None;
  systemName: string = '';
  optimizerProgramUniqeId: string = null; //No need for that, the server will work harder on save system instead
  @ViewChild(ConditionsOptimizerHeaderComponent, { static: true }) conditionToolbar: ConditionsOptimizerHeaderComponent;
  @ViewChild(ConditionsOptimizerComponent, { static: true }) conditionsComponent: ConditionsOptimizerComponent;
  @ViewChild(GamesOptimizerHeaderComponent, { static: true }) gamesToolbar: GamesOptimizerHeaderComponent;

  optimizerState$: Observable<OptimizerState>;
  optimizerStateSubscription$: Subscription;
  shrinkForOptimizerState$: Observable<ShrinkForOptimizerState>;
  shrinkForOptimizerStateSubscription$: Subscription;
  systemActions$: Observable<appActions.AppActionTypes>;
  systemActionsSubscription$: Subscription;
  optimizerExecutionState$: Observable<OptimizerExecutionState>;
  optimizerExecutionStateSubscription$: Subscription;

  constructor(private http: HttpClient,
    private route: ActivatedRoute,
    private totoGamesService: TotoGamesService,
    private executionService: ExecutionService,
    private optimizerService: OptimizerService,
    public dialogService: DialogService,
    private gatewayService: AppConnetorService,
    private store: Store<fromApp.AppState>) { }
  ngOnDestroy(): void {
    if (this.subscription$)
      this.subscription$.unsubscribe();
    if (this.optimizerStateSubscription$)
      this.optimizerStateSubscription$.unsubscribe();
    if (this.shrinkForOptimizerStateSubscription$)
      this.shrinkForOptimizerStateSubscription$.unsubscribe();
    if (this.systemActionsSubscription$)
      this.systemActionsSubscription$.unsubscribe();
    if (this.optimizerExecutionStateSubscription$)
      this.optimizerExecutionStateSubscription$.unsubscribe();
  }
  ngOnInit() {
    var self = this;
    this.gatewayService.registerOptimizer(this);
    this.optimizerState$ = this.store.pipe(select(fromApp.getOptimizer));
    this.optimizerStateSubscription$ = this.optimizerState$
      .subscribe(stateData => {
        if (stateData && stateData.data) {
          if (self.optimizerId != stateData.systemId
            || self.optimizedSystemId != stateData.data.optimizedSystemId) {
            console.log("open system from state", stateData);
            self.optimizerId = stateData.systemId;
            self.optimizedSystemId = stateData.data.optimizedSystemId;
            this.loadValues(stateData.data, stateData.mode);
          }
        }
      });
    this.shrinkForOptimizerState$ = this.store.pipe(select(fromApp.getShrinkForOptimizer));
    this.shrinkForOptimizerStateSubscription$ = this.shrinkForOptimizerState$
      .subscribe(stateData => {
        if (stateData && stateData.shrinkPayload != null) {
          console.log("open shrink for optimizer system from state", stateData);
          if (stateData.shrinkPayload.result == null)
            this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'מערכת צימצומים אינה מכילה תוצאות' }));
          else {
            this.optimizedSystemId = stateData.shrinkSystemId;
            self.optimizerId = undefined;
            this.fillData(stateData.shrinkPayload);
          }
        }
      })
    this.systemActions$ = this.gatewayService.getSystemActionSubject();
    this.systemActionsSubscription$ = this.systemActions$
      .subscribe(action => {
        if (action == appActions.AppActionTypes.SnapshotOptimizerSystem) {
          this.store.dispatch(new appActions.SnapshotOptimizerSystem({
            name: this.systemName,
            systemType: SystemType.Optimizer,
            uniqueId: this.optimizerProgramUniqeId,
            optimizerData: JSON.stringify(this.optimizerData),
            optimizedSystemId: this.optimizedSystemId,
            execution: this.execution
            //            lastExecutionId: this.executionId
          }));
        }
      })
    this.optimizerExecutionState$ = this.store.pipe(select(fromApp.getOptimizerExecution));
    this.optimizerExecutionStateSubscription$ = this.optimizerExecutionState$
      .subscribe(exState => {
        if (exState) {
          self.execution = exState;
        }
      })
  }
  loadValues(systemData: SaveOptimizerSystemRequest, stateMode: StateMode) {
    this.systemName = systemData.name;
    this.optimizedSystemId = systemData.optimizedSystemId;
    this.optimizerData = JSON.parse(systemData.optimizerData);
    this.execution = systemData.execution;
    if (systemData.execution) {
      if (systemData.execution.results) {
        this.gamesToolbar.optimizedParentBetsCounter = systemData.execution.results.parentBetsCounter;
        this.gamesToolbar.optimizedShrinkedBetsCounter = systemData.execution.results.shrinkedBetsCounter;
      }
      if (systemData.execution.parameters) {
        this.conditionToolbar.targetBetsCounter = systemData.execution.parameters.targetBetsCounter;
        this.conditionToolbar.targetConditionCounter = systemData.execution.parameters.targetConditionCounter;
      }
    }
    if (stateMode == StateMode.Loading) {
      this.conditionToolbar.calcStatus = CalculationStatus.None;
      this.conditionToolbar.outputFileUrl = null;
      this.conditionToolbar.playerFileUrl = null;
    }
  }
  startOptimizerAction(executionResponse: any) { //TODO interface
    this.execution.executionId = executionResponse.executionId;
    this.optimizer$ = this.executionService.getExecutionState(executionResponse.executionId);
//    this.optimizerStatus = CalculationStatus.Queue;
    this.execution.status = CalculationStatus.Queue; 
    this.polling$ = new BehaviorSubject('');
    //? Create a stream using the static of. This will fire an event immediately when subscribed to.
    //? Delay this event with 5000ms by using the delay operator.
    //? Use a tap where we can actually trigger the next request,
    //? Finally skip since we do not want to use the '' event anywhere, it was just a trigger.
    this.whenToRefresh$ = of('').pipe(
      delay(5000),
      tap(_ => {
        if (!this.polling$.isStopped)
          this.polling$.next('')
      }),
      skip(1),
    );
    this.polledExecutionData$ = this.polling$.pipe(
      concatMap(_ => this.optimizer$.pipe(concat(this.whenToRefresh$))),
    );
    this.subscription$ = this.polledExecutionData$.subscribe(execution => {
      this.executionService.sendExecutionData(execution);
      console.log("execution data", execution);
      if (execution && execution.isSuccess) {
        if (execution.state.status == 'Running') {
          this.execution.status  = CalculationStatus.Running;
          if (execution.state.results.conditions || execution.state.results.advancedConditions) {
            this.conditionsStatus = CalculationStatus.Running;
            this.gamesStatus = CalculationStatus.None;
          }
          else {
            this.conditionsStatus = CalculationStatus.None;
            if (execution.state.results.bets)
              this.gamesStatus = CalculationStatus.Running;
            else
              this.gamesStatus = CalculationStatus.None;
          }
        }
        else if (execution.state.status == 'Error') {
          this.conditionsStatus = CalculationStatus.Error;
          this.gamesStatus = CalculationStatus.Error;
        }
        else if (execution.state.status == 'Finished') {
          this.store.dispatch(new appActions.UpdateOptimizerExecutionResult({
            execution: this.execution,
            parentBetsCounter: execution.state.results.parentBetsCounter,
            shrinkedBetsCounter: execution.state.results.shrinkedBetsCounter
          }));
        }
      }
    });

  }
  resetBets(): void {
    for (let game of this.optimizerData.bets.gamesData) {
      game.bet1X2String_optimized = game.bet1X2String_original;
      game.calcProgress = "started";
      game.finalBet = false;
    }
    this.conditionsComponent.resetFields();
  }

  startOptimizer(params: StartCalcOptimizerParam) {
    if (this.execution && this.execution.executionId != 0) //If previous execution exists
      this.resetBets()
    if (this.conditionToolbar.targetBetsCounter == undefined ||
      this.conditionToolbar.targetBetsCounter < this.gamesToolbar.originalShrinkedBetsCounter) {
      this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Info, message: `לא ניתן להתחיל ריצה, אנא ודא שמספר טורים רצוי גדול מ - ${this.gamesToolbar.originalShrinkedBetsCounter}` }));
      this.conditionToolbar.clockSpinner.stop();
      this.conditionToolbar.setItemsByStatus(CalculationStatus.Stopped);
    }
    else {
      this.store.dispatch(new appActions.StartOptimizerExecution({ optimizedSystemId: this.optimizedSystemId, params: params }));
      this.executionService.startOptimizer(this.optimizedSystemId, params).subscribe(executionResponse => {
        this.conditionToolbar.setItemsByStatus(CalculationStatus.Running);
        this.startOptimizerAction(executionResponse);
      },
        (error: any) => {
          this.execution.executionId = 0;
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Info, message: 'לא ניתן להתחיל ריצה, אנא ודא שנתוני ריצה מלאים' }));
          this.conditionToolbar.setItemsByStatus(CalculationStatus.Error);
          this.conditionToolbar.clockSpinner.stop();
        });
    }
  }

  stopOptimizer($stopReason: StopExecutionReason) {
    if ($stopReason.manualStop == true && this.execution.executionId != 0 && this.execution.status != CalculationStatus.Finished) {
      this.executionService.stopOptimizer(this.execution.executionId).subscribe(executionResponse => {
        if (executionResponse.isSuccess) {
          if (this.subscription$)
            this.subscription$.unsubscribe();
            this.execution.status  = CalculationStatus.Stopped;
        }
        else
          console.warn("Stopping execution error:", executionResponse.errorMessage);
      },
        (error: any) => {
          console.warn("stop optimizer failed:", error)
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: `אירעה תקלה בעצירת ריצה מספר ${this.execution.executionId}` }));
          this.conditionToolbar.clockSpinner.stop();
        });
    }
    else {
      if (this.subscription$) {
        this.subscription$.unsubscribe();
        if ($stopReason.manualStop == false && $stopReason.status == CalculationStatus.Error) {
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: `אירעה שגיאת בריצה מספר ${this.execution.executionId}` }));
        }
      }
      this.execution.status  = CalculationStatus.Finished;
    }
  }

  isGamesOptimizationInProgress(): boolean {
    var res = false;
    res = this.execution && (this.execution.status  == CalculationStatus.Queue ||
      (this.execution.status == CalculationStatus.Running && this.gamesStatus == CalculationStatus.Running))
    return res;
  }
  isConditionsOptimizationInProgress(): boolean {
    var res = false;
    res = this.execution && (this.execution.status == CalculationStatus.Running && this.conditionsStatus == CalculationStatus.Running)
    return res;
  }

  generateConditions(data: ShrinkSystemPayload): OptimizerConditions {
    var settings1X2 = data.definitions.form.formSettings.includeSettings;
    var settingsABC = data.definitions.form.formSettings.combinationLettersSettings;
    var cons: OptimizerConditions = {
      amount1X2: [],
      sequence1X2: [],
      breaks1X2: [],
      amountABC: [],
      sequenceABC: [],
      breaksABC: [],
      range: [],
      advancedObjects: [],
      optimizationStatus: CalculationStatus.None
    };
    this.fillConditionObject("1", settings1X2.amount1, cons.amount1X2);
    this.fillConditionObject("X", settings1X2.amountX, cons.amount1X2);
    this.fillConditionObject("2", settings1X2.amount2, cons.amount1X2);
    this.fillConditionObject("1", settings1X2.sequence1, cons.sequence1X2);
    this.fillConditionObject("X", settings1X2.sequenceX, cons.sequence1X2);
    this.fillConditionObject("2", settings1X2.sequence2, cons.sequence1X2);
    this.fillConditionObject("", settings1X2.sequenceBreak, cons.breaks1X2);
    this.fillConditionObject("A", settingsABC.minMaxA, cons.amountABC);
    this.fillConditionObject("B", settingsABC.minMaxB, cons.amountABC);
    this.fillConditionObject("C", settingsABC.minMaxC, cons.amountABC);
    this.fillConditionObject("A", settingsABC.minMaxSequenceLengthA, cons.sequenceABC);
    this.fillConditionObject("B", settingsABC.minMaxSequenceLengthB, cons.sequenceABC);
    this.fillConditionObject("C", settingsABC.minMaxSequenceLengthC, cons.sequenceABC);
    this.fillConditionObject("", settingsABC.minMaxBreak, cons.breaksABC);
    this.fillConditionObject("", settings1X2.amountSum, cons.range);
    this.fillConditionAdvanceObjects(AdvancedConditionType.Single1X2, data.definitions.form.formSettings.combination1X2Settings, cons);
    this.fillConditionAdvanceObjects(AdvancedConditionType.SingleABC, data.definitions.form.formSettings.combinationLettersSettings.lettersSettings, cons);
    this.fillConditionAdvanceObjects(AdvancedConditionType.Pair1X2, data.definitions.form.formSettings.shapes2, cons);
    this.fillConditionAdvanceObjects(AdvancedConditionType.PairABC, data.definitions.form.formSettings.lettersShapes2, cons);
    this.fillConditionAdvanceObjects(AdvancedConditionType.Triplet1X2, data.definitions.form.formSettings.shapes3, cons);
    this.fillConditionAdvanceObjects(AdvancedConditionType.TripletABC, data.definitions.form.formSettings.lettersShapes3, cons);
    return cons;
  }
  generateBets(data: ShrinkSystemPayload): OptimizerBets {
    var gamesData: GameOptimizerData[] = [];
    for (let game of data.definitions.programData.programGames) {
      gamesData.push({
        index: game.gameIndex,
        name: game.gameName,
        rate_1: data.definitions.form.eventsRates[game.gameIndex - 1].rate1,
        rate_X: data.definitions.form.eventsRates[game.gameIndex - 1].rateX,
        rate_2: data.definitions.form.eventsRates[game.gameIndex - 1].rate2,
        ui_rates: null,
        bet1X2String_original: game.bet1X2String,
        bet1X2String_optimized: game.bet1X2String,
        calcProgress: "started",
        finalBet: false
      });
    }
    var bets: OptimizerBets = {
      optimizationStatus: CalculationStatus.None,
      gamesData: gamesData
    }
    return bets;
  }
  fillConditionObject(title: string, item: BetsCalculation.MinMax, objects: OptimizedFromTo[]) {
    objects.push({
      objectName: title,
      original: { from: { item: item.min, class: "" }, to: { item: item.max, class: "" } },
      optimized: { from: { item: item.min, class: "" }, to: { item: item.max, class: "" } }
    }
    );
  }
  fillConditionAdvanceObjects(conditionType: AdvancedConditionType,
    objects: any, conditions: OptimizerConditions) {
    var indexInCollection = 1;
    for (let obj of objects) {
      var titleType: string = '';
      var titleShape: string = '';
      switch (conditionType) {
        case AdvancedConditionType.Single1X2:
          titleType = "1X2";
          titleShape = "טור";
          break;
        case AdvancedConditionType.SingleABC:
          titleType = "ABC";
          titleShape = "טור";
          break;
        case AdvancedConditionType.Pair1X2:
          titleType = "1X2";
          titleShape = "זוגות";
          break;
        case AdvancedConditionType.PairABC:
          titleType = "ABC";
          titleShape = "זוגות";
          break;
        case AdvancedConditionType.Triplet1X2:
          titleType = "1X2";
          titleShape = "שלשות";
          break;
        case AdvancedConditionType.TripletABC:
          titleType = "ABC";
          titleShape = "שלשות";
          break;
      }
      conditions.advancedObjects.push({
        objectName: '',
        key: { index: indexInCollection, type: conditionType, titleShape: titleShape, titleType: titleType },
        original: { from: { item: obj.minMax.min, class: "" }, to: { item: obj.minMax.max, class: "" } },
        optimized: { from: { item: obj.minMax.min, class: "" }, to: { item: obj.minMax.max, class: "" } }
      }
      );
      indexInCollection++;
    }
  }
  calculateBetsCounter(gamesData: ProgramData): number {
    var betsCounter: number = 0;
    var trianglesCounter: number = 0;
    var doublesCounter: number = 0;
    for (let game of gamesData.programGames) {
      if (game.bet1X2String.length == 2)
        doublesCounter++;
      if (game.bet1X2String.length == 3)
        trianglesCounter++;
    }
    betsCounter = Math.max(1, Math.pow(3, trianglesCounter)) * Math.max(1, Math.pow(2, doublesCounter));
    return betsCounter;
  }
  fillData(data: ShrinkSystemPayload) {
    var sysConditions = this.generateConditions(data);
    var sysBets = this.generateBets(data);
    this.optimizerData = {
      programTitle: data.definitions.programData.programTitle,// data.state.programTitle,
      optimizedParentBetsCounter: 0,
      optimizedShrinkedBetsCounter: 0,
      originalParentBetsCounter: this.calculateBetsCounter(data.definitions.programData), //data.state.betsCounter,
      originalShrinkedBetsCounter: data.result.resultsStatistics.found,// data.state.shrinkedBetsCounter,
      bets: sysBets,
      conditions: sysConditions,
      shrinkSystemName: data.name
    };
    this.optimizerProgramUniqeId = data.uniqueId;
  }
  resultToExcel($event) {
    this.executionService.optimizerResultToExcel(this.execution.executionId, this.optimizedSystemId)
      .subscribe((response: any) => {
        if (response.isSuccess == true){
          this.conditionToolbar.outputFileUrl = response.outputFile;
          this.conditionToolbar.playerFileUrl = response.playerFile;
        }
        else {
          console.error(response.message);
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'ארעה תקלה לא צפויה ביצירת טופס אוטימיזציה' }));
        }
      },
        (error: any) => {
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Error, message: 'ארעה תקלה לא צפויה ביצירת טופס אוטימיזציה!' }));
        });
  }
}
