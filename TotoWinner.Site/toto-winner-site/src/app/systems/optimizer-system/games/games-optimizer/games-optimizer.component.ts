import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { OptimizerData} from '../../../../model/interfaces/optimizer.module';
import { ExecutionService } from 'src/app/_services';
import { CalculationStatus } from 'src/app/model/interfaces/shared.module';
@Component({
  selector: 'app-games-optimizer',
  templateUrl: './games-optimizer.component.html',
  styleUrls: ['./games-optimizer.component.css']
})
export class GamesOptimizerComponent implements OnInit, OnChanges {
  @Input() optimizerData: OptimizerData;
  constructor(private executionService: ExecutionService) { }

  ngOnInit() {
    this.executionService.getExecutionData().subscribe(execution => {
      if (this.optimizerData) {
        if (execution && execution.isSuccess) {
          switch(execution.state.status) {
            case 'Finished':
              this.optimizerData.bets.optimizationStatus = CalculationStatus.Finished;
            case 'Running':
            if (execution.state.results.bets)
              this.updateBets(execution.state.results.bets);
              break;
          }
        }
      }
    });
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes && changes.optimizerData && changes.optimizerData.currentValue) {
      this.prepareForUI();
    }
  }
  updateBets(bets: any) { // TODO Move to interface
    var self = this;
    this.optimizerData.bets.optimizationStatus = CalculationStatus.Running;
    for (let optimizedBets of bets) {
      var currentBet = this.optimizerData.bets.gamesData[optimizedBets.gameIndex - 1];
      if (currentBet.bet1X2String_optimized != optimizedBets.bet) {
        currentBet.bet1X2String_optimized = optimizedBets.bet;
        currentBet.calcProgress = "ValueChanged"; //TODO Move to enum
      }
      if (optimizedBets.isFinalBet)
        currentBet.finalBet = true;
    }
  }
  prepareForUI() {
    for (let game of this.optimizerData.bets.gamesData) {
      game.ui_rates = this.createRatesItems(game.rate_1, game.rate_X, game.rate_2);
    }
  }
  createRatesItems(rate1: number, rateX: number, rate2: number): SelectItem[] {
    let betsArr: SelectItem[] = [
      { label: (Math.round(rate1 * 100) / 100).toFixed(2).toString(), value: 1, disabled: true, styleClass: "" },
      { label: (Math.round(rateX * 100) / 100).toFixed(2).toString(), value: 2, disabled: true, styleClass: "" },
      { label: (Math.round(rate2 * 100) / 100).toFixed(2).toString(), value: 3, disabled: true, styleClass: "" }];
    return betsArr;
  }

}
