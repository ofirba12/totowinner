import { Component, OnInit, Input, OnChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { OptimizerData } from 'src/app/model/interfaces/optimizer.module';
import { ExecutionService } from 'src/app/_services';

@Component({
  selector: 'games-optimizer-header',
  templateUrl: './games-optimizer-header.component.html',
  styleUrls: ['./games-optimizer-header.component.css']
})
export class GamesOptimizerHeaderComponent implements OnInit, OnChanges {
  @Input() optimizerData: OptimizerData;
  @Output() onOpenSystem: EventEmitter<any> = new EventEmitter(null);
  @Output() onSaveSystem: EventEmitter<any> = new EventEmitter(null);
  originalParentBetsCounter: number;
  optimizedParentBetsCounter: number;
  originalShrinkedBetsCounter: number;
  optimizedShrinkedBetsCounter: number;
  optimizedSystemName: string = ''; //should be located in a top tool bar
  constructor(private executionService: ExecutionService) { }

  ngOnInit() {
    this.executionService.getExecutionData().subscribe(execution => {
      if (execution && execution.isSuccess && execution.state && execution.state.results) {
        this.updateOptimizedBets(execution.state.results);
      }
    });
  }
  updateOptimizedBets(state: any): void { //TODO: Move to interface
    if (this.optimizerData) {
      this.optimizerData.optimizedParentBetsCounter = state.parentBetsCounter;
      this.optimizerData.optimizedShrinkedBetsCounter = state.shrinkedBetsCounter;
      this.optimizedParentBetsCounter = this.optimizerData.optimizedParentBetsCounter;
      this.optimizedShrinkedBetsCounter = this.optimizerData.optimizedShrinkedBetsCounter;
    }
  }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes && changes.optimizerData && changes.optimizerData.currentValue) {
      this.prepareForUI();
    }
  }
  prepareForUI() {
    this.originalParentBetsCounter = this.optimizerData.originalParentBetsCounter;
    this.originalShrinkedBetsCounter = this.optimizerData.originalShrinkedBetsCounter;
    this.optimizedParentBetsCounter = this.optimizerData.optimizedParentBetsCounter;
    this.optimizedShrinkedBetsCounter = this.optimizerData.optimizedShrinkedBetsCounter;
  }
  onOpen(){
    this.onOpenSystem.emit();
  }
  onSave(){
    this.onSaveSystem.emit();
  }
}
