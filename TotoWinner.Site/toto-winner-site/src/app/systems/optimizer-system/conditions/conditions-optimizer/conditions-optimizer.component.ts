import { Component, OnInit, Input, OnChanges, SimpleChange, ViewChildren, QueryList } from '@angular/core';
import { OptimizerData, FromToItem, OptimizedFromTo, AdvancedConditionType } from 'src/app/model/interfaces/optimizer.module';
import { ExecutionService } from 'src/app/_services';
import { ConditionsTableComponent } from 'src/app/systems/bets-shrink-system/components/_directives/conditions-table/conditions-table.component';

@Component({
  selector: 'app-conditions-optimizer',
  templateUrl: './conditions-optimizer.component.html',
  styleUrls: ['./conditions-optimizer.component.css']
})
export class ConditionsOptimizerComponent implements OnInit {
  @Input() optimizerData: OptimizerData;
  amount1X2_cols: any[];
  sequence1X2_cols: any[];
  breaks1X2_cols: any[];
  amountABC_cols: any[];
  sequenceABC_cols: any[];
  breaksABC_cols: any[];
  range_cols: any[];
  advancedObjects_cols: any[];
  @ViewChildren(ConditionsTableComponent) conditionsSettings: QueryList<ConditionsTableComponent>;
  
  constructor(private executionService: ExecutionService) { }

  initTitles() {
    this.amount1X2_cols = [
      { field: 't1', header: 'כמה יצא 1X2' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.sequence1X2_cols = [
      { field: 't1', header: ' רצף 1X2' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.breaks1X2_cols = [
      { field: 't1', header: 'שבירות 1X2' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.amountABC_cols = [
      { field: 't1', header: 'כמה יצא ABC' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.sequenceABC_cols = [
      { field: 't1', header: ' רצף ABC' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.breaksABC_cols = [
      { field: 't1', header: 'שבירות ABC' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.range_cols = [
      { field: 't1', header: 'טווח יחסים' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
    this.advancedObjects_cols = [
      { field: 't1', header: '' },
      { field: 't2', header: 'מקור' },
      { field: 't3', header: 'אחרי' }
    ];
  }
  ngOnInit() {
    this.initTitles();
    this.executionService.getExecutionData().subscribe(execution => {
      if (execution && execution.isSuccess) {
        switch (execution.state.status) {
          case 'Finished':
          case 'Running':
            if (execution.state.results.conditions)
              this.setSimpleConditionCells(execution.state.results.conditions);
            if (execution.state.results.advancedConditions)
              this.setAdvancedConditionCells(execution.state.results.advancedConditions);
            break;
        }
      }
    });

  }

  resetFields() : void{
    for (let condition of this.conditionsSettings.toArray()) {
      condition.resetFields();
    }
  }
  setCell(cell: FromToItem, newValue: number): void {
    if (cell.item != newValue) {
      cell.item = newValue;
      cell.class = 'blink-class';
      setTimeout(function () {
        cell.class = 'changed-class';
      }, 2000);
    }
  }

  setSimpleConditionCells(simpleConditions: any) {
    if (this.optimizerData === undefined)
      return;
    for (let con of simpleConditions) {
      var cellCondition: any;
      var index: number = 0;
      switch (con.type) {
        case 'Amount1X2':
          cellCondition = this.optimizerData.conditions.amount1X2;
          break;
        case 'Sequence1X2':
          cellCondition = this.optimizerData.conditions.sequence1X2;
          break;
        case 'Breaks1X2':
          cellCondition = this.optimizerData.conditions.breaks1X2;
          break;
        case 'AmountABC':
          cellCondition = this.optimizerData.conditions.amountABC;
          break;
        case 'SequenceABC':
          cellCondition = this.optimizerData.conditions.sequenceABC;
          break;
        case 'BreaksABC':
          cellCondition = this.optimizerData.conditions.breaksABC;
          break;
        case 'Range':
          cellCondition = this.optimizerData.conditions.range;
          break;
      }
      switch (con.title) {
        case '1':
        case 'A':
          index = 0;
          break;
        case 'X':
        case 'B':
          index = 1;
          break;
        case '2':
        case 'C':
          index = 2;
          break;
      }
      this.setCell(cellCondition[index].optimized.from, con.condition.min);
      this.setCell(cellCondition[index].optimized.to, con.condition.max);
    }
  }
  setAdvancedConditionCells(advancedConditions: any) {// TODO Move to interface instead of any
    for (let shape of advancedConditions) {
      try {
        var shapeIndex = shape.indexPerType;
        var cell: OptimizedFromTo;
        switch (shape.type) {
          case 'Single1X2':
            cell = this.optimizerData.conditions.advancedObjects
              .find(con => con.key.type == AdvancedConditionType.Single1X2 && con.key.index == shapeIndex);
            break;
          case 'SingleABC':
            cell = this.optimizerData.conditions.advancedObjects
              .find(con => con.key.type == AdvancedConditionType.SingleABC && con.key.index == shapeIndex);
            break;
          case 'PairShape1X2':
            cell = this.optimizerData.conditions.advancedObjects
              .find(con => con.key.type == AdvancedConditionType.Pair1X2 && con.key.index == shapeIndex);
            break;
          case 'PairShapeABC':
            cell = this.optimizerData.conditions.advancedObjects
              .find(con => con.key.type == AdvancedConditionType.PairABC && con.key.index == shapeIndex);
            break;
          case 'TripletShape1X2':
            cell = this.optimizerData.conditions.advancedObjects
              .find(con => con.key.type == AdvancedConditionType.Triplet1X2 && con.key.index == shapeIndex);
            break;
          case 'TripletShapeABC':
            cell = this.optimizerData.conditions.advancedObjects
              .find(con => con.key.type == AdvancedConditionType.TripletABC && con.key.index == shapeIndex);
            break;
        }
        this.setCell(cell.optimized.from, shape.condition.min);
        this.setCell(cell.optimized.to, shape.condition.max);
      }
      catch (ex) {
        console.warn(`an error occurred in advanced object of type ${shape.type}[${shapeIndex}]`)
      }
    }
  }
}
