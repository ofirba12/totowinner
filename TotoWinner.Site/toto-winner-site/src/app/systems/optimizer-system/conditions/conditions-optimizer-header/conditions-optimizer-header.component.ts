import { Component, OnInit, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { StartCalcOptimizerParam, StopExecutionReason } from 'src/app/model/interfaces/optimizer.module';
import { environment } from 'src/environments/environment';
import { ExecutionService } from 'src/app/_services';
import { Subscription, Observable } from 'rxjs';
import { CalculationStatus } from 'src/app/model/interfaces/shared.module';
import { Store, select } from '@ngrx/store';
import * as fromApp from '../../../../state';
import * as appActions from '../../../../state/app.actions';
import { OptimizerExecutionState } from 'src/app/state/app';
import { ClockSpinnerComponent } from 'src/app/shared/directives';
@Component({
  selector: 'conditions-optimizer-header',
  templateUrl: './conditions-optimizer-header.component.html',
  styleUrls: ['./conditions-optimizer-header.component.css']
})
export class ConditionsOptimizerHeaderComponent implements OnInit, OnDestroy {
  @Output() onStart: EventEmitter<StartCalcOptimizerParam> = new EventEmitter(null);
  @Output() onStop: EventEmitter<StopExecutionReason> = new EventEmitter(null);
  @Output() onResultToExcel: EventEmitter<any> = new EventEmitter(null);
  targetBetsCounter: number;
  targetConditionCounter: number;
  offlineMode: boolean = false;
  calcStatus: CalculationStatus = CalculationStatus.None;
  executionButtonLabel: string = "הפעל";
  @ViewChild(ClockSpinnerComponent, { static: true }) clockSpinner: ClockSpinnerComponent;
  subscription$: Subscription;
  loading: boolean;
  outputFileUrl: string = null;
  playerFileUrl: string = null;
  optimizerExecutionState$: Observable<OptimizerExecutionState>;
  optimizerExecutionStateSubscription$: Subscription;
  constructor(private executionService: ExecutionService,
    private store: Store<fromApp.AppState>) { }

  ngOnDestroy(): void {
    if (this.subscription$)
      this.subscription$.unsubscribe();
    if (this.optimizerExecutionStateSubscription$)
      this.optimizerExecutionStateSubscription$.unsubscribe();
  }
  ngOnInit() {
    var self = this;
    this.optimizerExecutionState$ = this.store.pipe(select(fromApp.getOptimizerExecution));
    this.optimizerExecutionStateSubscription$ = this.optimizerExecutionState$
      .subscribe(state => {
        if (state && state.parameters) {
          self.targetBetsCounter = state.parameters.targetBetsCounter;
          self.targetConditionCounter = state.parameters.targetConditionCounter;
          if (state.executionId != 0)
            self.calcStatus = CalculationStatus.Finished;
        }
        else {
          self.targetBetsCounter  = null;
          self.targetConditionCounter = null;
          self.outputFileUrl = null;
          self.playerFileUrl = null;
          self.calcStatus = CalculationStatus.None;
        }
      })
    this.offlineMode = environment.offline;
    this.calcStatus = CalculationStatus.None;
    this.loading = true;
    this.subscription$ = this.executionService.getExecutionData().subscribe(execution => {
      if (execution && execution.isSuccess) {
        switch (execution.state.status) {
          case 'Finished':
            this.executionButtonLabel = 'הפעל';
            if (self.loading === false) {
              this.onStop.emit({ manualStop: false, status: CalculationStatus.Finished });
              this.clockSpinner.stop();
            }
            this.calcStatus = CalculationStatus.Finished;
            break;
          case 'Running':
            this.outputFileUrl = null;
            this.playerFileUrl = null;
            this.calcStatus = CalculationStatus.Running;
            break;
          case 'Queue':
            this.outputFileUrl = null;
            this.playerFileUrl = null;
            this.calcStatus = CalculationStatus.Queue;
            break;
          case 'Stopped':
            this.outputFileUrl = null;
            this.playerFileUrl = null;
            this.calcStatus = CalculationStatus.Stopped;
          case 'Error':
            this.outputFileUrl = null;
            this.playerFileUrl = null;
            this.calcStatus = CalculationStatus.Error;
            this.startStopOptimizer();
            this.clockSpinner.stop();
            break;
        }
      }
      self.loading = false;
    });
  }

  startStopOptimizer() {
    if (this.calcStatus != CalculationStatus.Queue &&
      this.calcStatus != CalculationStatus.Running &&
      this.calcStatus != CalculationStatus.Error) {
      var startParams: StartCalcOptimizerParam = {
        targetBetsCounter: this.targetBetsCounter,
        targetConditionCounter: this.targetConditionCounter
      };
      this.clockSpinner.start();
      this.setItemsByStatus(CalculationStatus.Queue);
      this.onStart.emit(startParams);
    }
    else if (this.calcStatus === CalculationStatus.Running || this.calcStatus === CalculationStatus.Queue) {
      this.setItemsByStatus(CalculationStatus.Stopped);
      this.onStop.emit({ manualStop: true, status: CalculationStatus.Stopped });
      this.clockSpinner.stop();
    }
    else if (this.calcStatus === CalculationStatus.Error) {
      this.setItemsByStatus(CalculationStatus.Error);
      this.onStop.emit({ manualStop: false, status: CalculationStatus.Error });
      this.clockSpinner.stop();
    }
  }
  setItemsByStatus(status: CalculationStatus): void {
    if (status === CalculationStatus.Stopped) {
      this.calcStatus = CalculationStatus.Stopped;
      this.executionButtonLabel = 'הפעל';
    }
    else if (status === CalculationStatus.Running) {
      this.calcStatus = CalculationStatus.Running;
      this.executionButtonLabel = 'עצור';
    }
    else if (status === CalculationStatus.Queue) {
      this.calcStatus = CalculationStatus.Queue;
      this.executionButtonLabel = 'המתן';
    }
    else if (status === CalculationStatus.Error) {
      this.calcStatus = CalculationStatus.Stopped;
      this.executionButtonLabel = 'הפעל';
    }
  }
  showPrepareButton(): boolean {
    var show = !this.outputFileUrl && this.calcStatus === CalculationStatus.Finished;
    return show;
  }
  resultsToExcel(): void {
    this.onResultToExcel.emit();
  }
}
