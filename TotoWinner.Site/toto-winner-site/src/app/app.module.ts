import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TotoGamesService } from './_services';
import { GamesComponent } from './systems/bets-shrink-system/components/games/games.component';
import { PrimeNgModule } from './primeng/primeng.module';
import { FormComponent } from './systems/bets-shrink-system/components/form/form.component';
import { PricerComponent } from './systems/bets-shrink-system/components/form/pricer/pricer.component';
import { BasicSubsetsComponent } from './systems/bets-shrink-system/components/form/basic-subsets/basic-subsets.component';
import { AdvancedSubsetsComponent } from './systems/bets-shrink-system/components/form/advanced-subsets/advanced-subsets.component';
import { ShapesSubsetsComponent } from './systems/bets-shrink-system/components/form/shapes-subsets/shapes-subsets.component';
import {
  CombinationPatternComponent,
  ShapesPatternComponent
} from './systems/bets-shrink-system/components/_directives'
import { ExcludeComponent } from './systems/bets-shrink-system/components/form/advanced-subsets/exclude/exclude.component';
import { Pattern1x2Component } from './systems/bets-shrink-system/components/form/advanced-subsets/pattern1x2/pattern1x2.component';
import { PatternABCComponent } from './systems/bets-shrink-system/components/form/advanced-subsets/pattern-abc/pattern-abc.component';
import { LoginComponent } from './login/login.component';
import { Routing } from './app.routing';
import { AuthGuard } from './_guards';
import { BetsShrinkSystemComponent } from './systems/bets-shrink-system/bets-shrink-system.component';
import { OptimizerSystemComponent } from './systems/optimizer-system/optimizer-system.component';
import { GamesOptimizerComponent } from './systems/optimizer-system/games/games-optimizer/games-optimizer.component';
import { ConditionsOptimizerComponent } from './systems/optimizer-system/conditions/conditions-optimizer/conditions-optimizer.component';
import { GamesOptimizerHeaderComponent } from './systems/optimizer-system/games/games-optimizer-header/games-optimizer-header.component';
import { ConditionsOptimizerHeaderComponent } from './systems/optimizer-system/conditions/conditions-optimizer-header/conditions-optimizer-header.component';
import { ConditionsTableComponent } from './systems/bets-shrink-system/components/_directives/conditions-table/conditions-table.component';
import { OpenSystemsComponent, SaveSystemComponent, TemplatesManagerComponent, FormDispatcherComponent, SmartDownloadComponent, RealtimeRatesScreenerComponent } from './shared/dialogs/index';
import { StoreModule, combineReducers } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { SystemsLayoutComponent } from './_layouts/systems-layout/systems-layout.component';
import { SystemsHeaderComponent } from './_layouts/systems-header/systems-header.component';
import { reducer, reducers} from './state/app.reducer';
//import { reducerA } from './state/app.reducer';
import { AppEffects } from './state/app.effects';
import { SharedModule } from './shared/shared.module';
import { TemplatesWatcherComponent } from './systems/bets-shrink-system/templates-watcher/templates-watcher.component';
import { BetsTemplateDialogComponent } from './systems/bets-shrink-system/templates-watcher/dialogs/bets-template-dialog/bets-template-dialog.component';
import { ProgramsSyncDialogComponent } from './shared/dialogs/programs-sync-dialog/programs-sync-dialog.component';
import { StatisicalDataComponent } from './shared/dialogs/statisical-data/statisical-data.component';
import { SmartDownloadPageComponent } from './smart-download-page/smart-download-page.component';

@NgModule({
  declarations: [
    AppComponent,
    GamesComponent,
    FormComponent,
    PricerComponent,
    BasicSubsetsComponent,
    AdvancedSubsetsComponent,
    ShapesSubsetsComponent,
    ExcludeComponent,
    Pattern1x2Component,
    PatternABCComponent,
    CombinationPatternComponent,
    ShapesPatternComponent,
    LoginComponent,
    BetsShrinkSystemComponent,
    OptimizerSystemComponent,
    GamesOptimizerComponent,
    ConditionsOptimizerComponent,
    GamesOptimizerHeaderComponent,
    ConditionsOptimizerHeaderComponent,
    ConditionsTableComponent,
    OpenSystemsComponent,
    SaveSystemComponent,
    SystemsLayoutComponent,
    SystemsHeaderComponent,
    TemplatesManagerComponent,
    TemplatesWatcherComponent,
    BetsTemplateDialogComponent,
    ProgramsSyncDialogComponent,
    StatisicalDataComponent,
    FormDispatcherComponent,
    SmartDownloadPageComponent,
    SmartDownloadComponent,
    RealtimeRatesScreenerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    Routing,
    //RouterModule.forRoot(appRoutes, { useHash: true }),// , enableTracing: true }) // <-- debugging purposes only)
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PrimeNgModule,
    SharedModule,
    StoreModule.forRoot(reducers),
    //StoreModule.forRoot(reducer), //CAN NOT SEE IN REDUX APP STATE IN ROOT
    // StoreModule.forRoot(combineReducers({
    //   reducer
    // })),
    StoreDevtoolsModule.instrument({
      name: 'TotoWinner App DevTools',
      maxAge: 25,
      logOnly: environment.production,
    }),
//    StoreModule.forFeature('toto-winner', reducerA),
    EffectsModule.forFeature(
      [ AppEffects ]
    ),
    EffectsModule.forRoot([])
  ],
  providers: [
    TotoGamesService,
    AuthGuard
  ],
  entryComponents: [
    OpenSystemsComponent,
    SaveSystemComponent,
    TemplatesManagerComponent,
    TemplatesWatcherComponent,
    BetsTemplateDialogComponent,
    ProgramsSyncDialogComponent,
    StatisicalDataComponent,
    FormDispatcherComponent,
    SmartDownloadComponent,
    RealtimeRatesScreenerComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
