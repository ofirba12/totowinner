import { AppActions, AppActionTypes } from './app.actions';
import { AppState } from '.';
import { SystemType } from '../model/interfaces/shared.module';
import { StateMode, NotificationType, NotificationContent, NotificationState, OptimizerState, ShrinkState, ProgramState, ProgramType, NavBarState } from './app';
import * as fromBacktesting from './../systems/backtesting/state/backtesting.reducer';
import { combineReducers, ActionReducerMap } from '@ngrx/store';
import { BacktestingDefinitions, BtBetType, LimitType } from '../systems/backtesting/backtesting';
import { navbarReducer } from './navbar.reducer';
import { templatesReducer } from './templates.reducer';
import { SelectItem } from 'primeng/api';
import { TemplatesState } from '../model/interfaces/templates.module';

export interface AppStore {
    'toto-winner': AppState;
    'navbar': NavBarState;
    'backtesting': fromBacktesting.BacktestingState,
    'templates': TemplatesState
}
const initialOptimizerState: OptimizerState = {
    mode: StateMode.None,
    data: null,
    shrinkSystemName: null,
    systemId: null,
    shrinkForOptimizer: null,
    execution: null
}
const initialAppState: AppState = {
    shrink: {
        mode: StateMode.None,
        systemId: null,
        definitions: {
            programData: {
                programGames: [],
                programTitle: null,
                finalResult: null,
                openForBets: null,
                programDate: null,
                programType: null,
                uniqueId: null
            },
            isProgramDataDirty: false,
            form: null,
            isFormDirty: false
        },
        toolbar: {
            keys: {
                selectedType: ProgramType.Winner16,
                selectedYear: new Date().getFullYear()
            },
            selectedProgram: null,
            programsRepository: []
        },
        result: null
        //ratesUpdated: new Date()
    },
    optimizer: initialOptimizerState
};
class reducerUtils {
    public generateNotificationState(systemType: SystemType, state: NotificationState, msgType: NotificationType, message: string): NotificationState {
        var sysNotification: NotificationState;
        if (systemType == SystemType.Optimizer) {
            sysNotification = {
                shrinkNotifications: state.shrinkNotifications,
                optimizerNotifications: { type: msgType, message: message },
                backtestingNotifications: state.backtestingNotifications,
            }
        }
        else if (systemType == SystemType.Shrink) {
            sysNotification = {
                shrinkNotifications: { type: msgType, message: message },
                optimizerNotifications: state.optimizerNotifications,
                backtestingNotifications: state.backtestingNotifications,
            }
        }
        else if (systemType == SystemType.Backtesting) {
            sysNotification = {
                shrinkNotifications: state.shrinkNotifications,
                optimizerNotifications: state.optimizerNotifications,
                backtestingNotifications: { type: msgType, message: message },
            }
        }
        return sysNotification;
    }
}
var utils = new reducerUtils();
// const rootReducer = combineReducers({
//     'toto-winner2' : reducer
//   })
// export function reducerA(state, action) {
//     return rootReducer(state, action);
// }  
export const reducers: ActionReducerMap<AppStore> = {
    'toto-winner': reducer,
    'navbar': navbarReducer,
    'backtesting': fromBacktesting.backtestingReducer,
    'templates': templatesReducer
};
export function reducer(state = initialAppState, action: AppActions): AppState {

    switch (action.type) {
        case AppActionTypes.OpenOptimizerSystem:
            return {
                ...state,
                optimizer: {
                    mode: StateMode.None,
                    systemId: action.payload,
                    data: null,
                    shrinkSystemName: null,
                    shrinkForOptimizer: null,
                    execution: null
                },
            };
        case AppActionTypes.OpenOptimizerSystemSuccess:
            return {
                ...state,
                optimizer: {
                    mode: StateMode.Loading,
                    systemId: state.optimizer.systemId,
                    data: action.payload.systemData,
                    shrinkSystemName: action.payload.optimizedSystemName,
                    shrinkForOptimizer: state.optimizer.shrinkForOptimizer,
                    execution: action.payload.systemData.execution
                },
            };
        case AppActionTypes.SnapshotOptimizerSystem:
            return {
                ...state,
                optimizer: {
                    mode: StateMode.Saving,
                    systemId: state.optimizer.systemId,
                    data: action.payload,
                    shrinkSystemName: state.optimizer.shrinkSystemName,
                    shrinkForOptimizer: state.optimizer.shrinkForOptimizer,
                    execution: state.optimizer.execution
                }
            };
        case AppActionTypes.SnapshotShrinkSystem:
            var newShrink = { ...state.shrink }
            newShrink.mode = StateMode.Saving;
            newShrink.definitions.form = action.payload;
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.SaveShrinkSystemSuccess:
            var newShrink = { ...state.shrink }
            newShrink.mode = StateMode.None
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.ShrinkSystemUpdateForm:
        case AppActionTypes.ShrinkSystemClearForm:
            var newShrink = { ...state.shrink }
            newShrink.definitions.form = action.payload;
            newShrink.result = null;
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.ShrinkSystemUpdateCalcResult:
            var newShrink = { ...state.shrink }
            newShrink.result = action.payload;
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.ShrinkSystemUpdateDirtyFormFlag:
            var newShrink = { ...state.shrink }
            newShrink.definitions.isFormDirty = action.payload;
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.ShrinkSystemUpdateDirtyProgramDataFlag:
            var newShrink = { ...state.shrink }
            newShrink.definitions.isProgramDataDirty = action.payload;
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.SaveOptimizerSystemSuccess:
            var newOptimizer = { ...state.optimizer }
            newOptimizer.systemId = action.payload.systemId
            return {
                ...state,
                optimizer: newOptimizer,
            };
        case AppActionTypes.OpenShrinkSystem:
            var newShrink = { ...state.shrink }
            newShrink.systemId = action.payload.systemId
            return {
                ...state,
                shrink: newShrink
            }
        case AppActionTypes.OpenShrinkSystemForOptimizer:
            return {
                ...state,
                optimizer: {
                    mode: state.optimizer.mode,
                    systemId: state.optimizer.systemId,
                    data: state.optimizer.data,
                    shrinkSystemName: state.optimizer.shrinkSystemName,
                    shrinkForOptimizer: {
                        shrinkSystemId: action.payload.systemId,
                        shrinkPayload: null
                    },
                    execution: null
                }
            }
        case AppActionTypes.OpenShrinkSystemSuccess:
            var optState: OptimizerState = state.optimizer;
            var shrinkState: ShrinkState = state.shrink;
            var programsState = state.shrink.toolbar.programsRepository.find(item => item.type == action.payload.shrinkPayload.definitions.programData.programType &&
                item.year == action.payload.shrinkPayload.year);
            shrinkState = {
                toolbar: {
                    ...state.shrink.toolbar,
                    keys: { selectedType: action.payload.shrinkPayload.definitions.programData.programType, selectedYear: action.payload.shrinkPayload.year },
                    selectedProgram: programsState.programs.find(item => item.value.uniqueId == action.payload.shrinkPayload.uniqueId)
                },
                systemId: state.shrink.systemId,
                mode: StateMode.Loading,
                definitions: action.payload.shrinkPayload.definitions,
                result: action.payload.shrinkPayload.result
                //ratesUpdated: state.shrink.ratesUpdated
            }
            return {
                ...state,
                shrink: shrinkState,
                optimizer: optState
            };
        case AppActionTypes.SelectSystem:
            var newShrink = { ...state.shrink }
            if (action.payload.systemType == SystemType.Shrink && state.shrink.systemId) {
                newShrink.mode = StateMode.Loading
            }
            return {
                ...state,
                shrink: newShrink
            };
        case AppActionTypes.OpenShrinkSystemForOptimizerSuccess:
            var optState: OptimizerState = state.optimizer;
            var shrinkState: ShrinkState = state.shrink;
            optState = {
                mode: state.optimizer.mode,
                systemId: state.optimizer.systemId,
                data: state.optimizer.data,
                shrinkSystemName: state.optimizer.shrinkSystemName,
                shrinkForOptimizer: {
                    shrinkSystemId: state.optimizer.shrinkForOptimizer.shrinkSystemId,
                    shrinkPayload: action.payload.shrinkPayload
                },
                execution: null
            }
            return {
                ...state,
                shrink: shrinkState,
                optimizer: optState
            };
        case AppActionTypes.ShrinkSystemLoadFinished:
            var newShrink = { ...state.shrink }
            newShrink.mode = StateMode.None
            return {
                ...state,
                shrink: newShrink
            }
        case AppActionTypes.SetShrinkSystemDataProgramGames:
            var shrinkState: ShrinkState = { ...state.shrink };
            shrinkState.definitions = {
                ...shrinkState.definitions,
                programData: action.payload
            };
            return {
                ...state,
                shrink: shrinkState
            }
        // case AppActionTypes.ProgramRatesUpdated:
        //     var shrinkState: ShrinkState = { ...state.shrink };
        //     shrinkState.ratesUpdated = new Date()
        //     return {
        //         ...state,
        //         shrink: shrinkState
        //     }
        case AppActionTypes.OpenSystemFail:
            return {
                ...state,
            }
        case AppActionTypes.SetPrograms:
            var programsExists = state.shrink.toolbar.programsRepository.find(item => item.type == action.payload.type
                && item.year == action.payload.year);
            var newState: ProgramState[];
            if (!programsExists) {
                newState = [...state.shrink.toolbar.programsRepository,
                { type: action.payload.type, year: action.payload.year, programs: action.payload.programs }]
            }
            else {
                newState = [...state.shrink.toolbar.programsRepository]
            }
            return {
                ...state,
                shrink: {
                    toolbar: {
                        keys: {
                            selectedType: state.shrink.toolbar.keys.selectedType,
                            selectedYear: state.shrink.toolbar.keys.selectedYear
                        },
                        selectedProgram: state.shrink.toolbar.selectedProgram,
                        programsRepository: newState
                    },
                    systemId: state.shrink.systemId,
                    mode: state.shrink.mode,
                    definitions: state.shrink.definitions,
                    result: state.shrink.result
                    //ratesUpdated: state.shrink.ratesUpdated
                }
            }
        case AppActionTypes.SetToolbarSelectedTypeYear:
            if (state.shrink.toolbar.keys.selectedType != action.payload.selectedType ||
                state.shrink.toolbar.keys.selectedYear != action.payload.selectedYear) {
                var newShrink = { ...state.shrink }
                newShrink.toolbar = {
                    keys: {
                        selectedType: action.payload.selectedType,
                        selectedYear: action.payload.selectedYear
                    },
                    selectedProgram: undefined,
                    programsRepository: state.shrink.toolbar.programsRepository
                }
                newShrink.definitions = {
                    programData: {
                        programGames: [],
                        programTitle: null,
                        finalResult: null,
                        openForBets: null,
                        programDate: null,
                        programType: null,
                        uniqueId: null
                    },
                    isProgramDataDirty: false,
                    form: null,
                    isFormDirty: false
                }
                result: null

                return {
                    ...state,
                    shrink: newShrink
                }
            }
            return state;
        case AppActionTypes.SetToolbarSelectedProgram:
            var newShrink = { ...state.shrink }
            newShrink.toolbar.selectedProgram = action.payload;
            newShrink.definitions = {
                programData: {
                    programGames: [],
                    programTitle: null,
                    finalResult: null,
                    openForBets: null,
                    programDate: null,
                    programType: null,
                    uniqueId: null
                },
                isProgramDataDirty: false,
                form: null,
                isFormDirty: false
            }
            newShrink.result = null
            return {
                ...state,
                shrink: newShrink
            }
        case AppActionTypes.StartOptimizerExecution:
            var newOptimizer = { ...state.optimizer }
            newOptimizer.execution = { ...state.optimizer.execution, parameters: action.payload.params }
            newOptimizer.mode = StateMode.None;
            return {
                ...state,
                optimizer: newOptimizer
            }
        case AppActionTypes.UpdateOptimizerExecutionResult:
            var newOptimizer = { ...state.optimizer }
            newOptimizer.execution = {
                ...state.optimizer.execution,
                executionId: action.payload.execution.executionId,
                status: action.payload.execution.status,
                results: { parentBetsCounter: action.payload.parentBetsCounter, shrinkedBetsCounter: action.payload.shrinkedBetsCounter }
            }
            return {
                ...state,
                optimizer: newOptimizer
            }
        default:
            return state;
    }
}
