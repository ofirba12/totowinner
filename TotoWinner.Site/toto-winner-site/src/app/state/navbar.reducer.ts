import { NotificationState, NotificationContent, NotificationType, NavBarState } from './app';
import { AppActions, AppActionTypes } from './app.actions';
import { SystemType } from '../model/interfaces/shared.module';
const initialAppState: NavBarState = {
    systems: {
        currentSelected: null,
        currentSystemNames: [
            { systemType: SystemType.Shrink, systemNames: [] },
            { systemType: SystemType.Optimizer, systemNames: [] },
            { systemType: SystemType.Backtesting, systemNames: [] }
        ]
    },
    notification: {
        shrinkNotifications: { type: NotificationType.None, message: null },
        optimizerNotifications: { type: NotificationType.None, message: null },
        backtestingNotifications: { type: NotificationType.None, message: null },
    }
}
class reducerUtils {
    public generateNotificationState(systemType: SystemType, state: NotificationState, msgType: NotificationType, message: string): NotificationState {
        var sysNotification: NotificationState;
        if (systemType == SystemType.Optimizer) {
            sysNotification = {
                shrinkNotifications: state.shrinkNotifications,
                optimizerNotifications: { type: msgType, message: message },
                backtestingNotifications: state.backtestingNotifications,
            }
        }
        else if (systemType == SystemType.Shrink) {
            sysNotification = {
                shrinkNotifications: { type: msgType, message: message },
                optimizerNotifications: state.optimizerNotifications,
                backtestingNotifications: state.backtestingNotifications,
            }
        }
        else if (systemType == SystemType.Backtesting) {
            sysNotification = {
                shrinkNotifications: state.shrinkNotifications,
                optimizerNotifications: state.optimizerNotifications,
                backtestingNotifications: { type: msgType, message: message },
            }
        }
        return sysNotification;
    }
}
var utils = new reducerUtils();

export function navbarReducer(state = initialAppState, action: AppActions): NavBarState {
    switch (action.type) {
        case AppActionTypes.SelectSystem:
            var notificationContent: NotificationContent;
            switch (action.payload.systemType) {
                case SystemType.Shrink:
                    notificationContent = state.notification.shrinkNotifications;
                    break;
                case SystemType.Optimizer:
                    notificationContent = state.notification.optimizerNotifications;
                    break;
                case SystemType.Backtesting:
                    notificationContent = state.notification.backtestingNotifications;
                    break;
            }
            return {
                ...state,                
                systems: {
                    currentSelected: action.payload,
                    currentSystemNames: state.systems.currentSystemNames,
                },
                notification: utils.generateNotificationState(action.payload.systemType,
                    state.notification,
                    notificationContent.type,
                    notificationContent.message)
            };
        case AppActionTypes.ShowMessage:
            return {
                ...state,
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    action.payload.type,
                    action.payload.message)
            };
        case AppActionTypes.SetToolbarSelectedTypeYear:
        case AppActionTypes.SetToolbarSelectedProgram:
        case AppActionTypes.StartOptimizerExecution:
        case AppActionTypes.ClearMessage:
            return {
                ...state,
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.None,
                    null)
            };
        case AppActionTypes.OpenOptimizerSystemSuccess:
            var sysNames = state.systems.currentSystemNames.map(item => {
                if (item.systemType == SystemType.Optimizer) {
                    item.systemNames = [
                        { systemType: SystemType.Optimizer, name: action.payload.systemData.name },
                        { systemType: SystemType.Shrink, name: action.payload.optimizedSystemName }
                    ]
                }
                return item;
            }
            );
            return {
                ...state,
                systems: {
                    currentSelected: state.systems.currentSelected,
                    currentSystemNames: sysNames
                },
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Success,
                    `אוטימיזציה [${action.payload.systemData.name}] נבחרה`)
            };
        case AppActionTypes.SaveShrinkSystemSuccess:
            return {
                ...state,
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Success,
                    ` מערכת צימצומים [${action.shrinkSystemPayload.name}] נשמרה`)
            };
        case AppActionTypes.ShrinkSystemUpdateForm:
        case AppActionTypes.ShrinkSystemClearForm:
            return {
                ...state,
                notification: { ...state.notification, shrinkNotifications: { type: NotificationType.None, message: null } },
            };
        case AppActionTypes.SaveOptimizerSystemSuccess:
            var sysNames = state.systems.currentSystemNames.map(item => {
                if (item.systemType == SystemType.Optimizer) {
                    var optimizerNames = item.systemNames.map(optName => {
                        if (optName.systemType == SystemType.Optimizer)
                            optName.name = action.newName;
                        return optName;
                    });
                }
                return item;
            });
            return {
                ...state,
                systems: {
                    currentSelected: state.systems.currentSelected,
                    currentSystemNames: sysNames,
                },
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Success,
                    `אוטימיזציה [${action.newName}] נשמרה`),
            };
        case AppActionTypes.OpenShrinkSystemSuccess:
            var sysNames = state.systems.currentSystemNames.map(item => {
                if (item.systemType == state.systems.currentSelected.systemType) {
                    item.systemNames = [
                        { systemType: SystemType.Shrink, name: action.payload.shrinkPayload.name }
                    ]
                }
                return item;
            }
            );
            return {
                ...state,
                systems: {
                    currentSelected: state.systems.currentSelected,
                    currentSystemNames: sysNames
                },
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Success,
                    `מערכת צימצומים [${action.payload.shrinkPayload.name}] נבחרה`),
            };
        case AppActionTypes.OpenShrinkSystemForOptimizerSuccess:
            var sysNames = state.systems.currentSystemNames.map(item => {
                if (item.systemType == state.systems.currentSelected.systemType) {
                    item.systemNames = [
                        { systemType: SystemType.Shrink, name: action.payload.shrinkPayload.name }
                    ]
                }
                return item;
            }
            );
            return {
                ...state,
                systems: {
                    currentSelected: state.systems.currentSelected,
                    currentSystemNames: sysNames
                },
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Success,
                    `מערכת צימצומים [${action.payload.shrinkPayload.name}] נבחרה`)
            };
        case AppActionTypes.OpenSystemFail:
            return {
                ...state,
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Error,
                    `ארעה תקלה בטעינת מערכת [Id=${action.payload.name}]`)
            }
        case AppActionTypes.DeleteSystemSuccess:
            var sysNames = state.systems.currentSystemNames.map(item => {
                if (item.systemType == action.payload.systemType) {
                    if (item.systemNames.find(names => names.name == action.payload.systemName))
                        item.systemNames = [];
                }
                return item;
            });
            return {
                ...state,
                systems: {
                    currentSelected: state.systems.currentSelected,
                    currentSystemNames: sysNames,
                },
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Success,
                    `מערכת [${action.payload.systemName}] נמחקה`)
            };
        case AppActionTypes.WatchTemplatesFail:
            return {
                ...state,
                notification: utils.generateNotificationState(state.systems.currentSelected.systemType,
                    state.notification,
                    NotificationType.Error,
                    `אירעה תקלה לא צפויה במעקב טמפלטים`)
            }
            break;

        default:
            return state;
    }
}
