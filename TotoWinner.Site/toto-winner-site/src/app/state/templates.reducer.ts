import { AppActions, AppActionTypes } from './app.actions';
import { ProgramType } from './app';
import { Template, TemplateWatcherRepository, Bet, TemplatesState, TemplatesBaseBetsResponse, TemplateWatcherState } from '../model/interfaces/templates.module';
const initialAppState: TemplatesState = {
    templates: [],
    watcher: {
        collection : {},
        needsUpdate: false
    },
    showBets: null,
    error: null
}

export function templatesReducer(state = initialAppState, action: AppActions): TemplatesState {
    switch (action.type) {
        case AppActionTypes.GetAllTemplatesSuccess:
            return {
                ...state,
                templates: action.payload.templates,
                error: null
            }
            break;
        case AppActionTypes.GetAllTemplatesFail:
            return {
                ...state,
                templates: [],
                error: 'אירעה תקלה לא צפויה באיחזור טמפלטים שמורים'
            }
            break;
        case AppActionTypes.AddTemplateSuccess:
            var newCollection = [...state.templates];
            newCollection.push(action.payload.templateInfo);
            var newWatcher: TemplateWatcherState = { 
                collection: {},
                needsUpdate: true
             }
            return {
                ...state,
                templates: newCollection,
                watcher: newWatcher,
                error: null
            }
            break;
        case AppActionTypes.AddTemplateFail:
            return {
                ...state,
                error: 'אירעה תקלה לא צפויה בהוספת טמפלט'
            }
            break;
        case AppActionTypes.DeleteTemplateFail:
            return {
                ...state,
                error: 'אירעה תקלה לא צפויה בבמחיקת טמפלט'
            }
            break;
        case AppActionTypes.DeleteTemplateSuccess:
            var newCollection = state.templates.filter(i => i.id != action.payload.templateId);
            var newWatcher: TemplateWatcherState = { 
                collection: {},
                needsUpdate: true
             }
            return {
                ...state,
                templates: newCollection,
                watcher: newWatcher,
                error: null
            }
            break;
        case AppActionTypes.WatchTemplatesSuccess:
            // var newCollection = {...state.templates};
            // Object.keys(action.payload.templatesBaseBets).forEach(element => {
            //     var elm = Object.values<Template>(newCollection).find(t => t.id === +element);
            // });
            var newWatcher: TemplateWatcherState = { ...state.watcher }
            // Object.assign<{ [id: number]: Bet; }, { [id: number]: Bet; }>(newWatcher.templatesBaseBets,
            //     action.payload.templatesBaseBets);
            Object.assign<{ [id: number] : TemplatesBaseBetsResponse; }, { [id: number] : TemplatesBaseBetsResponse; }>(newWatcher.collection,
                action.payload.templatesBaseBets); 
            newWatcher.needsUpdate = false;
            return {
                ...state,
                watcher: newWatcher,
                error: null
            }
            break;
        case AppActionTypes.ShowTemplateBetsDialog:
            return {
                ...state,
                showBets: action.payload
            }
            break;
        case AppActionTypes.HideTemplateBetsDialog:
            return {
                ...state,
                showBets: null
            }
            break;
        default:
            return state;
    }
}