import { Action } from '@ngrx/store';
import { SystemMetaData, OpenShrinkSystemRequest, DeleteSystemRequest, DeleteSystemResponse, SaveOptimizerSystemResponse, NotificationContent, ProgramState, ToolbarKeysState, ShrinkSystemLoadResponse, OptimizerExecutionResultsState, OptimizerExecutionState, ProgramType } from './app';
import { SaveOptimizerSystemRequest, LoadOptimizerSystemResponse, ShrinkSystemData, SaveShrinkSystemResponse, ShrinkSystemPayload } from '../model/interfaces/shared.module';
import { RunBacktestingRequest, RunBacktestingResponse, PollExecutionRequest, StopExecutionRequest, StopExecutionResponse, BtBetType, LimitType } from '../systems/backtesting/backtesting';
import { SelectItem } from 'primeng/api';
import { ProgramDetails, ProgramData } from '../model/interfaces/program.module';
import { StartCalcOptimizerParam } from '../model/interfaces/optimizer.module';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { WatchTemplatesRequest, WatchTemplatesResponse, GetAllTemplatesResponse, TemplateRequest, AddTemplateResponse, DeleteTemplateResponse, TemplateWatcher } from '../model/interfaces/templates.module';

export enum AppActionTypes {
    SelectSystem = '[NavBar] Select System',
    ShowMessage = '[NavBar] Show Message',
    ClearMessage = '[NavBar] Clear Message',
    OpenOptimizerSystem = '[NavBar] Open Optimizer System',
    OpenOptimizerSystemSuccess = '[NavBar] Open Optimizer System Success',
    OpenSystemFail = '[NavBar] Open System Fail',
    OpenShrinkSystem = '[NavBar] Open Shrink System',
    OpenShrinkSystemForOptimizer = '[NavBar] Open Shrink System For Optimizer',    
    OpenShrinkSystemSuccess = '[NavBar] Open Shrink System Success',
    OpenShrinkSystemForOptimizerSuccess = '[NavBar] Open Shrink System For Optimizer Success',
    DeleteSystem = '[NavBar] Delete System',
    DeleteSystemSuccess = '[NavBar] Delete System Success',
    DeleteSystemFail = '[NavBar] Delete System Fail',
    SaveOptimizerSystem = '[NavBar] Save Optimizer System',
    SaveOptimizerSystemSuccess = '[NavBar] Save Optimizer System Success',
    SaveSystemFail = '[NavBar] Save System Fail',
    SnapshotOptimizerSystem = '[NavBar] Snapshot Optimizer System',    
    SetPrograms = '[Shrink] Toolbar Set Programs',
    SetToolbarSelectedTypeYear = '[Shrink] Toolbar Set Selected Programs Type/Year',
    SetToolbarSelectedProgram = '[Shrink] Toolbar Set Selected Program',
    SetShrinkSystemDataProgramGames = '[Shrink] Toolbar Set Shrink System Data Program Games',
//    ProgramRatesUpdated = '[Shrink] Program Rates Updated',
    SaveShrinkSystem = '[NavBar] Save Shrink System',
    SaveShrinkSystemSuccess = '[NavBar] Save Shrink System Success',
    SnapshotShrinkSystem = '[NavBar] Snapshot Shrink System',
    ShrinkSystemLoadFinished = '[Shrink] Loading Finished',
    ShrinkSystemClearForm= '[Shrink] Clear Form Settings',
    ShrinkSystemUpdateForm= '[Shrink] Update Form Settings',
    ShrinkSystemUpdateCalcResult= '[Shrink] Update Calculation Result',
    ShrinkSystemUpdateDirtyProgramDataFlag= '[Shrink] Update Dirty Program Data Flag',
    ShrinkSystemUpdateDirtyFormFlag= '[Shrink] Update Dirty Form Flag',
    StartOptimizerExecution = '[Optimizer] Start Execution',
    UpdateOptimizerExecutionResult = '[Optimizer] Update Execution Result', 
    GetAllTemplates = '[TemplateManager] Get All Template',
    GetAllTemplatesSuccess = '[TemplateManager] Get All Template Success',
    GetAllTemplatesFail = '[TemplateManager] Get All Template Failed',
    AddTemplate = '[TemplateManager] Add Template',
    AddTemplateSuccess = '[TemplateManager] Add Template Success',
    AddTemplateFail = '[TemplateManager] Add Template Failed',
    DeleteTemplate = '[TemplateManager] Delete Template',
    DeleteTemplateSuccess = '[TemplateManager] Delete Template Success',
    DeleteTemplateFail = '[TemplateManager] Delete Template Failed',
    WatchTemplates = '[TemplatesWatcher] Watch Template',
    WatchTemplatesSuccess = '[TemplatesWatcher] Watch Template Success',
    WatchTemplatesFail = '[TemplatesWatcher] Watch Template Failed',
    ShowTemplateBetsDialog = '[TemplatesWatcher] Show Template Bets Dialog',
    HideTemplateBetsDialog = '[TemplatesWatcher] Hide Template Bets Dialog'
}

export class SelectSystem implements Action {
    readonly type = AppActionTypes.SelectSystem;

    constructor(public payload: SystemMetaData) { }
}
export class ShowMessage implements Action {
    readonly type = AppActionTypes.ShowMessage;

    constructor(public payload: NotificationContent) { }
}
export class ClearMessage implements Action {
    readonly type = AppActionTypes.ClearMessage;

    constructor() { }
}
export class OpenOptimizerSystem implements Action {
    readonly type = AppActionTypes.OpenOptimizerSystem;

    constructor(public payload: number) { }
}
export class OpenOptimizerSystemSuccess implements Action {
    readonly type = AppActionTypes.OpenOptimizerSystemSuccess;

    constructor(public payload: LoadOptimizerSystemResponse) { }
}
export class OpenSystemFail implements Action {
    readonly type = AppActionTypes.OpenSystemFail;

    constructor(public payload: SystemMetaData) { }
}
export class OpenShrinkSystemForOptimizer implements Action {
    readonly type = AppActionTypes.OpenShrinkSystemForOptimizer;

    constructor(public payload: OpenShrinkSystemRequest) { }
}
export class OpenShrinkSystem implements Action {
    readonly type = AppActionTypes.OpenShrinkSystem;

    constructor(public payload: OpenShrinkSystemRequest) { }
}
export class OpenShrinkSystemSuccess implements Action {
    readonly type = AppActionTypes.OpenShrinkSystemSuccess;

    constructor(public payload: ShrinkSystemLoadResponse) { }
}
export class OpenShrinkSystemForOptimizerSuccess implements Action {
    readonly type = AppActionTypes.OpenShrinkSystemForOptimizerSuccess;

    constructor(public payload: ShrinkSystemLoadResponse) { }
}
export class ShrinkSystemLoadFinished implements Action {
    readonly type = AppActionTypes.ShrinkSystemLoadFinished;

    constructor() { }
}
export class SetShrinkSystemDataProgramGames implements Action {
    readonly type = AppActionTypes.SetShrinkSystemDataProgramGames;

    constructor(public payload: ProgramData) { }
}
// export class ProgramRatesUpdated implements Action {
//     readonly type = AppActionTypes.ProgramRatesUpdated;

//     constructor() { }
// }
export class SaveShrinkSystem implements Action {
    readonly type = AppActionTypes.SaveShrinkSystem;

    constructor(public payload: ShrinkSystemPayload) { }
}
export class ShrinkSystemClearForm implements Action {
    readonly type = AppActionTypes.ShrinkSystemClearForm;

    constructor(public payload: BetsCalculation.FormCreateRequest) { }
}
export class ShrinkSystemUpdateForm implements Action {
    readonly type = AppActionTypes.ShrinkSystemUpdateForm;

    constructor(public payload: BetsCalculation.FormCreateRequest) { }
}
export class ShrinkSystemUpdateCalcResult implements Action {
    readonly type = AppActionTypes.ShrinkSystemUpdateCalcResult;

    constructor(public payload: ShrinkResultModule.SeResults) { }
}
export class ShrinkSystemUpdateDirtyProgramDataFlag implements Action {
    readonly type = AppActionTypes.ShrinkSystemUpdateDirtyProgramDataFlag;

    constructor(public payload: boolean) { }
}
export class ShrinkSystemUpdateDirtyFormFlag implements Action {
    readonly type = AppActionTypes.ShrinkSystemUpdateDirtyFormFlag;

    constructor(public payload: boolean) { }
}

export class SaveShrinkSystemSuccess implements Action {
    readonly type = AppActionTypes.SaveShrinkSystemSuccess;

    constructor(public payload: SaveShrinkSystemResponse, public shrinkSystemPayload: ShrinkSystemPayload) { }
}
export class DeleteSystem implements Action {
    readonly type = AppActionTypes.DeleteSystem;

    constructor(public payload: DeleteSystemRequest) { }
}
export class DeleteSystemSuccess implements Action {
    readonly type = AppActionTypes.DeleteSystemSuccess;

    constructor(public payload: DeleteSystemResponse) { }
}
export class DeleteSystemFail implements Action {
    readonly type = AppActionTypes.DeleteSystemFail;

    constructor(public payload: string) { }
}
export class SaveOptimizerSystem implements Action {
    readonly type = AppActionTypes.SaveOptimizerSystem;

    constructor(public payload: SaveOptimizerSystemRequest) { }
}
export class SaveOptimizerSystemSuccess implements Action {
    readonly type = AppActionTypes.SaveOptimizerSystemSuccess;

    constructor(public payload: SaveOptimizerSystemResponse, public newName: string) { }
}
export class SaveSystemFail implements Action {
    readonly type = AppActionTypes.SaveSystemFail;

    constructor(public payload: string) { } 
}
export class SnapshotOptimizerSystem implements Action {
    readonly type = AppActionTypes.SnapshotOptimizerSystem;

    constructor(public payload: SaveOptimizerSystemRequest) { }
}
export class SetPrograms implements Action {
    readonly type = AppActionTypes.SetPrograms;

    constructor(public payload: ProgramState) { }
}
export class SetToolbarSelectedTypeYear implements Action {
    readonly type = AppActionTypes.SetToolbarSelectedTypeYear;

    constructor(public payload: ToolbarKeysState) { }
}
export class SetToolbarSelectedProgram implements Action {
    readonly type = AppActionTypes.SetToolbarSelectedProgram;

    constructor(public payload: SelectItem) { }
}
export class SnapshotShrinkSystem implements Action {
    readonly type = AppActionTypes.SnapshotShrinkSystem;

    constructor(public payload: BetsCalculation.FormCreateRequest) { }
}
export class StartOptimizerExecution implements Action {
    readonly type = AppActionTypes.StartOptimizerExecution;

    constructor(public payload: { optimizedSystemId : number, params: StartCalcOptimizerParam}) { }
}
export class UpdateOptimizerExecutionResult implements Action {
    readonly type = AppActionTypes.UpdateOptimizerExecutionResult;

    constructor(public payload: { execution: OptimizerExecutionState, parentBetsCounter: number, shrinkedBetsCounter: number}) { }
}
export class GetAllTemplates implements Action {
    readonly type = AppActionTypes.GetAllTemplates;

    constructor() { }
}
export class GetAllTemplatesSuccess implements Action {
    readonly type = AppActionTypes.GetAllTemplatesSuccess;

    constructor(public payload: GetAllTemplatesResponse) { }
}
export class GetAllTemplatesFail implements Action {
    readonly type = AppActionTypes.GetAllTemplatesFail;

    constructor(public payload: string) { }
}
export class AddTemplate implements Action {
    readonly type = AppActionTypes.AddTemplate;

    constructor(public payload: TemplateRequest) { }
}
export class AddTemplateSuccess implements Action {
    readonly type = AppActionTypes.AddTemplateSuccess;

    constructor(public payload: AddTemplateResponse) { }
}
export class AddTemplateFail implements Action {
    readonly type = AppActionTypes.AddTemplateFail;

    constructor(public payload: string) { }
}
export class DeleteTemplate implements Action {
    readonly type = AppActionTypes.DeleteTemplate;

    constructor(public payload: TemplateRequest) { }
}
export class DeleteTemplateSuccess implements Action {
    readonly type = AppActionTypes.DeleteTemplateSuccess;

    constructor(public payload: DeleteTemplateResponse) { }
}
export class DeleteTemplateFail implements Action {
    readonly type = AppActionTypes.DeleteTemplateFail;

    constructor(public payload: string) { }
}
export class WatchTemplates implements Action {
    readonly type = AppActionTypes.WatchTemplates;

    constructor(public payload: WatchTemplatesRequest) { }
}
export class WatchTemplatesSuccess implements Action {
    readonly type = AppActionTypes.WatchTemplatesSuccess;

    constructor(public payload: WatchTemplatesResponse) { }
}
export class WatchTemplatesFail implements Action {
    readonly type = AppActionTypes.WatchTemplatesFail;

    constructor(public payload: string) { }
}
export class ShowTemplateBetsDialog implements Action {
    readonly type = AppActionTypes.ShowTemplateBetsDialog;

    constructor(public payload: TemplateWatcher) { }
}
export class HideTemplateBetsDialog implements Action {
    readonly type = AppActionTypes.HideTemplateBetsDialog;

    constructor() { }
}

export type AppActions = SelectSystem
    | ShowMessage
    | ClearMessage
    | OpenOptimizerSystem 
    | OpenOptimizerSystemSuccess
    | OpenShrinkSystem 
    | OpenShrinkSystemSuccess
    | OpenShrinkSystemForOptimizer
    | OpenShrinkSystemForOptimizerSuccess
    | OpenSystemFail
    | DeleteSystem 
    | DeleteSystemSuccess
    | DeleteSystemFail
    | SaveOptimizerSystem 
    | SaveOptimizerSystemSuccess
    | SaveSystemFail
    | SnapshotOptimizerSystem
    | SetPrograms
    | SetToolbarSelectedTypeYear
    | SetToolbarSelectedProgram
    | SetShrinkSystemDataProgramGames
//    | ProgramRatesUpdated
    | SaveShrinkSystem
    | SaveShrinkSystemSuccess
    | SnapshotShrinkSystem
    | ShrinkSystemLoadFinished
    | ShrinkSystemClearForm
    | ShrinkSystemUpdateForm
    | ShrinkSystemUpdateCalcResult
    | ShrinkSystemUpdateDirtyProgramDataFlag
    | ShrinkSystemUpdateDirtyFormFlag
    | StartOptimizerExecution
    | UpdateOptimizerExecutionResult
    | GetAllTemplates
    | GetAllTemplatesSuccess
    | GetAllTemplatesFail
    | AddTemplate
    | AddTemplateSuccess
    | AddTemplateFail
    | DeleteTemplate
    | DeleteTemplateSuccess
    | DeleteTemplateFail
    | WatchTemplates
    | WatchTemplatesSuccess
    | WatchTemplatesFail
    | ShowTemplateBetsDialog
    | HideTemplateBetsDialog
    ;

    