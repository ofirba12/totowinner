import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromApp from '.';
import { NavBarState, OptimizerState, ShrinkForOptimizerState, NotificationState, ShrinkState, StateMode, ProgramType } from './app';
import * as fromBacktesting from './../systems/backtesting/state/backtesting.reducer';
import { TemplateWatcherParameters, TemplatesState, TemplateWatcherDialogData, TemplateWatcherState, Template } from '../model/interfaces/templates.module';
import { watch } from 'fs';
export interface AppState {
    shrink: ShrinkState;
    optimizer: OptimizerState;
}

const getTemplatesState = createFeatureSelector<TemplatesState>('templates');
export const getTemplates = createSelector(
    getTemplatesState,
    state => state
);

const getNavBarState = createFeatureSelector<NavBarState>('navbar');
export const getNotification = createSelector(
    getNavBarState,
    state => state.notification
);
export const getSystems = createSelector(
    getNavBarState,
    state => state.systems
);
const getBacktestingState = createFeatureSelector<fromBacktesting.BacktestingState>('backtesting');
export const getBacktestingDefinition = createSelector(
    getBacktestingState,
    state => state.definitions
);
export const getBacktestingExecution = createSelector(
    getBacktestingState,
    state => state.execution
);
export const getBacktestingPastProgramSelected = createSelector(
    getBacktestingState,
    state => state.pastProgramSelected
);
export const getBacktestingProgramType = createSelector(
    getBacktestingState,
    state => state.definitions.programType
);
const getAppState = createFeatureSelector<fromApp.AppState>('toto-winner');
export const getShrink = createSelector(
    getAppState,
    state => state.shrink
);
export const getShrinkDefinitions = createSelector(
    getAppState,
    state => state.shrink.definitions
);
export const getShrinkToolbar = createSelector(
    getAppState,
    state => state.shrink.toolbar
);
export const getShrinkGamesBets = createSelector(
    getAppState,
    state => state.shrink.definitions.programData.programGames.map(p => p.bets)
);
// export const getShrinkProgramRatesUpdateTimeStamp = createSelector(
//     getAppState,
//     state => state.shrink.ratesUpdated
// );
export const getTemplateWatcherParameters = createSelector(
    getShrinkToolbar,
    getShrinkGamesBets,
    getTemplatesState,
    //getShrinkProgramRatesUpdateTimeStamp,
    (toolbar, bets, templateState) => {//, rateTimeStamp) => {
        var result: TemplateWatcherParameters = {
            programType: toolbar.keys.selectedType,
            bets: bets,
            templates: templateState.templates.filter(t => t.type === toolbar.keys.selectedType),
            watcherNeedsUpdate: templateState.watcher.needsUpdate
            //rateUpdateTimeStamp : rateTimeStamp
        }
        return result;
    }
);
export const getTemplateWatcherData = createSelector(
    getTemplatesState,
    state => state.watcher
);
export const getTemplateWatcherDataWithSelectedType = createSelector(
    getShrinkToolbar,
    getTemplatesState,
    (toolbar, templateState) => {
        var result: TemplateWatcherState = {
            collection: {},
            needsUpdate: templateState.watcher.needsUpdate
        }
        var telements = Object.values<Template>(templateState.templates).map(t => {
            if (t.type === toolbar.keys.selectedType)
                return t;
        });
        var welements = Object.entries(templateState.watcher.collection);
        telements.forEach(element => {
            if (element) {
                var witem = welements.find(item => +item[0] === element.id);
                if (witem) {
                    result.collection[witem[0]] = witem[1];
                }
            }
        });
        return result;
    }
);
export const getTemplateWatcherDialogData = createSelector(
    getShrinkGamesBets,
    getTemplatesState,
    (rates, allTemplates) => {
        var result: TemplateWatcherDialogData = {
            rates: rates,
            templateData: allTemplates.showBets
        }
        return result;
    }
);
export const getShrinkToolbarSelectedProgram = createSelector(
    getAppState,
    state => state.shrink.toolbar.selectedProgram
);
export const getProgramsRepository = createSelector(
    getAppState,
    state => state.shrink.toolbar.programsRepository
);
export const getOptimizer = createSelector(
    getAppState,
    state => state.optimizer
);
export const getShrinkForOptimizer = createSelector(
    getAppState,
    state => state.optimizer.shrinkForOptimizer
);
export const getOptimizerExecution = createSelector(
    getAppState,
    state => state.optimizer.execution
);
