import { SaveOptimizerSystemRequest, ShrinkSystemData, SystemType, ShrinkSystemPayload, ShrinkSystemDefinitions, CalculationStatus } from '../model/interfaces/shared.module';
import { SelectItem } from 'primeng/api';
import { StartCalcOptimizerParam } from '../model/interfaces/optimizer.module';
import { Template, TemplateWatcherRepository } from '../model/interfaces/templates.module';

export interface NavBarState{
    systems : NavBarSystems;
    notification: NotificationState;
}
export interface NavBarSystems{
    currentSelected : SystemMetaData;
    currentSystemNames: NavBarSystemNames[];
}
export interface NavBarSystemNames{
    systemType : SystemType;
    systemNames: SystemMetaData[]
}
export interface SystemMetaData{
    systemType : SystemType;
    name: string;
}
export enum StateMode{
    None = 0,
    Loading = 1,
    Saving = 2
}
export enum ProgramType{
    Winner16 = 96,
    WinnerHalf = 82,
    WinnerWorld = 87
}
export interface ProgramState{
    type: ProgramType,
    year: number,
    programs: SelectItem[]
}
export interface ToolbarKeysState{
    selectedType: ProgramType,
    selectedYear: number,
}
export interface ToolbarState{
    keys: ToolbarKeysState,
    selectedProgram: SelectItem,
    programsRepository: ProgramState[]
}
export interface ShrinkState{
    toolbar: ToolbarState;
    mode: StateMode
    systemId : number;
    definitions: ShrinkSystemDefinitions;
    result: ShrinkResultModule.SeResults;
//    ratesUpdated : Date;
}
export interface OptimizerExecutionResultsState{
    parentBetsCounter: number;
    shrinkedBetsCounter: number;
}
export interface OptimizerExecutionState{
    parameters: StartCalcOptimizerParam
    executionId: number;
    status : CalculationStatus;
    results: OptimizerExecutionResultsState;
}
export interface OptimizerState{
    mode: StateMode
    systemId : number;
    data: SaveOptimizerSystemRequest;
    shrinkSystemName : string;
    shrinkForOptimizer: ShrinkForOptimizerState;
    execution: OptimizerExecutionState;
}
export interface OpenShrinkSystemRequest{
    systemId : number;
    forOptimization: boolean
}
export interface ShrinkSystemLoadResponse{
    shrinkPayload: ShrinkSystemPayload
}
export interface ShrinkForOptimizerState{
    shrinkSystemId: number;
    shrinkPayload: ShrinkSystemPayload
}
export interface DeleteSystemRequest{
    systemId : number;
    systemType : SystemType; 
    systemName : string
}
export interface DeleteSystemResponse extends DeleteSystemRequest{
}
export interface SaveOptimizerSystemResponse{
    systemId : number;
}
export enum NotificationType{
    None = 0,
    Success = 1,
    Info = 2,
    Error = 3
}
export interface NotificationContent{
    type : NotificationType;
    message: string;
}
export interface NotificationState{
    shrinkNotifications : NotificationContent;
    optimizerNotifications : NotificationContent;
    backtestingNotifications : NotificationContent;
}
