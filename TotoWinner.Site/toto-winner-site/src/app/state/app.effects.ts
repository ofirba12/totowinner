import { Injectable, OnInit } from '@angular/core';
import { Observable, of, timer } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as appActions from './app.actions';
//import { RunBacktestingRequest, StopExecutionRequest } from '../backtesting';
import { OptimizerService, TotoGamesService, TemplatesService } from '../_services';
import { OpenShrinkSystemRequest, DeleteSystemRequest } from './app';
import { SaveOptimizerSystemRequest, SystemType, ShrinkSystemPayload } from '../model/interfaces/shared.module';
import { WatchTemplatesRequest, TemplateRequest } from '../model/interfaces/templates.module';

@Injectable()
export class AppEffects {
    constructor(private optimizerService: OptimizerService,
        private totoGamesService: TotoGamesService,
        private templateService: TemplatesService,
        private actions$: Actions) { }

    @Effect()
    openOptimizerSystem$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.OpenOptimizerSystem),
        map((action: appActions.OpenOptimizerSystem) => action.payload),
        mergeMap((systemId: number) =>
            this.optimizerService.loadSystem(systemId).pipe(
                map(loadSystemResponse => (new appActions.OpenOptimizerSystemSuccess(loadSystemResponse))),
                catchError(err => of(new appActions.OpenSystemFail({ systemType: SystemType.Optimizer, name: `${systemId}` })))
            )
        )
    );
    
    @Effect()
    openShrinkSystemForOptimizer$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.OpenShrinkSystemForOptimizer),
        map((action: appActions.OpenShrinkSystemForOptimizer) => action.payload),
        mergeMap((request: OpenShrinkSystemRequest) =>
            this.totoGamesService.loadSystem(request.systemId, request.forOptimization).pipe(
                map(loadSystemResponse => (new appActions.OpenShrinkSystemForOptimizerSuccess(loadSystemResponse))),
                catchError(err => of(new appActions.OpenSystemFail({ systemType: SystemType.Shrink, name: `${request.systemId}` })))
            )
        )
    );
    @Effect()
    openShrinkSystem$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.OpenShrinkSystem),
        map((action: appActions.OpenShrinkSystem) => action.payload),
        mergeMap((request: OpenShrinkSystemRequest) =>
            this.totoGamesService.loadSystem(request.systemId, request.forOptimization).pipe(
                map(loadSystemResponse => (new appActions.OpenShrinkSystemSuccess(loadSystemResponse))),
                catchError(err => of(new appActions.OpenSystemFail({ systemType: SystemType.Shrink, name: `${request.systemId}` })))
            )
        )
    );
    @Effect()
    saveShrinkSystem$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.SaveShrinkSystem),
        map((action: appActions.SaveShrinkSystem) => action.payload),
        mergeMap((request: ShrinkSystemPayload) =>
            this.totoGamesService.saveSystem(request).pipe(
                map(saveSystemResponse => (new appActions.SaveShrinkSystemSuccess(saveSystemResponse, request))),
                catchError(err => of(new appActions.SaveSystemFail(err)))
            )
        )
    );
    @Effect()
    deleteSystem$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.DeleteSystem),
        map((action: appActions.DeleteSystem) => action.payload),
        mergeMap((request: DeleteSystemRequest) =>
            this.totoGamesService.deleteSystem(request.systemId).pipe(
                map(deleteSystemResponse => (new appActions.DeleteSystemSuccess(request))),
                catchError(err => of(new appActions.DeleteSystemFail(err)))
            )
        )
    );
    @Effect()
    saveOptimizerSystem$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.SaveOptimizerSystem),
        map((action: appActions.SaveOptimizerSystem) => action.payload),
        mergeMap((request: SaveOptimizerSystemRequest) =>
            this.optimizerService.saveSystem_v2(request).pipe(
                map(loadSystemResponse => (new appActions.SaveOptimizerSystemSuccess(loadSystemResponse, request.name))),
                catchError(err => of(new appActions.SaveSystemFail(err)))
            )
        )
    );
    @Effect()
    getAllTemplates$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.GetAllTemplates),
        mergeMap(() =>
            this.templateService.getTemplates().pipe(
                map(getTemplatesResponse => (new appActions.GetAllTemplatesSuccess(getTemplatesResponse))),
                catchError(err => of(new appActions.GetAllTemplatesFail(err)))
            )
        )
    );
    @Effect()
    addTemplate$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.AddTemplate),
        map((action: appActions.AddTemplate) => action.payload),
        mergeMap((request: TemplateRequest) =>
            this.templateService.addTemplate(request).pipe(
                map(addTemplatesResponse => (new appActions.AddTemplateSuccess(addTemplatesResponse))),
                catchError(err => of(new appActions.AddTemplateFail(err)))
            )
        )
    );
    @Effect()
    deleteTemplate$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.DeleteTemplate),
        map((action: appActions.DeleteTemplate) => action.payload),
        mergeMap((request: TemplateRequest) =>
            this.templateService.deleteTemplate(request).pipe(
                map(deleteTemplatesResponse => (new appActions.DeleteTemplateSuccess(deleteTemplatesResponse))),
                catchError(err => of(new appActions.DeleteTemplateFail(err)))
            )
        )
    );
    @Effect()
    watchTemplates$: Observable<Action> = this.actions$.pipe(
        ofType(appActions.AppActionTypes.WatchTemplates),
        map((action: appActions.WatchTemplates) => action.payload),
        mergeMap((request: WatchTemplatesRequest) =>
            this.templateService.watchTemplates(request).pipe(
                map(watchTemplatesResponse => (new appActions.WatchTemplatesSuccess(watchTemplatesResponse ))),
                catchError(err => of(new appActions.WatchTemplatesFail(err)))
            )
        )
    );
    
}