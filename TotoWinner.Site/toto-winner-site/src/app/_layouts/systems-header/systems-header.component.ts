import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SelectItem, MessageService, DialogService, Message, ConfirmationService } from 'primeng/api';
import { Router } from '@angular/router';
import { SystemType, ShrinkSystemState, ShrinkSystemPayload, CalculationStatus } from 'src/app/model/interfaces/shared.module';
import { OpenSystemsComponent, SaveSystemComponent, FormDispatcherComponent, SmartDownloadComponent, RealtimeRatesScreenerComponent } from 'src/app/shared/dialogs';
import { Store, select } from '@ngrx/store';
import * as fromApp from './../../state';
import * as appActions from './../../state/app.actions';
import { Observable, Subscription } from 'rxjs';
import { SystemMetaData, NavBarState, OptimizerState, StateMode, NavBarSystemNames, NotificationState, NotificationType, ShrinkState, NavBarSystems } from 'src/app/state/app';
//import { forEach } from '@angular/router/src/utils/collection';
import { OptimizerSystemComponent } from 'src/app/systems/optimizer-system/optimizer-system.component';
import { AppConnetorService, AuthenticationService } from 'src/app/_services';
import { TemplatesManagerComponent } from 'src/app/shared/dialogs/templates-manager/templates-manager.component';
import { ProgramsSyncDialogComponent } from 'src/app/shared/dialogs/programs-sync-dialog/programs-sync-dialog.component';
import { StatisicalDataComponent } from 'src/app/shared/dialogs/statisical-data/statisical-data.component';
import { reject } from 'q';
import { StopExecutionReason } from 'src/app/model/interfaces/optimizer.module';

@Component({
  selector: 'systems-header',
  templateUrl: './systems-header.component.html',
  styleUrls: ['./systems-header.component.css']
})
export class SystemsHeaderComponent implements OnInit, OnDestroy {
  countries: any[];
  systems: SelectItem[];
  selectedSystem: any; //Must be any
  openedSystems: NavBarSystemNames[];
  systems$: Observable<NavBarSystems>;
  systemSubscription$: Subscription;
  optimizer$: Observable<OptimizerState>;
  optimizerSubscription$: Subscription;
  shrink$: Observable<ShrinkState>;
  shrinkSubscription$: Subscription;
  notification$: Observable<NotificationState>;
  notificationSubscription$: Subscription;
  visibleSidebarLeft: boolean = false;
  isAdminUser: boolean = false;
  optimizerSnapshot: OptimizerState;
  shrinkSnapshot: ShrinkState;
  shrinkSystemName: SystemMetaData = null;
  optimizerSystemName: SystemMetaData = null;
  msgs: Message[] = [];
  constructor(private router: Router,
    private messageService: MessageService,
    public dialogService: DialogService,
    private gatewayService: AppConnetorService,
    private authenticationService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private store: Store<fromApp.AppState>) {
    this.systems$ = this.store.pipe(select(fromApp.getSystems));
    this.optimizer$ = this.store.pipe(select(fromApp.getOptimizer));
    this.shrink$ = this.store.pipe(select(fromApp.getShrink));
    this.notification$ = this.store.pipe(select(fromApp.getNotification));

    this.systems = [
      { label: 'ראשי', value: { id: 1, name: 'ראשי', code: 'Shrink' } }
    ];
    this.isAdminUser = this.authenticationService.isAdminUser();
    if (this.isAdminUser) {
      this.systems.push({ label: 'אופטימיזציה', value: { id: 2, name: 'אופטימיזציה', code: 'Optimizer' } });
      this.systems.push({ label: 'Backtesting', value: { id: 3, name: 'Backtesting', code: 'Backtesting' } });
    }
    this.initOpenSystems();
    this.store.dispatch(new appActions.GetAllTemplates());
  }
  initOpenSystems() {
    this.openedSystems = [
      { systemType: SystemType.Shrink, systemNames: [] },
      { systemType: SystemType.Optimizer, systemNames: [] },
      { systemType: SystemType.Backtesting, systemNames: [] },
    ];
    this.shrinkSystemName = null;
    this.optimizerSystemName = null;
  }
  ngOnInit() {
    var self = this;
    this.selectSystem(1); //! Must be 1, otherwise programsRepository will not be initialized
    this.systemSubscription$ = this.systems$
      .subscribe(sys => {
        self.initOpenSystems();
        var sysNames = sys.currentSystemNames[sys.currentSelected.systemType - 1];
        if (sysNames.systemNames.length > 0) {
          self.openedSystems[sysNames.systemType - 1].systemNames.splice(0);
          self.openedSystems[sysNames.systemType - 1].systemNames = sysNames.systemNames;
          if (sysNames.systemType == SystemType.Optimizer) {
            self.shrinkSystemName = self.openedSystems[SystemType.Optimizer - 1].systemNames.find(names => names.systemType == SystemType.Shrink);
            self.optimizerSystemName = self.openedSystems[SystemType.Optimizer - 1].systemNames.find(names => names.systemType == SystemType.Optimizer);
          }
          if (sysNames.systemType == SystemType.Shrink) {
            self.shrinkSystemName = self.openedSystems[SystemType.Shrink - 1].systemNames.find(names => names.systemType == SystemType.Shrink);
          }
        }
      });
    this.optimizerSubscription$ = this.optimizer$
      .subscribe(opt => {
        if (opt != null && opt.mode == StateMode.Saving) {
          this.optimizerSnapshot = opt;
        }
      }
      );
    this.shrinkSubscription$ = this.shrink$
      .subscribe(opt => {
        if (opt != null && opt.mode == StateMode.Saving) {
          this.shrinkSnapshot = opt;
        }
      }
      );
    this.notificationSubscription$ = this.notification$
      .subscribe(notify => {
        this.msgs = [];
        switch (this.selectedSystem.id) {
          case 1:
            if (notify.shrinkNotifications.message)
              this.msgs.push({ severity: this.messageTypeToString(notify.shrinkNotifications.type), summary: '', detail: notify.shrinkNotifications.message });
            break;
          case 2:
            if (notify.optimizerNotifications.message)
              this.msgs.push({ severity: this.messageTypeToString(notify.optimizerNotifications.type), summary: '', detail: notify.optimizerNotifications.message });
            break;
          case 3:
            if (notify.backtestingNotifications.message)
              this.msgs.push({ severity: this.messageTypeToString(notify.backtestingNotifications.type), summary: '', detail: notify.backtestingNotifications.message });
            break;
        }
      });
  }
  messageTypeToString(type: NotificationType) {
    switch (type) {
      case NotificationType.Success:
        return 'success';
      case NotificationType.Error:
        return 'error';
      default:
        return 'info';
    }
  }
  openTemplateManagerDialog() {
    const ref = this.dialogService.open(TemplatesManagerComponent, {
      data: {},
      header: 'ניהול טמפלטים למעקב',
      width: '50%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .save-system-header",
      contentStyle: { "font-size": "11px" }
    });
    ref.onClose.subscribe(() => { //TODO: Use interface
      // this.gatewayService.sendSystemAction(appActions.AppActionTypes.ShrinkSystemClearForm);
      // this.store.dispatch(new appActions.OpenShrinkSystem({ systemId: system.selected.systemId, forOptimization: false }));
    }
    );
  }
  openProgramSyncDialog() {
    const ref = this.dialogService.open(ProgramsSyncDialogComponent, {
      data: {},
      header: 'סינכרון תוכניות',
      //        width: '40%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .sync-program-dialog",
      contentStyle: { "font-size": "11px" }
    });
  }
  openHistoricalDataDialog() {
    const ref = this.dialogService.open(StatisicalDataComponent, {
      data: {},
      header: 'מידע היסטורי',
      width: '90%',
      height: '90%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .sync-program-dialog",
      contentStyle: { "font-size": "11px" }
    });
  }
  openFormDispacherDialog(){
    const ref = this.dialogService.open(FormDispatcherComponent, {
      data: {},
      header: 'שליחת טפסים',
      width: '50%',
      height: '95%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .sync-program-dialog",
      contentStyle: { "font-size": "11px" }      
    });
  }
  openSmartDownloadDialog(){
    const ref = this.dialogService.open(SmartDownloadComponent, {
      data: {},
      header: 'SMART ניהול טפסים',
      width: '50%',
      height: '80%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .sync-program-dialog",
      contentStyle: { "font-size": "11px" }
    });
  }
  openRealtimeRatesScreenerDialog(){
    const ref = this.dialogService.open(RealtimeRatesScreenerComponent, {
      data: {},
      header: 'עידכון יחסים מקוון',
      width: '50%',
      height: '95%',
      rtl: true,
      closeOnEscape: false,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .sync-program-dialog",
      contentStyle: { "font-size": "11px" }      
    });
  }

  onOpenShrinkSystemForOptimization() {
    this.openSystemsDialog(SystemType.Shrink);
  }
  onOpenSystem() {
    switch (this.selectedSystem.id) {
      case 1:
        this.openSystemsDialog(SystemType.Shrink);
        break;
      case 2:
        this.openSystemsDialog(SystemType.Optimizer);
        break;
    }
  }
  openSystemsDialog(systemType: SystemType) {
    const ref = this.dialogService.open(OpenSystemsComponent, {
      data: {
        systemType: systemType
      },
      header: 'בחר מערכת',
      width: '40%',
      rtl: true,
      closeOnEscape: true,
      styleClass: ".ui-dialog .ui-dialog-titlebar .ui-dialog-title .save-system-header",
      contentStyle: { "max-height": "350px", "overflow": "auto" }
    });

    ref.onClose.subscribe((system: any) => { //TODO: Use interface
      if (system && system.selected && !system.deleteAction) {
        if (systemType == SystemType.Optimizer)
          this.store.dispatch(new appActions.OpenOptimizerSystem(system.selected.systemId));
        else if (systemType == SystemType.Shrink && this.selectedSystem.id == SystemType.Optimizer)
          this.store.dispatch(new appActions.OpenShrinkSystemForOptimizer({ systemId: system.selected.systemId, forOptimization: true }));
        else if (systemType == SystemType.Shrink && this.selectedSystem.id == SystemType.Shrink) {
          this.gatewayService.sendSystemAction(appActions.AppActionTypes.ShrinkSystemClearForm);
          this.store.dispatch(new appActions.OpenShrinkSystem({ systemId: system.selected.systemId, forOptimization: false }));
        }
      }

      if (system && system.selected && system.deleteAction) {
        this.store.dispatch(new appActions.DeleteSystem({
          systemId: system.selected.systemId,
          systemName: system.selected.name,
          systemType: systemType
        }));
      }
    });
  }

  onSaveSystem() {
    switch (this.selectedSystem.id) {
      case 1:
        this.gatewayService.sendSystemAction(appActions.AppActionTypes.SnapshotShrinkSystem);
        this.saveSystemsDialog(SystemType.Shrink);
        break;
      case 2:
        if (this.openedSystems[1].systemNames.length == 0) {
          this.store.dispatch(new appActions.ShowMessage({ type: NotificationType.Info, message: 'אין שום מערכת טעונה' }));
        }
        this.gatewayService.sendSystemAction(appActions.AppActionTypes.SnapshotOptimizerSystem);
        this.saveSystemsDialog(SystemType.Optimizer);
        break;
    }
  }
  saveSystemsDialog(systemType: SystemType) {
    var nameToSaveItem = this.openedSystems[systemType - 1].systemNames.find(name => name.systemType == systemType);
    var nameToSave = nameToSaveItem ? nameToSaveItem.name : '';
    const ref = this.dialogService.open(SaveSystemComponent, {
      data: {
        systemType: systemType,
        systemName: nameToSave
      },
      header: 'שמור מערכת',
      width: '40%',
      rtl: true,
      closeOnEscape: true,
      contentStyle: { "overflow": "auto" }
    });

    ref.onClose.subscribe((systemName: string) => {
      if (systemName) {
        if (systemType == SystemType.Optimizer) {
          this.optimizerSnapshot.data.name = systemName;
          this.optimizerSnapshot.data.execution = this.optimizerSnapshot.execution;
          this.store.dispatch(new appActions.SaveOptimizerSystem(this.optimizerSnapshot.data));
        }
        else if (systemType == SystemType.Shrink) {
          var payload: ShrinkSystemPayload = {
            name: systemName,
            uniqueId: this.shrinkSnapshot.toolbar.selectedProgram.value.uniqueId,
            year: this.shrinkSnapshot.toolbar.keys.selectedYear,
            definitions: this.shrinkSnapshot.definitions,
            result: this.shrinkSnapshot.result
          }
          this.store.dispatch(new appActions.SaveShrinkSystem(payload));
        }
      }
    });

  }
  onSystemClick($event) {
    var self = this;
    var currentSelectedSystemId = self.selectedSystem.id;
    if (currentSelectedSystemId == SystemType.Backtesting || currentSelectedSystemId == SystemType.Optimizer) {
      var msg: string = currentSelectedSystemId == SystemType.Optimizer ?
        "מעבר למערכת יגרום לעצירת ריצה קיימת. האם להמשיך?" :
        "מעבר למערכת יגרום למחיקת נתונים ועצירת ריצה קיימת. האם להמשיך?";
      this.confirmationService.confirm({
        header: "התראת מעבר מערכת",
        message: msg,
        accept: () => {
          if (currentSelectedSystemId == SystemType.Optimizer) {
            var stopReason: StopExecutionReason = { manualStop: true, status: CalculationStatus.Stopped };
            var optimizer = self.gatewayService.getOptimizerComponent();
            if (optimizer.execution) {
              optimizer.stopOptimizer(stopReason);
            }
          }
          else if (currentSelectedSystemId == SystemType.Backtesting) {
            var backtesting = self.gatewayService.getBacktestingComponent();
            if (backtesting.execution) {
              backtesting.stop();
            }
            backtesting.clearState();
            // setTimeout(function () {
            //   backtesting.clearState();
            // }, 500);
          }
          this.selectSystem($event.option.value.id);
        },
        reject: () => {
          setTimeout(function () {
            self.selectedSystem = self.systems[currentSelectedSystemId - 1].value;
          }, 500);
        }
      });
    }
    else {
      this.selectSystem($event.option.value.id);
    }
  }
  selectSystem(systemTypeId: number) {
    switch (systemTypeId) {
      case 1:
        this.router.navigateByUrl('/');
        break;
      case 2:
        this.router.navigateByUrl('/optimizer');
        break;
      case 3:
        this.router.navigateByUrl('/backtesting');
        break;
      default:
        systemTypeId = 1;
        this.router.navigateByUrl('/');
        break;
    }
    this.selectedSystem = this.systems[systemTypeId - 1].value;
    this.store.dispatch(new appActions.SelectSystem({ systemType: systemTypeId, name: this.systems[systemTypeId - 1].label }));
  }
  ngOnDestroy() {
    if (this.systemSubscription$)
      this.systemSubscription$.unsubscribe();
    if (this.shrinkSubscription$)
      this.shrinkSubscription$.unsubscribe();
    if (this.optimizerSubscription$)
      this.optimizerSubscription$.unsubscribe();
    if (this.notificationSubscription$)
      this.notificationSubscription$.unsubscribe();
  }
}
