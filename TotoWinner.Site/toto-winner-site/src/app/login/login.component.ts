import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;

  errorMessage: string;
  returnUrl: string;
  username: string;
  password: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // reset login status
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    var username = this.authenticationService.login(this.f.username.value, this.f.password.value);
    if (username) {
      if (this.authenticationService.isSmartUser()) {
        this.router.navigate(['/download'], { queryParams: { returnUrl: '/' } });
      }
      else {
        this.router.navigate([this.returnUrl]);
      }
    }
    else {
      this.errorMessage = "שם משתמש או סיסמה לא תקינים";
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
      this.loading = false;
    }
  }
}
