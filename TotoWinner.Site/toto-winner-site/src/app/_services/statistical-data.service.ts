import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HttpUtils } from './http.utils';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { GetStatisticalDataRequest } from '../model/interfaces/statisical-data.module';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class StatisticalDataService extends HttpUtils {
  constructor(private http: HttpClient) { super(); }

  getData(request: GetStatisticalDataRequest) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/StatisticalData/GetData";
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
}
