import { Injectable } from '@angular/core';
import { HttpUtils } from './http.utils';
import { AppHelperService } from './app-helper.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ProgramType } from '../state/app';
import { ProgramDataSyncRequest, ProgramDataRequest, RealTimePipeGetRequest, RealTimePipeGetResponse, RatesPipe, RealTimePipeSetRequest } from '../model/interfaces/program.module';
import { ResponseBase } from '../model/interfaces/shared.module';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ProgramsService extends HttpUtils {
  constructor(private http: HttpClient,
    private helper: AppHelperService) {
    super();
  }
  getProgramRatesScreener(type: ProgramType) : Observable<RealTimePipeGetResponse>{
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Programs/GetRealTimePipe";
    var payload: string;
    var request: RealTimePipeGetRequest;
    request = {
      programType: type
    }
    payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));    
  }
  updateProgramRatesScreener(type: ProgramType, newRates: RatesPipe[]) : Observable<ResponseBase>{
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Programs/SetRealTimePipe";
    var payload: string;
    var request: RealTimePipeSetRequest;
    request = {
      programType: type,
      rates : newRates
    }
    payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));    
  }
  
  getPrograms_BankrimAPI(programType: string, selectedProgramYear: number) {
    if (environment.offline) {
      var url = "assets/offline-data/programs.json";
      return this.http.get<any>(url);
    }
    else {
      var lastProgramDate = new Date();
      var currentYear = lastProgramDate.getFullYear();
      var lastProgramDateStr = this.helper.getFormatedDate(lastProgramDate, "YYYY-MM-DD");
      if (currentYear != selectedProgramYear)
        lastProgramDateStr = selectedProgramYear.toString() + "-12-31";
      var url = "https://www.bankerim.co.il/api/totoGames/getPrograms?date=" + lastProgramDateStr + "&gameType=" + programType;
      return this.http.get<any>(url)
        .pipe(catchError(this.handleError));
    }
  }

  getProgram(roundEndDate: Date, programType: ProgramType) {
    if (environment.offline) {
      var url = "assets/offline-data/program-winner.json";
      return this.http.get<any>(url);
    }
    else {
      var date = new Date(roundEndDate);
      var payload = this.generateWinnerPayload(date, programType);
      var url = environment.winnerSiteDomain + "/TotoWinner/api/Programs/GetProgram";
      return this.http.post<any>(url, payload, httpOptions)
        .pipe(catchError(this.handleError));
    }
  }
  generateWinnerPayload(roundEndDate: Date, programType: ProgramType): string {
    var payload: string;
    var programDataRequest: ProgramDataRequest;
    programDataRequest = {
      ProgramEndDate: this.helper.getFormatedDate(roundEndDate, "YYYY-MM-DD"),
      ProgramType: programType
    }
    payload = JSON.stringify(programDataRequest);
    return payload;
  }
  syncProgram_Api(roundEndDate: Date, programType: ProgramType, totoRoundId: number) {
      var date = new Date(roundEndDate);
      var syncRequest: ProgramDataSyncRequest = {
        ProgramEndDate: this.helper.getFormatedDate(date, "YYYY-MM-DD"),
        ProgramType: programType,
        TotoRoundId: totoRoundId  
      }
      var payload = payload = JSON.stringify(syncRequest);
      var url = environment.winnerSiteDomain + "/TotoWinner/api/Programs/SyncProgram";
      return this.http.post<any>(url, payload, httpOptions)
        .pipe(catchError(this.handleError));
  }
}
