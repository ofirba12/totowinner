import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  users: any[] = [
    { username: "ben", password: "147258" , role: "user"},
    { username: "admin", password: "8828685" , role: "admin"},
    { username: "smart", password: "1234" , role: "smart"},
  ];
  constructor() { }

  login(username: string, password: string) : string {
    let authenticatedUser = { username: "", password: "", role:"user" };
    let filteredUsers = this.users.filter(user => {
      return user.username === username.trim().toLowerCase() && user.password === password;
    });
    if (filteredUsers.length) {
      authenticatedUser = filteredUsers[0];
      sessionStorage.setItem('currentUser', authenticatedUser.username)
      sessionStorage.setItem('userRole', authenticatedUser.role)
    }
    return authenticatedUser.username;
  }

  logout() {
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem('userRole');
  }
  isAdminUser() : boolean {
    return sessionStorage.getItem('userRole') === "admin";
  }
  isSmartUser() : boolean{
    return sessionStorage.getItem('userRole') === "smart";
  }
}
