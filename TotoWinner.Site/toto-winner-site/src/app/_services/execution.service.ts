import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ExecutionStateRequest, StartOptimizerRequest, StopOptimizerRequest, OptimizerResultsToExcelRequest } from '../model/interfaces/shared.module';
import { environment } from 'src/environments/environment';
import { HttpUtils } from './http.utils';
import { catchError } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { StartCalcOptimizerParam } from '../model/interfaces/optimizer.module';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ExecutionService extends HttpUtils {
  private executionConnector$ = new BehaviorSubject(null);

  constructor(private http: HttpClient) {
    super();
  }

  getExecutionState(executionId: number) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Optimizer/GetExecutionStatus";
    //var request = this.generateExecutionStateRequest(executionId);
    var request: ExecutionStateRequest = {executionId : executionId};
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  sendExecutionData(state: any) { // TODO: make sure to create relevant interface
    this.executionConnector$.next(state);
  }
  getExecutionData() : Observable<any> { // TODO: make sure to create relevant interface
    return this.executionConnector$.asObservable();
  }
  startOptimizer(systemId: number, params: StartCalcOptimizerParam): Observable<any> { // TODO: make sure to create relevant interface
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Optimizer/StartOptimizer";
    var request = this.generateStartOptimizerRequest(systemId, params);
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  private generateStartOptimizerRequest(systemId: number, params: StartCalcOptimizerParam): StartOptimizerRequest {
    var startOptimizerRequest: StartOptimizerRequest;
    startOptimizerRequest = {
      parameters: {
        optimizedSystemId: systemId,
        targetBetsCounter: params.targetBetsCounter,
        targetAdditionalConditionCounter: params.targetConditionCounter
      }
    }
    return startOptimizerRequest;
  }

  stopOptimizer(executionId: number): Observable<any> { // TODO: make sure to create relevant interface
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Optimizer/StopOptimizer";
    var request: StopOptimizerRequest = {executionId : executionId};
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );  
  }
  optimizerResultToExcel(executionId: number, systemId: number): Observable<any>{ // TODO: make sure to create relevant interface
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Optimizer/ResultsToExcel";
    var request: OptimizerResultsToExcelRequest = {
      executionId : executionId,
      optimizerSystemId : systemId
    }
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );  
  }
}
