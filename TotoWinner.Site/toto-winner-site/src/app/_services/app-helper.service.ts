import { Injectable } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { ProgramType } from '../state/app';

@Injectable({
  providedIn: 'root'
})
export class AppHelperService {
  private years: number[] = [];
  private yearsForUI: SelectItem[] = [];
  public programTypesForUI: SelectItem[] = [
    { label: 'ווינר 16', value: ProgramType.Winner16 },//96
    { label: 'ווינר מחצית', value: ProgramType.WinnerHalf }, //82
    { label: 'ווינר עולמי', value: ProgramType.WinnerWorld } //87
  ];
  constructor() { }
  
  public getFormatedDate(date: Date, format: string): string {
    var formatedDate = "";
    switch (format) {
      case "YYYY-MM-DD":
        formatedDate = [
          date.getFullYear(),
          (date.getMonth() + 1).toLocaleString('en-US', { style: 'decimal', minimumIntegerDigits: 2, useGrouping: false }),
          date.getDate().toLocaleString('en-US', { style: 'decimal', minimumIntegerDigits: 2, useGrouping: false })
        ].join('-');
        break;
      case "DD.MM.YYYY":
        formatedDate = [
          date.getDate().toLocaleString('en-US', { style: 'decimal', minimumIntegerDigits: 2, useGrouping: false }),
          (date.getMonth() + 1).toLocaleString('en-US', { style: 'decimal', minimumIntegerDigits: 2, useGrouping: false }),
          date.getFullYear()
        ].join('.');
        break;
    }
    return formatedDate;
  }

  public getYearsForUI(): SelectItem[] {
    var self = this;
    if (this.yearsForUI.length == 0) {
      this.getYears()
        .forEach(item => self.yearsForUI.push({ label: `${item}`, value: item }));
    }
    return this.yearsForUI;
  }

  public getYears(): number[] {
    if (this.years.length == 0) {
      this.setYearsCollection();
    }
    return this.years;
  }
  private setYearsCollection(): void {
    var currentYear = new Date().getFullYear();
    for (let year = 2017; year <= currentYear; year++) {
      this.years.push(year);
    }
  }
}
