import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HttpUtils } from './http.utils';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { RunBacktestingRequest, RunBacktestingResponse, BacktestingExecution, BacktestingExecutionRequest, StopExecutionRequest, StopExecutionResponse, PollExecutionRequest, ShowBacktestingPastProgramRequest, BacktestingPastProgramSelected, BtRowData } from '../systems/backtesting/backtesting';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BacktestingService extends HttpUtils {
  public rowsExecutionData: BtRowData[];
  constructor(private http: HttpClient) { super(); }
  
  //#region Run Backtesting
  runBacktesting(runRequest: RunBacktestingRequest): Observable<RunBacktestingResponse> {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Backtesting/RunBacktesting";
    var request: RunBacktestingRequest = {
      definitions: runRequest.definitions
    }    
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        tap(data => console.log('runBacktesting: ' + payload)),
        catchError(this.handleError)
      );
  }

  //#region Poll Backtesting
  pollBacktesting(request : PollExecutionRequest): Observable<BacktestingExecution> {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Backtesting/GetExecutionStatus";
    //debugger;
    //request.fromIndex = this.rowsExecutionData.length;
    //var request: PollExecutionRequest = { executionId: executionId, fromIndex: fromRowIndex };
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        tap(data => console.log('pollBacktesting: ' + payload)),
        catchError(this.handleError)
      );
  }

  //#region Stop Backtesting
  stopBacktesting(stopRequest: StopExecutionRequest): Observable<StopExecutionResponse> {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Backtesting/StopBacktesting";
    var request: StopExecutionRequest = {
      executionId: stopRequest.executionId
    }
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        tap(data => console.log('stopBacktesting: ' + payload)),
        catchError(this.handleError)
      );
  }
  getPastProgram(request: ShowBacktestingPastProgramRequest) : Observable<BacktestingPastProgramSelected> {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Backtesting/GetPastProgram";
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        tap(data => console.log('getPastProgram: ' + payload)),
        catchError(this.handleError)
      );
  }
}
