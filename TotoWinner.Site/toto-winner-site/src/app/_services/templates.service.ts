import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HttpUtils } from './http.utils';
import { Store } from '@ngrx/store';
import * as fromApp from './../state';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { WatchTemplatesRequest, TemplateRequest } from '../model/interfaces/templates.module';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TemplatesService extends HttpUtils {

  constructor(private http: HttpClient,
    public store: Store<fromApp.AppState>) {
    super();
  }
  
  getTemplates() {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Templates/GetAllTemplates";
    var payload = {};
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
  addTemplate(request: TemplateRequest) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Templates/AddTemplate";
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
  deleteTemplate(request: TemplateRequest) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Templates/DeleteTemplate";
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
  watchTemplates(request: WatchTemplatesRequest) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Templates/WatchTemplates";
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
}
