import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ProgramDataRequest, ProgramData, ProgramDetails, ProgramDataSyncRequest } from '../model/interfaces/program.module';
import { ShrinkSystemData, LoadShrinkSystemRequest, GetAllSystemsRequest, ShrinkSystemUIState, SystemType, ShrinkSystemPayload, UnionSystemsRequest, UnionSystemsResponse, FormTypeGeneration, GenerateFormToDownloadResponse, GenerateFormToDownloadRequest, GetAllSystemsForUnionRequest } from '../model/interfaces/shared.module';
import { HttpUtils } from './http.utils';
import { BetsShrinkSystemComponent } from '../systems/bets-shrink-system/bets-shrink-system.component';
import { Store } from '@ngrx/store';
import * as fromApp from './../state';
import { ProgramType } from '../state/app';
import { AppHelperService } from './app-helper.service';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TotoGamesService extends HttpUtils {
  constructor(private http: HttpClient,
    public store: Store<fromApp.AppState>,
    private helper: AppHelperService) {
    super();
  }

  saveSystem(payloadObject: ShrinkSystemPayload) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/SaveShrinkSystem";
    var payload = JSON.stringify(payloadObject);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
  loadSystem(systemId: number, forOptimization: boolean) {
    var url: string;
    if (forOptimization == true)
      url = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/LoadSystemForOptimization";
    else
      url = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/LoadShrinkSystem";

    var loadSystemRequest: LoadShrinkSystemRequest = { systemId : systemId };
    var payload = JSON.stringify(loadSystemRequest);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
  
  public generateBetsFormCreateRequest(shrinkSystem: BetsShrinkSystemComponent, generateOutputFile: boolean): BetsCalculation.FormCreateRequest {
    var betsFormCreateRequest: BetsCalculation.FormCreateRequest;
    betsFormCreateRequest = {
      eventsBets: [],
      eventsRates: [],
      formSettings: {
        includeSettings: {
          amountSum: { min: 14, max: 100 },
          amount1: { min: 0, max: 14 },
          amountX: { min: 0, max: 14 },
          amount2: { min: 0, max: 14 },
          sequence1: { min: 0, max: 14 },
          sequenceX: { min: 0, max: 14 },
          sequence2: { min: 0, max: 14 },
          sequenceBreak: { min: 1, max: 14 },
        },
        excludeSettings: {
          amountSum: { min: 0, max: 100 },
          amount1: { min: 0, max: 0 },
          amountX: { min: 0, max: 0 },
          amount2: { min: 0, max: 0 },
          sequence1: 0,
          sequenceX: 0,
          sequence2: 0,
          sequenceBreak: { min: 0, max: 0 },
          activateSumAmountFilter: false,
          activateOneAmountFilter: false,
          activateXAmountFilter: false,
          activateTwoAmountFilter: false,
          activateOneSequenceFilter: false,
          activateXSequenceFilter: false,
          activateTwoSequenceFilter: false,
          activateBreakFilter: false
        },
        combination1X2Settings: null,
        combinationLettersSettings: null,
        shapes2: null,
        shapes3: null,
        lettersShapes2: null,
        lettersShapes3: null
      },
      cleanOnDiff: false,
      generateOutputFile: generateOutputFile
    }
    shrinkSystem.gamesComponent.fillEvents(betsFormCreateRequest.eventsBets, betsFormCreateRequest.eventsRates);
    shrinkSystem.formComponent.fillIncludeSettings(betsFormCreateRequest.formSettings);
    betsFormCreateRequest.cleanOnDiff = shrinkSystem.formComponent.basicSubsets.cleanOnDiff;
    return betsFormCreateRequest;
  }
  calculateBets(shrinkSystem: BetsShrinkSystemComponent, generateOutputFile: boolean) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/CalculateBets";
    var betsFormCreateRequest = this.generateBetsFormCreateRequest(shrinkSystem, generateOutputFile);
    var payload = JSON.stringify(betsFormCreateRequest);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));
  }
  BetsResultToExcel(calcResult: any, cleanOnDiff: boolean) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/ResultsToExcel";
    var request = this.generateResultToExcelRequest(calcResult, cleanOnDiff);
    var payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  generateResultToExcelRequest(calcResult: any, cleanOneDiff: boolean): any {
    var request: any;
    request = {
      seResults: calcResult,
      cleanOneDiff: cleanOneDiff
    }
    return request;
  }

  //#region Delete System
  deleteSystem(systemId: number) {
    var url = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/DeleteSystem";
    //var loadSystemRequest = this.generateLoadSystemRequest(systemId);
    var loadSystemRequest: LoadShrinkSystemRequest = { systemId : systemId };
    var payload = JSON.stringify(loadSystemRequest);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  //#endregion

  //#region Get All Systems
  generateGetAllSystemsRequest(systemType: string): GetAllSystemsRequest {
    var loadRequest: GetAllSystemsRequest;
    loadRequest = {
      type: systemType
    }
    return loadRequest;
  }
  getAllSystems(systemType: string) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/TotoWinnerBets/GetAllSystems";

    var getSystemsRequest = this.generateGetAllSystemsRequest(systemType);
    var payload = JSON.stringify(getSystemsRequest);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  
  //#endregion

}
