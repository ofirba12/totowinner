import { Injectable } from '@angular/core';
import { ProgramData } from '../model/interfaces/program.module';
import { TotoGamesService } from './toto-games.service';
import { BehaviorSubject } from 'rxjs';
import { AppActionTypes } from '../state/app.actions';
import { PricerData } from '../model/pricer-data';
import { OptimizerSystemComponent } from '../systems/optimizer-system/optimizer-system.component';
import { BacktestingSystemComponent } from '../systems/backtesting/components/backtesting-system/backtesting-system.component';

@Injectable({
  providedIn: 'root'
})
export class AppConnetorService {
  private systemActionsSubject$ = new BehaviorSubject(null); 
  private gameSelectionSubject = new BehaviorSubject(null);
  private pricerDataSubject = new BehaviorSubject(null);
  private optimizerComponent : OptimizerSystemComponent;
  private backtestingComponent : BacktestingSystemComponent;
  
  constructor() { }
  registerBacktesting(system : BacktestingSystemComponent){
    this.backtestingComponent = system;
  }
  getBacktestingComponent() : BacktestingSystemComponent{
    return this.backtestingComponent;
  }
  clearBacktestingState() : void{
    this.backtestingComponent.clearState();
  }
  registerOptimizer(system : OptimizerSystemComponent){
    this.optimizerComponent = system;
  }
  getOptimizerComponent() : OptimizerSystemComponent{
    return this.optimizerComponent;
  }
  getSystemActionSubject() {
    return this.systemActionsSubject$.asObservable();
  }
  sendSystemAction(action: AppActionTypes) {
    this.systemActionsSubject$.next(action);
  }
  destroyActionSubject(){
    this.systemActionsSubject$.complete();
    this.systemActionsSubject$ = new BehaviorSubject(null)
  }
  sendProgramDetail(programData: ProgramData) {
    this.gameSelectionSubject.next(programData);
  }
  getGameSelectionSubject() {
    return this.gameSelectionSubject.asObservable();
  }
  sendPricerData(pricerData: PricerData) {
    this.pricerDataSubject.next(pricerData);
  }
  getPricerDataSubject() {
    return this.pricerDataSubject.asObservable();
  }
  
}
