import { Injectable } from '@angular/core';
import { HttpUtils } from './http.utils';
import { AppHelperService } from './app-helper.service';
import { FormTypeGeneration, GenerateFormToDownloadResponse, GenerateFormToDownloadRequest, UnionSystemsResponse, UnionSystemsRequest, ResponseBase, DeleteSmartFileRequest, GetSmartFilesResponse, GetAllSystemsForUnionRequest } from '../model/interfaces/shared.module';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ProgramType } from '../state/app';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class DispatchFormService extends HttpUtils {
  constructor(private http: HttpClient,
    private helper: AppHelperService) {
    super();
  }
  GetAllSystemsForUnion(programType: ProgramType) {
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/DispatchForm/GetAllSystemsForUnion";
    var loadRequest: GetAllSystemsForUnionRequest;
    loadRequest = {
      type: programType
    }
    var payload = JSON.stringify(loadRequest);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  generateUnionSystemFormToDownload(unionId: number, formTypeToGenerate: FormTypeGeneration) : Observable<GenerateFormToDownloadResponse>{
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/DispatchForm/GenerateFormToDownload";
    var payload: string;
    var request: GenerateFormToDownloadRequest;
    request = {
      unionResultId: unionId,
      formType: formTypeToGenerate
    }
    payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));    
  }
  unionSystems(systemIdsForUnion: number[]) : Observable<UnionSystemsResponse>{
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/DispatchForm/UnionSystems";
    var payload: string;
    var unionSystemsRequest: UnionSystemsRequest;
    unionSystemsRequest = {
      systemIds: systemIdsForUnion
    }
    payload = JSON.stringify(unionSystemsRequest);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));    
  }
  
  deleteSmartFile(smartId: number) : Observable<ResponseBase>{
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/DispatchForm/DeleteSmartFile";
    var payload: string;
    var request: DeleteSmartFileRequest;
    request = {
      id: smartId
    }
    payload = JSON.stringify(request);
    return this.http.post<any>(url, payload, httpOptions)
      .pipe(catchError(this.handleError));    
  }

  getSmartFiles() : Observable<GetSmartFilesResponse>{
    var url: string = environment.winnerSiteDomain + "/TotoWinner/api/DispatchForm/GetSmartFiles";
    return this.http.post<any>(url, "", httpOptions)
      .pipe(catchError(this.handleError));    
  }

}
