import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpUtils } from './http.utils';
import { SystemType, SaveOptimizerSystemRequest, LoadOptimizerSystemRequest } from '../model/interfaces/shared.module';
import { OptimizerSystemComponent } from '../systems/optimizer-system/optimizer-system.component';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class OptimizerService extends HttpUtils {

  constructor(private http: HttpClient) { super(); }
    //#region Load System
    loadSystem(systemId: number) { 
      var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Optimizer/LoadOptimizerSystem"; 
      //var loadSystemRequest = this.generateLoadSystemRequest(systemId);
      var loadSystemRequest: LoadOptimizerSystemRequest = { systemId : systemId };
      var payload = JSON.stringify(loadSystemRequest);
      return this.http.post<any>(url, payload, httpOptions)
        .pipe(
          catchError(this.handleError)
        );
    }
    //#endregion

    //#region Save System
    saveSystem_v2(saveRequest: SaveOptimizerSystemRequest)
    {
      var url: string = environment.winnerSiteDomain + "/TotoWinner/api/Optimizer/SaveOptimizerSystem";
      var payload = JSON.stringify(saveRequest);
      return this.http.post<any>(url, payload, httpOptions)
        .pipe(
          catchError(this.handleError)
        );
    }
    //#endregion
}
