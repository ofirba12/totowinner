import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitButton';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { PanelModule } from 'primeng/panel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { DataViewModule } from 'primeng/dataview';
import { CardModule } from 'primeng/card';
import { TooltipModule } from 'primeng/tooltip';
import { MessageService, ConfirmationService, DialogService } from 'primeng/api';
import { CheckboxModule } from 'primeng/checkbox';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { KeyFilterModule } from 'primeng/keyfilter';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {InputSwitchModule} from 'primeng/inputswitch';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {SpinnerModule} from 'primeng/spinner';
import {FieldsetModule} from 'primeng/fieldset';
import {ProgressBarModule} from 'primeng/progressbar';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {SidebarModule} from 'primeng/sidebar';
import {CodeHighlighterModule} from 'primeng/CodeHighlighter'
import {InputTextModule} from 'primeng/inputtext'

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    ToolbarModule,
    ButtonModule,
    SplitButtonModule,
    DropdownModule,
    TableModule,
    PanelModule,
    SelectButtonModule,
    TabViewModule,
    ToastModule,
    DataViewModule,
    CardModule,
    TooltipModule,
    CheckboxModule,
    BlockUIModule,
    ProgressSpinnerModule,
    KeyFilterModule,
    MessagesModule,
    MessageModule,
    InputSwitchModule,
    ConfirmDialogModule,
    SpinnerModule,
    FieldsetModule,
    ProgressBarModule,
    TieredMenuModule,
    DynamicDialogModule,
    DialogModule,
    CalendarModule,
    OverlayPanelModule,
    SidebarModule,
    CodeHighlighterModule,
    InputTextModule
  ],
  providers: [
    MessageService,
    ConfirmationService,
    DialogService
  ],
})

export class PrimeNgModule {}