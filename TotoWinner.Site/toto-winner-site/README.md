# TotoWinnerSite Porduction deployment 
ng build --prod --base-href /TotoWinnerSite/
###### Once copied to prod server, make sure to copy previous Output folder to current
# TotoWinnerSite local deployment 
ng build --base-href /TotoWinnerSite/

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.
## Regressions
look for ###, and fetch all the code blocks that needs regression

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

ng g c components/_directives/minmax -d
ng g p _pipes/seconds-to-time
ng g s /_services/backtesting -d
ng g c /shared/dialogs/templates-manager/templates-list -d

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## migrate 8 to 9 NOT POSSIBLE SINCE NGRX DOES NOT SUPPORT V9 YES (Dec 2019)
ng update @angular/core@8 @angular/cli@8 #migrate to latest 8
ng update @angular/cli @angular/core --next
npm install primeng@latest --save

## Upgrade from angular 7 to 8
npm install @angular/cli@8.3.7  --save-dev
npm install @angular/http@7.2.15  --save
npm install @angular/cdk@8.2.2  --save

npm install primeng@latest --save
npm install primeicons@latest --save

ng update @angular/core --from 7 --to 8 --migrate-only #this will fixes the code
