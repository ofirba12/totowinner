﻿CREATE PROCEDURE [dbo].[SystemState_Merge]
	@Name	NVARCHAR(400),
	@Value	NVARCHAR(500)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[SystemState] AS [Target]
	USING ( SELECT	 @Name	AS	"Name"
					,@Value	AS	"Value"
	) AS [Source]	ON [Source].[Name]	= [Target].[Name]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Value]		= [Source].[Value]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Name]
				,[Value])
		VALUES ([Source].[Name]
				,[Source].[Value]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
