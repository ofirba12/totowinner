﻿CREATE PROCEDURE [dbo].[SubscribersUrls_Insert]
	@UrlGuid		UNIQUEIDENTIFIER,
	@TipId			INT,
	@SubscriberId	BIGINT
AS
	INSERT INTO SubscribersUrls (UrlGuid, TipId, SubscriberId, VisitUrlTS, PickedUpByChatServer) 
		VALUES (@UrlGuid, @TipId, @SubscriberId, NULL, NULL)
RETURN 0