﻿CREATE TABLE [dbo].[SubscribersUrls]
(
	[UrlId]					INT IDENTITY(1000,1)	NOT NULL,
	[UrlGuid]				UNIQUEIDENTIFIER		NOT NULL,
	[TipId]					INT						NOT NULL,
	[SubscriberId]			BIGINT					NOT NULL,
	[VisitUrlTS]			DATETIME,
	[PickedUpByChatServer]	DATETIME				
)
