﻿CREATE PROCEDURE [dbo].[SubscribersUrls_Visit]
	@UrlGuid		UNIQUEIDENTIFIER

AS
	UPDATE SubscribersUrls 
	SET VisitUrlTS = GetDate(),
		PickedUpByChatServer = NULL
	WHERE UrlGuid=@UrlGuid


RETURN 0
