﻿CREATE PROCEDURE [dbo].[Tippers_Merge]
	@TipperId			INT,			
	@TipperName			NVARCHAR(400)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @Updated DATETIME = GetDate();

	MERGE dbo.[Tippers] AS [Target]
	USING ( SELECT	 @TipperId			AS	"TipperId"
					,@TipperName		AS	"TipperName"
	) AS [Source]	ON [Source].[TipperId] = [Target].[TipperId]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[TipperName]			=[Source].[TipperName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TipperName])
		VALUES (
			[Source].[TipperName]
			)
			OUTPUT Inserted.TipperId
			;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END