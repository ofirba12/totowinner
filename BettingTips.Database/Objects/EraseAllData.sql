﻿CREATE PROCEDURE [dbo].[EraseAllData]
AS
	DELETE FROM TipsAudit;
	DELETE FROM SubscribersUrls;
	DELETE FROM Tips;
	DELETE FROM Programs;

RETURN 0
