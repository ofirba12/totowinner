﻿CREATE TABLE [dbo].[TipsAudit]
(
	[ProgramId]		BIGINT			NOT NULL,
	[GameIndex]		INT				NOT NULL,
	[AuditTS]		DATETIME		NOT NULL,
	[AuditDetails]	NVARCHAR(MAX)	NOT NULL
)
