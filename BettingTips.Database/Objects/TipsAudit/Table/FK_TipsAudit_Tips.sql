﻿ALTER TABLE [dbo].[TipsAudit]
	ADD CONSTRAINT [FK_TipsAudit_Tips]
	FOREIGN KEY ([ProgramId], [GameIndex])
	REFERENCES [Tips] ([ProgramId], [GameIndex])
