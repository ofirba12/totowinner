﻿CREATE PROCEDURE [dbo].[Tips_Audit]
	@ProgramId BIGINT,
    @GameIndex INT,
    @AuditTS DATETIME,
    @AuditDetails NVARCHAR(MAX)
AS
INSERT INTO [dbo].[TipsAudit]
           ([ProgramId]
           ,[GameIndex]
           ,[AuditTS]
           ,[AuditDetails])
     VALUES
           (@ProgramId
           ,@GameIndex
           ,@AuditTS
           ,@AuditDetails)
RETURN 0
