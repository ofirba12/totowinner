﻿CREATE TABLE [dbo].[Subscribers]
(
	[Id]						INT IDENTITY(10000,1)	NOT NULL,
	[SubscriberChatId]			BIGINT NULL,
	[FirstName]					NVARCHAR(300) NOT NULL,
	[LastName]					NVARCHAR(300) NOT NULL,
	[CellNumber]				NVARCHAR(300) NOT NULL,
	[Mail]						NVARCHAR(500) NOT NULL,
	[RegisteredDate]			DATETIME NOT NULL,
	[Remarks]					NVARCHAR(MAX),
	[BlockedDate]				DATETIME,
	[BlockageReason]			NVARCHAR(MAX),
)
