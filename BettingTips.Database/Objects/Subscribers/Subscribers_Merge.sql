﻿CREATE PROCEDURE [dbo].[Subscribers_Merge]
	@Id					INT,
	@SubscriberChatId	BIGINT,
	@FirstName			NVARCHAR(300),
	@LastName			NVARCHAR(300),
	@CellNumber			NVARCHAR(300),
	@Mail				NVARCHAR(500),
	@RegisteredDate		DATETIME,
	@Remarks			NVARCHAR(MAX),
	@BlockedDate		DATETIME,
	@BlockageReason		NVARCHAR(MAX)

AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Subscribers] AS [Target]
	USING ( SELECT	 @Id				AS	"Id",
					 @SubscriberChatId	AS	"SubscriberChatId",
					 @FirstName			AS	"FirstName",
					 @LastName			AS	"LastName",
					 @CellNumber		AS	"CellNumber",
					 @Mail				AS	"Mail",
					 @RegisteredDate	AS	"RegisteredDate",
					 @Remarks			AS	"Remarks",
					 @BlockedDate		AS	"BlockedDate",
					 @BlockageReason	AS	"BlockageReason"
	) AS [Source]	ON [Source].[Id]		= [Target].[Id]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[SubscriberChatId]		= [Source].[SubscriberChatId], 
				   [Target].[FirstName]				= [Source].[FirstName], 
				   [Target].[LastName]				= [Source].[LastName],
				   [Target].[CellNumber]			= [Source].[CellNumber],
				   [Target].[Mail]					= [Source].[Mail],
				   [Target].[RegisteredDate]		= [Source].[RegisteredDate],
				   [Target].[Remarks]				= [Source].[Remarks],
				   [Target].[BlockedDate]			= [Source].[BlockedDate],
				   [Target].[BlockageReason]		= [Source].[BlockageReason]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (	[FirstName]
					,[LastName]
					,[CellNumber]
					,[Mail]
					,[RegisteredDate]
					,[Remarks]
				)
		VALUES (	[Source].[FirstName]
					,[Source].[LastName]
					,[Source].[CellNumber]
					,[Source].[Mail]
					,[Source].[RegisteredDate]
					,[Source].[Remarks]
				)
			OUTPUT Inserted.Id
			;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
