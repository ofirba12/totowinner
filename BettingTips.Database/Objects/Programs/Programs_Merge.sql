﻿CREATE PROCEDURE [dbo].[Programs_Merge]
	@ProgramId		BIGINT,
	@ProgramDate	DATETIME
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Programs] AS [Target]
	USING ( SELECT	 @ProgramId		AS	"ProgramId"
					,@ProgramDate	AS	"ProgramDate"
	) AS [Source]	ON [Source].[ProgramId]		= [Target].[ProgramId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[ProgramId]			= [Source].[ProgramId], 
				   [Target].[ProgramDate]		= [Source].[ProgramDate]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramId]
				,[ProgramDate])
		VALUES ([Source].[ProgramId]
				,[Source].[ProgramDate]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END