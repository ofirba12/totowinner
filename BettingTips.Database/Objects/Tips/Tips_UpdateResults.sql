﻿CREATE PROCEDURE [dbo].[Tips_UpdateResults]
	@ProgramId			BIGINT,			
	@GameIndex			INT,	
	@TipOfficialResult	NVARCHAR(50),	
	@TipOfficialMark	CHAR(1),			
	@TipManualResult	NVARCHAR(50),	
	@TipManualMark		CHAR(1)
AS
	UPDATE Tips 
	SET 
		TipOfficialResult = @TipOfficialResult,
		TipOfficialMark = @TipOfficialMark,
		TipManualResult = @TipManualResult,
		TipManualMark = @TipManualMark
	WHERE ProgramId = @ProgramId AND GameIndex = @GameIndex
RETURN 0
