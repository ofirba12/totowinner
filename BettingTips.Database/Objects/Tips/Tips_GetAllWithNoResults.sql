﻿CREATE PROCEDURE [dbo].[Tips_GetAllWithNoResults]
AS
	SELECT * FROM Tips 
	WHERE (TipOfficialResult IS NULL OR TipOfficialMark IS NULL)
	AND TipManualResult IS NULL
	ORDER BY [GameStart] ASC
RETURN 0
