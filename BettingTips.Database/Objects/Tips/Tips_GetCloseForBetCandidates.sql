﻿CREATE PROCEDURE [dbo].[Tips_GetCloseForBetCandidates]

AS
	SELECT * from Tips
	WHERE [Status] != N'סגור' AND GameStart < DATEADD(minute, 10, GetDate())
RETURN 0
