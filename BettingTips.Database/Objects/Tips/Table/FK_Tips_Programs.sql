﻿ALTER TABLE [dbo].[Tips]
	ADD CONSTRAINT [FK_Tips_Programs]
	FOREIGN KEY ([ProgramId])
	REFERENCES [Programs] ([ProgramId])
