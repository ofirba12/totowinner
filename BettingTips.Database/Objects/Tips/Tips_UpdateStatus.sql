﻿CREATE PROCEDURE [dbo].[Tips_UpdateStatus]
	@ProgramId			BIGINT,			
	@GameIndex			INT,
	@Status				NVARCHAR(50)	
AS
	UPDATE Tips
		SET [Status]=@Status
	WHERE ProgramId=@ProgramId AND GameIndex=@GameIndex

RETURN 0
