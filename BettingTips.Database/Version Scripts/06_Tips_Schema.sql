﻿USE BettingTips
GO

--IF OBJECT_ID(N'dbo.Tips', N'U') IS NOT NULL  
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[Tips]') AND type in (N'U'))
BEGIN
	EXEC [dbo].[EraseAllData]
	ALTER TABLE [dbo].[TipsAudit] DROP CONSTRAINT [FK_TipsAudit_Tips]
	ALTER TABLE [dbo].[Tips] DROP CONSTRAINT [FK_Tips_Programs]
--	ALTER TABLE [dbo].[SubscribersUrls] DROP CONSTRAINT [FK_SubscribersUrls_Tips]
    DROP TABLE [dbo].[Tips];  
END
GO

CREATE TABLE [dbo].[Tips]
(
	[TipId]				INT IDENTITY(10000,1)	NOT NULL,
	[ProgramId]			INT				NOT NULL,
	[GameIndex]			INT				NOT NULL,
	[SportId]			INT				NOT NULL,
	[TotoId]			INT				NOT NULL,
	[GameStart]			DATETIME		NOT NULL,
	[TipType]			BIT				NOT NULL,
	[PowerTip]			TINYINT			NOT NULL,
	[InsertedTS]		DATETIME		NOT NULL,
	[UpdatedTS]			DATETIME		NULL,
	[BetName]			NVARCHAR(500)	NOT NULL,
	[BetLeague]			NVARCHAR(100)	NOT NULL,
	[Country]			NVARCHAR(100)	NOT NULL,
	[BetParentChild]	BIT				NOT NULL,
	[BetType]			CHAR(1)			NOT NULL,
	[IsSingle]			BIT				NOT NULL,
	[BetRate]			FLOAT			NOT NULL,
	[Status]			NVARCHAR(50)	NOT NULL,
	[TipTime]			NVARCHAR(500)	NOT NULL,
	[TipContent]		NVARCHAR(MAX)	NOT NULL,
	[TipMarks]			NVARCHAR(500)	NOT NULL,
	[TipUrl]			NVARCHAR(2000)	NOT NULL,
	[TipOfficialResult]	NVARCHAR(50)	NULL,
	[TipOfficialMark]	CHAR(1)			NULL,
	[TipManualResult]	NVARCHAR(50)	NULL,
	[TipManualMark]		CHAR(1)			NULL,
	[Active]			BIT				NOT NULL DEFAULT(1)
)

ALTER TABLE [dbo].[Tips]
	ADD CONSTRAINT [PK_Tips]
	PRIMARY KEY (TipId)

GO
ALTER TABLE [dbo].[Tips]
	ADD CONSTRAINT [UCK_Tips]
	UNIQUE ([ProgramId], [GameIndex])

GO
ALTER TABLE [dbo].[Tips]
	ADD CONSTRAINT [FK_Tips_Programs]
	FOREIGN KEY ([ProgramId])
	REFERENCES [Programs] ([ProgramId])

GO
ALTER TABLE [dbo].[TipsAudit]
	ADD CONSTRAINT [FK_TipsAudit_Tips]
	FOREIGN KEY ([ProgramId], [GameIndex])
	REFERENCES [Tips] ([ProgramId], [GameIndex])

GO
--ALTER TABLE [dbo].[SubscribersUrls]
--	ADD CONSTRAINT [FK_SubscribersUrls_Tips]
--	FOREIGN KEY (TipId)
--	REFERENCES [Tips] (TipId)
--GO

GO