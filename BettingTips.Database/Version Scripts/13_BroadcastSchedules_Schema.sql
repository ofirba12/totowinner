﻿USE [BettingTips]
GO

CREATE TABLE [dbo].[BroadcastSchedules]
(
	[Hour] INT NOT NULL,
	[Minute] INT NOT NULL
)
GO

ALTER TABLE [dbo].[BroadcastSchedules]
	ADD CONSTRAINT [PK_BroadcastScheduless]
	PRIMARY KEY ([Hour], [Minute])
GO