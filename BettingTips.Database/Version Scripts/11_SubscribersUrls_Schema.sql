﻿USE [BettingTips]
GO

DELETE FROM [SubscribersUrls]
GO

ALTER TABLE SubscribersUrls
ADD [PickedUpByChatServer]	DATETIME;

GO
ALTER PROCEDURE [dbo].[SubscribersUrls_Visit]
	@UrlGuid		UNIQUEIDENTIFIER

AS
	UPDATE SubscribersUrls 
	SET VisitUrlTS = GetDate(),
		PickedUpByChatServer = NULL
	WHERE UrlGuid=@UrlGuid

RETURN 0
GO
ALTER PROCEDURE [dbo].[SubscribersUrls_Insert]
	@UrlGuid		UNIQUEIDENTIFIER,
	@TipId			INT,
	@SubscriberId	INT
AS
	INSERT INTO SubscribersUrls (UrlGuid, TipId, SubscriberId, VisitUrlTS, PickedUpByChatServer) 
		VALUES (@UrlGuid, @TipId, @SubscriberId, NULL, NULL)
RETURN 0