﻿USE BettingTips
GO
CREATE PROCEDURE [dbo].[Subscribers_Merge]
	@SubscriberId		INT,
	@FirstName			NVARCHAR(300),
	@LastName			NVARCHAR(300),
	@CellNumber			NVARCHAR(300),
	@Mail				NVARCHAR(500),
	@RequestMoreTips	BIT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Subscribers] AS [Target]
	USING ( SELECT	 @SubscriberId		AS	"SubscriberId",
					 @FirstName			AS	"FirstName",
					 @LastName			AS	"LastName",
					 @CellNumber		AS	"CellNumber",
					 @Mail				AS	"Mail",
					 @RequestMoreTips	AS	"RequestMoreTips"
	) AS [Source]	ON [Source].[SubscriberId]		= [Target].[SubscriberId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[FirstName]				= [Source].[FirstName], 
				   [Target].[LastName]				= [Source].[LastName],
				   [Target].[CellNumber]			= [Source].[CellNumber],
				   [Target].[Mail]					= [Source].[Mail],
				   [Target].[RequestMoreTips]		= [Source].[RequestMoreTips]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SubscriberId]
				,[FirstName]
				,[LastName]
				,[CellNumber]
				,[Mail]
				,[RequestMoreTips]
				)
		VALUES (
				 [Source].SubscriberId
				,[Source].FirstName
				,[Source].LastName
				,[Source].CellNumber
				,[Source].Mail
				,[Source].RequestMoreTips
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[SubscribersUrls_Insert]
	@UrlGuid		UNIQUEIDENTIFIER,
	@TipId			INT,
	@SubscriberId	INT
AS
	INSERT INTO SubscribersUrls (UrlGuid, TipId, SubscriberId, VisitUrlTS) 
		VALUES (@UrlGuid, @TipId, @SubscriberId, NULL)
RETURN 0

GO
CREATE PROCEDURE [dbo].[SubscribersUrls_Visit]
	@UrlGuid		UNIQUEIDENTIFIER

AS
	UPDATE SubscribersUrls 
	SET VisitUrlTS = GetDate()
	WHERE UrlGuid=@UrlGuid

RETURN 0
GO