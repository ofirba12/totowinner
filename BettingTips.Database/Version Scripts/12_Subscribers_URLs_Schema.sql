﻿USE BettingTips
GO

IF OBJECT_ID(N'dbo.SubscribersUrls', N'U') IS NOT NULL  
   DROP TABLE [dbo].[SubscribersUrls];  
GO
IF OBJECT_ID(N'dbo.Subscribers', N'U') IS NOT NULL  
   DROP TABLE [dbo].[Subscribers];  
GO


CREATE TABLE [dbo].[Subscribers]
(
	[Id]						INT IDENTITY(10000,1)	NOT NULL,
	[SubscriberChatId]			BIGINT NULL,
	[FirstName]					NVARCHAR(300) NOT NULL,
	[LastName]					NVARCHAR(300) NOT NULL,
	[CellNumber]				NVARCHAR(300) NOT NULL,
	[Mail]						NVARCHAR(500) NOT NULL,
	[RegisteredDate]			DATETIME NOT NULL,
	[Remarks]					NVARCHAR(MAX),
	[BlockedDate]				DATETIME,
	[BlockageReason]			NVARCHAR(MAX),
)


ALTER TABLE [dbo].[Subscribers]
	ADD CONSTRAINT [PK_Subscribers]
	PRIMARY KEY (Id)

GO
ALTER PROCEDURE [dbo].[Subscribers_Merge]
	@Id					INT,
	@SubscriberChatId	BIGINT,
	@FirstName			NVARCHAR(300),
	@LastName			NVARCHAR(300),
	@CellNumber			NVARCHAR(300),
	@Mail				NVARCHAR(500),
	@RegisteredDate		DATETIME,
	@Remarks			NVARCHAR(MAX),
	@BlockedDate		DATETIME,
	@BlockageReason		NVARCHAR(MAX)

AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Subscribers] AS [Target]
	USING ( SELECT	 @Id				AS	"Id",
					 @SubscriberChatId	AS	"SubscriberChatId",
					 @FirstName			AS	"FirstName",
					 @LastName			AS	"LastName",
					 @CellNumber		AS	"CellNumber",
					 @Mail				AS	"Mail",
					 @RegisteredDate	AS	"RegisteredDate",
					 @Remarks			AS	"Remarks",
					 @BlockedDate		AS	"BlockedDate",
					 @BlockageReason	AS	"BlockageReason"
	) AS [Source]	ON [Source].[Id]		= [Target].[Id]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[SubscriberChatId]		= [Source].[SubscriberChatId], 
				   [Target].[FirstName]				= [Source].[FirstName], 
				   [Target].[LastName]				= [Source].[LastName],
				   [Target].[CellNumber]			= [Source].[CellNumber],
				   [Target].[Mail]					= [Source].[Mail],
				   [Target].[RegisteredDate]		= [Source].[RegisteredDate],
				   [Target].[Remarks]				= [Source].[Remarks],
				   [Target].[BlockedDate]			= [Source].[BlockedDate],
				   [Target].[BlockageReason]		= [Source].[BlockageReason]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (	[FirstName]
					,[LastName]
					,[CellNumber]
					,[Mail]
					,[RegisteredDate]
					,[Remarks]
				)
		VALUES (	[Source].[FirstName]
					,[Source].[LastName]
					,[Source].[CellNumber]
					,[Source].[Mail]
					,[Source].[RegisteredDate]
					,[Source].[Remarks]
				)
			OUTPUT Inserted.Id
			;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

CREATE TABLE [dbo].[SubscribersUrls]
(
	[UrlId]					INT IDENTITY(1000,1)	NOT NULL,
	[UrlGuid]				UNIQUEIDENTIFIER		NOT NULL,
	[TipId]					INT						NOT NULL,
	[SubscriberId]			BIGINT					NOT NULL,
	[VisitUrlTS]			DATETIME,
	[PickedUpByChatServer]	DATETIME				
)
GO
ALTER TABLE [dbo].[SubscribersUrls]
	ADD CONSTRAINT [PK_SubscribersUrls]
	PRIMARY KEY (UrlId)
Go
ALTER TABLE [dbo].[SubscribersUrls]
	ADD CONSTRAINT [UCK_SubscribersUrls]
	UNIQUE ([UrlGuid], [TipId],[SubscriberId])

GO
ALTER TABLE [dbo].[SubscribersUrls]
	ADD CONSTRAINT [FK_SubscribersUrls_Tips]
	FOREIGN KEY (TipId)
	REFERENCES [Tips] (TipId)
GO
ALTER PROCEDURE [dbo].[SubscribersUrls_Insert]
	@UrlGuid		UNIQUEIDENTIFIER,
	@TipId			INT,
	@SubscriberId	BIGINT
AS
	INSERT INTO SubscribersUrls (UrlGuid, TipId, SubscriberId, VisitUrlTS, PickedUpByChatServer) 
		VALUES (@UrlGuid, @TipId, @SubscriberId, NULL, NULL)
RETURN 0
GO

INSERT INTO [dbo].[Subscribers]
           ([SubscriberChatId]
           ,[FirstName]
           ,[LastName]
           ,[CellNumber]
           ,[Mail]
           ,[RegisteredDate]
           ,[Remarks]
           ,[BlockedDate]
           ,[BlockageReason])
           VALUES (NULL,N'אופיר',N'בן אברהם','054-2688805','ofirba@gmail.com',GetDate(), NULL,NULL, NULL)
GO

INSERT INTO [dbo].[Subscribers]
           ([SubscriberChatId]
           ,[FirstName]
           ,[LastName]
           ,[CellNumber]
           ,[Mail]
           ,[RegisteredDate]
           ,[Remarks]
           ,[BlockedDate]
           ,[BlockageReason])
           VALUES (NULL,N'בן',N'גביזון','054-8070403','gabi9200@gmail.com',GetDate(), NULL,NULL, NULL)
GO