﻿USE BettingTips
GO

IF OBJECT_ID(N'dbo.Tips', N'U') IS NOT NULL  
   DROP TABLE [dbo].[Tips];  
GO

CREATE TABLE [dbo].[Tips]
(
	[ProgramId]			INT				NOT NULL,
	[GameIndex]			INT				NOT NULL,
	[SportId]			INT				NOT NULL,
	[TotoId]			INT				NOT NULL,
	[GameStart]			DATETIME		NOT NULL,
	[TipType]			BIT				NOT NULL,
	[PowerTip]			TINYINT			NOT NULL,
	[InsertedTS]		DATETIME		NOT NULL,
	[UpdatedTS]			DATETIME		NULL,
	[BetName]			NVARCHAR(500)	NOT NULL,
	[BetLeague]			NVARCHAR(100)	NOT NULL,
	[Country]			NVARCHAR(100)	NOT NULL,
	[BetParentChild]	BIT				NOT NULL,
	[BetType]			CHAR(1)			NOT NULL,
	[IsSingle]			BIT				NOT NULL,
	[BetRate]			FLOAT			NOT NULL,
	[Status]			NVARCHAR(50)	NOT NULL,
	[TipTime]			NVARCHAR(500)	NOT NULL,
	[TipContent]		NVARCHAR(MAX)	NOT NULL,
	[TipMarks]			NVARCHAR(500)	NOT NULL,
	[TipUrl]			NVARCHAR(2000)	NOT NULL,
	[TipOfficialResult]	NVARCHAR(50)	NULL,
	[TipOfficialMark]	CHAR(1)			NULL,
	[TipManualResult]	NVARCHAR(50)	NULL,
	[TipManualMark]		CHAR(1)			NULL
)


GO
ALTER TABLE [dbo].[Tips]
	ADD CONSTRAINT [PK_Tips]
	PRIMARY KEY ([ProgramId], [GameIndex])

GO
ALTER TABLE [dbo].[Tips]
	ADD CONSTRAINT [FK_Tips_Programs]
	FOREIGN KEY ([ProgramId])
	REFERENCES [Programs] ([ProgramId])

GO
