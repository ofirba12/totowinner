﻿USE BettingTips
GO

GO
IF OBJECT_ID(N'dbo.Programs', N'U') IS NOT NULL  
   DROP TABLE [dbo].[Programs];  
GO

CREATE TABLE [dbo].[Programs]
(
	[ProgramId]		INT				NOT NULL,
	[ProgramDate]	DATETIME		NOT NULL
)

GO
ALTER TABLE [dbo].[Programs]
	ADD CONSTRAINT [PK_Programs]
	PRIMARY KEY ([ProgramId])
GO
