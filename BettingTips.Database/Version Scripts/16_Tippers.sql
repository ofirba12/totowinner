﻿USE BettingTips
GO

GO
IF OBJECT_ID(N'dbo.Tippers', N'U') IS NOT NULL  
   DROP TABLE [dbo].[Tippers];  
GO

CREATE TABLE [dbo].[Tippers]
(
	[TipperId]		INT IDENTITY(1,1)	NOT NULL,
	[TipperName]	NVARCHAR(400) NOT NULL
)
GO
ALTER TABLE [dbo].[Tippers]
	ADD CONSTRAINT [PK_Tippers]
	PRIMARY KEY (TipperId)
GO
CREATE PROCEDURE [dbo].[Tippers_Merge]
	@TipperId			INT,			
	@TipperName			NVARCHAR(400)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @Updated DATETIME = GetDate();

	MERGE dbo.[Tippers] AS [Target]
	USING ( SELECT	 @TipperId			AS	"TipperId"
					,@TipperName		AS	"TipperName"
	) AS [Source]	ON [Source].[TipperId] = [Target].[TipperId]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[TipperName]			=[Source].[TipperName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TipperName])
		VALUES (
			[Source].[TipperName]
			)
			OUTPUT Inserted.TipperId
			;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [Tippers_GetAll]
AS
	SELECT * FROM Tippers 
RETURN 0
GO
CREATE PROCEDURE [dbo].[Tipper_Delete]
	@TipperId			INT
AS
	DELETE FROM dbo.Tippers WHERE TipperId=@TipperId
RETURN 0
GO
ALTER TABLE dbo.Tips
	ADD [TipperId]			INT				NOT NULL DEFAULT(1)
GO
ALTER PROCEDURE [dbo].[Tips_Merge]
	@ProgramId			BIGINT,			
	@GameIndex			INT,		
	@SportId			INT,
	@TotoId				INT,				
	@GameStart			DATETIME,		
	@TipType			BIT,			
	@PowerTip			TINYINT,			
	@InsertedTS			DATETIME,		
	@UpdatedTS			DATETIME,	
	@BetName			NVARCHAR(500),	
	@BetLeague			NVARCHAR(100),	
	@Country			NVARCHAR(100),	
	@BetParentChild		BIT,				
	@BetType			CHAR(1),			
	@IsSingle			BIT,				
	@BetRate			FLOAT,			
	@Status				NVARCHAR(50),	
	@TipTime			NVARCHAR(500),	
	@TipContent			NVARCHAR(MAX),	
	@TipMarks			NVARCHAR(500),	
	@TipUrl				NVARCHAR(2000),
	@TipperId			INT				
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @Updated DATETIME = GetDate();

	MERGE dbo.[Tips] AS [Target]
	USING ( SELECT	 @ProgramId			AS	"ProgramId"
					,@GameIndex			AS	"GameIndex"
					,@SportId			AS 	"SportId"
					,@TotoId			AS 	"TotoId"
					,@GameStart			AS 	"GameStart"
					,@TipType			AS 	"TipType"
					,@PowerTip			AS 	"PowerTip"
					,@InsertedTS		AS 	"InsertedTS"
					,@UpdatedTS			AS 	"UpdatedTS"
					,@BetName			AS 	"BetName"
					,@BetLeague			AS 	"BetLeague"
					,@Country			AS 	"Country"
					,@BetParentChild	AS 	"BetParentChild"
					,@BetType			AS 	"BetType"
					,@IsSingle			AS 	"IsSingle"
					,@BetRate			AS 	"BetRate"
					,@Status			AS 	"Status"
					,@TipTime			AS 	"TipTime"
					,@TipContent		AS 	"TipContent"
					,@TipMarks			AS 	"TipMarks"
					,@TipUrl			AS 	"TipUrl"
					,@TipperId			AS 	"TipperId"
	) AS [Source]	ON [Source].[ProgramId] = [Target].[ProgramId]
					AND	[Source].[GameIndex] = [Target].[GameIndex]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[ProgramId]			=[Source].[ProgramId],
			[Target].[GameIndex]			=[Source].[GameIndex],
			[Target].[SportId]				=[Source].[SportId],		
			[Target].[TotoId]				=[Source].[TotoId],		
			[Target].[GameStart]			=[Source].[GameStart],
			[Target].[TipType]				=[Source].[TipType],		
			[Target].[PowerTip]				=[Source].[PowerTip],		
			[Target].[InsertedTS]			=[Source].[InsertedTS],
			[Target].[UpdatedTS]			=[Source].[UpdatedTS],		
			[Target].[BetName]				=[Source].[BetName],		
			[Target].[BetLeague]			=[Source].[BetLeague],
			[Target].[Country]				=[Source].[Country],			
			[Target].[BetParentChild]		=[Source].[BetParentChild],
			[Target].[BetType]				=[Source].[BetType],
			[Target].[IsSingle]				=[Source].[IsSingle],		
			[Target].[BetRate]				=[Source].[BetRate],		
			[Target].[Status]				=[Source].[Status],		
			[Target].[TipTime]				=[Source].[TipTime],			
			[Target].[TipContent]			=[Source].[TipContent],
			[Target].[TipMarks]				=[Source].[TipMarks],	
			[Target].[TipUrl]				=[Source].[TipUrl],
			[Target].[TipperId]				=[Source].[TipperId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([ProgramId],		
				[GameIndex],	
				[SportId],			
				[TotoId],			
				[GameStart],
				[TipType],			
				[PowerTip],			
				[InsertedTS],
				[UpdatedTS],	
				[BetName],		
				[BetLeague],
				[Country],		
				[BetParentChild],
				[BetType],	
				[IsSingle],			
				[BetRate],		
				[Status],		
				[TipTime],			
				[TipContent],
				[TipMarks],		
				[TipUrl],
				[TipperId]
				)
		VALUES (
			[Source].[ProgramId],
			[Source].[GameIndex],
			[Source].[SportId],		
			[Source].[TotoId],		
			[Source].[GameStart],
			[Source].[TipType],		
			[Source].[PowerTip],			
			[Source].[InsertedTS],
			[Source].[UpdatedTS],		
			[Source].[BetName],		
			[Source].[BetLeague],
			[Source].[Country],			
			[Source].[BetParentChild],
			[Source].[BetType],
			[Source].[IsSingle],			
			[Source].[BetRate],		
			[Source].[Status],		
			[Source].[TipTime],			
			[Source].[TipContent],
			[Source].[TipMarks],	
			[Source].[TipUrl],
			[Source].[TipperId]
			)
			OUTPUT Inserted.TipId
			;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO