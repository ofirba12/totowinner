﻿USE BettingTips
GO

IF OBJECT_ID(N'dbo.Subscribers', N'U') IS NOT NULL  
   DROP TABLE [dbo].[Subscribers];  
GO

CREATE TABLE [dbo].[Subscribers]
(
	[SubscriberId]		INT NOT NULL,
	[FirstName]			NVARCHAR(300) NOT NULL,
	[LastName]			NVARCHAR(300) NOT NULL,
	[CellNumber]		NVARCHAR(300) NOT NULL,
	[Mail]				NVARCHAR(500) NOT NULL,
	[RequestMoreTips]	BIT NOT NULL
)

ALTER TABLE [dbo].[Subscribers]
	ADD CONSTRAINT [PK_Subscribers]
	PRIMARY KEY (SubscriberId)

GO

CREATE TABLE [dbo].[SubscribersUrls]
(
	[UrlId]			INT IDENTITY(1000,1)	NOT NULL,
	[UrlGuid]		UNIQUEIDENTIFIER		NOT NULL,
	[TipId]			INT						NOT NULL,
	[SubscriberId]	INT						NOT NULL,
	[VisitUrlTS]	DATETIME				
)
GO
ALTER TABLE [dbo].[SubscribersUrls]
	ADD CONSTRAINT [PK_SubscribersUrls]
	PRIMARY KEY (UrlId)
Go
ALTER TABLE [dbo].[SubscribersUrls]
	ADD CONSTRAINT [UCK_SubscribersUrls]
	UNIQUE ([UrlGuid], [TipId],[SubscriberId])

GO
ALTER TABLE [dbo].[SubscribersUrls]
	ADD CONSTRAINT [FK_SubscribersUrls_Tips]
	FOREIGN KEY (TipId)
	REFERENCES [Tips] (TipId)
GO
