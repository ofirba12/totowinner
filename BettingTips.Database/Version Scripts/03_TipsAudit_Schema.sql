﻿USE BettingTips
GO

IF OBJECT_ID(N'dbo.TipsAudit', N'U') IS NOT NULL  
   DROP TABLE [dbo].[TipsAudit];  
GO


CREATE TABLE [dbo].[TipsAudit]
(
	[ProgramId]		INT				NOT NULL,
	[GameIndex]		INT				NOT NULL,
	[AuditTS]		DATETIME		NOT NULL,
	[AuditDetails]	NVARCHAR(MAX)	NOT NULL
)


GO
ALTER TABLE [dbo].[TipsAudit]
	ADD CONSTRAINT [PK_TipsAudit]
	PRIMARY KEY ([ProgramId], [GameIndex], [AuditTS])


GO
ALTER TABLE [dbo].[TipsAudit]
	ADD CONSTRAINT [FK_TipsAudit_Tips]
	FOREIGN KEY ([ProgramId], [GameIndex])
	REFERENCES [Tips] ([ProgramId], [GameIndex])


GO
