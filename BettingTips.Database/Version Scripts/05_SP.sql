﻿USE BettingTips
GO

CREATE PROCEDURE [dbo].[Tips_GetAllWithNoResults]
AS
	SELECT * FROM Tips 
	WHERE (TipOfficialResult IS NULL OR TipOfficialMark IS NULL)
	AND TipManualResult IS NULL
	ORDER BY [GameStart] ASC
RETURN 0
GO
CREATE PROCEDURE [dbo].[Tips_UpdateResults]
	@ProgramId			INT,			
	@GameIndex			INT,	
	@TipOfficialResult	NVARCHAR(50),	
	@TipOfficialMark	CHAR(1),			
	@TipManualResult	NVARCHAR(50),	
	@TipManualMark		CHAR(1)
AS
	UPDATE Tips 
	SET 
		TipOfficialResult = @TipOfficialResult,
		TipOfficialMark = @TipOfficialMark,
		TipManualResult = @TipManualResult,
		TipManualMark = @TipManualMark
	WHERE ProgramId = @ProgramId AND GameIndex = @GameIndex
RETURN 0
GO

ALTER PROCEDURE [dbo].[Tips_Merge]
	@ProgramId			INT,			
	@GameIndex			INT,		
	@SportId			INT,
	@TotoId				INT,				
	@GameStart			DATETIME,		
	@TipType			BIT,			
	@PowerTip			TINYINT,			
	@InsertedTS			DATETIME,		
	@UpdatedTS			DATETIME,	
	@BetName			NVARCHAR(500),	
	@BetLeague			NVARCHAR(100),	
	@Country			NVARCHAR(100),	
	@BetParentChild		BIT,				
	@BetType			CHAR(1),			
	@IsSingle			BIT,				
	@BetRate			FLOAT,			
	@Status				NVARCHAR(50),	
	@TipTime			NVARCHAR(500),	
	@TipContent			NVARCHAR(MAX),	
	@TipMarks			NVARCHAR(500),	
	@TipUrl				NVARCHAR(2000)
	--@TipOfficialResult	NVARCHAR(50),	
	--@TipOfficialMark	CHAR(1),			
	--@TipManualResult	NVARCHAR(50),	
	--@TipManualMark		CHAR(1)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	DECLARE @Updated DATETIME = GetDate();

	MERGE dbo.[Tips] AS [Target]
	USING ( SELECT	 @ProgramId			AS	"ProgramId"
					,@GameIndex			AS	"GameIndex"
					,@SportId			AS 	"SportId"
					,@TotoId			AS 	"TotoId"
					,@GameStart			AS 	"GameStart"
					,@TipType			AS 	"TipType"
					,@PowerTip			AS 	"PowerTip"
					,@InsertedTS		AS 	"InsertedTS"
					,@UpdatedTS			AS 	"UpdatedTS"
					,@BetName			AS 	"BetName"
					,@BetLeague			AS 	"BetLeague"
					,@Country			AS 	"Country"
					,@BetParentChild	AS 	"BetParentChild"
					,@BetType			AS 	"BetType"
					,@IsSingle			AS 	"IsSingle"
					,@BetRate			AS 	"BetRate"
					,@Status			AS 	"Status"
					,@TipTime			AS 	"TipTime"
					,@TipContent		AS 	"TipContent"
					,@TipMarks			AS 	"TipMarks"
					,@TipUrl			AS 	"TipUrl"
					--,@TipOfficialResult AS	"TipOfficialResult"
					--,@TipOfficialMark	AS 	"TipOfficialMark"
					--,@TipManualResult	AS 	"TipManualResult"
					--,@TipManualMark		AS 	"TipManualMark"
	) AS [Source]	ON [Source].[ProgramId] = [Target].[ProgramId]
					AND	[Source].[GameIndex] = [Target].[GameIndex]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[ProgramId]			=[Source].[ProgramId],
			[Target].[GameIndex]			=[Source].[GameIndex],
			[Target].[SportId]				=[Source].[SportId],		
			[Target].[TotoId]				=[Source].[TotoId],		
			[Target].[GameStart]			=[Source].[GameStart],
			[Target].[TipType]				=[Source].[TipType],		
			[Target].[PowerTip]				=[Source].[PowerTip],		
			[Target].[InsertedTS]			=[Source].[InsertedTS],
			[Target].[UpdatedTS]			=[Source].[UpdatedTS],		
			[Target].[BetName]				=[Source].[BetName],		
			[Target].[BetLeague]			=[Source].[BetLeague],
			[Target].[Country]				=[Source].[Country],			
			[Target].[BetParentChild]		=[Source].[BetParentChild],
			[Target].[BetType]				=[Source].[BetType],
			[Target].[IsSingle]				=[Source].[IsSingle],		
			[Target].[BetRate]				=[Source].[BetRate],		
			[Target].[Status]				=[Source].[Status],		
			[Target].[TipTime]				=[Source].[TipTime],			
			[Target].[TipContent]			=[Source].[TipContent],
			[Target].[TipMarks]				=[Source].[TipMarks],	
			[Target].[TipUrl]				=[Source].[TipUrl]		
			--[Target].[TipOfficialResult]	=[Source].[TipOfficialResult],
			--[Target].[TipOfficialMark]		=[Source].[TipOfficialMark],
			--[Target].[TipManualResult]		=[Source].[TipManualResult],
			--[Target].[TipManualMark]		=[Source].[TipManualMark]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([ProgramId],		
				[GameIndex],	
				[SportId],			
				[TotoId],			
				[GameStart],
				[TipType],			
				[PowerTip],			
				[InsertedTS],
				[UpdatedTS],	
				[BetName],		
				[BetLeague],
				[Country],		
				[BetParentChild],
				[BetType],	
				[IsSingle],			
				[BetRate],		
				[Status],		
				[TipTime],			
				[TipContent],
				[TipMarks],		
				[TipUrl]
				--[TipOfficialResult],
				--[TipOfficialMark],
				--[TipManualResult],	
				--[TipManualMark]
				)
		VALUES (
			[Source].[ProgramId],
			[Source].[GameIndex],
			[Source].[SportId],		
			[Source].[TotoId],		
			[Source].[GameStart],
			[Source].[TipType],		
			[Source].[PowerTip],			
			[Source].[InsertedTS],
			[Source].[UpdatedTS],		
			[Source].[BetName],		
			[Source].[BetLeague],
			[Source].[Country],			
			[Source].[BetParentChild],
			[Source].[BetType],
			[Source].[IsSingle],			
			[Source].[BetRate],		
			[Source].[Status],		
			[Source].[TipTime],			
			[Source].[TipContent],
			[Source].[TipMarks],	
			[Source].[TipUrl]
			--[Source].[TipOfficialResult],
			--[Source].[TipOfficialMark],
			--[Source].[TipManualResult],
			--[Source].[TipManualMark]
			)
			;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Tips_GetCloseForBetCandidates]

AS
	SELECT * from Tips
	WHERE [Status] != N'סגור' AND GameStart < DATEADD(minute, 10, GetDate())
RETURN 0
GO

CREATE PROCEDURE [dbo].[Tips_UpdateStatus]
	@ProgramId			INT,			
	@GameIndex			INT,
	@Status				NVARCHAR(50)	
AS
	UPDATE Tips
		SET [Status]=@Status
	WHERE ProgramId=@ProgramId AND GameIndex=@GameIndex

RETURN 0
GO

CREATE TABLE [dbo].[SystemState]
(
	[Name]	NVARCHAR(400) NOT NULL,
	[Value] NVARCHAR(500) NOT NULL
)
GO
ALTER TABLE [dbo].[SystemState]
	ADD CONSTRAINT [PK_SystemState]
	PRIMARY KEY ([Name])
GO
CREATE PROCEDURE [dbo].[SystemState_Merge]
	@Name	NVARCHAR(400),
	@Value	NVARCHAR(500)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[SystemState] AS [Target]
	USING ( SELECT	 @Name	AS	"Name"
					,@Value	AS	"Value"
	) AS [Source]	ON [Source].[Name]	= [Target].[Name]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Value]		= [Source].[Value]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Name]
				,[Value])
		VALUES ([Source].[Name]
				,[Source].[Value]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

ALTER PROCEDURE [dbo].[Programs_Merge]
	@ProgramId		INT,
	@ProgramDate	DATETIME
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Programs] AS [Target]
	USING ( SELECT	 @ProgramId		AS	"ProgramId"
					,@ProgramDate	AS	"ProgramDate"
	) AS [Source]	ON [Source].[ProgramId]		= [Target].[ProgramId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[ProgramId]			= [Source].[ProgramId], 
				   [Target].[ProgramDate]		= [Source].[ProgramDate]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramId]
				,[ProgramDate])
		VALUES ([Source].[ProgramId]
				,[Source].[ProgramDate]
				)
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
