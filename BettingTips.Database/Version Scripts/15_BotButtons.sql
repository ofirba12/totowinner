﻿USE BettingTips
GO
CREATE TABLE [dbo].[BotButtons]
(
	[Id] INT NOT NULL,
	[Counter] INT NOT NULL
)
GO
ALTER TABLE [dbo].[BotButtons]
	ADD CONSTRAINT [PK_BotButtons]
	PRIMARY KEY (Id)
GO
--Favorite = 1,
--In3HoursAndFavorite = 2,
--SingleHighRate = 3,
--Custom = 4
INSERT INTO [BotButtons]([Id], [Counter]) VALUES (1,0)
INSERT INTO [BotButtons]([Id], [Counter]) VALUES (2,0)
INSERT INTO [BotButtons]([Id], [Counter]) VALUES (3,0)
INSERT INTO [BotButtons]([Id], [Counter]) VALUES (4,0)