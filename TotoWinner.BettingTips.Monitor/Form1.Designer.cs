﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGetAllTips = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonTippers = new System.Windows.Forms.Button();
            this.buttonBroadcastHours = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label_numberOfTips = new System.Windows.Forms.Label();
            this.buttonCRM = new System.Windows.Forms.Button();
            this.dateTimePickerWarehouse = new System.Windows.Forms.DateTimePicker();
            this.buttonShowWarhouse = new System.Windows.Forms.Button();
            this.buttonAddManualTip = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewStack = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewWarehouse = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStack)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWarehouse)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGetAllTips
            // 
            this.buttonGetAllTips.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonGetAllTips.Location = new System.Drawing.Point(1096, 0);
            this.buttonGetAllTips.Name = "buttonGetAllTips";
            this.buttonGetAllTips.Size = new System.Drawing.Size(92, 50);
            this.buttonGetAllTips.TabIndex = 0;
            this.buttonGetAllTips.Text = "הצג מחסנית";
            this.buttonGetAllTips.UseVisualStyleBackColor = true;
            this.buttonGetAllTips.Click += new System.EventHandler(this.buttonGetAllTips_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.buttonTippers);
            this.panel1.Controls.Add(this.buttonBroadcastHours);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label_numberOfTips);
            this.panel1.Controls.Add(this.buttonCRM);
            this.panel1.Controls.Add(this.dateTimePickerWarehouse);
            this.panel1.Controls.Add(this.buttonShowWarhouse);
            this.panel1.Controls.Add(this.buttonAddManualTip);
            this.panel1.Controls.Add(this.buttonGetAllTips);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1188, 50);
            this.panel1.TabIndex = 1;
            // 
            // buttonTippers
            // 
            this.buttonTippers.Location = new System.Drawing.Point(367, 14);
            this.buttonTippers.Name = "buttonTippers";
            this.buttonTippers.Size = new System.Drawing.Size(90, 23);
            this.buttonTippers.TabIndex = 9;
            this.buttonTippers.Text = "ממליצים";
            this.buttonTippers.UseVisualStyleBackColor = true;
            this.buttonTippers.Click += new System.EventHandler(this.buttonTippers_Click);
            // 
            // buttonBroadcastHours
            // 
            this.buttonBroadcastHours.Location = new System.Drawing.Point(271, 14);
            this.buttonBroadcastHours.Name = "buttonBroadcastHours";
            this.buttonBroadcastHours.Size = new System.Drawing.Size(90, 23);
            this.buttonBroadcastHours.TabIndex = 8;
            this.buttonBroadcastHours.Text = "שעות שידור";
            this.buttonBroadcastHours.UseVisualStyleBackColor = true;
            this.buttonBroadcastHours.Click += new System.EventHandler(this.buttonBroadcastHours_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(135, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "סטטיסטיקות";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label_numberOfTips
            // 
            this.label_numberOfTips.AutoSize = true;
            this.label_numberOfTips.Location = new System.Drawing.Point(1090, 17);
            this.label_numberOfTips.Name = "label_numberOfTips";
            this.label_numberOfTips.Size = new System.Drawing.Size(0, 13);
            this.label_numberOfTips.TabIndex = 1;
            // 
            // buttonCRM
            // 
            this.buttonCRM.Location = new System.Drawing.Point(27, 14);
            this.buttonCRM.Name = "buttonCRM";
            this.buttonCRM.Size = new System.Drawing.Size(102, 23);
            this.buttonCRM.TabIndex = 6;
            this.buttonCRM.Text = "ניהול לקוחות";
            this.buttonCRM.UseVisualStyleBackColor = true;
            this.buttonCRM.Click += new System.EventHandler(this.buttonCRM_Click);
            // 
            // dateTimePickerWarehouse
            // 
            this.dateTimePickerWarehouse.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerWarehouse.Location = new System.Drawing.Point(637, 15);
            this.dateTimePickerWarehouse.Name = "dateTimePickerWarehouse";
            this.dateTimePickerWarehouse.Size = new System.Drawing.Size(209, 20);
            this.dateTimePickerWarehouse.TabIndex = 4;
            // 
            // buttonShowWarhouse
            // 
            this.buttonShowWarhouse.Location = new System.Drawing.Point(879, 14);
            this.buttonShowWarhouse.Name = "buttonShowWarhouse";
            this.buttonShowWarhouse.Size = new System.Drawing.Size(92, 23);
            this.buttonShowWarhouse.TabIndex = 3;
            this.buttonShowWarhouse.Text = "הצג מחסן";
            this.buttonShowWarhouse.UseVisualStyleBackColor = true;
            this.buttonShowWarhouse.Click += new System.EventHandler(this.buttonShowWarhouse_Click);
            // 
            // buttonAddManualTip
            // 
            this.buttonAddManualTip.Location = new System.Drawing.Point(477, 14);
            this.buttonAddManualTip.Name = "buttonAddManualTip";
            this.buttonAddManualTip.Size = new System.Drawing.Size(126, 23);
            this.buttonAddManualTip.TabIndex = 2;
            this.buttonAddManualTip.Text = "הוסף טיפ ידנית";
            this.buttonAddManualTip.UseVisualStyleBackColor = true;
            this.buttonAddManualTip.Click += new System.EventHandler(this.buttonAddManualTip_Click);
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1188, 536);
            this.panel2.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1188, 536);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewStack);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1180, 510);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "מחסנית";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewStack
            // 
            this.dataGridViewStack.AllowUserToOrderColumns = true;
            this.dataGridViewStack.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStack.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewStack.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewStack.Name = "dataGridViewStack";
            this.dataGridViewStack.Size = new System.Drawing.Size(1174, 504);
            this.dataGridViewStack.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewWarehouse);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1180, 510);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "מחסן";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewWarehouse
            // 
            this.dataGridViewWarehouse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWarehouse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewWarehouse.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewWarehouse.Name = "dataGridViewWarehouse";
            this.dataGridViewWarehouse.Size = new System.Drawing.Size(1174, 504);
            this.dataGridViewWarehouse.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 586);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "ניהול בוט טלגרם - בנקרים";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStack)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWarehouse)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGetAllTips;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.DataGridView dataGridViewStack;
        private System.Windows.Forms.Label label_numberOfTips;
        private System.Windows.Forms.Button buttonAddManualTip;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridViewWarehouse;
        private System.Windows.Forms.DateTimePicker dateTimePickerWarehouse;
        private System.Windows.Forms.Button buttonShowWarhouse;
        private System.Windows.Forms.Button buttonCRM;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonBroadcastHours;
        private System.Windows.Forms.Button buttonTippers;
    }
}

