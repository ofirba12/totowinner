﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class BroadcastHours : Form
    {
        private TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;

        public BroadcastHours()
        {
            InitializeComponent();
            this.dataGridViewHours.Columns.Add("BroadcastTime", "שעת שידור");
            DisplayData();
        }

        private void DisplayData()
        {
            var times = _botPersistanceSrv.GetBroadcastSchedules();
            foreach (var t in times)
            {
                DataGridViewRow row = (DataGridViewRow)this.dataGridViewHours.Rows[0].Clone();
                row.Cells[0].Value = $"{string.Format("{0:00}", t.Hour)}:{string.Format("{0:00}", t.Minute)}";
                this.dataGridViewHours.Rows.Add(row);
            }
        }

        private void buttonAddTime_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = (DataGridViewRow)this.dataGridViewHours.Rows[0].Clone();
            row.Cells[0].Value = $"{string.Format("{0:00}", this.numericUpDownHour.Value)}:{string.Format("{0:00}", this.numericUpDownMinutes.Value)}";
            this.dataGridViewHours.Rows.Add(row);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            this.dataGridViewHours.Rows.Clear();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var times = new List<PushTipTime>();
            for (var r = 0; r < this.dataGridViewHours.Rows.Count-1; r++)
            {
                var splitted = this.dataGridViewHours.Rows[r].Cells[0].Value.ToString().Split(':');
                var hour = int.Parse(splitted[0].ToString());
                var minute = int.Parse(splitted[1].ToString());
                times.Add(new PushTipTime(hour, minute));
            }
            _botPersistanceSrv.UpdateBroadcastSchedules(times);
        }
    }
}
