﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BettingTips;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class Form1 : Form
    {
        private BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        private TelegramBotPersistanceServices _btTelegramBotPersistanceSrv = TelegramBotPersistanceServices.Instance;
        private DataGridViewCellEventArgs mouseLocation;

        public Form1()
        {
            InitializeComponent();
            this.dataGridViewWarehouse.CellMouseEnter += DataGridViewWarehouse_CellMouseEnter;
            this.dataGridViewStack.CellMouseEnter += DataGridViewWarehouse_CellMouseEnter;
        }

        private void DataGridViewWarehouse_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }

        ToolStripMenuItem toolStripItem1 = new ToolStripMenuItem();
        ToolStripMenuItem toolStripItem2 = new ToolStripMenuItem();
        ToolStripMenuItem toolStripItem3 = new ToolStripMenuItem();
        ToolStripMenuItem toolStripItem4 = new ToolStripMenuItem();

        public void AddStackContextMenu()
        {
            toolStripItem2.Text = "סמן לא לשליחה";
            toolStripItem2.Click -= new EventHandler(toolStripItem2_Click);
            toolStripItem2.Click += new EventHandler(toolStripItem2_Click);
            toolStripItem3.Text = "סמן לשליחה";
            toolStripItem3.Click -= new EventHandler(toolStripItem3_Click);
            toolStripItem3.Click += new EventHandler(toolStripItem3_Click);
            toolStripItem4.Text = "עדכן טיפ";
            toolStripItem4.Click -= new EventHandler(toolStripItem4_Click);
            toolStripItem4.Click += new EventHandler(toolStripItem4_Click);
            ContextMenuStrip strip = new ContextMenuStrip();
            foreach (DataGridViewRow row in dataGridViewStack.Rows)
            {
                row.ContextMenuStrip = strip;
                row.ContextMenuStrip.Items.Add(toolStripItem2);
                row.ContextMenuStrip.Items.Add(toolStripItem3);
                //if (!row.IsNewRow && row.Cells[6].Value.ToString().ToUpper()=="FALSE") //isAutomatic
                row.ContextMenuStrip.Items.Add(toolStripItem4);
            }
        }

        public void AddWarehouseContextMenu()
        {
            toolStripItem1.Text = "עדכן תוצאה";
            toolStripItem1.Click -= new EventHandler(toolStripItem1_Click);
            toolStripItem1.Click += new EventHandler(toolStripItem1_Click);
            ContextMenuStrip strip = new ContextMenuStrip();
            foreach (DataGridViewRow row in dataGridViewWarehouse.Rows)
            {
                row.ContextMenuStrip = strip;
                row.ContextMenuStrip.Items.Add(toolStripItem1);
            }
        }
        private void toolStripItem4_Click(object sender, EventArgs args)
        {
            if (!_btPersistanceSrv.ReadFinishTipGenerationState())
            {
                var tipId = this.dataGridViewStack.Rows[mouseLocation.RowIndex].Cells[0].Value.ToString();
                this.dataGridViewStack.Rows[mouseLocation.RowIndex].DefaultCellStyle.BackColor = Color.LightYellow;
                var tip = _btTipsSrv.GetTip(int.Parse(tipId));
                var manualTipDialog = new ManualTipForm(tip);
                DialogResult dialogresult = manualTipDialog.ShowDialog();
                if (dialogresult == DialogResult.OK)
                {
                    var updatedTip = manualTipDialog.GetTip();
                    _btTipsSrv.AddManualTip(updatedTip.ProgramId,
                        updatedTip.Sport,
                        updatedTip.GameIndex,
                        updatedTip.Country,
                        updatedTip.BetLeague,
                        updatedTip.GameStart,
                        updatedTip.PowerTip,
                        updatedTip.IsSingle,
                        updatedTip.BetMark,
                        updatedTip.TipContent,
                        updatedTip.TipUrl,
                        updatedTip.BetRate,
                        updatedTip.BetName,
                        updatedTip.IsParentBet,
                        updatedTip.TipperId
                        );
                    manualTipDialog.Dispose();
                }
            }
            else
                MessageBox.Show($"אנא המתן שתהליך סינכרון תוצאות במחסן הסתיים");
        }
        private void toolStripItem2_Click(object sender, EventArgs args)
        {
            if (!_btPersistanceSrv.ReadFinishTipGenerationState())
            {
                var tipId = this.dataGridViewStack.Rows[mouseLocation.RowIndex].Cells[0].Value.ToString();
                this.dataGridViewStack.Rows[mouseLocation.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                _btTelegramBotPersistanceSrv.SetActiveTip(int.Parse(tipId), false);
            }
            else
                MessageBox.Show($"אנא המתן שתהליך סינכרון תוצאות במחסן הסתיים");
        }
        private void toolStripItem3_Click(object sender, EventArgs args)
        {
            if (!_btPersistanceSrv.ReadFinishTipGenerationState())
            {
                var tipId = this.dataGridViewStack.Rows[mouseLocation.RowIndex].Cells[0].Value.ToString();
                this.dataGridViewStack.Rows[mouseLocation.RowIndex].DefaultCellStyle.BackColor = Color.White;
                _btTelegramBotPersistanceSrv.SetActiveTip(int.Parse(tipId), true);
            }
            else
                MessageBox.Show($"אנא המתן שתהליך סינכרון תוצאות במחסן הסתיים");
        }
        private void toolStripItem1_Click(object sender, EventArgs args)
        {
            if (!_btPersistanceSrv.ReadFinishTipGenerationState())
            {
                var programId = this.dataGridViewWarehouse.Rows[mouseLocation.RowIndex].Cells[1].Value.ToString();
                var gameIndex = this.dataGridViewWarehouse.Rows[mouseLocation.RowIndex].Cells[2].Value.ToString();
                var sport = this.dataGridViewWarehouse.Rows[mouseLocation.RowIndex].Cells[3].Value.ToString();
                var betName = this.dataGridViewWarehouse.Rows[mouseLocation.RowIndex].Cells[8].Value.ToString();
                var dialog = new UpdateManualResults(programId, gameIndex, sport, betName);
                DialogResult dialogresult = dialog.ShowDialog();
                if (dialogresult == DialogResult.OK)
                {
                    _btPersistanceSrv.UpdateTipWithManualResults(long.Parse(programId),
                        int.Parse(gameIndex),
                        dialog.Mark, dialog.Result);
                }
            }
            else
                MessageBox.Show($"אנא המתן שתהליך סינכרון תוצאות במחסן הסתיים");
        }

        private void buttonGetAllTips_Click(object sender, EventArgs e)
        {
            var tips = _btTipsSrv.GetAllStackTips();
            var counters = _btTipsSrv.GetTipsCounters(tips);
            this.label_numberOfTips.Text = $"{tips.Count()} מספר המלצות פתוחות";
            dataGridViewStack.Rows.Clear();
            dataGridViewStack.Columns.Clear();
            dataGridViewStack.Refresh();
            BuildGrid(tips, counters, dataGridViewStack, false, true);
            AddStackContextMenu();
        }

        private void BuildGrid(List<Tip> tips, Dictionary<int, int> counters, DataGridView dgv, bool showResults, bool showBankerimFullTip)
        {
            Type type = typeof(MonitorTip);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            foreach (var column in columns)
            {
                if (!showResults && (column.Name.Contains("Official") || column.Name.Contains("Manual")))
                    continue;
                if (!showBankerimFullTip && column.Name.Contains("Bankerim"))
                    continue;
                dgv.Columns.Add(column.Name, column.Name);
            }
            var tippers = _btPersistanceSrv.GetAllTippers();
            foreach (var tip in tips)
            {
                var monitorTip = new MonitorTip(tip,
                    tippers.FirstOrDefault(t => t.TipperId == tip.TipperId)?.TipperName,
                    counters.ContainsKey(tip.TipId)
                    ? counters[tip.TipId]
                    : 0);
                FillGrid(monitorTip, dgv.Columns, type, dgv, showResults);
            }
            if (!showResults)
                ShrinkColumns(dgv.Columns);
        }

        private void ShrinkColumns(DataGridViewColumnCollection columns)
        {
            columns[5].Width = 2; //totoid
            columns[7].Width = 2; //is automatic
            columns[12].Width = 2; //isparent
            columns[14].Width = 2; //isSingle
            columns[16].Width = 2; //status
            columns[17].Width = 2; //tiptime
            columns[19].Width = 2; //tipmarks
        }

        private void FillGrid(MonitorTip tip, DataGridViewColumnCollection columns, Type type, DataGridView dgv, bool showResults)
        {
            DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
            var cellIndex = 0;
            var isActive = true;
            for (var colIndex = 0; colIndex < columns.Count; colIndex++)
            {
                var methodInfo = type.GetMethod($"get_{columns[colIndex].Name}");
                var result = methodInfo.Invoke(tip, null);
                row.Cells[cellIndex].Value = result;
                if (columns[colIndex].Name == "IsActive")
                    isActive = Boolean.Parse(result.ToString());
                cellIndex++;
            }
            row.DefaultCellStyle.BackColor = isActive
                ? Color.White
                : Color.LightGray;
            if (showResults)
                StyleRow(row);
            dgv.Rows.Add(row);
        }


        private void StyleRow(DataGridViewRow row)
        {
            var officialMark = row.Cells[22].Value?.ToString().Trim();
            var manualMark = row.Cells[24].Value?.ToString().Trim();
            var betMark = row.Cells[13]?.Value.ToString().Trim();
            if (!string.IsNullOrWhiteSpace(officialMark) && !string.IsNullOrEmpty(officialMark))
            {
                row.Cells[22].Style.BackColor = officialMark != betMark
                    ? Color.OrangeRed
                    : Color.GreenYellow;
                row.Cells[24].Style.BackColor = Color.White;
            }
            else if (!string.IsNullOrWhiteSpace(manualMark) && !string.IsNullOrEmpty(manualMark))
            {
                row.Cells[24].Style.BackColor = manualMark != betMark
                    ? Color.OrangeRed
                    : Color.GreenYellow;
                row.Cells[22].Style.BackColor = Color.White;
            }
            else
            {
                row.Cells[22].Style.BackColor = Color.White;
                row.Cells[24].Style.BackColor = Color.White;
            }
        }

        private void buttonAddManualTip_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_btPersistanceSrv.ReadFinishTipGenerationState())
                {
                    var programId = this.dataGridViewStack.Rows.Count > 0
                        ? (long)this.dataGridViewStack.Rows[0].Cells[1].Value
                        : (long?)null;
                    var manualTipDialog = new ManualTipForm(programId);
                    DialogResult dialogresult = manualTipDialog.ShowDialog();
                    if (dialogresult == DialogResult.OK)
                    {
                        var tip = manualTipDialog.GetTip();
                        _btTipsSrv.AddManualTip(tip.ProgramId,
                            tip.Sport,
                            tip.GameIndex,
                            tip.Country,
                            tip.BetLeague,
                            tip.GameStart,
                            tip.PowerTip,
                            tip.IsSingle,
                            tip.BetMark,
                            tip.TipContent,
                            tip.TipUrl,
                            tip.BetRate,
                            tip.BetName,
                            tip.IsParentBet,
                            tip.TipperId
                            );
                        manualTipDialog.Dispose();
                    }
                }
                else
                    MessageBox.Show($"אנא המתן שתהליך סינכרון תוצאות במחסן הסתיים");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error occurred: {Environment.NewLine}{ex.ToString()}");
            }
        }

        private void buttonShowWarhouse_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_btPersistanceSrv.ReadFinishTipGenerationState())
                {

                    var tips = _btTipsSrv.GetWarehouseTips(this.dateTimePickerWarehouse.Value);
                    var counters = _btTipsSrv.GetTipsCounters(tips);
                    dataGridViewWarehouse.Rows.Clear();
                    dataGridViewWarehouse.Columns.Clear();
                    dataGridViewWarehouse.Refresh();
                    BuildGrid(tips, counters, dataGridViewWarehouse, true, false);
                    AddWarehouseContextMenu();
                }
                else
                    MessageBox.Show($"אנא המתן שתהליך סינכרון תוצאות במחסן הסתיים");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error occurred: {Environment.NewLine}{ex.ToString()}");
            }
        }

        private void buttonCRM_Click(object sender, EventArgs e)
        {
            var dialog = new CRM();
            var dialogresult = dialog.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dialog = new SubscribersStatistics();
            var dialogresult = dialog.ShowDialog();
        }

        private void buttonBroadcastHours_Click(object sender, EventArgs e)
        {
            var dialog = new BroadcastHours();
            var dialogresult = dialog.ShowDialog();
        }

        private void buttonTippers_Click(object sender, EventArgs e)
        {
            var dialog = new Tippers();
            var dialogresult = dialog.ShowDialog();
        }
    }
}
