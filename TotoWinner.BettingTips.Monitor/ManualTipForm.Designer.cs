﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class ManualTipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonAddTip = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxProgramId = new System.Windows.Forms.TextBox();
            this.labelSport = new System.Windows.Forms.Label();
            this.comboBoxSport = new System.Windows.Forms.ComboBox();
            this.labelGameStartDate = new System.Windows.Forms.Label();
            this.dateTimePickerGameStart = new System.Windows.Forms.DateTimePicker();
            this.labelCountry = new System.Windows.Forms.Label();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.checkBoxSingle = new System.Windows.Forms.CheckBox();
            this.radioButtonFather = new System.Windows.Forms.RadioButton();
            this.panelParentChild = new System.Windows.Forms.Panel();
            this.radioButtonChild = new System.Windows.Forms.RadioButton();
            this.numericUpDownRate = new System.Windows.Forms.NumericUpDown();
            this.groupBoxBetMark = new System.Windows.Forms.GroupBox();
            this.radioButtonMark2 = new System.Windows.Forms.RadioButton();
            this.radioButtonMarkX = new System.Windows.Forms.RadioButton();
            this.radioButtonMark1 = new System.Windows.Forms.RadioButton();
            this.textBoxBetName = new System.Windows.Forms.TextBox();
            this.labelBetName = new System.Windows.Forms.Label();
            this.labelRate = new System.Windows.Forms.Label();
            this.numericUpDownGameIndex = new System.Windows.Forms.NumericUpDown();
            this.labelGameIndex = new System.Windows.Forms.Label();
            this.textBoxTipContent = new System.Windows.Forms.TextBox();
            this.labelTipContent = new System.Windows.Forms.Label();
            this.textBoxTipUrl = new System.Windows.Forms.TextBox();
            this.labelTipUrl = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.numericUpDownPowerTip = new System.Windows.Forms.NumericUpDown();
            this.labelPowerTip = new System.Windows.Forms.Label();
            this.labelLeague = new System.Windows.Forms.Label();
            this.textBoxLeague = new System.Windows.Forms.TextBox();
            this.comboBoxTipper = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelParentChild.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRate)).BeginInit();
            this.groupBoxBetMark.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGameIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPowerTip)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAddTip
            // 
            this.buttonAddTip.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonAddTip.Location = new System.Drawing.Point(22, 400);
            this.buttonAddTip.Name = "buttonAddTip";
            this.buttonAddTip.Size = new System.Drawing.Size(385, 23);
            this.buttonAddTip.TabIndex = 18;
            this.buttonAddTip.Text = "הוסף טיפ";
            this.buttonAddTip.UseVisualStyleBackColor = true;
            this.buttonAddTip.Click += new System.EventHandler(this.buttonAddTip_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(678, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "מספר תוכניה";
            // 
            // textBoxProgramId
            // 
            this.textBoxProgramId.Location = new System.Drawing.Point(566, 52);
            this.textBoxProgramId.Name = "textBoxProgramId";
            this.textBoxProgramId.Size = new System.Drawing.Size(100, 20);
            this.textBoxProgramId.TabIndex = 2;
            // 
            // labelSport
            // 
            this.labelSport.AutoSize = true;
            this.labelSport.Location = new System.Drawing.Point(678, 16);
            this.labelSport.Name = "labelSport";
            this.labelSport.Size = new System.Drawing.Size(61, 13);
            this.labelSport.TabIndex = 3;
            this.labelSport.Text = "ענף ספורט";
            // 
            // comboBoxSport
            // 
            this.comboBoxSport.FormattingEnabled = true;
            this.comboBoxSport.Items.AddRange(new object[] {
            "כדורגל",
            "כדורסל",
            "טניס",
            "כדור יד",
            "בייסבול",
            "פוטבול אמריקאי"});
            this.comboBoxSport.Location = new System.Drawing.Point(566, 16);
            this.comboBoxSport.Name = "comboBoxSport";
            this.comboBoxSport.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.comboBoxSport.Size = new System.Drawing.Size(100, 21);
            this.comboBoxSport.TabIndex = 1;
            this.comboBoxSport.Text = "כדורגל";
            // 
            // labelGameStartDate
            // 
            this.labelGameStartDate.AutoSize = true;
            this.labelGameStartDate.Location = new System.Drawing.Point(678, 89);
            this.labelGameStartDate.Name = "labelGameStartDate";
            this.labelGameStartDate.Size = new System.Drawing.Size(73, 13);
            this.labelGameStartDate.TabIndex = 5;
            this.labelGameStartDate.Text = "תחילת משחק";
            // 
            // dateTimePickerGameStart
            // 
            this.dateTimePickerGameStart.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dateTimePickerGameStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerGameStart.Location = new System.Drawing.Point(539, 83);
            this.dateTimePickerGameStart.Name = "dateTimePickerGameStart";
            this.dateTimePickerGameStart.Size = new System.Drawing.Size(127, 20);
            this.dateTimePickerGameStart.TabIndex = 3;
            // 
            // labelCountry
            // 
            this.labelCountry.AutoSize = true;
            this.labelCountry.Location = new System.Drawing.Point(676, 125);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(75, 13);
            this.labelCountry.TabIndex = 11;
            this.labelCountry.Text = "מדינה/תחרות";
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Location = new System.Drawing.Point(566, 122);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxCountry.Size = new System.Drawing.Size(100, 20);
            this.textBoxCountry.TabIndex = 4;
            // 
            // checkBoxSingle
            // 
            this.checkBoxSingle.AutoSize = true;
            this.checkBoxSingle.Checked = true;
            this.checkBoxSingle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSingle.Location = new System.Drawing.Point(684, 221);
            this.checkBoxSingle.Name = "checkBoxSingle";
            this.checkBoxSingle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxSingle.Size = new System.Drawing.Size(56, 17);
            this.checkBoxSingle.TabIndex = 7;
            this.checkBoxSingle.Text = "סינגל";
            this.checkBoxSingle.UseVisualStyleBackColor = true;
            // 
            // radioButtonFather
            // 
            this.radioButtonFather.AutoSize = true;
            this.radioButtonFather.Checked = true;
            this.radioButtonFather.Location = new System.Drawing.Point(129, 3);
            this.radioButtonFather.Name = "radioButtonFather";
            this.radioButtonFather.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonFather.Size = new System.Drawing.Size(48, 17);
            this.radioButtonFather.TabIndex = 8;
            this.radioButtonFather.TabStop = true;
            this.radioButtonFather.Text = "אבא";
            this.radioButtonFather.UseVisualStyleBackColor = true;
            // 
            // panelParentChild
            // 
            this.panelParentChild.Controls.Add(this.radioButtonChild);
            this.panelParentChild.Controls.Add(this.radioButtonFather);
            this.panelParentChild.Location = new System.Drawing.Point(472, 217);
            this.panelParentChild.Name = "panelParentChild";
            this.panelParentChild.Size = new System.Drawing.Size(200, 27);
            this.panelParentChild.TabIndex = 7;
            // 
            // radioButtonChild
            // 
            this.radioButtonChild.AutoSize = true;
            this.radioButtonChild.Location = new System.Drawing.Point(69, 3);
            this.radioButtonChild.Name = "radioButtonChild";
            this.radioButtonChild.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonChild.Size = new System.Drawing.Size(37, 17);
            this.radioButtonChild.TabIndex = 9;
            this.radioButtonChild.Text = "בן";
            this.radioButtonChild.UseVisualStyleBackColor = true;
            // 
            // numericUpDownRate
            // 
            this.numericUpDownRate.DecimalPlaces = 2;
            this.numericUpDownRate.Location = new System.Drawing.Point(543, 259);
            this.numericUpDownRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRate.Name = "numericUpDownRate";
            this.numericUpDownRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericUpDownRate.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownRate.TabIndex = 10;
            this.numericUpDownRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownRate.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBoxBetMark
            // 
            this.groupBoxBetMark.Controls.Add(this.radioButtonMark2);
            this.groupBoxBetMark.Controls.Add(this.radioButtonMarkX);
            this.groupBoxBetMark.Controls.Add(this.radioButtonMark1);
            this.groupBoxBetMark.Location = new System.Drawing.Point(544, 285);
            this.groupBoxBetMark.Name = "groupBoxBetMark";
            this.groupBoxBetMark.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBoxBetMark.Size = new System.Drawing.Size(136, 91);
            this.groupBoxBetMark.TabIndex = 9;
            this.groupBoxBetMark.TabStop = false;
            this.groupBoxBetMark.Text = "הימור";
            // 
            // radioButtonMark2
            // 
            this.radioButtonMark2.AutoSize = true;
            this.radioButtonMark2.Location = new System.Drawing.Point(92, 65);
            this.radioButtonMark2.Name = "radioButtonMark2";
            this.radioButtonMark2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonMark2.TabIndex = 13;
            this.radioButtonMark2.Text = "2";
            this.radioButtonMark2.UseVisualStyleBackColor = true;
            // 
            // radioButtonMarkX
            // 
            this.radioButtonMarkX.AutoSize = true;
            this.radioButtonMarkX.Location = new System.Drawing.Point(92, 42);
            this.radioButtonMarkX.Name = "radioButtonMarkX";
            this.radioButtonMarkX.Size = new System.Drawing.Size(32, 17);
            this.radioButtonMarkX.TabIndex = 12;
            this.radioButtonMarkX.Text = "X";
            this.radioButtonMarkX.UseVisualStyleBackColor = true;
            // 
            // radioButtonMark1
            // 
            this.radioButtonMark1.AutoSize = true;
            this.radioButtonMark1.Location = new System.Drawing.Point(92, 20);
            this.radioButtonMark1.Name = "radioButtonMark1";
            this.radioButtonMark1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonMark1.TabIndex = 11;
            this.radioButtonMark1.TabStop = true;
            this.radioButtonMark1.Text = "1";
            this.radioButtonMark1.UseVisualStyleBackColor = true;
            // 
            // textBoxBetName
            // 
            this.textBoxBetName.Location = new System.Drawing.Point(137, 40);
            this.textBoxBetName.Name = "textBoxBetName";
            this.textBoxBetName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxBetName.Size = new System.Drawing.Size(193, 20);
            this.textBoxBetName.TabIndex = 14;
            // 
            // labelBetName
            // 
            this.labelBetName.AutoSize = true;
            this.labelBetName.Location = new System.Drawing.Point(344, 43);
            this.labelBetName.Name = "labelBetName";
            this.labelBetName.Size = new System.Drawing.Size(63, 13);
            this.labelBetName.TabIndex = 21;
            this.labelBetName.Text = "שם ההימור";
            // 
            // labelRate
            // 
            this.labelRate.AutoSize = true;
            this.labelRate.Location = new System.Drawing.Point(677, 261);
            this.labelRate.Name = "labelRate";
            this.labelRate.Size = new System.Drawing.Size(26, 13);
            this.labelRate.TabIndex = 16;
            this.labelRate.Text = "יחס";
            // 
            // numericUpDownGameIndex
            // 
            this.numericUpDownGameIndex.Location = new System.Drawing.Point(548, 188);
            this.numericUpDownGameIndex.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownGameIndex.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownGameIndex.Name = "numericUpDownGameIndex";
            this.numericUpDownGameIndex.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericUpDownGameIndex.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownGameIndex.TabIndex = 6;
            this.numericUpDownGameIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownGameIndex.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelGameIndex
            // 
            this.labelGameIndex.AutoSize = true;
            this.labelGameIndex.Location = new System.Drawing.Point(677, 190);
            this.labelGameIndex.Name = "labelGameIndex";
            this.labelGameIndex.Size = new System.Drawing.Size(67, 13);
            this.labelGameIndex.TabIndex = 23;
            this.labelGameIndex.Text = "מספר הימור";
            // 
            // textBoxTipContent
            // 
            this.textBoxTipContent.Location = new System.Drawing.Point(22, 86);
            this.textBoxTipContent.Multiline = true;
            this.textBoxTipContent.Name = "textBoxTipContent";
            this.textBoxTipContent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxTipContent.Size = new System.Drawing.Size(385, 174);
            this.textBoxTipContent.TabIndex = 15;
            // 
            // labelTipContent
            // 
            this.labelTipContent.AutoSize = true;
            this.labelTipContent.Location = new System.Drawing.Point(331, 70);
            this.labelTipContent.Name = "labelTipContent";
            this.labelTipContent.Size = new System.Drawing.Size(60, 13);
            this.labelTipContent.TabIndex = 26;
            this.labelTipContent.Text = "תוכן הטיפ";
            // 
            // textBoxTipUrl
            // 
            this.textBoxTipUrl.Location = new System.Drawing.Point(71, 266);
            this.textBoxTipUrl.Name = "textBoxTipUrl";
            this.textBoxTipUrl.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxTipUrl.Size = new System.Drawing.Size(336, 20);
            this.textBoxTipUrl.TabIndex = 16;
            // 
            // labelTipUrl
            // 
            this.labelTipUrl.AutoSize = true;
            this.labelTipUrl.Location = new System.Drawing.Point(36, 269);
            this.labelTipUrl.Name = "labelTipUrl";
            this.labelTipUrl.Size = new System.Drawing.Size(29, 13);
            this.labelTipUrl.TabIndex = 29;
            this.labelTipUrl.Text = "URL";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // numericUpDownPowerTip
            // 
            this.numericUpDownPowerTip.Location = new System.Drawing.Point(103, 297);
            this.numericUpDownPowerTip.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownPowerTip.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPowerTip.Name = "numericUpDownPowerTip";
            this.numericUpDownPowerTip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericUpDownPowerTip.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownPowerTip.TabIndex = 17;
            this.numericUpDownPowerTip.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownPowerTip.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // labelPowerTip
            // 
            this.labelPowerTip.AutoSize = true;
            this.labelPowerTip.Location = new System.Drawing.Point(36, 299);
            this.labelPowerTip.Name = "labelPowerTip";
            this.labelPowerTip.Size = new System.Drawing.Size(52, 13);
            this.labelPowerTip.TabIndex = 31;
            this.labelPowerTip.Text = "PowerTip";
            // 
            // labelLeague
            // 
            this.labelLeague.AutoSize = true;
            this.labelLeague.Location = new System.Drawing.Point(676, 158);
            this.labelLeague.Name = "labelLeague";
            this.labelLeague.Size = new System.Drawing.Size(32, 13);
            this.labelLeague.TabIndex = 32;
            this.labelLeague.Text = "ליגה";
            // 
            // textBoxLeague
            // 
            this.textBoxLeague.Location = new System.Drawing.Point(567, 155);
            this.textBoxLeague.Name = "textBoxLeague";
            this.textBoxLeague.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxLeague.Size = new System.Drawing.Size(100, 20);
            this.textBoxLeague.TabIndex = 5;
            // 
            // comboBoxTipper
            // 
            this.comboBoxTipper.FormattingEnabled = true;
            this.comboBoxTipper.Location = new System.Drawing.Point(181, 13);
            this.comboBoxTipper.Name = "comboBoxTipper";
            this.comboBoxTipper.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.comboBoxTipper.Size = new System.Drawing.Size(149, 21);
            this.comboBoxTipper.TabIndex = 33;
            this.comboBoxTipper.Text = "אתר בנקרים";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(344, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "שם ממליץ";
            // 
            // ManualTipForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 441);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxTipper);
            this.Controls.Add(this.textBoxLeague);
            this.Controls.Add(this.labelLeague);
            this.Controls.Add(this.numericUpDownPowerTip);
            this.Controls.Add(this.labelPowerTip);
            this.Controls.Add(this.labelTipUrl);
            this.Controls.Add(this.textBoxTipUrl);
            this.Controls.Add(this.labelTipContent);
            this.Controls.Add(this.textBoxTipContent);
            this.Controls.Add(this.numericUpDownGameIndex);
            this.Controls.Add(this.labelGameIndex);
            this.Controls.Add(this.textBoxBetName);
            this.Controls.Add(this.labelBetName);
            this.Controls.Add(this.groupBoxBetMark);
            this.Controls.Add(this.numericUpDownRate);
            this.Controls.Add(this.labelRate);
            this.Controls.Add(this.panelParentChild);
            this.Controls.Add(this.checkBoxSingle);
            this.Controls.Add(this.textBoxCountry);
            this.Controls.Add(this.labelCountry);
            this.Controls.Add(this.dateTimePickerGameStart);
            this.Controls.Add(this.labelGameStartDate);
            this.Controls.Add(this.comboBoxSport);
            this.Controls.Add(this.labelSport);
            this.Controls.Add(this.textBoxProgramId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAddTip);
            this.Name = "ManualTipForm";
            this.Text = "ManualTipForm";
            this.panelParentChild.ResumeLayout(false);
            this.panelParentChild.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRate)).EndInit();
            this.groupBoxBetMark.ResumeLayout(false);
            this.groupBoxBetMark.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGameIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPowerTip)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddTip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxProgramId;
        private System.Windows.Forms.Label labelSport;
        private System.Windows.Forms.ComboBox comboBoxSport;
        private System.Windows.Forms.Label labelGameStartDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerGameStart;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.CheckBox checkBoxSingle;
        private System.Windows.Forms.RadioButton radioButtonFather;
        private System.Windows.Forms.Panel panelParentChild;
        private System.Windows.Forms.RadioButton radioButtonChild;
        private System.Windows.Forms.NumericUpDown numericUpDownRate;
        private System.Windows.Forms.GroupBox groupBoxBetMark;
        private System.Windows.Forms.RadioButton radioButtonMark2;
        private System.Windows.Forms.RadioButton radioButtonMarkX;
        private System.Windows.Forms.RadioButton radioButtonMark1;
        private System.Windows.Forms.TextBox textBoxBetName;
        private System.Windows.Forms.Label labelBetName;
        private System.Windows.Forms.Label labelRate;
        private System.Windows.Forms.NumericUpDown numericUpDownGameIndex;
        private System.Windows.Forms.Label labelGameIndex;
        private System.Windows.Forms.TextBox textBoxTipContent;
        private System.Windows.Forms.Label labelTipContent;
        private System.Windows.Forms.TextBox textBoxTipUrl;
        private System.Windows.Forms.Label labelTipUrl;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.NumericUpDown numericUpDownPowerTip;
        private System.Windows.Forms.Label labelPowerTip;
        private System.Windows.Forms.Label labelLeague;
        private System.Windows.Forms.TextBox textBoxLeague;
        private System.Windows.Forms.ComboBox comboBoxTipper;
        private System.Windows.Forms.Label label2;
    }
}