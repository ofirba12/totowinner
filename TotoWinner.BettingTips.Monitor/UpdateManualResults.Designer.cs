﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class UpdateManualResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelProgramId = new System.Windows.Forms.Label();
            this.textBoxProgramId = new System.Windows.Forms.TextBox();
            this.labelGameIndex = new System.Windows.Forms.Label();
            this.textBoxGameIndex = new System.Windows.Forms.TextBox();
            this.labelBetName = new System.Windows.Forms.Label();
            this.textBoxBetName = new System.Windows.Forms.TextBox();
            this.labelResult = new System.Windows.Forms.Label();
            this.textBoxManualResult = new System.Windows.Forms.TextBox();
            this.labelMark = new System.Windows.Forms.Label();
            this.textBoxManualMark = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(132, 195);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(144, 23);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "שמור";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelProgramId
            // 
            this.labelProgramId.AutoSize = true;
            this.labelProgramId.Location = new System.Drawing.Point(337, 15);
            this.labelProgramId.Name = "labelProgramId";
            this.labelProgramId.Size = new System.Drawing.Size(73, 13);
            this.labelProgramId.TabIndex = 4;
            this.labelProgramId.Text = "מספר תוכנית";
            // 
            // textBoxProgramId
            // 
            this.textBoxProgramId.Location = new System.Drawing.Point(229, 12);
            this.textBoxProgramId.Name = "textBoxProgramId";
            this.textBoxProgramId.ReadOnly = true;
            this.textBoxProgramId.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxProgramId.Size = new System.Drawing.Size(100, 20);
            this.textBoxProgramId.TabIndex = 3;
            // 
            // labelGameIndex
            // 
            this.labelGameIndex.AutoSize = true;
            this.labelGameIndex.Location = new System.Drawing.Point(337, 52);
            this.labelGameIndex.Name = "labelGameIndex";
            this.labelGameIndex.Size = new System.Drawing.Size(67, 13);
            this.labelGameIndex.TabIndex = 6;
            this.labelGameIndex.Text = "מספר הימור";
            // 
            // textBoxGameIndex
            // 
            this.textBoxGameIndex.Location = new System.Drawing.Point(229, 49);
            this.textBoxGameIndex.Name = "textBoxGameIndex";
            this.textBoxGameIndex.ReadOnly = true;
            this.textBoxGameIndex.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxGameIndex.Size = new System.Drawing.Size(100, 20);
            this.textBoxGameIndex.TabIndex = 5;
            // 
            // labelBetName
            // 
            this.labelBetName.AutoSize = true;
            this.labelBetName.Location = new System.Drawing.Point(348, 82);
            this.labelBetName.Name = "labelBetName";
            this.labelBetName.Size = new System.Drawing.Size(56, 13);
            this.labelBetName.TabIndex = 8;
            this.labelBetName.Text = "שם הימור";
            // 
            // textBoxBetName
            // 
            this.textBoxBetName.Location = new System.Drawing.Point(17, 79);
            this.textBoxBetName.Name = "textBoxBetName";
            this.textBoxBetName.ReadOnly = true;
            this.textBoxBetName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxBetName.Size = new System.Drawing.Size(312, 20);
            this.textBoxBetName.TabIndex = 7;
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(261, 136);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(41, 13);
            this.labelResult.TabIndex = 10;
            this.labelResult.Text = "תוצאה";
            // 
            // textBoxManualResult
            // 
            this.textBoxManualResult.Location = new System.Drawing.Point(153, 130);
            this.textBoxManualResult.Name = "textBoxManualResult";
            this.textBoxManualResult.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxManualResult.Size = new System.Drawing.Size(100, 20);
            this.textBoxManualResult.TabIndex = 9;
            // 
            // labelMark
            // 
            this.labelMark.AutoSize = true;
            this.labelMark.Location = new System.Drawing.Point(262, 162);
            this.labelMark.Name = "labelMark";
            this.labelMark.Size = new System.Drawing.Size(36, 13);
            this.labelMark.TabIndex = 12;
            this.labelMark.Text = "סימון";
            // 
            // textBoxManualMark
            // 
            this.textBoxManualMark.Location = new System.Drawing.Point(153, 156);
            this.textBoxManualMark.Name = "textBoxManualMark";
            this.textBoxManualMark.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxManualMark.Size = new System.Drawing.Size(100, 20);
            this.textBoxManualMark.TabIndex = 11;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // UpdateManualResults
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 235);
            this.Controls.Add(this.labelMark);
            this.Controls.Add(this.textBoxManualMark);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.textBoxManualResult);
            this.Controls.Add(this.labelBetName);
            this.Controls.Add(this.textBoxBetName);
            this.Controls.Add(this.labelGameIndex);
            this.Controls.Add(this.textBoxGameIndex);
            this.Controls.Add(this.labelProgramId);
            this.Controls.Add(this.textBoxProgramId);
            this.Controls.Add(this.buttonSave);
            this.Name = "UpdateManualResults";
            this.Text = "UpdateManualResults";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelProgramId;
        private System.Windows.Forms.TextBox textBoxProgramId;
        private System.Windows.Forms.Label labelGameIndex;
        private System.Windows.Forms.TextBox textBoxGameIndex;
        private System.Windows.Forms.Label labelBetName;
        private System.Windows.Forms.TextBox textBoxBetName;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.TextBox textBoxManualResult;
        private System.Windows.Forms.Label labelMark;
        private System.Windows.Forms.TextBox textBoxManualMark;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}