﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class AddEditSubscriber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxChatId = new System.Windows.Forms.TextBox();
            this.textBoxRegistrationId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCell = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxRemarks = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.checkBoxBlockSubscriber = new System.Windows.Forms.CheckBox();
            this.textBoxBlockedReason = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "מספר לקוח";
            // 
            // textBoxChatId
            // 
            this.textBoxChatId.Location = new System.Drawing.Point(87, 34);
            this.textBoxChatId.Name = "textBoxChatId";
            this.textBoxChatId.Size = new System.Drawing.Size(100, 20);
            this.textBoxChatId.TabIndex = 1;
            // 
            // textBoxRegistrationId
            // 
            this.textBoxRegistrationId.Location = new System.Drawing.Point(87, 69);
            this.textBoxRegistrationId.Name = "textBoxRegistrationId";
            this.textBoxRegistrationId.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegistrationId.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "מספר רישום";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(81, 110);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(132, 20);
            this.textBoxFirstName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "שם פרטי";
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(81, 149);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(131, 20);
            this.textBoxLastName.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "שם משפחה";
            // 
            // textBoxMail
            // 
            this.textBoxMail.Location = new System.Drawing.Point(271, 113);
            this.textBoxMail.Name = "textBoxMail";
            this.textBoxMail.Size = new System.Drawing.Size(128, 20);
            this.textBoxMail.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "מייל";
            // 
            // textBoxCell
            // 
            this.textBoxCell.Location = new System.Drawing.Point(272, 148);
            this.textBoxCell.Name = "textBoxCell";
            this.textBoxCell.Size = new System.Drawing.Size(125, 20);
            this.textBoxCell.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(230, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "טלפון";
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.Location = new System.Drawing.Point(15, 204);
            this.textBoxRemarks.Multiline = true;
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(373, 121);
            this.textBoxRemarks.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "הערות";
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(15, 331);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(373, 43);
            this.buttonSubmit.TabIndex = 14;
            this.buttonSubmit.Text = "הוסף לקוח";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // checkBoxBlockSubscriber
            // 
            this.checkBoxBlockSubscriber.AutoSize = true;
            this.checkBoxBlockSubscriber.Location = new System.Drawing.Point(233, 33);
            this.checkBoxBlockSubscriber.Name = "checkBoxBlockSubscriber";
            this.checkBoxBlockSubscriber.Size = new System.Drawing.Size(77, 17);
            this.checkBoxBlockSubscriber.TabIndex = 15;
            this.checkBoxBlockSubscriber.Text = "חסום לקוח";
            this.checkBoxBlockSubscriber.UseVisualStyleBackColor = true;
            // 
            // textBoxBlockedReason
            // 
            this.textBoxBlockedReason.Location = new System.Drawing.Point(233, 56);
            this.textBoxBlockedReason.Multiline = true;
            this.textBoxBlockedReason.Name = "textBoxBlockedReason";
            this.textBoxBlockedReason.Size = new System.Drawing.Size(169, 42);
            this.textBoxBlockedReason.TabIndex = 16;
            // 
            // AddEditSubscriber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 377);
            this.Controls.Add(this.textBoxBlockedReason);
            this.Controls.Add(this.checkBoxBlockSubscriber);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxRemarks);
            this.Controls.Add(this.textBoxCell);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxMail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxLastName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxFirstName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxRegistrationId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxChatId);
            this.Controls.Add(this.label1);
            this.Name = "AddEditSubscriber";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.Text = "פרטי לקוח";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxChatId;
        private System.Windows.Forms.TextBox textBoxRegistrationId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCell;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxRemarks;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.CheckBox checkBoxBlockSubscriber;
        private System.Windows.Forms.TextBox textBoxBlockedReason;
    }
}