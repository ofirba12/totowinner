﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TotoWinner.Common;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    internal static class MonitorUtils
    {
        internal static void BuildGrid<T>(List<T> monitorData, DataGridView dgv)
        {
            Type type = typeof(T);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            var columnIndex = 0;
            var zebraCells = new List<int>();
            var colorAlgoLogic_1Cells = new List<int>();
            var colorAlgoLogic_2Cells = new List<int>();
            var colorAlgoLogic_3Cells = new List<int>();
            var colorAlgoLogic_4Cells = new List<int>();
            var colorAlgoLogic_5Cells = new List<int>();
            dgv.EnableHeadersVisualStyles = false;
            foreach (var column in columns)
            {
                dgv.Columns.Add(column.Name, column.Name);
                var style = column.GetCustomAttribute<ColumnStyleAttribute>();
                if (style != null)
                {
                    dgv.Columns[column.Name].Visible = !style.Hide;
                    if (style.Width > 0)
                        dgv.Columns[column.Name].Width = style.Width;
                    if (!string.IsNullOrEmpty(style.DisplayName))
                    {
                        dgv.Columns[column.Name].HeaderText = style.DisplayName;
                        dgv.Columns[column.Name].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    }
                    if (style.DisableSort)
                        dgv.Columns[column.Name].SortMode = DataGridViewColumnSortMode.NotSortable;
                    if (style.FontBold)
                    {
                        dgv.Columns[column.Name].DefaultCellStyle.Font = new Font(dgv.Font, FontStyle.Bold);
                    }
                    if (style.ZebraOn)
                        zebraCells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 1)
                        colorAlgoLogic_1Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 2)
                        colorAlgoLogic_2Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 3)
                        colorAlgoLogic_3Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 4)
                        colorAlgoLogic_4Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 5)
                        colorAlgoLogic_5Cells.Add(columnIndex);
                    if (style.HeaderColor == 1)
                        dgv.Columns[column.Name].HeaderCell.Style.BackColor = Color.GreenYellow;
                    if (style.Freeze)
                        dgv.Columns[column.Name].Frozen = true;
                }
                dgv.Columns[column.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columnIndex++;
            }
            foreach (var match in monitorData)
            {
                var row = CreateGridRow<T>(match, type, dgv,
                    zebraCells,
                    colorAlgoLogic_1Cells,
                    colorAlgoLogic_2Cells,
                    colorAlgoLogic_3Cells,
                    colorAlgoLogic_4Cells,
                    colorAlgoLogic_5Cells);
                dgv.Rows.Add(row);
            }
        }
        internal static DataGridViewRow CreateGridRow<T>(T match,
            Type type,
            DataGridView dgv,
            List<int> zebraCells,
            List<int> colorAlgoLogic_1Cells,
            List<int> colorAlgoLogic_2Cells,
            List<int> colorAlgoLogic_3Cells,
            List<int> colorAlgoLogic_4Cells,
            List<int> colorAlgoLogic_5Cells)
        {
            DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
            var cellIndex = 0;
            var ignore = false;
            var missingOdds = false;
            var missingScores = false;
            var missingQuarters = false;
            var rankItem = 0;
            for (var colIndex = 0; colIndex < dgv.Columns.Count; colIndex++)
            {
                var property = dgv.Columns[colIndex].Name;
                var methodInfo = type.GetMethod($"get_{property}");
                var result = methodInfo.Invoke(match, null);
                row.Cells[cellIndex].Value = result;
                cellIndex++;
            }
            row.Tag = match;
            StyleGridRow(row, ignore, missingOdds, missingScores, missingQuarters, rankItem, zebraCells,
                colorAlgoLogic_1Cells, colorAlgoLogic_2Cells, colorAlgoLogic_3Cells, colorAlgoLogic_4Cells, colorAlgoLogic_5Cells);
            return row;
        }

        private static void StyleGridRow(DataGridViewRow row,
            bool ignore,
            bool missingOdds,
            bool missingScores,
            bool missingQuarters,
            int rankItem,
            List<int> zebraCells,
            List<int> colorAlgoLogic_1Cells,
            List<int> colorAlgoLogic_2Cells,
            List<int> colorAlgoLogic_3Cells,
            List<int> colorAlgoLogic_4Cells,
            List<int> colorAlgoLogic_5Cells)
        {
            row.DefaultCellStyle.BackColor = ignore
                ? Color.LightGray
                : Color.White;
            row.DefaultCellStyle.BackColor = missingQuarters
                ? Color.LightSalmon
                : row.DefaultCellStyle.BackColor;
            row.DefaultCellStyle.BackColor = missingOdds || rankItem > 0
                ? Color.LightCoral
                : row.DefaultCellStyle.BackColor;
            row.DefaultCellStyle.BackColor = missingScores
                ? Color.OrangeRed
                : row.DefaultCellStyle.BackColor;

            foreach (var cellIndex in zebraCells)
                row.Cells[cellIndex].Style.BackColor = Color.LightGray;
            foreach (var cellIndex in colorAlgoLogic_1Cells)
            {
                var cellVal = (int?)row.Cells[cellIndex].Value;
                if (cellVal.HasValue)
                {
                    if (cellVal.Value >= 75 && cellVal.Value <= 94)
                        row.Cells[cellIndex].Style.BackColor = Color.LightYellow;
                    else if (cellVal.Value >= 95 && cellVal.Value <= 100)
                        row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                    else
                    {
                        if (zebraCells.Contains(cellIndex))
                            row.Cells[cellIndex].Style.BackColor = Color.LightGray;
                        else
                            row.Cells[cellIndex].Style.BackColor = Color.White;
                    }
                }
                else
                {
                    if (zebraCells.Contains(cellIndex))
                        row.Cells[cellIndex].Style.BackColor = Color.LightGray;
                    else
                        row.Cells[cellIndex].Style.BackColor = Color.White;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_2Cells)
            {
                var cellVal = (int?)row.Cells[cellIndex].Value;
                if (cellVal.HasValue && cellVal.Value > 0)
                    row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                else
                    row.Cells[cellIndex].Style.BackColor = Color.White;
            }
            foreach (var cellIndex in colorAlgoLogic_3Cells)
            {
                var cellVal = (bool)row.Cells[cellIndex + 1].Value;
                if (!cellVal)
                    row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                else
                    row.Cells[cellIndex].Style.BackColor = Color.White;
            }
            foreach (var cellIndex in colorAlgoLogic_4Cells)
            {
                var cellVal = row.Cells[cellIndex].Value;
                switch(cellVal)
                {
                    case "HOME WIN":
                    case "OVER 2.5":
                    case "RANGE 0-1":
                        row.Cells[cellIndex - 2].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.LightYellow;
                        break;
                    case "DRAW":
                    case "RANGE 2-3":
                        row.Cells[cellIndex - 2].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.LightYellow;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.White;
                        break;
                    case "AWAY WIN":
                    case "UNDER 2.5":
                    case "RANGE 4+":
                        row.Cells[cellIndex - 2].Style.BackColor = Color.LightYellow;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.White;
                        break;
                    default:
                        row.Cells[cellIndex - 2].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.White;
                        break;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_5Cells)
            {
                var cellVal = (int?)row.Cells[cellIndex].Value;
                if (cellVal.HasValue)
                {
                    if (cellVal.Value > 60)
                        row.Cells[cellIndex].Style.BackColor = Color.LightYellow;
                    //else 
                    //    row.Cells[cellIndex].Style.BackColor = Color.White;
                }
            }
        }
    }
}
