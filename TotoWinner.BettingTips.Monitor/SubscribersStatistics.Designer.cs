﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class SubscribersStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelTotalVisited = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonShow = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewStatistics = new System.Windows.Forms.DataGridView();
            this.buttonButtonsStatistics = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatistics)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonButtonsStatistics);
            this.panel1.Controls.Add(this.labelTotalVisited);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buttonShow);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(973, 49);
            this.panel1.TabIndex = 0;
            // 
            // labelTotalVisited
            // 
            this.labelTotalVisited.AutoSize = true;
            this.labelTotalVisited.Location = new System.Drawing.Point(662, 19);
            this.labelTotalVisited.Name = "labelTotalVisited";
            this.labelTotalVisited.Size = new System.Drawing.Size(84, 13);
            this.labelTotalVisited.TabIndex = 2;
            this.labelTotalVisited.Text = "labelTotalVisited";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(703, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "סה\"כ ביקורים";
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(819, 14);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(133, 23);
            this.buttonShow.TabIndex = 0;
            this.buttonShow.Text = "הצג";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewStatistics);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(973, 401);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewStatistics
            // 
            this.dataGridViewStatistics.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStatistics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewStatistics.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewStatistics.Name = "dataGridViewStatistics";
            this.dataGridViewStatistics.Size = new System.Drawing.Size(973, 401);
            this.dataGridViewStatistics.TabIndex = 0;
            // 
            // buttonButtonsStatistics
            // 
            this.buttonButtonsStatistics.Location = new System.Drawing.Point(466, 13);
            this.buttonButtonsStatistics.Name = "buttonButtonsStatistics";
            this.buttonButtonsStatistics.Size = new System.Drawing.Size(144, 23);
            this.buttonButtonsStatistics.TabIndex = 3;
            this.buttonButtonsStatistics.Text = "סטטיסטיקת כפתורים";
            this.buttonButtonsStatistics.UseVisualStyleBackColor = true;
            this.buttonButtonsStatistics.Click += new System.EventHandler(this.buttonButtonsStatistics_Click);
            // 
            // SubscribersStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "SubscribersStatistics";
            this.Text = "סטטיסטיקות";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatistics)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewStatistics;
        private System.Windows.Forms.Label labelTotalVisited;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonButtonsStatistics;
    }
}