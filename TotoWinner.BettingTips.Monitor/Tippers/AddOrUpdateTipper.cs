﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BettingTips;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class AddOrUpdateTipper : Form
    {
        public bool IsAddMode { get; }
        private List<Tipper> _tippers = null;
        public string TipperName { get; private set; }
        public AddOrUpdateTipper(bool addTipper, List<Data.BettingTips.Tipper> tippers, string tipperName)
        {
            this.IsAddMode = addTipper;
            this._tippers = tippers;
            this.TipperName = tipperName;
            InitializeComponent();
            if (IsAddMode)
            {
                this.Text = "הוסף ממליץ";
                this.buttonUpsert.Text = "הוסף ממליץ";
            }
            else
                this.textBoxTipperName.Text = tipperName;
        }

        private void buttonUpsert_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBoxTipperName.Text))
            {
                MessageBox.Show("אנא מלא שם ממליץ", "חסר שם ממליץ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            var exists = _tippers.Any(t => t.TipperName == this.textBoxTipperName.Text);
            if (exists)
            {
                MessageBox.Show("אנא בחר שם ממליץ אחר", "שם ממליץ כבר קיים", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            this.TipperName = this.textBoxTipperName.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
