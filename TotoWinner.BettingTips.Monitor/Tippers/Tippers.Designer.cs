﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class Tippers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonAddTipper = new System.Windows.Forms.Button();
            this.dataGridViewTippers = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTippers)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonAddTipper);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(510, 44);
            this.panel1.TabIndex = 0;
            // 
            // buttonAddTipper
            // 
            this.buttonAddTipper.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddTipper.Location = new System.Drawing.Point(435, 0);
            this.buttonAddTipper.Name = "buttonAddTipper";
            this.buttonAddTipper.Size = new System.Drawing.Size(75, 44);
            this.buttonAddTipper.TabIndex = 0;
            this.buttonAddTipper.Text = "הוסף ממליץ";
            this.buttonAddTipper.UseVisualStyleBackColor = true;
            this.buttonAddTipper.Click += new System.EventHandler(this.buttonAddTipper_Click);
            // 
            // dataGridViewTippers
            // 
            this.dataGridViewTippers.AllowUserToDeleteRows = false;
            this.dataGridViewTippers.AllowUserToOrderColumns = true;
            this.dataGridViewTippers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewTippers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTippers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTippers.Location = new System.Drawing.Point(0, 44);
            this.dataGridViewTippers.Name = "dataGridViewTippers";
            this.dataGridViewTippers.ReadOnly = true;
            this.dataGridViewTippers.Size = new System.Drawing.Size(510, 406);
            this.dataGridViewTippers.TabIndex = 1;
            this.dataGridViewTippers.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTippers_CellMouseEnter);
            // 
            // Tippers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 450);
            this.Controls.Add(this.dataGridViewTippers);
            this.Controls.Add(this.panel1);
            this.Name = "Tippers";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.Text = "ממליצים";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTippers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonAddTipper;
        public System.Windows.Forms.DataGridView dataGridViewTippers;
    }
}