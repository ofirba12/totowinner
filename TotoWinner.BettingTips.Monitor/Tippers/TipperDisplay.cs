﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.BettingTips.Monitor
{
    public class TipperDisplay
    {
        [ColumnStyle(DisplayName = "מספר", Width = 20)]
        public int TipperId { get; }
        [ColumnStyle(DisplayName = "שם ממליץ")]
        public string TipperName { get; }
        public TipperDisplay(int tipperId, string tipperName)
        {
            this.TipperId = tipperId;
            this.TipperName = tipperName;
        }
    }
}
