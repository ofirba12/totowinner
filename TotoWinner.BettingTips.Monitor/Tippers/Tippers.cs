﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telegram.Bot.Types;
using TotoWinner.Data.BettingTips;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class Tippers : Form
    {
        private BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        private List<Tipper> _tippers = null;

        private DataGridViewCellEventArgs mouseLocation;
        ToolStripMenuItem contextMenuEdit = new ToolStripMenuItem();
        ToolStripMenuItem contextMenuDelete = new ToolStripMenuItem();

        public Tippers()
        {
            InitializeComponent();
            LoadTippers();
        }

        private void LoadTippers()
        {
            _tippers = _btPersistanceSrv.GetAllTippers().ConvertAll<Tipper>(t => new Tipper(t.TipperId, t.TipperName));
            this.dataGridViewTippers.Rows.Clear();
            this.dataGridViewTippers.Columns.Clear();
            this.dataGridViewTippers.Refresh();
            MonitorUtils.BuildGrid<TipperDisplay>(_tippers.ConvertAll<TipperDisplay>(t => new TipperDisplay(t.TipperId, t.TipperName)),
                this.dataGridViewTippers);
            AddContextMenu();
        }

        private void buttonAddTipper_Click(object sender, EventArgs e)
        {
            var dialog = new AddOrUpdateTipper(true, _tippers, null);
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                _btPersistanceSrv.AddTipper(dialog.TipperName);
                LoadTippers();
            }
        }

        private void dataGridViewTippers_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        private void AddContextMenu()
        {
            contextMenuEdit.Text = "עדכן ממליץ";
            contextMenuEdit.Click -= new EventHandler(contextMenuEdit_Click);
            contextMenuEdit.Click += new EventHandler(contextMenuEdit_Click);

            contextMenuDelete.Text = "מחק ממליץ";
            contextMenuDelete.Click -= new EventHandler(contextMenuDelete_Click);
            contextMenuDelete.Click += new EventHandler(contextMenuDelete_Click);

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridViewTippers.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridViewTippers.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridViewTippers_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewTippers.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            selectedRow.ContextMenuStrip.Items.Clear();
            selectedRow.ContextMenuStrip.Items.Add(contextMenuEdit);
            var selectedTipper = (TipperDisplay)selectedRow.Tag;
            if (selectedTipper.TipperId != 1)
                selectedRow.ContextMenuStrip.Items.Add(contextMenuDelete);
        }
        private void contextMenuEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewTippers.Rows[mouseLocation.RowIndex];
            var selectedTip = (TipperDisplay)selectedRow.Tag;
            var dialog = new AddOrUpdateTipper(false, _tippers, selectedTip.TipperName);
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                _btPersistanceSrv.UpdateTipper(selectedTip.TipperId, dialog.TipperName);
                LoadTippers();
            }
        }
        private void contextMenuDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewTippers.Rows[mouseLocation.RowIndex];
            var selectedTip = (TipperDisplay)selectedRow.Tag;
            if (!_btPersistanceSrv.IsTipperInUse(selectedTip.TipperId))
            {
                var yesNoDialog = MessageBox.Show($"אתה עומד למחוק את ממליץ {selectedTip.TipperName}.{Environment.NewLine} ?אתה מאשר","מחיקת ממליץ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNoDialog == DialogResult.Yes)
                {
                    _btPersistanceSrv.DeleteTipper(selectedTip.TipperId);
                    LoadTippers();
                }
            }
            else
                MessageBox.Show("לא ניתן למחוק ממליץ זה, כיוון שישנם טיפים במערכת שהוא ממליץ שלהם", "לא ניתן למחוק ממליץ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
    }
}
