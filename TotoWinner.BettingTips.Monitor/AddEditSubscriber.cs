﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class AddEditSubscriber : Form
    {
        private bool EditSubscriber;
        protected TelegramBotServices _botSrv = TelegramBotServices.Instance;

        public AddEditSubscriber(Data.TelegramBot.BotSubsriber subscriber)
        {
            InitializeComponent();
            this.EditSubscriber = subscriber != null;
            if (this.EditSubscriber)
            {
                this.textBoxChatId.Text = subscriber.ClientChatId.ToString();
                //this.textBoxChatId.ReadOnly = !string.IsNullOrEmpty(this.textBoxChatId.Text);
                this.textBoxRegistrationId.Text = subscriber.RegistrationId.ToString();
                this.textBoxRegistrationId.ReadOnly = true;
                this.textBoxFirstName.Text = subscriber.FirstName;
                this.textBoxLastName.Text = subscriber.LastName;
                this.textBoxMail.Text = subscriber.Mail;
                this.textBoxCell.Text = subscriber.CellNumber;
                this.textBoxBlockedReason.Text = subscriber.BlockageReason;
                this.checkBoxBlockSubscriber.Checked = subscriber.BlockedDate != null;
                this.textBoxRemarks.Text = subscriber.Remarks;
                this.buttonSubmit.Text = "עדכן לקוח";
            }
            else
            {
                this.textBoxRegistrationId.ReadOnly = true;
                this.textBoxChatId.ReadOnly = true;
                this.buttonSubmit.Text = "הוסף לקוח";
            }
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.EditSubscriber)
                {
                    var newSubscriber = new BotSubsriber(-1, this.textBoxFirstName.Text,
                        this.textBoxLastName.Text,
                        this.textBoxCell.Text,
                        this.textBoxMail.Text,
                        -1,
                        DateTime.Now,
                        this.textBoxRemarks.Text,
                        null, null);
                    var subscribers = _botSrv.GetSubsribers(false);
                    if (subscribers.Any(s=> s.CellNumber == this.textBoxCell.Text))
                    {
                        MessageBox.Show($"קיים כבר לקוח במאגר הנתונים עם מספר טלפון {this.textBoxCell.Text}");
                        this.DialogResult = DialogResult.None;
                        return;
                    }
                    var registrationId = _botSrv.InsertSubscriber(newSubscriber);
                    this.textBoxRegistrationId.Text = registrationId.ToString();
                }
                else
                {
                    var chatId = long.Parse(this.textBoxChatId.Text);
                    var registrationId = int.Parse(this.textBoxRegistrationId.Text);
                    var beforeSubscriber = _botSrv.GetSubscriberByRegistarionId(registrationId);

                    DateTime? blocked = beforeSubscriber.BlockedDate;
                    if (!beforeSubscriber.BlockedDate.HasValue && this.checkBoxBlockSubscriber.Checked)
                        blocked = DateTime.Now;
                    else if (!this.checkBoxBlockSubscriber.Checked)
                        blocked = null;

                    var newSubscriber = new BotSubsriber(chatId, this.textBoxFirstName.Text,
                        this.textBoxLastName.Text,
                        this.textBoxCell.Text,
                        this.textBoxMail.Text,
                        registrationId,
                        DateTime.Now,
                        this.textBoxRemarks.Text,
                        blocked, this.textBoxBlockedReason.Text);
                    _botSrv.UpdateSubscriber(newSubscriber);
                }
                this.DialogResult = DialogResult.OK;
            }
            catch(Exception ex)
            {
                MessageBox.Show($"אירעה תקלה, אנא בדוק נתונים{Environment.NewLine}{Environment.NewLine}{ex.ToString()}");
                this.DialogResult = DialogResult.None;
            }
        }
    }
}
