﻿using System.Windows.Forms;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class BotButtonStatistics : Form
    {
        private TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;

        public BotButtonStatistics()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            var counters = _botPersistanceSrv.GetButtonStatistics();
            foreach (var item in counters)
            {
                switch(item.Id)
                {
                    case 1:
                        this.label1.Text = "הטיפ המועדף שלנו";
                        this.textBox1.Text = item.Counter.ToString();
                        break;
                    case 2:
                        this.label2.Text = "טיפ למשחק שמתחיל בקרוב";
                        this.textBox2.Text = item.Counter.ToString();
                        break;
                    case 3:
                        this.label3.Text = "טיפ סינגל ביחס גבוה";
                        this.textBox3.Text = item.Counter.ToString();
                        break;
                    case 4:
                        this.label4.Text = "טיפ מותאם אישית";
                        this.textBox4.Text = item.Counter.ToString();
                        break;
                    case 5:
                        this.label5.Text = "נבחרת ממליצים";
                        this.textBox5.Text = item.Counter.ToString();
                        break;
                }
            }
        }
    }
}
