﻿using System;
using System.Windows.Forms;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class UpdateManualResults : Form
    {
        public string Result
        {
            get
            {
                return this.textBoxManualResult.Text;
            }
        }
        public string Mark
        {
            get
            {
                return this.textBoxManualMark.Text;
            }
        }
        public UpdateManualResults()
        {
            InitializeComponent();
        }
        public UpdateManualResults(string programId, string gameIndex, string sport, string betName) : this()
        {
            this.textBoxBetName.Text = betName;
            this.textBoxGameIndex.Text = gameIndex;
            this.textBoxProgramId.Text = programId;
            this.Text = $"Manual Results Update For {sport}";
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!ValidateNonEmptyValues(this.textBoxManualResult)
                || !ValidateNonEmptyValues(this.textBoxManualMark))
            {
                DialogResult = DialogResult.None;
            }
            else
                DialogResult = DialogResult.OK;
        }
        private bool ValidateNonEmptyValues(TextBox textBox)
        {
            var bStatus = true;
            if (string.IsNullOrEmpty(textBox.Text.Trim()))
            {
                bStatus = false;
                this.errorProvider1.SetError(textBox, "נא למלא ערך");
            }
            else
                errorProvider1.SetError(textBox, "");
            return bStatus;
        }
    }
}
