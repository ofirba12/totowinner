﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BettingTips;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class ManualTipForm : Form
    {
        private BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private BettingTipsPersistanceServices _btPersistance = BettingTipsPersistanceServices.Instance;
        private List<Services.Persistance.BettingTips.Tippers_GetAll_Result> _tippers;
        public ManualTipForm(Tip tip) : this(tip.ProgramId)
        {
            this.buttonAddTip.Text = "עדכן טיפ";
            this.comboBoxSport.Text = BettingTipsServicesHelpers.HebrewTranslate(tip.Sport);
            //this.textBoxProgramId.Text = tip.ProgramId.ToString();
            this.dateTimePickerGameStart.Value = tip.GameStart;
            this.textBoxCountry.Text = tip.Country;
            this.textBoxLeague.Text = tip.BetLeague;
            this.numericUpDownGameIndex.Value = tip.GameIndex;
            this.checkBoxSingle.Checked = tip.IsSingle;
            this.radioButtonFather.Checked = tip.IsParentBet;
            this.radioButtonChild.Checked = !tip.IsParentBet;
            this.numericUpDownRate.Value = tip.BetRate;
            this.radioButtonMark1.Checked = tip.BetMark == '1';
            this.radioButtonMarkX.Checked = tip.BetMark == 'X';
            this.radioButtonMark2.Checked = tip.BetMark == '2';
            this.textBoxBetName.Text = tip.BetName;
            this.textBoxTipContent.Text = tip.TipContent;
            this.textBoxTipUrl.Text = tip.TipUrl;
            this.numericUpDownPowerTip.Value = tip.PowerTip;
            this.comboBoxTipper.Text = _tippers.First(t => t.TipperId == tip.TipperId).TipperName;
        }
        public ManualTipForm(long? defaultProgramId)
        {
            InitializeComponent();
            this.textBoxProgramId.Text = defaultProgramId.HasValue 
                ? defaultProgramId.Value.ToString() 
                : string.Empty;
            this.textBoxProgramId.Validating += TextBoxProgramId_Validating;
            this.textBoxCountry.Validating += TextBoxCountry_Validating;
            this.textBoxBetName.Validating += TextBoxBetName_Validating;
            this.textBoxTipContent.Validating += TextBoxTipContent_Validating;
            this.textBoxTipUrl.Validating += TextBoxTipUrl_Validating;
            _tippers = _btPersistance.GetAllTippers();
            this.comboBoxTipper.DataSource = _tippers.Select(t => t.TipperName).ToList();
        }
        public Tip GetTip()
        {
            var sport = SportIds.Soccer;
            switch(this.comboBoxSport.Text)
            {
                case "כדורגל":
                    sport = SportIds.Soccer;
                    break;
                case "כדורסל":
                    sport = SportIds.Basketball;
                    break;
                case "טניס":
                    sport = SportIds.Tennis;
                    break;
                case "כדור יד":
                    sport = SportIds.Handball;
                    break;
                case "בייסבול":
                    sport = SportIds.Baseball;
                    break;
                case "פוטבול אמריקאי":
                    sport = SportIds.Football;
                    break;
                default:
                    throw new Exception($"{this.comboBoxSport.Text} ספורט לא נתמך");
            }
            var mark = '1';
            if (this.radioButtonMark1.Checked)
                mark = '1';
            else if (this.radioButtonMarkX.Checked)
                mark = 'X';
            else if (this.radioButtonMark2.Checked)
                mark = '2';

            var selectedTipper = _tippers.FirstOrDefault(t => t.TipperName == comboBoxTipper.Text);
            var tipperId = selectedTipper != null ? selectedTipper.TipperId : 1;
            var tip = new Tip(0, long.Parse(this.textBoxProgramId.Text),
                (int)this.numericUpDownPowerTip.Value,
                sport,
                -1,
                this.textBoxCountry.Text,
                (int)this.numericUpDownGameIndex.Value,
                this.dateTimePickerGameStart.Value,
                false,
                this.textBoxBetName.Text,
                this.textBoxLeague.Text,
                this.radioButtonFather.Checked,
                this.checkBoxSingle.Checked,
                mark,
                this.numericUpDownRate.Value,
                null,
                this.textBoxTipContent.Text,
                null,
                this.textBoxTipUrl.Text,
                null, null, null, null,
                true,
                tipperId
                );
            return tip;
        }
        private void TextBoxTipUrl_Validating(object sender, CancelEventArgs e)
        {
           // e.Cancel = !ValidateNonEmptyValues(this.textBoxTipUrl);
        }

        private void TextBoxTipContent_Validating(object sender, CancelEventArgs e)
        {
            //e.Cancel = !ValidateNonEmptyValues(this.textBoxTipContent);
        }

        private void TextBoxBetName_Validating(object sender, CancelEventArgs e)
        {
           // e.Cancel = !ValidateNonEmptyValues(this.textBoxBetName);
        }

        private void TextBoxCountry_Validating(object sender, CancelEventArgs e)
        {
           // e.Cancel = !ValidateNonEmptyValues(this.textBoxCountry);
        }

        private void TextBoxProgramId_Validating(object sender, CancelEventArgs e)
        {
            //e.Cancel = !ValidateProgramId();
        }

        private void buttonAddTip_Click(object sender, EventArgs e)
        {
            if (!ValidateProgramId() ||
                !ValidateNonEmptyValues(this.textBoxCountry) ||
                !ValidateNonEmptyValues(this.textBoxLeague) ||
                !ValidateNonEmptyValues(this.textBoxBetName) ||
                !ValidateNonEmptyValues(this.textBoxTipContent) ||
                !ValidateNonEmptyValues(this.textBoxTipUrl)  ||
                !ValidateMarkingButtons()
                )
            {
                this.DialogResult = DialogResult.None;
            }
            else
                this.DialogResult = DialogResult.OK;
        }

        private bool ValidateMarkingButtons()
        {
            var isChecked = this.radioButtonMark1.Checked || radioButtonMarkX.Checked || radioButtonMark2.Checked;
            if (!isChecked)
                this.errorProvider1.SetError(this.radioButtonMark1, "נא לעדכן סימון");
            return isChecked;
        }

        private bool ValidateProgramId()
        {
            var bStatus = true;
            if (textBoxProgramId.Text.Length < 9 || !long.TryParse(textBoxProgramId.Text, out var programId))
            {
                bStatus = false;
                this.errorProvider1.SetError(textBoxProgramId, "מספר תוכניה לא חוקי; מינימום 9 ספרות");
            }
            else
                errorProvider1.SetError(textBoxProgramId, "");
            return bStatus;
        }
        private bool ValidateNonEmptyValues(TextBox textBox)
        {
            var bStatus = true;
            if (string.IsNullOrEmpty(textBox.Text.Trim()))
            {
                bStatus = false;
                this.errorProvider1.SetError(textBox, "נא למלא ערך");
            }
            else
                errorProvider1.SetError(textBox, "");
            return bStatus;
        }
    }
}
