﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class SubscribersStatistics : Form
    {
        private TelegramBotServices _botSrv = TelegramBotServices.Instance;
        public SubscribersStatistics()
        {
            InitializeComponent();
            buttonShow_Click(null, null);
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            var statistics = _botSrv.GenerateStatistics();
            dataGridViewStatistics.Rows.Clear();
            dataGridViewStatistics.Columns.Clear();
            dataGridViewStatistics.Refresh();
            BuildGrid(statistics, dataGridViewStatistics);
            this.labelTotalVisited.Text = statistics.TotalVisited.ToString();
        }

        private void BuildGrid(BotSubsribersStatistics repository, DataGridView dgv)
        {
            Type type = typeof(BotSubsribersStatistic);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            foreach (var column in columns)
                dgv.Columns.Add(column.Name, column.Name);

            foreach (var item in repository.Statistics)
                FillGrid(item.Value, dgv.Columns, type, dgv);
        }

        private void FillGrid(BotSubsribersStatistic item, DataGridViewColumnCollection columns, Type type, DataGridView dgv)
        {
            DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
            var cellIndex = 0;
            for (var colIndex = 0; colIndex < columns.Count; colIndex++)
            {
                var methodInfo = type.GetMethod($"get_{columns[colIndex].Name}");
                var result = methodInfo.Invoke(item, null);
                row.Cells[cellIndex].Value = result;
                cellIndex++;
            }
            row.Tag = item;
            dgv.Rows.Add(row);
        }

        private void buttonButtonsStatistics_Click(object sender, EventArgs e)
        {
            var dialog = new BotButtonStatistics();
            var dialogresult = dialog.ShowDialog();
        }
    }
}
