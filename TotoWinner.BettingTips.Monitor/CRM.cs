﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Telegram.Bot;
using TotoWinner.Data;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Monitor
{
    public partial class CRM : Form
    {
        private string _botToken;
        static TelegramBotClient _botClient;
        private TelegramBotServices _botSrv = TelegramBotServices.Instance;
        protected BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private DataGridViewCellEventArgs mouseLocation;
        ToolStripMenuItem toolStripEditCustomer = new ToolStripMenuItem();
        //ToolStripMenuItem toolStripBlockCustomer = new ToolStripMenuItem();
        ToolStripMenuItem toolStripSendTip = new ToolStripMenuItem();
        ToolStripMenuItem toolStripDeleteCustomer = new ToolStripMenuItem();



        public CRM()
        {
            InitializeComponent();
            _botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
            _botClient = new TelegramBotClient(_botToken);
            this.dataGridViewCustomers.CellMouseEnter += DataGridViewCustomers_CellMouseEnter;
            buttonRefreshCustomers_Click(null, null);
        }

        private void DataGridViewCustomers_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }

        private void buttonRefreshCustomers_Click(object sender, EventArgs e)
        {
            var subsribers = _botSrv.GetSubsribers(false);
            UpdateTotalsTitles(subsribers);
            dataGridViewCustomers.Rows.Clear();
            dataGridViewCustomers.Columns.Clear();
            dataGridViewCustomers.Refresh();
            BuildGrid(subsribers, dataGridViewCustomers);
            AddStackContextMenu();
        }

        private void UpdateTotalsTitles(List<BotSubsriber> subsribers)
        {
            this.labelBlockedSubscribers.Text = subsribers.Count(s => s.BlockedDate.HasValue).ToString();
            this.labelNonBlockedSubscribers.Text = subsribers.Count(s => !s.BlockedDate.HasValue).ToString();
        }

        private void AddStackContextMenu()
        {
            toolStripEditCustomer.Text = "ערוך";
            toolStripEditCustomer.Click -= new EventHandler(toolStripEditCustomer_Click);
            toolStripEditCustomer.Click += new EventHandler(toolStripEditCustomer_Click);

            //toolStripBlockCustomer.Text = "חסום";
            //toolStripBlockCustomer.Click -= new EventHandler(toolStripBlockCustomer_Click);
            //toolStripBlockCustomer.Click += new EventHandler(toolStripBlockCustomer_Click);

            toolStripSendTip.Text = "שלח טיפ";
            toolStripSendTip.Click -= new EventHandler(toolStripSendTip_Click);
            toolStripSendTip.Click += new EventHandler(toolStripSendTip_Click);

            toolStripDeleteCustomer.Text = "מחק";
            toolStripDeleteCustomer.Click -= new EventHandler(toolStripDeleteCustomer_Click);
            toolStripDeleteCustomer.Click += new EventHandler(toolStripDeleteCustomer_Click);

            ContextMenuStrip strip = new ContextMenuStrip();
            foreach (DataGridViewRow row in dataGridViewCustomers.Rows)
            {
                row.ContextMenuStrip = strip;
                row.ContextMenuStrip.Items.Add(toolStripEditCustomer);
//                row.ContextMenuStrip.Items.Add(toolStripBlockCustomer);
                row.ContextMenuStrip.Items.Add(toolStripSendTip);
                row.ContextMenuStrip.Items.Add(toolStripDeleteCustomer);
            }
        }
        private void toolStripEditCustomer_Click(object sender, EventArgs args)
        {
            var subscriber = (BotSubsriber)this.dataGridViewCustomers.Rows[mouseLocation.RowIndex].Tag;
            UpsertSubscriber(subscriber);
        }


        //private void toolStripBlockCustomer_Click(object sender, EventArgs args)
        //{

        //}
        private void toolStripSendTip_Click(object sender, EventArgs args)
        {
            var subscriber = (BotSubsriber)this.dataGridViewCustomers.Rows[mouseLocation.RowIndex].Tag;
            var tip = _botSrv.FindTipToPublish(subscriber.ClientChatId.Value);
            _botSrv.SentTip(tip, subscriber.ClientChatId.Value, _botClient);
        }
        private void toolStripDeleteCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                var subscriber = (BotSubsriber)this.dataGridViewCustomers.Rows[mouseLocation.RowIndex].Tag;
                var message = $"האם אתה בטוח שברצונך למחוק לקוח {Environment.NewLine}{subscriber.FirstName} {subscriber.LastName}{Environment.NewLine}?לקוח מספר {subscriber.ClientChatId}";
                var yesNoDialog = MessageBox.Show(message, "מחיקת לקוח", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign);
                if (yesNoDialog == DialogResult.Yes)
                {
                    _botSrv.DeleteSubscriber(subscriber.ClientChatId, subscriber.RegistrationId);
                    buttonRefreshCustomers_Click(null, null);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"אירעה תקלה, אנא בדוק נתונים{Environment.NewLine}{Environment.NewLine}{ex.ToString()}");
            }
        }
        private void BuildGrid(List<BotSubsriber> subsribers, DataGridView dgv)
        {
            Type type = typeof(BotSubsriber);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            foreach (var column in columns)
                dgv.Columns.Add(column.Name, column.Name);

            foreach (var subscriber in subsribers)
                FillGrid(subscriber, dgv.Columns, type, dgv);
        }

        private void FillGrid(BotSubsriber subscriber, DataGridViewColumnCollection columns, Type type, DataGridView dgv)
        {
            DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
            var cellIndex = 0;
            for (var colIndex = 0; colIndex < columns.Count; colIndex++)
            {
                var methodInfo = type.GetMethod($"get_{columns[colIndex].Name}");
                var result = methodInfo.Invoke(subscriber, null);
                row.Cells[cellIndex].Value = result;
                cellIndex++;
            }
            row.Tag = subscriber;
            dgv.Rows.Add(row);
        }

        private void buttonAddCustomer_Click(object sender, EventArgs e)
        {
            UpsertSubscriber(null);
        }

        private void UpsertSubscriber(BotSubsriber subscriber)
        {
            var dialog = new AddEditSubscriber(subscriber);
            var dialogresult = dialog.ShowDialog();
            if (dialogresult == DialogResult.OK)
                buttonRefreshCustomers_Click(null, null);
        }

    }
}
