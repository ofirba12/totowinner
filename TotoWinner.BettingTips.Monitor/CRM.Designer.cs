﻿namespace TotoWinner.BettingTips.Monitor
{
    partial class CRM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelNonBlockedSubscribers = new System.Windows.Forms.Label();
            this.labelBlockedSubscribers = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAddCustomer = new System.Windows.Forms.Button();
            this.buttonRefreshCustomers = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewCustomers = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomers)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelNonBlockedSubscribers);
            this.panel1.Controls.Add(this.labelBlockedSubscribers);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buttonAddCustomer);
            this.panel1.Controls.Add(this.buttonRefreshCustomers);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1226, 37);
            this.panel1.TabIndex = 0;
            // 
            // labelNonBlockedSubscribers
            // 
            this.labelNonBlockedSubscribers.AutoSize = true;
            this.labelNonBlockedSubscribers.Location = new System.Drawing.Point(753, 9);
            this.labelNonBlockedSubscribers.Name = "labelNonBlockedSubscribers";
            this.labelNonBlockedSubscribers.Size = new System.Drawing.Size(14, 13);
            this.labelNonBlockedSubscribers.TabIndex = 5;
            this.labelNonBlockedSubscribers.Text = "#";
            // 
            // labelBlockedSubscribers
            // 
            this.labelBlockedSubscribers.AutoSize = true;
            this.labelBlockedSubscribers.Location = new System.Drawing.Point(926, 9);
            this.labelBlockedSubscribers.Name = "labelBlockedSubscribers";
            this.labelBlockedSubscribers.Size = new System.Drawing.Size(14, 13);
            this.labelBlockedSubscribers.TabIndex = 4;
            this.labelBlockedSubscribers.Text = "#";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(773, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "מספר לקוחות לא חסומים";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(946, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "מספר לקוחות חסומים";
            // 
            // buttonAddCustomer
            // 
            this.buttonAddCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddCustomer.Location = new System.Drawing.Point(1076, 0);
            this.buttonAddCustomer.Name = "buttonAddCustomer";
            this.buttonAddCustomer.Size = new System.Drawing.Size(75, 37);
            this.buttonAddCustomer.TabIndex = 1;
            this.buttonAddCustomer.Text = "הוסף לקוח";
            this.buttonAddCustomer.UseVisualStyleBackColor = true;
            this.buttonAddCustomer.Click += new System.EventHandler(this.buttonAddCustomer_Click);
            // 
            // buttonRefreshCustomers
            // 
            this.buttonRefreshCustomers.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRefreshCustomers.Location = new System.Drawing.Point(1151, 0);
            this.buttonRefreshCustomers.Name = "buttonRefreshCustomers";
            this.buttonRefreshCustomers.Size = new System.Drawing.Size(75, 37);
            this.buttonRefreshCustomers.TabIndex = 0;
            this.buttonRefreshCustomers.Text = "הצג/רענן";
            this.buttonRefreshCustomers.UseVisualStyleBackColor = true;
            this.buttonRefreshCustomers.Click += new System.EventHandler(this.buttonRefreshCustomers_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewCustomers);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1226, 592);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewCustomers
            // 
            this.dataGridViewCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCustomers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCustomers.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewCustomers.Name = "dataGridViewCustomers";
            this.dataGridViewCustomers.Size = new System.Drawing.Size(1226, 592);
            this.dataGridViewCustomers.TabIndex = 0;
            // 
            // CRM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 629);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "CRM";
            this.Text = "CRM";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonAddCustomer;
        private System.Windows.Forms.Button buttonRefreshCustomers;
        private System.Windows.Forms.DataGridView dataGridViewCustomers;
        private System.Windows.Forms.Label labelNonBlockedSubscribers;
        private System.Windows.Forms.Label labelBlockedSubscribers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}