﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TotoWinner.Backtesting.Analyzer
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //TODO: not use RAM, use File System instead
        //Instead of Results use Folder with programNumber and inside files with template
        private static Dictionary<int, List<string>> Results = new Dictionary<int, List<string>>(); //programNumber, template
        //Instead of Results use Folder with template and inside files with programNumber
        private static Dictionary<string, List<int>> TemplatesWithPrograms = new Dictionary<string, List<int>>();//template, list<programNumber>
        private static readonly string rootResultFolder = "Results";
        private static readonly string rootTempFolder = "#Temp#";
        private static int deepFileReaderLinesSize = 0;
        static void Main(string[] args)
        {
            try
            {
                Console.OutputEncoding = new UTF8Encoding();
                var rootFolder = ConfigurationManager.AppSettings["BacktestingResultRootFolder"];
                deepFileReaderLinesSize = Convert.ToInt32(ConfigurationManager.AppSettings["DeepFileReaderLinesSize"]);
                if (Directory.Exists(rootResultFolder))
                {
                    Directory.Delete(rootResultFolder, true);
                }
                var resultInfo = Directory.CreateDirectory(rootResultFolder);
                if (Directory.Exists(rootTempFolder))
                {
                    Directory.Delete(rootTempFolder, true);
                }
                Directory.CreateDirectory(rootTempFolder);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("ROUND 1 - Starting");
                ProcessDirectory(rootFolder);
                Console.WriteLine("ROUND 1 - Finish");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ROUND 2 - Starting");
                Round2Processing(rootResultFolder);
                Console.WriteLine("ROUND 2 - Finish");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Result can be seen in folder :{resultInfo.FullName}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _log.Fatal("An unexpected error occurred.", ex);
            }
            finally
            {
                Console.WriteLine("Press any key to exist:");
                Console.ReadLine();
            }
        }

        private static void Round2Processing(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                Round2ProcessFile(fileName);

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                Round2Processing(subdirectory);
        }

        private static void Round2ProcessFile(string fullFileName)
        {
            //D1-G1  BA1-BB1-BC1-CC1_2_4_944784.txt
            if (fullFileName.Contains("__"))
                return;
            var info = new FileInfo(fullFileName);
            var programNumber = info.Directory.Name;
            var filename = info.Name;
            var units = filename.Split('_');
            var template = units[0];
            var tempDir = Path.Combine(rootTempFolder, template);
            if (!Directory.Exists(tempDir))
                return;
            var tempDirInfo = new DirectoryInfo(Path.Combine(rootTempFolder, template));
            string[] fileEntries = Directory.GetFiles(tempDir);
            var programs = fileEntries.Select(i => Path.GetFileNameWithoutExtension(i)).ToList();
            foreach (var pNumber in fileEntries)
            {
                var pNumberInfo = Path.GetFileNameWithoutExtension(pNumber);
                var resultFilePathToUpdate = Path.Combine(rootResultFolder, pNumberInfo);
                var candidatesFiles = Directory.GetFiles(resultFilePathToUpdate);
                foreach (var fileRename in candidatesFiles)
                {
                    var pureFilename = Path.GetFileNameWithoutExtension(fileRename);
                    var pureUnits = pureFilename.Split('_');
                    if (pureUnits[0] == template)
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(fileRename, true))
                        {
                            file.WriteLine(string.Join(Environment.NewLine, programs));
                        }
                        var newNamePath = Path.Combine(Path.GetDirectoryName(fileRename), $"{programs.Count}__{Path.GetFileName(fileRename)}");
                        File.Move(fileRename, newNamePath);
                    }
                }
            }
        }

        private static void DumpResult_V2()
        {
            foreach (var kpv in Results)
            {
                var filename = Path.Combine("Results", $"{kpv.Key.ToString()}.csv");
                using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(filename, true))
                {
                    file.WriteLine("template,bankers,doubles,total,other16");
                    foreach (var template in kpv.Value)
                    {
                        var newLine = new List<string>();
                        newLine.Add(template);
                        var tuple = ParseTemplate(template);
                        var bankers = tuple.Item1;
                        newLine.Add(bankers.ToString());
                        var doubles = tuple.Item2;
                        newLine.Add(doubles.ToString());
                        var totals = Math.Max(1, Math.Pow(3, 16 - bankers - doubles)) * Math.Max(1, Math.Pow(2, doubles));
                        newLine.Add(totals.ToString());
                        var other16 = string.Join("+", TemplatesWithPrograms[template]);
                        newLine.Add(other16);
                        file.WriteLine(string.Join(",", newLine));
                    }
                }
            }
        }

        private static Tuple<int, int> ParseTemplate(string template)
        {
            var bankers = 0;
            var doubles = 0;
            var pattern = @"([A-Z]+)([0-9]+)";
            var templatesUnits = template.Split(' ');//D1-G1  BA1-BB1-BC1-FC1
            foreach (var unit in templatesUnits)
            {
                if (string.IsNullOrWhiteSpace(unit))
                    continue;
                var tUnits = unit.Split('-');
                foreach (var tUnit in tUnits)
                {
                    var match = Regex.Match(tUnit, pattern);
                    var tName = match.Groups[1].ToString();
                    var tCounter = Convert.ToInt32(match.Groups[2].ToString());
                    if (tName.Length == 1)
                        bankers += tCounter;
                    if (tName.Length == 2)
                        doubles += tCounter;
                }
            }

            var result = new Tuple<int, int>(bankers, doubles);
            return result;
        }

        //private static void DumpResult_V1()
        //{
        //    var filename = "Analyzer.csv";
        //    List<string> all = new List<string>();
        //    all.Add(string.Join(",", Results.Keys));
        //    System.IO.File.WriteAllLines(filename, all.ToArray());
        //    using (System.IO.StreamWriter file =
        //    new System.IO.StreamWriter(filename, true))
        //    {
        //        var maxLines = Results.Values.Max(i => i.Keys.Max());
        //        for (var currentLine = 0; currentLine <= maxLines; currentLine++)
        //        {
        //            var newLine = new List<string>();
        //            foreach (var kpv in Results)
        //            {
        //                if (kpv.Value.ContainsKey(currentLine))
        //                {
        //                    newLine.Add(kpv.Value[currentLine]);
        //                }
        //                else
        //                {
        //                    newLine.Add(" ");
        //                }
        //            }
        //            file.WriteLine(string.Join(",", newLine));
        //        }
        //    }

        //    Console.WriteLine($"{Environment.NewLine}File created: {filename}");
        //}

        public static void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName);

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                ProcessDirectory(subdirectory);
        }
        public static void AddTemplateToProgramFolder(int programNumber, string template)
        {
            var programFolder = Path.Combine(rootResultFolder, programNumber.ToString());
            if (!Directory.Exists(programFolder))
            {
                Directory.CreateDirectory(programFolder);
            }
            var filenameUnits = new List<string>();
            filenameUnits.Add(template);
            var tuple = ParseTemplate(template);
            var bankers = tuple.Item1;
            filenameUnits.Add(bankers.ToString());
            var doubles = tuple.Item2;
            filenameUnits.Add(doubles.ToString());
            var totals = Math.Max(1, Math.Pow(3, 16 - bankers - doubles)) * Math.Max(1, Math.Pow(2, doubles));
            filenameUnits.Add(totals.ToString());
            //Add this on second round
            //var other16 = string.Join("+", TemplatesWithPrograms[template]);
            //newLine.Add(other16);
            var filename = string.Join("_", filenameUnits);
            var templateFullFilePath = Path.Combine(programFolder, $"{filename}.txt");
            if (!File.Exists(templateFullFilePath))
            {
                using (var fs = File.Create(templateFullFilePath)) { }
            }
        }
        public static void AddProgramToTemplateFolder(int programNumber, string template)
        {
            var templateFolder = Path.Combine(rootTempFolder, template);
            if (!Directory.Exists(templateFolder))
            {
                Directory.CreateDirectory(templateFolder);
            }
            var programFullFilePath = Path.Combine(templateFolder, $"{programNumber}.txt");
            if (!File.Exists(programFullFilePath))
            {
                using (var fs = File.Create(programFullFilePath)) { }
            }
        }
        // Insert logic for processing found files here.
        public static void ProcessFile(string path)
        {
            Console.WriteLine("Processed file '{0}'.", path);
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            //var currentIndex = Results.Values.Count > 0
            //    ? Results.Values.Max(i => i.Keys.Max())
            //    : 0;
            var lineNumber = 0; 
            while ((line = file.ReadLine()) != null && lineNumber < deepFileReaderLinesSize)
            {
                if (line.StartsWith("ProgramType"))
                    continue;
                lineNumber++;
                string[] items = line.Split(",".ToCharArray());
                var firstWin = Convert.ToInt32(items[7]);
                if (firstWin == 0)
                {
                    lineNumber = deepFileReaderLinesSize;
                    continue;
                }
                var template = items[3];
                var winningProgramsFirstWin = items[10].Split('+');
                foreach (var programNumberStr in winningProgramsFirstWin)
                {
                    try
                    {
                        var programNumber = Convert.ToInt32(programNumberStr);
                        if (!Results.ContainsKey(programNumber))
                            Results.Add(programNumber, new List<string>());
                        //Results.Add(programNumber, new Dictionary<int, string>());
                        //Results[programNumber].Add(currentIndex, template);
                        AddTemplateToProgramFolder(programNumber, template);
                        AddProgramToTemplateFolder(programNumber, template);
                        Results[programNumber].Add(template);
                        if (!TemplatesWithPrograms.ContainsKey(template))
                        {
                            TemplatesWithPrograms.Add(template, new List<int>());
                        }
                        TemplatesWithPrograms[template].Add(programNumber);
                    }
                    catch (Exception ex)
                    {
                        var msg = $"An error occurred in {programNumberStr}, {path}";
                        throw new Exception(msg, ex);
                    }
                }
                //currentIndex++;
            }
        }
    }
}
