﻿USE BgExchangeTrading
GO

CREATE TABLE [dbo].[Portfolios]
(
	[PortfolioId]		INT	IDENTITY(10000,1)	NOT NULL PRIMARY KEY,
	[Expiry]			DATETIME				NOT NULL 
)
GO
CREATE PROCEDURE [dbo].[Portfolios_Merge]
	@PortfolioId	INT,
	@Expiry			DATETIME			
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Portfolios] AS [Target]
	USING ( SELECT	 @PortfolioId			AS	"PortfolioId"
					,@Expiry				AS	"Expiry"
	) AS [Source]	ON [Source].[PortfolioId]	= [Target].[PortfolioId]
					AND [Source].[Expiry]		= [Target].[Expiry]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Expiry])
		VALUES ([Source].[Expiry])
		OUTPUT Inserted.PortfolioId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END

GO
CREATE PROCEDURE [dbo].[Portfolios_GetAll]
AS
	SELECT * FROM Portfolios 
RETURN 0
GO
ALTER PROCEDURE [dbo].[Portfolios_Delete]
	@PortfolioId INT 
AS
	DELETE FROM PortfolioAssets WHERE [PortfolioId]=@PortfolioId
	DELETE FROM Portfolios WHERE [PortfolioId]=@PortfolioId
RETURN 0

GO
----#########
CREATE TABLE [dbo].[PortfolioAssets]
(
	[AssetId]			INT	IDENTITY(20000,1)	NOT NULL PRIMARY KEY,
	[PortfolioId]		INT						NOT NULL,
	[TickerId]			INT						NOT NULL,
	[TickerExpiry]		DATETIME				NOT NULL,
	[IsCall]			BIT						NULL,
	[Strike]			FLOAT					NULL,
	[IsLong]			BIT						NULL,
	[Premium]			FLOAT					NULL,
	[Size]				INT						NULL,
	[Fee]				FLOAT					NULL
)
GO
CREATE PROCEDURE [dbo].[PortfolioAssets_Merge]
	@AssetId		INT = NULL,
	@PortfolioId	INT,	
	@TickerId		INT,		
	@TickerExpiry	DATETIME,
	@IsCall			BIT,		
	@Strike			FLOAT,	
	@IsLong			BIT,		
	@Premium		FLOAT,	
	@Size			INT,		
	@Fee			FLOAT	
AS

	MERGE dbo.[PortfolioAssets] AS [Target]
	USING ( SELECT	 @AssetId		AS	"AssetId"
					,@PortfolioId	AS	"PortfolioId"
					,@TickerId		AS	"TickerId"
					,@TickerExpiry	AS	"TickerExpiry"
					,@IsCall		AS	"IsCall"
					,@Strike		AS	"Strike"
					,@IsLong		AS	"IsLong"
					,@Premium		AS	"Premium"
					,@Size			AS	"Size"
					,@Fee			AS	"Fee"
	) AS [Source]	ON [Source].[AssetId]	= [Target].[AssetId]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[PortfolioId]		=[Source].[PortfolioId]				
		,[Target].[TickerId]		=[Source].[TickerId]		
		,[Target].[TickerExpiry]	=[Source].[TickerExpiry]	
		,[Target].[IsCall]			=[Source].[IsCall]	
		,[Target].[Strike]			=[Source].[Strike]	
		,[Target].[IsLong]			=[Source].[IsLong]
		,[Target].[Premium]			=[Source].[Premium]
		,[Target].[Size]			=[Source].[Size]
		,[Target].[Fee]				=[Source].[Fee]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[PortfolioId]	
		,[TickerId]		
		,[TickerExpiry]	
		,[IsCall]		
		,[Strike]		
		,[IsLong]		
		,[Premium]		
		,[Size]		
		,[Fee]			
				)
		VALUES (
			[Source].[PortfolioId]			
			,[Source].[TickerId]		
			,[Source].[TickerExpiry]	
			,[Source].[IsCall]		
			,[Source].[Strike]	
			,[Source].[IsLong]
			,[Source].[Premium]
			,[Source].[Size]
			,[Source].[Fee]
				)
				OUTPUT Inserted.AssetId
				;

RETURN 0

GO
CREATE PROCEDURE [dbo].[PortfolioAssets_Get]
	@PortfolioId INT
AS
	SELECT * FROM PortfolioAssets where PortfolioId = @PortfolioId
RETURN 0
GO
CREATE PROCEDURE [dbo].[PortfolioAsset_Delete]
	@AssetId INT 
AS
	DELETE FROM PortfolioAssets WHERE [AssetId]=@AssetId
RETURN 0
GO