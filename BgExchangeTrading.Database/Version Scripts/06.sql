﻿USE BgExchangeTrading
GO

CREATE TABLE [dbo].[TickersLiveData]
(
	[TickerId]		INT			NOT NULL,
	[Spot]			FLOAT		NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL
)
GO
ALTER TABLE [dbo].[TickersLiveData]
	ADD CONSTRAINT [PK_TickersLiveData]
	PRIMARY KEY ([TickerId])
GO
CREATE TYPE [dbo].[TickersLiveData_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
    [Spot]			FLOAT		NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[TickersLiveData_Merge]
	@Collection TickersLiveData_UDT READONLY
AS

MERGE dbo.[TickersLiveData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] 
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[Spot]	=[Src].[Spot]				
		,[Target].[LastUpdate]	=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[Spot]			
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[Spot]			
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO
ALTER PROCEDURE [dbo].[Tickers_GetAll]
AS

SELECT T.*
    ,TD.[Date]
    ,TD.[OpenPrice]
    ,TD.[ClosePrice]
    ,TD.[High]
    ,TD.[Low]
    ,TD.[Volume]
    ,TD.[MA50]
    ,TD.[MA75]
    ,TD.[MA100]
    ,TD.[MA125]
    ,TD.[MA150]
    ,TD.[MA175]
    ,TD.[MA200]
    ,TD.[LastUpdate]
	,TLD.[Spot]
	,TLD.[LastUpdate] AS [SpotLastUpdate]
    FROM Tickers AS T
    INNER JOIN (
        SELECT TickerId, MAX(Date) AS MaxDate
        FROM TickersData
        GROUP BY TickerId
    ) AS MaxDates ON T.TickerId = MaxDates.TickerId
    INNER JOIN TickersData AS TD 
        ON MaxDates.TickerId = TD.TickerId AND MaxDates.MaxDate = TD.Date
    LEFT JOIN TickersLiveData AS TLD 
        ON T.TickerId = TLD.TickerId;
RETURN 0
RETURN 0
GO
CREATE PROCEDURE [dbo].[TickersLiveData_Delete]
	@TickerId int
AS
	DELETE FROM TickersLiveData where TickerId=@TickerId
RETURN 0
GO