﻿USE BgExchangeTrading
GO

CREATE TYPE [dbo].[TickersCalculatedData_UDT] AS TABLE(
	[TickerId]		INT	NOT NULL,
    [MV]            INT NOT NULL,
    [IsBuy]         BIT NOT NULL,
    [SuccessRate]   FLOAT NOT NULL,
    [TotalPosition] INT NOT NULL,
    [LastUpdate]    DATETIME NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [MV] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE TABLE [dbo].[TickersCalculatedData]
(
	    [TickerId]      INT NOT NULL,
        [MV]            INT NOT NULL,
        [IsBuy]         BIT NOT NULL,
        [SuccessRate]   FLOAT NOT NULL,
        [TotalPosition] INT NOT NULL,
        [LastUpdate]    DATETIME NOT NULL,
)
GO
ALTER TABLE [dbo].[TickersCalculatedData]
	ADD CONSTRAINT [PK_TickersCalculatedData]
	PRIMARY KEY ([TickerId], [MV])
GO
CREATE PROCEDURE [dbo].[TickersCalculatedData_Merge]
	@Collection TickersCalculatedData_UDT READONLY
AS

MERGE dbo.[TickersCalculatedData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] AND
		[Src].[MV] = [Target].[MV]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[IsBuy]			=[Src].[IsBuy]				
		,[Target].[SuccessRate]		=[Src].[SuccessRate]		
		,[Target].[TotalPosition]	=[Src].[TotalPosition]	
		,[Target].[LastUpdate]		=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[MV]			
		,[IsBuy]		
		,[SuccessRate]	
		,[TotalPosition]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[MV]			
			,[Src].[IsBuy]		
			,[Src].[SuccessRate]		
			,[Src].[TotalPosition]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO
-- SAMPLE CALL WITH UDT --
--DECLARE @InputTable dbo.TickerIdExpiries_UDT;
--INSERT INTO @InputTable (TickerId, Expiry)
--VALUES
--    (1004, '2023-11-17'),
--    (1014, '2023-11-17')
--EXEC dbo.[TickersDataPerExpiry_Get] @Collection = @InputTable;

CREATE PROCEDURE [dbo].[TickersCalculatedData_Get]
AS
	SELECT * FROM TickersCalculatedData
RETURN 0
GO
CREATE PROCEDURE [dbo].[TickersCalculatedData_Delete]
	@TickerId int
AS
	DELETE FROM TickersCalculatedData where TickerId=@TickerId
RETURN 0
GO