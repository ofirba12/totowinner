﻿USE BgExchangeTrading
GO

CREATE TABLE [dbo].[Tickers]
(
	[TickerId]				INT	IDENTITY(1000,1)	NOT NULL,
	[Symbol]				NVARCHAR(300)			NOT NULL PRIMARY KEY,
	[FullName]				NVARCHAR(600)			NOT NULL,
	[Type]					NVARCHAR(100)			NOT NULL,
	[LastSpot]				FLOAT					NULL,
	[LastSpotTimeStamp]		DATETIME				NULL,
	[LastMA150]				FLOAT					NULL,
	[StockRating]			FLOAT					NULL,
	[LastUpdateStockRating]	DATETIME				NULL,
	[Cap]					FLOAT					NULL,
	[Floor]					FLOAT					NULL,
	[MA150CapPercent]		FLOAT					NULL,
	[MA150FloorPercent]		FLOAT					NULL,
)
GO
CREATE PROCEDURE [dbo].[Tickers_Merge]
	@Symbol					VARCHAR(300),
	@FullName				VARCHAR(600)			NULL,
	@Type					NVARCHAR(100)			NULL,
	@LastSpot				FLOAT					NULL,
	@LastSpotTimeStamp		DATETIME				NULL,
	@LastMA150				FLOAT					NULL,
	@StockRating			FLOAT					NULL,
	@LastUpdateStockRating	DATETIME				NULL,
	@Cap					FLOAT					NULL,
	@Floor					FLOAT					NULL,
	@MA150CapPercent		FLOAT					NULL,
	@MA150FloorPercent		FLOAT					NULL
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Tickers] AS [Target]
	USING ( SELECT	 @Symbol				AS	"Symbol"
					,@FullName				AS	"FullName"
					,@Type					AS	"Type"
					,@LastSpot				AS	"LastSpot"
					,@LastSpotTimeStamp		AS	"LastSpotTimeStamp"
					,@LastMA150				AS	"LastMA150"
					,@StockRating			AS	"StockRating"
					,@LastUpdateStockRating	AS	"LastUpdateStockRating"
					,@Cap					AS	"Cap"
					,@Floor					AS	"Floor"
					,@MA150CapPercent		AS	"MA150CapPercent"
					,@MA150FloorPercent		AS	"MA150FloorPercent"
	) AS [Source]	ON [Source].[Symbol]	= [Target].[Symbol]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[FullName] = ISNULL([Source].[FullName], [Target].[FullName]),
			[Target].[Type] = ISNULL([Source].[Type], [Target].[Type]),
			[Target].[LastSpot] = ISNULL([Source].[LastSpot], [Target].[LastSpot]),
			[Target].[LastSpotTimeStamp] = ISNULL([Source].[LastSpotTimeStamp], [Target].[LastSpotTimeStamp]),
			[Target].[LastMA150] = ISNULL([Source].[LastMA150], [Target].[LastMA150]),
			[Target].[StockRating] = ISNULL([Source].[StockRating], [Target].[StockRating]),
			[Target].[LastUpdateStockRating] = ISNULL([Source].[LastUpdateStockRating], [Target].[LastUpdateStockRating]),
			[Target].[Cap] = ISNULL([Source].[Cap], [Target].[Cap]),
			[Target].[Floor] = ISNULL([Source].[Floor], [Target].[Floor]),
			[Target].[MA150CapPercent] = ISNULL([Source].[MA150CapPercent], [Target].[MA150CapPercent]),
			[Target].[MA150FloorPercent] = ISNULL([Source].[MA150FloorPercent], [Target].[MA150FloorPercent])
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Symbol]
				,[FullName]
				,[Type]					
				,[LastSpot]
				,[LastSpotTimeStamp]
				,[LastMA150]	
				,[StockRating]
				,[LastUpdateStockRating]
				,[Cap]
				,[Floor]
				,[MA150CapPercent]
				,[MA150FloorPercent]		
				)
		VALUES ([Source].[Symbol]
				,[Source].[FullName]
				,[Source].[Type]					
				,[Source].[LastSpot]
				,[Source].[LastSpotTimeStamp]
				,[Source].[LastMA150]	
				,[Source].[StockRating]
				,[Source].[LastUpdateStockRating]
				,[Source].[Cap]
				,[Source].[Floor]
				,[Source].[MA150CapPercent]
				,[Source].[MA150FloorPercent]
				)
		OUTPUT Inserted.TickerId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Tickers_Delete]
	@TickerId int 
AS
	DELETE FROM Tickers where TickerId=@TickerId
RETURN 0
GO
CREATE TABLE [dbo].[TickersData]
(
	[TickerId]      INT NOT NULL,
	[Date]          DATETIME NOT NULL,
    [OpenPrice]     FLOAT NOT NULL,
    [ClosePrice]    FLOAT NOT NULL,
    [High]          FLOAT NOT NULL,
    [Low]           FLOAT NOT NULL,
    [Volume]        BIGINT NOT NULL,
	[MA150]			FLOAT NULL,
    [LastUpdate]    DATETIME NOT NULL,
)
GO
ALTER TABLE [dbo].[TickersData]
	ADD CONSTRAINT [PK_TickersData]
	PRIMARY KEY ([TickerId], [Date])
GO
CREATE TYPE [dbo].[TickersData_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
	[Date]			DATETIME	NOT NULL,
	[OpenPrice]		FLOAT		NOT NULL,
	[ClosePrice]	FLOAT		NOT NULL,
	[High]			FLOAT		NOT NULL,
	[Low]			FLOAT		NOT NULL,
	[Volume]		BIGINT		NOT NULL,
	[MA150]			FLOAT		NULL,
	[LastUpdate]	DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [Date] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[TickersData_Merge]
	@Collection TickersData_UDT READONLY
AS

MERGE dbo.[TickersData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] AND
		[Src].[Date] = [Target].[Date]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[OpenPrice]	=[Src].[OpenPrice]				
		,[Target].[ClosePrice]	=[Src].[ClosePrice]		
		,[Target].[High]		=[Src].[High]	
		,[Target].[Low]			=[Src].[Low]	
		,[Target].[Volume]		=[Src].[Volume]	
		,[Target].[MA150]		=[Src].[MA150]	
		,[Target].[LastUpdate]	=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[Date]			
		,[OpenPrice]		
		,[ClosePrice]	
		,[High]
		,[Low]
		,[Volume]
		,[MA150]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[Date]			
			,[Src].[OpenPrice]		
			,[Src].[ClosePrice]		
			,[Src].[High]
			,[Src].[Low]
			,[Src].[Volume]
			,[Src].[MA150]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO
CREATE PROCEDURE [dbo].[TickersData_Delete]
	@TickerId int
AS
	DELETE FROM TickersData where TickerId=@TickerId
RETURN 0
GO