﻿USE BgExchangeTrading
GO
CREATE TABLE [dbo].[Tickers]
(
	[TickerId]				INT	IDENTITY(1000,1)	NOT NULL,
	[Symbol]				NVARCHAR(300)			NOT NULL PRIMARY KEY,
	[FullName]				NVARCHAR(600)			NOT NULL,
	[Type]					NVARCHAR(100)			NOT NULL,
	[Group]					NVARCHAR(100)			NOT NULL,
	[StockRating]			FLOAT					NULL,
	[Buffer]				FLOAT					NULL,
	[LastUpdateStockRating]	DATETIME				NULL,
	[Cap]					FLOAT					NULL,
	[Floor]					FLOAT					NULL,
	[MA50CapPercent]		FLOAT					NULL,
	[MA50FloorPercent]		FLOAT					NULL,
	[MA75CapPercent]		FLOAT					NULL,
	[MA75FloorPercent]		FLOAT					NULL,
	[MA100CapPercent]		FLOAT					NULL,
	[MA100FloorPercent]		FLOAT					NULL,
	[MA125CapPercent]		FLOAT					NULL,
	[MA125FloorPercent]		FLOAT					NULL,
	[MA150CapPercent]		FLOAT					NULL,
	[MA150FloorPercent]		FLOAT					NULL,
	[MA175CapPercent]		FLOAT					NULL,
	[MA175FloorPercent]		FLOAT					NULL,
	[MA200CapPercent]		FLOAT					NULL,
	[MA200FloorPercent]		FLOAT					NULL,
)

GO
CREATE PROCEDURE [dbo].[Tickers_Merge]
	@Symbol					VARCHAR(300),
	@FullName				VARCHAR(600)			NULL,
	@Type					NVARCHAR(100)			NULL,
	@Group					NVARCHAR(100)			NULL,
	@StockRating			FLOAT					NULL,
	@Buffer					FLOAT					NULL,
	@LastUpdateStockRating	DATETIME				NULL,
	@Cap					FLOAT					NULL,
	@Floor					FLOAT					NULL,
	@MA50CapPercent			FLOAT					NULL,
	@MA50FloorPercent		FLOAT					NULL,
	@MA75CapPercent			FLOAT					NULL,
	@MA75FloorPercent		FLOAT					NULL,
	@MA100CapPercent		FLOAT					NULL,
	@MA100FloorPercent		FLOAT					NULL,
	@MA125CapPercent		FLOAT					NULL,
	@MA125FloorPercent		FLOAT					NULL,
	@MA150CapPercent		FLOAT					NULL,
	@MA150FloorPercent		FLOAT					NULL,
	@MA175CapPercent		FLOAT					NULL,
	@MA175FloorPercent		FLOAT					NULL,
	@MA200CapPercent		FLOAT					NULL,
	@MA200FloorPercent		FLOAT					NULL
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Tickers] AS [Target]
	USING ( SELECT	 @Symbol				AS	"Symbol"
					,@FullName				AS	"FullName"
					,@Type					AS	"Type"
					,@Group					AS	"Group"
					,@StockRating			AS	"StockRating"
					,@Buffer				AS	"Buffer"
					,@LastUpdateStockRating	AS	"LastUpdateStockRating"
					,@Cap					AS	"Cap"
					,@Floor					AS	"Floor"
					,@MA50CapPercent		AS	"MA50CapPercent"
					,@MA50FloorPercent		AS	"MA50FloorPercent"
					,@MA75CapPercent		AS	"MA75CapPercent"
					,@MA75FloorPercent		AS	"MA75FloorPercent"
					,@MA100CapPercent		AS	"MA100CapPercent"
					,@MA100FloorPercent		AS	"MA100FloorPercent"
					,@MA125CapPercent		AS	"MA125CapPercent"
					,@MA125FloorPercent		AS	"MA125FloorPercent"
					,@MA150CapPercent		AS	"MA150CapPercent"
					,@MA150FloorPercent		AS	"MA150FloorPercent"
					,@MA175CapPercent		AS	"MA175CapPercent"
					,@MA175FloorPercent		AS	"MA175FloorPercent"
					,@MA200CapPercent		AS	"MA200CapPercent"
					,@MA200FloorPercent		AS	"MA200FloorPercent"
	) AS [Source]	ON [Source].[Symbol]	= [Target].[Symbol]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[FullName] = ISNULL([Source].[FullName], [Target].[FullName]),
			[Target].[Type] = [Source].[Type],
			[Target].[Group] = [Source].[Group],
			[Target].[StockRating] = [Source].[StockRating],
			[Target].[Buffer] = [Source].[Buffer],
			[Target].[LastUpdateStockRating] = [Source].[LastUpdateStockRating],
			[Target].[Cap] = [Source].[Cap],
			[Target].[Floor] = [Source].[Floor], 
			[Target].[MA50CapPercent] = [Source].[MA50CapPercent], 
			[Target].[MA50FloorPercent] = [Source].[MA50FloorPercent],
			[Target].[MA75CapPercent] = [Source].[MA75CapPercent], 
			[Target].[MA75FloorPercent] = [Source].[MA75FloorPercent],
			[Target].[MA100CapPercent] = [Source].[MA100CapPercent], 
			[Target].[MA100FloorPercent] = [Source].[MA100FloorPercent],
			[Target].[MA125CapPercent] = [Source].[MA125CapPercent], 
			[Target].[MA125FloorPercent] = [Source].[MA125FloorPercent],
			[Target].[MA150CapPercent] = [Source].[MA150CapPercent], 
			[Target].[MA150FloorPercent] = [Source].[MA150FloorPercent],
			[Target].[MA175CapPercent] = [Source].[MA175CapPercent], 
			[Target].[MA175FloorPercent] = [Source].[MA175FloorPercent],
			[Target].[MA200CapPercent] = [Source].[MA200CapPercent], 
			[Target].[MA200FloorPercent] = [Source].[MA200FloorPercent]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Symbol]
				,[FullName]
				,[Type]					
				,[Group]					
				,[StockRating]
				,[Buffer]
				,[LastUpdateStockRating]
				,[Cap]
				,[Floor]
				,[MA50CapPercent]
				,[MA50FloorPercent]		
				,[MA75CapPercent]
				,[MA75FloorPercent]		
				,[MA100CapPercent]
				,[MA100FloorPercent]		
				,[MA125CapPercent]
				,[MA125FloorPercent]		
				,[MA150CapPercent]
				,[MA150FloorPercent]		
				,[MA175CapPercent]
				,[MA175FloorPercent]		
				,[MA200CapPercent]
				,[MA200FloorPercent]		
				)
		VALUES ([Source].[Symbol]
				,[Source].[FullName]
				,[Source].[Type]					
				,[Source].[Group]					
				,[Source].[StockRating]
				,[Source].[Buffer]
				,[Source].[LastUpdateStockRating]
				,[Source].[Cap]
				,[Source].[Floor]
				,[Source].[MA50CapPercent]
				,[Source].[MA50FloorPercent]
				,[Source].[MA75CapPercent]
				,[Source].[MA75FloorPercent]
				,[Source].[MA100CapPercent]
				,[Source].[MA100FloorPercent]
				,[Source].[MA125CapPercent]
				,[Source].[MA125FloorPercent]
				,[Source].[MA150CapPercent]
				,[Source].[MA150FloorPercent]
				,[Source].[MA175CapPercent]
				,[Source].[MA175FloorPercent]
				,[Source].[MA200CapPercent]
				,[Source].[MA200FloorPercent]
				)
		OUTPUT Inserted.TickerId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Tickers_GetAll]
AS

SELECT T.*
    ,TD.[Date]
    ,TD.[OpenPrice]
    ,TD.[ClosePrice]
    ,TD.[High]
    ,TD.[Low]
    ,TD.[Volume]
    ,TD.[MA50]
    ,TD.[MA75]
    ,TD.[MA100]
    ,TD.[MA125]
    ,TD.[MA150]
    ,TD.[MA175]
    ,TD.[MA200]
    ,TD.[LastUpdate]
    FROM Tickers AS T
    INNER JOIN (
        SELECT TickerId, MAX(Date) AS MaxDate
        FROM TickersData
        GROUP BY TickerId
    ) AS MaxDates ON T.TickerId = MaxDates.TickerId
INNER JOIN TickersData AS TD 
    ON MaxDates.TickerId = TD.TickerId AND MaxDates.MaxDate = TD.Date;
RETURN 0
GO
CREATE PROCEDURE [dbo].[Tickers_Delete]
	@TickerId int 
AS
	DELETE FROM Tickers where TickerId=@TickerId
RETURN 0
GO
CREATE TABLE [dbo].[TickersData]
(
	[TickerId]      INT NOT NULL,
	[Date]          DATETIME NOT NULL,
    [OpenPrice]     FLOAT NOT NULL,
    [ClosePrice]    FLOAT NOT NULL,
    [High]          FLOAT NOT NULL,
    [Low]           FLOAT NOT NULL,
    [Volume]        BIGINT NOT NULL,
    [MA50]			FLOAT NULL,
    [MA75]			FLOAT NULL,
    [MA100]			FLOAT NULL,
    [MA125]			FLOAT NULL,
    [MA150]			FLOAT NULL,
    [MA175]			FLOAT NULL,
    [MA200]			FLOAT NULL,
    [LastUpdate]    DATETIME NOT NULL,
)
GO
ALTER TABLE [dbo].[TickersData]
	ADD CONSTRAINT [PK_TickersData]
	PRIMARY KEY ([TickerId], [Date])
GO
CREATE TYPE [dbo].[TickersData_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
	[Date]			DATETIME	NOT NULL,
	[OpenPrice]		FLOAT		NOT NULL,
	[ClosePrice]	FLOAT		NOT NULL,
	[High]			FLOAT		NOT NULL,
	[Low]			FLOAT		NOT NULL,
	[Volume]		BIGINT		NOT NULL,
    [MA50]			FLOAT		NULL,
    [MA75]			FLOAT		NULL,
    [MA100]			FLOAT		NULL,
    [MA125]			FLOAT		NULL,
    [MA150]			FLOAT		NULL,
    [MA175]			FLOAT		NULL,
    [MA200]			FLOAT		NULL,
	[LastUpdate]	DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [Date] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[TickersData_Merge]
	@Collection TickersData_UDT READONLY
AS

MERGE dbo.[TickersData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] AND
		[Src].[Date] = [Target].[Date]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[OpenPrice]	=[Src].[OpenPrice]				
		,[Target].[ClosePrice]	=[Src].[ClosePrice]		
		,[Target].[High]		=[Src].[High]	
		,[Target].[Low]			=[Src].[Low]	
		,[Target].[Volume]		=[Src].[Volume]	
		,[Target].[MA50]		=[Src].[MA50]
		,[Target].[MA75]		=[Src].[MA75]
		,[Target].[MA100]		=[Src].[MA100]
		,[Target].[MA125]		=[Src].[MA125]
		,[Target].[MA150]		=[Src].[MA150]
		,[Target].[MA175]		=[Src].[MA175]
		,[Target].[MA200]		=[Src].[MA200]
		,[Target].[LastUpdate]	=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[Date]			
		,[OpenPrice]		
		,[ClosePrice]	
		,[High]
		,[Low]
		,[Volume]
		,[MA50]
		,[MA75]
		,[MA100]
		,[MA125]
		,[MA150]
		,[MA175]
		,[MA200]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[Date]			
			,[Src].[OpenPrice]		
			,[Src].[ClosePrice]		
			,[Src].[High]
			,[Src].[Low]
			,[Src].[Volume]
			,[Src].[MA50]
			,[Src].[MA75]
			,[Src].[MA100]
			,[Src].[MA125]
			,[Src].[MA150]
			,[Src].[MA175]
			,[Src].[MA200]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO
CREATE PROCEDURE [dbo].[TickersData_Delete]
	@TickerId int
AS
	DELETE FROM TickersData where TickerId=@TickerId
RETURN 0
