﻿USE BgExchangeTrading
GO

CREATE TYPE [dbo].[TickerIdExpiries_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
	[Expiry]		DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [Expiry] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
-- SAMPLE CALL --
--DECLARE @InputTable dbo.TickerIdExpiries_UDT;
--INSERT INTO @InputTable (TickerId, Expiry)
--VALUES
--    (1004, '2023-11-17'),
--    (1014, '2023-11-17')
--EXEC dbo.[TickersDataPerExpiry_Get] @Collection = @InputTable;
CREATE PROCEDURE [dbo].[TickersDataPerExpiry_Get]
	@Collection TickerIdExpiries_UDT READONLY
AS

SELECT T.[TickerId], T.[Date] AS [Expiry], T.[ClosePrice] FROM [TickersData] T
JOIN @Collection C ON C.[TickerId] = T.[TickerId] AND C.[Expiry] = T.[Date]

RETURN 0
GO
ALTER PROCEDURE [dbo].[TickersData_Delete]
	@TickerId int
AS
	DELETE FROM TickersData where TickerId=@TickerId
	DELETE FROM PortfolioAssets WHERE TickerId=@TickerId
RETURN 0