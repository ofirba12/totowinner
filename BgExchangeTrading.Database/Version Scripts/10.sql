﻿USE BgExchangeTrading
GO

CREATE PROCEDURE [dbo].[PortfolioAssets_GetActive]
AS
	SELECT * FROM [dbo].PortfolioAssets WHERE TickerExpiry >= GetDate()
RETURN 0

GO
ALTER TABLE Tickers
ADD [Url]					NVARCHAR(MAX)			NULL;
GO
ALTER PROCEDURE [dbo].[Tickers_Merge]
	@Symbol					VARCHAR(300),
	@FullName				VARCHAR(600)			NULL,
	@Type					NVARCHAR(100)			NULL,
	@Group					NVARCHAR(100)			NULL,
	@StockRating			FLOAT					NULL,
	@Buffer					FLOAT					NULL,
	@LastUpdateStockRating	DATETIME				NULL,
	@Cap					FLOAT					NULL,
	@Floor					FLOAT					NULL,
	@MA50CapPercent			FLOAT					NULL,
	@MA50FloorPercent		FLOAT					NULL,
	@MA75CapPercent			FLOAT					NULL,
	@MA75FloorPercent		FLOAT					NULL,
	@MA100CapPercent		FLOAT					NULL,
	@MA100FloorPercent		FLOAT					NULL,
	@MA125CapPercent		FLOAT					NULL,
	@MA125FloorPercent		FLOAT					NULL,
	@MA150CapPercent		FLOAT					NULL,
	@MA150FloorPercent		FLOAT					NULL,
	@MA175CapPercent		FLOAT					NULL,
	@MA175FloorPercent		FLOAT					NULL,
	@MA200CapPercent		FLOAT					NULL,
	@MA200FloorPercent		FLOAT					NULL,
	@Url					NVARCHAR(MAX)			NULL
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Tickers] AS [Target]
	USING ( SELECT	 @Symbol				AS	"Symbol"
					,@FullName				AS	"FullName"
					,@Type					AS	"Type"
					,@Group					AS	"Group"
					,@StockRating			AS	"StockRating"
					,@Buffer				AS	"Buffer"
					,@LastUpdateStockRating	AS	"LastUpdateStockRating"
					,@Cap					AS	"Cap"
					,@Floor					AS	"Floor"
					,@MA50CapPercent		AS	"MA50CapPercent"
					,@MA50FloorPercent		AS	"MA50FloorPercent"
					,@MA75CapPercent		AS	"MA75CapPercent"
					,@MA75FloorPercent		AS	"MA75FloorPercent"
					,@MA100CapPercent		AS	"MA100CapPercent"
					,@MA100FloorPercent		AS	"MA100FloorPercent"
					,@MA125CapPercent		AS	"MA125CapPercent"
					,@MA125FloorPercent		AS	"MA125FloorPercent"
					,@MA150CapPercent		AS	"MA150CapPercent"
					,@MA150FloorPercent		AS	"MA150FloorPercent"
					,@MA175CapPercent		AS	"MA175CapPercent"
					,@MA175FloorPercent		AS	"MA175FloorPercent"
					,@MA200CapPercent		AS	"MA200CapPercent"
					,@MA200FloorPercent		AS	"MA200FloorPercent"
					,@Url					AS	"Url"
	) AS [Source]	ON [Source].[Symbol]	= [Target].[Symbol]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[FullName] = ISNULL([Source].[FullName], [Target].[FullName]),
			[Target].[Type] = [Source].[Type],
			[Target].[Group] = [Source].[Group],
			[Target].[StockRating] = [Source].[StockRating],
			[Target].[Buffer] = [Source].[Buffer],
			[Target].[LastUpdateStockRating] = [Source].[LastUpdateStockRating],
			[Target].[Cap] = [Source].[Cap],
			[Target].[Floor] = [Source].[Floor], 
			[Target].[MA50CapPercent] = [Source].[MA50CapPercent], 
			[Target].[MA50FloorPercent] = [Source].[MA50FloorPercent],
			[Target].[MA75CapPercent] = [Source].[MA75CapPercent], 
			[Target].[MA75FloorPercent] = [Source].[MA75FloorPercent],
			[Target].[MA100CapPercent] = [Source].[MA100CapPercent], 
			[Target].[MA100FloorPercent] = [Source].[MA100FloorPercent],
			[Target].[MA125CapPercent] = [Source].[MA125CapPercent], 
			[Target].[MA125FloorPercent] = [Source].[MA125FloorPercent],
			[Target].[MA150CapPercent] = [Source].[MA150CapPercent], 
			[Target].[MA150FloorPercent] = [Source].[MA150FloorPercent],
			[Target].[MA175CapPercent] = [Source].[MA175CapPercent], 
			[Target].[MA175FloorPercent] = [Source].[MA175FloorPercent],
			[Target].[MA200CapPercent] = [Source].[MA200CapPercent], 
			[Target].[MA200FloorPercent] = [Source].[MA200FloorPercent],
			[Target].[Url] = [Source].[Url]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Symbol]
				,[FullName]
				,[Type]					
				,[Group]					
				,[StockRating]
				,[Buffer]
				,[LastUpdateStockRating]
				,[Cap]
				,[Floor]
				,[MA50CapPercent]
				,[MA50FloorPercent]		
				,[MA75CapPercent]
				,[MA75FloorPercent]		
				,[MA100CapPercent]
				,[MA100FloorPercent]		
				,[MA125CapPercent]
				,[MA125FloorPercent]		
				,[MA150CapPercent]
				,[MA150FloorPercent]		
				,[MA175CapPercent]
				,[MA175FloorPercent]		
				,[MA200CapPercent]
				,[MA200FloorPercent]		
				,[Url]		
				)
		VALUES ([Source].[Symbol]
				,[Source].[FullName]
				,[Source].[Type]					
				,[Source].[Group]					
				,[Source].[StockRating]
				,[Source].[Buffer]
				,[Source].[LastUpdateStockRating]
				,[Source].[Cap]
				,[Source].[Floor]
				,[Source].[MA50CapPercent]
				,[Source].[MA50FloorPercent]
				,[Source].[MA75CapPercent]
				,[Source].[MA75FloorPercent]
				,[Source].[MA100CapPercent]
				,[Source].[MA100FloorPercent]
				,[Source].[MA125CapPercent]
				,[Source].[MA125FloorPercent]
				,[Source].[MA150CapPercent]
				,[Source].[MA150FloorPercent]
				,[Source].[MA175CapPercent]
				,[Source].[MA175FloorPercent]
				,[Source].[MA200CapPercent]
				,[Source].[MA200FloorPercent]
				,[Source].[Url]
				)
		OUTPUT Inserted.TickerId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END


