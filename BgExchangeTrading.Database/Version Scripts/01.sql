﻿USE BgExchangeTrading
GO

CREATE TABLE [dbo].[Tickers]
(
	[TickerId]		INT	IDENTITY(1000,1)	NOT NULL,
	[Symbol]		NVARCHAR(300)			NOT NULL PRIMARY KEY,
	[FullName]		NVARCHAR(600)			NOT NULL,
)
GO
CREATE PROCEDURE [dbo].[Tickers_Merge]
	@Symbol	VARCHAR(300),
	@FullName	VARCHAR(600)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Tickers] AS [Target]
	USING ( SELECT	 @Symbol		AS	"Symbol"
					,@FullName		AS	"FullName"
	) AS [Source]	ON [Source].[Symbol]	= [Target].[Symbol]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[FullName]		= [Source].[FullName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Symbol]
				,[FullName])
		VALUES ([Source].[Symbol]
				,[Source].[FullName]
				)
		OUTPUT Inserted.TickerId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE TABLE [dbo].[TickersData]
(
	[TickerId]      INT NOT NULL,
	[Date]          DATETIME NOT NULL,
    [OpenPrice]     FLOAT NOT NULL,
    [ClosePrice]    FLOAT NOT NULL,
    [High]          FLOAT NOT NULL,
    [Low]           FLOAT NOT NULL,
    [Volume]        BIGINT NOT NULL,
    [LastUpdate]    DATETIME NOT NULL,
)
GO
ALTER TABLE [dbo].[TickersData]
	ADD CONSTRAINT [PK_TickersData]
	PRIMARY KEY ([TickerId], [Date])
GO
CREATE TYPE [dbo].[TickersData_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
	[Date]			DATETIME	NOT NULL,
	[OpenPrice]		FLOAT		NOT NULL,
	[ClosePrice]	FLOAT		NOT NULL,
	[High]			FLOAT		NOT NULL,
	[Low]			FLOAT		NOT NULL,
	[Volume]		BIGINT		NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [Date] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[TickersData_Merge]
	@Collection TickersData_UDT READONLY
AS

MERGE dbo.[TickersData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] AND
		[Src].[Date] = [Target].[Date]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[OpenPrice]	=[Src].[OpenPrice]				
		,[Target].[ClosePrice]	=[Src].[ClosePrice]		
		,[Target].[High]		=[Src].[High]	
		,[Target].[Low]			=[Src].[Low]	
		,[Target].[Volume]		=[Src].[Volume]	
		,[Target].[LastUpdate]	=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[Date]			
		,[OpenPrice]		
		,[ClosePrice]	
		,[High]
		,[Low]
		,[Volume]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[Date]			
			,[Src].[OpenPrice]		
			,[Src].[ClosePrice]		
			,[Src].[High]
			,[Src].[Low]
			,[Src].[Volume]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO