﻿USE BgExchangeTrading
GO
DROP PROCEDURE [Tickers_Merge]
GO
ALTER TABLE [Tickers] DROP COLUMN LastSpot
ALTER TABLE [Tickers] DROP COLUMN LastSpotTimeStamp
ALTER TABLE [Tickers] DROP COLUMN LastMA150

GO
CREATE PROCEDURE [dbo].[Tickers_Merge]
	@Symbol					VARCHAR(300),
	@FullName				VARCHAR(600)			NULL,
	@Type					NVARCHAR(100)			NULL,
	@StockRating			FLOAT					NULL,
	@LastUpdateStockRating	DATETIME				NULL,
	@Cap					FLOAT					NULL,
	@Floor					FLOAT					NULL,
	@MA150CapPercent		FLOAT					NULL,
	@MA150FloorPercent		FLOAT					NULL
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Tickers] AS [Target]
	USING ( SELECT	 @Symbol				AS	"Symbol"
					,@FullName				AS	"FullName"
					,@Type					AS	"Type"
					,@StockRating			AS	"StockRating"
					,@LastUpdateStockRating	AS	"LastUpdateStockRating"
					,@Cap					AS	"Cap"
					,@Floor					AS	"Floor"
					,@MA150CapPercent		AS	"MA150CapPercent"
					,@MA150FloorPercent		AS	"MA150FloorPercent"
	) AS [Source]	ON [Source].[Symbol]	= [Target].[Symbol]
	WHEN MATCHED THEN 
		UPDATE SET 
			[Target].[FullName] = ISNULL([Source].[FullName], [Target].[FullName]),
			[Target].[Type] = [Source].[Type],
			[Target].[StockRating] = [Source].[StockRating],
			[Target].[LastUpdateStockRating] = [Source].[LastUpdateStockRating],
			[Target].[Cap] = [Source].[Cap],
			[Target].[Floor] = [Source].[Floor], 
			[Target].[MA150CapPercent] = [Source].[MA150CapPercent], 
			[Target].[MA150FloorPercent] = [Source].[MA150FloorPercent]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Symbol]
				,[FullName]
				,[Type]					
				,[StockRating]
				,[LastUpdateStockRating]
				,[Cap]
				,[Floor]
				,[MA150CapPercent]
				,[MA150FloorPercent]		
				)
		VALUES ([Source].[Symbol]
				,[Source].[FullName]
				,[Source].[Type]					
				,[Source].[StockRating]
				,[Source].[LastUpdateStockRating]
				,[Source].[Cap]
				,[Source].[Floor]
				,[Source].[MA150CapPercent]
				,[Source].[MA150FloorPercent]
				)
		OUTPUT Inserted.TickerId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Tickers_GetAll]
AS
	SELECT T.*, TD.[Date]
      ,TD.[OpenPrice]
      ,TD.[ClosePrice]
      ,TD.[High]
      ,TD.[Low]
      ,TD.[Volume]
      ,TD.[MA150]
      ,TD.[LastUpdate]
        FROM Tickers AS T
        INNER JOIN (
            SELECT TickerId, MAX(Date) AS MaxDate
            FROM TickersData
            GROUP BY TickerId
        ) AS MaxDates ON T.TickerId = MaxDates.TickerId
    INNER JOIN TickersData AS TD 
        ON MaxDates.TickerId = TD.TickerId AND MaxDates.MaxDate = TD.Date;
RETURN 0
GO

