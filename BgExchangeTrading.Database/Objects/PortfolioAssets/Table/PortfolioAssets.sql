﻿CREATE TABLE [dbo].[PortfolioAssets]
(
	[AssetId]			INT	IDENTITY(20000,1)	NOT NULL PRIMARY KEY,
	[PortfolioId]		INT						NOT NULL,
	[TickerId]			INT						NOT NULL,
	[TickerExpiry]		DATETIME				NOT NULL,
	[IsCall]			BIT						NULL,
	[Strike]			FLOAT					NULL,
	[IsLong]			BIT						NULL,
	[Premium]			FLOAT					NULL,
	[Size]				INT						NULL,
	[Fee]				FLOAT					NULL
)
