﻿CREATE PROCEDURE [dbo].[PortfolioAssets_Merge]
	@AssetId		INT = NULL,
	@PortfolioId	INT,	
	@TickerId		INT,		
	@TickerExpiry	DATETIME,
	@IsCall			BIT,		
	@Strike			FLOAT,	
	@IsLong			BIT,		
	@Premium		FLOAT,	
	@Size			INT,		
	@Fee			FLOAT	
AS

	MERGE dbo.[PortfolioAssets] AS [Target]
	USING ( SELECT	 @AssetId		AS	"AssetId"
					,@PortfolioId	AS	"PortfolioId"
					,@TickerId		AS	"TickerId"
					,@TickerExpiry	AS	"TickerExpiry"
					,@IsCall		AS	"IsCall"
					,@Strike		AS	"Strike"
					,@IsLong		AS	"IsLong"
					,@Premium		AS	"Premium"
					,@Size			AS	"Size"
					,@Fee			AS	"Fee"
	) AS [Source]	ON [Source].[AssetId]	= [Target].[AssetId]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[PortfolioId]		=[Source].[PortfolioId]				
		,[Target].[TickerId]		=[Source].[TickerId]		
		,[Target].[TickerExpiry]	=[Source].[TickerExpiry]	
		,[Target].[IsCall]			=[Source].[IsCall]	
		,[Target].[Strike]			=[Source].[Strike]	
		,[Target].[IsLong]			=[Source].[IsLong]
		,[Target].[Premium]			=[Source].[Premium]
		,[Target].[Size]			=[Source].[Size]
		,[Target].[Fee]				=[Source].[Fee]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[PortfolioId]	
		,[TickerId]		
		,[TickerExpiry]	
		,[IsCall]		
		,[Strike]		
		,[IsLong]		
		,[Premium]		
		,[Size]		
		,[Fee]			
				)
		VALUES (
			[Source].[PortfolioId]			
			,[Source].[TickerId]		
			,[Source].[TickerExpiry]	
			,[Source].[IsCall]		
			,[Source].[Strike]	
			,[Source].[IsLong]
			,[Source].[Premium]
			,[Source].[Size]
			,[Source].[Fee]
				)
				OUTPUT Inserted.AssetId
				;

RETURN 0

GO