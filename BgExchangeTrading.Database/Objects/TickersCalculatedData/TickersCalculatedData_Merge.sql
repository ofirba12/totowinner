﻿CREATE PROCEDURE [dbo].[TickersCalculatedData_Merge]
	@Collection TickersCalculatedData_UDT READONLY
AS

MERGE dbo.[TickersCalculatedData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] AND
		[Src].[MV] = [Target].[MV]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[IsBuy]			=[Src].[IsBuy]				
		,[Target].[SuccessRate]		=[Src].[SuccessRate]		
		,[Target].[TotalPosition]	=[Src].[TotalPosition]	
		,[Target].[LastUpdate]		=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[MV]			
		,[IsBuy]		
		,[SuccessRate]	
		,[TotalPosition]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[MV]			
			,[Src].[IsBuy]		
			,[Src].[SuccessRate]		
			,[Src].[TotalPosition]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO