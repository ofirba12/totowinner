﻿CREATE TABLE [dbo].[TickersCalculatedData]
(
	    [TickerId]      INT NOT NULL,
        [MV]            INT NOT NULL,
        [IsBuy]         BIT NOT NULL,
        [SuccessRate]   FLOAT NOT NULL,
        [TotalPosition] INT NOT NULL,
        [LastUpdate]    DATETIME NOT NULL,
)
