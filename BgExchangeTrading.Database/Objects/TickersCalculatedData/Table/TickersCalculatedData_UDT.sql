﻿CREATE TYPE [dbo].[TickersCalculatedData_UDT] AS TABLE(
	[TickerId]		INT	NOT NULL,
    [MV]            INT NOT NULL,
    [IsBuy]         BIT NOT NULL,
    [SuccessRate]   FLOAT NOT NULL,
    [TotalPosition] INT NOT NULL,
    [LastUpdate]    DATETIME NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [MV] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
