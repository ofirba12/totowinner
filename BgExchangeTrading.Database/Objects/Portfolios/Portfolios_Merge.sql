﻿CREATE PROCEDURE [dbo].[Portfolios_Merge]
	@PortfolioId	INT,
	@Expiry			DATETIME			
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Portfolios] AS [Target]
	USING ( SELECT	 @PortfolioId			AS	"PortfolioId"
					,@Expiry				AS	"Expiry"
	) AS [Source]	ON [Source].[PortfolioId]	= [Target].[PortfolioId]
					AND [Source].[Expiry]		= [Target].[Expiry]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Expiry])
		VALUES ([Source].[Expiry])
		OUTPUT Inserted.PortfolioId
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END

