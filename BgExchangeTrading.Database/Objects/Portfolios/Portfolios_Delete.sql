﻿CREATE PROCEDURE [dbo].[Portfolios_Delete]
	@PortfolioId INT 
AS
	DELETE FROM PortfolioAssets WHERE [PortfolioId]=@PortfolioId
	DELETE FROM Portfolios WHERE [PortfolioId]=@PortfolioId
RETURN 0
