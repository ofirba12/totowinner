﻿CREATE PROCEDURE [dbo].[Tickers_GetAll]
AS

SELECT T.*
    ,TD.[Date]
    ,TD.[OpenPrice]
    ,TD.[ClosePrice]
    ,TD.[High]
    ,TD.[Low]
    ,TD.[Volume]
    ,TD.[MA50]
    ,TD.[MA75]
    ,TD.[MA100]
    ,TD.[MA125]
    ,TD.[MA150]
    ,TD.[MA175]
    ,TD.[MA200]
    ,TD.[LastUpdate]
	,TLD.[Spot]
	,TLD.[LastUpdate] AS [SpotLastUpdate]
    FROM Tickers AS T
    INNER JOIN (
        SELECT TickerId, MAX(Date) AS MaxDate
        FROM TickersData
        GROUP BY TickerId
    ) AS MaxDates ON T.TickerId = MaxDates.TickerId
    INNER JOIN TickersData AS TD 
        ON MaxDates.TickerId = TD.TickerId AND MaxDates.MaxDate = TD.Date
    LEFT JOIN TickersLiveData AS TLD 
        ON T.TickerId = TLD.TickerId;
RETURN 0
