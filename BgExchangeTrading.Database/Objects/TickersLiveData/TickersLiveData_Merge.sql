﻿CREATE PROCEDURE [dbo].[TickersLiveData_Merge]
	@Collection TickersLiveData_UDT READONLY
AS

MERGE dbo.[TickersLiveData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] 
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[Spot]	=[Src].[Spot]				
		,[Target].[LastUpdate]	=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[Spot]			
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[Spot]			
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO