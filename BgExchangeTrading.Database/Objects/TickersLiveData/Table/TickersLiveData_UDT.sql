﻿CREATE TYPE [dbo].[TickersLiveData_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
    [Spot]			FLOAT		NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
