﻿CREATE TABLE [dbo].[TickersLiveData]
(
	[TickerId]		INT			NOT NULL,
	[Spot]			FLOAT		NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL
)
