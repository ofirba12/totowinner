﻿    CREATE TABLE [dbo].[TickersData]
    (
	    [TickerId]      INT NOT NULL,
	    [Date]          DATETIME NOT NULL,
        [OpenPrice]     FLOAT NOT NULL,
        [ClosePrice]    FLOAT NOT NULL,
        [High]          FLOAT NOT NULL,
        [Low]           FLOAT NOT NULL,
        [Volume]        BIGINT NOT NULL,
        [MA50]			FLOAT NULL,
        [MA75]			FLOAT NULL,
        [MA100]			FLOAT NULL,
        [MA125]			FLOAT NULL,
        [MA150]			FLOAT NULL,
        [MA175]			FLOAT NULL,
        [MA200]			FLOAT NULL,
        [LastUpdate]    DATETIME NOT NULL,
    )
