﻿CREATE TYPE [dbo].[TickerIdExpiries_UDT] AS TABLE(
	[TickerId]		INT			NOT NULL,
	[Expiry]		DATETIME	NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC, [Expiry] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
