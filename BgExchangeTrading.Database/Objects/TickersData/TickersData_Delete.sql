﻿CREATE PROCEDURE [dbo].[TickersData_Delete]
	@TickerId int
AS
	DELETE FROM TickersData where TickerId=@TickerId
	DELETE FROM PortfolioAssets WHERE TickerId=@TickerId
RETURN 0
