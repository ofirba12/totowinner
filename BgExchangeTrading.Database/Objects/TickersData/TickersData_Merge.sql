﻿CREATE PROCEDURE [dbo].[TickersData_Merge]
	@Collection TickersData_UDT READONLY
AS

MERGE dbo.[TickersData] AS [Target]
	USING @Collection AS [Src]	
		ON 
		[Src].[TickerId] = [Target].[TickerId] AND
		[Src].[Date] = [Target].[Date]
	WHEN MATCHED THEN 
		UPDATE SET
		 [Target].[OpenPrice]	=[Src].[OpenPrice]				
		,[Target].[ClosePrice]	=[Src].[ClosePrice]		
		,[Target].[High]		=[Src].[High]	
		,[Target].[Low]			=[Src].[Low]	
		,[Target].[Volume]		=[Src].[Volume]	
		,[Target].[MA50]		=[Src].[MA50]
		,[Target].[MA75]		=[Src].[MA75]
		,[Target].[MA100]		=[Src].[MA100]
		,[Target].[MA125]		=[Src].[MA125]
		,[Target].[MA150]		=[Src].[MA150]
		,[Target].[MA175]		=[Src].[MA175]
		,[Target].[MA200]		=[Src].[MA200]
		,[Target].[LastUpdate]	=[Src].[LastUpdate]			
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (
		[TickerId]
		,[Date]			
		,[OpenPrice]		
		,[ClosePrice]	
		,[High]
		,[Low]
		,[Volume]
		,[MA50]
		,[MA75]
		,[MA100]
		,[MA125]
		,[MA150]
		,[MA175]
		,[MA200]
		,[LastUpdate]			
				)
		VALUES (
			[Src].[TickerId]			
			,[Src].[Date]			
			,[Src].[OpenPrice]		
			,[Src].[ClosePrice]		
			,[Src].[High]
			,[Src].[Low]
			,[Src].[Volume]
			,[Src].[MA50]
			,[Src].[MA75]
			,[Src].[MA100]
			,[Src].[MA125]
			,[Src].[MA150]
			,[Src].[MA175]
			,[Src].[MA200]
			,[Src].[LastUpdate]
				)
				;

RETURN 0

GO