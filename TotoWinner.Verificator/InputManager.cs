﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using TotoWinner.Common;

namespace TotoWinner.Verificator
{
    internal class InputManager
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string SmartFile { get; private set; }
        public string WinnerProgramFile { get; private set; }
        public string WinnerProgramDescription { get; private set; }
        public int NumberOfGamesToValidate { get; private set; }

        private string inputDirectory;

        internal InputManager()
        {
            var rootFolder = ConfigurationManager.AppSettings["RootFolder"];
            inputDirectory = Path.Combine(rootFolder, "Input");
            if (!Directory.Exists(inputDirectory))
            {
                Directory.CreateDirectory(inputDirectory);
                Console.WriteLine($"Directory {inputDirectory} has been created");
                _log.Info($"Directory {inputDirectory} has been created");
            }
        }

        internal void ValidateInputFiles()
        {

            var fileEntries = Directory.GetFiles(inputDirectory).ToList();
            if (fileEntries.Count() > 3)
                throw new Exception($"Please make sure there are only 2 files (smart zip file + WinningProgram.txt file) in folder {inputDirectory}");
            foreach (string fileName in fileEntries)
            {
                var extension = Path.GetExtension(fileName).ToLower();
                switch (extension)
                {
                    case ".zip":
                        Utilities.UnCompressZipFile(fileName, inputDirectory);
                        this.SmartFile = Path.Combine(inputDirectory,$"{Path.GetFileNameWithoutExtension(fileName)}.STR");
                        break;
                    case ".txt":
                        this.WinnerProgramFile = fileName;
                        break;
                    case ".str":
                        File.Delete(fileName);
                        break;
                    default:
                        throw new Exception($"Unsupported file extension {extension} was found in {inputDirectory}");
                }
            }
        }
        public Dictionary<int, string> ParseWinningProgramFile()
        {
            var games = new Dictionary<int, string>();
            var validGameResult = "1X2x0";
            var programDesc = new StringBuilder();
            using (System.IO.StreamReader file = new System.IO.StreamReader(this.WinnerProgramFile))
            {
                var line = file.ReadLine();
                if (line.Count() > 16 || line.Count() < 14)
                    throw new Exception($"The specified program result has {line.Count()} games. must be min(14) max(16) games.[{line}]");
                var index = 0;
                foreach (var gameRes in line)
                {
                    if (!validGameResult.Contains(gameRes.ToString()))
                        throw new Exception($"The specified game result #{index+1} is Illegal [{gameRes}], game result must be in 0/1/Xx/2");
                    games.Add(++index, gameRes.ToString());
                    var gameDescription = gameRes.ToString().ToUpper();
                    if (gameRes == '0')
                        gameDescription = "Skipping";
                    else
                        this.NumberOfGamesToValidate++;

                    programDesc.AppendLine($"{index.ToString().PadLeft(2)}. {gameDescription}");
                }
            }
            this.WinnerProgramDescription = programDesc.ToString();
            return games;
        }
    }
}
