﻿using log4net.Appender;
using System.IO;

namespace TotoWinner.Verificator
{
    class TWVRollingFileAppender : RollingFileAppender
    {
        public override string File
        {
            set
            {
                base.File = Path.Combine(Directory.GetCurrentDirectory(), value);
            }
        }
    }
}
