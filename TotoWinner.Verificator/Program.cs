﻿using log4net;
using System;
using System.Text;

namespace TotoWinner.Verificator
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                var inputManager = new InputManager();
                inputManager.ValidateInputFiles();
                var winningProgram = inputManager.ParseWinningProgramFile();
                switch(winningProgram.Count)
                {
                    case 16:
                        Console.WriteLine($"Start Analyzing Winner 16...");
                        Console.WriteLine($"Games to Verify {inputManager.NumberOfGamesToValidate}");
                        Console.WriteLine(inputManager.WinnerProgramDescription);
                        break;
                    case 15:
                        Console.WriteLine($"Start Analyzing World Winner...");
                        break;
                    case 14:
                        Console.WriteLine($"Start Analyzing Half Winner...");
                        break;
                    default:
                        throw new Exception($"An illegal number of games [{winningProgram.Count}] was found in Winning Program file");
                }
                Console.ForegroundColor = ConsoleColor.Yellow;
                var outputManager = new OutputManager();
                outputManager.Analyze(inputManager.SmartFile, winningProgram);
                outputManager.DumpToFiles();
                Console.WriteLine(" ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Finished");
                Console.ReadKey();
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Unexpected error has occurred: {ex}");
                _log.Fatal(ex.ToString());
                Console.ReadKey();
            }
        }
    }
}
