﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TotoWinner.Common;

namespace TotoWinner.Verificator
{
    internal class OutputManager
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private SortedDictionary<int, List<string>> winningsHitsAccumulator; // {0-16}, {programs}
        private string outputDirectory;
        private int totalInputSmartLines = 0;
        internal OutputManager()
        {
            this.winningsHitsAccumulator = new SortedDictionary<int, List<string>>();
            var rootFolder = ConfigurationManager.AppSettings["RootFolder"];
            outputDirectory = Path.Combine(rootFolder, "Output");
            if (Directory.Exists(outputDirectory))
                Directory.Delete(outputDirectory, true);

            Directory.CreateDirectory(outputDirectory);
            Console.WriteLine($"Directory {outputDirectory} has been created");
            _log.Info($"Directory {outputDirectory} has been created");
            while (!Directory.Exists(outputDirectory)) ;
        }

        internal void Analyze(string smartFile, Dictionary<int, string> winners)
        {
            System.IO.StreamReader file = new System.IO.StreamReader(smartFile);
            var line = string.Empty;
            var lineNumber = 0;

            while ((line = file.ReadLine()) != null)
            {
                lineNumber++;
                Utilities.DrawTextCounterProgressInConsoleWindow("Analyzing ", lineNumber);

                if (line.Count() != winners.Count())
                    throw new Exception($"Smart file programs has {line.Count()} games Vs winning program that has {winners.Count()}");

                var hits = FindWinnings(line, winners);
                if (!this.winningsHitsAccumulator.ContainsKey(hits))
                    this.winningsHitsAccumulator.Add(hits, new List<string>());
                this.winningsHitsAccumulator[hits].Add(SmartToNormal(line));
            }
            totalInputSmartLines = lineNumber;
        }

        private string SmartToNormal(string line)
        {
            var result = new List<char>();
            for (var i = 0; i < line.Count(); i++)
            {
                var charToAdd = ' ';
                switch (line[i])
                {
                    case '2':
                        charToAdd = 'X';
                        break;
                    case '4':
                        charToAdd = '2';
                        break;
                    default:
                        charToAdd = line[i];
                        break;
                }                 
                result.Add(charToAdd);
                if (i % 3 == 2 && i < 14)
                    result.Add(' ');
            }

            var formatedLine = new string(result.ToArray());
            return formatedLine;
        }

        internal void DumpToFiles()
        {
            var accumulatedResults = new StringBuilder();
            accumulatedResults.AppendLine($"פגיעות\tטורים");
            foreach (var result in this.winningsHitsAccumulator)
            {
                accumulatedResults.Append($"{result.Key}\t{result.Value.Count}{Environment.NewLine}");
                var hitsFilename = Path.Combine(outputDirectory, $"{result.Key}_טורים_{result.Value.Count}_פגיעות.txt");
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(hitsFilename, true))
                {
                    var hitResults = string.Join(Environment.NewLine, result.Value);
                    file.WriteLine(hitResults);
                }
            }
            var outputAccumulated = Path.Combine(outputDirectory, $"טורים_{totalInputSmartLines}.txt");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(outputAccumulated, true))
            {
                file.WriteLine(accumulatedResults.ToString());
            }
        }

        private int FindWinnings(string line, Dictionary<int, string> winners)
        {
            var hits = 0;
            var index = 1;
            foreach (var game in line)
            {
                var hit = false;
                if (winners[index] != "0")
                {
                    switch (winners[index])
                    {
                        case "1":
                            hit = game == '1';
                            break;
                        case "X":
                            hit = game == '2';
                            break;
                        case "2":
                            hit = game == '4';
                            break;
                    }
                    if (hit)
                        hits++;
                }
                index++;
            }

            return hits;
        }
    }
}
