﻿namespace TotoWinner.Maintenance
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaintenanceServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.MaintenanceServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // MaintenanceServiceProcessInstaller
            // 
            this.MaintenanceServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.MaintenanceServiceProcessInstaller.Password = null;
            this.MaintenanceServiceProcessInstaller.Username = null;
            // 
            // MaintenanceServiceInstaller
            // 
            this.MaintenanceServiceInstaller.DelayedAutoStart = true;
            this.MaintenanceServiceInstaller.Description = "Executes sync process of programs data from https://www.bankerim.co.il";
            this.MaintenanceServiceInstaller.DisplayName = "Toto Winner Maintenance Service";
            this.MaintenanceServiceInstaller.ServiceName = "TotoWinnerMaintenanceService";
            this.MaintenanceServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.MaintenanceServiceProcessInstaller,
            this.MaintenanceServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller MaintenanceServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller MaintenanceServiceInstaller;
    }
}