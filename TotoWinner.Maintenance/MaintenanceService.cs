﻿using log4net;
using System;
using System.ServiceProcess;
using System.Threading;
using System.Timers;

namespace TotoWinner.Maintenance
{
    public partial class MaintenanceService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private System.Timers.Timer _workSeekTimer = null;
        private bool _isWorking = false;
        public MaintenanceService()
        {
            this.ServiceName = "MaintenanceService";
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("Maintanence service started");
            try
            {
                this._workSeekTimer = new System.Timers.Timer(TimeSpan.FromMinutes(5).TotalMilliseconds);
                this._workSeekTimer.Enabled = true;
                this._workSeekTimer.AutoReset = true;
                this._workSeekTimer.Elapsed += WorkSeekTimer_Elapsed;
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start maintenance service", ex);
            }
        }
        private void WorkSeekTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                this._workSeekTimer.Stop();
                this._isWorking = true;
                ProgramsManager.HandleProgramSyncronization();
                this._isWorking = false;
                this._workSeekTimer.Start();
            }
            catch (Exception ex)
            {
                this._isWorking = false;
                _log.Fatal("An error occurred trying to seek for work", ex);
                this._workSeekTimer.Start();
            }
        }
        protected override void OnStop()
        {
            try
            {
                while (this._isWorking == true)
                    Thread.Sleep(1000);
                this._workSeekTimer.Stop();
                this._workSeekTimer.Close();
                _log.Info("Maintenance service stopped");
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop maintenance service", ex);
            }
        }
    }
}
