﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Data;

namespace TotoWinner.Maintenance
{
    public static class ProgramsManager
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Services.ProgramServices _programSrv = Services.ProgramServices.Instance;
        private static Services.PersistanceServices _persistanceSrv = Services.PersistanceServices.Instance;
        public static void HandleProgramSyncronization()
        {
            try
            {
                if (_persistanceSrv.IsProgramsDataEmpty())
                {
                    _log.Info($"No program data was found, retrieving all programs.");
                    SyncAllPrograms();
                    _log.Info($"All programs have been retrieved. some might have issues, see details above");
                }
                else
                    SyncLatestPrograms();
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to handle program syncronization task", ex);
            }
        }
        public static void SyncLatestPrograms()
        {
            try
            {
                var latestPrograms = _programSrv.GetAllLatestPrograms();
                var newCandidates = latestPrograms.Collection.Where(p => p.Value.EndDate > DateTime.Today);
                foreach (var newProgram in newCandidates)
                    RetrieveProgram(newProgram);

                var candidates = _persistanceSrv.LookforOpenForBetsPrograms();
                if (candidates.Count > 0)
                {
                    var programs = _persistanceSrv.GetProgramsData(candidates);
                    foreach (var program in programs)
                    {
                        var exists = newCandidates.FirstOrDefault(p => p.Key.ProgramType == (ProgramType)program.Type && p.Key.TotoRoundId == program.Number);
                        if(exists.Equals(default(KeyValuePair<ProgramUniqueId, ProgramMetaData>)))
                        {
                            var games = _programSrv.GetProgramData(program.EndDate, (ProgramType)program.Type);
                            _programSrv.UpsertGames(program.EndDate, (ProgramType)program.Type, games, program.Id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to sync latest programs.", ex);
            }
        }

        public static void SyncAllPrograms()
        {
            try
            {
                var programs = _programSrv.GetAllPrograms();
                //var programs = GetPrograms(DateTime.Today, ProgramType.Winner16);
                var sortedColl = from pair in programs.Collection
                                 orderby pair.Value.EndDate ascending// descending
                                 select pair;
                foreach (var program in sortedColl)
                {
                    RetrieveProgram(program);
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to sync programs.", ex);
            }
        }

        private static void RetrieveProgram(KeyValuePair<ProgramUniqueId, ProgramMetaData> program)
        {
            try
            {
                var games = _programSrv.GetProgramData(program.Value.EndDate, program.Key.ProgramType);
                _programSrv.UpsertProgramData(games, program.Key.ProgramType, program.Value.EndDate, program.Key.TotoRoundId);
                //var programId = _persistanceSrv.ProgramUpsert(program.Key.ProgramType,
                //    (short)program.Value.EndDate.Year,
                //    program.Key.TotoRoundId,
                //    program.Value.EndDate);
                //UpsertGames(program.Value.EndDate, program.Key.ProgramType, games, programId);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to retrieve program << {program.Key.ToString()}; {program.Value.ToString()} >>", ex);
            }
        }

        //private static void UpsertGames(DateTime endDate, ProgramType programType, ProgramDataResult games, int programId)
        //{
        //    try
        //    {
        //        foreach (var game in games.Details.Keys)
        //        {
        //            var item = games.Details[game];
        //            char won = ' ';
        //            var wonItem = item.Bets.Details.FirstOrDefault(i => i.Value.Won == true);
        //            if (!wonItem.Equals(default(KeyValuePair<BetOutputType, BetDetails>)))
        //            {
        //                switch (wonItem.Key)
        //                {
        //                    case BetOutputType.Bet1:
        //                        won = '1';
        //                        break;
        //                    case BetOutputType.BetX:
        //                        won = 'X';
        //                        break;
        //                    case BetOutputType.Bet2:
        //                        won = '2';
        //                        break;
        //                    default:
        //                        throw new Exception($"bet of type {wonItem.Key} is not supported");
        //                }
        //            }
        //            _persistanceSrv.GameUpsert(programId, (byte)item.GameIndex,
        //                item.GameTime,
        //                item.GameName,
        //                item.LeagueName,
        //                item.GameResult,
        //                item.Bets.OpenForBets,
        //                item.BetColor,
        //                item.Bets.Details[BetOutputType.Bet1].Rate,
        //                item.Bets.Details[BetOutputType.BetX].Rate,
        //                item.Bets.Details[BetOutputType.Bet2].Rate,
        //                won != ' ' ? won.ToString() : null,
        //                item.Validations.Keys.ToList());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"An error occured trying to upsert games of program programId={programId}, programType={programType}, endDate={endDate}", ex);
        //    }
        //}

    }
}
