﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;

namespace TotoWinner.Common
{
    public static class Utilities
    {
        public static int DecimalStrToInt(string decStr)
        {
            if (decimal.TryParse(decStr, out var decimalValue))
                return decimal.ToInt32(decimalValue);
            return int.Parse(decStr);
        }
        //for 1,2,3
        // return 1,2,3,12,13,23,123
        public static IEnumerable<IEnumerable<T>> GetMathCombinations<T>(IEnumerable<T> source)
        {
            var lists = new List<List<T>>();
            for (var i = 1; i <= source.Count(); i++)
            {
                foreach (var numbers in source.Combinations(i))
                {
                    lists.Add(new List<T>(numbers));
                }
            }
            return lists;
        }
        //for A,B,C ; n=2
        // return AA,AB,AC, BA,BB,BC CA,CB,CC
        public static IEnumerable<string> GetMathCombinationsWithRepetition<T>(IEnumerable<T> source, int length)
        {
            var lists = new List<string>();
            foreach (var c in source.CombinationsWithRepetition<T>(length))
                lists.Add(c);
            return lists;
        }

        public static void DrawTextCounterProgressInConsoleWindow(string prefix, int lineNumber)
        {
            Console.CursorLeft = 0;
            Console.Write($"{prefix} "); //start
            Console.CursorLeft = prefix.Count()+1;
            Console.Write($"{lineNumber}");
        }

        public static void DrawTextProgressBarInConsoleWindow(int progress, int total)
        {
            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("["); //start
            Console.CursorLeft = 32;
            Console.Write("]"); //end
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            //draw filled part
            int position = 1;
            for (int i = 0; i < onechunk * progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw unfilled part
            for (int i = position; i <= 31; i++)
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw totals
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(progress.ToString() + " of " + total.ToString() + "    "); //blanks at the end remove any excess
        }
        public static void CompressWithZipFile(string zipFileFullPath, string fileFullPath, string filename)
        {
            try
            {
                using (var archive = ZipFile.Open(zipFileFullPath, ZipArchiveMode.Create))
                {
                    archive.CreateEntryFromFile(fileFullPath, filename);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to zip a file: {filename}, path={fileFullPath}, zipPath={zipFileFullPath}.", ex);
            }
        }
        public static void UnCompressZipFile(string zipFileFullPath, string directoryToExtract)
        {
            try
            {
                using (var archive = ZipFile.Open(zipFileFullPath, ZipArchiveMode.Read))
                {
                    archive.ExtractToDirectory(directoryToExtract);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured trying to unzip a file: {zipFileFullPath}, to directory={directoryToExtract}.", ex);
            }
        }
    }
}
