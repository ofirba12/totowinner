﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace TotoWinner.Common
{
    public static class WebApiInvoker
    {
        public static T Post<T>(Uri requestUri, ExpandoObject payload)
        {
            T result = PostInvoker<T>(requestUri, payload).GetAwaiter().GetResult();
            return result;
        }

        private static async Task<T> PostInvoker<T>(Uri requestUri, ExpandoObject payload)
        {
            try
            {
                string jsonPayload = Newtonsoft.Json.JsonConvert.SerializeObject(payload);
                var objClint = new HttpClient();
                HttpResponseMessage response = await objClint.PostAsync(requestUri,
                    new StringContent(jsonPayload, System.Text.Encoding.UTF8, "application/json"));
                response.EnsureSuccessStatusCode();
                string responJsonText = await response.Content.ReadAsStringAsync();
                return await Task.Run(() => Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responJsonText));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to call web api url = [{0}] | payload = [{1}]",
                    requestUri.AbsoluteUri,
                    Newtonsoft.Json.JsonConvert.SerializeObject(payload)),
                    ex);
            }
        }
        public static dynamic[] JsonDeserializeDynamicArray(string jsonString)
        {
            var jss = new JavaScriptSerializer();
            jss.MaxJsonLength = 2147483647;
            var dict = jss.Deserialize<dynamic[]>(jsonString);
            return dict;
        }
        public static dynamic JsonDeserializeDynamic(string jsonString)
        {
            var jss = new JavaScriptSerializer();
            jss.MaxJsonLength = 2147483647;
            var dict = jss.Deserialize<dynamic>(jsonString);
            return dict;
        }

        //static async void Test()
        //{
        //    var r = await DownloadPage("http://stackoverflow.com");
        //    Console.WriteLine(r.Substring(0, 50));
        //}

        //static async Task<string> DownloadPage(string url)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        using (var r = await client.GetAsync(new Uri(url)))
        //        {
        //            string result = await r.Content.ReadAsStringAsync();
        //            return result;
        //        }
        //    }
        //}
    }
}
