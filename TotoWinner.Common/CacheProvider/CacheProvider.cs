﻿using System;
using System.Runtime.Caching;

namespace TotoWinner.Common
{
    public class CacheProvider : ICacheProvider
    {
        private ObjectCache Cache => MemoryCache.Default;

        public object Get(string key)
        {
            return Cache[key];
        }
        public void Set(string key, object data, int cacheTimeInMinutes)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cacheTimeInMinutes);

            Cache.Add(new CacheItem(key, data), policy);
        }
        public bool IsSet(string key)
        {
            return (Cache[key] != null);
        }
        public void Invalidate(string key)
        {
            Cache.Remove(key);
        }
    }
}
