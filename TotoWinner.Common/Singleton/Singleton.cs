﻿using System;
using System.Reflection;

namespace TotoWinner.Common.Infra
{
    public abstract class Singleton<T>
            where T : Singleton<T>
    {
        #region Members

        private static T _instance = null;
        private static object _lock = new object();

        #endregion

        #region Properties

        #region Instance

        public static T Instance
        {
            get
            {
                try
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            // Binding flags exclude public constructors:
                            ConstructorInfo constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[0], null);

                            // Also exclude internal constructors:
                            if (constructor == null || constructor.IsAssembly)
                                throw new Exception(string.Format("A private or protected constructor is missing for [{0}].", typeof(T).FullName));

                            _instance = (T)constructor.Invoke(null);
                            _instance.InstanceCreated();
                        }
                    }

                    return _instance;
                }
                catch (System.Exception ex)
                {
                    throw new Exception(string.Format("An error occurred retrieving an instance of [{0}] singleton.", typeof(T).FullName), ex);
                }
            }
        }

        #endregion

        #endregion

        #region Constructors

        protected Singleton()
        {
        }

        #endregion

        #region Methods

        #region Reset

        public static void Reset()
        {
            lock (_lock)
            {
                if (_instance != null)
                {
                    _instance.InstanceReseted();
                    _instance = null;
                }
            }
        }

        #endregion

        #region InstanceCreated

        protected virtual void InstanceCreated() { }

        #endregion

        #region InstanceReseted

        protected virtual void InstanceReseted() { }

        #endregion

        #endregion
    }
}
