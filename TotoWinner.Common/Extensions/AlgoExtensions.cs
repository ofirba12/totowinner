﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TotoWinner.Common
{
    public static class AlgoExtensions
    {
        public static IEnumerable<IEnumerable<T>> Combinations<T>(this IEnumerable<T> source, int n)
        {
            if (n == 0)
                yield return Enumerable.Empty<T>();

            int count = 1;
            foreach (T item in source)
            {
                foreach (var innerSequence in source.Skip(count).Combinations(n - 1))
                {
                    yield return new T[] { item }.Concat(innerSequence);
                }
                count++;
            }
        }
        public static IEnumerable<String> CombinationsWithRepetition<T>(this IEnumerable<T> input, int length)
        {
            if (length <= 0)
                yield return "";
            else
            {
                foreach (var i in input)
                    foreach (var c in CombinationsWithRepetition<T>(input, length - 1))
                        yield return i.ToString() + c;
            }
        }

    }
}
