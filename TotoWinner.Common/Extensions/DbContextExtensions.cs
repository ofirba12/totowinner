﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

namespace TotoWinner.Common
{
    public static class DbContextExtensions
    {
        /// <summary>
        /// Execute stored procedure with single table value parameter.
        /// </summary>
        /// <typeparam name="T">Type of object to store.</typeparam>
        /// <param name="context">DbContext instance.</param>
        /// <param name="data">Data to store</param>
        /// <param name="procedureName">Procedure name</param>
        /// <param name="paramName">Parameter name</param>
        /// <param name="typeName">User table type name</param>
        public static void ExecuteTableValueProcedure<T>(this DbContext context, IEnumerable<T> data, string procedureName, string paramName, string typeName)
        {
            //// convert source data to DataTable
            DataTable table = data.ToDataTable();

            //// create parameter
            SqlParameter parameter = new SqlParameter(paramName, table);
            parameter.SqlDbType = SqlDbType.Structured;
            parameter.TypeName = typeName;

            //// execute sp sql
            string sql = String.Format("EXEC {0} {1};", procedureName, paramName);

            //// execute sql
            context.Database.ExecuteSqlCommand(sql, parameter);
        }
        public static void ExecuteTablesValueProcedure<T1, T2>(this DbContext context, 
            IEnumerable<T1> data1,
            IEnumerable<T2> data2,
            string procedureName, 
            string[] paramName, 
            string[] typeName)
        {
            //// convert source data to DataTable
            DataTable table1 = data1.ToDataTable();
            DataTable table2 = data2.ToDataTable();

            //// create parameter
            SqlParameter parameter1 = new SqlParameter(paramName[0], table1);
            parameter1.SqlDbType = SqlDbType.Structured;
            parameter1.TypeName = typeName[0];
            SqlParameter parameter2 = new SqlParameter(paramName[1], table2);
            parameter2.SqlDbType = SqlDbType.Structured;
            parameter2.TypeName = typeName[1];

            //// execute sp sql
            string sql = String.Format("EXEC {0} {1},{2};", procedureName, paramName[0], paramName[1]);

            //// execute sql
            context.Database.ExecuteSqlCommand(sql, new object[] { parameter1 , parameter2 });
        }
    }
}
