﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Common
{
    public static class ListsExtensions
    {
        public static bool ContainsPerIndex(this List<string> source, List<string> dest)
        {
            if (source.Count != dest.Count)
                return false;
            var flag = true;
            for (var index = 0; index < source.Count; index++)
            {
                flag = flag && source[index].Contains(dest[index]);
            }
            return flag;
        }
    }
}
