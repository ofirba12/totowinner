﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    internal static class BotHandlers
    {
        private static readonly ILog _log = LogManager.GetLogger("TelegramBot");
        private static TelegramBotServices _botSrv = TelegramBotServices.Instance;
        internal static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            // Only process Message updates: https://core.telegram.org/bots/api#message
            if (update.Type == UpdateType.CallbackQuery)
            {
                BotClient_OnCallbackQuery(update.CallbackQuery.Message.Chat.Id, update.CallbackQuery.Message, update.CallbackQuery.Data);
            }

            if (update.Type != UpdateType.Message)
                return;
            if (update.Message?.Type != MessageType.Text)
                return;

            await BotClient_OnTextMessage(botClient as TelegramBotClient, update.Message);
        }

        internal static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var errorMessage = string.Empty;
            if (exception is ApiRequestException)
            { 
                var apiRequestException = (ApiRequestException)exception;
                errorMessage = $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message},\n{exception}";
            };

            Console.WriteLine(errorMessage);
            return Task.CompletedTask;
        }

        private static async Task BotClient_OnTextMessage(TelegramBotClient botClient, Message message)
        {
            try
            {
                _log.Info($"ChatId: {message.Chat.Id}, Text:{message.Text}");
                if (message.Type == Telegram.Bot.Types.Enums.MessageType.Text && message.Text == @"/start")
                {//TODO: OnboardingChat should be set to ISChatOpen=false after 30 minutes, so it can be cleared
                    var welcome = new OnboardingChat(botClient, message.Chat.Id, message.From.FirstName, message.From.LastName);
                    await welcome.Show();
                    if (!ChatRepository.RegistrationChatItems.ContainsKey(message.Chat.Id))
                        ChatRepository.RegistrationChatItems.Add(message.Chat.Id, welcome);
                }
                else if (ChatRepository.RegistrationChatItems.Where(c => c.Value != null && c.Value.IsChatOpen).Count() > 0 &&
                    message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
                {
                    if (ChatRepository.RegistrationChatItems.ContainsKey(message.Chat.Id) &&
                        ChatRepository.RegistrationChatItems[message.Chat.Id] != null)
                    {
                        await ChatRepository.RegistrationChatItems[message.Chat.Id].PostUserAction(message.Text);
                        var registrationSuccess = ChatRepository.RegistrationChatItems[message.Chat.Id].IsChatOpen == false;
                        ChatRepository.MaintainRegistrationChat();
                        if (registrationSuccess)
                        {
                            _log.Info($"Start Chart after success registration with subscriber={message.Chat.Id}");
                            StartChat(botClient, message.Chat.Id);
                        }
                    }
                }
                if (message.Text == "קוד")
                {
                    SendMessage(botClient, message.Chat.Id, $"מספר לקוח: {message.Chat.Id}");
                }
                else if (message.Text == "טיפ")
                {
                    var start = new DoYouWantMoreTips(botClient, message.Chat.Id);
                    await start.Show();
                    ChatRepository.AddChatItem(null, start.MessageId.Value, start);
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"Unexpected error occurred trying to recieve messages from subscriber {message.Chat.Id}", ex);
            }
        }
        private static async void StartChat(TelegramBotClient botClient, long subscriber)
        {
            DoYouWantMoreTips start = null;
            try
            {
                var tip = _botSrv.FindTipToPublish(subscriber);
                if (tip != null)
                {
                    start = new DoYouWantMoreTips(botClient, subscriber);
                    await start.Show();
                    ChatRepository.AddChatItem(null, start.MessageId.Value, start);
                }
                else
                {
                    SendMessage(botClient, subscriber, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*");
                }
            }
            catch (Exception ex)
            {
                if (start != null)
                    start = null;
                _log.Fatal($"An error occurred trying to start chat with subscriber {subscriber}", ex);
            }
        }
        private static async void SendMessage(TelegramBotClient botClient,long chatId, string message)
        {
            try
            {
                Task.Delay(1000).Wait();
                await botClient.SendTextMessageAsync(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Markdown);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to send message to chatId:{chatId}, message:{Environment.NewLine}{message}", ex);
            }
        }
        private static async void BotClient_OnCallbackQuery(long chatId, Message message, string data)
        {
            try
            {
                _log.Info($"chatId:{chatId}, MessageId: {message.MessageId}, Data:{data}");
                await ChatRepository.GetPostUserAction(message.MessageId, data);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to get callback query for message {message.MessageId}, data {data}", ex);
            }
        }
    }
}
