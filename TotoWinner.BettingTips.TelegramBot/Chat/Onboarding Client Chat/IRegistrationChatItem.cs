﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.BettingTips.TelegramBot
{
    public interface IRegistrationChatItem
    {
        int? MessageId { get; }
        bool IsChatOpen { get; }

        Task Show();
        Task PostUserAction(string callbackData);
    }
}
