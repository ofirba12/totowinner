﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.BettingTips.TelegramBot
{
    public interface IChatItem
    {
        int? MessageId { get; }

        Task Show();
        Task PostUserAction(string callbackData);
        void BuildMenu();
    }
}
