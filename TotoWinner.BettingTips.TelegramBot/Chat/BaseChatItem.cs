﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public abstract class BaseChatItem
    {
        protected static readonly ILog _log = LogManager.GetLogger("TelegramBot");
        protected TelegramBotClient BotClient;
        protected Timer WaitForAnswer;
        protected double WaitForAnswerInterval;
        public long SubscriberId;
        protected int? _messageId;
        public int? MessageId => this._messageId;
        protected InlineKeyboardMarkup Menu;
        protected int ManagerSubscriberId = 585508035;
        protected List<int> AlreadySentTipsToday;
        protected BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        protected BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        protected TelegramBotServices _botSrv = TelegramBotServices.Instance;
        protected TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;
        protected int NumberOfNonChatItemsInMenu;

        protected async void ShowMenu()
        {
            if (this.Menu.InlineKeyboard.Count() == this.NumberOfNonChatItemsInMenu)
                await BotClient.SendTextMessageAsync(this.SubscriberId, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*", 
                    Telegram.Bot.Types.Enums.ParseMode.Markdown);
            else
            {
                this.WaitForAnswer = new Timer(this.WaitForAnswerInterval);
                this.WaitForAnswer.Enabled = true;
                this.WaitForAnswer.Elapsed += WaitForAnswer_Elapsed;
            }
        }
        protected bool IsActiveMenu()
        {
            return this.WaitForAnswer!= null && this.WaitForAnswer.Enabled;
        }
        protected async Task HideMenu()
        {
            this.WaitForAnswer.Enabled = false;
            this.WaitForAnswer.Dispose();
            await Task.Delay(1000);
            await BotClient.EditMessageReplyMarkupAsync(this.SubscriberId, this._messageId.Value);
        }
        protected async void WaitForAnswer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                await HideMenu();
                //var replyMessage = "*עקב חוסר פעילות השיחה נותקה. טיפ הבא יעלה בהמשך*";
                var replyMessage = "עקב חוסר פעילות השיחה נותקה. טיפ הבא יעלה בהמשך. *במידה ואתה מעוניין לקבל עוד טיפ כעת, לחץ על הלינק \"לצפייה ביחס המעודכן\" על הטיפ האחרון שעלה.*👆";
                await this.BotClient.SendTextMessageAsync(this.SubscriberId, replyMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);                
            }
            catch(Exception ex)
            {
                var msgId = this.MessageId.HasValue
                    ? this.MessageId.Value.ToString()
                    : "[N/A]";
                _log.Fatal($"Failed to wait for an answer from subscriber:{this.SubscriberId} to messageId:{msgId}, seems like subscriber does not have network connection",ex);
            }
            finally
            {
                if (this.MessageId.HasValue)
                    ChatRepository.ReleaseChatItems(this.MessageId.Value);
            }
        }
    }
}