﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public class WhichCountryDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        public SportIds Sport;
        private List<Tip> CurrentStack;

        public WhichCountryDoYouWant(TelegramBotClient client, long subscriberId, SportIds sport, double waitForAnswerInterval, 
            List<int> alreadySentTipsToday, List<Tip> currentStack)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.Sport = sport;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            this.CurrentStack = currentStack;
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת ענף {BettingTipsServicesHelpers.HebrewTranslate(this.Sport)} {BettingTipsServicesHelpers.GetSportEmoji(this.Sport)}, אנא בחר את המדינה או התחרות הרצויה",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                 null,
                 false,
                 false,
                 0,
                 true,
                 this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackDataCountry)
        {
            if (!IsActiveMenu())
                return;

            await HideMenu();
            switch (callbackDataCountry)
            {
                case "BACK":
                    this.NextChat = new WhichSportDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, this.AlreadySentTipsToday);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
                default:
                    this.NextChat = new WhichBetDoYouWant(this.BotClient, this.SubscriberId, this.Sport, callbackDataCountry,
                        this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
            }
        }

        public void BuildMenu()
        {
            var countries = GetAllActiveCountries();
            var baseMenu = new List<InlineKeyboardButton[]>();
            foreach (var country in countries)
            {
                var countryCode = BettingTipsServicesHelpers.GetHebrewCountryCode(country);
                var emoji = BettingTipsServicesHelpers.GetCountryFlagEmoji(countryCode);
                var inlineButton = InlineKeyboardButton.WithCallbackData($"{country} {emoji}", country);
                baseMenu.Add(new[] { inlineButton });
            }
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("⬅️ חזרה לשאלה הקודמת", "BACK");
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
        private List<string> GetAllActiveCountries()
        {
            var filter = new BettingTipsFilter(BettingTipFilterType.Filtered);
            var allTips = this.CurrentStack
                .Where(t => this.AlreadySentTipsToday != null && !this.AlreadySentTipsToday.Contains(t.TipId));
            var countries = allTips
                .Where(t => t.Sport == this.Sport && t.BetRate <= filter.MaxRate.Value && t.BetRate >= filter.MinRate.Value)
                .Select(t => t.Country)
                .Distinct().OrderBy(a => a);
            return countries.ToList();
        }
    }
}
