﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace TotoWinner.BettingTips.TelegramBot
{
    public class DoYouWantMoreTips : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        private static int WaitForAnswerInSeconds;

        public DoYouWantMoreTips(TelegramBotClient client, long subscriberId, List<int> alreadySentTipsToday = null)
        {
            WaitForAnswerInSeconds = int.Parse(ConfigurationManager.AppSettings["WaitForAnswerInSeconds"].ToString());
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = TimeSpan.FromSeconds(WaitForAnswerInSeconds).TotalMilliseconds;
            this.NextChat = null;
            AlreadySentTipsToday = alreadySentTipsToday ?? _btPersistanceSrv.GetAllSubscriberTodayAndFutureTipIds(this.SubscriberId);
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"האם אתה מעוניין בטיפ נוסף?",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                 null,
                 false,
                 false,
                 0,
                 true,
                 this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackData)
        {
            if (!IsActiveMenu())
                return;

            await HideMenu();
            switch (callbackData)
            {
                case "1":
                    this.NextChat = new WhichTipDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, AlreadySentTipsToday);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
                case "2":
                    var replyMessage = "*בקשת לא לצפות בטיפ נוסף, *שיהיה המשך יום טוב ובהצלחה.";
                    await this.BotClient.SendTextMessageAsync(this.SubscriberId, replyMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);
                    break;
                case "3":
                    replyMessage = "נציג שירות קיבל את הפניה והוא יחזור אליך בקרוב.";
                    await this.BotClient.SendTextMessageAsync(this.SubscriberId, replyMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);
                    var subscriber = _botSrv.GetSubscriberByChatId(this.SubscriberId);
                    replyMessage = $"נא ליצור קשר עם לקוח {this.SubscriberId}: {subscriber.FirstName} {subscriber.LastName} {subscriber.CellNumber} {subscriber.Mail}";
                    await this.BotClient.SendTextMessageAsync(ManagerSubscriberId, replyMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);
                    ChatRepository.ReleaseChatItems(this.MessageId.Value);
                    break;
            }            
        }

        public void BuildMenu()
        {
            var inlineButtonYes = InlineKeyboardButton.WithCallbackData("כן ✅", "1");
            var inlineButtonNo = InlineKeyboardButton.WithCallbackData("לא ❌", "2");
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("💭 פנייה לנציג שירות בכל נושא אחר", "3");
            var baseMenu = new List<InlineKeyboardButton[]>();
            baseMenu.Add(new[] { inlineButtonYes });
            baseMenu.Add(new[] { inlineButtonNo });
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 2;
        }
    }
}
