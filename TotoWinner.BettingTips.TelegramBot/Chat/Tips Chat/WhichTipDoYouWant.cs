﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Data.TelegramBot;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public class WhichTipDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        static internal StackTipsRepository stackTips = new StackTipsRepository();

        public WhichTipDoYouWant(TelegramBotClient client, long subscriberId, double waitForAnswerInterval, List<int> alreadySentTipsToday)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת לצפות בטיפ נוסף, *אנא בחר את סוג הטיפ הרצוי*",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                 null,
                 false,
                 false,
                 0,
                 true,
                 this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackData)
        {
            try
            {
                if (!IsActiveMenu())
                    return;

                await HideMenu();
                switch (callbackData)
                {
                    case "1":
                        SetButtonStatistics(BotButtonType.Favorite);
                        var filter = new BettingTipsFilter(BettingTipFilterType.Favorite);
                        var tip = _btTipsSrv.GetNextActiveTips(filter, this.AlreadySentTipsToday);
                        _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                        ChatRepository.ReleaseChatItems(this.MessageId.Value);
                        break;
                    case "2":
                        SetButtonStatistics(BotButtonType.In3HoursAndFavorite);
                        filter = new BettingTipsFilter(BettingTipFilterType.In3HoursAndFavorite);
                        tip = _btTipsSrv.GetNextActiveTips(filter, this.AlreadySentTipsToday);
                        _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                        ChatRepository.ReleaseChatItems(this.MessageId.Value);
                        break;
                    case "3":
                        SetButtonStatistics(BotButtonType.SingleHighRate);
                        filter = new BettingTipsFilter(BettingTipFilterType.SingleHighRate);
                        tip = _btTipsSrv.GetNextActiveTips(filter, this.AlreadySentTipsToday);
                        _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                        ChatRepository.ReleaseChatItems(this.MessageId.Value);
                        break;
                    case "4":
                        SetButtonStatistics(BotButtonType.Custom);
                        this.NextChat = new WhichSportDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, this.AlreadySentTipsToday);
                        await this.NextChat.Show();
                        ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                        break;
                    case "5":
                        SetButtonStatistics(BotButtonType.Tippers);
                        this.NextChat = new WhichTipperDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, this.AlreadySentTipsToday, 
                            stackTips.GetActiveStackTips());
                        await this.NextChat.Show();
                        ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                        break;
                    case "BACK":
                        this.NextChat = new DoYouWantMoreTips(this.BotClient, this.SubscriberId);
                        await this.NextChat.Show();
                        ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                        break;
                }
            }
            catch(Exception ex) //TODO: could be network issues, need to release chat or retry when network is available
            {
                throw new Exception($"An error occurred trying to handle user callbackData {callbackData}", ex);
            }
        }
        private List<int> GetAllActiveTippers()
        {
            var filter = new BettingTipsFilter(BettingTipFilterType.WideFilter);
            var allTips = stackTips.GetActiveStackTips()
                .Where(t => this.AlreadySentTipsToday != null && !this.AlreadySentTipsToday.Contains(t.TipId));
            var tippers = allTips
                .Where(t => t.BetRate <= filter.MaxRate.Value && t.BetRate >= filter.MinRate.Value)
                .Select(t => t.TipperId)
                .Distinct().OrderBy(a => a);
            return tippers.ToList();
        }
        private void SetButtonStatistics(BotButtonType button)
        {
            try
            {
                _botSrv.SetButtonStatistics(button);
            }
            catch(Exception ex)
            {
                _log.Fatal($"An error occurred trying to set statistics for button={button}", ex);
            }
        }
        public void BuildMenu()
        {
            var inlineButton1 = InlineKeyboardButton.WithCallbackData("🌟 הטיפ המועדף שלנו", "1");
            var inlineButton2 = InlineKeyboardButton.WithCallbackData("🕗 טיפ למשחק שמתחיל בקרוב", "2");
            var inlineButton3 = InlineKeyboardButton.WithCallbackData("💥 טיפ סינגל ביחס גבוה", "3");
            var inlineButton4 = InlineKeyboardButton.WithCallbackData("⭐️ טיפ מותאם אישית", "4");
            var inlineButton5 = InlineKeyboardButton.WithCallbackData("👥 נבחרת ממליצים", "5");
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("⬅️ חזרה לשאלה קודמת", "BACK");
            var baseMenu = new List<InlineKeyboardButton[]>();
            var activeTippers = GetAllActiveTippers();
            if (activeTippers.Count > 0)
                baseMenu.Add(new[] { inlineButton5 });
            baseMenu.Add(new[] { inlineButton1 });
            baseMenu.Add(new[] { inlineButton2 });
            baseMenu.Add(new[] { inlineButton3 });
            baseMenu.Add(new[] { inlineButton4 });
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
    }
}
