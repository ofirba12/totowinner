﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public class WhichTipperDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        private List<Tip> CurrentStack;
        private Dictionary<int, string> AllTippers;

        public WhichTipperDoYouWant(TelegramBotClient client, long subscriberId, double waitForAnswerInterval, 
            List<int> alreadySentTipsToday,
            List<Tip> currentStack)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            this.CurrentStack = currentStack;
            AllTippers = _btPersistanceSrv.GetAllTippers().ToDictionary(t => t.TipperId, t => t.TipperName);
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת לצפות בטיפ של נבחרת ממליצים, *אנא בחר ממליץ*",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                 null,
                 false,
                 false,
                 0,
                 true,
                 this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackDataTipper)
        {
            if (!IsActiveMenu())
                return;

            await HideMenu();
            switch (callbackDataTipper)
            {
                case "BACK":
                    this.NextChat = new WhichTipDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, this.AlreadySentTipsToday);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
                default:
                    this.NextChat = new WhichTipperBetDoYouWant(this.BotClient, this.SubscriberId, 
                        AllTippers.Where(t=> t.Value == callbackDataTipper).First(),
                        this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
            }
        }

        public void BuildMenu()
        {
            var tippersList = GetAllActiveTippers();
            var baseMenu = new List<InlineKeyboardButton[]>();
            foreach (var tipper in tippersList)
            {
                var inlineButton = InlineKeyboardButton.WithCallbackData($"{tipper}", tipper);
                baseMenu.Add(new[] { inlineButton });
            }
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("⬅️ חזרה לשאלה הקודמת", "BACK");
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
        private List<string> GetAllActiveTippers()
        {
            var filter = new BettingTipsFilter(BettingTipFilterType.WideFilter);
            var allTips = this.CurrentStack
                .Where(t => this.AlreadySentTipsToday != null && !this.AlreadySentTipsToday.Contains(t.TipId));
            var tippers = allTips
                .Where(t => t.BetRate <= filter.MaxRate.Value && t.BetRate >= filter.MinRate.Value)
                .Select(t => t.TipperId);

            var tipperNames = tippers.ToList()
                .ConvertAll<string>(t => AllTippers[t])
                .Distinct().OrderBy(a => a)
                .ToList();
            return tipperNames;
        }
    }
}
