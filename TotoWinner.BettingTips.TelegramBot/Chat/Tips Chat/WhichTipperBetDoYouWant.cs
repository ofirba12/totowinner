﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public class WhichTipperBetDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        public KeyValuePair<int, string> Tipper;
        private List<Tip> CurrentStack;

        public WhichTipperBetDoYouWant(TelegramBotClient client, long subscriberId, KeyValuePair<int, string> tipper, double waitForAnswerInterval, 
            List<int> alreadySentTipsToday, List<Tip> currentStack)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            this.CurrentStack = currentStack;
            this.Tipper = tipper;
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת לראות הטיפ של {this.Tipper.Value}.",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                 null,
                 false,
                 false,
                 0,
                 true,
                 this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackSelectedTipId)
        {
            if (!IsActiveMenu())
                return;

            await HideMenu();
            switch (callbackSelectedTipId)
            {
                case "BACK":
                    this.NextChat = new WhichTipperDoYouWant(this.BotClient, this.SubscriberId, 
                        this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
                default:
                    var tipId = int.Parse(callbackSelectedTipId);
                    var tip = _btTipsSrv.GetTip(tipId);
                    _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                    ChatRepository.ReleaseChatItems(this.MessageId.Value);
                    break;
            }
        }

        public void BuildMenu()
        {
            var filter = new BettingTipsFilter(BettingTipFilterType.WideFilter);
            var bets = this.CurrentStack
                .Where(b => this.AlreadySentTipsToday != null && 
                        !this.AlreadySentTipsToday.Contains(b.TipId) && 
                        b.TipperId == this.Tipper.Key && b.BetRate <= filter.MaxRate.Value && b.BetRate >= filter.MinRate.Value)
                .ToList();
            var baseMenu = new List<InlineKeyboardButton[]>();
            foreach (var bet in bets)
            {
                var emoji = BettingTipsServicesHelpers.GetSportEmoji(bet.Sport);
                var inlineButton = InlineKeyboardButton.WithCallbackData($"{bet.BetName} {emoji}", bet.TipId.ToString());
                baseMenu.Add(new[] { inlineButton });
            }
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("⬅️ חזרה לשאלה הקודמת", "BACK");
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
    }
}
