﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public class WhichSportDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        private List<Tip> CurrentStack;

        public WhichSportDoYouWant(TelegramBotClient client, long subscriberId, double waitForAnswerInterval, List<int> alreadySentTipsToday)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            this.CurrentStack = _btTipsSrv.GetAllActiveStackTips();
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת טיפ מותאם אישית, *אנא בחר את סוג הענף הרצוי*",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                 null,
                 false,
                 false,
                 0,
                 true,
                 this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackData)
        {
            if (!IsActiveMenu())
                return;

            await HideMenu();
            switch (callbackData)
            {
                case "כדורגל":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, SportIds.Soccer, this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    break;
                case "כדורסל":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, SportIds.Basketball, this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    break;
                case "טניס":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, SportIds.Tennis, this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    break;
                case "כדור יד":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, SportIds.Handball, this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    break;
                case "בייסבול":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, SportIds.Baseball, this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    break;
                case "פוטבול אמריקאי":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, SportIds.Football, this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    break;
                case "BACK":
                    this.NextChat = new WhichTipDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, this.AlreadySentTipsToday);
                    break;
            }
            await this.NextChat.Show();
            ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
        }
        public void BuildMenu()
        {
            var sports = GetAllActiveStackSports();
            var baseMenu = new List<InlineKeyboardButton[]>();
            foreach (var sport in sports)
            {
                var emoji = BettingTipsServicesHelpers.GetSportEmoji(sport);
                var inlineButton = InlineKeyboardButton.WithCallbackData($"{emoji} {sport}", sport);
                baseMenu.Add(new[] { inlineButton });
            }
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("⬅️ חזרה לשאלה הקודמת", "BACK");
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
        private List<string> GetAllActiveStackSports()
        {
            var filter = new BettingTipsFilter(BettingTipFilterType.Filtered);
            var allTips = this.CurrentStack
                .Where(t => this.AlreadySentTipsToday != null && !this.AlreadySentTipsToday.Contains(t.TipId) && t.BetRate <= filter.MaxRate.Value);
            var sports = allTips.Select(t => t.Sport)
                .Distinct()
                .Select(t => BettingTipsServicesHelpers.HebrewTranslate(t));
            return sports.ToList();
        }
    }
}
