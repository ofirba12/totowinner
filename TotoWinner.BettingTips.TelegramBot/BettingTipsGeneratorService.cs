﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    partial class BettingTipsGeneratorService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger("BettingTipsGenerator");
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        private static BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private System.Timers.Timer _generatorTimer = null;
        private HourMinute generatorTime = null;
        public BettingTipsGeneratorService()
        {
            try
            {
                InitializeComponent();
                this.ServiceName = "BettingTipsGeneratorService";
                var generatorTime = ConfigurationManager.AppSettings["TipsGeneratorTime"];
                var parts = generatorTime.Split(':');
                this.generatorTime = new HourMinute(int.Parse(parts[0]), int.Parse(parts[1]));
                _log.Info($"Generator will execute everyday at {this.generatorTime.Hour.ToString("D2")}:{this.generatorTime.Minute.ToString("D2")}");

            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to construct BettingTipsGeneratorService service", ex);
            }
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("BettingTips Generator service started");
            try
            {
                this._generatorTimer = new System.Timers.Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
                this._generatorTimer.Enabled = true;
                this._generatorTimer.AutoReset = true;
                this._generatorTimer.Elapsed += _generatorTimer_Elapsed;

            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start BettingTips Generator service", ex);
            }
        }

        private void _generatorTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this._generatorTimer.Enabled = false;
            var canStart = this.generatorTime.Hour == DateTime.Now.Hour && this.generatorTime.Minute == DateTime.Now.Minute;
            var systemBusy = _btPersistanceSrv.ReadFinishTipGenerationState();
            if (canStart && !systemBusy)
            {
                _log.Info("Starting to generate tips.");
                Generate();
            }
            this._generatorTimer.Enabled = true;
        }
        protected override void OnStop()
        {
            try
            {
                _log.Info("BettingTips Generator service stopped");
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop BettingTips Generator service", ex);
            }
        }
        private void Generate()
        {
            try
            {
                _log.Info($"Updating database with programs");
                var winnerLineDate = DateTime.Today;
                var winnerLineJson = _btTipsSrv.GetWinnerLineResponse(winnerLineDate, false);
                var marketData = WebApiInvoker.JsonDeserializeDynamic(winnerLineJson);
                var roundId = FetchRoundId(marketData, winnerLineDate);
                _btTipsSrv.SetPrograms(new List<long>() { roundId }, winnerLineDate);
                _log.Info($"Fetching Data from Bankerim...");
                var sportsToParse = new List<SportIds>() { SportIds.Soccer, SportIds.Basketball };
                var repository = _btTipsSrv.ParseWinnerLineResponse(marketData, roundId, winnerLineDate, false, sportsToParse);
                if (repository.SkippedEventsInfo.Count > 0)
                {
                    foreach (var warning in repository.SkippedEventsInfo)
                        _log.Warn(warning);
                }

                var allData = _btTipsSrv.CollectData(repository, 4);
                if (allData.Errors.Count > 0)
                {
                    _log.Debug($"Attention: some events were skipped as bad data encounter, please refer to log");
                    foreach (var e in allData.Errors)
                        _log.Error(e);
                }
                _log.Info($"Generating CSV file...");
                _btPersistanceSrv.CsvDump(allData, repository);
                _log.Info($"Pushing to database...");
                _btTipsSrv.PersistProgramAndTips(allData, repository);
                _btTipsSrv.FinishTipGeneration();
                _log.Info("FinishTipGeneration set to true, update tips wharehouse is ready to start");
            }
            catch (Exception ex)
            {
                _log.Error($"An error occurred trying to generate tips",ex);
            }
        }

        private long FetchRoundId(dynamic marketData, DateTime winnerLineDate)
        {
            var roundId = (long)0;
            var dRounds = marketData["rounds"];
            Console.WriteLine("Rounds found:");
            var rounds = new Dictionary<long, int>(); //#roundid, #games
            Console.ForegroundColor = ConsoleColor.Blue;
            var allProgram = _btTipsSrv.GetAllPrograms();
            foreach (var dRound in dRounds)
            {
                var id = BettingTipsServicesHelpers.DynamicResolver<long>(dRound, null, "id", ResolveEnum.LONG);
                var games = BettingTipsServicesHelpers.DynamicResolver<int>(dRound, null, "games", ResolveEnum.INT);
                var okToAdd = allProgram.Any(p => p.ProgramId == id && p.ProgramDate == winnerLineDate) ||
                    allProgram.All(p => p.ProgramId != id);
                var okToAddStr = "[EXISTS]";
                if (okToAdd)
                {
                    okToAddStr = "[OK to add]";
                    rounds.Add(id, games);
                }
                _log.Info($"{id} has {games} games {okToAddStr}");
            }
            var maxGames = rounds.Max(g => g.Value);
            roundId = rounds.Where(r => r.Value == maxGames).First().Key;
            _log.Info($"Chosen Round Id: {roundId} with {maxGames} games");
            return roundId;
        }
    }
}
