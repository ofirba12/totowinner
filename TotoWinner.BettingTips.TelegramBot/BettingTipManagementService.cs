﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    //partial class BettingTipManagementService : ServiceBase
    //{
    //    public BettingTipManagementService()
    //    {
    //        InitializeComponent();
    //    }

    //    protected override void OnStart(string[] args)
    //    {
    //        // TODO: Add code here to start your service.
    //    }

    //    protected override void OnStop()
    //    {
    //        // TODO: Add code here to perform any tear-down necessary to stop your service.
    //    }
    //}

    public partial class BettingTipManagementService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger("BettingTipsManagement");
        private BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        private System.Timers.Timer _updateResultsTimer = null;

        public BettingTipManagementService()
        {
            InitializeComponent();
            this.ServiceName = "BettingTipsManagementService";
        }
        protected override void OnStart(string[] args)
        {
            _log.Info("BettingTips Management service started");
            try
            {
                this._updateResultsTimer = new System.Timers.Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
                this._updateResultsTimer.Enabled = true;
                this._updateResultsTimer.AutoReset = true;
                this._updateResultsTimer.Elapsed += _updateResultsTimer_Elapsed;

            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start BettingTips Management service", ex);
            }
        }

        private void _updateResultsTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                this._updateResultsTimer.Enabled = false;
                _btTipsSrv.SetCloseStatusForCandidateTips();
                var canStart = _btPersistanceSrv.ReadFinishTipGenerationState();
                if (canStart)
                {
                    _log.Info("Starting to Collect Tips And Update Results.");
                    var tipWithNoResults = _btTipsSrv.GetAllTipsWithNoResults();
                    _log.Info($"Found {tipWithNoResults.Count} games with no results");
                    foreach (var tip in tipWithNoResults)
                        _log.Info($"Candidate Tip with no result found ProgramId=[{tip.ProgramId}], GameNumber=[{tip.GameIndex}]");
                    _btTipsSrv.CollectTipsAndUpdateResults(tipWithNoResults);
                    _btPersistanceSrv.UpdateFinishTipGenerationState(false);
                    _log.Info("Update FinishTipGeneration to false");
                }
                this._updateResultsTimer.Enabled = true;
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to update tips status or results", ex);
                this._updateResultsTimer.Enabled = true;
                _btPersistanceSrv.UpdateFinishTipGenerationState(false);
            }
        }

        protected override void OnStop()
        {
            try
            {
                _log.Info("BettingTips Management service stopped");
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop BettingTips Management service", ex);
            }
        }
    }
}
