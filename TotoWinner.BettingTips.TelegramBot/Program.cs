﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TotoWinner.BettingTips.TelegramBot
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] servicesToRun;
#if !DEBUG
            servicesToRun = new ServiceBase[]
            {
                new TelegramBotService(),
                new BettingTipManagementService(),
                new BettingTipsGeneratorService()
            };
#else
            servicesToRun = new ServiceBase[]
            {
                new TelegramBotService()
//                new BettingTipsGeneratorService()
            };
#endif
            //To debug Environment.UserInteractive, change the project output type to Console Application
            //To Check in, change the project output type to Window Application
            if (Environment.UserInteractive)
            {
                RunInteractive(servicesToRun);
            }
            else
            {
                MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart",
                    BindingFlags.Instance | BindingFlags.NonPublic);
                onStartMethod.Invoke(servicesToRun[1], new object[] { new string[] { } });
                onStartMethod.Invoke(servicesToRun[2], new object[] { new string[] { } });
                ServiceBase.Run(servicesToRun);
            }
        }
        static void RunInteractive(ServiceBase[] servicesToRun)
        {
            Console.WriteLine("Services running in interactive mode.");
            Console.WriteLine();
            MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart",
                BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (ServiceBase service in servicesToRun)
            {
                Console.WriteLine("Starting {0}...", service.ServiceName);
                onStartMethod.Invoke(service, new object[] { new string[] { } });
                Console.Write("Started");
            }
            Console.WriteLine();
            Console.WriteLine();
            //Console.WriteLine("Press any key to stop the services and end the process...");
            //Console.Read();
            //Console.WriteLine();
            //MethodInfo onStopMethod = typeof(ServiceBase).GetMethod("OnStop",
            //    BindingFlags.Instance | BindingFlags.NonPublic);
            //foreach (ServiceBase service in servicesToRun)
            //{
            //    Console.WriteLine("Stopping {0}...", service.ServiceName);
            //    onStopMethod.Invoke(service, null);
            //    Console.WriteLine("Stopped");
            //}
            //Console.WriteLine("All services stopped.");
            // Keep the console alive for a second to allow the user to see the message.
            Thread.Sleep(-1);
        }
    }
}
