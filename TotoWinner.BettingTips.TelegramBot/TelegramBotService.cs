﻿using log4net;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.TelegramBot
{
    public partial class TelegramBotService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger("TelegramBot");
        private TelegramBotServices _botSrv = TelegramBotServices.Instance;
        private TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;
        static TelegramBotClient _botClient;
        private System.Timers.Timer _autoPushTipTimer = null;
        private System.Timers.Timer _lookForVisitedTips = null;
        private string _botToken;
        private readonly object timersLock = new object();

        public TelegramBotService()
        {
            InitializeComponent();
            this.ServiceName = "TelegramBotService";
            _botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
            _botClient = new TelegramBotClient(_botToken);
        }

        protected override async void OnStart(string[] args)
        {
            _log.Info("TelegramBot service started");
            try
            {
                this._autoPushTipTimer = new System.Timers.Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
                this._autoPushTipTimer.Enabled = true;
                this._autoPushTipTimer.AutoReset = true;
                this._autoPushTipTimer.Elapsed += _pushWatcher_Elapsed;

                this._lookForVisitedTips = new System.Timers.Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
                this._lookForVisitedTips.Enabled = true;
                this._lookForVisitedTips.AutoReset = true;
                this._lookForVisitedTips.Elapsed += _lookForVisitedTips_Elapsed;

                var cts = new CancellationTokenSource();

                // StartReceiving does not block the caller thread. Receiving is done on the ThreadPool.
                var receiverOptions = new ReceiverOptions
                {
                    AllowedUpdates = { } // receive all update types
                };
                _botClient.StartReceiving(
                    BotHandlers.HandleUpdateAsync,
                    BotHandlers.HandleErrorAsync,
                    receiverOptions,
                    cancellationToken: cts.Token);

                var me = await _botClient.GetMeAsync();

                Console.WriteLine($"Start listening for @{me.Username}");
                Thread.Sleep(-1);

                // Send cancellation request to stop bot
                cts.Cancel();

                //_botClient.OnMessage += BotClient_OnMessage;
                //_botClient.OnCallbackQuery += BotClient_OnCallbackQuery;
                //_botClient.OnReceiveGeneralError += _botClient_OnReceiveGeneralError;
                //_botClient.StartReceiving();
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start TelegramBot service", ex);
            }
        }

        private void _lookForVisitedTips_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock (timersLock)
                {
                    this._lookForVisitedTips.Enabled = false;
                    var urls = _botPersistanceSrv.GetVisitedSubscribersUrls();
                    var subscribers = urls.Select(u => u.SubscriberId).Distinct();
                    foreach (var url in urls)
                    {
                        _log.Info($"UpdateVisitedUrlPickedByChatServer UrlId={url.UrlId}");
                        _botPersistanceSrv.UpdateVisitedUrlPickedByChatServer(url.UrlId);
                    }
                    foreach (var subscriber in subscribers)
                    {
                        _log.Info($"Start Chart with subscriber={subscriber} after url visit");
                        StartChat(subscriber);
                    }
                    this._lookForVisitedTips.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start TelegramBot service", ex);
                this._lookForVisitedTips.Enabled = true;
            }
        }
        private async void StartChat(long subscriber)
        {
            try
            {
                var tip = _botSrv.FindTipToPublish(subscriber);
                if (tip != null)
                {
                    var start = new DoYouWantMoreTips(_botClient, subscriber);
                    await start.Show();
                    ChatRepository.AddChatItem(null, start.MessageId.Value, start);
                }
                else
                {
                    SendMessage(subscriber, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*");
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to start chat with subscriber {subscriber}", ex);
            }
        }
        private void _pushWatcher_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock (timersLock)
                {
                    this._autoPushTipTimer.Enabled = false;
                    this._lookForVisitedTips.Enabled = false;
                    var pushTime = _botSrv.GetPushTipTimes();
                    var pushTips = pushTime.Any(t => t.Hour == DateTime.Now.Hour && t.Minute == DateTime.Now.Minute);
                    var isFirstPush = pushTime.FirstOrDefault()?.Hour == DateTime.Now.Hour && pushTime.FirstOrDefault()?.Minute == DateTime.Now.Minute;
                    if (pushTips)
                    {
                        var subsribers = _botSrv.GetSubsribers(true);
                        foreach (var client in subsribers)
                        {
                            var tipToPublish = _botSrv.FindTipToPublish(client.ClientChatId.Value);
                            var fullTipDescription = _botSrv.PrepareTipForPublish(tipToPublish);
                            if (!string.IsNullOrEmpty(fullTipDescription))
                            {
                                if (isFirstPush)
                                    SendMessage(client.ClientChatId.Value, $"*בוקר טוב {client.FirstName}*");
                                var urlGuid = Guid.NewGuid();
                                var tipDescription = fullTipDescription.Replace("[#GUID#]", urlGuid.ToString());
                                _botPersistanceSrv.InsertSubscriberTip(urlGuid, tipToPublish.TipId, client.ClientChatId.Value);

                                SendMessage(client.ClientChatId.Value, tipDescription);
                            }
                        }
                    }
                    this._autoPushTipTimer.Enabled = true;
                    this._lookForVisitedTips.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to watch for push messages", ex);
                this._autoPushTipTimer.Enabled = true;
                this._lookForVisitedTips.Enabled = true;
            }
        }

        private async void SendMessage(long chatId, string message)
        {
            try
            {
                Task.Delay(1000).Wait();
                await _botClient.SendTextMessageAsync(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Markdown);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to send message to chatId:{chatId}, message:{Environment.NewLine}{message}", ex);
            }
        }
        protected override void OnStop()
        {
            try
            {
                _log.Info("TelegramBot service stopped");
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop TelegramBot service", ex);
            }
        }

    }
}
