﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using BG.ExchangeTrading.Services.Persistance;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Linq;
using System.ServiceProcess;
using System.Timers;

namespace BG.ExchangeTrading.Worker
{
    public partial class BGExchangeTradingService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Timer timerEodStockFetcher = new Timer();
        private static Timer timerLiveStockDataFetcher = new Timer();
        private static int eofFetcherStartHour = 5;
        private static int eofFetcherStartMinute = 0;
        private static readonly List<DayOfWeek> eofDays = new List<DayOfWeek>()
        { DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday };
        private static readonly List<DayOfWeek> liveDataDays = new List<DayOfWeek>()
        { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };
        private static readonly List<int> liveDataHours = new List<int>()
        { 15, 16, 17, 18, 19, 20, 21, 22, 23 };
        private static readonly List<int> movingAverageRuler = new List<int>()
        { 50,75,100,125,150,175,200 };
        private static readonly List<int> buyPoints = new List<int>()
        { -10,-15,-20,-25,-30,-35,-40 };
        private static readonly List<int> sellPoints = new List<int>()
        { 10,15,20,25,30,35,40 };
        public BGExchangeTradingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
#if DEBUG
            DebugAndSaveCalcMa();
#else
            timerEodStockFetcher.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
            timerEodStockFetcher.Enabled = true;
            timerEodStockFetcher.Elapsed += FetchEodStockData;
            timerLiveStockDataFetcher.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
            timerLiveStockDataFetcher.Enabled = true;
            timerLiveStockDataFetcher.Elapsed += FetchLiveData;
#endif
        }
        private static void DebugAndSaveCalcMa()
        {
            try
            {
                var exitingTickers = TickerServices.Instance.GetExistingTickersWithDetails();// GetExistingTickersList();
                var movingAverageSuccessRates = new List<MovingAverageSuccessRate>();
                foreach (var ticker in exitingTickers)
                {
                    var data = TickersPersistanceServices.Instance.GetTickerData(ticker.TickerId);
                    var lastEofDate = data.OrderByDescending(d => d.Date).First();
                    var allData = data
                        .ConvertAll<StockPrice>(s => new StockPrice(s.Date, s.OpenPrice, s.ClosePrice, s.High, s.Low, s.Volume));
                    var fullData = TickerServices.Instance.CalculateEofData(allData);
                    if (ticker.Buffer.HasValue)
                    {
                        try
                        {
                            var stockPrices = fullData.ConvertAll<StockPrice>(s =>
                                new StockPrice(s.Date, s.OpenPrice, s.ClosePrice, s.High, s.Low, s.Volume));
                            var heatmapData = TickerServices.Instance.GenerateHeatMapData(stockPrices, movingAverageRuler);
                            var heatmap = TickerServices.Instance.GenerateHeatMapRepository(heatmapData,
                                buyPoints,
                                sellPoints,
                                movingAverageRuler,
                                (int)ticker.Buffer.Value);
                            var movingAverageSuccessRate = TickerServices.Instance.CalulateMAs(ticker.TickerId,
                                fullData.First(),
                                heatmap.Summary,
                                movingAverageRuler);
                            movingAverageSuccessRates.AddRange(movingAverageSuccessRate);
                            //TickersPersistanceServices.Instance.AddCalculatedData(movingAverageSuccessRates);
                        }
                        catch (Exception ex)
                        {
                            _log.Fatal($"failed with ticker {ticker.TickerId} {ticker.Symbol}", ex);
                        }
                    }
                    movingAverageSuccessRates.Clear();
                }
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
        }
        private static async void FetchEodStockData(object sender, ElapsedEventArgs e)
        {
            timerEodStockFetcher.Enabled = false;
            try
            {
                if (eofDays.Contains(DateTime.Today.DayOfWeek) &&
                    DateTime.Now.Hour == eofFetcherStartHour &&
                    DateTime.Now.Minute == eofFetcherStartMinute)
                {
                    var exitingTickers = TickerServices.Instance.GetExistingTickersWithDetails();// GetExistingTickersList();
                    var movingAverageSuccessRates = new List<MovingAverageSuccessRate>();
                    foreach (var ticker in exitingTickers)
                    {
                        var data = TickersPersistanceServices.Instance.GetTickerData(ticker.TickerId);
                        var lastEofDate = data.OrderByDescending(d => d.Date).First();
                        var fullSymbol = TickerServices.Instance.GetFullSymbol(ticker.Symbol);
                        var historicalStockPrice = await TickerServices.Instance.FetchEofStockPrice(fullSymbol,
                            lastEofDate.Date.AddDays(1), DateTime.Today);
                        if (historicalStockPrice.Count > 0)
                        {
                            var allData = data
                                .ConvertAll<StockPrice>(s => new StockPrice(s.Date, s.OpenPrice, s.ClosePrice, s.High, s.Low, s.Volume));
                            var newData = historicalStockPrice.ConvertAll<StockPrice>(s => new StockPrice(s.Date, s.OpenPrice, s.ClosePrice, s.High, s.Low, s.Volume));
                            allData.AddRange(newData);
                            var fullData = TickerServices.Instance.CalculateEofData(allData);
                            TickersPersistanceServices.Instance.AddHistoricalData(ticker.TickerId, fullData);
                            if (ticker.Buffer.HasValue)
                            {
                                var mvSuccessRates = CreateMVSuccessRatesCollection(ticker, fullData);
                                movingAverageSuccessRates.AddRange(mvSuccessRates);

                            }
                        }
                    }
                    TickersPersistanceServices.Instance.AddCalculatedData(movingAverageSuccessRates);
                }
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                timerEodStockFetcher.Enabled = true;
            }
        }

        private static List<MovingAverageSuccessRate> CreateMVSuccessRatesCollection(BGTickerDetails ticker, List<StockEodData> fullData)
        {
            try
            {
                var stockPrices = fullData.ConvertAll<StockPrice>(s =>
                    new StockPrice(s.Date, s.OpenPrice, s.ClosePrice, s.High, s.Low, s.Volume));
                var heatmapData = TickerServices.Instance.GenerateHeatMapData(stockPrices, movingAverageRuler);
                var heatmap = TickerServices.Instance.GenerateHeatMapRepository(heatmapData,
                    buyPoints,
                    sellPoints,
                    movingAverageRuler,
                    (int)ticker.Buffer.Value);
                var movingAverageSuccessRate = TickerServices.Instance.CalulateMAs(ticker.TickerId,
                    fullData.First(),
                    heatmap.Summary,
                    movingAverageRuler);
                return movingAverageSuccessRate;
            }
            catch(Exception ex)
            {
                _log.Fatal($"Failed to create MV success rates for ticker {ticker.TickerId} {ticker.Symbol}", ex);
            }
            return new List<MovingAverageSuccessRate>();
        }

        private static async void FetchLiveData(object sender, ElapsedEventArgs e)
        {
            timerLiveStockDataFetcher.Enabled = false;
            try
            {
                if (liveDataDays.Contains(DateTime.Today.DayOfWeek) &&
                    DateTime.Now.Minute == 0 &&
                    liveDataHours.Contains(DateTime.Now.Hour)
                    )
                {
                    var exitingTickers = TickerServices.Instance.GetExistingTickersList();
                    var liveData = new List<StockLiveData>();
                    foreach (var ticker in exitingTickers)
                    {
                        var lastUpdate = DateTime.Now;
                        var fullSymbol = TickerServices.Instance.GetFullSymbol(ticker.Symbol);
                        var spot = await TickerServices.Instance.FetchLiveStockPrice(fullSymbol);
                        if (spot.HasValue)
                            liveData.Add(new StockLiveData(ticker.TickerId, spot.Value, lastUpdate));
                    }
                    if (liveData.Count > 0)
                    {
                        TickersPersistanceServices.Instance.UpdateLiveData(liveData);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                timerLiveStockDataFetcher.Enabled = true;
            }
        }
        protected override void OnStop()
        {
            try
            {
                _log.Info("BGExchangeTrading service stopped");
                timerEodStockFetcher.Stop();
                timerEodStockFetcher.Close();
                timerLiveStockDataFetcher.Stop();
                timerLiveStockDataFetcher.Close();
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop BGExchangeTrading service", ex);
            }
        }
    }
}
