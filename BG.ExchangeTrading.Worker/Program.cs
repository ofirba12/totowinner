﻿using System;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

namespace BG.ExchangeTrading.Worker
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] servicesToRun;
            servicesToRun = new ServiceBase[]
            {
                new BGExchangeTradingService()
            };
            //To debug Environment.UserInteractive, change the project output type to Console Application
            //To Check in, change the project output type to Window Application
            if (Environment.UserInteractive)
            {
                RunInteractive(servicesToRun);
            }
            else
            {
                MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart",
                    BindingFlags.Instance | BindingFlags.NonPublic);
                ServiceBase.Run(servicesToRun);
            }
        }
        static void RunInteractive(ServiceBase[] servicesToRun)
        {
            Console.WriteLine("Services running in interactive mode.");
            Console.WriteLine();
            MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart",
                BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (ServiceBase service in servicesToRun)
            {
                Console.WriteLine("Starting {0}...", service.ServiceName);
                onStartMethod.Invoke(service, new object[] { new string[] { } });
                Console.Write("Started");
            }
            Console.WriteLine();
            Console.WriteLine();

            Thread.Sleep(-1);
        }
    }
}
