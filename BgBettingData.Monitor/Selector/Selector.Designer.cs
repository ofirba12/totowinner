﻿namespace BgBettingData.Monitor
{
    partial class Selector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.goalserveButton = new MaterialSkin.Controls.MaterialButton();
            this.bgMapperButton = new MaterialSkin.Controls.MaterialButton();
            this.buttonBgData = new MaterialSkin.Controls.MaterialButton();
            this.manualFeedButton = new MaterialSkin.Controls.MaterialButton();
            this.buttonTeamsWatcher = new MaterialSkin.Controls.MaterialButton();
            this.buttonTeamABAnalysis = new MaterialSkin.Controls.MaterialButton();
            this.groupBoxProviders = new System.Windows.Forms.GroupBox();
            this.winnerButton = new MaterialSkin.Controls.MaterialButton();
            this.buttonBgBlackFilter = new MaterialSkin.Controls.MaterialButton();
            this.groupBoxBgData = new System.Windows.Forms.GroupBox();
            this.buttonWinnerBgData = new MaterialSkin.Controls.MaterialButton();
            this.groupBoxAnalysis = new System.Windows.Forms.GroupBox();
            this.buttonBettingData = new MaterialSkin.Controls.MaterialButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonLeaguesWhitelist = new MaterialSkin.Controls.MaterialButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.winnerMapperButton = new MaterialSkin.Controls.MaterialButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonBankerim = new MaterialSkin.Controls.MaterialButton();
            this.groupBoxProviders.SuspendLayout();
            this.groupBoxBgData.SuspendLayout();
            this.groupBoxAnalysis.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // goalserveButton
            // 
            this.goalserveButton.AutoSize = false;
            this.goalserveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.goalserveButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.goalserveButton.Depth = 0;
            this.goalserveButton.HighEmphasis = true;
            this.goalserveButton.Icon = null;
            this.goalserveButton.Location = new System.Drawing.Point(7, 22);
            this.goalserveButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.goalserveButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.goalserveButton.Name = "goalserveButton";
            this.goalserveButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.goalserveButton.Size = new System.Drawing.Size(142, 70);
            this.goalserveButton.TabIndex = 0;
            this.goalserveButton.Text = "Goalserve Feed";
            this.goalserveButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.goalserveButton.UseAccentColor = false;
            this.goalserveButton.UseVisualStyleBackColor = true;
            this.goalserveButton.Click += new System.EventHandler(this.goalserveButton_Click);
            // 
            // bgMapperButton
            // 
            this.bgMapperButton.AutoSize = false;
            this.bgMapperButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bgMapperButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.bgMapperButton.Depth = 0;
            this.bgMapperButton.HighEmphasis = true;
            this.bgMapperButton.Icon = null;
            this.bgMapperButton.Location = new System.Drawing.Point(25, 104);
            this.bgMapperButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bgMapperButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.bgMapperButton.Name = "bgMapperButton";
            this.bgMapperButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.bgMapperButton.Size = new System.Drawing.Size(142, 70);
            this.bgMapperButton.TabIndex = 1;
            this.bgMapperButton.Text = "Mapper";
            this.bgMapperButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.bgMapperButton.UseAccentColor = false;
            this.bgMapperButton.UseVisualStyleBackColor = true;
            this.bgMapperButton.Click += new System.EventHandler(this.bgMapperButton_Click);
            // 
            // buttonBgData
            // 
            this.buttonBgData.AutoSize = false;
            this.buttonBgData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBgData.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonBgData.Depth = 0;
            this.buttonBgData.HighEmphasis = true;
            this.buttonBgData.Icon = null;
            this.buttonBgData.Location = new System.Drawing.Point(7, 22);
            this.buttonBgData.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonBgData.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonBgData.Name = "buttonBgData";
            this.buttonBgData.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonBgData.Size = new System.Drawing.Size(142, 70);
            this.buttonBgData.TabIndex = 2;
            this.buttonBgData.Text = "BG Data";
            this.buttonBgData.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonBgData.UseAccentColor = false;
            this.buttonBgData.UseVisualStyleBackColor = true;
            this.buttonBgData.Click += new System.EventHandler(this.buttonBgData_Click);
            // 
            // manualFeedButton
            // 
            this.manualFeedButton.AutoSize = false;
            this.manualFeedButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.manualFeedButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.manualFeedButton.Depth = 0;
            this.manualFeedButton.HighEmphasis = true;
            this.manualFeedButton.Icon = null;
            this.manualFeedButton.Location = new System.Drawing.Point(25, 22);
            this.manualFeedButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.manualFeedButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.manualFeedButton.Name = "manualFeedButton";
            this.manualFeedButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.manualFeedButton.Size = new System.Drawing.Size(142, 70);
            this.manualFeedButton.TabIndex = 3;
            this.manualFeedButton.Text = "Manual Update";
            this.manualFeedButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.manualFeedButton.UseAccentColor = false;
            this.manualFeedButton.UseVisualStyleBackColor = true;
            this.manualFeedButton.Click += new System.EventHandler(this.manualFeedButton_Click);
            // 
            // buttonTeamsWatcher
            // 
            this.buttonTeamsWatcher.AutoSize = false;
            this.buttonTeamsWatcher.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonTeamsWatcher.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonTeamsWatcher.Depth = 0;
            this.buttonTeamsWatcher.HighEmphasis = true;
            this.buttonTeamsWatcher.Icon = null;
            this.buttonTeamsWatcher.Location = new System.Drawing.Point(7, 104);
            this.buttonTeamsWatcher.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonTeamsWatcher.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonTeamsWatcher.Name = "buttonTeamsWatcher";
            this.buttonTeamsWatcher.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonTeamsWatcher.Size = new System.Drawing.Size(142, 70);
            this.buttonTeamsWatcher.TabIndex = 5;
            this.buttonTeamsWatcher.Text = "Teams Watcher";
            this.buttonTeamsWatcher.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonTeamsWatcher.UseAccentColor = false;
            this.buttonTeamsWatcher.UseVisualStyleBackColor = true;
            this.buttonTeamsWatcher.Click += new System.EventHandler(this.buttonTeamsWatcher_Click);
            // 
            // buttonTeamABAnalysis
            // 
            this.buttonTeamABAnalysis.AutoSize = false;
            this.buttonTeamABAnalysis.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonTeamABAnalysis.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonTeamABAnalysis.Depth = 0;
            this.buttonTeamABAnalysis.HighEmphasis = true;
            this.buttonTeamABAnalysis.Icon = null;
            this.buttonTeamABAnalysis.Location = new System.Drawing.Point(7, 21);
            this.buttonTeamABAnalysis.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonTeamABAnalysis.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonTeamABAnalysis.Name = "buttonTeamABAnalysis";
            this.buttonTeamABAnalysis.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonTeamABAnalysis.Size = new System.Drawing.Size(142, 70);
            this.buttonTeamABAnalysis.TabIndex = 6;
            this.buttonTeamABAnalysis.Text = "Teams Analysis";
            this.buttonTeamABAnalysis.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonTeamABAnalysis.UseAccentColor = false;
            this.buttonTeamABAnalysis.UseVisualStyleBackColor = true;
            this.buttonTeamABAnalysis.Click += new System.EventHandler(this.buttonTeamABAnalysis_Click);
            // 
            // groupBoxProviders
            // 
            this.groupBoxProviders.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxProviders.Controls.Add(this.winnerButton);
            this.groupBoxProviders.Controls.Add(this.goalserveButton);
            this.groupBoxProviders.Location = new System.Drawing.Point(19, 78);
            this.groupBoxProviders.Name = "groupBoxProviders";
            this.groupBoxProviders.Size = new System.Drawing.Size(170, 284);
            this.groupBoxProviders.TabIndex = 7;
            this.groupBoxProviders.TabStop = false;
            this.groupBoxProviders.Text = "Data Sources";
            // 
            // winnerButton
            // 
            this.winnerButton.AutoSize = false;
            this.winnerButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.winnerButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.winnerButton.Depth = 0;
            this.winnerButton.HighEmphasis = true;
            this.winnerButton.Icon = null;
            this.winnerButton.Location = new System.Drawing.Point(7, 104);
            this.winnerButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.winnerButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.winnerButton.Name = "winnerButton";
            this.winnerButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.winnerButton.Size = new System.Drawing.Size(142, 70);
            this.winnerButton.TabIndex = 9;
            this.winnerButton.Text = "Winner Feed";
            this.winnerButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.winnerButton.UseAccentColor = false;
            this.winnerButton.UseVisualStyleBackColor = true;
            this.winnerButton.Click += new System.EventHandler(this.winnerButton_Click);
            // 
            // buttonBgBlackFilter
            // 
            this.buttonBgBlackFilter.AutoSize = false;
            this.buttonBgBlackFilter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBgBlackFilter.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonBgBlackFilter.Depth = 0;
            this.buttonBgBlackFilter.HighEmphasis = true;
            this.buttonBgBlackFilter.Icon = null;
            this.buttonBgBlackFilter.Location = new System.Drawing.Point(25, 186);
            this.buttonBgBlackFilter.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonBgBlackFilter.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonBgBlackFilter.Name = "buttonBgBlackFilter";
            this.buttonBgBlackFilter.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonBgBlackFilter.Size = new System.Drawing.Size(142, 70);
            this.buttonBgBlackFilter.TabIndex = 8;
            this.buttonBgBlackFilter.Text = "Bg Black Filters";
            this.buttonBgBlackFilter.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonBgBlackFilter.UseAccentColor = false;
            this.buttonBgBlackFilter.UseVisualStyleBackColor = true;
            this.buttonBgBlackFilter.Click += new System.EventHandler(this.buttonBgBlackFilter_Click);
            // 
            // groupBoxBgData
            // 
            this.groupBoxBgData.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxBgData.Controls.Add(this.buttonWinnerBgData);
            this.groupBoxBgData.Controls.Add(this.buttonBgData);
            this.groupBoxBgData.Controls.Add(this.buttonTeamsWatcher);
            this.groupBoxBgData.Location = new System.Drawing.Point(387, 78);
            this.groupBoxBgData.Name = "groupBoxBgData";
            this.groupBoxBgData.Size = new System.Drawing.Size(165, 270);
            this.groupBoxBgData.TabIndex = 8;
            this.groupBoxBgData.TabStop = false;
            this.groupBoxBgData.Text = "Bg Data";
            // 
            // buttonWinnerBgData
            // 
            this.buttonWinnerBgData.AutoSize = false;
            this.buttonWinnerBgData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonWinnerBgData.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonWinnerBgData.Depth = 0;
            this.buttonWinnerBgData.HighEmphasis = true;
            this.buttonWinnerBgData.Icon = null;
            this.buttonWinnerBgData.Location = new System.Drawing.Point(7, 186);
            this.buttonWinnerBgData.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonWinnerBgData.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonWinnerBgData.Name = "buttonWinnerBgData";
            this.buttonWinnerBgData.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonWinnerBgData.Size = new System.Drawing.Size(142, 70);
            this.buttonWinnerBgData.TabIndex = 6;
            this.buttonWinnerBgData.Text = "Winner && BGDATA";
            this.buttonWinnerBgData.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonWinnerBgData.UseAccentColor = false;
            this.buttonWinnerBgData.UseVisualStyleBackColor = true;
            this.buttonWinnerBgData.Click += new System.EventHandler(this.buttonWinnerBgData_Click);
            // 
            // groupBoxAnalysis
            // 
            this.groupBoxAnalysis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxAnalysis.Controls.Add(this.buttonBettingData);
            this.groupBoxAnalysis.Controls.Add(this.buttonTeamABAnalysis);
            this.groupBoxAnalysis.Location = new System.Drawing.Point(556, 78);
            this.groupBoxAnalysis.Name = "groupBoxAnalysis";
            this.groupBoxAnalysis.Size = new System.Drawing.Size(161, 186);
            this.groupBoxAnalysis.TabIndex = 9;
            this.groupBoxAnalysis.TabStop = false;
            this.groupBoxAnalysis.Text = "Analysis";
            // 
            // buttonBettingData
            // 
            this.buttonBettingData.AutoSize = false;
            this.buttonBettingData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBettingData.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonBettingData.Depth = 0;
            this.buttonBettingData.HighEmphasis = true;
            this.buttonBettingData.Icon = null;
            this.buttonBettingData.Location = new System.Drawing.Point(7, 103);
            this.buttonBettingData.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonBettingData.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonBettingData.Name = "buttonBettingData";
            this.buttonBettingData.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonBettingData.Size = new System.Drawing.Size(142, 70);
            this.buttonBettingData.TabIndex = 7;
            this.buttonBettingData.Text = "Betting Data";
            this.buttonBettingData.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonBettingData.UseAccentColor = false;
            this.buttonBettingData.UseVisualStyleBackColor = true;
            this.buttonBettingData.Click += new System.EventHandler(this.buttonBettingData_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.buttonLeaguesWhitelist);
            this.groupBox1.Location = new System.Drawing.Point(723, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 106);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Site Management";
            // 
            // buttonLeaguesWhitelist
            // 
            this.buttonLeaguesWhitelist.AutoSize = false;
            this.buttonLeaguesWhitelist.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonLeaguesWhitelist.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonLeaguesWhitelist.Depth = 0;
            this.buttonLeaguesWhitelist.HighEmphasis = true;
            this.buttonLeaguesWhitelist.Icon = null;
            this.buttonLeaguesWhitelist.Location = new System.Drawing.Point(7, 21);
            this.buttonLeaguesWhitelist.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonLeaguesWhitelist.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonLeaguesWhitelist.Name = "buttonLeaguesWhitelist";
            this.buttonLeaguesWhitelist.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonLeaguesWhitelist.Size = new System.Drawing.Size(142, 70);
            this.buttonLeaguesWhitelist.TabIndex = 2;
            this.buttonLeaguesWhitelist.Text = "Leagues Whitelist";
            this.buttonLeaguesWhitelist.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonLeaguesWhitelist.UseAccentColor = false;
            this.buttonLeaguesWhitelist.UseVisualStyleBackColor = true;
            this.buttonLeaguesWhitelist.Click += new System.EventHandler(this.buttonLeaguesWhitelist_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Controls.Add(this.winnerMapperButton);
            this.groupBox2.Controls.Add(this.manualFeedButton);
            this.groupBox2.Controls.Add(this.bgMapperButton);
            this.groupBox2.Controls.Add(this.buttonBgBlackFilter);
            this.groupBox2.Location = new System.Drawing.Point(195, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(186, 366);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Generation";
            // 
            // winnerMapperButton
            // 
            this.winnerMapperButton.AutoSize = false;
            this.winnerMapperButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.winnerMapperButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.winnerMapperButton.Depth = 0;
            this.winnerMapperButton.HighEmphasis = true;
            this.winnerMapperButton.Icon = null;
            this.winnerMapperButton.Location = new System.Drawing.Point(25, 268);
            this.winnerMapperButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.winnerMapperButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.winnerMapperButton.Name = "winnerMapperButton";
            this.winnerMapperButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.winnerMapperButton.Size = new System.Drawing.Size(142, 70);
            this.winnerMapperButton.TabIndex = 9;
            this.winnerMapperButton.Text = "Mapper Winner";
            this.winnerMapperButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.winnerMapperButton.UseAccentColor = false;
            this.winnerMapperButton.UseVisualStyleBackColor = true;
            this.winnerMapperButton.Click += new System.EventHandler(this.winnerMapperButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Controls.Add(this.buttonBankerim);
            this.groupBox3.Location = new System.Drawing.Point(890, 78);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(161, 106);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bankerim";
            // 
            // buttonBankerim
            // 
            this.buttonBankerim.AutoSize = false;
            this.buttonBankerim.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBankerim.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonBankerim.Depth = 0;
            this.buttonBankerim.HighEmphasis = true;
            this.buttonBankerim.Icon = null;
            this.buttonBankerim.Location = new System.Drawing.Point(7, 21);
            this.buttonBankerim.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonBankerim.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonBankerim.Name = "buttonBankerim";
            this.buttonBankerim.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonBankerim.Size = new System.Drawing.Size(142, 70);
            this.buttonBankerim.TabIndex = 2;
            this.buttonBankerim.Text = "Winner && BGDATA Tips";
            this.buttonBankerim.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonBankerim.UseAccentColor = false;
            this.buttonBankerim.UseVisualStyleBackColor = true;
            this.buttonBankerim.Click += new System.EventHandler(this.buttonBankerim_Click);
            // 
            // Selector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 462);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxAnalysis);
            this.Controls.Add(this.groupBoxBgData);
            this.Controls.Add(this.groupBoxProviders);
            this.Name = "Selector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bg Data Selector";
            this.groupBoxProviders.ResumeLayout(false);
            this.groupBoxBgData.ResumeLayout(false);
            this.groupBoxAnalysis.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialButton goalserveButton;
        private MaterialSkin.Controls.MaterialButton bgMapperButton;
        private MaterialSkin.Controls.MaterialButton buttonBgData;
        private MaterialSkin.Controls.MaterialButton manualFeedButton;
        private MaterialSkin.Controls.MaterialButton buttonTeamsWatcher;
        private MaterialSkin.Controls.MaterialButton buttonTeamABAnalysis;
        private System.Windows.Forms.GroupBox groupBoxProviders;
        private MaterialSkin.Controls.MaterialButton buttonBgBlackFilter;
        private System.Windows.Forms.GroupBox groupBoxBgData;
        private System.Windows.Forms.GroupBox groupBoxAnalysis;
        private MaterialSkin.Controls.MaterialButton buttonBettingData;
        private System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialButton buttonLeaguesWhitelist;
        private MaterialSkin.Controls.MaterialButton winnerButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private MaterialSkin.Controls.MaterialButton winnerMapperButton;
        private MaterialSkin.Controls.MaterialButton buttonWinnerBgData;
        private System.Windows.Forms.GroupBox groupBox3;
        private MaterialSkin.Controls.MaterialButton buttonBankerim;
    }
}