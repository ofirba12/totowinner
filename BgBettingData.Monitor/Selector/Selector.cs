﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;

namespace BgBettingData.Monitor
{
    public partial class Selector : MaterialForm
    {
        public Selector()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

        }

        private void goalserveButton_Click(object sender, EventArgs e)
        {
            var goalserveDashboard = new GoalserveDashboard();
            goalserveDashboard.Show();
        }

        private void bgMapperButton_Click(object sender, EventArgs e)
        {
            var mapper = new Mapper();
            mapper.Show();
        }

        private void buttonBgData_Click(object sender, EventArgs e)
        {
            var dashboard = new BgDataDashboard();
            dashboard.Show();
        }

        private void manualFeedButton_Click(object sender, EventArgs e)
        {
            var dashboard = new ManualProviderDashboard();
            dashboard.Show();
        }

        private void buttonTeamsWatcher_Click(object sender, EventArgs e)
        {
            var dashboard = new TeamsWatcherDashboard();
            dashboard.Show();
        }

        private void buttonTeamABAnalysis_Click(object sender, EventArgs e)
        {
            var dashboard = new TeamABAnalysisDashboard();
            dashboard.Show();
        }

        private void buttonBgBlackFilter_Click(object sender, EventArgs e)
        {
            var dashboard = new BgBlackFilterDashboard();
            dashboard.Show();
        }

        private void buttonBettingData_Click(object sender, EventArgs e)
        {
            var dashboard = new BettingDataDashboard();
            dashboard.Show();
        }

        private void buttonLeaguesWhitelist_Click(object sender, EventArgs e)
        {
            var dashboard = new LeaguesWhitelistDashboard();
            dashboard.Show();
        }

        private void winnerButton_Click(object sender, EventArgs e)
        {
            var dashboard = new WinnerDashboard();
            dashboard.Show();
        }

        private void winnerMapperButton_Click(object sender, EventArgs e)
        {
            var mapper = new WinnerMapper();
            mapper.Show();
        }

        private void buttonWinnerBgData_Click(object sender, EventArgs e)
        {
            var dashboard = new WinnerBgDataDashboard();
            dashboard.Show();

        }
        private void buttonBankerim_Click(object sender, EventArgs e)
        {
            var dashboard = new BankerimDataDashboard();
            dashboard.Show();
        }
    }
}
