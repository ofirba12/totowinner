﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class Mapper : MaterialForm
    {
        //private List<BgMapperMonitor> bgMapperMonitors = new List<BgMapperMonitor>();
        private MapperRespository Repository;
        private Dictionary<string, int> bgNameRowIndex = new Dictionary<string, int>();
        private DataGridViewCellEventArgs mouseLocation;
        ToolStripMenuItem toolStripUpdateRecord = new ToolStripMenuItem();
        public Mapper()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.dataGridViewMapper.CellMouseEnter += DataGridViewWarehouse_CellMouseEnter;
            materialButtonRefresh_Click(null, null);
        }
        private void DataGridViewWarehouse_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddMapperContextMenu()
        {
            toolStripUpdateRecord.Text = "Update Record";
            toolStripUpdateRecord.Click -= new EventHandler(toolStripUpdateRecord_Click);
            toolStripUpdateRecord.Click += new EventHandler(toolStripUpdateRecord_Click);
            ContextMenuStrip strip = new ContextMenuStrip();
            foreach (DataGridViewRow row in dataGridViewMapper.Rows)
            {
                row.ContextMenuStrip = strip;
                row.ContextMenuStrip.Items.Add(toolStripUpdateRecord);
            }
        }

        private void toolStripUpdateRecord_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewMapper.Rows[mouseLocation.RowIndex];
            var selectedBgId = selectedRow.Cells[0].Value.ToString();
            var selectedBgName = selectedRow.Cells[3].Value.ToString();

            var dialog = new MapperBgNameUpdateDialog(selectedBgId, selectedBgName);
            var dialogresult = dialog.ShowDialog();
            if (dialogresult == DialogResult.OK)
            {
                var beforeBgName = selectedRow.Cells[3].Value.ToString();
                if (beforeBgName != dialog.BgName)
                {
                    var bgId = int.Parse(selectedBgId);
                    this.Repository.BgMapperMonitors.Where(x => x.BgId == bgId).ToList().ForEach(m => m.SetBgName(dialog.BgName));
                    var selectedMapperType = GetSelectedMapperType();
                    SetAutoComplete(this.Repository.BgMapperMonitors.Where(x => x.BgType == selectedMapperType));
                    SetGridWithNewBgName(dialog.BgName, bgId);
                    BgDataPersistanceServices.Instance.UpdateMapperBgName(bgId, dialog.BgName);
                    MonitorUtils.Mapper.ClearCache();
                }
            }
        }

        private void SetGridWithNewBgName(string bgName, int selectedBgId)
        {
            this.bgNameRowIndex.Clear();
            var rowIndex = 0;
            foreach (DataGridViewRow row in this.dataGridViewMapper.Rows)
            {
                if (!row.IsNewRow)
                {
                    var rowBgId = row.Cells[0].Value.ToString();
                    if (rowBgId == selectedBgId.ToString())
                        row.Cells[3].Value = bgName;

                    if (!this.bgNameRowIndex.ContainsKey(bgName))
                        this.bgNameRowIndex.Add(bgName, rowIndex);
                }
                rowIndex++;
            }
        }

        private BgMapperType GetSelectedMapperType()
        {
            if (this.materialRadioButtonCountries.Checked)
                return BgMapperType.Countries;
            else if (this.materialRadioButtonLeagues.Checked)
                return BgMapperType.Leagues;
            return BgMapperType.Teams;
        }
        private void materialRadioButtonCountries_CheckedChanged(object sender, EventArgs e)
        {
            this.textBoxSearch.Clear();
            ShowData(BgMapperType.Countries, (MaterialRadioButton)sender);
        }

        private void materialRadioButtonLeagues_CheckedChanged(object sender, EventArgs e)
        {
            this.textBoxSearch.Clear();
            ShowData(BgMapperType.Leagues, (MaterialRadioButton)sender);
        }
        private void materialRadioButtonTeams_CheckedChanged(object sender, EventArgs e)
        {
            this.textBoxSearch.Clear();
            ShowData(BgMapperType.Teams, (MaterialRadioButton)sender);
        }
        private void ShowData(BgMapperType mapperType, MaterialRadioButton btn)
        {
            if (btn.Checked)
            {
                this.textBoxSearch.Clear();
                this.bgNameRowIndex.Clear();
                this.dataGridViewMapper.Rows.Clear();
                this.dataGridViewMapper.Refresh();
                Type type = typeof(BgMapperMonitor);
                var collection = this.Repository.BgMapperMonitors.Where(x => x.BgType == mapperType);
                SetAutoComplete(collection);
                foreach (var item in collection)
                    FillGrid(item, type);
                AddMapperContextMenu();
                this.dataGridViewMapper.AutoResizeColumns();
                this.dataGridViewMapper.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void SetAutoComplete(IEnumerable<BgMapperMonitor> collection)
        {
            var source = new AutoCompleteStringCollection();
            source.AddRange(collection.Select(m => m.BgName).Distinct().ToArray());
            this.textBoxSearch.AutoCompleteCustomSource = source;
            this.textBoxSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.textBoxSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        private void materialButtonRefresh_Click(object sender, EventArgs e)
        {
            //this.bgMapperMonitors.Clear();
            this.Repository = BgMapperServices.Instance.GetBgMapper();
            this.dataGridViewMapper.Rows.Clear();
            this.dataGridViewMapper.Columns.Clear();
            this.dataGridViewMapper.Refresh();
            BuildGrid(Repository);
            ShowData(BgMapperType.Countries, this.materialRadioButtonCountries);
        }
        private void BuildGrid(MapperRespository mapper)
        {
            Type type = typeof(BgMapperMonitor);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            foreach (var column in columns)
            {
                this.dataGridViewMapper.Columns.Add(column.Name, column.Name);
                var style = column.GetCustomAttribute<ColumnStyleAttribute>();
                if (style != null)
                {
                    this.dataGridViewMapper.Columns[column.Name].Visible = !style.Hide;
                }
                this.dataGridViewMapper.Columns[column.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        private void FillGrid(BgMapperMonitor mapperItem, Type type)
        {
            DataGridViewRow row = (DataGridViewRow)this.dataGridViewMapper.Rows[0].Clone();
            var cellIndex = 0;
            for (var colIndex = 0; colIndex < this.dataGridViewMapper.Columns.Count; colIndex++)
            {
                var methodInfo = type.GetMethod($"get_{this.dataGridViewMapper.Columns[colIndex].Name}");
                var result = methodInfo.Invoke(mapperItem, null);
                row.Cells[cellIndex].Value = result;
                cellIndex++;
            }
            this.dataGridViewMapper.Rows.Add(row);
            var bgName = row.Cells[3].Value.ToString();
            if (!this.bgNameRowIndex.ContainsKey(bgName))
                this.bgNameRowIndex.Add(bgName, this.dataGridViewMapper.Rows.Count-2);
        }

        private void textBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var selectedBgName = ((TextBox)sender).Text;
                if (this.bgNameRowIndex.ContainsKey(selectedBgName))
                    this.dataGridViewMapper.FirstDisplayedScrollingRowIndex = this.bgNameRowIndex[selectedBgName];
            }
        }

        private void textBoxSearch_Leave(object sender, EventArgs e)
        {
            var selectedBgName = ((TextBox)sender).Text;
            if (this.bgNameRowIndex.ContainsKey(selectedBgName))
                this.dataGridViewMapper.FirstDisplayedScrollingRowIndex = this.bgNameRowIndex[selectedBgName];
        }
        private void dataGridViewMapper_Sorted(object sender, EventArgs e)
        {
            this.textBoxSearch.Clear();
            this.bgNameRowIndex.Clear();
            var rowIndex = 0;
            foreach (DataGridViewRow row in this.dataGridViewMapper.Rows)
            {
                if (!row.IsNewRow)
                {
                    var bgName = row.Cells[3].Value.ToString();
                    if (!this.bgNameRowIndex.ContainsKey(bgName))
                        this.bgNameRowIndex.Add(bgName, rowIndex);
                }
                rowIndex++;
            }
        }
    }
}
