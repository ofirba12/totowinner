﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class WinnerToBgMapDialog : MaterialForm
    {
        private WinnerMapperMonitor selectedMapItem;
        private BgSport bgSport;
        private MapperRespository mapper;
        private int? selectedTeamId;
        public WinnerToBgMapDialog(WinnerMapperMonitor selectedMapItem)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            mapper = MonitorUtils.Mapper.GetBgMapper();
            this.selectedMapItem = selectedMapItem;
            this.labelSportValue.Text = selectedMapItem.Sport.ToString();
            this.labelWinnerIdValue.Text = selectedMapItem.WinnerId.ToString();
            this.labelWinnerNameValue.Text = selectedMapItem.WinnerName;
            this.textBoxSearch.LostFocus += TextBoxSearch_LostFocus;
            bgSport = MonitorUtils.GoalserveSportToBgSport(selectedMapItem.Sport);
            //bgSport = selectedMapItem.Sport == SportIds.Soccer
            //    ? BgSport.Soccer
            //    : BgSport.Basketball;
            this.labelBgItemName.Text = string.Empty;
            //MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
            //    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == bgSport), this.textBoxSearch);
        }

        private void TextBoxSearch_LostFocus(object sender, EventArgs e)
        {
            var bgName = GetBgName(this.textBoxSearch.Text);
            if (!string.IsNullOrEmpty(bgName))
                labelBgItemName.Text = bgName;
            else
            {
                labelBgItemName.Text = "No Found !!!";
                selectedTeamId = null;
                MessageBox.Show($"BgId [{this.textBoxSearch.Text}] is invalid or empty");
            }
        }
        private string GetBgName(string bgIdString)
        {
            if (int.TryParse(bgIdString, out var bgId))
            {
                var pk = new ProviderKey(bgId, bgSport, BgMapperType.Teams);
                if (mapper.BgIdItemName.ContainsKey(pk))
                {
                    selectedTeamId = bgId;
                    return mapper.BgIdItemName[pk];
                }
            }
            return null;
        }
        private void materialButtonUpdate_Click(object sender, EventArgs e)
        {
            //var teamId = MonitorUtils.SelectBgItem(this.textBoxSearch.Text, BgMapperType.Teams);
            if (!selectedTeamId.HasValue)
            {
                var yesNoDialog = MessageBox.Show($"Bg Team was not selected, are you sure you want to remove the current mapped BgId?", $"BgTeam not selected", MessageBoxButtons.YesNo);
                if (yesNoDialog == DialogResult.Yes)
                {
                    BgDataWinnerPersistanceServices.Instance.UpdateWinnerMapperBgId(
                        new BgWinnerMapper(selectedMapItem.Sport, selectedMapItem.WinnerId, selectedMapItem.WinnerName, null, null));
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                BgDataWinnerPersistanceServices.Instance.UpdateWinnerMapperBgId(
                    new BgWinnerMapper(selectedMapItem.Sport, selectedMapItem.WinnerId, selectedMapItem.WinnerName, selectedTeamId, null));// teamId.Value));
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
