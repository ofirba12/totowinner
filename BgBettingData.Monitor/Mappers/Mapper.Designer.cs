﻿namespace BgBettingData.Monitor
{
    partial class Mapper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelData = new System.Windows.Forms.Panel();
            this.dataGridViewMapper = new System.Windows.Forms.DataGridView();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.materialButtonRefresh = new MaterialSkin.Controls.MaterialButton();
            this.materialRadioButtonTeams = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRadioButtonLeagues = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRadioButtonCountries = new MaterialSkin.Controls.MaterialRadioButton();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.panelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapper)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelData
            // 
            this.panelData.Controls.Add(this.dataGridViewMapper);
            this.panelData.Controls.Add(this.panelMenu);
            this.panelData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelData.Location = new System.Drawing.Point(3, 64);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(813, 619);
            this.panelData.TabIndex = 1;
            // 
            // dataGridViewMapper
            // 
            this.dataGridViewMapper.AllowUserToDeleteRows = false;
            this.dataGridViewMapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMapper.Location = new System.Drawing.Point(147, 0);
            this.dataGridViewMapper.Name = "dataGridViewMapper";
            this.dataGridViewMapper.Size = new System.Drawing.Size(666, 619);
            this.dataGridViewMapper.TabIndex = 1;
            this.dataGridViewMapper.Sorted += new System.EventHandler(this.dataGridViewMapper_Sorted);
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.materialButtonRefresh);
            this.panelMenu.Controls.Add(this.materialRadioButtonTeams);
            this.panelMenu.Controls.Add(this.materialRadioButtonLeagues);
            this.panelMenu.Controls.Add(this.materialRadioButtonCountries);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(147, 619);
            this.panelMenu.TabIndex = 0;
            // 
            // materialButtonRefresh
            // 
            this.materialButtonRefresh.AutoSize = false;
            this.materialButtonRefresh.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButtonRefresh.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButtonRefresh.Depth = 0;
            this.materialButtonRefresh.HighEmphasis = true;
            this.materialButtonRefresh.Icon = null;
            this.materialButtonRefresh.Location = new System.Drawing.Point(19, 205);
            this.materialButtonRefresh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButtonRefresh.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButtonRefresh.Name = "materialButtonRefresh";
            this.materialButtonRefresh.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButtonRefresh.Size = new System.Drawing.Size(86, 52);
            this.materialButtonRefresh.TabIndex = 3;
            this.materialButtonRefresh.Text = "Refresh";
            this.materialButtonRefresh.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButtonRefresh.UseAccentColor = false;
            this.materialButtonRefresh.UseVisualStyleBackColor = true;
            this.materialButtonRefresh.Click += new System.EventHandler(this.materialButtonRefresh_Click);
            // 
            // materialRadioButtonTeams
            // 
            this.materialRadioButtonTeams.AutoSize = true;
            this.materialRadioButtonTeams.Depth = 0;
            this.materialRadioButtonTeams.Location = new System.Drawing.Point(10, 126);
            this.materialRadioButtonTeams.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButtonTeams.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButtonTeams.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButtonTeams.Name = "materialRadioButtonTeams";
            this.materialRadioButtonTeams.Ripple = true;
            this.materialRadioButtonTeams.Size = new System.Drawing.Size(84, 37);
            this.materialRadioButtonTeams.TabIndex = 2;
            this.materialRadioButtonTeams.TabStop = true;
            this.materialRadioButtonTeams.Text = "Teams";
            this.materialRadioButtonTeams.UseVisualStyleBackColor = true;
            this.materialRadioButtonTeams.CheckedChanged += new System.EventHandler(this.materialRadioButtonTeams_CheckedChanged);
            // 
            // materialRadioButtonLeagues
            // 
            this.materialRadioButtonLeagues.AutoSize = true;
            this.materialRadioButtonLeagues.Depth = 0;
            this.materialRadioButtonLeagues.Location = new System.Drawing.Point(10, 80);
            this.materialRadioButtonLeagues.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButtonLeagues.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButtonLeagues.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButtonLeagues.Name = "materialRadioButtonLeagues";
            this.materialRadioButtonLeagues.Ripple = true;
            this.materialRadioButtonLeagues.Size = new System.Drawing.Size(95, 37);
            this.materialRadioButtonLeagues.TabIndex = 1;
            this.materialRadioButtonLeagues.TabStop = true;
            this.materialRadioButtonLeagues.Text = "Leagues";
            this.materialRadioButtonLeagues.UseVisualStyleBackColor = true;
            this.materialRadioButtonLeagues.CheckedChanged += new System.EventHandler(this.materialRadioButtonLeagues_CheckedChanged);
            // 
            // materialRadioButtonCountries
            // 
            this.materialRadioButtonCountries.AutoSize = true;
            this.materialRadioButtonCountries.Depth = 0;
            this.materialRadioButtonCountries.Location = new System.Drawing.Point(10, 32);
            this.materialRadioButtonCountries.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButtonCountries.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButtonCountries.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButtonCountries.Name = "materialRadioButtonCountries";
            this.materialRadioButtonCountries.Ripple = true;
            this.materialRadioButtonCountries.Size = new System.Drawing.Size(102, 37);
            this.materialRadioButtonCountries.TabIndex = 0;
            this.materialRadioButtonCountries.TabStop = true;
            this.materialRadioButtonCountries.Text = "Countries";
            this.materialRadioButtonCountries.UseVisualStyleBackColor = true;
            this.materialRadioButtonCountries.CheckedChanged += new System.EventHandler(this.materialRadioButtonCountries_CheckedChanged);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBoxSearch.Location = new System.Drawing.Point(190, 38);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(289, 24);
            this.textBoxSearch.TabIndex = 2;
            this.textBoxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxSearch_KeyDown);
            this.textBoxSearch.Leave += new System.EventHandler(this.textBoxSearch_Leave);
            // 
            // Mapper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 686);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.panelData);
            this.Name = "Mapper";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mapper";
            this.panelData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapper)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.Panel panelMenu;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButtonLeagues;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButtonCountries;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButtonTeams;
        private System.Windows.Forms.DataGridView dataGridViewMapper;
        private MaterialSkin.Controls.MaterialButton materialButtonRefresh;
        private System.Windows.Forms.TextBox textBoxSearch;
    }
}