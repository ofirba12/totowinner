﻿namespace BgBettingData.Monitor
{
    partial class WinnerToBgMapDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSport = new MaterialSkin.Controls.MaterialLabel();
            this.labelWinnerName = new MaterialSkin.Controls.MaterialLabel();
            this.labelWinnerId = new MaterialSkin.Controls.MaterialLabel();
            this.labelBgItem = new MaterialSkin.Controls.MaterialLabel();
            this.materialButtonUpdate = new MaterialSkin.Controls.MaterialButton();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.labelSportValue = new MaterialSkin.Controls.MaterialLabel();
            this.labelWinnerIdValue = new MaterialSkin.Controls.MaterialLabel();
            this.labelWinnerNameValue = new MaterialSkin.Controls.MaterialLabel();
            this.labelBgItemName = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // labelSport
            // 
            this.labelSport.AutoSize = true;
            this.labelSport.Depth = 0;
            this.labelSport.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelSport.Location = new System.Drawing.Point(52, 104);
            this.labelSport.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelSport.Name = "labelSport";
            this.labelSport.Size = new System.Drawing.Size(47, 19);
            this.labelSport.TabIndex = 6;
            this.labelSport.Text = "Sport :";
            // 
            // labelWinnerName
            // 
            this.labelWinnerName.AutoSize = true;
            this.labelWinnerName.Depth = 0;
            this.labelWinnerName.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelWinnerName.Location = new System.Drawing.Point(256, 146);
            this.labelWinnerName.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelWinnerName.Name = "labelWinnerName";
            this.labelWinnerName.Size = new System.Drawing.Size(100, 19);
            this.labelWinnerName.TabIndex = 7;
            this.labelWinnerName.Text = "Winner Name:";
            // 
            // labelWinnerId
            // 
            this.labelWinnerId.AutoSize = true;
            this.labelWinnerId.Depth = 0;
            this.labelWinnerId.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelWinnerId.Location = new System.Drawing.Point(52, 146);
            this.labelWinnerId.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelWinnerId.Name = "labelWinnerId";
            this.labelWinnerId.Size = new System.Drawing.Size(75, 19);
            this.labelWinnerId.TabIndex = 8;
            this.labelWinnerId.Text = "Winner Id :";
            // 
            // labelBgItem
            // 
            this.labelBgItem.AutoSize = true;
            this.labelBgItem.Depth = 0;
            this.labelBgItem.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBgItem.Location = new System.Drawing.Point(54, 202);
            this.labelBgItem.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelBgItem.Name = "labelBgItem";
            this.labelBgItem.Size = new System.Drawing.Size(63, 19);
            this.labelBgItem.TabIndex = 9;
            this.labelBgItem.Text = "Bg Item :";
            // 
            // materialButtonUpdate
            // 
            this.materialButtonUpdate.AutoSize = false;
            this.materialButtonUpdate.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButtonUpdate.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButtonUpdate.Depth = 0;
            this.materialButtonUpdate.HighEmphasis = true;
            this.materialButtonUpdate.Icon = null;
            this.materialButtonUpdate.Location = new System.Drawing.Point(46, 276);
            this.materialButtonUpdate.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButtonUpdate.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButtonUpdate.Name = "materialButtonUpdate";
            this.materialButtonUpdate.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButtonUpdate.Size = new System.Drawing.Size(158, 36);
            this.materialButtonUpdate.TabIndex = 10;
            this.materialButtonUpdate.Text = "update";
            this.materialButtonUpdate.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButtonUpdate.UseAccentColor = false;
            this.materialButtonUpdate.UseVisualStyleBackColor = true;
            this.materialButtonUpdate.Click += new System.EventHandler(this.materialButtonUpdate_Click);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBoxSearch.Location = new System.Drawing.Point(123, 202);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(481, 24);
            this.textBoxSearch.TabIndex = 11;
            // 
            // labelSportValue
            // 
            this.labelSportValue.AutoSize = true;
            this.labelSportValue.Depth = 0;
            this.labelSportValue.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelSportValue.Location = new System.Drawing.Point(120, 104);
            this.labelSportValue.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelSportValue.Name = "labelSportValue";
            this.labelSportValue.Size = new System.Drawing.Size(57, 19);
            this.labelSportValue.TabIndex = 12;
            this.labelSportValue.Text = "#sport#";
            // 
            // labelWinnerIdValue
            // 
            this.labelWinnerIdValue.AutoSize = true;
            this.labelWinnerIdValue.Depth = 0;
            this.labelWinnerIdValue.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelWinnerIdValue.Location = new System.Drawing.Point(133, 146);
            this.labelWinnerIdValue.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelWinnerIdValue.Name = "labelWinnerIdValue";
            this.labelWinnerIdValue.Size = new System.Drawing.Size(85, 19);
            this.labelWinnerIdValue.TabIndex = 13;
            this.labelWinnerIdValue.Text = "#winner id#";
            // 
            // labelWinnerNameValue
            // 
            this.labelWinnerNameValue.AutoSize = true;
            this.labelWinnerNameValue.Depth = 0;
            this.labelWinnerNameValue.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelWinnerNameValue.Location = new System.Drawing.Point(362, 146);
            this.labelWinnerNameValue.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelWinnerNameValue.Name = "labelWinnerNameValue";
            this.labelWinnerNameValue.Size = new System.Drawing.Size(112, 19);
            this.labelWinnerNameValue.TabIndex = 14;
            this.labelWinnerNameValue.Text = "#winner name#";
            // 
            // labelBgItemName
            // 
            this.labelBgItemName.AutoSize = true;
            this.labelBgItemName.Depth = 0;
            this.labelBgItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBgItemName.Location = new System.Drawing.Point(119, 239);
            this.labelBgItemName.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelBgItemName.Name = "labelBgItemName";
            this.labelBgItemName.Size = new System.Drawing.Size(74, 19);
            this.labelBgItemName.TabIndex = 15;
            this.labelBgItemName.Text = "#bg item#";
            // 
            // WinnerToBgMapDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 347);
            this.Controls.Add(this.labelBgItemName);
            this.Controls.Add(this.labelWinnerNameValue);
            this.Controls.Add(this.labelWinnerIdValue);
            this.Controls.Add(this.labelSportValue);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.materialButtonUpdate);
            this.Controls.Add(this.labelBgItem);
            this.Controls.Add(this.labelWinnerId);
            this.Controls.Add(this.labelWinnerName);
            this.Controls.Add(this.labelSport);
            this.Name = "WinnerToBgMapDialog";
            this.Text = "Winner to BG map dialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel labelSport;
        private MaterialSkin.Controls.MaterialLabel labelWinnerName;
        private MaterialSkin.Controls.MaterialLabel labelWinnerId;
        private MaterialSkin.Controls.MaterialLabel labelBgItem;
        private MaterialSkin.Controls.MaterialButton materialButtonUpdate;
        private System.Windows.Forms.TextBox textBoxSearch;
        private MaterialSkin.Controls.MaterialLabel labelSportValue;
        private MaterialSkin.Controls.MaterialLabel labelWinnerIdValue;
        private MaterialSkin.Controls.MaterialLabel labelWinnerNameValue;
        private MaterialSkin.Controls.MaterialLabel labelBgItemName;
    }
}