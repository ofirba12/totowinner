﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class WinnerMapper : MaterialForm
    {
        private DataGridViewCellEventArgs mouseLocation;
        ToolStripMenuItem mapToBgContextMenu = new ToolStripMenuItem();
        ToolStripMenuItem locateMatchContextMenu = new ToolStripMenuItem();
        //ToolStripMenuItem mapToGoalserveContextMenu = new ToolStripMenuItem();
        BgWinnerMapperRepository winnerMapper = null;

        public SportIds FilterSport { get; private set; }

        public WinnerMapper()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.dataGridViewMapper.CellMouseEnter += DataGridView_CellMouseEnter;
            //buttonRefresh_Click(null, null);
            comboBoxSportType_SelectedIndexChanged(this.comboBoxSportType, null);
        }
        private void DataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }

        public void AddContextMenu()
        {
            mapToBgContextMenu.Text = "Map to BG";
            mapToBgContextMenu.Click -= new EventHandler(mapToBG_Click);
            mapToBgContextMenu.Click += new EventHandler(mapToBG_Click);
            locateMatchContextMenu.Text = "Locate Last Match";
            locateMatchContextMenu.Click -= new EventHandler(locateMatch_Click);
            locateMatchContextMenu.Click += new EventHandler(locateMatch_Click);
            //mapToGoalserveContextMenu.Text = "Map to Goalserve"; 
            //mapToGoalserveContextMenu.Click -= new EventHandler(mapToGoalserve_Click);
            //mapToGoalserveContextMenu.Click += new EventHandler(mapToGoalserve_Click);
            ContextMenuStrip rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;

            foreach (DataGridViewRow row in dataGridViewMapper.Rows)
                row.ContextMenuStrip = rowContextMenu;
                //row.ContextMenuStrip.Items.Add(mapToBgContextMenu);
                //row.ContextMenuStrip.Items.Add(locateMatchContextMenu);
            if (this.dataGridViewMapper.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                DataGridView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewMapper.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            selectedRow.ContextMenuStrip.Items.Clear();
            var selectedTicker = (WinnerMapperMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Add(mapToBgContextMenu);
            selectedRow.ContextMenuStrip.Items.Add(locateMatchContextMenu);
            //if (selectedTicker.Sport == SportIds.Soccer || selectedTicker.Sport == SportIds.Basketball)
            //{
            //    selectedRow.ContextMenuStrip.Items.Add(mapToBgContextMenu);
            //    selectedRow.ContextMenuStrip.Items.Add(locateMatchContextMenu);
            //}
            //else
            //    selectedRow.ContextMenuStrip.Items.Add(mapToGoalserveContextMenu);
        }
        //private void mapToGoalserve_Click(object sender, EventArgs e)
        //{
        //    var selectedRow = this.dataGridViewMapper.Rows[mouseLocation.RowIndex];
        //    if (selectedRow.IsNewRow)
        //        return;
        //    var selectedMapItem = (WinnerMapperMonitor)selectedRow.Tag;
        //    var dialog = new WinnerToGoalserveMapDialog(selectedMapItem);
        //    if (dialog.ShowDialog() == DialogResult.OK)
        //        buttonRefresh_Click(null, null);
        //}
        private void mapToBG_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewMapper.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMapItem = (WinnerMapperMonitor)selectedRow.Tag;

            var dialog = new WinnerToBgMapDialog(selectedMapItem);
            if (dialog.ShowDialog() == DialogResult.OK)
                buttonRefresh_Click(null, null);
        }
        private void locateMatch_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewMapper.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMapItem = (WinnerMapperMonitor)selectedRow.Tag;
            var match = WinnerDataServices.Instance.FindTeamInWinnerFeed(selectedMapItem.WinnerId, winnerMapper);
            MessageBox.Show($"TeamId {selectedMapItem.WinnerId} recently played on {match.GameStart} program:{match.ProgramId} place:{match.Place}", $"Winner match found", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            this.dataGridViewMapper.Rows.Clear();
            this.dataGridViewMapper.Columns.Clear();
            this.dataGridViewMapper.Refresh();
            var mapper = MonitorUtils.Mapper.GetBgMapper();
            Func<BgWinnerMapper, string> getBgName = (m) =>
            {
                if (!m.BgId.HasValue)
                    return string.Empty;
                var sport = MonitorUtils.GoalserveSportToBgSport(m.Sport);
                //var sport = m.Sport == SportIds.Soccer
                //    ? BgSport.Soccer
                //    : BgSport.Basketball;
                var prk = new ProviderKey(m.BgId.Value, sport, BgMapperType.Teams);
                return mapper.BgIdItemName[prk];
            };
            winnerMapper = WinnerDataServices.Instance.GetWinnerMapper();
            //winnerMapper = persistance.GetWinnerMapper();
            var monitorData = winnerMapper.Collection.ToList()
                .Where(x => x.Sport == this.FilterSport)
                .ToList()
                .ConvertAll<WinnerMapperMonitor>(m => new WinnerMapperMonitor(m.Sport,
                    m.WinnerId,
                    m.WinnerName,
                    m.BgId,
                    getBgName(m)));
                    //m.GoalserveId));
            //var noneMappedBg = monitorData.Where(m => (m.Sport == SportIds.Soccer || m.Sport == SportIds.Basketball) && !m.BgId.HasValue).Count();
            //var noneMappedGoalserve = monitorData.Where(m => m.Sport != SportIds.Soccer && m.Sport != SportIds.Basketball && !m.GoalserveId.HasValue).Count();
            //this.labelCounters.Text = $"Total:{monitorData.Count}, None-Mapped Bg:{noneMappedBg}, None-Mapped Goalserve:{noneMappedGoalserve}";
            this.labelCounters.Text = $"Total:{monitorData.Count}, Non-Mapped:{monitorData.Where(m => !m.BgId.HasValue).Count()}";
            MonitorUtils.BuildGrid<WinnerMapperMonitor>(monitorData, this.dataGridViewMapper, BgSport.Soccer /*fake*/);
            AddContextMenu();
        }

        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;

                if (Enum.TryParse<SportIds>(comboBox.Text, out var sport))
                    this.FilterSport = sport;
            }
        }
    }
}
