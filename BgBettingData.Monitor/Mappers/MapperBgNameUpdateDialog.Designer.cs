﻿namespace BgBettingData.Monitor
{
    partial class MapperBgNameUpdateDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxBgId = new MaterialSkin.Controls.MaterialTextBox2();
            this.textBoxBgName = new MaterialSkin.Controls.MaterialTextBox2();
            this.materialButtonUpdate = new MaterialSkin.Controls.MaterialButton();
            this.labelBgId = new MaterialSkin.Controls.MaterialLabel();
            this.labelBgName = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // textBoxBgId
            // 
            this.textBoxBgId.AnimateReadOnly = false;
            this.textBoxBgId.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.textBoxBgId.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.textBoxBgId.Depth = 0;
            this.textBoxBgId.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxBgId.HideSelection = true;
            this.textBoxBgId.LeadingIcon = null;
            this.textBoxBgId.Location = new System.Drawing.Point(155, 101);
            this.textBoxBgId.MaxLength = 32767;
            this.textBoxBgId.MouseState = MaterialSkin.MouseState.OUT;
            this.textBoxBgId.Name = "textBoxBgId";
            this.textBoxBgId.PasswordChar = '\0';
            this.textBoxBgId.PrefixSuffixText = null;
            this.textBoxBgId.ReadOnly = true;
            this.textBoxBgId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxBgId.SelectedText = "";
            this.textBoxBgId.SelectionLength = 0;
            this.textBoxBgId.SelectionStart = 0;
            this.textBoxBgId.ShortcutsEnabled = true;
            this.textBoxBgId.Size = new System.Drawing.Size(250, 48);
            this.textBoxBgId.TabIndex = 0;
            this.textBoxBgId.TabStop = false;
            this.textBoxBgId.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.textBoxBgId.TrailingIcon = null;
            this.textBoxBgId.UseSystemPasswordChar = false;
            // 
            // textBoxBgName
            // 
            this.textBoxBgName.AnimateReadOnly = false;
            this.textBoxBgName.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.textBoxBgName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.textBoxBgName.Depth = 0;
            this.textBoxBgName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxBgName.HideSelection = true;
            this.textBoxBgName.LeadingIcon = null;
            this.textBoxBgName.Location = new System.Drawing.Point(155, 155);
            this.textBoxBgName.MaxLength = 32767;
            this.textBoxBgName.MouseState = MaterialSkin.MouseState.OUT;
            this.textBoxBgName.Name = "textBoxBgName";
            this.textBoxBgName.PasswordChar = '\0';
            this.textBoxBgName.PrefixSuffixText = null;
            this.textBoxBgName.ReadOnly = false;
            this.textBoxBgName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxBgName.SelectedText = "";
            this.textBoxBgName.SelectionLength = 0;
            this.textBoxBgName.SelectionStart = 0;
            this.textBoxBgName.ShortcutsEnabled = true;
            this.textBoxBgName.Size = new System.Drawing.Size(250, 48);
            this.textBoxBgName.TabIndex = 1;
            this.textBoxBgName.TabStop = false;
            this.textBoxBgName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.textBoxBgName.TrailingIcon = null;
            this.textBoxBgName.UseSystemPasswordChar = false;
            // 
            // materialButtonUpdate
            // 
            this.materialButtonUpdate.AutoSize = false;
            this.materialButtonUpdate.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButtonUpdate.Depth = 0;
            this.materialButtonUpdate.HighEmphasis = true;
            this.materialButtonUpdate.Icon = null;
            this.materialButtonUpdate.Location = new System.Drawing.Point(128, 238);
            this.materialButtonUpdate.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButtonUpdate.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButtonUpdate.Name = "materialButtonUpdate";
            this.materialButtonUpdate.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButtonUpdate.Size = new System.Drawing.Size(158, 36);
            this.materialButtonUpdate.TabIndex = 4;
            this.materialButtonUpdate.Text = "update";
            this.materialButtonUpdate.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButtonUpdate.UseAccentColor = false;
            this.materialButtonUpdate.UseVisualStyleBackColor = true;
            this.materialButtonUpdate.Click += new System.EventHandler(this.materialButtonUpdate_Click);
            // 
            // labelBgId
            // 
            this.labelBgId.AutoSize = true;
            this.labelBgId.Depth = 0;
            this.labelBgId.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBgId.Location = new System.Drawing.Point(29, 116);
            this.labelBgId.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelBgId.Name = "labelBgId";
            this.labelBgId.Size = new System.Drawing.Size(45, 19);
            this.labelBgId.TabIndex = 5;
            this.labelBgId.Text = "Bg Id :";
            // 
            // labelBgName
            // 
            this.labelBgName.AutoSize = true;
            this.labelBgName.Depth = 0;
            this.labelBgName.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBgName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelBgName.Location = new System.Drawing.Point(29, 167);
            this.labelBgName.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelBgName.Name = "labelBgName";
            this.labelBgName.Size = new System.Drawing.Size(70, 19);
            this.labelBgName.TabIndex = 6;
            this.labelBgName.Text = "Bg Name:";
            // 
            // MapperBgNameUpdateDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(445, 306);
            this.Controls.Add(this.labelBgName);
            this.Controls.Add(this.labelBgId);
            this.Controls.Add(this.materialButtonUpdate);
            this.Controls.Add(this.textBoxBgName);
            this.Controls.Add(this.textBoxBgId);
            this.Name = "MapperBgNameUpdateDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bg Name Mapper Details";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialTextBox2 textBoxBgId;
        private MaterialSkin.Controls.MaterialTextBox2 textBoxBgName;
        private MaterialSkin.Controls.MaterialButton materialButtonUpdate;
        private MaterialSkin.Controls.MaterialLabel labelBgId;
        private MaterialSkin.Controls.MaterialLabel labelBgName;
    }
}