﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class MapperBgNameUpdateDialog : MaterialForm
    {
        public string BgName { get; private set; }
        public MapperBgNameUpdateDialog(string selectedBgId, 
            string selectedBgName)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.textBoxBgId.Text = selectedBgId;
            this.textBoxBgName.Text = selectedBgName;
        }

        private void materialButtonUpdate_Click(object sender, EventArgs e)
        {
            var mapper = MonitorUtils.Mapper.GetBgMapper();//BgMapperServices.Instance.GetBgMapper();
            var selectedBgId = int.Parse(this.textBoxBgId.Text);
            var sport = mapper.BgIdBgMap[selectedBgId].First().Sport;
            var type = mapper.BgIdBgMap[selectedBgId].First().BgType;
            var alreadyExists = false;
            var pk = new ProviderName(sport, this.textBoxBgName.Text);
            switch (type)
            {
                case BgMapperType.Countries:
                    alreadyExists = mapper.BgCountriesNames.ContainsKey(pk);
                    break;
                case BgMapperType.Leagues:
                    alreadyExists = mapper.BgLeaguesNames.ContainsKey(pk);
                    break;
                case BgMapperType.Teams:
                    alreadyExists = mapper.BgTeamsNames.ContainsKey(pk);
                    break;
            }
            if (alreadyExists)
            {
                MessageBox.Show($"{this.textBoxBgName.Text} already exists, please choose another name", $"BgName already exists in {type}", MessageBoxButtons.OK);
                this.DialogResult = DialogResult.None;
            }
            else
            {
                this.BgName = this.textBoxBgName.Text;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
