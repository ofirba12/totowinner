﻿namespace BgBettingData.Monitor
{
    partial class WinnerMapper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewMapper = new System.Windows.Forms.DataGridView();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.labelCounters = new MaterialSkin.Controls.MaterialLabel();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapper)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewMapper
            // 
            this.dataGridViewMapper.AllowUserToDeleteRows = false;
            this.dataGridViewMapper.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMapper.Location = new System.Drawing.Point(3, 64);
            this.dataGridViewMapper.Name = "dataGridViewMapper";
            this.dataGridViewMapper.Size = new System.Drawing.Size(991, 383);
            this.dataGridViewMapper.TabIndex = 2;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(893, 35);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.buttonRefresh.TabIndex = 3;
            this.buttonRefresh.Text = "Get Mapper";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // labelCounters
            // 
            this.labelCounters.AutoSize = true;
            this.labelCounters.Depth = 0;
            this.labelCounters.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelCounters.Location = new System.Drawing.Point(293, 35);
            this.labelCounters.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelCounters.Name = "labelCounters";
            this.labelCounters.Size = new System.Drawing.Size(64, 19);
            this.labelCounters.TabIndex = 4;
            this.labelCounters.Text = "Counters";
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.comboBoxSportType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.comboBoxSportType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Tennis",
            "Soccer",
            "Baseball",
            "Handball",
            "Basketball",
            "Football"});
            this.comboBoxSportType.Location = new System.Drawing.Point(160, 31);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 25);
            this.comboBoxSportType.TabIndex = 5;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // WinnerMapper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 450);
            this.Controls.Add(this.comboBoxSportType);
            this.Controls.Add(this.labelCounters);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.dataGridViewMapper);
            this.Name = "WinnerMapper";
            this.Text = "Mapper Winner";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewMapper;
        private System.Windows.Forms.Button buttonRefresh;
        private MaterialSkin.Controls.MaterialLabel labelCounters;
        private System.Windows.Forms.ComboBox comboBoxSportType;
    }
}