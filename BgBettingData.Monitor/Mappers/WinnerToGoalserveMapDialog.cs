﻿//using MaterialSkin;
//using MaterialSkin.Controls;
//using System;
//using System.Windows.Forms;
//using TotoWinner.Data;
//using TotoWinner.Data.BgBettingData;
//using TotoWinner.Services;

//namespace BgBettingData.Monitor
//{
//    public partial class WinnerToGoalserveMapDialog : MaterialForm
//    {
//        private WinnerMapperMonitor selectedMapItem;
//        public WinnerToGoalserveMapDialog(WinnerMapperMonitor selectedMapItem)
//        {
//            InitializeComponent();
//            var materialSkinManager = MaterialSkinManager.Instance;
//            materialSkinManager.AddFormToManage(this);
//            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
//            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
//            this.selectedMapItem = selectedMapItem;
//            this.labelSportValue.Text = selectedMapItem.Sport.ToString();
//            this.labelWinnerIdValue.Text = selectedMapItem.WinnerId.ToString();
//            this.labelWinnerNameValue.Text = selectedMapItem.WinnerName;
//            //this.textBoxGoalserveId.Text = selectedMapItem.GoalserveId?.ToString();
//        }

//        private void materialButtonUpdate_Click(object sender, EventArgs e)
//        {
//            var yesNoDialog = string.IsNullOrEmpty(textBoxGoalserveId.Text)
//                ? MessageBox.Show($"Are you sure you want to delete goalserve mapping for {this.labelWinnerIdValue.Text}?", $"Remove Mapping", MessageBoxButtons.YesNo)
//                : MessageBox.Show($"Are you sure you want to set goalserve mapping for {this.labelWinnerIdValue.Text}?", $"Update Mapping", MessageBoxButtons.YesNo);
//            if (yesNoDialog == DialogResult.Yes)
//            {
//                var goalserveId = !string.IsNullOrEmpty(textBoxGoalserveId.Text) ? Convert.ToInt32(textBoxGoalserveId.Text) : (int?)null;
//                BgDataWinnerPersistanceServices.Instance.UpdateWinnerMapperGoalserveId(
//                    new BgWinnerMapper(selectedMapItem.Sport, selectedMapItem.WinnerId, selectedMapItem.WinnerName, null, goalserveId));
//                this.DialogResult = DialogResult.OK;
//            }
//        }
//        private void textBoxGoalserveId_KeyPress(object sender, KeyPressEventArgs e)
//        {
//            // Allow control keys (e.g., Backspace), digits, or specific characters
//            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
//            {
//                e.Handled = true; // Ignore the key press
//            }
//        }
//    }
//}
