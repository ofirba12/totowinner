﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    internal static class MonitorUtils
    {
        private static BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;
        static internal BgMapperRepository Mapper = new BgMapperRepository();
        internal static BgSport GoalserveSportToBgSport(SportIds GoalserveSportIds)
        {
            switch (GoalserveSportIds)
            {
                case SportIds.Soccer:
                    return BgSport.Soccer;
                case SportIds.Baseball:
                    return BgSport.Baseball;
                case SportIds.Tennis:
                    return BgSport.Tennis;
                case SportIds.Basketball:
                    return BgSport.Basketball;
                case SportIds.Handball:
                    return BgSport.Handball;
                case SportIds.Football:
                    return BgSport.Football;
                default:
                    throw new Exception($"Goalserve SportIds {GoalserveSportIds} is not supported in BG");
            }
        }
        internal static void SetAutoComplete(IEnumerable<BgMapperMonitor> collection, TextBox textBox)
        {
            var source = new AutoCompleteStringCollection();
            source.AddRange(collection.Select(m => $"{m.BgName} # {m.BgId}").Distinct().ToArray());
            textBox.AutoCompleteCustomSource = source;
            textBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }
        internal static int? SelectBgItem(string item, BgMapperType type)
        {
            if (string.IsNullOrEmpty(item))
            {
                return null;
            }
            if (item.Contains('#'))
            {
                var bgId = item.Split('#')[1].Trim();
                return int.Parse(bgId);
            }
            else
            {
                MessageBox.Show($"Bg Id of type {type} could not be fetched from {item}", $"Invalid {type}:{item} found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
        internal static void BuildGrid<T>(List<T> monitorData, DataGridView dgv, BgSport sport)
        {
            Type type = typeof(T);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            var columnIndex = 0;
            var zebraCells = new List<int>();
            var colorAlgoLogic_1Cells = new List<int>();
            var colorAlgoLogic_2Cells = new List<int>();
            var colorAlgoLogic_3Cells = new List<int>();
            var colorAlgoLogic_4Cells = new List<int>();
            var colorAlgoLogic_5Cells = new List<int>();
            var colorAlgoLogic_6Cells = new List<int>();
            dgv.EnableHeadersVisualStyles = false;
            foreach (var column in columns)
            {
                dgv.Columns.Add(column.Name, column.Name);
                var style = column.GetCustomAttribute<ColumnStyleAttribute>();
                if (style != null)
                {
                    dgv.Columns[column.Name].Visible = !style.Hide;
                    if (style.Width > 0)
                        dgv.Columns[column.Name].Width = style.Width;
                    if (sport != BgSport.Basketball && style.BasketballScores)
                        dgv.Columns[column.Name].Visible = false;
                    if (!string.IsNullOrEmpty(style.DisplayName))
                    {
                        dgv.Columns[column.Name].HeaderText = style.DisplayName;
                        dgv.Columns[column.Name].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    }
                    if (style.DisableSort)
                        dgv.Columns[column.Name].SortMode = DataGridViewColumnSortMode.NotSortable;
                    if (style.FontBold)
                    {
                        dgv.Columns[column.Name].DefaultCellStyle.Font = new Font(dgv.Font, FontStyle.Bold);
                    }
                    if (style.ZebraOn)
                        zebraCells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 1)
                        colorAlgoLogic_1Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 2)
                        colorAlgoLogic_2Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 3)
                        colorAlgoLogic_3Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 4)
                        colorAlgoLogic_4Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 5)
                        colorAlgoLogic_5Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 6)
                        colorAlgoLogic_6Cells.Add(columnIndex);
                    if (style.HeaderColor == 1)
                        dgv.Columns[column.Name].HeaderCell.Style.BackColor = Color.GreenYellow;
                    if (style.Freeze)
                        dgv.Columns[column.Name].Frozen = true;
                }
                dgv.Columns[column.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columnIndex++;
            }
            foreach (var match in monitorData)
            {
                var row = CreateGridRow<T>(match, type, dgv,
                    zebraCells,
                    colorAlgoLogic_1Cells,
                    colorAlgoLogic_2Cells,
                    colorAlgoLogic_3Cells,
                    colorAlgoLogic_4Cells,
                    colorAlgoLogic_5Cells,
                    colorAlgoLogic_6Cells);
                dgv.Rows.Add(row);
            }
        }
        internal static DataGridViewRow CreateGridRow<T>(T match,
            Type type,
            DataGridView dgv,
            List<int> zebraCells,
            List<int> colorAlgoLogic_1Cells,
            List<int> colorAlgoLogic_2Cells,
            List<int> colorAlgoLogic_3Cells,
            List<int> colorAlgoLogic_4Cells,
            List<int> colorAlgoLogic_5Cells,
            List<int> colorAlgoLogic_6Cells)
        {
            DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
            var cellIndex = 0;
            var ignore = false;
            var missingOdds = false;
            var missingScores = false;
            var missingQuarters = false;
            var rankItem = 0;
            for (var colIndex = 0; colIndex < dgv.Columns.Count; colIndex++)
            {
                var property = dgv.Columns[colIndex].Name;
                var methodInfo = type.GetMethod($"get_{property}");
                var result = methodInfo.Invoke(match, null);
                row.Cells[cellIndex].Value = result;
                if (result?.GetType() == typeof(BgRangeIndex))
                {
                    row.Cells[cellIndex].Value = ((BgRangeIndex)result).GetDescription<BgRangeIndex>();
                }
                if (property.ToLower() == "ignore")
                    ignore = (bool)result;
                if (match is MonitorBgMatchTeamAnalysis)
                {
                    if (property.ToLower() == "missingodds")
                        missingOdds = (bool)result;
                    if (property.ToLower() == "missingscores")
                        missingScores = (bool)result;
                    if (property.ToLower() == "missingquarters")
                        missingQuarters = (bool)result;
                }
                if (match is BgTeamAnalysisMonitor)
                {
                    if (property.ToLower() == "missingodds" || property.ToLower() == "missingresult")
                        rankItem += (int)result;
                }
                cellIndex++;
            }
            row.Tag = match;
            StyleGridRow(row, ignore, missingOdds, missingScores, missingQuarters, rankItem, zebraCells,
                colorAlgoLogic_1Cells, colorAlgoLogic_2Cells, colorAlgoLogic_3Cells, colorAlgoLogic_4Cells, colorAlgoLogic_5Cells, colorAlgoLogic_6Cells);
            return row;
        }

        private static void StyleGridRow(DataGridViewRow row,
            bool ignore,
            bool missingOdds,
            bool missingScores,
            bool missingQuarters,
            int rankItem,
            List<int> zebraCells,
            List<int> colorAlgoLogic_1Cells,
            List<int> colorAlgoLogic_2Cells,
            List<int> colorAlgoLogic_3Cells,
            List<int> colorAlgoLogic_4Cells,
            List<int> colorAlgoLogic_5Cells,
            List<int> colorAlgoLogic_6Cells)
        {
            row.DefaultCellStyle.BackColor = ignore
                ? Color.LightGray
                : Color.White;
            row.DefaultCellStyle.BackColor = missingQuarters
                ? Color.LightSalmon
                : row.DefaultCellStyle.BackColor;
            row.DefaultCellStyle.BackColor = missingOdds || rankItem > 0
                ? Color.LightCoral
                : row.DefaultCellStyle.BackColor;
            row.DefaultCellStyle.BackColor = missingScores
                ? Color.OrangeRed
                : row.DefaultCellStyle.BackColor;

            foreach (var cellIndex in zebraCells)
                row.Cells[cellIndex].Style.BackColor = Color.LightGray;
            foreach (var cellIndex in colorAlgoLogic_1Cells)
            {
                var cellVal = (int?)row.Cells[cellIndex].Value;
                if (cellVal.HasValue)
                {
                    if (cellVal.Value >= 75 && cellVal.Value <= 94)
                        row.Cells[cellIndex].Style.BackColor = Color.LightYellow;
                    else if (cellVal.Value >= 95 && cellVal.Value <= 100)
                        row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                    else
                    {
                        if (zebraCells.Contains(cellIndex))
                            row.Cells[cellIndex].Style.BackColor = Color.LightGray;
                        else
                            row.Cells[cellIndex].Style.BackColor = Color.White;
                    }
                }
                else
                {
                    if (zebraCells.Contains(cellIndex))
                        row.Cells[cellIndex].Style.BackColor = Color.LightGray;
                    else
                        row.Cells[cellIndex].Style.BackColor = Color.White;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_2Cells)
            {
                var cellVal = (int?)row.Cells[cellIndex].Value;
                if (cellVal.HasValue && cellVal.Value > 0)
                    row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                else
                    row.Cells[cellIndex].Style.BackColor = Color.White;
            }
            foreach (var cellIndex in colorAlgoLogic_3Cells)
            {
                var cellVal = (bool)row.Cells[cellIndex + 1].Value;
                if (!cellVal)
                    row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                else
                    row.Cells[cellIndex].Style.BackColor = Color.White;
            }
            foreach (var cellIndex in colorAlgoLogic_4Cells)
            {
                var cellVal = row.Cells[cellIndex].Value;
                switch(cellVal)
                {
                    case "HOME WIN":
                    case "OVER 2.5":
                    case "RANGE 0-1":
                        row.Cells[cellIndex - 2].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.LightYellow;
                        break;
                    case "DRAW":
                    case "RANGE 2-3":
                        row.Cells[cellIndex - 2].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.LightYellow;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.White;
                        break;
                    case "AWAY WIN":
                    case "UNDER 2.5":
                    case "RANGE 4+":
                        row.Cells[cellIndex - 2].Style.BackColor = Color.LightYellow;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.White;
                        break;
                    default:
                        row.Cells[cellIndex - 2].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 3].Style.BackColor = Color.White;
                        row.Cells[cellIndex - 4].Style.BackColor = Color.White;
                        break;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_5Cells)
            {
                var cellVal = (int?)row.Cells[cellIndex].Value;
                if (cellVal.HasValue)
                {
                    if (cellVal.Value > 60)
                        row.Cells[cellIndex].Style.BackColor = Color.LightYellow;
                    //else 
                    //    row.Cells[cellIndex].Style.BackColor = Color.White;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_6Cells)
            {
                var cellVal = (bool)row.Cells[cellIndex - 1].Value;
                if (cellVal)
                    row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                else
                    row.Cells[cellIndex].Style.BackColor = Color.White;
            }
        }

        internal static void HandleMatchDetailsDialog(MatchDetails dialog, Action<object, EventArgs> showDataButton_Click)
        {
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var m = dialog.SelectedMatchParams;
                var matchToUpsert = new ManualMatch(m.Sport,
                    m.Status,
                    m.Country.BgId,
                    m.Country.DisplayName,
                    m.League.DisplayName,
                    m.League.BgId,
                    m.IsCup,
                    m.GameTime,
                    m.HomeTeam.DisplayName,
                    m.HomeTeam.BgId,
                    m.AwayTeam.DisplayName,
                    m.AwayTeam.BgId,
                    m.Ignore,
                    m.Matchid,
                    m.OddsAndScores,
                    DateTime.Now);
                ManualProviderServices.Instance.AddOrUpdateMatch(matchToUpsert);
                showDataButton_Click.Invoke(null, null);
            }
        }

        internal static Tuple<string, string> BgTeamsFilterConvertor(BgTeamsFilter filter)
        {
            var filterLeft = string.Empty;
            var filterRight = string.Empty;
            switch (filter)
            {
                case BgTeamsFilter.All_All:
                    filterLeft = "ALL";
                    filterRight = "ALL";
                    break;
                case BgTeamsFilter.All_Favorite:
                    filterLeft = "ALL";
                    filterRight = "Favorite";
                    break;
                case BgTeamsFilter.All_Netral:
                    filterLeft = "ALL";
                    filterRight = "Netral";
                    break;
                case BgTeamsFilter.All_Underdog:
                    filterLeft = "ALL";
                    filterRight = "Underdog";
                    break;
                case BgTeamsFilter.Home_All:
                    filterLeft = "Home";
                    filterRight = "ALL";
                    break;
                case BgTeamsFilter.Home_Favorite:
                    filterLeft = "Home";
                    filterRight = "Favorite";
                    break;
                case BgTeamsFilter.Home_Netral:
                    filterLeft = "Home";
                    filterRight = "Netral";
                    break;
                case BgTeamsFilter.Home_Underdog:
                    filterLeft = "Home";
                    filterRight = "Underdog";
                    break;
                case BgTeamsFilter.Away_All:
                    filterLeft = "Away";
                    filterRight = "ALL";
                    break;
                case BgTeamsFilter.Away_Favorite:
                    filterLeft = "Away";
                    filterRight = "Favorite";
                    break;
                case BgTeamsFilter.Away_Netral:
                    filterLeft = "Away";
                    filterRight = "Netral";
                    break;
                case BgTeamsFilter.Away_Underdog:
                    filterLeft = "Away";
                    filterRight = "Underdog";
                    break;
            }
            return new Tuple<string, string>(filterLeft, filterRight);
        }

        internal static int? GetMax(int? v1, int? v2)
        {
            int? val = null;
            if (v1.HasValue && v2.HasValue)
                val = Math.Max(v1.Value, v2.Value);
            else if (!v1.HasValue && v2.HasValue)
                val = v2;
            else if (v1.HasValue && !v2.HasValue)
                val = v1;
            return val;
        }

        public static void MonitorBgMatchWithIndexesCellConditionalFormating(DataGridView dgv, DataGridViewCellFormattingEventArgs e)
        {
            var match = (MonitorBgMatchWithIndexes)dgv.Rows[e.RowIndex].Tag;
            var wdlCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (dgv.Columns[e.ColumnIndex].Name == "WDL")
            {
                var homeOddCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex - 5];
                var drawOddCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex - 4];
                var awayOddCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex - 3];
                var oddPowerCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex + 1];
                if (match.OddPower > 0)
                    oddPowerCell.Style.ForeColor = Color.Green;
                else if (match.OddPower < 0)
                    oddPowerCell.Style.ForeColor = Color.Red;
                var specialDrawStatus = false;
                switch (match.Sport)
                {
                    case BgSport.Soccer:
                        specialDrawStatus = analysisServices.SoccerSpecialDrawStatuses.Contains(match.Status.ToLower());// match.Status.ToLower() == "pen." || match.Status.ToLower() == "after over" || match.Status.ToLower() == "aet";
                        break;
                    case BgSport.Basketball:
                        specialDrawStatus = false;
                        break;
                }

                switch (wdlCell.Value.ToString())
                {
                    case "W":
                        wdlCell.Style.BackColor = Color.LightGreen;
                        break;
                    case "D":
                        if (specialDrawStatus)
                            wdlCell.Style.BackColor = Color.Orange;
                        else
                            wdlCell.Style.BackColor = Color.Yellow;
                        break;
                    case "L":
                        wdlCell.Style.BackColor = Color.LightCoral;
                        break;
                }

                if (!specialDrawStatus)
                {
                    if (match.HomeScore > match.AwayScore)
                    {
                        homeOddCell.Style.BackColor = Color.DimGray;
                    }
                    else if (match.HomeScore < match.AwayScore)
                    {
                        awayOddCell.Style.BackColor = Color.DimGray;
                    }
                    else
                        drawOddCell.Style.BackColor = Color.DimGray;
                }
                else
                    drawOddCell.Style.BackColor = Color.Orange;
            }

        }
    }
}
