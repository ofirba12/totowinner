﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    internal class WinnerDashboardState
    {
        public DateTime WinnerLineDate { get; set; }
        public SportIds? SportFilter { get; set; }
        public int? BetTypeIdFilter { get; set; }
        public dynamic MarketData { get; set; }
        public bool ShowFathersOnly { get; set; }
    }
}
