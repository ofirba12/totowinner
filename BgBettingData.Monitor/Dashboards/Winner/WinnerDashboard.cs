﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class WinnerDashboard : MaterialForm
    {
        private GoalServeProviderServices _gsProviderSrv = GoalServeProviderServices.Instance;
        private static BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private WinnerDashboardState state;
        private ToolStripMenuItem mapToBgContextMenu = new ToolStripMenuItem();
        private ToolStripMenuItem mapToGoalserveContextMenu = new ToolStripMenuItem();
        private List<WinnerFeedMonitor> feed;
        private Dictionary<int, string> betTypes;
        private DataGridViewCellEventArgs mouseLocation;
        private int homeIdColumnIndex = 2;
        private int guestIdColumnIndex = 4;
        public WinnerDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.monthCalendarChooseDate.DateSelected += MonthCalendarChooseDate_DateSelected;
            this.dataGridViewFeed.CellMouseEnter += DataGridView_CellMouseEnter;
            this.state = new WinnerDashboardState();
            this.state.WinnerLineDate = DateTime.Today;
            this.state.SportFilter = null;
        }
        private void DataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        private void MonthCalendarChooseDate_DateSelected(object sender, DateRangeEventArgs e)
        {
            this.state.WinnerLineDate = e.Start.Date;
        }

        private void materialButtonShowFeed_Click(object sender, EventArgs e)
        {
            var winnerLineJson = _btTipsSrv.GetWinnerLineResponse(this.state.WinnerLineDate, false);
            this.state.MarketData = WebApiInvoker.JsonDeserializeDynamic(winnerLineJson);
            Dictionary<long, int> rounds = FetchRounds(this.state.MarketData, this.state.WinnerLineDate);
            comboBoxPrograms.Items.Clear();
            comboBoxPrograms.Items.AddRange(rounds.Keys.Select(i => i.ToString()).ToArray());
            comboBoxPrograms.Text = comboBoxPrograms.Items[0].ToString();

        }
        private void buttonFetchData_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(comboBoxPrograms.Text) && long.TryParse(comboBoxPrograms.Text, out var roundId))
            {
                var mapper = BgDataWinnerPersistanceServices.Instance.GetWinnerMapper();
                var mapperBg = mapper.ToDictionary(m => m.WinnerId, m => m.BgId);
                var mapperGoalserve = mapper.ToDictionary(m => m.WinnerId, m => m.GoalserveId);
                //var mapper = BgDataWinnerPersistanceServices.Instance.GetWinnerMapper()
                //    .ToDictionary(m => m.WinnerId, m => m.BgId);
                Func<int, SportIds, bool> isWinnerMappedToBg = (winnerId, sport) =>
                {
                    var res = mapperBg.ContainsKey(winnerId) && mapperBg[winnerId].HasValue;
                    return res;
                    //var res = false;
                    //if (sport != SportIds.Soccer && sport != SportIds.Basketball)
                    //{
                    //    res = mapperGoalserve.ContainsKey(winnerId) && mapperGoalserve[winnerId].HasValue;
                    //}
                    //else
                    //{
                    //    //var bgSport = sport == SportIds.Soccer
                    //    //    ? BgSport.Soccer
                    //    //    : BgSport.Basketball;
                    //    res = mapperBg.ContainsKey(winnerId) && mapperBg[winnerId].HasValue;
                    //}
                    //return res;
                };
                Func<bool, bool, string> getSD = (isSingle, isDouble) =>
                {
                    var sdStr = $"{(isSingle ? "S" : "")}{(isDouble ? "D" : "")}";
                    return sdStr;
                };
                this.checkBoxShowFathers.Checked = false;
                var sportsToParse = new List<SportIds>() { SportIds.Soccer, SportIds.Basketball, SportIds.Baseball, SportIds.Tennis, SportIds.Handball, SportIds.Football };
                var repository = _btTipsSrv.ParseWinnerLineResponse(this.state.MarketData, roundId, this.state.WinnerLineDate, false, sportsToParse);
                Dictionary<int, BettingTipsParentEvent> events = repository.Events;
                feed = events.Values.ToList()
                    .ConvertAll<WinnerFeedMonitor>(f => new WinnerFeedMonitor(
                        f.IsFather,
                        f.SportId,
                        f.Bet.BetTypeId,
                        f.Bet.BetTypeName,
                        f.EventId,
                        f.Place,
                        f.TotoId,
                        f.Time,
                        f.Status,
                        f.Bet.Name,
                        f.HomeTeamName,
                        f.HomeId,
                        isWinnerMappedToBg(f.HomeId, f.SportId),
                        f.GuestTeamName,
                        f.GuestId,
                        isWinnerMappedToBg(f.GuestId, f.SportId),
                        f.LeagueName,
                        f.Country,
                        f.Bet.Home,
                        f.Bet.Draw,
                        f.Bet.Away,
                        getSD(f.Bet.Single, f.Bet.Double),
                        f.GameResult,
                        f.Bet.FinalHomeWin,
                        f.Bet.FinalDraw,
                        f.Bet.FinalAwayWin
                        ));
                dataGridViewFeed.Rows.Clear();
                dataGridViewFeed.Columns.Clear();
                dataGridViewFeed.Refresh();
                comboBoxSportType.Items.Clear();
                comboBoxSportType.Items.Add("All");
                comboBoxSportType.Items.AddRange(feed.Select(f => f.Sport.ToString()).Distinct().ToArray());
                comboBoxSportType.SelectedIndex = 0;
                MonitorUtils.BuildGrid<WinnerFeedMonitor>(feed, this.dataGridViewFeed, BgSport.Soccer);
                AddContextMenu();
            }
        }
        private Dictionary<long, int> FetchRounds(dynamic marketData, DateTime winnerLineDate)
        {
            var dRounds = marketData["rounds"];
            var rounds = new Dictionary<long, int>(); //#roundid, #games
            foreach (var dRound in dRounds)
            {
                var id = BettingTipsServicesHelpers.DynamicResolver<long>(dRound, null, "id", ResolveEnum.LONG);
                var games = BettingTipsServicesHelpers.DynamicResolver<int>(dRound, null, "games", ResolveEnum.INT);
                rounds.Add(id, games);
            }
            return rounds;
        }
        public void AddContextMenu()
        {
            mapToBgContextMenu.Text = "Map to BG";
            mapToBgContextMenu.Click -= new EventHandler(mapToBG_Click);
            mapToBgContextMenu.Click += new EventHandler(mapToBG_Click);
            //mapToGoalserveContextMenu.Text = "Map to Goalserve";
            //mapToGoalserveContextMenu.Click -= new EventHandler(mapToGoalserve_Click);
            //mapToGoalserveContextMenu.Click += new EventHandler(mapToGoalserve_Click);

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridViewFeed.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridViewFeed.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                DataGridView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        //private void mapToGoalserve_Click(object sender, EventArgs e)
        //{
        //    var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
        //    //var selectedColumn = this.dataGridViewFeed.Rows[mouseLocation.ColumnIndex];

        //    var selectedMatch = (WinnerFeedMonitor)selectedRow.Tag;
        //    if (mouseLocation.ColumnIndex == homeIdColumnIndex)
        //    {
        //        var dialog = new WinnerToGoalserveMapDialog(new WinnerMapperMonitor(selectedMatch.Sport, selectedMatch.HomeId, selectedMatch.HomeTeamName, null, null, null));
        //        dialog.ShowDialog();
        //    }
        //    else if (mouseLocation.ColumnIndex == guestIdColumnIndex)
        //    {
        //        var dialog = new WinnerToGoalserveMapDialog(new WinnerMapperMonitor(selectedMatch.Sport, selectedMatch.GuestId, selectedMatch.GuestTeamName, null, null, null));
        //        dialog.ShowDialog();
        //    }
        //}
        private void mapToBG_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            //var selectedColumn = this.dataGridViewFeed.Rows[mouseLocation.ColumnIndex];

            var selectedMatch = (WinnerFeedMonitor)selectedRow.Tag;
            if (mouseLocation.ColumnIndex == homeIdColumnIndex)
            {
                var dialog = new WinnerToBgMapDialog(new WinnerMapperMonitor(selectedMatch.Sport, selectedMatch.HomeId, selectedMatch.HomeTeamName, null, null));
                dialog.ShowDialog();
            }
            else if (mouseLocation.ColumnIndex == guestIdColumnIndex)
            {
                var dialog = new WinnerToBgMapDialog(new WinnerMapperMonitor(selectedMatch.Sport, selectedMatch.GuestId, selectedMatch.GuestTeamName, null, null));
                dialog.ShowDialog();
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (WinnerFeedMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            //if ((!selectedMatch.HomeIdMapped && mouseLocation.ColumnIndex == 11) || 
            //    (!selectedMatch.GuestIdMapped && mouseLocation.ColumnIndex == 14))
            if (!selectedMatch.HomeIdMapped || !selectedMatch.GuestIdMapped)
            {
                selectedRow.ContextMenuStrip.Items.Add(mapToBgContextMenu);
                //if (selectedMatch.Sport == SportIds.Soccer || selectedMatch.Sport == SportIds.Basketball)
                //    selectedRow.ContextMenuStrip.Items.Add(mapToBgContextMenu);
                //else
                //    selectedRow.ContextMenuStrip.Items.Add(mapToGoalserveContextMenu);
            }
        }

        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.ComboBox)
            {
                var comboBox = sender as System.Windows.Forms.ComboBox;

                if (Enum.TryParse<SportIds>(comboBox.Text, out var sport))
                {
                    this.state.SportFilter = sport;
                    var typesColl = feed
                        .Where(s => s.Sport == sport)
                        .Select(i => new Tuple<int, string>(i.BetTypeId, i.BetName)).ToLookup(m => m.Item1, m => m.Item2).Distinct();
                    betTypes = typesColl.ToDictionary(i => i.Key, i => i.First());
                    comboBoxBetType.Items.Clear();
                    comboBoxBetType.Items.Add("All");
                    comboBoxBetType.Items.AddRange(betTypes.Values.ToArray());
                }
                else
                    this.state.SportFilter = null;
            }
        }
        private void comboBoxBetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.ComboBox)
            {
                var comboBox = sender as System.Windows.Forms.ComboBox;
                var betTypeId = comboBox.Text != "All"
                    ? betTypes.First(i => i.Value == comboBox.Text).Key
                    : default(int?);
                this.state.BetTypeIdFilter = betTypeId;

            }
        }
        private void checkBoxShowFathers_CheckedChanged(object sender, EventArgs e)
        {
            this.state.ShowFathersOnly = this.checkBoxShowFathers.Checked;
        }
        private void buttonApplyFilter_Click(object sender, EventArgs e)
        {
            var filtered = feed.Where(f =>
                (!this.state.SportFilter.HasValue || f.Sport == this.state.SportFilter) &&
                (!this.state.BetTypeIdFilter.HasValue || f.BetTypeId == this.state.BetTypeIdFilter))
                .ToList();
            if (this.state.ShowFathersOnly)
                filtered = filtered.Where(f => f.IsFather == true)
                    .ToList();
            dataGridViewFeed.Rows.Clear();
            dataGridViewFeed.Columns.Clear();
            dataGridViewFeed.Refresh();
            MonitorUtils.BuildGrid<WinnerFeedMonitor>(filtered, this.dataGridViewFeed, BgSport.Soccer);
            AddContextMenu();
        }

        private void checkboxDisabledFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkboxDisabledFilter.Checked)
            {
                this.state.SportFilter = null;
                this.state.BetTypeIdFilter = null;
                this.state.ShowFathersOnly = false;
                comboBoxBetType.Enabled = false;
                comboBoxSportType.Enabled = false;
                buttonApplyFilter.Enabled = false;
                checkBoxShowFathers.Enabled = false;
                buttonApplyFilter_Click(null, null);
            }
            else
            {
                comboBoxBetType.Enabled = true;
                comboBoxSportType.Enabled = true;
                buttonApplyFilter.Enabled = true;
                checkBoxShowFathers.Enabled = true;
                if (Enum.TryParse<SportIds>(comboBoxSportType.Text, out var sport))
                    this.state.SportFilter = sport;
                if (comboBoxBetType.Text != "All")
                    this.state.BetTypeIdFilter = betTypes.First(i => i.Value == comboBoxBetType.Text).Key;
                this.state.ShowFathersOnly = checkBoxShowFathers.Checked;
                buttonApplyFilter_Click(null, null);
            }
        }


    }
}
