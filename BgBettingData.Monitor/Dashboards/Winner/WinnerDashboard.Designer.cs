﻿namespace BgBettingData.Monitor
{
    partial class WinnerDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkboxDisabledFilter = new System.Windows.Forms.CheckBox();
            this.groupBoxFilters = new System.Windows.Forms.GroupBox();
            this.checkBoxShowFathers = new System.Windows.Forms.CheckBox();
            this.buttonApplyFilter = new MaterialSkin.Controls.MaterialButton();
            this.comboBoxBetType = new System.Windows.Forms.ComboBox();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.buttonFetchData = new MaterialSkin.Controls.MaterialButton();
            this.comboBoxPrograms = new System.Windows.Forms.ComboBox();
            this.monthCalendarChooseDate = new System.Windows.Forms.MonthCalendar();
            this.materialButtonShowFeed = new MaterialSkin.Controls.MaterialButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewFeed = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.groupBoxFilters.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkboxDisabledFilter);
            this.panel1.Controls.Add(this.groupBoxFilters);
            this.panel1.Controls.Add(this.buttonFetchData);
            this.panel1.Controls.Add(this.comboBoxPrograms);
            this.panel1.Controls.Add(this.monthCalendarChooseDate);
            this.panel1.Controls.Add(this.materialButtonShowFeed);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 616);
            this.panel1.TabIndex = 1;
            // 
            // checkboxDisabledFilter
            // 
            this.checkboxDisabledFilter.AutoSize = true;
            this.checkboxDisabledFilter.Location = new System.Drawing.Point(39, 518);
            this.checkboxDisabledFilter.Name = "checkboxDisabledFilter";
            this.checkboxDisabledFilter.Size = new System.Drawing.Size(86, 17);
            this.checkboxDisabledFilter.TabIndex = 6;
            this.checkboxDisabledFilter.Text = "Disable Filter";
            this.checkboxDisabledFilter.UseVisualStyleBackColor = true;
            this.checkboxDisabledFilter.CheckedChanged += new System.EventHandler(this.checkboxDisabledFilter_CheckedChanged);
            // 
            // groupBoxFilters
            // 
            this.groupBoxFilters.Controls.Add(this.checkBoxShowFathers);
            this.groupBoxFilters.Controls.Add(this.buttonApplyFilter);
            this.groupBoxFilters.Controls.Add(this.comboBoxBetType);
            this.groupBoxFilters.Controls.Add(this.comboBoxSportType);
            this.groupBoxFilters.Location = new System.Drawing.Point(32, 319);
            this.groupBoxFilters.Name = "groupBoxFilters";
            this.groupBoxFilters.Size = new System.Drawing.Size(164, 179);
            this.groupBoxFilters.TabIndex = 5;
            this.groupBoxFilters.TabStop = false;
            this.groupBoxFilters.Text = "Filters";
            // 
            // checkBoxShowFathers
            // 
            this.checkBoxShowFathers.AutoSize = true;
            this.checkBoxShowFathers.Location = new System.Drawing.Point(7, 91);
            this.checkBoxShowFathers.Name = "checkBoxShowFathers";
            this.checkBoxShowFathers.Size = new System.Drawing.Size(115, 17);
            this.checkBoxShowFathers.TabIndex = 7;
            this.checkBoxShowFathers.Text = "Show Fathers Only";
            this.checkBoxShowFathers.UseVisualStyleBackColor = true;
            this.checkBoxShowFathers.CheckedChanged += new System.EventHandler(this.checkBoxShowFathers_CheckedChanged);
            // 
            // buttonApplyFilter
            // 
            this.buttonApplyFilter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonApplyFilter.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonApplyFilter.Depth = 0;
            this.buttonApplyFilter.HighEmphasis = true;
            this.buttonApplyFilter.Icon = null;
            this.buttonApplyFilter.Location = new System.Drawing.Point(7, 117);
            this.buttonApplyFilter.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonApplyFilter.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonApplyFilter.Name = "buttonApplyFilter";
            this.buttonApplyFilter.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonApplyFilter.Size = new System.Drawing.Size(117, 36);
            this.buttonApplyFilter.TabIndex = 6;
            this.buttonApplyFilter.Text = "Apply Filter";
            this.buttonApplyFilter.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonApplyFilter.UseAccentColor = false;
            this.buttonApplyFilter.UseVisualStyleBackColor = true;
            this.buttonApplyFilter.Click += new System.EventHandler(this.buttonApplyFilter_Click);
            // 
            // comboBoxBetType
            // 
            this.comboBoxBetType.FormattingEnabled = true;
            this.comboBoxBetType.Items.AddRange(new object[] {
            "All"});
            this.comboBoxBetType.Location = new System.Drawing.Point(6, 57);
            this.comboBoxBetType.Name = "comboBoxBetType";
            this.comboBoxBetType.Size = new System.Drawing.Size(126, 21);
            this.comboBoxBetType.TabIndex = 3;
            this.comboBoxBetType.Text = "All";
            this.comboBoxBetType.SelectedIndexChanged += new System.EventHandler(this.comboBoxBetType_SelectedIndexChanged);
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "All",
            "Soccer",
            "Basketball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(6, 19);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(126, 21);
            this.comboBoxSportType.TabIndex = 2;
            this.comboBoxSportType.Text = "All";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // buttonFetchData
            // 
            this.buttonFetchData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonFetchData.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonFetchData.Depth = 0;
            this.buttonFetchData.HighEmphasis = true;
            this.buttonFetchData.Icon = null;
            this.buttonFetchData.Location = new System.Drawing.Point(32, 274);
            this.buttonFetchData.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonFetchData.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonFetchData.Name = "buttonFetchData";
            this.buttonFetchData.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonFetchData.Size = new System.Drawing.Size(90, 36);
            this.buttonFetchData.TabIndex = 4;
            this.buttonFetchData.Text = "Get Data";
            this.buttonFetchData.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonFetchData.UseAccentColor = false;
            this.buttonFetchData.UseVisualStyleBackColor = true;
            this.buttonFetchData.Click += new System.EventHandler(this.buttonFetchData_Click);
            // 
            // comboBoxPrograms
            // 
            this.comboBoxPrograms.FormattingEnabled = true;
            this.comboBoxPrograms.Location = new System.Drawing.Point(32, 231);
            this.comboBoxPrograms.Name = "comboBoxPrograms";
            this.comboBoxPrograms.Size = new System.Drawing.Size(150, 21);
            this.comboBoxPrograms.TabIndex = 3;
            // 
            // monthCalendarChooseDate
            // 
            this.monthCalendarChooseDate.Location = new System.Drawing.Point(4, 9);
            this.monthCalendarChooseDate.Name = "monthCalendarChooseDate";
            this.monthCalendarChooseDate.TabIndex = 1;
            // 
            // materialButtonShowFeed
            // 
            this.materialButtonShowFeed.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButtonShowFeed.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButtonShowFeed.Depth = 0;
            this.materialButtonShowFeed.HighEmphasis = true;
            this.materialButtonShowFeed.Icon = null;
            this.materialButtonShowFeed.Location = new System.Drawing.Point(32, 186);
            this.materialButtonShowFeed.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButtonShowFeed.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButtonShowFeed.Name = "materialButtonShowFeed";
            this.materialButtonShowFeed.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButtonShowFeed.Size = new System.Drawing.Size(132, 36);
            this.materialButtonShowFeed.TabIndex = 0;
            this.materialButtonShowFeed.Text = "Get Programs";
            this.materialButtonShowFeed.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButtonShowFeed.UseAccentColor = false;
            this.materialButtonShowFeed.UseVisualStyleBackColor = true;
            this.materialButtonShowFeed.Click += new System.EventHandler(this.materialButtonShowFeed_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewFeed);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(241, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1056, 616);
            this.panel2.TabIndex = 2;
            // 
            // dataGridViewFeed
            // 
            this.dataGridViewFeed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFeed.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewFeed.Name = "dataGridViewFeed";
            this.dataGridViewFeed.Size = new System.Drawing.Size(1056, 616);
            this.dataGridViewFeed.TabIndex = 0;
            // 
            // WinnerDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 683);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "WinnerDashboard";
            this.Text = "Winner Feed";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxFilters.ResumeLayout(false);
            this.groupBoxFilters.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialButton materialButtonShowFeed;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MonthCalendar monthCalendarChooseDate;
        private System.Windows.Forms.DataGridView dataGridViewFeed;
        private System.Windows.Forms.ComboBox comboBoxSportType;
        private System.Windows.Forms.ComboBox comboBoxPrograms;
        private MaterialSkin.Controls.MaterialButton buttonFetchData;
        private System.Windows.Forms.GroupBox groupBoxFilters;
        private System.Windows.Forms.ComboBox comboBoxBetType;
        private MaterialSkin.Controls.MaterialButton buttonApplyFilter;
        private System.Windows.Forms.CheckBox checkboxDisabledFilter;
        private System.Windows.Forms.CheckBox checkBoxShowFathers;
    }
}

