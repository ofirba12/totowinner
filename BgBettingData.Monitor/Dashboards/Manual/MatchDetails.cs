﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class MatchDetails : MaterialForm
    {
        public enum Mode {  NewMatch, UpdateMatch };
        public Mode DialogMode { get; private set; }

        private MapperRespository mapper;
        internal SelectedMatchParams SelectedMatchParams { get; private set; }
        public MatchDetails(ManualMatchMonitor selectedMatch = null)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            mapper = MonitorUtils.Mapper.GetBgMapper();
            this.SelectedMatchParams = new SelectedMatchParams();
            if (selectedMatch != null)
            {
                this.DialogMode = Mode.UpdateMatch;
                this.buttonSave.Text = "Update Match";
                this.comboBoxSport.SelectedIndex = (int)selectedMatch.Sport;
                UpdateControls(selectedMatch);
            }
            else
            {
                this.DialogMode = Mode.NewMatch;
                this.buttonSave.Text = "Save Match";
                this.comboBoxSport.SelectedIndex = 0; //Soccer
            }
            this.labelMatchId.Visible = this.DialogMode == Mode.UpdateMatch;
            this.labelMatchIdValue.Visible = this.DialogMode == Mode.UpdateMatch;
            this.groupBoxBasketballQuarters.Visible = selectedMatch?.Sport == BgSport.Basketball;
        }

        private void UpdateControls(ManualMatchMonitor selectedMatch)
        {
            this.SelectedMatchParams.Matchid = selectedMatch.MatchId;
            this.SelectedMatchParams.Sport = selectedMatch.Sport;
            this.SelectedMatchParams.IsCup = selectedMatch.IsCup;
            this.SelectedMatchParams.Ignore = selectedMatch.Ignore;
            this.SelectedMatchParams.GameTime = selectedMatch.GameStart;
            this.labelMatchIdValue.Text = this.SelectedMatchParams.Matchid.ToString();
            var countryId = selectedMatch.CountryId.HasValue
                ? $" # {selectedMatch.CountryId}"
                : String.Empty;
            SelectCountry($"{selectedMatch.Country}{countryId}");
            var leagueId = selectedMatch.LeagueId.HasValue
                ? $" # {selectedMatch.LeagueId}"
                : String.Empty;
            SelectLeague($"{selectedMatch.League}{leagueId}");
            var homeId = selectedMatch.HomeId.HasValue
                ? $" # {selectedMatch.HomeId}"
                : String.Empty;
            SelectHomeTeam($"{selectedMatch.Home}{homeId}");
            var awayId = selectedMatch.AwayId.HasValue
                ? $" # {selectedMatch.AwayId}"
                : String.Empty;
            SelectAwayTeam($"{selectedMatch.Away}{awayId}");
            this.dateTimePickerMatchTime.Value = selectedMatch.GameStart;
            this.comboBoxStatus.SelectedItem = selectedMatch.Status;
            this.checkboxCup.Checked = selectedMatch.IsCup;
            this.numericHomeOdd.Value = selectedMatch.HomeOdd ?? 1;
            this.numericDrawOdd.Value = selectedMatch.DrawOdd ?? 1;
            this.numericAwayOdd.Value = selectedMatch.AwayOdd ?? 1;
            this.numericHomeScore.Value = selectedMatch.HomeScore ?? 0;
            this.numericAwayScore.Value = selectedMatch.AwayScore ?? 0;
            if (this.SelectedMatchParams.Sport == BgSport.Basketball)
            {
                this.numericUpDownHQ1.Value = selectedMatch.HQ1 ?? 0;
                this.numericUpDownAQ1.Value = selectedMatch.AQ1 ?? 0;
                this.numericUpDownHQ2.Value = selectedMatch.HQ2 ?? 0;
                this.numericUpDownAQ2.Value = selectedMatch.AQ2 ?? 0;
                this.numericUpDownHQ3.Value = selectedMatch.HQ3 ?? 0;
                this.numericUpDownAQ3.Value = selectedMatch.AQ3 ?? 0;
                this.numericUpDownHQ4.Value = selectedMatch.HQ4 ?? 0;
                this.numericUpDownAQ4.Value = selectedMatch.AQ4 ?? 0;
            }
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors.Where(m => m.BgType == BgMapperType.Countries && m.BgSport == this.SelectedMatchParams.Sport), this.textBoxCountry);
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors.Where(m => m.BgType == BgMapperType.Leagues && m.BgSport == this.SelectedMatchParams.Sport), this.textBoxLeague);
            this.comboBoxSport.Enabled = false;
            this.textBoxCountry.Enabled = false;
            this.textBoxLeague.Enabled = false;
            this.textBoxHomeTeam.Enabled = false;
            this.textBoxAwayTeam.Enabled = false;
        }

        private void InitializeControls()
        {
            this.labelCountryId.Text = string.Empty;
            this.labelLeagueId.Text = string.Empty;
            this.labelHomeId.Text = string.Empty;
            this.labelAwayId.Text = string.Empty;
            this.textBoxCountry.Text = string.Empty;
            this.textBoxLeague.Text = string.Empty;
            this.textBoxHomeTeam.Text = string.Empty;
            this.textBoxAwayTeam.Text = string.Empty;
            this.dateTimePickerMatchTime.Value = DateTime.Now;
            this.comboBoxStatus.SelectedIndex = this.SelectedMatchParams.Sport == BgSport.Soccer
                ? 0
                : 1;
            this.checkboxCup.Checked = false;
            this.numericHomeOdd.Value = 1;
            this.numericDrawOdd.Value = 1;
            this.numericAwayOdd.Value = 1;
            this.numericHomeScore.Value = 0;
            this.numericAwayScore.Value = 0;
            this.groupBoxBasketballQuarters.Visible = this.SelectedMatchParams.Sport == BgSport.Basketball;
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors.Where(m => m.BgType == BgMapperType.Countries && m.BgSport == this.SelectedMatchParams.Sport), this.textBoxCountry);
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors.Where(m => m.BgType == BgMapperType.Leagues && m.BgSport == this.SelectedMatchParams.Sport), this.textBoxLeague);
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors.Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.SelectedMatchParams.Sport), this.textBoxHomeTeam);
        }
        #region Country Selection
        private void textBoxCountry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SelectCountry(((TextBox)sender).Text);
        }

        private void textBoxCountry_Leave(object sender, System.EventArgs e)
        {
            SelectCountry(((TextBox)sender).Text);
        }

        private void SelectCountry(string selectedCountry)
        {
            if (string.IsNullOrEmpty(selectedCountry))
                return;
            if (selectedCountry.Contains('#'))
            {
                var bgId = selectedCountry.Split('#')[1].Trim();
                var country = mapper.BgMapperMonitors.FirstOrDefault(i => i.BgId == int.Parse(bgId));
                this.labelCountryId.Text = bgId;
                this.SelectedMatchParams.Country = new ManualMapperItem(int.Parse(bgId), BgMapperType.Countries, country.BgName);
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxCountry.Text = $"{country.BgName} # {bgId}";
            }
            else
            {
                this.labelCountryId.Text = "New";
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxCountry.Text = $"{selectedCountry.Trim()}";
                this.SelectedMatchParams.Country = new ManualMapperItem(null, BgMapperType.Countries, selectedCountry.Trim());
            }
        }
        #endregion

        #region League Selection
        private void textBoxLeague_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SelectLeague(((TextBox)sender).Text);
        }

        private void SelectLeague(string selectedLeague)
        {
            if (string.IsNullOrEmpty(selectedLeague))
                return;
            if (selectedLeague.Contains('#'))
            {
                var bgId = selectedLeague.Split('#')[1].Trim();
                var league = mapper.BgMapperMonitors.FirstOrDefault(i => i.BgId == int.Parse(bgId));
                this.labelLeagueId.Text = bgId;
                this.SelectedMatchParams.League = new ManualMapperItem(int.Parse(bgId), BgMapperType.Leagues, league.BgName);
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxLeague.Text = $"{league.BgName} # {bgId}";
            }
            else
            {
                this.labelLeagueId.Text = "New";
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxLeague.Text = $"{selectedLeague.Trim()}";
                this.SelectedMatchParams.League = new ManualMapperItem(null, BgMapperType.Leagues, selectedLeague.Trim());
            }
        }

        private void textBoxLeague_Leave(object sender, System.EventArgs e)
        {
            SelectLeague(((TextBox)sender).Text);
        }
        #endregion

        #region Hometeam Selection
        private void textBoxHomeTeam_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SelectHomeTeam(((TextBox)sender).Text);
        }
        private void textBoxHomeTeam_Leave(object sender, System.EventArgs e)
        {
            SelectHomeTeam(((TextBox)sender).Text);
        }
        private void SelectHomeTeam(string selectedTeam)
        {
            if (string.IsNullOrEmpty(selectedTeam))
                return;
            if (selectedTeam.Contains('#'))
            {
                var bgId = selectedTeam.Split('#')[1].Trim();
                var team = mapper.BgMapperMonitors.FirstOrDefault(i => i.BgId == int.Parse(bgId));
                this.labelHomeId.Text = bgId;
                this.SelectedMatchParams.HomeTeam = new ManualMapperItem(int.Parse(bgId), BgMapperType.Teams, team.BgName);
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxHomeTeam.Text = $"{team.BgName} # {bgId}";

                MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.SelectedMatchParams.Sport)
                    .Except(new List<BgMapperMonitor>() { team }, new BgMapperMonitorComparer()),
                    this.textBoxAwayTeam);
            }
            else
            {
                this.labelHomeId.Text = "New";
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxHomeTeam.Text = $"{selectedTeam.Trim()}";
                this.SelectedMatchParams.HomeTeam = new ManualMapperItem(null, BgMapperType.Teams, selectedTeam.Trim());
                MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.SelectedMatchParams.Sport),
                    this.textBoxAwayTeam);
            }
        }
        #endregion

        #region Awayteam Selection
        private void textBoxAwayTeam_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SelectAwayTeam(((TextBox)sender).Text);
        }
        private void textBoxAwayTeam_Leave(object sender, System.EventArgs e)
        {
            SelectAwayTeam(((TextBox)sender).Text);
        }
        private void SelectAwayTeam(string selectedTeam)
        {
            if (string.IsNullOrEmpty(selectedTeam))
                return;
            if (selectedTeam.Contains('#'))
            {
                var bgId = selectedTeam.Split('#')[1].Trim();
                var team = mapper.BgMapperMonitors.FirstOrDefault(i => i.BgId == int.Parse(bgId));
                this.labelAwayId.Text = bgId;
                this.SelectedMatchParams.AwayTeam = new ManualMapperItem(int.Parse(bgId), BgMapperType.Teams, team.BgName); ;
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxAwayTeam.Text = $"{team.BgName} # {bgId}";

                MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.SelectedMatchParams.Sport)
                    .Except(new List<BgMapperMonitor>() { team }, new BgMapperMonitorComparer()),
                    this.textBoxHomeTeam);
            }
            else
            {
                this.labelAwayId.Text = "New";
                if (this.DialogMode == Mode.UpdateMatch)
                    this.textBoxAwayTeam.Text = $"{selectedTeam.Trim()}";
                this.SelectedMatchParams.AwayTeam = new ManualMapperItem(null, BgMapperType.Teams, selectedTeam.Trim());
                MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.SelectedMatchParams.Sport),
                    this.textBoxHomeTeam);
            }
        }
        #endregion
        private void comboBoxSport_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.SelectedMatchParams.Sport = sport;
                    InitializeControls();
                }
            }
        }
        private void buttonSave_Click(object sender, EventArgs e)
        {
            var isValidData = IsValid();
            if (isValidData)
            {
                this.SelectedMatchParams.GameTime = DateTime.Parse(this.dateTimePickerMatchTime.Text);
                this.SelectedMatchParams.IsCup = this.checkboxCup.Checked;
                var homeQuarters = new List<decimal?>() { GetValue(this.numericUpDownHQ1), GetValue(this.numericUpDownHQ2), GetValue(this.numericUpDownHQ3), GetValue(this.numericUpDownHQ4) };
                var awayQuarters = new List<decimal?>() { GetValue(this.numericUpDownAQ1), GetValue(this.numericUpDownAQ2), GetValue(this.numericUpDownAQ3), GetValue(this.numericUpDownAQ4) };
                BasketballQuatersScores basketballQuatersScores = this.SelectedMatchParams.Sport == BgSport.Basketball 
                    && homeQuarters.All(i => i.HasValue) 
                    && awayQuarters.All(i => i.HasValue)
                    ? new BasketballQuatersScores((int)homeQuarters[0].Value,
                        (int)homeQuarters[1].Value,
                        (int)homeQuarters[2].Value,
                        (int)homeQuarters[3].Value,
                        (int)awayQuarters[0].Value,
                        (int)awayQuarters[1].Value,
                        (int)awayQuarters[2].Value,
                        (int)awayQuarters[3].Value
                        )
                    : null;
                this.SelectedMatchParams.OddsAndScores = new OddsAndScores(GetValue(this.numericHomeOdd),
                    GetValue(this.numericDrawOdd),
                    GetValue(this.numericAwayOdd),
                    (int?)GetValue(this.numericHomeScore),
                    (int?)GetValue(this.numericAwayScore),
                    basketballQuatersScores);
            }
            this.DialogResult = isValidData
                ? DialogResult.OK
                : DialogResult.None;
        }

        private bool IsValid()
        {
            var isValid = true;
            if (this.SelectedMatchParams.Country == null)
            {
                isValid = false;
                MessageBox.Show("Country is missing.", "Country Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (this.SelectedMatchParams.League == null)
            {
                isValid = false;
                MessageBox.Show("League is missing.", "League Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (this.SelectedMatchParams.HomeTeam == null)
            {
                isValid = false;
                MessageBox.Show("Home team is missing.", "Teams Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (this.SelectedMatchParams.AwayTeam == null)
            {
                isValid = false;
                MessageBox.Show("Away team is missing.", "Teams Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (this.dateTimePickerMatchTime.Value > DateTime.Now)
            {
                isValid = false;
                MessageBox.Show("Game start must be in the past.", "Game Start Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (this.DialogMode == Mode.UpdateMatch && this.dateTimePickerMatchTime.Value.Date != this.SelectedMatchParams.GameTime.Date)
            {
                isValid = false;
                MessageBox.Show($"Only game time can be updated, game date must be {this.SelectedMatchParams.GameTime.Date.ToString("dd/MM/yyyy")}.", "Game Start Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if ((this.SelectedMatchParams.HomeTeam.BgId.HasValue && this.SelectedMatchParams.AwayTeam.BgId.HasValue &&
               this.SelectedMatchParams.HomeTeam.BgId == this.SelectedMatchParams.AwayTeam.BgId) ||
                    (!this.SelectedMatchParams.HomeTeam.BgId.HasValue && !this.SelectedMatchParams.AwayTeam.BgId.HasValue &&
               this.SelectedMatchParams.HomeTeam.DisplayName == this.SelectedMatchParams.AwayTeam.DisplayName))
            {
                isValid = false;
                MessageBox.Show("Home and Away team are the same.", "Teams Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            var homeQuarters = new List<decimal?>() { GetValue(this.numericUpDownHQ1), GetValue(this.numericUpDownHQ2), GetValue(this.numericUpDownHQ3), GetValue(this.numericUpDownHQ4) };
            var awayQuarters = new List<decimal?>() { GetValue(this.numericUpDownAQ1), GetValue(this.numericUpDownAQ2), GetValue(this.numericUpDownAQ3), GetValue(this.numericUpDownAQ4) };
            var homeScores = GetValue(this.numericHomeScore);
            var awayScores = GetValue(this.numericAwayScore);
            if (this.SelectedMatchParams.Sport == BgSport.Basketball && homeQuarters.All(i => i.HasValue) && homeScores != null && awayQuarters.All(i => i.HasValue) && awayScores != null)
            {
                if (homeQuarters.Sum() != homeScores) 
                {
                    MessageBox.Show("Home quarters do not sum up to final home score.", "Quarters Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (awayQuarters.Sum() != awayScores)
                {
                    MessageBox.Show("Away quarters do not sum up to final away score.", "Quarters Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            return isValid;
        }
        private decimal? GetValue(NumericUpDown control)
        {
            return Decimal.Round(control.Value, 2); 
            //var ctrlValue = control.Value == (decimal)0.0 
            //    ? (decimal?)null 
            //    : Decimal.Round(control.Value, 2);
            //return ctrlValue;
        }

        private void comboBoxStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                this.SelectedMatchParams.Status = comboBox.Text;
            }
        }

        #region numericUpDown_Enter
        private void numericHomeOdd_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        private void numericDrawOdd_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        private void numericAwayOdd_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        private void numericHomeScore_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        private void numericAwayScore_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        private void numericUpDownHQ1_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownAQ1_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownHQ2_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownAQ2_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownHQ3_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownAQ3_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownHQ4_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericUpDownAQ4_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        #endregion
    }
}
