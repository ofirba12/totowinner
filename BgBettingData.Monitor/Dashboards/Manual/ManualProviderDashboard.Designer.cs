﻿namespace BgBettingData.Monitor
{
    partial class ManualProviderDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.monthCalendarDateFilter = new System.Windows.Forms.MonthCalendar();
            this.comboBoxSport = new System.Windows.Forms.ComboBox();
            this.showDataButton = new MaterialSkin.Controls.MaterialButton();
            this.manualMatchedGridView = new System.Windows.Forms.DataGridView();
            this.addMatchButton = new MaterialSkin.Controls.MaterialButton();
            ((System.ComponentModel.ISupportInitialize)(this.manualMatchedGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(3, 64);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(227, 383);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // monthCalendarDateFilter
            // 
            this.monthCalendarDateFilter.Location = new System.Drawing.Point(3, 73);
            this.monthCalendarDateFilter.Name = "monthCalendarDateFilter";
            this.monthCalendarDateFilter.TabIndex = 1;
            this.monthCalendarDateFilter.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarDateFilter_DateSelected);
            // 
            // comboBoxSport
            // 
            this.comboBoxSport.FormattingEnabled = true;
            this.comboBoxSport.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSport.Location = new System.Drawing.Point(39, 247);
            this.comboBoxSport.Name = "comboBoxSport";
            this.comboBoxSport.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSport.TabIndex = 2;
            this.comboBoxSport.SelectedIndexChanged += new System.EventHandler(this.comboBoxSport_SelectedIndexChanged);
            // 
            // showDataButton
            // 
            this.showDataButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.showDataButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.showDataButton.Depth = 0;
            this.showDataButton.HighEmphasis = true;
            this.showDataButton.Icon = null;
            this.showDataButton.Location = new System.Drawing.Point(39, 296);
            this.showDataButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.showDataButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.showDataButton.Name = "showDataButton";
            this.showDataButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.showDataButton.Size = new System.Drawing.Size(105, 36);
            this.showDataButton.TabIndex = 3;
            this.showDataButton.Text = "Show Data";
            this.showDataButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.showDataButton.UseAccentColor = false;
            this.showDataButton.UseVisualStyleBackColor = true;
            this.showDataButton.Click += new System.EventHandler(this.showDataButton_Click);
            // 
            // manualMatchedGridView
            // 
            this.manualMatchedGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.manualMatchedGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manualMatchedGridView.Location = new System.Drawing.Point(230, 64);
            this.manualMatchedGridView.Name = "manualMatchedGridView";
            this.manualMatchedGridView.Size = new System.Drawing.Size(567, 383);
            this.manualMatchedGridView.TabIndex = 4;
            this.manualMatchedGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.manualMatchedGridView_CellMouseEnter);
            // 
            // addMatchButton
            // 
            this.addMatchButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addMatchButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.addMatchButton.Depth = 0;
            this.addMatchButton.HighEmphasis = true;
            this.addMatchButton.Icon = null;
            this.addMatchButton.Location = new System.Drawing.Point(39, 356);
            this.addMatchButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.addMatchButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.addMatchButton.Name = "addMatchButton";
            this.addMatchButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.addMatchButton.Size = new System.Drawing.Size(106, 36);
            this.addMatchButton.TabIndex = 5;
            this.addMatchButton.Text = "Add Match";
            this.addMatchButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.addMatchButton.UseAccentColor = false;
            this.addMatchButton.UseMnemonic = false;
            this.addMatchButton.UseVisualStyleBackColor = true;
            this.addMatchButton.Click += new System.EventHandler(this.addMatchButton_Click);
            // 
            // ManualProviderDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.addMatchButton);
            this.Controls.Add(this.manualMatchedGridView);
            this.Controls.Add(this.showDataButton);
            this.Controls.Add(this.comboBoxSport);
            this.Controls.Add(this.monthCalendarDateFilter);
            this.Controls.Add(this.splitter1);
            this.Name = "ManualProviderDashboard";
            this.Text = "Manual Update";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.manualMatchedGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.MonthCalendar monthCalendarDateFilter;
        private System.Windows.Forms.ComboBox comboBoxSport;
        private MaterialSkin.Controls.MaterialButton showDataButton;
        private System.Windows.Forms.DataGridView manualMatchedGridView;
        private MaterialSkin.Controls.MaterialButton addMatchButton;
    }
}