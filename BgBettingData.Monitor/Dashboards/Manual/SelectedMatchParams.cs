﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    internal class SelectedMatchParams
    {
        public BgSport Sport { get; internal set; }
        public DateTime GameTime { get; internal set; }
        public string Status { get; internal set; }
        public bool IsCup { get; internal set; }
        public bool Ignore { get; internal set; }
        public ManualMapperItem Country { get; internal set; }
        public ManualMapperItem League { get; internal set; }
        public ManualMapperItem HomeTeam { get; internal set; }
        public ManualMapperItem AwayTeam { get; internal set; }
        public OddsAndScores OddsAndScores { get; internal set; }
        public long? Matchid { get; internal set; }
    }
}
