﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class ManualProviderDashboard : MaterialForm
    {
        private ShowBgFeedParams selectedParams;
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextMenuUpdateMatch = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuIgnoreMatch = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuUseMatch = new ToolStripMenuItem();

        public ManualProviderDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.selectedParams = new ShowBgFeedParams();
            this.comboBoxSport.SelectedIndex = 0;
            this.selectedParams.ChosenDate = DateTime.Today.Date;
        }

        private void addMatchButton_Click(object sender, System.EventArgs e)
        {
            var dialog = new MatchDetails();
            MonitorUtils.HandleMatchDetailsDialog(dialog, showDataButton_Click);
        }

        private void showDataButton_Click(object sender, EventArgs e)
        {
            this.manualMatchedGridView.Rows.Clear();
            this.manualMatchedGridView.Columns.Clear();
            this.manualMatchedGridView.Refresh();
            this.Text = $"Manual Dashboard - {this.selectedParams.ChosenDate.ToLongDateString()}";
            var monitorData = ManualProviderServices.Instance.FetchManualMatchesByLastUpdate(this.selectedParams.ChosenDate)
                .Where(m => m.Sport == this.selectedParams.Sport)
                .ToList()
                .ConvertAll<ManualMatchMonitor>(f => new ManualMatchMonitor(f.Sport,
                    f.Status,
                    f.LastUpdate,
                    f.CountryId,
                    f.Country,
                    f.League,
                    f.LeagueId,
                    f.IsCup,
                    f.GameStart,
                    f.Home,
                    f.HomeId,
                    f.Away,
                    f.AwayId,
                    f.Ignore,
                    f.MatchId.Value,
                    f.OddsAndScores.HomeOdd, f.OddsAndScores.DrawOdd, f.OddsAndScores.AwayOdd,
                    f.OddsAndScores.HomeScore, f.OddsAndScores.AwayScore,
                    f.OddsAndScores.BasketballQuatersScores));
            MonitorUtils.BuildGrid<ManualMatchMonitor>(monitorData, this.manualMatchedGridView, this.selectedParams.Sport);
            AddContextMenu();
        }
        private void comboBoxSport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                    this.selectedParams.Sport = sport;
            }
        }
        #region Context Menu
        private void manualMatchedGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextMenuUpdateMatch.Text = "Update Match";
            contextMenuUpdateMatch.Click -= new EventHandler(contextMenuUpdateMatch_Click);
            contextMenuUpdateMatch.Click += new EventHandler(contextMenuUpdateMatch_Click);
            contextMenuIgnoreMatch.Text = "Ignore Match";
            contextMenuIgnoreMatch.Click -= new EventHandler(contextMenuIgnoreMatch_Click);
            contextMenuIgnoreMatch.Click += new EventHandler(contextMenuIgnoreMatch_Click);
            contextMenuUseMatch.Text = "Use Match";
            contextMenuUseMatch.Click -= new EventHandler(contextMenuUseMatch_Click);
            contextMenuUseMatch.Click += new EventHandler(contextMenuUseMatch_Click);
            ContextMenuStrip rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.manualMatchedGridView.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.manualMatchedGridView.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                manualMatchedGridView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }

        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.manualMatchedGridView.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (ManualMatchMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            if (selectedMatch.Ignore)
            {
                selectedRow.ContextMenuStrip.Items.Add(contextMenuUpdateMatch);
                selectedRow.ContextMenuStrip.Items.Add(contextMenuUseMatch);
            }
            else
            {
                selectedRow.ContextMenuStrip.Items.Add(contextMenuUpdateMatch);
                selectedRow.ContextMenuStrip.Items.Add(contextMenuIgnoreMatch);
            }
        }

        private void contextMenuUpdateMatch_Click(object sender, EventArgs e)
        {
            var selectedRow = this.manualMatchedGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (ManualMatchMonitor)selectedRow.Tag;
            var dialog = new MatchDetails(selectedMatch);
            MonitorUtils.HandleMatchDetailsDialog(dialog, showDataButton_Click);
        }

        private void contextMenuIgnoreMatch_Click(object sender, EventArgs e)
        {
            SetIgnoreUseFlag(true);
        }

        private void SetIgnoreUseFlag(bool ignore)
        {
            var selectedRow = this.manualMatchedGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (ManualMatchMonitor)selectedRow.Tag;
            var match = new ManualMatch(selectedMatch);
            match.Ignore = ignore;
            ManualProviderServices.Instance.AddOrUpdateMatch(match);
            showDataButton_Click(null, null);
        }

        private void contextMenuUseMatch_Click(object sender, EventArgs e)
        {
            SetIgnoreUseFlag(false);
        }

        #endregion

        private void monthCalendarDateFilter_DateSelected(object sender, DateRangeEventArgs e)
        {
            this.selectedParams.ChosenDate = e.Start.Date;
        }
    }
}
