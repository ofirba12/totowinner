﻿namespace BgBettingData.Monitor
{
    partial class MatchDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCountry = new MaterialSkin.Controls.MaterialLabel();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.labelLeague = new MaterialSkin.Controls.MaterialLabel();
            this.textBoxLeague = new System.Windows.Forms.TextBox();
            this.textBoxHomeTeam = new System.Windows.Forms.TextBox();
            this.textBoxAwayTeam = new System.Windows.Forms.TextBox();
            this.labelHomeTeam = new MaterialSkin.Controls.MaterialLabel();
            this.labelAwayTeam = new MaterialSkin.Controls.MaterialLabel();
            this.labelCountryId = new System.Windows.Forms.Label();
            this.labelLeagueId = new System.Windows.Forms.Label();
            this.labelHomeId = new System.Windows.Forms.Label();
            this.labelAwayId = new System.Windows.Forms.Label();
            this.comboBoxSport = new System.Windows.Forms.ComboBox();
            this.labelSport = new MaterialSkin.Controls.MaterialLabel();
            this.dateTimePickerMatchTime = new System.Windows.Forms.DateTimePicker();
            this.labelMatchTime = new MaterialSkin.Controls.MaterialLabel();
            this.labelHomeOdd = new MaterialSkin.Controls.MaterialLabel();
            this.labelDrawOdd = new MaterialSkin.Controls.MaterialLabel();
            this.labelAwayOdd = new MaterialSkin.Controls.MaterialLabel();
            this.groupBoxOdds = new System.Windows.Forms.GroupBox();
            this.numericAwayOdd = new System.Windows.Forms.NumericUpDown();
            this.numericDrawOdd = new System.Windows.Forms.NumericUpDown();
            this.numericHomeOdd = new System.Windows.Forms.NumericUpDown();
            this.groupBoxScores = new System.Windows.Forms.GroupBox();
            this.numericAwayScore = new System.Windows.Forms.NumericUpDown();
            this.numericHomeScore = new System.Windows.Forms.NumericUpDown();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.buttonSave = new MaterialSkin.Controls.MaterialButton();
            this.labelStatus = new MaterialSkin.Controls.MaterialLabel();
            this.checkboxCup = new MaterialSkin.Controls.MaterialCheckbox();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.groupBoxBasketballQuarters = new System.Windows.Forms.GroupBox();
            this.labelQ4 = new MaterialSkin.Controls.MaterialLabel();
            this.numericUpDownAQ4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHQ4 = new System.Windows.Forms.NumericUpDown();
            this.labelQ3 = new MaterialSkin.Controls.MaterialLabel();
            this.numericUpDownAQ3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHQ3 = new System.Windows.Forms.NumericUpDown();
            this.labelQ2 = new MaterialSkin.Controls.MaterialLabel();
            this.numericUpDownAQ2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHQ2 = new System.Windows.Forms.NumericUpDown();
            this.labelQ1 = new MaterialSkin.Controls.MaterialLabel();
            this.labelAwayQ = new MaterialSkin.Controls.MaterialLabel();
            this.labelHomeQ = new MaterialSkin.Controls.MaterialLabel();
            this.numericUpDownAQ1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHQ1 = new System.Windows.Forms.NumericUpDown();
            this.labelMatchId = new System.Windows.Forms.Label();
            this.labelMatchIdValue = new System.Windows.Forms.Label();
            this.groupBoxOdds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAwayOdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDrawOdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHomeOdd)).BeginInit();
            this.groupBoxScores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAwayScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHomeScore)).BeginInit();
            this.groupBoxBasketballQuarters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCountry
            // 
            this.labelCountry.AutoSize = true;
            this.labelCountry.Depth = 0;
            this.labelCountry.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelCountry.Location = new System.Drawing.Point(37, 116);
            this.labelCountry.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(56, 19);
            this.labelCountry.TabIndex = 0;
            this.labelCountry.Text = "Country";
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Location = new System.Drawing.Point(141, 115);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(332, 20);
            this.textBoxCountry.TabIndex = 2;
            this.textBoxCountry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxCountry_KeyDown);
            this.textBoxCountry.Leave += new System.EventHandler(this.textBoxCountry_Leave);
            // 
            // labelLeague
            // 
            this.labelLeague.AutoSize = true;
            this.labelLeague.Depth = 0;
            this.labelLeague.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelLeague.Location = new System.Drawing.Point(37, 145);
            this.labelLeague.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelLeague.Name = "labelLeague";
            this.labelLeague.Size = new System.Drawing.Size(53, 19);
            this.labelLeague.TabIndex = 2;
            this.labelLeague.Text = "League";
            // 
            // textBoxLeague
            // 
            this.textBoxLeague.Location = new System.Drawing.Point(141, 145);
            this.textBoxLeague.Name = "textBoxLeague";
            this.textBoxLeague.Size = new System.Drawing.Size(332, 20);
            this.textBoxLeague.TabIndex = 3;
            this.textBoxLeague.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxLeague_KeyDown);
            this.textBoxLeague.Leave += new System.EventHandler(this.textBoxLeague_Leave);
            // 
            // textBoxHomeTeam
            // 
            this.textBoxHomeTeam.Location = new System.Drawing.Point(141, 173);
            this.textBoxHomeTeam.Name = "textBoxHomeTeam";
            this.textBoxHomeTeam.Size = new System.Drawing.Size(332, 20);
            this.textBoxHomeTeam.TabIndex = 4;
            this.textBoxHomeTeam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxHomeTeam_KeyDown);
            this.textBoxHomeTeam.Leave += new System.EventHandler(this.textBoxHomeTeam_Leave);
            // 
            // textBoxAwayTeam
            // 
            this.textBoxAwayTeam.Location = new System.Drawing.Point(141, 204);
            this.textBoxAwayTeam.Name = "textBoxAwayTeam";
            this.textBoxAwayTeam.Size = new System.Drawing.Size(332, 20);
            this.textBoxAwayTeam.TabIndex = 5;
            this.textBoxAwayTeam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAwayTeam_KeyDown);
            this.textBoxAwayTeam.Leave += new System.EventHandler(this.textBoxAwayTeam_Leave);
            // 
            // labelHomeTeam
            // 
            this.labelHomeTeam.AutoSize = true;
            this.labelHomeTeam.Depth = 0;
            this.labelHomeTeam.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelHomeTeam.Location = new System.Drawing.Point(37, 174);
            this.labelHomeTeam.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelHomeTeam.Name = "labelHomeTeam";
            this.labelHomeTeam.Size = new System.Drawing.Size(88, 19);
            this.labelHomeTeam.TabIndex = 6;
            this.labelHomeTeam.Text = "Home Team";
            // 
            // labelAwayTeam
            // 
            this.labelAwayTeam.AutoSize = true;
            this.labelAwayTeam.Depth = 0;
            this.labelAwayTeam.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAwayTeam.Location = new System.Drawing.Point(37, 205);
            this.labelAwayTeam.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelAwayTeam.Name = "labelAwayTeam";
            this.labelAwayTeam.Size = new System.Drawing.Size(85, 19);
            this.labelAwayTeam.TabIndex = 7;
            this.labelAwayTeam.Text = "Away Team";
            // 
            // labelCountryId
            // 
            this.labelCountryId.AutoSize = true;
            this.labelCountryId.Location = new System.Drawing.Point(487, 118);
            this.labelCountryId.Name = "labelCountryId";
            this.labelCountryId.Size = new System.Drawing.Size(35, 13);
            this.labelCountryId.TabIndex = 100;
            this.labelCountryId.Text = "label1";
            // 
            // labelLeagueId
            // 
            this.labelLeagueId.AutoSize = true;
            this.labelLeagueId.Location = new System.Drawing.Point(488, 148);
            this.labelLeagueId.Name = "labelLeagueId";
            this.labelLeagueId.Size = new System.Drawing.Size(35, 13);
            this.labelLeagueId.TabIndex = 101;
            this.labelLeagueId.Text = "label2";
            // 
            // labelHomeId
            // 
            this.labelHomeId.AutoSize = true;
            this.labelHomeId.Location = new System.Drawing.Point(488, 176);
            this.labelHomeId.Name = "labelHomeId";
            this.labelHomeId.Size = new System.Drawing.Size(35, 13);
            this.labelHomeId.TabIndex = 102;
            this.labelHomeId.Text = "label3";
            // 
            // labelAwayId
            // 
            this.labelAwayId.AutoSize = true;
            this.labelAwayId.Location = new System.Drawing.Point(488, 207);
            this.labelAwayId.Name = "labelAwayId";
            this.labelAwayId.Size = new System.Drawing.Size(35, 13);
            this.labelAwayId.TabIndex = 103;
            this.labelAwayId.Text = "label4";
            // 
            // comboBoxSport
            // 
            this.comboBoxSport.FormattingEnabled = true;
            this.comboBoxSport.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSport.Location = new System.Drawing.Point(141, 78);
            this.comboBoxSport.Name = "comboBoxSport";
            this.comboBoxSport.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSport.TabIndex = 1;
            this.comboBoxSport.SelectedIndexChanged += new System.EventHandler(this.comboBoxSport_SelectedIndexChanged);
            // 
            // labelSport
            // 
            this.labelSport.AutoSize = true;
            this.labelSport.Depth = 0;
            this.labelSport.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelSport.Location = new System.Drawing.Point(37, 80);
            this.labelSport.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelSport.Name = "labelSport";
            this.labelSport.Size = new System.Drawing.Size(39, 19);
            this.labelSport.TabIndex = 13;
            this.labelSport.Text = "Sport";
            // 
            // dateTimePickerMatchTime
            // 
            this.dateTimePickerMatchTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dateTimePickerMatchTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMatchTime.Location = new System.Drawing.Point(141, 241);
            this.dateTimePickerMatchTime.Name = "dateTimePickerMatchTime";
            this.dateTimePickerMatchTime.Size = new System.Drawing.Size(160, 20);
            this.dateTimePickerMatchTime.TabIndex = 6;
            // 
            // labelMatchTime
            // 
            this.labelMatchTime.AutoSize = true;
            this.labelMatchTime.Depth = 0;
            this.labelMatchTime.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelMatchTime.Location = new System.Drawing.Point(37, 242);
            this.labelMatchTime.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelMatchTime.Name = "labelMatchTime";
            this.labelMatchTime.Size = new System.Drawing.Size(37, 19);
            this.labelMatchTime.TabIndex = 15;
            this.labelMatchTime.Text = "Time";
            // 
            // labelHomeOdd
            // 
            this.labelHomeOdd.AutoSize = true;
            this.labelHomeOdd.Depth = 0;
            this.labelHomeOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelHomeOdd.Location = new System.Drawing.Point(26, 26);
            this.labelHomeOdd.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelHomeOdd.Name = "labelHomeOdd";
            this.labelHomeOdd.Size = new System.Drawing.Size(43, 19);
            this.labelHomeOdd.TabIndex = 16;
            this.labelHomeOdd.Text = "Home";
            // 
            // labelDrawOdd
            // 
            this.labelDrawOdd.AutoSize = true;
            this.labelDrawOdd.Depth = 0;
            this.labelDrawOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelDrawOdd.Location = new System.Drawing.Point(26, 52);
            this.labelDrawOdd.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelDrawOdd.Name = "labelDrawOdd";
            this.labelDrawOdd.Size = new System.Drawing.Size(38, 19);
            this.labelDrawOdd.TabIndex = 18;
            this.labelDrawOdd.Text = "Draw";
            // 
            // labelAwayOdd
            // 
            this.labelAwayOdd.AutoSize = true;
            this.labelAwayOdd.Depth = 0;
            this.labelAwayOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAwayOdd.Location = new System.Drawing.Point(26, 78);
            this.labelAwayOdd.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelAwayOdd.Name = "labelAwayOdd";
            this.labelAwayOdd.Size = new System.Drawing.Size(40, 19);
            this.labelAwayOdd.TabIndex = 20;
            this.labelAwayOdd.Text = "Away";
            // 
            // groupBoxOdds
            // 
            this.groupBoxOdds.Controls.Add(this.numericAwayOdd);
            this.groupBoxOdds.Controls.Add(this.numericDrawOdd);
            this.groupBoxOdds.Controls.Add(this.numericHomeOdd);
            this.groupBoxOdds.Controls.Add(this.labelDrawOdd);
            this.groupBoxOdds.Controls.Add(this.labelHomeOdd);
            this.groupBoxOdds.Controls.Add(this.labelAwayOdd);
            this.groupBoxOdds.Location = new System.Drawing.Point(40, 315);
            this.groupBoxOdds.Name = "groupBoxOdds";
            this.groupBoxOdds.Size = new System.Drawing.Size(174, 114);
            this.groupBoxOdds.TabIndex = 22;
            this.groupBoxOdds.TabStop = false;
            this.groupBoxOdds.Text = "Odds";
            // 
            // numericAwayOdd
            // 
            this.numericAwayOdd.DecimalPlaces = 2;
            this.numericAwayOdd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericAwayOdd.Location = new System.Drawing.Point(88, 77);
            this.numericAwayOdd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAwayOdd.Name = "numericAwayOdd";
            this.numericAwayOdd.Size = new System.Drawing.Size(63, 20);
            this.numericAwayOdd.TabIndex = 11;
            this.numericAwayOdd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAwayOdd.Enter += new System.EventHandler(this.numericAwayOdd_Enter);
            // 
            // numericDrawOdd
            // 
            this.numericDrawOdd.DecimalPlaces = 2;
            this.numericDrawOdd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericDrawOdd.Location = new System.Drawing.Point(88, 51);
            this.numericDrawOdd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericDrawOdd.Name = "numericDrawOdd";
            this.numericDrawOdd.Size = new System.Drawing.Size(63, 20);
            this.numericDrawOdd.TabIndex = 10;
            this.numericDrawOdd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericDrawOdd.Enter += new System.EventHandler(this.numericDrawOdd_Enter);
            // 
            // numericHomeOdd
            // 
            this.numericHomeOdd.DecimalPlaces = 2;
            this.numericHomeOdd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericHomeOdd.Location = new System.Drawing.Point(88, 25);
            this.numericHomeOdd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericHomeOdd.Name = "numericHomeOdd";
            this.numericHomeOdd.Size = new System.Drawing.Size(63, 20);
            this.numericHomeOdd.TabIndex = 9;
            this.numericHomeOdd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericHomeOdd.Enter += new System.EventHandler(this.numericHomeOdd_Enter);
            // 
            // groupBoxScores
            // 
            this.groupBoxScores.Controls.Add(this.numericAwayScore);
            this.groupBoxScores.Controls.Add(this.numericHomeScore);
            this.groupBoxScores.Controls.Add(this.materialLabel2);
            this.groupBoxScores.Controls.Add(this.materialLabel3);
            this.groupBoxScores.Location = new System.Drawing.Point(299, 315);
            this.groupBoxScores.Name = "groupBoxScores";
            this.groupBoxScores.Size = new System.Drawing.Size(174, 114);
            this.groupBoxScores.TabIndex = 25;
            this.groupBoxScores.TabStop = false;
            this.groupBoxScores.Text = "Scores";
            // 
            // numericAwayScore
            // 
            this.numericAwayScore.Location = new System.Drawing.Point(85, 77);
            this.numericAwayScore.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericAwayScore.Name = "numericAwayScore";
            this.numericAwayScore.Size = new System.Drawing.Size(63, 20);
            this.numericAwayScore.TabIndex = 13;
            this.numericAwayScore.Enter += new System.EventHandler(this.numericAwayScore_Enter);
            // 
            // numericHomeScore
            // 
            this.numericHomeScore.Location = new System.Drawing.Point(85, 25);
            this.numericHomeScore.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericHomeScore.Name = "numericHomeScore";
            this.numericHomeScore.Size = new System.Drawing.Size(63, 20);
            this.numericHomeScore.TabIndex = 12;
            this.numericHomeScore.Enter += new System.EventHandler(this.numericHomeScore_Enter);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel2.Location = new System.Drawing.Point(23, 26);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(43, 19);
            this.materialLabel2.TabIndex = 16;
            this.materialLabel2.Text = "Home";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel3.Location = new System.Drawing.Point(23, 78);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(40, 19);
            this.materialLabel3.TabIndex = 20;
            this.materialLabel3.Text = "Away";
            // 
            // buttonSave
            // 
            this.buttonSave.AutoSize = false;
            this.buttonSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSave.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonSave.Depth = 0;
            this.buttonSave.HighEmphasis = true;
            this.buttonSave.Icon = null;
            this.buttonSave.Location = new System.Drawing.Point(175, 567);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonSave.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonSave.Size = new System.Drawing.Size(199, 36);
            this.buttonSave.TabIndex = 22;
            this.buttonSave.Text = "Save Match";
            this.buttonSave.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonSave.UseAccentColor = false;
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Depth = 0;
            this.labelStatus.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelStatus.Location = new System.Drawing.Point(40, 272);
            this.labelStatus.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(47, 19);
            this.labelStatus.TabIndex = 27;
            this.labelStatus.Text = "Status";
            // 
            // checkboxCup
            // 
            this.checkboxCup.AutoSize = true;
            this.checkboxCup.Depth = 0;
            this.checkboxCup.Location = new System.Drawing.Point(325, 263);
            this.checkboxCup.Margin = new System.Windows.Forms.Padding(0);
            this.checkboxCup.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkboxCup.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkboxCup.Name = "checkboxCup";
            this.checkboxCup.ReadOnly = false;
            this.checkboxCup.Ripple = true;
            this.checkboxCup.Size = new System.Drawing.Size(63, 37);
            this.checkboxCup.TabIndex = 8;
            this.checkboxCup.Text = "Cup";
            this.checkboxCup.UseVisualStyleBackColor = true;
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Items.AddRange(new object[] {
            "FT",
            "Finished",
            "After Over",
            "Not Started",
            ""});
            this.comboBoxStatus.Location = new System.Drawing.Point(141, 271);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(121, 21);
            this.comboBoxStatus.TabIndex = 7;
            this.comboBoxStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatus_SelectedIndexChanged);
            // 
            // groupBoxBasketballQuarters
            // 
            this.groupBoxBasketballQuarters.Controls.Add(this.labelQ4);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownAQ4);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownHQ4);
            this.groupBoxBasketballQuarters.Controls.Add(this.labelQ3);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownAQ3);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownHQ3);
            this.groupBoxBasketballQuarters.Controls.Add(this.labelQ2);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownAQ2);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownHQ2);
            this.groupBoxBasketballQuarters.Controls.Add(this.labelQ1);
            this.groupBoxBasketballQuarters.Controls.Add(this.labelAwayQ);
            this.groupBoxBasketballQuarters.Controls.Add(this.labelHomeQ);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownAQ1);
            this.groupBoxBasketballQuarters.Controls.Add(this.numericUpDownHQ1);
            this.groupBoxBasketballQuarters.Location = new System.Drawing.Point(43, 447);
            this.groupBoxBasketballQuarters.Name = "groupBoxBasketballQuarters";
            this.groupBoxBasketballQuarters.Size = new System.Drawing.Size(430, 111);
            this.groupBoxBasketballQuarters.TabIndex = 104;
            this.groupBoxBasketballQuarters.TabStop = false;
            this.groupBoxBasketballQuarters.Text = "Basketball Quarters Scores";
            // 
            // labelQ4
            // 
            this.labelQ4.AutoSize = true;
            this.labelQ4.Depth = 0;
            this.labelQ4.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelQ4.Location = new System.Drawing.Point(363, 28);
            this.labelQ4.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelQ4.Name = "labelQ4";
            this.labelQ4.Size = new System.Drawing.Size(21, 19);
            this.labelQ4.TabIndex = 107;
            this.labelQ4.Text = "Q4";
            // 
            // numericUpDownAQ4
            // 
            this.numericUpDownAQ4.Location = new System.Drawing.Point(344, 81);
            this.numericUpDownAQ4.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownAQ4.Name = "numericUpDownAQ4";
            this.numericUpDownAQ4.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownAQ4.TabIndex = 21;
            this.numericUpDownAQ4.Enter += new System.EventHandler(this.numericUpDownAQ4_Enter);
            // 
            // numericUpDownHQ4
            // 
            this.numericUpDownHQ4.Location = new System.Drawing.Point(344, 52);
            this.numericUpDownHQ4.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownHQ4.Name = "numericUpDownHQ4";
            this.numericUpDownHQ4.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownHQ4.TabIndex = 20;
            this.numericUpDownHQ4.Enter += new System.EventHandler(this.numericUpDownHQ4_Enter);
            // 
            // labelQ3
            // 
            this.labelQ3.AutoSize = true;
            this.labelQ3.Depth = 0;
            this.labelQ3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelQ3.Location = new System.Drawing.Point(278, 27);
            this.labelQ3.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelQ3.Name = "labelQ3";
            this.labelQ3.Size = new System.Drawing.Size(21, 19);
            this.labelQ3.TabIndex = 106;
            this.labelQ3.Text = "Q3";
            // 
            // numericUpDownAQ3
            // 
            this.numericUpDownAQ3.Location = new System.Drawing.Point(259, 80);
            this.numericUpDownAQ3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownAQ3.Name = "numericUpDownAQ3";
            this.numericUpDownAQ3.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownAQ3.TabIndex = 19;
            this.numericUpDownAQ3.Enter += new System.EventHandler(this.numericUpDownAQ3_Enter);
            // 
            // numericUpDownHQ3
            // 
            this.numericUpDownHQ3.Location = new System.Drawing.Point(259, 51);
            this.numericUpDownHQ3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownHQ3.Name = "numericUpDownHQ3";
            this.numericUpDownHQ3.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownHQ3.TabIndex = 18;
            this.numericUpDownHQ3.Enter += new System.EventHandler(this.numericUpDownHQ3_Enter);
            // 
            // labelQ2
            // 
            this.labelQ2.AutoSize = true;
            this.labelQ2.Depth = 0;
            this.labelQ2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelQ2.Location = new System.Drawing.Point(193, 28);
            this.labelQ2.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelQ2.Name = "labelQ2";
            this.labelQ2.Size = new System.Drawing.Size(21, 19);
            this.labelQ2.TabIndex = 105;
            this.labelQ2.Text = "Q2";
            // 
            // numericUpDownAQ2
            // 
            this.numericUpDownAQ2.Location = new System.Drawing.Point(174, 81);
            this.numericUpDownAQ2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownAQ2.Name = "numericUpDownAQ2";
            this.numericUpDownAQ2.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownAQ2.TabIndex = 17;
            this.numericUpDownAQ2.Enter += new System.EventHandler(this.numericUpDownAQ2_Enter);
            // 
            // numericUpDownHQ2
            // 
            this.numericUpDownHQ2.Location = new System.Drawing.Point(174, 52);
            this.numericUpDownHQ2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownHQ2.Name = "numericUpDownHQ2";
            this.numericUpDownHQ2.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownHQ2.TabIndex = 16;
            this.numericUpDownHQ2.Enter += new System.EventHandler(this.numericUpDownHQ2_Enter);
            // 
            // labelQ1
            // 
            this.labelQ1.AutoSize = true;
            this.labelQ1.Depth = 0;
            this.labelQ1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelQ1.Location = new System.Drawing.Point(107, 28);
            this.labelQ1.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelQ1.Name = "labelQ1";
            this.labelQ1.Size = new System.Drawing.Size(21, 19);
            this.labelQ1.TabIndex = 104;
            this.labelQ1.Text = "Q1";
            // 
            // labelAwayQ
            // 
            this.labelAwayQ.AutoSize = true;
            this.labelAwayQ.Depth = 0;
            this.labelAwayQ.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAwayQ.Location = new System.Drawing.Point(22, 81);
            this.labelAwayQ.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelAwayQ.Name = "labelAwayQ";
            this.labelAwayQ.Size = new System.Drawing.Size(40, 19);
            this.labelAwayQ.TabIndex = 21;
            this.labelAwayQ.Text = "Away";
            // 
            // labelHomeQ
            // 
            this.labelHomeQ.AutoSize = true;
            this.labelHomeQ.Depth = 0;
            this.labelHomeQ.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelHomeQ.Location = new System.Drawing.Point(22, 52);
            this.labelHomeQ.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelHomeQ.Name = "labelHomeQ";
            this.labelHomeQ.Size = new System.Drawing.Size(43, 19);
            this.labelHomeQ.TabIndex = 21;
            this.labelHomeQ.Text = "Home";
            // 
            // numericUpDownAQ1
            // 
            this.numericUpDownAQ1.Location = new System.Drawing.Point(88, 81);
            this.numericUpDownAQ1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownAQ1.Name = "numericUpDownAQ1";
            this.numericUpDownAQ1.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownAQ1.TabIndex = 15;
            this.numericUpDownAQ1.Enter += new System.EventHandler(this.numericUpDownAQ1_Enter);
            // 
            // numericUpDownHQ1
            // 
            this.numericUpDownHQ1.Location = new System.Drawing.Point(88, 52);
            this.numericUpDownHQ1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownHQ1.Name = "numericUpDownHQ1";
            this.numericUpDownHQ1.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownHQ1.TabIndex = 14;
            this.numericUpDownHQ1.Enter += new System.EventHandler(this.numericUpDownHQ1_Enter);
            // 
            // labelMatchId
            // 
            this.labelMatchId.AutoSize = true;
            this.labelMatchId.Location = new System.Drawing.Point(418, 78);
            this.labelMatchId.Name = "labelMatchId";
            this.labelMatchId.Size = new System.Drawing.Size(46, 13);
            this.labelMatchId.TabIndex = 105;
            this.labelMatchId.Text = "MatchId";
            // 
            // labelMatchIdValue
            // 
            this.labelMatchIdValue.AutoSize = true;
            this.labelMatchIdValue.Location = new System.Drawing.Point(487, 78);
            this.labelMatchIdValue.Name = "labelMatchIdValue";
            this.labelMatchIdValue.Size = new System.Drawing.Size(73, 13);
            this.labelMatchIdValue.TabIndex = 106;
            this.labelMatchIdValue.Text = "MatchIdValue";
            // 
            // MatchDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 612);
            this.Controls.Add(this.labelMatchIdValue);
            this.Controls.Add(this.labelMatchId);
            this.Controls.Add(this.groupBoxBasketballQuarters);
            this.Controls.Add(this.comboBoxStatus);
            this.Controls.Add(this.checkboxCup);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBoxScores);
            this.Controls.Add(this.groupBoxOdds);
            this.Controls.Add(this.labelMatchTime);
            this.Controls.Add(this.dateTimePickerMatchTime);
            this.Controls.Add(this.labelSport);
            this.Controls.Add(this.comboBoxSport);
            this.Controls.Add(this.labelAwayId);
            this.Controls.Add(this.labelHomeId);
            this.Controls.Add(this.labelLeagueId);
            this.Controls.Add(this.labelCountryId);
            this.Controls.Add(this.labelAwayTeam);
            this.Controls.Add(this.labelHomeTeam);
            this.Controls.Add(this.textBoxAwayTeam);
            this.Controls.Add(this.textBoxHomeTeam);
            this.Controls.Add(this.textBoxLeague);
            this.Controls.Add(this.labelLeague);
            this.Controls.Add(this.textBoxCountry);
            this.Controls.Add(this.labelCountry);
            this.Name = "MatchDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MatchDetails";
            this.groupBoxOdds.ResumeLayout(false);
            this.groupBoxOdds.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAwayOdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDrawOdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHomeOdd)).EndInit();
            this.groupBoxScores.ResumeLayout(false);
            this.groupBoxScores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAwayScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHomeScore)).EndInit();
            this.groupBoxBasketballQuarters.ResumeLayout(false);
            this.groupBoxBasketballQuarters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAQ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHQ1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel labelCountry;
        private System.Windows.Forms.TextBox textBoxCountry;
        private MaterialSkin.Controls.MaterialLabel labelLeague;
        private System.Windows.Forms.TextBox textBoxLeague;
        private System.Windows.Forms.TextBox textBoxHomeTeam;
        private System.Windows.Forms.TextBox textBoxAwayTeam;
        private MaterialSkin.Controls.MaterialLabel labelHomeTeam;
        private MaterialSkin.Controls.MaterialLabel labelAwayTeam;
        private System.Windows.Forms.Label labelCountryId;
        private System.Windows.Forms.Label labelLeagueId;
        private System.Windows.Forms.Label labelHomeId;
        private System.Windows.Forms.Label labelAwayId;
        private System.Windows.Forms.ComboBox comboBoxSport;
        private MaterialSkin.Controls.MaterialLabel labelSport;
        private System.Windows.Forms.DateTimePicker dateTimePickerMatchTime;
        private MaterialSkin.Controls.MaterialLabel labelMatchTime;
        private MaterialSkin.Controls.MaterialLabel labelHomeOdd;
        private MaterialSkin.Controls.MaterialLabel labelDrawOdd;
        private MaterialSkin.Controls.MaterialLabel labelAwayOdd;
        private System.Windows.Forms.GroupBox groupBoxOdds;
        private System.Windows.Forms.NumericUpDown numericAwayOdd;
        private System.Windows.Forms.NumericUpDown numericDrawOdd;
        private System.Windows.Forms.NumericUpDown numericHomeOdd;
        private System.Windows.Forms.GroupBox groupBoxScores;
        private System.Windows.Forms.NumericUpDown numericAwayScore;
        private System.Windows.Forms.NumericUpDown numericHomeScore;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialButton buttonSave;
        private MaterialSkin.Controls.MaterialLabel labelStatus;
        private MaterialSkin.Controls.MaterialCheckbox checkboxCup;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.GroupBox groupBoxBasketballQuarters;
        private MaterialSkin.Controls.MaterialLabel labelAwayQ;
        private MaterialSkin.Controls.MaterialLabel labelHomeQ;
        private System.Windows.Forms.NumericUpDown numericUpDownAQ1;
        private System.Windows.Forms.NumericUpDown numericUpDownHQ1;
        private MaterialSkin.Controls.MaterialLabel labelQ1;
        private MaterialSkin.Controls.MaterialLabel labelQ4;
        private System.Windows.Forms.NumericUpDown numericUpDownAQ4;
        private System.Windows.Forms.NumericUpDown numericUpDownHQ4;
        private MaterialSkin.Controls.MaterialLabel labelQ3;
        private System.Windows.Forms.NumericUpDown numericUpDownAQ3;
        private System.Windows.Forms.NumericUpDown numericUpDownHQ3;
        private MaterialSkin.Controls.MaterialLabel labelQ2;
        private System.Windows.Forms.NumericUpDown numericUpDownAQ2;
        private System.Windows.Forms.NumericUpDown numericUpDownHQ2;
        private System.Windows.Forms.Label labelMatchId;
        private System.Windows.Forms.Label labelMatchIdValue;
    }
}