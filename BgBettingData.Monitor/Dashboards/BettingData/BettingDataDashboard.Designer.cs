﻿namespace BgBettingData.Monitor
{
    partial class BettingDataDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.labelAwayOdd = new System.Windows.Forms.Label();
            this.labelDrawOdd = new System.Windows.Forms.Label();
            this.labelHomeOdd = new System.Windows.Forms.Label();
            this.numericAwayOdd = new System.Windows.Forms.NumericUpDown();
            this.numericDrawOdd = new System.Windows.Forms.NumericUpDown();
            this.numericHomeOdd = new System.Windows.Forms.NumericUpDown();
            this.buttonGo = new System.Windows.Forms.Button();
            this.textBoxTeamB = new System.Windows.Forms.TextBox();
            this.labelTeamB = new System.Windows.Forms.Label();
            this.textBoxTeamA = new System.Windows.Forms.TextBox();
            this.labelTeamA = new System.Windows.Forms.Label();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.panelGeneralData = new System.Windows.Forms.Panel();
            this.labelRedLinesB = new System.Windows.Forms.Label();
            this.labelRedLinesA = new System.Windows.Forms.Label();
            this.labelGamesH2H = new System.Windows.Forms.Label();
            this.labelGamesB = new System.Windows.Forms.Label();
            this.labelGamesA = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageFullTime = new System.Windows.Forms.TabPage();
            this.tabControlFullTime = new System.Windows.Forms.TabControl();
            this.tabPageHomeWin = new System.Windows.Forms.TabPage();
            this.dgvFullTimeHomeWin = new System.Windows.Forms.DataGridView();
            this.tabPageH2HHomeWin = new System.Windows.Forms.TabPage();
            this.dgvFullTimeHomeWinH2H = new System.Windows.Forms.DataGridView();
            this.tabPageDraw = new System.Windows.Forms.TabPage();
            this.dgvFullTimeHomeDrawAwayDraw = new System.Windows.Forms.DataGridView();
            this.tabPageH2HDraw = new System.Windows.Forms.TabPage();
            this.dgvFullTimeDrawH2H = new System.Windows.Forms.DataGridView();
            this.tabPageAwayWin = new System.Windows.Forms.TabPage();
            this.dgvFullTimeAwayWin = new System.Windows.Forms.DataGridView();
            this.tabPageH2HAwayWin = new System.Windows.Forms.TabPage();
            this.dgvFullTimeAwayWinH2H = new System.Windows.Forms.DataGridView();
            this.tabPage3_5 = new System.Windows.Forms.TabPage();
            this.tabControl35OverUnder = new System.Windows.Forms.TabControl();
            this.tabPageOver3_5 = new System.Windows.Forms.TabPage();
            this.dvgOver3_5 = new System.Windows.Forms.DataGridView();
            this.tabPageUnder3_5 = new System.Windows.Forms.TabPage();
            this.dvgUnder3_5 = new System.Windows.Forms.DataGridView();
            this.tabPageOver3_5H2H = new System.Windows.Forms.TabPage();
            this.dvgOver3_5H2H = new System.Windows.Forms.DataGridView();
            this.tabPageUnder3_5H2H = new System.Windows.Forms.TabPage();
            this.dvgUnder3_5H2H = new System.Windows.Forms.DataGridView();
            this.tabPage2_5 = new System.Windows.Forms.TabPage();
            this.tabControl25OverUnder = new System.Windows.Forms.TabControl();
            this.tabPageOver2_5 = new System.Windows.Forms.TabPage();
            this.dvgOver2_5 = new System.Windows.Forms.DataGridView();
            this.tabPageUnder2_5 = new System.Windows.Forms.TabPage();
            this.dvgUnder2_5 = new System.Windows.Forms.DataGridView();
            this.tabPageOver2_5H2H = new System.Windows.Forms.TabPage();
            this.dvgOver2_5H2H = new System.Windows.Forms.DataGridView();
            this.tabPageUnder2_5H2H = new System.Windows.Forms.TabPage();
            this.dvgUnder2_5H2H = new System.Windows.Forms.DataGridView();
            this.tabPage1_5 = new System.Windows.Forms.TabPage();
            this.tabControl15OverUnder = new System.Windows.Forms.TabControl();
            this.tabPageOver1_5 = new System.Windows.Forms.TabPage();
            this.dvgOver1_5 = new System.Windows.Forms.DataGridView();
            this.tabPageUnder1_5 = new System.Windows.Forms.TabPage();
            this.dvgUnder1_5 = new System.Windows.Forms.DataGridView();
            this.tabPageOver1_5H2H = new System.Windows.Forms.TabPage();
            this.dvgOver1_5H2H = new System.Windows.Forms.DataGridView();
            this.tabPageUnder1_5H2H = new System.Windows.Forms.TabPage();
            this.dvgUnder1_5H2H = new System.Windows.Forms.DataGridView();
            this.tabPageRange = new System.Windows.Forms.TabPage();
            this.tabControlRange = new System.Windows.Forms.TabControl();
            this.tabPageRange0_1 = new System.Windows.Forms.TabPage();
            this.dvg01Range = new System.Windows.Forms.DataGridView();
            this.tabPageRange2_3 = new System.Windows.Forms.TabPage();
            this.dvg23Range = new System.Windows.Forms.DataGridView();
            this.tabPageRange4Plus = new System.Windows.Forms.TabPage();
            this.dvg4PlusRange = new System.Windows.Forms.DataGridView();
            this.tabPageRange0_1H2H = new System.Windows.Forms.TabPage();
            this.dvg01H2HRange = new System.Windows.Forms.DataGridView();
            this.tabPageRange2_3H2H = new System.Windows.Forms.TabPage();
            this.dvg23H2HRange = new System.Windows.Forms.DataGridView();
            this.tabPageRange4PlusH2H = new System.Windows.Forms.TabPage();
            this.dvg4PlusH2HRange = new System.Windows.Forms.DataGridView();
            this.tabPageBothScore = new System.Windows.Forms.TabPage();
            this.tabControlBothScore = new System.Windows.Forms.TabControl();
            this.tabPageBothScoreYes = new System.Windows.Forms.TabPage();
            this.tabPageBothScoreNo = new System.Windows.Forms.TabPage();
            this.tabPageBothScoreYesH2H = new System.Windows.Forms.TabPage();
            this.tabPageBothScoreNoH2H = new System.Windows.Forms.TabPage();
            this.tabPageWinDrawLose = new System.Windows.Forms.TabPage();
            this.tabControlWinDrawLose = new System.Windows.Forms.TabControl();
            this.tabPageWinDraw = new System.Windows.Forms.TabPage();
            this.tabPageNoDraw = new System.Windows.Forms.TabPage();
            this.tabPageDrawLose = new System.Windows.Forms.TabPage();
            this.tabPageWinDrawH2H = new System.Windows.Forms.TabPage();
            this.tabPageNoDrawH2H = new System.Windows.Forms.TabPage();
            this.tabPageDrawLoseH2H = new System.Windows.Forms.TabPage();
            this.dvgWinDraw = new System.Windows.Forms.DataGridView();
            this.dvgNoDraw = new System.Windows.Forms.DataGridView();
            this.dvgDrawLose = new System.Windows.Forms.DataGridView();
            this.dvgWinDrawH2H = new System.Windows.Forms.DataGridView();
            this.dvgNoDrawH2H = new System.Windows.Forms.DataGridView();
            this.dvgDrawLoseH2H = new System.Windows.Forms.DataGridView();
            this.dvgBothScoreYes = new System.Windows.Forms.DataGridView();
            this.dvgBothScoreNo = new System.Windows.Forms.DataGridView();
            this.dvgBothScoreYesH2H = new System.Windows.Forms.DataGridView();
            this.dvgBothScoreNoH2H = new System.Windows.Forms.DataGridView();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAwayOdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDrawOdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHomeOdd)).BeginInit();
            this.panelGeneralData.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageFullTime.SuspendLayout();
            this.tabControlFullTime.SuspendLayout();
            this.tabPageHomeWin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeHomeWin)).BeginInit();
            this.tabPageH2HHomeWin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeHomeWinH2H)).BeginInit();
            this.tabPageDraw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeHomeDrawAwayDraw)).BeginInit();
            this.tabPageH2HDraw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeDrawH2H)).BeginInit();
            this.tabPageAwayWin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeAwayWin)).BeginInit();
            this.tabPageH2HAwayWin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeAwayWinH2H)).BeginInit();
            this.tabPage3_5.SuspendLayout();
            this.tabControl35OverUnder.SuspendLayout();
            this.tabPageOver3_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver3_5)).BeginInit();
            this.tabPageUnder3_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder3_5)).BeginInit();
            this.tabPageOver3_5H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver3_5H2H)).BeginInit();
            this.tabPageUnder3_5H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder3_5H2H)).BeginInit();
            this.tabPage2_5.SuspendLayout();
            this.tabControl25OverUnder.SuspendLayout();
            this.tabPageOver2_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver2_5)).BeginInit();
            this.tabPageUnder2_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder2_5)).BeginInit();
            this.tabPageOver2_5H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver2_5H2H)).BeginInit();
            this.tabPageUnder2_5H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder2_5H2H)).BeginInit();
            this.tabPage1_5.SuspendLayout();
            this.tabControl15OverUnder.SuspendLayout();
            this.tabPageOver1_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver1_5)).BeginInit();
            this.tabPageUnder1_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder1_5)).BeginInit();
            this.tabPageOver1_5H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver1_5H2H)).BeginInit();
            this.tabPageUnder1_5H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder1_5H2H)).BeginInit();
            this.tabPageRange.SuspendLayout();
            this.tabControlRange.SuspendLayout();
            this.tabPageRange0_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg01Range)).BeginInit();
            this.tabPageRange2_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg23Range)).BeginInit();
            this.tabPageRange4Plus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg4PlusRange)).BeginInit();
            this.tabPageRange0_1H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg01H2HRange)).BeginInit();
            this.tabPageRange2_3H2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg23H2HRange)).BeginInit();
            this.tabPageRange4PlusH2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg4PlusH2HRange)).BeginInit();
            this.tabPageBothScore.SuspendLayout();
            this.tabControlBothScore.SuspendLayout();
            this.tabPageBothScoreYes.SuspendLayout();
            this.tabPageBothScoreNo.SuspendLayout();
            this.tabPageBothScoreYesH2H.SuspendLayout();
            this.tabPageBothScoreNoH2H.SuspendLayout();
            this.tabPageWinDrawLose.SuspendLayout();
            this.tabControlWinDrawLose.SuspendLayout();
            this.tabPageWinDraw.SuspendLayout();
            this.tabPageNoDraw.SuspendLayout();
            this.tabPageDrawLose.SuspendLayout();
            this.tabPageWinDrawH2H.SuspendLayout();
            this.tabPageNoDrawH2H.SuspendLayout();
            this.tabPageDrawLoseH2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgWinDraw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgNoDraw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgDrawLose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgWinDrawH2H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgNoDrawH2H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgDrawLoseH2H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreYesH2H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreNoH2H)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.AutoSize = true;
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panelTop.Controls.Add(this.labelAwayOdd);
            this.panelTop.Controls.Add(this.labelDrawOdd);
            this.panelTop.Controls.Add(this.labelHomeOdd);
            this.panelTop.Controls.Add(this.numericAwayOdd);
            this.panelTop.Controls.Add(this.numericDrawOdd);
            this.panelTop.Controls.Add(this.numericHomeOdd);
            this.panelTop.Controls.Add(this.buttonGo);
            this.panelTop.Controls.Add(this.textBoxTeamB);
            this.panelTop.Controls.Add(this.labelTeamB);
            this.panelTop.Controls.Add(this.textBoxTeamA);
            this.panelTop.Controls.Add(this.labelTeamA);
            this.panelTop.Controls.Add(this.comboBoxSportType);
            this.panelTop.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.panelTop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelTop.Location = new System.Drawing.Point(6, 67);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(950, 82);
            this.panelTop.TabIndex = 0;
            // 
            // labelAwayOdd
            // 
            this.labelAwayOdd.AutoSize = true;
            this.labelAwayOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelAwayOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAwayOdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelAwayOdd.Location = new System.Drawing.Point(758, 57);
            this.labelAwayOdd.Name = "labelAwayOdd";
            this.labelAwayOdd.Size = new System.Drawing.Size(43, 17);
            this.labelAwayOdd.TabIndex = 28;
            this.labelAwayOdd.Text = "Away";
            // 
            // labelDrawOdd
            // 
            this.labelDrawOdd.AutoSize = true;
            this.labelDrawOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelDrawOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelDrawOdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelDrawOdd.Location = new System.Drawing.Point(758, 32);
            this.labelDrawOdd.Name = "labelDrawOdd";
            this.labelDrawOdd.Size = new System.Drawing.Size(41, 17);
            this.labelDrawOdd.TabIndex = 27;
            this.labelDrawOdd.Text = "Draw";
            // 
            // labelHomeOdd
            // 
            this.labelHomeOdd.AutoSize = true;
            this.labelHomeOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelHomeOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelHomeOdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelHomeOdd.Location = new System.Drawing.Point(758, 6);
            this.labelHomeOdd.Name = "labelHomeOdd";
            this.labelHomeOdd.Size = new System.Drawing.Size(45, 17);
            this.labelHomeOdd.TabIndex = 26;
            this.labelHomeOdd.Text = "Home";
            // 
            // numericAwayOdd
            // 
            this.numericAwayOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.numericAwayOdd.DecimalPlaces = 2;
            this.numericAwayOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.numericAwayOdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numericAwayOdd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericAwayOdd.Location = new System.Drawing.Point(809, 55);
            this.numericAwayOdd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAwayOdd.Name = "numericAwayOdd";
            this.numericAwayOdd.Size = new System.Drawing.Size(63, 24);
            this.numericAwayOdd.TabIndex = 6;
            this.numericAwayOdd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAwayOdd.Enter += new System.EventHandler(this.numericAwayOdd_Enter);
            // 
            // numericDrawOdd
            // 
            this.numericDrawOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.numericDrawOdd.DecimalPlaces = 2;
            this.numericDrawOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.numericDrawOdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numericDrawOdd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericDrawOdd.Location = new System.Drawing.Point(809, 29);
            this.numericDrawOdd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericDrawOdd.Name = "numericDrawOdd";
            this.numericDrawOdd.Size = new System.Drawing.Size(63, 24);
            this.numericDrawOdd.TabIndex = 5;
            this.numericDrawOdd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericDrawOdd.Enter += new System.EventHandler(this.numericDrawOdd_Enter);
            // 
            // numericHomeOdd
            // 
            this.numericHomeOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.numericHomeOdd.DecimalPlaces = 2;
            this.numericHomeOdd.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.numericHomeOdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numericHomeOdd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericHomeOdd.Location = new System.Drawing.Point(809, 3);
            this.numericHomeOdd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericHomeOdd.Name = "numericHomeOdd";
            this.numericHomeOdd.Size = new System.Drawing.Size(63, 24);
            this.numericHomeOdd.TabIndex = 4;
            this.numericHomeOdd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericHomeOdd.Enter += new System.EventHandler(this.numericHomeOdd_Enter);
            // 
            // buttonGo
            // 
            this.buttonGo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonGo.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.buttonGo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonGo.Location = new System.Drawing.Point(878, 3);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(68, 76);
            this.buttonGo.TabIndex = 7;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = false;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // textBoxTeamB
            // 
            this.textBoxTeamB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.textBoxTeamB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxTeamB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBoxTeamB.Location = new System.Drawing.Point(197, 54);
            this.textBoxTeamB.Name = "textBoxTeamB";
            this.textBoxTeamB.Size = new System.Drawing.Size(549, 24);
            this.textBoxTeamB.TabIndex = 3;
            this.textBoxTeamB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTeamB_KeyDown);
            this.textBoxTeamB.Leave += new System.EventHandler(this.textBoxTeamB_Leave);
            // 
            // labelTeamB
            // 
            this.labelTeamB.AutoSize = true;
            this.labelTeamB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelTeamB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelTeamB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelTeamB.Location = new System.Drawing.Point(130, 57);
            this.labelTeamB.Name = "labelTeamB";
            this.labelTeamB.Size = new System.Drawing.Size(57, 17);
            this.labelTeamB.TabIndex = 20;
            this.labelTeamB.Text = "Team B:";
            // 
            // textBoxTeamA
            // 
            this.textBoxTeamA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.textBoxTeamA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxTeamA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBoxTeamA.Location = new System.Drawing.Point(197, 3);
            this.textBoxTeamA.Name = "textBoxTeamA";
            this.textBoxTeamA.Size = new System.Drawing.Size(549, 24);
            this.textBoxTeamA.TabIndex = 2;
            this.textBoxTeamA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTeamA_KeyDown);
            this.textBoxTeamA.Leave += new System.EventHandler(this.textBoxTeamA_Leave);
            // 
            // labelTeamA
            // 
            this.labelTeamA.AutoSize = true;
            this.labelTeamA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelTeamA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelTeamA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelTeamA.Location = new System.Drawing.Point(130, 6);
            this.labelTeamA.Name = "labelTeamA";
            this.labelTeamA.Size = new System.Drawing.Size(57, 17);
            this.labelTeamA.TabIndex = 18;
            this.labelTeamA.Text = "Team A:";
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.comboBoxSportType.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.comboBoxSportType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(13, 3);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 25);
            this.comboBoxSportType.TabIndex = 1;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // panelGeneralData
            // 
            this.panelGeneralData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panelGeneralData.Controls.Add(this.labelRedLinesB);
            this.panelGeneralData.Controls.Add(this.labelRedLinesA);
            this.panelGeneralData.Controls.Add(this.labelGamesH2H);
            this.panelGeneralData.Controls.Add(this.labelGamesB);
            this.panelGeneralData.Controls.Add(this.labelGamesA);
            this.panelGeneralData.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.panelGeneralData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelGeneralData.Location = new System.Drawing.Point(7, 150);
            this.panelGeneralData.Name = "panelGeneralData";
            this.panelGeneralData.Size = new System.Drawing.Size(950, 30);
            this.panelGeneralData.TabIndex = 1;
            // 
            // labelRedLinesB
            // 
            this.labelRedLinesB.AutoSize = true;
            this.labelRedLinesB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelRedLinesB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelRedLinesB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelRedLinesB.Location = new System.Drawing.Point(714, 6);
            this.labelRedLinesB.Name = "labelRedLinesB";
            this.labelRedLinesB.Size = new System.Drawing.Size(80, 17);
            this.labelRedLinesB.TabIndex = 23;
            this.labelRedLinesB.Text = "Red Lines B";
            // 
            // labelRedLinesA
            // 
            this.labelRedLinesA.AutoSize = true;
            this.labelRedLinesA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelRedLinesA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelRedLinesA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelRedLinesA.Location = new System.Drawing.Point(567, 6);
            this.labelRedLinesA.Name = "labelRedLinesA";
            this.labelRedLinesA.Size = new System.Drawing.Size(83, 17);
            this.labelRedLinesA.TabIndex = 22;
            this.labelRedLinesA.Text = "Red Lines A ";
            // 
            // labelGamesH2H
            // 
            this.labelGamesH2H.AutoSize = true;
            this.labelGamesH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelGamesH2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelGamesH2H.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelGamesH2H.Location = new System.Drawing.Point(289, 6);
            this.labelGamesH2H.Name = "labelGamesH2H";
            this.labelGamesH2H.Size = new System.Drawing.Size(117, 17);
            this.labelGamesH2H.TabIndex = 21;
            this.labelGamesH2H.Text = "Games H2H Total";
            // 
            // labelGamesB
            // 
            this.labelGamesB.AutoSize = true;
            this.labelGamesB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelGamesB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelGamesB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelGamesB.Location = new System.Drawing.Point(152, 6);
            this.labelGamesB.Name = "labelGamesB";
            this.labelGamesB.Size = new System.Drawing.Size(98, 17);
            this.labelGamesB.TabIndex = 20;
            this.labelGamesB.Text = "Games B Total";
            // 
            // labelGamesA
            // 
            this.labelGamesA.AutoSize = true;
            this.labelGamesA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelGamesA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelGamesA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelGamesA.Location = new System.Drawing.Point(13, 6);
            this.labelGamesA.Name = "labelGamesA";
            this.labelGamesA.Size = new System.Drawing.Size(98, 17);
            this.labelGamesA.TabIndex = 19;
            this.labelGamesA.Text = "Games A Total";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageFullTime);
            this.tabControl.Controls.Add(this.tabPage3_5);
            this.tabControl.Controls.Add(this.tabPage2_5);
            this.tabControl.Controls.Add(this.tabPage1_5);
            this.tabControl.Controls.Add(this.tabPageRange);
            this.tabControl.Controls.Add(this.tabPageBothScore);
            this.tabControl.Controls.Add(this.tabPageWinDrawLose);
            this.tabControl.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControl.Location = new System.Drawing.Point(7, 182);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(950, 262);
            this.tabControl.TabIndex = 2;
            // 
            // tabPageFullTime
            // 
            this.tabPageFullTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageFullTime.Controls.Add(this.tabControlFullTime);
            this.tabPageFullTime.Location = new System.Drawing.Point(4, 26);
            this.tabPageFullTime.Name = "tabPageFullTime";
            this.tabPageFullTime.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFullTime.Size = new System.Drawing.Size(942, 232);
            this.tabPageFullTime.TabIndex = 0;
            this.tabPageFullTime.Text = "Full Time";
            // 
            // tabControlFullTime
            // 
            this.tabControlFullTime.Controls.Add(this.tabPageHomeWin);
            this.tabControlFullTime.Controls.Add(this.tabPageH2HHomeWin);
            this.tabControlFullTime.Controls.Add(this.tabPageDraw);
            this.tabControlFullTime.Controls.Add(this.tabPageH2HDraw);
            this.tabControlFullTime.Controls.Add(this.tabPageAwayWin);
            this.tabControlFullTime.Controls.Add(this.tabPageH2HAwayWin);
            this.tabControlFullTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlFullTime.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControlFullTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControlFullTime.Location = new System.Drawing.Point(3, 3);
            this.tabControlFullTime.Name = "tabControlFullTime";
            this.tabControlFullTime.SelectedIndex = 0;
            this.tabControlFullTime.Size = new System.Drawing.Size(936, 226);
            this.tabControlFullTime.TabIndex = 0;
            // 
            // tabPageHomeWin
            // 
            this.tabPageHomeWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageHomeWin.Controls.Add(this.dgvFullTimeHomeWin);
            this.tabPageHomeWin.Location = new System.Drawing.Point(4, 26);
            this.tabPageHomeWin.Name = "tabPageHomeWin";
            this.tabPageHomeWin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHomeWin.Size = new System.Drawing.Size(928, 196);
            this.tabPageHomeWin.TabIndex = 0;
            this.tabPageHomeWin.Text = "Home Win";
            // 
            // dgvFullTimeHomeWin
            // 
            this.dgvFullTimeHomeWin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFullTimeHomeWin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFullTimeHomeWin.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFullTimeHomeWin.Location = new System.Drawing.Point(3, 3);
            this.dgvFullTimeHomeWin.Name = "dgvFullTimeHomeWin";
            this.dgvFullTimeHomeWin.Size = new System.Drawing.Size(922, 190);
            this.dgvFullTimeHomeWin.TabIndex = 0;
            // 
            // tabPageH2HHomeWin
            // 
            this.tabPageH2HHomeWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageH2HHomeWin.Controls.Add(this.dgvFullTimeHomeWinH2H);
            this.tabPageH2HHomeWin.Location = new System.Drawing.Point(4, 26);
            this.tabPageH2HHomeWin.Name = "tabPageH2HHomeWin";
            this.tabPageH2HHomeWin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageH2HHomeWin.Size = new System.Drawing.Size(928, 196);
            this.tabPageH2HHomeWin.TabIndex = 1;
            this.tabPageH2HHomeWin.Text = "H2H-Home Win ";
            // 
            // dgvFullTimeHomeWinH2H
            // 
            this.dgvFullTimeHomeWinH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFullTimeHomeWinH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFullTimeHomeWinH2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFullTimeHomeWinH2H.Location = new System.Drawing.Point(3, 3);
            this.dgvFullTimeHomeWinH2H.Name = "dgvFullTimeHomeWinH2H";
            this.dgvFullTimeHomeWinH2H.Size = new System.Drawing.Size(922, 190);
            this.dgvFullTimeHomeWinH2H.TabIndex = 1;
            // 
            // tabPageDraw
            // 
            this.tabPageDraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageDraw.Controls.Add(this.dgvFullTimeHomeDrawAwayDraw);
            this.tabPageDraw.Location = new System.Drawing.Point(4, 26);
            this.tabPageDraw.Name = "tabPageDraw";
            this.tabPageDraw.Size = new System.Drawing.Size(928, 196);
            this.tabPageDraw.TabIndex = 2;
            this.tabPageDraw.Text = "Draw";
            // 
            // dgvFullTimeHomeDrawAwayDraw
            // 
            this.dgvFullTimeHomeDrawAwayDraw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFullTimeHomeDrawAwayDraw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFullTimeHomeDrawAwayDraw.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFullTimeHomeDrawAwayDraw.Location = new System.Drawing.Point(0, 0);
            this.dgvFullTimeHomeDrawAwayDraw.Name = "dgvFullTimeHomeDrawAwayDraw";
            this.dgvFullTimeHomeDrawAwayDraw.Size = new System.Drawing.Size(928, 196);
            this.dgvFullTimeHomeDrawAwayDraw.TabIndex = 1;
            // 
            // tabPageH2HDraw
            // 
            this.tabPageH2HDraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageH2HDraw.Controls.Add(this.dgvFullTimeDrawH2H);
            this.tabPageH2HDraw.Location = new System.Drawing.Point(4, 26);
            this.tabPageH2HDraw.Name = "tabPageH2HDraw";
            this.tabPageH2HDraw.Size = new System.Drawing.Size(928, 196);
            this.tabPageH2HDraw.TabIndex = 3;
            this.tabPageH2HDraw.Text = "H2H-Draw";
            // 
            // dgvFullTimeDrawH2H
            // 
            this.dgvFullTimeDrawH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFullTimeDrawH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFullTimeDrawH2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFullTimeDrawH2H.Location = new System.Drawing.Point(0, 0);
            this.dgvFullTimeDrawH2H.Name = "dgvFullTimeDrawH2H";
            this.dgvFullTimeDrawH2H.Size = new System.Drawing.Size(928, 196);
            this.dgvFullTimeDrawH2H.TabIndex = 2;
            // 
            // tabPageAwayWin
            // 
            this.tabPageAwayWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageAwayWin.Controls.Add(this.dgvFullTimeAwayWin);
            this.tabPageAwayWin.Location = new System.Drawing.Point(4, 26);
            this.tabPageAwayWin.Name = "tabPageAwayWin";
            this.tabPageAwayWin.Size = new System.Drawing.Size(928, 196);
            this.tabPageAwayWin.TabIndex = 4;
            this.tabPageAwayWin.Text = "Away Win";
            // 
            // dgvFullTimeAwayWin
            // 
            this.dgvFullTimeAwayWin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFullTimeAwayWin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFullTimeAwayWin.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFullTimeAwayWin.Location = new System.Drawing.Point(0, 0);
            this.dgvFullTimeAwayWin.Name = "dgvFullTimeAwayWin";
            this.dgvFullTimeAwayWin.Size = new System.Drawing.Size(928, 196);
            this.dgvFullTimeAwayWin.TabIndex = 1;
            // 
            // tabPageH2HAwayWin
            // 
            this.tabPageH2HAwayWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageH2HAwayWin.Controls.Add(this.dgvFullTimeAwayWinH2H);
            this.tabPageH2HAwayWin.Location = new System.Drawing.Point(4, 26);
            this.tabPageH2HAwayWin.Name = "tabPageH2HAwayWin";
            this.tabPageH2HAwayWin.Size = new System.Drawing.Size(928, 196);
            this.tabPageH2HAwayWin.TabIndex = 5;
            this.tabPageH2HAwayWin.Text = "H2H-AwayWin";
            // 
            // dgvFullTimeAwayWinH2H
            // 
            this.dgvFullTimeAwayWinH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFullTimeAwayWinH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFullTimeAwayWinH2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvFullTimeAwayWinH2H.Location = new System.Drawing.Point(0, 0);
            this.dgvFullTimeAwayWinH2H.Name = "dgvFullTimeAwayWinH2H";
            this.dgvFullTimeAwayWinH2H.Size = new System.Drawing.Size(928, 196);
            this.dgvFullTimeAwayWinH2H.TabIndex = 2;
            // 
            // tabPage3_5
            // 
            this.tabPage3_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPage3_5.Controls.Add(this.tabControl35OverUnder);
            this.tabPage3_5.Location = new System.Drawing.Point(4, 26);
            this.tabPage3_5.Name = "tabPage3_5";
            this.tabPage3_5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3_5.Size = new System.Drawing.Size(942, 232);
            this.tabPage3_5.TabIndex = 1;
            this.tabPage3_5.Text = "3.5 Over/Under";
            // 
            // tabControl35OverUnder
            // 
            this.tabControl35OverUnder.Controls.Add(this.tabPageOver3_5);
            this.tabControl35OverUnder.Controls.Add(this.tabPageUnder3_5);
            this.tabControl35OverUnder.Controls.Add(this.tabPageOver3_5H2H);
            this.tabControl35OverUnder.Controls.Add(this.tabPageUnder3_5H2H);
            this.tabControl35OverUnder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl35OverUnder.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControl35OverUnder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControl35OverUnder.Location = new System.Drawing.Point(3, 3);
            this.tabControl35OverUnder.Name = "tabControl35OverUnder";
            this.tabControl35OverUnder.SelectedIndex = 0;
            this.tabControl35OverUnder.Size = new System.Drawing.Size(936, 226);
            this.tabControl35OverUnder.TabIndex = 0;
            // 
            // tabPageOver3_5
            // 
            this.tabPageOver3_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageOver3_5.Controls.Add(this.dvgOver3_5);
            this.tabPageOver3_5.Location = new System.Drawing.Point(4, 26);
            this.tabPageOver3_5.Name = "tabPageOver3_5";
            this.tabPageOver3_5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOver3_5.Size = new System.Drawing.Size(928, 196);
            this.tabPageOver3_5.TabIndex = 0;
            this.tabPageOver3_5.Text = "Over 3.5";
            // 
            // dvgOver3_5
            // 
            this.dvgOver3_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgOver3_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgOver3_5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgOver3_5.Location = new System.Drawing.Point(3, 3);
            this.dvgOver3_5.Name = "dvgOver3_5";
            this.dvgOver3_5.Size = new System.Drawing.Size(922, 190);
            this.dvgOver3_5.TabIndex = 1;
            // 
            // tabPageUnder3_5
            // 
            this.tabPageUnder3_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageUnder3_5.Controls.Add(this.dvgUnder3_5);
            this.tabPageUnder3_5.Location = new System.Drawing.Point(4, 26);
            this.tabPageUnder3_5.Name = "tabPageUnder3_5";
            this.tabPageUnder3_5.Size = new System.Drawing.Size(928, 196);
            this.tabPageUnder3_5.TabIndex = 3;
            this.tabPageUnder3_5.Text = "Under 3.5";
            // 
            // dvgUnder3_5
            // 
            this.dvgUnder3_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgUnder3_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgUnder3_5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgUnder3_5.Location = new System.Drawing.Point(0, 0);
            this.dvgUnder3_5.Name = "dvgUnder3_5";
            this.dvgUnder3_5.Size = new System.Drawing.Size(928, 196);
            this.dvgUnder3_5.TabIndex = 2;
            // 
            // tabPageOver3_5H2H
            // 
            this.tabPageOver3_5H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageOver3_5H2H.Controls.Add(this.dvgOver3_5H2H);
            this.tabPageOver3_5H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageOver3_5H2H.Name = "tabPageOver3_5H2H";
            this.tabPageOver3_5H2H.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOver3_5H2H.Size = new System.Drawing.Size(928, 196);
            this.tabPageOver3_5H2H.TabIndex = 1;
            this.tabPageOver3_5H2H.Text = "Over 3.5-H2H";
            // 
            // dvgOver3_5H2H
            // 
            this.dvgOver3_5H2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgOver3_5H2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgOver3_5H2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgOver3_5H2H.Location = new System.Drawing.Point(3, 3);
            this.dvgOver3_5H2H.Name = "dvgOver3_5H2H";
            this.dvgOver3_5H2H.Size = new System.Drawing.Size(922, 190);
            this.dvgOver3_5H2H.TabIndex = 3;
            // 
            // tabPageUnder3_5H2H
            // 
            this.tabPageUnder3_5H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageUnder3_5H2H.Controls.Add(this.dvgUnder3_5H2H);
            this.tabPageUnder3_5H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageUnder3_5H2H.Name = "tabPageUnder3_5H2H";
            this.tabPageUnder3_5H2H.Size = new System.Drawing.Size(928, 196);
            this.tabPageUnder3_5H2H.TabIndex = 2;
            this.tabPageUnder3_5H2H.Text = "Under 3.5-H2H";
            // 
            // dvgUnder3_5H2H
            // 
            this.dvgUnder3_5H2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgUnder3_5H2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgUnder3_5H2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgUnder3_5H2H.Location = new System.Drawing.Point(0, 0);
            this.dvgUnder3_5H2H.Name = "dvgUnder3_5H2H";
            this.dvgUnder3_5H2H.Size = new System.Drawing.Size(928, 196);
            this.dvgUnder3_5H2H.TabIndex = 4;
            // 
            // tabPage2_5
            // 
            this.tabPage2_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPage2_5.Controls.Add(this.tabControl25OverUnder);
            this.tabPage2_5.Location = new System.Drawing.Point(4, 26);
            this.tabPage2_5.Name = "tabPage2_5";
            this.tabPage2_5.Size = new System.Drawing.Size(942, 232);
            this.tabPage2_5.TabIndex = 2;
            this.tabPage2_5.Text = "2.5 Over/Under";
            // 
            // tabControl25OverUnder
            // 
            this.tabControl25OverUnder.Controls.Add(this.tabPageOver2_5);
            this.tabControl25OverUnder.Controls.Add(this.tabPageUnder2_5);
            this.tabControl25OverUnder.Controls.Add(this.tabPageOver2_5H2H);
            this.tabControl25OverUnder.Controls.Add(this.tabPageUnder2_5H2H);
            this.tabControl25OverUnder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl25OverUnder.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControl25OverUnder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControl25OverUnder.Location = new System.Drawing.Point(0, 0);
            this.tabControl25OverUnder.Name = "tabControl25OverUnder";
            this.tabControl25OverUnder.SelectedIndex = 0;
            this.tabControl25OverUnder.Size = new System.Drawing.Size(942, 232);
            this.tabControl25OverUnder.TabIndex = 1;
            // 
            // tabPageOver2_5
            // 
            this.tabPageOver2_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageOver2_5.Controls.Add(this.dvgOver2_5);
            this.tabPageOver2_5.Location = new System.Drawing.Point(4, 26);
            this.tabPageOver2_5.Name = "tabPageOver2_5";
            this.tabPageOver2_5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOver2_5.Size = new System.Drawing.Size(934, 202);
            this.tabPageOver2_5.TabIndex = 0;
            this.tabPageOver2_5.Text = "Over 2.5";
            // 
            // dvgOver2_5
            // 
            this.dvgOver2_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgOver2_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgOver2_5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgOver2_5.Location = new System.Drawing.Point(3, 3);
            this.dvgOver2_5.Name = "dvgOver2_5";
            this.dvgOver2_5.Size = new System.Drawing.Size(928, 196);
            this.dvgOver2_5.TabIndex = 4;
            // 
            // tabPageUnder2_5
            // 
            this.tabPageUnder2_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageUnder2_5.Controls.Add(this.dvgUnder2_5);
            this.tabPageUnder2_5.Location = new System.Drawing.Point(4, 26);
            this.tabPageUnder2_5.Name = "tabPageUnder2_5";
            this.tabPageUnder2_5.Size = new System.Drawing.Size(934, 202);
            this.tabPageUnder2_5.TabIndex = 3;
            this.tabPageUnder2_5.Text = "Under 2.5";
            // 
            // dvgUnder2_5
            // 
            this.dvgUnder2_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgUnder2_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgUnder2_5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgUnder2_5.Location = new System.Drawing.Point(0, 0);
            this.dvgUnder2_5.Name = "dvgUnder2_5";
            this.dvgUnder2_5.Size = new System.Drawing.Size(934, 202);
            this.dvgUnder2_5.TabIndex = 4;
            // 
            // tabPageOver2_5H2H
            // 
            this.tabPageOver2_5H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageOver2_5H2H.Controls.Add(this.dvgOver2_5H2H);
            this.tabPageOver2_5H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageOver2_5H2H.Name = "tabPageOver2_5H2H";
            this.tabPageOver2_5H2H.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOver2_5H2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageOver2_5H2H.TabIndex = 1;
            this.tabPageOver2_5H2H.Text = "Over 2.5-H2H";
            // 
            // dvgOver2_5H2H
            // 
            this.dvgOver2_5H2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgOver2_5H2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgOver2_5H2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgOver2_5H2H.Location = new System.Drawing.Point(3, 3);
            this.dvgOver2_5H2H.Name = "dvgOver2_5H2H";
            this.dvgOver2_5H2H.Size = new System.Drawing.Size(928, 196);
            this.dvgOver2_5H2H.TabIndex = 4;
            // 
            // tabPageUnder2_5H2H
            // 
            this.tabPageUnder2_5H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageUnder2_5H2H.Controls.Add(this.dvgUnder2_5H2H);
            this.tabPageUnder2_5H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageUnder2_5H2H.Name = "tabPageUnder2_5H2H";
            this.tabPageUnder2_5H2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageUnder2_5H2H.TabIndex = 2;
            this.tabPageUnder2_5H2H.Text = "Under 2.5-H2H";
            // 
            // dvgUnder2_5H2H
            // 
            this.dvgUnder2_5H2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgUnder2_5H2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgUnder2_5H2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgUnder2_5H2H.Location = new System.Drawing.Point(0, 0);
            this.dvgUnder2_5H2H.Name = "dvgUnder2_5H2H";
            this.dvgUnder2_5H2H.Size = new System.Drawing.Size(934, 202);
            this.dvgUnder2_5H2H.TabIndex = 4;
            // 
            // tabPage1_5
            // 
            this.tabPage1_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPage1_5.Controls.Add(this.tabControl15OverUnder);
            this.tabPage1_5.Location = new System.Drawing.Point(4, 26);
            this.tabPage1_5.Name = "tabPage1_5";
            this.tabPage1_5.Size = new System.Drawing.Size(942, 232);
            this.tabPage1_5.TabIndex = 3;
            this.tabPage1_5.Text = "1.5 Over/Under";
            // 
            // tabControl15OverUnder
            // 
            this.tabControl15OverUnder.Controls.Add(this.tabPageOver1_5);
            this.tabControl15OverUnder.Controls.Add(this.tabPageUnder1_5);
            this.tabControl15OverUnder.Controls.Add(this.tabPageOver1_5H2H);
            this.tabControl15OverUnder.Controls.Add(this.tabPageUnder1_5H2H);
            this.tabControl15OverUnder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl15OverUnder.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControl15OverUnder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControl15OverUnder.Location = new System.Drawing.Point(0, 0);
            this.tabControl15OverUnder.Name = "tabControl15OverUnder";
            this.tabControl15OverUnder.SelectedIndex = 0;
            this.tabControl15OverUnder.Size = new System.Drawing.Size(942, 232);
            this.tabControl15OverUnder.TabIndex = 1;
            // 
            // tabPageOver1_5
            // 
            this.tabPageOver1_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageOver1_5.Controls.Add(this.dvgOver1_5);
            this.tabPageOver1_5.Location = new System.Drawing.Point(4, 26);
            this.tabPageOver1_5.Name = "tabPageOver1_5";
            this.tabPageOver1_5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOver1_5.Size = new System.Drawing.Size(934, 202);
            this.tabPageOver1_5.TabIndex = 0;
            this.tabPageOver1_5.Text = "Over 1.5";
            // 
            // dvgOver1_5
            // 
            this.dvgOver1_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgOver1_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgOver1_5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgOver1_5.Location = new System.Drawing.Point(3, 3);
            this.dvgOver1_5.Name = "dvgOver1_5";
            this.dvgOver1_5.Size = new System.Drawing.Size(928, 196);
            this.dvgOver1_5.TabIndex = 5;
            // 
            // tabPageUnder1_5
            // 
            this.tabPageUnder1_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageUnder1_5.Controls.Add(this.dvgUnder1_5);
            this.tabPageUnder1_5.Location = new System.Drawing.Point(4, 26);
            this.tabPageUnder1_5.Name = "tabPageUnder1_5";
            this.tabPageUnder1_5.Size = new System.Drawing.Size(934, 202);
            this.tabPageUnder1_5.TabIndex = 3;
            this.tabPageUnder1_5.Text = "Under 1.5";
            // 
            // dvgUnder1_5
            // 
            this.dvgUnder1_5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgUnder1_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgUnder1_5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgUnder1_5.Location = new System.Drawing.Point(0, 0);
            this.dvgUnder1_5.Name = "dvgUnder1_5";
            this.dvgUnder1_5.Size = new System.Drawing.Size(934, 202);
            this.dvgUnder1_5.TabIndex = 5;
            // 
            // tabPageOver1_5H2H
            // 
            this.tabPageOver1_5H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageOver1_5H2H.Controls.Add(this.dvgOver1_5H2H);
            this.tabPageOver1_5H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageOver1_5H2H.Name = "tabPageOver1_5H2H";
            this.tabPageOver1_5H2H.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOver1_5H2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageOver1_5H2H.TabIndex = 1;
            this.tabPageOver1_5H2H.Text = "Over 1.5-H2H";
            // 
            // dvgOver1_5H2H
            // 
            this.dvgOver1_5H2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgOver1_5H2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgOver1_5H2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgOver1_5H2H.Location = new System.Drawing.Point(3, 3);
            this.dvgOver1_5H2H.Name = "dvgOver1_5H2H";
            this.dvgOver1_5H2H.Size = new System.Drawing.Size(928, 196);
            this.dvgOver1_5H2H.TabIndex = 5;
            // 
            // tabPageUnder1_5H2H
            // 
            this.tabPageUnder1_5H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageUnder1_5H2H.Controls.Add(this.dvgUnder1_5H2H);
            this.tabPageUnder1_5H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageUnder1_5H2H.Name = "tabPageUnder1_5H2H";
            this.tabPageUnder1_5H2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageUnder1_5H2H.TabIndex = 2;
            this.tabPageUnder1_5H2H.Text = "Under 1.5-H2H";
            // 
            // dvgUnder1_5H2H
            // 
            this.dvgUnder1_5H2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgUnder1_5H2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgUnder1_5H2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgUnder1_5H2H.Location = new System.Drawing.Point(0, 0);
            this.dvgUnder1_5H2H.Name = "dvgUnder1_5H2H";
            this.dvgUnder1_5H2H.Size = new System.Drawing.Size(934, 202);
            this.dvgUnder1_5H2H.TabIndex = 5;
            // 
            // tabPageRange
            // 
            this.tabPageRange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange.Controls.Add(this.tabControlRange);
            this.tabPageRange.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange.Name = "tabPageRange";
            this.tabPageRange.Size = new System.Drawing.Size(942, 232);
            this.tabPageRange.TabIndex = 4;
            this.tabPageRange.Text = "Range";
            // 
            // tabControlRange
            // 
            this.tabControlRange.Controls.Add(this.tabPageRange0_1);
            this.tabControlRange.Controls.Add(this.tabPageRange2_3);
            this.tabControlRange.Controls.Add(this.tabPageRange4Plus);
            this.tabControlRange.Controls.Add(this.tabPageRange0_1H2H);
            this.tabControlRange.Controls.Add(this.tabPageRange2_3H2H);
            this.tabControlRange.Controls.Add(this.tabPageRange4PlusH2H);
            this.tabControlRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlRange.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControlRange.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControlRange.Location = new System.Drawing.Point(0, 0);
            this.tabControlRange.Name = "tabControlRange";
            this.tabControlRange.SelectedIndex = 0;
            this.tabControlRange.Size = new System.Drawing.Size(942, 232);
            this.tabControlRange.TabIndex = 0;
            // 
            // tabPageRange0_1
            // 
            this.tabPageRange0_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange0_1.Controls.Add(this.dvg01Range);
            this.tabPageRange0_1.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange0_1.Name = "tabPageRange0_1";
            this.tabPageRange0_1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRange0_1.Size = new System.Drawing.Size(934, 202);
            this.tabPageRange0_1.TabIndex = 0;
            this.tabPageRange0_1.Text = "0-1 Range";
            // 
            // dvg01Range
            // 
            this.dvg01Range.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvg01Range.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg01Range.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvg01Range.Location = new System.Drawing.Point(3, 3);
            this.dvg01Range.Name = "dvg01Range";
            this.dvg01Range.Size = new System.Drawing.Size(928, 196);
            this.dvg01Range.TabIndex = 6;
            // 
            // tabPageRange2_3
            // 
            this.tabPageRange2_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange2_3.Controls.Add(this.dvg23Range);
            this.tabPageRange2_3.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange2_3.Name = "tabPageRange2_3";
            this.tabPageRange2_3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRange2_3.Size = new System.Drawing.Size(934, 202);
            this.tabPageRange2_3.TabIndex = 1;
            this.tabPageRange2_3.Text = "2-3 Range";
            // 
            // dvg23Range
            // 
            this.dvg23Range.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvg23Range.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg23Range.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvg23Range.Location = new System.Drawing.Point(3, 3);
            this.dvg23Range.Name = "dvg23Range";
            this.dvg23Range.Size = new System.Drawing.Size(928, 196);
            this.dvg23Range.TabIndex = 6;
            // 
            // tabPageRange4Plus
            // 
            this.tabPageRange4Plus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange4Plus.Controls.Add(this.dvg4PlusRange);
            this.tabPageRange4Plus.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange4Plus.Name = "tabPageRange4Plus";
            this.tabPageRange4Plus.Size = new System.Drawing.Size(934, 202);
            this.tabPageRange4Plus.TabIndex = 2;
            this.tabPageRange4Plus.Text = "4+ Range";
            // 
            // dvg4PlusRange
            // 
            this.dvg4PlusRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvg4PlusRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg4PlusRange.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvg4PlusRange.Location = new System.Drawing.Point(0, 0);
            this.dvg4PlusRange.Name = "dvg4PlusRange";
            this.dvg4PlusRange.Size = new System.Drawing.Size(934, 202);
            this.dvg4PlusRange.TabIndex = 6;
            // 
            // tabPageRange0_1H2H
            // 
            this.tabPageRange0_1H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange0_1H2H.Controls.Add(this.dvg01H2HRange);
            this.tabPageRange0_1H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange0_1H2H.Name = "tabPageRange0_1H2H";
            this.tabPageRange0_1H2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageRange0_1H2H.TabIndex = 3;
            this.tabPageRange0_1H2H.Text = "Range 0-1 H2H";
            // 
            // dvg01H2HRange
            // 
            this.dvg01H2HRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvg01H2HRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg01H2HRange.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvg01H2HRange.Location = new System.Drawing.Point(0, 0);
            this.dvg01H2HRange.Name = "dvg01H2HRange";
            this.dvg01H2HRange.Size = new System.Drawing.Size(934, 202);
            this.dvg01H2HRange.TabIndex = 6;
            // 
            // tabPageRange2_3H2H
            // 
            this.tabPageRange2_3H2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange2_3H2H.Controls.Add(this.dvg23H2HRange);
            this.tabPageRange2_3H2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange2_3H2H.Name = "tabPageRange2_3H2H";
            this.tabPageRange2_3H2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageRange2_3H2H.TabIndex = 4;
            this.tabPageRange2_3H2H.Text = "Range 2-3 H2H";
            // 
            // dvg23H2HRange
            // 
            this.dvg23H2HRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvg23H2HRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg23H2HRange.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvg23H2HRange.Location = new System.Drawing.Point(0, 0);
            this.dvg23H2HRange.Name = "dvg23H2HRange";
            this.dvg23H2HRange.Size = new System.Drawing.Size(934, 202);
            this.dvg23H2HRange.TabIndex = 6;
            // 
            // tabPageRange4PlusH2H
            // 
            this.tabPageRange4PlusH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageRange4PlusH2H.Controls.Add(this.dvg4PlusH2HRange);
            this.tabPageRange4PlusH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageRange4PlusH2H.Name = "tabPageRange4PlusH2H";
            this.tabPageRange4PlusH2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageRange4PlusH2H.TabIndex = 5;
            this.tabPageRange4PlusH2H.Text = "Range 4+ H2H";
            // 
            // dvg4PlusH2HRange
            // 
            this.dvg4PlusH2HRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvg4PlusH2HRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg4PlusH2HRange.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvg4PlusH2HRange.Location = new System.Drawing.Point(0, 0);
            this.dvg4PlusH2HRange.Name = "dvg4PlusH2HRange";
            this.dvg4PlusH2HRange.Size = new System.Drawing.Size(934, 202);
            this.dvg4PlusH2HRange.TabIndex = 6;
            // 
            // tabPageBothScore
            // 
            this.tabPageBothScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageBothScore.Controls.Add(this.tabControlBothScore);
            this.tabPageBothScore.Location = new System.Drawing.Point(4, 26);
            this.tabPageBothScore.Name = "tabPageBothScore";
            this.tabPageBothScore.Size = new System.Drawing.Size(942, 232);
            this.tabPageBothScore.TabIndex = 5;
            this.tabPageBothScore.Text = "Both Score";
            // 
            // tabControlBothScore
            // 
            this.tabControlBothScore.Controls.Add(this.tabPageBothScoreYes);
            this.tabControlBothScore.Controls.Add(this.tabPageBothScoreNo);
            this.tabControlBothScore.Controls.Add(this.tabPageBothScoreYesH2H);
            this.tabControlBothScore.Controls.Add(this.tabPageBothScoreNoH2H);
            this.tabControlBothScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlBothScore.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControlBothScore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControlBothScore.Location = new System.Drawing.Point(0, 0);
            this.tabControlBothScore.Name = "tabControlBothScore";
            this.tabControlBothScore.SelectedIndex = 0;
            this.tabControlBothScore.Size = new System.Drawing.Size(942, 232);
            this.tabControlBothScore.TabIndex = 0;
            // 
            // tabPageBothScoreYes
            // 
            this.tabPageBothScoreYes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageBothScoreYes.Controls.Add(this.dvgBothScoreYes);
            this.tabPageBothScoreYes.Location = new System.Drawing.Point(4, 26);
            this.tabPageBothScoreYes.Name = "tabPageBothScoreYes";
            this.tabPageBothScoreYes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBothScoreYes.Size = new System.Drawing.Size(934, 202);
            this.tabPageBothScoreYes.TabIndex = 0;
            this.tabPageBothScoreYes.Text = "Yes";
            // 
            // tabPageBothScoreNo
            // 
            this.tabPageBothScoreNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageBothScoreNo.Controls.Add(this.dvgBothScoreNo);
            this.tabPageBothScoreNo.Location = new System.Drawing.Point(4, 26);
            this.tabPageBothScoreNo.Name = "tabPageBothScoreNo";
            this.tabPageBothScoreNo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBothScoreNo.Size = new System.Drawing.Size(934, 202);
            this.tabPageBothScoreNo.TabIndex = 1;
            this.tabPageBothScoreNo.Text = "No";
            // 
            // tabPageBothScoreYesH2H
            // 
            this.tabPageBothScoreYesH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageBothScoreYesH2H.Controls.Add(this.dvgBothScoreYesH2H);
            this.tabPageBothScoreYesH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageBothScoreYesH2H.Name = "tabPageBothScoreYesH2H";
            this.tabPageBothScoreYesH2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageBothScoreYesH2H.TabIndex = 2;
            this.tabPageBothScoreYesH2H.Text = "Yes - H2H";
            // 
            // tabPageBothScoreNoH2H
            // 
            this.tabPageBothScoreNoH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageBothScoreNoH2H.Controls.Add(this.dvgBothScoreNoH2H);
            this.tabPageBothScoreNoH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageBothScoreNoH2H.Name = "tabPageBothScoreNoH2H";
            this.tabPageBothScoreNoH2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageBothScoreNoH2H.TabIndex = 3;
            this.tabPageBothScoreNoH2H.Text = "No - H2H";
            // 
            // tabPageWinDrawLose
            // 
            this.tabPageWinDrawLose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageWinDrawLose.Controls.Add(this.tabControlWinDrawLose);
            this.tabPageWinDrawLose.Location = new System.Drawing.Point(4, 26);
            this.tabPageWinDrawLose.Name = "tabPageWinDrawLose";
            this.tabPageWinDrawLose.Size = new System.Drawing.Size(942, 232);
            this.tabPageWinDrawLose.TabIndex = 6;
            this.tabPageWinDrawLose.Text = "Win/Draw/Lose";
            // 
            // tabControlWinDrawLose
            // 
            this.tabControlWinDrawLose.Controls.Add(this.tabPageWinDraw);
            this.tabControlWinDrawLose.Controls.Add(this.tabPageNoDraw);
            this.tabControlWinDrawLose.Controls.Add(this.tabPageDrawLose);
            this.tabControlWinDrawLose.Controls.Add(this.tabPageWinDrawH2H);
            this.tabControlWinDrawLose.Controls.Add(this.tabPageNoDrawH2H);
            this.tabControlWinDrawLose.Controls.Add(this.tabPageDrawLoseH2H);
            this.tabControlWinDrawLose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlWinDrawLose.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControlWinDrawLose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControlWinDrawLose.Location = new System.Drawing.Point(0, 0);
            this.tabControlWinDrawLose.Name = "tabControlWinDrawLose";
            this.tabControlWinDrawLose.SelectedIndex = 0;
            this.tabControlWinDrawLose.Size = new System.Drawing.Size(942, 232);
            this.tabControlWinDrawLose.TabIndex = 0;
            // 
            // tabPageWinDraw
            // 
            this.tabPageWinDraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageWinDraw.Controls.Add(this.dvgWinDraw);
            this.tabPageWinDraw.Location = new System.Drawing.Point(4, 26);
            this.tabPageWinDraw.Name = "tabPageWinDraw";
            this.tabPageWinDraw.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWinDraw.Size = new System.Drawing.Size(934, 202);
            this.tabPageWinDraw.TabIndex = 0;
            this.tabPageWinDraw.Text = "Win + Draw";
            // 
            // tabPageNoDraw
            // 
            this.tabPageNoDraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageNoDraw.Controls.Add(this.dvgNoDraw);
            this.tabPageNoDraw.Location = new System.Drawing.Point(4, 26);
            this.tabPageNoDraw.Name = "tabPageNoDraw";
            this.tabPageNoDraw.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNoDraw.Size = new System.Drawing.Size(934, 202);
            this.tabPageNoDraw.TabIndex = 1;
            this.tabPageNoDraw.Text = "No Draw";
            // 
            // tabPageDrawLose
            // 
            this.tabPageDrawLose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageDrawLose.Controls.Add(this.dvgDrawLose);
            this.tabPageDrawLose.Location = new System.Drawing.Point(4, 26);
            this.tabPageDrawLose.Name = "tabPageDrawLose";
            this.tabPageDrawLose.Size = new System.Drawing.Size(934, 202);
            this.tabPageDrawLose.TabIndex = 2;
            this.tabPageDrawLose.Text = "Draw + Lose";
            // 
            // tabPageWinDrawH2H
            // 
            this.tabPageWinDrawH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageWinDrawH2H.Controls.Add(this.dvgWinDrawH2H);
            this.tabPageWinDrawH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageWinDrawH2H.Name = "tabPageWinDrawH2H";
            this.tabPageWinDrawH2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageWinDrawH2H.TabIndex = 3;
            this.tabPageWinDrawH2H.Text = "Win+Draw - H2H";
            // 
            // tabPageNoDrawH2H
            // 
            this.tabPageNoDrawH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageNoDrawH2H.Controls.Add(this.dvgNoDrawH2H);
            this.tabPageNoDrawH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageNoDrawH2H.Name = "tabPageNoDrawH2H";
            this.tabPageNoDrawH2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageNoDrawH2H.TabIndex = 4;
            this.tabPageNoDrawH2H.Text = "No Draw - H2H";
            // 
            // tabPageDrawLoseH2H
            // 
            this.tabPageDrawLoseH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageDrawLoseH2H.Controls.Add(this.dvgDrawLoseH2H);
            this.tabPageDrawLoseH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageDrawLoseH2H.Name = "tabPageDrawLoseH2H";
            this.tabPageDrawLoseH2H.Size = new System.Drawing.Size(934, 202);
            this.tabPageDrawLoseH2H.TabIndex = 5;
            this.tabPageDrawLoseH2H.Text = "Draw+Lose - H2H";
            // 
            // dvgWinDraw
            // 
            this.dvgWinDraw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgWinDraw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgWinDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgWinDraw.Location = new System.Drawing.Point(3, 3);
            this.dvgWinDraw.Name = "dvgWinDraw";
            this.dvgWinDraw.Size = new System.Drawing.Size(928, 196);
            this.dvgWinDraw.TabIndex = 7;
            // 
            // dvgNoDraw
            // 
            this.dvgNoDraw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgNoDraw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgNoDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgNoDraw.Location = new System.Drawing.Point(3, 3);
            this.dvgNoDraw.Name = "dvgNoDraw";
            this.dvgNoDraw.Size = new System.Drawing.Size(928, 196);
            this.dvgNoDraw.TabIndex = 7;
            // 
            // dvgDrawLose
            // 
            this.dvgDrawLose.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgDrawLose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgDrawLose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgDrawLose.Location = new System.Drawing.Point(0, 0);
            this.dvgDrawLose.Name = "dvgDrawLose";
            this.dvgDrawLose.Size = new System.Drawing.Size(934, 202);
            this.dvgDrawLose.TabIndex = 7;
            // 
            // dvgWinDrawH2H
            // 
            this.dvgWinDrawH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgWinDrawH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgWinDrawH2H.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgWinDrawH2H.Location = new System.Drawing.Point(0, 0);
            this.dvgWinDrawH2H.Name = "dvgWinDrawH2H";
            this.dvgWinDrawH2H.Size = new System.Drawing.Size(934, 202);
            this.dvgWinDrawH2H.TabIndex = 7;
            // 
            // dvgNoDrawH2H
            // 
            this.dvgNoDrawH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgNoDrawH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgNoDrawH2H.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgNoDrawH2H.Location = new System.Drawing.Point(0, 0);
            this.dvgNoDrawH2H.Name = "dvgNoDrawH2H";
            this.dvgNoDrawH2H.Size = new System.Drawing.Size(934, 202);
            this.dvgNoDrawH2H.TabIndex = 7;
            // 
            // dvgDrawLoseH2H
            // 
            this.dvgDrawLoseH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgDrawLoseH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgDrawLoseH2H.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgDrawLoseH2H.Location = new System.Drawing.Point(0, 0);
            this.dvgDrawLoseH2H.Name = "dvgDrawLoseH2H";
            this.dvgDrawLoseH2H.Size = new System.Drawing.Size(934, 202);
            this.dvgDrawLoseH2H.TabIndex = 7;
            // 
            // dvgBothScoreYes
            // 
            this.dvgBothScoreYes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgBothScoreYes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgBothScoreYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgBothScoreYes.Location = new System.Drawing.Point(3, 3);
            this.dvgBothScoreYes.Name = "dvgBothScoreYes";
            this.dvgBothScoreYes.Size = new System.Drawing.Size(928, 196);
            this.dvgBothScoreYes.TabIndex = 7;
            // 
            // dvgBothScoreNo
            // 
            this.dvgBothScoreNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgBothScoreNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgBothScoreNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgBothScoreNo.Location = new System.Drawing.Point(3, 3);
            this.dvgBothScoreNo.Name = "dvgBothScoreNo";
            this.dvgBothScoreNo.Size = new System.Drawing.Size(928, 196);
            this.dvgBothScoreNo.TabIndex = 7;
            // 
            // dvgBothScoreYesH2H
            // 
            this.dvgBothScoreYesH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgBothScoreYesH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgBothScoreYesH2H.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgBothScoreYesH2H.Location = new System.Drawing.Point(0, 0);
            this.dvgBothScoreYesH2H.Name = "dvgBothScoreYesH2H";
            this.dvgBothScoreYesH2H.Size = new System.Drawing.Size(934, 202);
            this.dvgBothScoreYesH2H.TabIndex = 7;
            // 
            // dvgBothScoreNoH2H
            // 
            this.dvgBothScoreNoH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgBothScoreNoH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgBothScoreNoH2H.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dvgBothScoreNoH2H.Location = new System.Drawing.Point(0, 0);
            this.dvgBothScoreNoH2H.Name = "dvgBothScoreNoH2H";
            this.dvgBothScoreNoH2H.Size = new System.Drawing.Size(934, 202);
            this.dvgBothScoreNoH2H.TabIndex = 7;
            // 
            // BettingDataDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 450);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panelGeneralData);
            this.Controls.Add(this.panelTop);
            this.Name = "BettingDataDashboard";
            this.Text = "BettingDataDashboard";
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAwayOdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDrawOdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHomeOdd)).EndInit();
            this.panelGeneralData.ResumeLayout(false);
            this.panelGeneralData.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageFullTime.ResumeLayout(false);
            this.tabControlFullTime.ResumeLayout(false);
            this.tabPageHomeWin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeHomeWin)).EndInit();
            this.tabPageH2HHomeWin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeHomeWinH2H)).EndInit();
            this.tabPageDraw.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeHomeDrawAwayDraw)).EndInit();
            this.tabPageH2HDraw.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeDrawH2H)).EndInit();
            this.tabPageAwayWin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeAwayWin)).EndInit();
            this.tabPageH2HAwayWin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFullTimeAwayWinH2H)).EndInit();
            this.tabPage3_5.ResumeLayout(false);
            this.tabControl35OverUnder.ResumeLayout(false);
            this.tabPageOver3_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver3_5)).EndInit();
            this.tabPageUnder3_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder3_5)).EndInit();
            this.tabPageOver3_5H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver3_5H2H)).EndInit();
            this.tabPageUnder3_5H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder3_5H2H)).EndInit();
            this.tabPage2_5.ResumeLayout(false);
            this.tabControl25OverUnder.ResumeLayout(false);
            this.tabPageOver2_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver2_5)).EndInit();
            this.tabPageUnder2_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder2_5)).EndInit();
            this.tabPageOver2_5H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver2_5H2H)).EndInit();
            this.tabPageUnder2_5H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder2_5H2H)).EndInit();
            this.tabPage1_5.ResumeLayout(false);
            this.tabControl15OverUnder.ResumeLayout(false);
            this.tabPageOver1_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver1_5)).EndInit();
            this.tabPageUnder1_5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder1_5)).EndInit();
            this.tabPageOver1_5H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgOver1_5H2H)).EndInit();
            this.tabPageUnder1_5H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgUnder1_5H2H)).EndInit();
            this.tabPageRange.ResumeLayout(false);
            this.tabControlRange.ResumeLayout(false);
            this.tabPageRange0_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg01Range)).EndInit();
            this.tabPageRange2_3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg23Range)).EndInit();
            this.tabPageRange4Plus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg4PlusRange)).EndInit();
            this.tabPageRange0_1H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg01H2HRange)).EndInit();
            this.tabPageRange2_3H2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg23H2HRange)).EndInit();
            this.tabPageRange4PlusH2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg4PlusH2HRange)).EndInit();
            this.tabPageBothScore.ResumeLayout(false);
            this.tabControlBothScore.ResumeLayout(false);
            this.tabPageBothScoreYes.ResumeLayout(false);
            this.tabPageBothScoreNo.ResumeLayout(false);
            this.tabPageBothScoreYesH2H.ResumeLayout(false);
            this.tabPageBothScoreNoH2H.ResumeLayout(false);
            this.tabPageWinDrawLose.ResumeLayout(false);
            this.tabControlWinDrawLose.ResumeLayout(false);
            this.tabPageWinDraw.ResumeLayout(false);
            this.tabPageNoDraw.ResumeLayout(false);
            this.tabPageDrawLose.ResumeLayout(false);
            this.tabPageWinDrawH2H.ResumeLayout(false);
            this.tabPageNoDrawH2H.ResumeLayout(false);
            this.tabPageDrawLoseH2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgWinDraw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgNoDraw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgDrawLose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgWinDrawH2H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgNoDrawH2H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgDrawLoseH2H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreYesH2H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgBothScoreNoH2H)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.ComboBox comboBoxSportType;
        private System.Windows.Forms.Label labelTeamA;
        private System.Windows.Forms.TextBox textBoxTeamA;
        private System.Windows.Forms.Label labelTeamB;
        private System.Windows.Forms.TextBox textBoxTeamB;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Panel panelGeneralData;
        private System.Windows.Forms.Label labelRedLinesB;
        private System.Windows.Forms.Label labelRedLinesA;
        private System.Windows.Forms.Label labelGamesH2H;
        private System.Windows.Forms.Label labelGamesB;
        private System.Windows.Forms.Label labelGamesA;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageFullTime;
        private System.Windows.Forms.TabPage tabPage3_5;
        private System.Windows.Forms.TabControl tabControlFullTime;
        private System.Windows.Forms.TabPage tabPageHomeWin;
        private System.Windows.Forms.TabPage tabPageH2HHomeWin;
        private System.Windows.Forms.TabPage tabPageDraw;
        private System.Windows.Forms.TabPage tabPageH2HDraw;
        private System.Windows.Forms.TabPage tabPageAwayWin;
        private System.Windows.Forms.TabPage tabPageH2HAwayWin;
        private System.Windows.Forms.TabPage tabPage2_5;
        private System.Windows.Forms.TabPage tabPage1_5;
        private System.Windows.Forms.TabControl tabControl35OverUnder;
        private System.Windows.Forms.TabPage tabPageOver3_5;
        private System.Windows.Forms.TabPage tabPageOver3_5H2H;
        private System.Windows.Forms.TabPage tabPageUnder3_5H2H;
        private System.Windows.Forms.TabControl tabControl25OverUnder;
        private System.Windows.Forms.TabPage tabPageOver2_5;
        private System.Windows.Forms.TabPage tabPageOver2_5H2H;
        private System.Windows.Forms.TabPage tabPageUnder2_5H2H;
        private System.Windows.Forms.TabControl tabControl15OverUnder;
        private System.Windows.Forms.TabPage tabPageOver1_5;
        private System.Windows.Forms.TabPage tabPageOver1_5H2H;
        private System.Windows.Forms.TabPage tabPageUnder1_5H2H;
        private System.Windows.Forms.TabPage tabPageRange;
        private System.Windows.Forms.TabPage tabPageBothScore;
        private System.Windows.Forms.TabPage tabPageWinDrawLose;
        private System.Windows.Forms.TabControl tabControlRange;
        private System.Windows.Forms.TabPage tabPageRange0_1;
        private System.Windows.Forms.TabPage tabPageRange2_3;
        private System.Windows.Forms.TabPage tabPageRange4Plus;
        private System.Windows.Forms.TabPage tabPageRange0_1H2H;
        private System.Windows.Forms.TabPage tabPageRange2_3H2H;
        private System.Windows.Forms.TabPage tabPageRange4PlusH2H;
        private System.Windows.Forms.TabControl tabControlBothScore;
        private System.Windows.Forms.TabPage tabPageBothScoreYes;
        private System.Windows.Forms.TabPage tabPageBothScoreNo;
        private System.Windows.Forms.TabPage tabPageBothScoreYesH2H;
        private System.Windows.Forms.TabPage tabPageBothScoreNoH2H;
        private System.Windows.Forms.TabControl tabControlWinDrawLose;
        private System.Windows.Forms.TabPage tabPageWinDraw;
        private System.Windows.Forms.TabPage tabPageNoDraw;
        private System.Windows.Forms.TabPage tabPageDrawLose;
        private System.Windows.Forms.TabPage tabPageWinDrawH2H;
        private System.Windows.Forms.TabPage tabPageNoDrawH2H;
        private System.Windows.Forms.TabPage tabPageDrawLoseH2H;
        private System.Windows.Forms.NumericUpDown numericAwayOdd;
        private System.Windows.Forms.NumericUpDown numericDrawOdd;
        private System.Windows.Forms.NumericUpDown numericHomeOdd;
        private System.Windows.Forms.Label labelAwayOdd;
        private System.Windows.Forms.Label labelDrawOdd;
        private System.Windows.Forms.Label labelHomeOdd;
        private System.Windows.Forms.DataGridView dgvFullTimeHomeWin;
        private System.Windows.Forms.DataGridView dgvFullTimeHomeWinH2H;
        private System.Windows.Forms.DataGridView dgvFullTimeHomeDrawAwayDraw;
        private System.Windows.Forms.DataGridView dgvFullTimeDrawH2H;
        private System.Windows.Forms.DataGridView dgvFullTimeAwayWin;
        private System.Windows.Forms.DataGridView dgvFullTimeAwayWinH2H;
        private System.Windows.Forms.DataGridView dvgOver3_5;
        private System.Windows.Forms.TabPage tabPageUnder3_5;
        private System.Windows.Forms.DataGridView dvgUnder3_5;
        private System.Windows.Forms.DataGridView dvgOver3_5H2H;
        private System.Windows.Forms.DataGridView dvgUnder3_5H2H;
        private System.Windows.Forms.TabPage tabPageUnder2_5;
        private System.Windows.Forms.DataGridView dvgOver2_5;
        private System.Windows.Forms.DataGridView dvgOver2_5H2H;
        private System.Windows.Forms.DataGridView dvgUnder2_5H2H;
        private System.Windows.Forms.DataGridView dvgUnder2_5;
        private System.Windows.Forms.TabPage tabPageUnder1_5;
        private System.Windows.Forms.DataGridView dvgOver1_5;
        private System.Windows.Forms.DataGridView dvgUnder1_5;
        private System.Windows.Forms.DataGridView dvgOver1_5H2H;
        private System.Windows.Forms.DataGridView dvgUnder1_5H2H;
        private System.Windows.Forms.DataGridView dvg01Range;
        private System.Windows.Forms.DataGridView dvg23Range;
        private System.Windows.Forms.DataGridView dvg4PlusRange;
        private System.Windows.Forms.DataGridView dvg01H2HRange;
        private System.Windows.Forms.DataGridView dvg23H2HRange;
        private System.Windows.Forms.DataGridView dvg4PlusH2HRange;
        private System.Windows.Forms.DataGridView dvgBothScoreYes;
        private System.Windows.Forms.DataGridView dvgBothScoreNo;
        private System.Windows.Forms.DataGridView dvgBothScoreYesH2H;
        private System.Windows.Forms.DataGridView dvgBothScoreNoH2H;
        private System.Windows.Forms.DataGridView dvgWinDraw;
        private System.Windows.Forms.DataGridView dvgNoDraw;
        private System.Windows.Forms.DataGridView dvgDrawLose;
        private System.Windows.Forms.DataGridView dvgWinDrawH2H;
        private System.Windows.Forms.DataGridView dvgNoDrawH2H;
        private System.Windows.Forms.DataGridView dvgDrawLoseH2H;
    }
}