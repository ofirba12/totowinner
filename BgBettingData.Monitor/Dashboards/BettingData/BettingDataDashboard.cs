﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    public partial class BettingDataDashboard : BaseAB
    {
        public BettingDataDashboard() : base()
        {
            InitializeComponent();
            this.Init();
            this.Text = "Betting Data";
            this.labelGamesA.Text = $"Games A:";
            this.labelGamesB.Text = $"Games B:";
            this.labelGamesH2H.Text = $"Games H2H:";
            this.labelRedLinesA.Text = $"RedLines A:";
            this.labelRedLinesB.Text = $"RedLines B:";
            comboBoxSportType_SelectedIndexChanged(this.comboBoxSportType, null);
        }
        public BettingDataDashboard(BgSport sport,
            int homeTeamId,
            string homeTeam,
            int awayTeamId, 
            string awayTeam,
            decimal homeOdd, 
            decimal drawOdd,
            decimal awayOdd) : base()
        {
            InitializeComponent();
            this.Init();
            this.abParams.Sport = sport;
            this.abParams.TeamAId = homeTeamId;
            this.abParams.TeamBId = awayTeamId;
            this.textBoxTeamA.Text = $"{homeTeam} # {homeTeamId}";
            this.textBoxTeamB.Text = $"{awayTeam} # {awayTeamId}";
            this.numericHomeOdd.Value = homeOdd;
            this.numericDrawOdd.Value = drawOdd;
            this.numericAwayOdd.Value = awayOdd;
            this.comboBoxSportType.Enabled = false;
            this.textBoxTeamA.Enabled = false;
            this.textBoxTeamB.Enabled = false;
            this.numericHomeOdd.Enabled = false;
            this.numericDrawOdd.Enabled = false;
            this.numericAwayOdd.Enabled = false;
        }
        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.abParams.Sport = sport;
                    this.textBoxTeamA.Text = String.Empty;
                    this.textBoxTeamB.Text = String.Empty;
                    MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                        .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.abParams.Sport), this.textBoxTeamA);
                }
            }
        }

        #region Auto Complete Events
        private void textBoxTeamA_Leave(object sender, EventArgs e)
        {
            validateTeamATextBox(sender, this.textBoxTeamB);
        }
        private void textBoxTeamA_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                validateTeamATextBox(sender, this.textBoxTeamB);
            }
        }
        private void textBoxTeamB_Leave(object sender, EventArgs e)
        {
            validateTeamBTextBox(sender, this.textBoxTeamA);
        }
        private void textBoxTeamB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.abParams.TeamBId = MonitorUtils.SelectBgItem(((TextBox)sender).Text, BgMapperType.Teams);
            }
        }
        #endregion
        private void buttonGo_Click(object sender, EventArgs e)
        {
            if (!this.abParams.TeamAId.HasValue)
            {
                MessageBox.Show($"It seems that team A was not selected", $"Invalid team selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!this.abParams.TeamBId.HasValue)
            {
                MessageBox.Show($"It seems that team B was not selected", $"Invalid team selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (this.numericHomeOdd.Value == 1 || this.numericAwayOdd.Value == 1)
            {
                MessageBox.Show($"It seems that some odds are missing", $"Missing Odds", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var maxRows = 6;
            var bgTeamsAnalysisRepo = analysisServices.PrepareTeamsRepository(this.abParams.TeamAId, this.abParams.TeamBId);
            var bettingDataRepo = analysisServices.PrepareTeamsBettingDataRepository(bgTeamsAnalysisRepo, maxRows);
            var bgTeamsStatisitcsRepo = analysisServices.PrepareTeamsStatisitcsRepository(bgTeamsAnalysisRepo, maxRows);
            this.labelGamesA.Text = $"Games A: {bettingDataRepo.GamesATotal}";
            this.labelGamesB.Text = $"Games B: {bettingDataRepo.GamesBTotal}";
            this.labelGamesH2H.Text = $"Games H2H: {bettingDataRepo.GamesH2HTotal}";
            this.labelRedLinesA.Text = $"RedLines A: {bettingDataRepo.RedLinesA}";
            this.labelRedLinesB.Text = $"RedLines B: {bettingDataRepo.RedLinesB}";

            var match = new BgMatchOdds(this.abParams.Sport,
                this.abParams.TeamAId.Value,
                this.abParams.TeamBId.Value,
                this.numericHomeOdd.Value,
                this.numericDrawOdd.Value,
                this.numericAwayOdd.Value);

            var fullTimeBetData = analysisServices.GetFullTimeBetData(match,
                bgTeamsStatisitcsRepo);
            SetFullTimeTabData(fullTimeBetData);

            var overUnder35BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder3_5, match,
                bgTeamsStatisitcsRepo);
            SetOverUnderTabData(overUnder35BetData, this.dvgOver3_5, this.dvgOver3_5H2H, this.dvgUnder3_5, this.dvgUnder3_5H2H);

            var overUnder25BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder2_5, match,
                bgTeamsStatisitcsRepo);
            SetOverUnderTabData(overUnder25BetData, this.dvgOver2_5, this.dvgOver2_5H2H, this.dvgUnder2_5, this.dvgUnder2_5H2H);

            var overUnder15BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder1_5, match,
                bgTeamsStatisitcsRepo);
            SetOverUnderTabData(overUnder15BetData, this.dvgOver1_5, this.dvgOver1_5H2H, this.dvgUnder1_5, this.dvgUnder1_5H2H);

            var rangeBetData = analysisServices.GetRangeBetData(match,
                bgTeamsStatisitcsRepo);
            SetRangeTabData(rangeBetData, this.dvg01Range, this.dvg23Range, this.dvg4PlusRange, this.dvg01H2HRange, this.dvg23H2HRange, this.dvg4PlusH2HRange);

            var bothScoreBetData = analysisServices.GetBothScoreBetData(match,
                bgTeamsStatisitcsRepo);
            SetBothScoreTabData(bothScoreBetData, this.dvgBothScoreYes, this.dvgBothScoreNo, this.dvgBothScoreYesH2H, this.dvgBothScoreNoH2H);

            var winDrawLoseBetData = analysisServices.GetWinDrawLoseBetData(match,
                bgTeamsStatisitcsRepo);
            SetWinDrawLoseTabData(winDrawLoseBetData, this.dvgWinDraw, this.dvgNoDraw, this.dvgDrawLose, this.dvgWinDrawH2H, this.dvgNoDrawH2H, this.dvgDrawLoseH2H);
        }

        private void SetBothScoreTabData(BgBothScoreBetData bothScoreBetData, DataGridView dvgBothScoreYes, DataGridView dvgBothScoreNo, DataGridView dvgBothScoreYesH2H, DataGridView dvgBothScoreNoH2H)
        {
            var bothYes = new List<MonitorYesNoBet>();
            var bothYesH2H = new List<MonitorYesNoH2HBet>();
            for (var index = 0; index < bothScoreBetData.BothScoreYes.Totals.Count; index++)
            {
                bothYes.Add(new MonitorYesNoBet(BgBetTypeComponent.BothScoreYes, bothScoreBetData.BothScoreYes, index));
                bothYesH2H.Add(new MonitorYesNoH2HBet(BgBetTypeComponent.BothScoreYes, bothScoreBetData.BothScoreYesH2H, index));
            }
            RefreshTabData<MonitorYesNoBet>(dvgBothScoreYes, bothYes);
            RefreshTabData<MonitorYesNoH2HBet>(dvgBothScoreYesH2H, bothYesH2H);

            var bothNo = new List<MonitorYesNoBet>();
            var bothNoH2H = new List<MonitorYesNoH2HBet>();
            for (var index = 0; index < bothScoreBetData.BothScoreNo.Totals.Count; index++)
            {
                bothNo.Add(new MonitorYesNoBet(BgBetTypeComponent.BothScoreNo, bothScoreBetData.BothScoreNo, index));
                bothNoH2H.Add(new MonitorYesNoH2HBet(BgBetTypeComponent.BothScoreNo, bothScoreBetData.BothScoreNoH2H, index));
            }
            RefreshTabData<MonitorYesNoBet>(dvgBothScoreNo, bothNo);
            RefreshTabData<MonitorYesNoH2HBet>(dvgBothScoreNoH2H, bothNoH2H);
        }

        private void SetWinDrawLoseTabData(BgWinDrawLoseBetData winDrawLoseBetData, DataGridView dvgWinDraw, DataGridView dvgNoDraw, DataGridView dvgDrawLose, DataGridView dvgWinDrawH2H, DataGridView dvgNoDrawH2H, DataGridView dvgDrawLoseH2H)
        {
            var winDraw = new List<MonitorYesNoBet>();
            var noDraw = new List<MonitorYesNoBet>();
            var drawLose = new List<MonitorYesNoBet>();
            var winDrawH2H = new List<MonitorYesNoH2HBet>();
            var noDrawH2H = new List<MonitorYesNoH2HBet>();
            var drawLoseH2H = new List<MonitorYesNoH2HBet>();
            for (var index = 0; index < winDrawLoseBetData.WinDraw.Totals.Count; index++)
            {
                winDraw.Add(new MonitorYesNoBet(BgBetTypeComponent.WinOrDraw, winDrawLoseBetData.WinDraw, index));
                noDraw.Add(new MonitorYesNoBet(BgBetTypeComponent.HomeAwayNoDraw, winDrawLoseBetData.NoDraw, index));
                drawLose.Add(new MonitorYesNoBet(BgBetTypeComponent.DrawOrLose, winDrawLoseBetData.DrawLose, index));
                winDrawH2H.Add(new MonitorYesNoH2HBet(BgBetTypeComponent.WinOrDraw, winDrawLoseBetData.WinDrawH2H, index));
                noDrawH2H.Add(new MonitorYesNoH2HBet(BgBetTypeComponent.HomeAwayNoDraw, winDrawLoseBetData.NoDrawH2H, index));
                drawLoseH2H.Add(new MonitorYesNoH2HBet(BgBetTypeComponent.DrawOrLose, winDrawLoseBetData.DrawLoseH2H, index));
            }
            RefreshTabData<MonitorYesNoBet>(dvgWinDraw, winDraw);
            RefreshTabData<MonitorYesNoBet>(dvgNoDraw, noDraw);
            RefreshTabData<MonitorYesNoBet>(dvgDrawLose, drawLose);
            RefreshTabData<MonitorYesNoH2HBet>(dvgWinDrawH2H, winDrawH2H);
            RefreshTabData<MonitorYesNoH2HBet>(dvgNoDrawH2H, noDrawH2H);
            RefreshTabData<MonitorYesNoH2HBet>(dvgDrawLoseH2H, drawLoseH2H);
        }

        private void SetRangeTabData(BgRangeBetData rangeBetData, 
            DataGridView dvg01Range, 
            DataGridView dvg23Range, 
            DataGridView dvg4PlusRange, 
            DataGridView dvg01H2HRange, 
            DataGridView dvg23H2HRange, 
            DataGridView dvg4PlusH2HRange)
        {
            var linesRange01 = new List<MonitorRangeHomeAwayBet>();
            var linesRange23 = new List<MonitorRangeHomeAwayBet>();
            var linesRange4Plus = new List<MonitorRangeHomeAwayBet>();
            var range01H2H = new List<MonitorRangeH2HBet>();
            var range23H2H = new List<MonitorRangeH2HBet>();
            var range4PlusH2H = new List<MonitorRangeH2HBet>();
            for (var index = 0; index < rangeBetData.RangeBetween0and1.Totals.Count; index++)
            {
                linesRange01.Add(new MonitorRangeHomeAwayBet(BgBetTypeComponent.Range0_1HomeAway, rangeBetData.RangeBetween0and1, index));
                linesRange23.Add(new MonitorRangeHomeAwayBet(BgBetTypeComponent.Range2_3HomeAway, rangeBetData.RangeBetween2and3, index));
                linesRange4Plus.Add(new MonitorRangeHomeAwayBet(BgBetTypeComponent.Range4PlusHomeAway, rangeBetData.Range4Plus, index));
                range01H2H.Add(new MonitorRangeH2HBet(BgBetTypeComponent.Range0_1HomeAway, rangeBetData.RangeBetween0and1H2H, index));
                range23H2H.Add(new MonitorRangeH2HBet(BgBetTypeComponent.Range2_3HomeAway, rangeBetData.RangeBetween2and3H2H, index));
                range4PlusH2H.Add(new MonitorRangeH2HBet(BgBetTypeComponent.Range4PlusHomeAway, rangeBetData.Range4PlusH2H, index));
            }
            RefreshTabData<MonitorRangeHomeAwayBet>(dvg01Range, linesRange01);
            RefreshTabData<MonitorRangeHomeAwayBet>(dvg23Range, linesRange23);
            RefreshTabData<MonitorRangeHomeAwayBet>(dvg4PlusRange, linesRange4Plus);
            RefreshTabData<MonitorRangeH2HBet>(dvg01H2HRange, range01H2H);
            RefreshTabData<MonitorRangeH2HBet>(dvg23H2HRange, range23H2H);
            RefreshTabData<MonitorRangeH2HBet>(dvg4PlusH2HRange, range4PlusH2H);
        }

        private void SetOverUnderTabData(BgOverUnderBetData overUnder35BetData, 
            DataGridView dvgOver, 
            DataGridView dvgOverH2H, 
            DataGridView dvgUnder, 
            DataGridView dvgUnderH2H)
        {
            var linesOverOver = new List<MonitorHomeOverAwayOver>();
            var overH2H = new List<MonitorOverH2HBet>();
            for (var index = 0; index < overUnder35BetData.HomeOverAwayOver.Totals.Count; index++)
            {
                linesOverOver.Add(new MonitorHomeOverAwayOver(overUnder35BetData, index));
                overH2H.Add(new MonitorOverH2HBet(overUnder35BetData, index));
            }
            RefreshTabData<MonitorHomeOverAwayOver>(dvgOver, linesOverOver);
            RefreshTabData<MonitorOverH2HBet>(dvgOverH2H, overH2H);

            var linesUnderUnder = new List<MonitorHomeUnderAwayUnder>();
            var underH2H = new List<MonitorUnderH2HBet>();
            for (var index = 0; index < overUnder35BetData.HomeUnderAwayUnder.Totals.Count; index++)
            {
                linesUnderUnder.Add(new MonitorHomeUnderAwayUnder(overUnder35BetData, index));
                underH2H.Add(new MonitorUnderH2HBet(overUnder35BetData, index));
            }
            RefreshTabData<MonitorHomeUnderAwayUnder>(dvgUnder, linesUnderUnder);
            RefreshTabData<MonitorUnderH2HBet>(dvgUnderH2H, underH2H);
        }

        private void SetFullTimeTabData(BgFullTimeBetData fullTimeBetData)
        {
            var linesHomeWin = new List<MonitorFullTimeHomeWinBet>();
            var linesHomeWinH2H = new List<MonitorFullTimeHomeWinH2HBet>();
            for (var index = 0; index < fullTimeBetData.HomeWinAwayLose.Totals.Count; index++)
            {
                linesHomeWin.Add(new MonitorFullTimeHomeWinBet(fullTimeBetData, index));
                linesHomeWinH2H.Add(new MonitorFullTimeHomeWinH2HBet(fullTimeBetData, index));
            }
            RefreshTabData<MonitorFullTimeHomeWinBet>(this.dgvFullTimeHomeWin, linesHomeWin);
            RefreshTabData<MonitorFullTimeHomeWinH2HBet>(this.dgvFullTimeHomeWinH2H, linesHomeWinH2H);

            var linesHomeDrawAwayDraw = new List<MonitorFullTimeHomeDrawAwayDraw>();
            var linesDrawH2H = new List<MonitorFullTimeDrawH2HBet>();
            for (var index = 0; index < fullTimeBetData.HomeDrawAwayDraw.Totals.Count; index++)
            {
                linesHomeDrawAwayDraw.Add(new MonitorFullTimeHomeDrawAwayDraw(fullTimeBetData, index));
                linesDrawH2H.Add(new MonitorFullTimeDrawH2HBet(fullTimeBetData, index));

            }
            RefreshTabData<MonitorFullTimeHomeDrawAwayDraw>(this.dgvFullTimeHomeDrawAwayDraw, linesHomeDrawAwayDraw);
            RefreshTabData<MonitorFullTimeDrawH2HBet>(this.dgvFullTimeDrawH2H, linesDrawH2H);

            var linesAwayWin = new List<MonitorFullTimeAwayWinBet>();
            var linesAwayWinH2H = new List<MonitorFullTimeAwayWinH2HBet>();
            for (var index = 0; index < fullTimeBetData.AwayWinHomeLose.Totals.Count; index++)
            {
                linesAwayWin.Add(new MonitorFullTimeAwayWinBet(fullTimeBetData, index));
                linesAwayWinH2H.Add(new MonitorFullTimeAwayWinH2HBet(fullTimeBetData, index));
            }
            RefreshTabData<MonitorFullTimeAwayWinBet>(this.dgvFullTimeAwayWin, linesAwayWin);
            RefreshTabData<MonitorFullTimeAwayWinH2HBet>(this.dgvFullTimeAwayWinH2H, linesAwayWinH2H);
        }
        private void RefreshTabData<T>(DataGridView dgv, List<T> collection)
        {
            dgv.Rows.Clear();
            dgv.Columns.Clear();
            dgv.Refresh();
            MonitorUtils.BuildGrid<T>(collection, dgv, this.abParams.Sport);
        }

        #region numericUpDown_Enter
        private void numericHomeOdd_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericDrawOdd_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void numericAwayOdd_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }
        #endregion
    }
}
