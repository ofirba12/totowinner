﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    public partial class ShowBgWinnerOdds : MaterialForm
    {
        public ShowBgWinnerOdds()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

        }
        public ShowBgWinnerOdds(List<TotoWinner.Services.Persistance.BgData.Feed_Goalserve_Odds_Fetch_Result> odds,
            string homeTeam,
            string awayTeam,
            BgSport sport) : this()
        {
            var oddList = odds.ToList();
            if (oddList.Count > 0)
            {
                this.Text = $"Odds for Goalserve MatchId={oddList.First().MatchId} --> {homeTeam} vs {awayTeam}";
            }
            dataGridView.Rows.Clear();
            dataGridView.Columns.Clear();
            dataGridView.Refresh();
            var oddsToShow = oddList
                .ConvertAll<WinnerBgOddsMonitor>(o => new WinnerBgOddsMonitor(o.Bookmaker,
                    (decimal?)o.HomeOdd,
                    (decimal?)o.DrawOdd,
                    (decimal?)o.AwayOdd,
                    o.Inserted,
                    o.LastUpdate));
            MonitorUtils.BuildGrid<WinnerBgOddsMonitor>(oddsToShow, this.dataGridView, sport);
        }
    }
}
