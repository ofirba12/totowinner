﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;
using static MaterialSkin.MaterialSkinManager;

namespace BgBettingData.Monitor
{
    public partial class WhyThisBetDashboard : MaterialForm
    {
        private BgSport sport;
        private int homeId;
        private string home;
        private int awayId;
        private string away;
        private decimal homeOdds;
        private decimal drawOdds;
        private decimal awayOdds;
        private BgImpliedStatisticType statisticType;
        private int statisticTypeValue;
        private BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;

        public WhyThisBetDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

        }

        public WhyThisBetDashboard(int columnIndex, int statisticTypeValue, BgSport sport, int homeId, string home, int awayId, string away, decimal homeOdds, decimal drawOdds, decimal awayOdds) : this()
        {
            this.sport = sport;
            this.homeId = homeId;
            this.home = home;
            this.awayId = awayId;
            this.away = away;
            this.homeOdds = homeOdds;
            this.drawOdds = drawOdds;
            this.awayOdds = awayOdds;
            this.statisticTypeValue = statisticTypeValue;
            var match = new BgMatchOdds(this.sport,
                this.homeId,
                this.awayId,
                this.homeOdds,
                this.drawOdds,
                this.awayOdds);
            var maxRows = 6;
            var bgTeamsAnalysisRepo = analysisServices.PrepareTeamsRepository(this.homeId, this.awayId);
            var bgTeamsStatisitcsRepo = analysisServices.PrepareTeamsStatisitcsRepository(bgTeamsAnalysisRepo, maxRows);
            var description = new List<string>() { string.Empty, string.Empty, string.Empty };
            switch (columnIndex)
            {
                case 11:
                    this.statisticType = BgImpliedStatisticType.Web_1;
                    var fullTimeBetData = analysisServices.GetFullTimeBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_1, home, away,fullTimeBetData);
                    BuildPopulationsGrids(fullTimeBetData.HomeWinAwayLose.HomeCandidates[0].Filter,
                        "HomeWin",
                        fullTimeBetData.HomeWinAwayLose.AwayCandidates[0].Filter,
                        "AwayLose",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 12:
                    this.statisticType = BgImpliedStatisticType.Web_X;
                    fullTimeBetData = analysisServices.GetFullTimeBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_X, home, away, fullTimeBetData);
                    BuildPopulationsGrids(fullTimeBetData.HomeDrawAwayDraw.HomeCandidates[0].Filter,
                        "HomeDraw",
                        fullTimeBetData.HomeDrawAwayDraw.AwayCandidates[0].Filter,
                        "AwayDraw",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 13:
                    this.statisticType = BgImpliedStatisticType.Web_2;
                    fullTimeBetData = analysisServices.GetFullTimeBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_2, home, away, fullTimeBetData);
                    BuildPopulationsGrids(fullTimeBetData.AwayWinHomeLose.HomeCandidates[0].Filter,
                        "AwayWin",
                        fullTimeBetData.AwayWinHomeLose.AwayCandidates[0].Filter,
                        "HomeLose",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo, 
                        description);
                    break;
                case 14:
                    this.statisticType = BgImpliedStatisticType.Web_Under1_5;
                    var overUnder15BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder1_5, match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Under1_5, home, away, overUnder15BetData);
                    BuildPopulationsGrids(overUnder15BetData.HomeUnderAwayUnder.HomeCandidates[0].Filter,
                        "HomeUnder",
                        overUnder15BetData.HomeUnderAwayUnder.AwayCandidates[0].Filter,
                        "AwayUnder",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 15:
                    this.statisticType = BgImpliedStatisticType.Web_Over1_5;
                    overUnder15BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder1_5, match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Over1_5, home, away, overUnder15BetData);
                    BuildPopulationsGrids(overUnder15BetData.HomeOverAwayOver.HomeCandidates[0].Filter,
                        "HomeOver",
                        overUnder15BetData.HomeOverAwayOver.AwayCandidates[0].Filter,
                        "AwayOver",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 16:
                    this.statisticType = BgImpliedStatisticType.Web_Under2_5;
                    var overUnder25BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder2_5, match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Under2_5, home, away, overUnder25BetData);
                    BuildPopulationsGrids(overUnder25BetData.HomeUnderAwayUnder.HomeCandidates[0].Filter,
                        "HomeUnder",
                        overUnder25BetData.HomeUnderAwayUnder.AwayCandidates[0].Filter,
                        "AwayUnder",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 17:
                    this.statisticType = BgImpliedStatisticType.Web_Over2_5;
                    overUnder25BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder2_5, match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Over2_5, home, away, overUnder25BetData);
                    BuildPopulationsGrids(overUnder25BetData.HomeOverAwayOver.HomeCandidates[0].Filter,
                        "HomeOver",
                        overUnder25BetData.HomeOverAwayOver.AwayCandidates[0].Filter,
                        "AwayOver",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 18:
                    this.statisticType = BgImpliedStatisticType.Web_Under3_5;
                    var overUnder35BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder3_5, match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Under3_5, home, away, overUnder35BetData);
                    BuildPopulationsGrids(overUnder35BetData.HomeUnderAwayUnder.HomeCandidates[0].Filter,
                        "HomeUnder",
                        overUnder35BetData.HomeUnderAwayUnder.AwayCandidates[0].Filter,
                        "AwayUnder",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 19:
                    this.statisticType = BgImpliedStatisticType.Web_Over3_5;
                    overUnder35BetData = analysisServices.GetOverUnderBetData(BgBetType.OverUnder3_5, match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_Over3_5, home, away, overUnder35BetData);
                    BuildPopulationsGrids(overUnder35BetData.HomeOverAwayOver.HomeCandidates[0].Filter,
                        "HomeOver",
                        overUnder35BetData.HomeOverAwayOver.AwayCandidates[0].Filter,
                        "AwayOver",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 20:
                    this.statisticType = BgImpliedStatisticType.Web_0_1;
                    var rangeBetData = analysisServices.GetRangeBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_0_1, home, away, rangeBetData);
                    BuildPopulationsGrids(rangeBetData.RangeBetween0and1.HomeCandidates[0].Filter,
                        "Home Range[0-1]",
                        rangeBetData.RangeBetween0and1.AwayCandidates[0].Filter,
                        "Away Range[0-1]",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 21:
                    this.statisticType = BgImpliedStatisticType.Web_2_3;
                    rangeBetData = analysisServices.GetRangeBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_2_3, home, away, rangeBetData);
                    BuildPopulationsGrids(rangeBetData.RangeBetween2and3.HomeCandidates[0].Filter,
                        "Home Range[2-3]",
                        rangeBetData.RangeBetween2and3.AwayCandidates[0].Filter,
                        "Away Range[2-3]",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 22:
                    this.statisticType = BgImpliedStatisticType.Web_4Plus;
                    rangeBetData = analysisServices.GetRangeBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_4Plus, home, away, rangeBetData);
                    BuildPopulationsGrids(rangeBetData.Range4Plus.HomeCandidates[0].Filter,
                        "Home Range[4+]",
                        rangeBetData.Range4Plus.AwayCandidates[0].Filter,
                        "Away Range[4+]",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 23:
                    this.statisticType = BgImpliedStatisticType.Web_BothScore;
                    var bothScoreBetData = analysisServices.GetBothScoreBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_BothScore, home, away, bothScoreBetData);
                    BuildPopulationsGrids(bothScoreBetData.BothScoreYes.HomeCandidates[0].Filter,
                        "Home Both Score",
                        bothScoreBetData.BothScoreYes.AwayCandidates[0].Filter,
                        "Away Both Score",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 24:
                    this.statisticType = BgImpliedStatisticType.Web_BothNoScore;
                    bothScoreBetData = analysisServices.GetBothScoreBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_BothNoScore, home, away, bothScoreBetData);
                    BuildPopulationsGrids(bothScoreBetData.BothScoreNo.HomeCandidates[0].Filter,
                        "Home Both No Score",
                        bothScoreBetData.BothScoreNo.AwayCandidates[0].Filter,
                        "Away Both No Score",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 25:
                    this.statisticType = BgImpliedStatisticType.Web_1X;
                    var winDrawLoseBetData = analysisServices.GetWinDrawLoseBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_1X, home, away, winDrawLoseBetData);
                    BuildPopulationsGrids(winDrawLoseBetData.WinDraw.HomeCandidates[0].Filter,
                        "Home Win Draw",
                        winDrawLoseBetData.WinDraw.AwayCandidates[0].Filter,
                        "Away Win Draw",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 26:
                    this.statisticType = BgImpliedStatisticType.Web_12;
                    winDrawLoseBetData = analysisServices.GetWinDrawLoseBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_12, home, away, winDrawLoseBetData);
                    BuildPopulationsGrids(winDrawLoseBetData.NoDraw.HomeCandidates[0].Filter,
                        "Home No Draw",
                        winDrawLoseBetData.NoDraw.AwayCandidates[0].Filter,
                        "Away No Draw",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
                case 27:
                    this.statisticType = BgImpliedStatisticType.Web_2X;
                    winDrawLoseBetData = analysisServices.GetWinDrawLoseBetData(match,
                        bgTeamsStatisitcsRepo);
                    description = analysisServices.WhyThisBetDescription(BgImpliedStatisticType.Web_2X, home, away, winDrawLoseBetData);
                    BuildPopulationsGrids(winDrawLoseBetData.DrawLose.HomeCandidates[0].Filter,
                        "Home Lose Draw",
                        winDrawLoseBetData.DrawLose.AwayCandidates[0].Filter,
                        "Away Lose Draw",
                        BgTeamsFilter.All_All,
                        bgTeamsAnalysisRepo,
                        description);
                    break;
            }
            this.Text = $"{home} Vs {away} => Why This Bet [{statisticTypeValue}] for [{this.statisticType}] Column";
        }
        private void BuildPopulationsGrids(BgTeamsFilter homeFilter,
            string homeTitle,
            BgTeamsFilter awayFilter,
            string awayTitle,
            BgTeamsFilter h2hFilter,
            BgTeamsAnalysisRepository bgTeamsAnalysisRepo,
            List<string> description)
        {
            var maxRows = 6;
            this.materialLabel1.Text = $"{homeTitle} - {this.home} - {homeFilter.ToString()}";
            this.textBox11.Text = $"{description[0]}{Environment.NewLine}{description[1]}{Environment.NewLine}{description[2]}{Environment.NewLine}{description[3]}{Environment.NewLine}{description[4]}";
            var fullTimeHomeWinTopCandidatePopulation = bgTeamsAnalysisRepo.TeamAmatchesFilteredWithIndexes[homeFilter].Take(maxRows);
            var fullTimeHomeWinTopCandidateMatches = fullTimeHomeWinTopCandidatePopulation.ToList()
                .ConvertAll<MonitorBgMatchWithIndexes>(f => new MonitorBgMatchWithIndexes(f));
            MonitorUtils.BuildGrid<MonitorBgMatchWithIndexes>(fullTimeHomeWinTopCandidateMatches, this.dataGridViewHomeBestItem, this.sport);

            this.materialLabel2.Text = $"{awayTitle} - {this.away} - {awayFilter.ToString()}";
            var fullTimeAwayLoseTopCandidatePopulation = bgTeamsAnalysisRepo.TeamBmatchesFilteredWithIndexes[awayFilter].Take(maxRows);
            var fullTimeAwayLoseTopCandidateMatches = fullTimeAwayLoseTopCandidatePopulation.ToList()
                    .ConvertAll<MonitorBgMatchWithIndexes>(f => new MonitorBgMatchWithIndexes(f));
            MonitorUtils.BuildGrid<MonitorBgMatchWithIndexes>(fullTimeAwayLoseTopCandidateMatches, this.dataGridViewAwayBestItem, this.sport);

            this.materialLabel3.Text = $"{homeTitle} - {this.home} - {BgTeamsFilter.All_All.ToString()}";
            var fullTimeHomeWinAllAllPopulation = bgTeamsAnalysisRepo.TeamAmatchesFilteredWithIndexes[BgTeamsFilter.All_All].Take(maxRows);
            var fullTimeHomeAllAllCandidateMatches = fullTimeHomeWinAllAllPopulation.ToList()
                    .ConvertAll<MonitorBgMatchWithIndexes>(f => new MonitorBgMatchWithIndexes(f));
            MonitorUtils.BuildGrid<MonitorBgMatchWithIndexes>(fullTimeHomeAllAllCandidateMatches, this.dataGridViewHomeAllAll, this.sport);

            this.materialLabel4.Text = $"{awayTitle} - {this.away} - {BgTeamsFilter.All_All.ToString()}";
            var fullTimeAwayLoseAllAllPopulation = bgTeamsAnalysisRepo.TeamBmatchesFilteredWithIndexes[BgTeamsFilter.All_All].Take(maxRows);
            var fullTimeAwayAllAllCandidateMatches = fullTimeAwayLoseAllAllPopulation.ToList()
                    .ConvertAll<MonitorBgMatchWithIndexes>(f => new MonitorBgMatchWithIndexes(f));
            MonitorUtils.BuildGrid<MonitorBgMatchWithIndexes>(fullTimeAwayAllAllCandidateMatches, this.dataGridViewAwayAllAll, this.sport);

            this.materialLabel5.Text = $"H2H - {this.home} Vs {this.away} - {h2hFilter.ToString()}";
            var homeWinH2HTopCandidatePopulation = bgTeamsAnalysisRepo.H2hFilteredWithIndexes[h2hFilter].Take(maxRows);
            var fullTimeH2HTopCandidateMatches = homeWinH2HTopCandidatePopulation.ToList()
                    .ConvertAll<MonitorBgMatchWithIndexes>(f => new MonitorBgMatchWithIndexes(f));
            MonitorUtils.BuildGrid<MonitorBgMatchWithIndexes>(fullTimeH2HTopCandidateMatches, this.dataGridViewHomeH2HBestItem, this.sport);
        }

        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dgv = (DataGridView)sender;
            if (dgv.Rows[e.RowIndex].Tag is MonitorBgMatchWithIndexes)
                MonitorUtils.MonitorBgMatchWithIndexesCellConditionalFormating(dgv, e);
        }

    }
}
