﻿namespace BgBettingData.Monitor
{
    partial class WhyThisBetDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewHomeBestItem = new System.Windows.Forms.DataGridView();
            this.dataGridViewAwayBestItem = new System.Windows.Forms.DataGridView();
            this.dataGridViewHomeAllAll = new System.Windows.Forms.DataGridView();
            this.dataGridViewAwayAllAll = new System.Windows.Forms.DataGridView();
            this.dataGridViewHomeH2HBestItem = new System.Windows.Forms.DataGridView();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox11 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomeBestItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAwayBestItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomeAllAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAwayAllAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomeH2HBestItem)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewHomeBestItem
            // 
            this.dataGridViewHomeBestItem.AllowUserToDeleteRows = false;
            this.dataGridViewHomeBestItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHomeBestItem.Location = new System.Drawing.Point(3, 143);
            this.dataGridViewHomeBestItem.Name = "dataGridViewHomeBestItem";
            this.dataGridViewHomeBestItem.ReadOnly = true;
            this.dataGridViewHomeBestItem.Size = new System.Drawing.Size(1675, 170);
            this.dataGridViewHomeBestItem.TabIndex = 0;
            this.dataGridViewHomeBestItem.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // dataGridViewAwayBestItem
            // 
            this.dataGridViewAwayBestItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAwayBestItem.Location = new System.Drawing.Point(3, 338);
            this.dataGridViewAwayBestItem.Name = "dataGridViewAwayBestItem";
            this.dataGridViewAwayBestItem.Size = new System.Drawing.Size(1675, 161);
            this.dataGridViewAwayBestItem.TabIndex = 1;
            this.dataGridViewAwayBestItem.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // dataGridViewHomeAllAll
            // 
            this.dataGridViewHomeAllAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHomeAllAll.Location = new System.Drawing.Point(3, 524);
            this.dataGridViewHomeAllAll.Name = "dataGridViewHomeAllAll";
            this.dataGridViewHomeAllAll.Size = new System.Drawing.Size(1675, 168);
            this.dataGridViewHomeAllAll.TabIndex = 2;
            this.dataGridViewHomeAllAll.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // dataGridViewAwayAllAll
            // 
            this.dataGridViewAwayAllAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAwayAllAll.Location = new System.Drawing.Point(3, 717);
            this.dataGridViewAwayAllAll.Name = "dataGridViewAwayAllAll";
            this.dataGridViewAwayAllAll.Size = new System.Drawing.Size(1675, 161);
            this.dataGridViewAwayAllAll.TabIndex = 3;
            this.dataGridViewAwayAllAll.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // dataGridViewHomeH2HBestItem
            // 
            this.dataGridViewHomeH2HBestItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHomeH2HBestItem.Location = new System.Drawing.Point(3, 903);
            this.dataGridViewHomeH2HBestItem.Name = "dataGridViewHomeH2HBestItem";
            this.dataGridViewHomeH2HBestItem.Size = new System.Drawing.Size(1675, 150);
            this.dataGridViewHomeH2HBestItem.TabIndex = 4;
            this.dataGridViewHomeH2HBestItem.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel1.Location = new System.Drawing.Point(3, 121);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(107, 19);
            this.materialLabel1.TabIndex = 5;
            this.materialLabel1.Text = "materialLabel1";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel2.Location = new System.Drawing.Point(3, 316);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(107, 19);
            this.materialLabel2.TabIndex = 6;
            this.materialLabel2.Text = "materialLabel2";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel3.Location = new System.Drawing.Point(3, 502);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(107, 19);
            this.materialLabel3.TabIndex = 7;
            this.materialLabel3.Text = "materialLabel3";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel4.Location = new System.Drawing.Point(3, 695);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(107, 19);
            this.materialLabel4.TabIndex = 8;
            this.materialLabel4.Text = "materialLabel4";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel5.Location = new System.Drawing.Point(3, 881);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(107, 19);
            this.materialLabel5.TabIndex = 9;
            this.materialLabel5.Text = "materialLabel5";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.textBox11);
            this.panel1.Controls.Add(this.materialLabel1);
            this.panel1.Controls.Add(this.dataGridViewAwayBestItem);
            this.panel1.Controls.Add(this.materialLabel2);
            this.panel1.Controls.Add(this.materialLabel5);
            this.panel1.Controls.Add(this.dataGridViewHomeBestItem);
            this.panel1.Controls.Add(this.materialLabel4);
            this.panel1.Controls.Add(this.dataGridViewHomeAllAll);
            this.panel1.Controls.Add(this.materialLabel3);
            this.panel1.Controls.Add(this.dataGridViewAwayAllAll);
            this.panel1.Controls.Add(this.dataGridViewHomeH2HBestItem);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1709, 750);
            this.panel1.TabIndex = 10;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(3, 3);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(1675, 105);
            this.textBox11.TabIndex = 13;
            // 
            // WhyThisBetDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1715, 817);
            this.Controls.Add(this.panel1);
            this.Name = "WhyThisBetDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WhyThisBetDashboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomeBestItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAwayBestItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomeAllAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAwayAllAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomeH2HBestItem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewHomeBestItem;
        private System.Windows.Forms.DataGridView dataGridViewAwayBestItem;
        private System.Windows.Forms.DataGridView dataGridViewHomeAllAll;
        private System.Windows.Forms.DataGridView dataGridViewAwayAllAll;
        private System.Windows.Forms.DataGridView dataGridViewHomeH2HBestItem;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox11;
    }
}