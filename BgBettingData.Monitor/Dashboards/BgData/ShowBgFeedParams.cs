﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    internal class ShowBgFeedParams
    {
        public DateTime ChosenDate { get; set; }
        public BgSport Sport { get; set; }
//        public bool IsOnline { get; set; }
        public int GmtOffset { get; set; }
        //public int? CountryId { get; set; }
        //public int? LeagueId { get; set; }
    }
}
