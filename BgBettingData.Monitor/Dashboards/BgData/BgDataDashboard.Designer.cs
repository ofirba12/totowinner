﻿namespace BgBettingData.Monitor
{
    partial class BgDataDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.buttonShow = new MaterialSkin.Controls.MaterialButton();
            this.numericUpDownGmtOffset = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelLeague = new System.Windows.Forms.Label();
            this.comboBoxCountries = new System.Windows.Forms.ComboBox();
            this.comboBoxLeagues = new System.Windows.Forms.ComboBox();
            this.buttonClearFilter = new MaterialSkin.Controls.MaterialButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGmtOffset)).BeginInit();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(3, 64);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(227, 414);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(230, 64);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(567, 414);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellMouseEnter);
            // 
            // monthCalendar
            // 
            this.monthCalendar.Location = new System.Drawing.Point(3, 64);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 2;
            this.monthCalendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar_DateSelected);
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Soccer",
            "Basketball",
            "Tennis",
            "Handball",
            "Football",
            "Baseball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(18, 238);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 21);
            this.comboBoxSportType.TabIndex = 3;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // buttonShow
            // 
            this.buttonShow.AutoSize = false;
            this.buttonShow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonShow.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonShow.Depth = 0;
            this.buttonShow.HighEmphasis = true;
            this.buttonShow.Icon = null;
            this.buttonShow.Location = new System.Drawing.Point(18, 268);
            this.buttonShow.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonShow.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonShow.Size = new System.Drawing.Size(197, 36);
            this.buttonShow.TabIndex = 4;
            this.buttonShow.Text = "Get Data From Database";
            this.buttonShow.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonShow.UseAccentColor = false;
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // numericUpDownGmtOffset
            // 
            this.numericUpDownGmtOffset.Location = new System.Drawing.Point(167, 239);
            this.numericUpDownGmtOffset.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.numericUpDownGmtOffset.Minimum = new decimal(new int[] {
            12,
            0,
            0,
            -2147483648});
            this.numericUpDownGmtOffset.Name = "numericUpDownGmtOffset";
            this.numericUpDownGmtOffset.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownGmtOffset.TabIndex = 11;
            this.numericUpDownGmtOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "GMT ";
            // 
            // labelCountry
            // 
            this.labelCountry.AutoSize = true;
            this.labelCountry.Location = new System.Drawing.Point(14, 331);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(43, 13);
            this.labelCountry.TabIndex = 13;
            this.labelCountry.Text = "Country";
            // 
            // labelLeague
            // 
            this.labelLeague.AutoSize = true;
            this.labelLeague.Location = new System.Drawing.Point(15, 373);
            this.labelLeague.Name = "labelLeague";
            this.labelLeague.Size = new System.Drawing.Size(43, 13);
            this.labelLeague.TabIndex = 14;
            this.labelLeague.Text = "League";
            // 
            // comboBoxCountries
            // 
            this.comboBoxCountries.FormattingEnabled = true;
            this.comboBoxCountries.Location = new System.Drawing.Point(18, 345);
            this.comboBoxCountries.Name = "comboBoxCountries";
            this.comboBoxCountries.Size = new System.Drawing.Size(197, 21);
            this.comboBoxCountries.TabIndex = 15;
            this.comboBoxCountries.SelectedIndexChanged += new System.EventHandler(this.comboBoxCountries_SelectedIndexChanged);
            // 
            // comboBoxLeagues
            // 
            this.comboBoxLeagues.FormattingEnabled = true;
            this.comboBoxLeagues.Location = new System.Drawing.Point(18, 391);
            this.comboBoxLeagues.Name = "comboBoxLeagues";
            this.comboBoxLeagues.Size = new System.Drawing.Size(197, 21);
            this.comboBoxLeagues.TabIndex = 16;
            this.comboBoxLeagues.SelectedIndexChanged += new System.EventHandler(this.comboBoxLeagues_SelectedIndexChanged);
            // 
            // buttonClearFilter
            // 
            this.buttonClearFilter.AutoSize = false;
            this.buttonClearFilter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonClearFilter.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonClearFilter.Depth = 0;
            this.buttonClearFilter.HighEmphasis = true;
            this.buttonClearFilter.Icon = null;
            this.buttonClearFilter.Location = new System.Drawing.Point(61, 430);
            this.buttonClearFilter.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonClearFilter.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonClearFilter.Name = "buttonClearFilter";
            this.buttonClearFilter.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonClearFilter.Size = new System.Drawing.Size(90, 36);
            this.buttonClearFilter.TabIndex = 17;
            this.buttonClearFilter.Text = "Clear Filter";
            this.buttonClearFilter.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonClearFilter.UseAccentColor = false;
            this.buttonClearFilter.UseVisualStyleBackColor = true;
            this.buttonClearFilter.Click += new System.EventHandler(this.buttonClearFilter_Click);
            // 
            // BgDataDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 481);
            this.Controls.Add(this.buttonClearFilter);
            this.Controls.Add(this.comboBoxLeagues);
            this.Controls.Add(this.comboBoxCountries);
            this.Controls.Add(this.labelLeague);
            this.Controls.Add(this.labelCountry);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownGmtOffset);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.comboBoxSportType);
            this.Controls.Add(this.monthCalendar);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.splitter1);
            this.Name = "BgDataDashboard";
            this.Text = "BG Data";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGmtOffset)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.ComboBox comboBoxSportType;
        private MaterialSkin.Controls.MaterialButton buttonShow;
        private System.Windows.Forms.NumericUpDown numericUpDownGmtOffset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelLeague;
        private System.Windows.Forms.ComboBox comboBoxCountries;
        private System.Windows.Forms.ComboBox comboBoxLeagues;
        private MaterialSkin.Controls.MaterialButton buttonClearFilter;
    }
}