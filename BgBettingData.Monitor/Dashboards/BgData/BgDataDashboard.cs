﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class BgDataDashboard : MaterialForm
    {
        private GoalServeProviderServices _gsProviderSrv = GoalServeProviderServices.Instance;
        private ShowBgFeedParams showFeedParams;
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextMenuCloneToManual = new ToolStripMenuItem();
        private ToolStripMenuItem contextTeamsAnalysis = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuShowBetDetails = new ToolStripMenuItem();
        private ToolStripMenuItem contextWhyThisBet = new ToolStripMenuItem();
        private MapperRespository mapper;
        private List<BgFeedMonitor> monitorData;
        public BgDataDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            mapper = MonitorUtils.Mapper.GetBgMapper();
            this.showFeedParams = new ShowBgFeedParams();
            this.showFeedParams.ChosenDate = DateTime.Today;
            comboBoxSportType_SelectedIndexChanged(this.comboBoxSportType, null);
        }
        #region Context Menu
        private void dataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextMenuCloneToManual.Text = "Clone To Manual";
            contextMenuCloneToManual.Click -= new EventHandler(contextMenuCloneToManual_Click);
            contextMenuCloneToManual.Click += new EventHandler(contextMenuCloneToManual_Click);
            contextMenuShowBetDetails.Text = "Show Bet Details";
            contextMenuShowBetDetails.Click -= new EventHandler(contextMenuShowBetDetails_Click);
            contextMenuShowBetDetails.Click += new EventHandler(contextMenuShowBetDetails_Click);
            contextTeamsAnalysis.Text = "Show Teams Analysis";
            contextTeamsAnalysis.Click -= new EventHandler(contextMenuTeamsAnalysis_Click);
            contextTeamsAnalysis.Click += new EventHandler(contextMenuTeamsAnalysis_Click);
            contextWhyThisBet.Text = "Why This Bet?";
            contextWhyThisBet.Click -= new EventHandler(contextWhyThisBet_Click);
            contextWhyThisBet.Click += new EventHandler(contextWhyThisBet_Click);
            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridView.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridView.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (BgFeedMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            if (selectedMatch?.Source != BgSourceProvider.Manual)
            {
                selectedRow.ContextMenuStrip.Items.Add(contextMenuCloneToManual);
                selectedRow.ContextMenuStrip.Items.Add(contextTeamsAnalysis);                
                if (selectedMatch.HomeMatches.HasValue || selectedMatch.AwayMatches.HasValue)
                    selectedRow.ContextMenuStrip.Items.Add(contextMenuShowBetDetails);
            }
            else if (selectedMatch.HomeMatches.HasValue || selectedMatch.AwayMatches.HasValue)
            {
                selectedRow.ContextMenuStrip.Items.Add(contextTeamsAnalysis);
                selectedRow.ContextMenuStrip.Items.Add(contextMenuShowBetDetails);
            }
            selectedRow.ContextMenuStrip.Items.Add(contextWhyThisBet);
        }
        private void contextWhyThisBet_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            if (mouseLocation.ColumnIndex >= 11 /* WEB1 */ && mouseLocation.ColumnIndex <= 27 /*Web_2X*/)
            {
                var selectedMatch = (BgFeedMonitor)selectedRow.Tag;
                var dashboard = new WhyThisBetDashboard(
                    mouseLocation.ColumnIndex,
                    (int)selectedRow.Cells[mouseLocation.ColumnIndex].Value,
                    this.showFeedParams.Sport,
                    selectedMatch.HomeId,
                    selectedMatch.Home,
                    selectedMatch.AwayId,
                    selectedMatch.Away,
                    selectedMatch.HomeOdd.Value,
                    selectedMatch.DrawOdd.Value,
                    selectedMatch.AwayOdd.Value);
                dashboard.Show();
            }
        }
        private void contextMenuShowBetDetails_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (BgFeedMonitor)selectedRow.Tag;
            var dashboard = new BettingDataDashboard(this.showFeedParams.Sport,
                selectedMatch.HomeId,
                selectedMatch.Home,
                selectedMatch.AwayId,
                selectedMatch.Away,
                selectedMatch.HomeOdd.Value,
                selectedMatch.DrawOdd.Value,
                selectedMatch.AwayOdd.Value);
            dashboard.Show();
        }
        private void contextMenuTeamsAnalysis_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (BgFeedMonitor)selectedRow.Tag;
            var dashboard = new TeamABAnalysisDashboard(selectedMatch.HomeId,
                selectedMatch.Home,
                selectedMatch.AwayId,
                selectedMatch.Away,
                this.showFeedParams.Sport);
            dashboard.Show();
        }
        private void contextMenuCloneToManual_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (BgFeedMonitor)selectedRow.Tag;
            var quarters = new List<int?>() { selectedMatch.HQ1, selectedMatch.HQ2, selectedMatch.HQ3, selectedMatch.HQ4,
                        selectedMatch.AQ1, selectedMatch.AQ2, selectedMatch.AQ3, selectedMatch.AQ4 };
            var matchToUpsert = new ManualMatch(selectedMatch.Sport,
                selectedMatch.Status,
                selectedMatch.CountryId,
                selectedMatch.Country,
                selectedMatch.League,
                selectedMatch.LeagueId,
                selectedMatch.IsCup,
                selectedMatch.GameTime,
                selectedMatch.Home,
                selectedMatch.HomeId,
                selectedMatch.Away,
                selectedMatch.AwayId,
                false,
                null,
                new OddsAndScores(selectedMatch.HomeOdd, selectedMatch.DrawOdd, selectedMatch.AwayOdd, selectedMatch.HomeScore, selectedMatch.AwayScore,
                    selectedMatch.Sport == BgSport.Basketball && quarters.All(q => q.HasValue)
                    ? new BasketballQuatersScores(selectedMatch.HQ1.Value, selectedMatch.HQ2.Value, selectedMatch.HQ3.Value, selectedMatch.HQ4.Value,
                        selectedMatch.AQ1.Value, selectedMatch.AQ2.Value, selectedMatch.AQ3.Value, selectedMatch.AQ4.Value)
                    : null),
                DateTime.Now
                );
            var matchExists = ManualProviderServices.Instance.GetMatch(selectedMatch.CountryId.Value, selectedMatch.LeagueId, selectedMatch.HomeId, selectedMatch.AwayId, selectedMatch.GameTime);
            if (matchExists == null)
                ManualProviderServices.Instance.AddOrUpdateMatch(matchToUpsert);
            else
                MessageBox.Show($"Match already exists matchId={matchExists.MatchId} manualLatsUpdate={matchExists.LastUpdate} countryId {selectedMatch.CountryId}, leagueId {selectedMatch.LeagueId}, homeId {selectedMatch.HomeId}, awayId {selectedMatch.AwayId}, gameTime {selectedMatch.GameTime}", "Manual match already exist !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        #endregion
        private void monthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            this.showFeedParams.ChosenDate = e.Start.Date;
        }

        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.showFeedParams.Sport = sport;
                    if (this.showFeedParams.Sport == BgSport.Tennis || this.showFeedParams.Sport == BgSport.Football || this.showFeedParams.Sport == BgSport.Baseball)
                    {
                        this.comboBoxCountries.Hide();
                        this.labelCountry.Hide();
                    }
                    else
                    {
                        this.comboBoxCountries.Show();
                        this.labelCountry.Show();
                    }
                }
            }
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            var mapper = MonitorUtils.Mapper.GetBgMapper(); //BgMapperServices.Instance.GetBgMapper();
            var gmtOffset = (int)this.numericUpDownGmtOffset.Value;
            var finalFeed = BgDataServices.Instance.GetBgFeedWithFilter(this.showFeedParams.Sport,
                this.showFeedParams.ChosenDate,
                gmtOffset,
                null,
                null,
                mapper);
            //var statisticsTotals = BgDataServices.Instance.FilterMatchStatistics(finalFeed.Statistics);
            this.Text = $"BgData Dashboard - {this.showFeedParams.ChosenDate.ToLongDateString()}";

            this.dataGridView.Rows.Clear();
            this.dataGridView.Columns.Clear();
            this.dataGridView.Refresh();
            this.comboBoxCountries.Items.Clear();

            var impliedStatistics = BgDataServices.Instance.CreateImpliedStatistics(finalFeed.Matches, finalFeed.Statistics, finalFeed.StatisticsDetails);
            monitorData = finalFeed.Matches.Values.ToList()
                .ConvertAll<BgFeedMonitor>(f => new BgFeedMonitor(f.Source,
                    f.Sport,
                    f.MatchId,
                    f.Status,
                    f.LastUpdate,
                    f.CountryId,
                    f.Country,
                    f.League,
                    f.LeagueId,
                    f.IsCup,
                    f.GameStart,
                    f.Home,
                    f.HomeId,
                    f.Away,
                    f.AwayId,
                    finalFeed.Info.ContainsKey(f.MatchId) ? finalFeed.Info[f.MatchId].BadMatches : default(int?),
                    finalFeed.Info.ContainsKey(f.MatchId) ? finalFeed.Info[f.MatchId]?.HomeMatches : default(int?),
                    finalFeed.Info.ContainsKey(f.MatchId) ? finalFeed.Info[f.MatchId]?.AwayMatches : default(int?),
                    finalFeed.Info.ContainsKey(f.MatchId) ? finalFeed.Info[f.MatchId]?.Head2HeadMatches : default(int?),
                    finalFeed.Info.ContainsKey(f.MatchId) ? finalFeed.Info[f.MatchId]?.LastUpdate : default(DateTime?),
                    f.HomeOdd, f.DrawOdd, f.AwayOdd,
                    f.HomeScore, f.AwayScore,
                    f.HQ1, f.AQ1,
                    f.HQ2, f.AQ2,
                    f.HQ3, f.AQ3,
                    f.HQ4, f.AQ4,
                    impliedStatistics.ContainsKey(f.MatchId)? impliedStatistics[f.MatchId]: null,
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.FullTimeHomeWin : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.FullTimeDraw : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.FullTimeAwayWin : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.FullTimeHomeWinH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.FullTimeDrawH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.FullTimeAwayWinH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Over3_5 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Under3_5 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Over3_5H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Under3_5H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Over2_5 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Under2_5 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Over2_5H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Under2_5H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Over1_5 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Under1_5 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Over1_5H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Under1_5H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Range0_1 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Range2_3 : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Range4Plus : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Range0_1H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Range2_3H2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.Range4PlusH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.BothScore : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.NoBothScore : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.BothScoreH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.NoBothScoreH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.HomeDraw : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.NoDraw : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.DrawAway : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.HomeDrawH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.NoDrawH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.DrawAwayH2H : default(int?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.HomePower : default(decimal?),
                    finalFeed.Statistics.ContainsKey(f.MatchId) ? finalFeed.Statistics[f.MatchId].FirstOrDefault()?.AwayPower : default(decimal?)
                    ));                                                                          
            MonitorUtils.BuildGrid<BgFeedMonitor>(monitorData, this.dataGridView, this.showFeedParams.Sport);
            AddContextMenu();
            this.comboBoxCountries.Items.AddRange(monitorData.Select(m => m.Country).Distinct().OrderBy(c => c).ToArray());
            if (this.showFeedParams.Sport == BgSport.Tennis || this.showFeedParams.Sport == BgSport.Football || this.showFeedParams.Sport == BgSport.Baseball)
                this.comboBoxLeagues.Items.AddRange(monitorData.Select(m => m.League).Distinct().OrderBy(c => c).ToArray());
        }

        private void comboBoxCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            var countryItem = (ComboBox)sender;
            var countryId = mapper.BgCountriesNames.Where(c => c.Key.ItemName == countryItem.Text).First().Value;
            this.comboBoxLeagues.Items.Clear();
            this.comboBoxLeagues.Text = string.Empty;
            this.comboBoxLeagues.Items.AddRange(monitorData
                .Where(m => m.CountryId == countryId)
                .Select(m => m.League).Distinct().OrderBy(c => c).ToArray());
            for (var i = 0; i < this.dataGridView.Rows.Count; i++)
            {
                var match = (BgFeedMonitor)this.dataGridView.Rows[i].Tag;
                if (!this.dataGridView.Rows[i].IsNewRow && match.Country != countryItem.Text)
                    this.dataGridView.Rows[i].Visible = false;
                else
                    this.dataGridView.Rows[i].Visible = true;
            }
        }

        private void comboBoxLeagues_SelectedIndexChanged(object sender, EventArgs e)
        {
            var leagueItem = (ComboBox)sender;
            for (var i = 0; i < this.dataGridView.Rows.Count; i++)
            {
                var match = (BgFeedMonitor)this.dataGridView.Rows[i].Tag;
                if (!this.dataGridView.Rows[i].IsNewRow && match.League != leagueItem.Text)
                    this.dataGridView.Rows[i].Visible = false;
                else
                    this.dataGridView.Rows[i].Visible = true;
            }
        }

        private void buttonClearFilter_Click(object sender, EventArgs e)
        {
            this.comboBoxLeagues.Text = string.Empty;
            this.comboBoxCountries.Text = string.Empty;
            for (var i = 0; i < this.dataGridView.Rows.Count; i++)
            {
                this.dataGridView.Rows[i].Visible = true;
            }
        }

        List<BgMatch> GetPersistanceFeed()
        {
            var mapper = MonitorUtils.Mapper.GetBgMapper(); //BgMapperServices.Instance.GetBgMapper();
            var bgDataForDate = BgDataServices.Instance.GetBgData(this.showFeedParams.Sport, this.showFeedParams.ChosenDate, mapper);
            var feed = bgDataForDate.FeedWithNoDuplicates.Values.ToList();
            if (bgDataForDate.FeedWithDuplicates.Count > 0)
            {
                var duplicates = new StringBuilder();
                foreach (var dup in bgDataForDate.FeedWithDuplicates)
                {
                    dup.Value.ForEach(m => duplicates.AppendLine(m.MatchId.ToString()));
                    duplicates.AppendLine("--------");
                }
                MessageBox.Show($"There are {bgDataForDate.FeedWithDuplicates.Count} duplicates found{Environment.NewLine}{duplicates.ToString()}", "Duplicates found in feed !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                feed = bgDataForDate.All;
            }
            return feed;
        }
        List<BgMatch> GetOnlineFeed()
        {
            var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed((GoalServeSportType)this.showFeedParams.Sport, this.showFeedParams.ChosenDate);
            var mapper = MonitorUtils.Mapper.GetBgMapper(); //BgMapperServices.Instance.GetBgMapper();
            var bgData = BgDataServices.Instance.GenerateData(goldserveFeed.Feed, mapper);
            var filteredBgData = BgDataServices.Instance.MergeDuplicates(bgData);
            var manualDataByDate = ManualProviderServices.Instance.FetchManualMatchesByDate(this.showFeedParams.ChosenDate)
                .Where(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue && m.Sport == this.showFeedParams.Sport)
                .ToList();
            var finalFeed = BgDataServices.Instance.OverrideFeedWithManual(filteredBgData, manualDataByDate);
            return finalFeed.BgFeedAddOrUpdate;
        }
    }
}
