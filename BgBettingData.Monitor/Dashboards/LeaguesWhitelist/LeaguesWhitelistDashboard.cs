﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class LeaguesWhitelistDashboard : MaterialForm
    {
        private ShowParams showParams;
        private DataGridViewCellEventArgs mouseLocation;
        private MapperRespository mapper;
        private ToolStripMenuItem contextMenuDeleteLeague = new ToolStripMenuItem();


        public LeaguesWhitelistDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.showParams = new ShowParams();
            this.Text = "Leagues Whitelist Dashboard";
            this.labelTotalLeagues.Text = String.Empty;
            mapper = MonitorUtils.Mapper.GetBgMapper();
            comboBoxSportType_SelectedIndexChanged(this.comboBoxSportType, null);
        }

        private void buttonAddLeague_Click(object sender, EventArgs e)
        {
            var dialog = new WhitelistLeague(this.showParams.Sport);
            if (dialog.ShowDialog() == DialogResult.OK)
                buttonShow_Click(null, null);
        }
        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.showParams.Sport = sport;
                }
            }
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            var leagues = BgDataLeaguesWhitelistServices.Instance.GetAllBgLeaguesWhitelist(this.showParams.Sport, mapper)
                .ConvertAll<BgLeaguesWhitelistMonitor>(l => new BgLeaguesWhitelistMonitor(l.LeagueId, l.LeagueName))
                .ToList();
            this.labelTotalLeagues.Text = $"{leagues.Count()} Leagues";
            this.dataGridView.Rows.Clear();
            this.dataGridView.Columns.Clear();
            this.dataGridView.Refresh();
            MonitorUtils.BuildGrid<BgLeaguesWhitelistMonitor>(leagues, this.dataGridView, this.showParams.Sport);
            AddContextMenu();
        }

        private void dataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextMenuDeleteLeague.Text = "Delete League";
            contextMenuDeleteLeague.Click -= new EventHandler(contextMenuDeleteLeague_Click);
            contextMenuDeleteLeague.Click += new EventHandler(contextMenuDeleteLeague_Click);
            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;

            foreach (DataGridViewRow row in this.dataGridView.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridView.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            //var selectedMatch = (BgLeaguesWhitelistMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            selectedRow.ContextMenuStrip.Items.Add(contextMenuDeleteLeague);
        }
        private void contextMenuDeleteLeague_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var selected= (BgLeaguesWhitelistMonitor)selectedRow.Tag;
            BgDataPersistanceServices.Instance.DeleteBgLeaguesWhitelist(selected.LeagueId);
            buttonShow_Click(null, null);
        }
    }

}
