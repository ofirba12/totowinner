﻿namespace BgBettingData.Monitor 
{
    partial class WhitelistLeague
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelChoose = new MaterialSkin.Controls.MaterialLabel();
            this.textBoxLeague = new System.Windows.Forms.TextBox();
            this.buttonAdd = new MaterialSkin.Controls.MaterialButton();
            this.SuspendLayout();
            // 
            // labelChoose
            // 
            this.labelChoose.AutoSize = true;
            this.labelChoose.Depth = 0;
            this.labelChoose.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelChoose.Location = new System.Drawing.Point(13, 77);
            this.labelChoose.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelChoose.Name = "labelChoose";
            this.labelChoose.Size = new System.Drawing.Size(235, 19);
            this.labelChoose.TabIndex = 15;
            this.labelChoose.Text = "Please choose league to whitelist";
            // 
            // textBoxLeague
            // 
            this.textBoxLeague.Location = new System.Drawing.Point(16, 111);
            this.textBoxLeague.Name = "textBoxLeague";
            this.textBoxLeague.Size = new System.Drawing.Size(574, 20);
            this.textBoxLeague.TabIndex = 16;
            // 
            // buttonAdd
            // 
            this.buttonAdd.AutoSize = false;
            this.buttonAdd.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAdd.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonAdd.Depth = 0;
            this.buttonAdd.HighEmphasis = true;
            this.buttonAdd.Icon = null;
            this.buttonAdd.Location = new System.Drawing.Point(150, 149);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonAdd.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonAdd.Size = new System.Drawing.Size(243, 36);
            this.buttonAdd.TabIndex = 17;
            this.buttonAdd.Text = "ADD";
            this.buttonAdd.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonAdd.UseAccentColor = false;
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // WhitelistLeague
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 208);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.textBoxLeague);
            this.Controls.Add(this.labelChoose);
            this.Name = "WhitelistLeague";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "WhitelistLeague";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel labelChoose;
        private System.Windows.Forms.TextBox textBoxLeague;
        private MaterialSkin.Controls.MaterialButton buttonAdd;
    }
}