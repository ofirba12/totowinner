﻿namespace BgBettingData.Monitor
{
    partial class LeaguesWhitelistDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonShow = new System.Windows.Forms.Button();
            this.buttonAddLeague = new System.Windows.Forms.Button();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.labelTotalLeagues = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(408, 33);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(73, 25);
            this.buttonShow.TabIndex = 16;
            this.buttonShow.Text = "Show";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // buttonAddLeague
            // 
            this.buttonAddLeague.Location = new System.Drawing.Point(501, 33);
            this.buttonAddLeague.Name = "buttonAddLeague";
            this.buttonAddLeague.Size = new System.Drawing.Size(234, 25);
            this.buttonAddLeague.TabIndex = 17;
            this.buttonAddLeague.Text = "Add League To Whitelist";
            this.buttonAddLeague.UseVisualStyleBackColor = true;
            this.buttonAddLeague.Click += new System.EventHandler(this.buttonAddLeague_Click);
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(287, 35);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 21);
            this.comboBoxSportType.TabIndex = 18;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 64);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(1027, 383);
            this.dataGridView.TabIndex = 19;
            this.dataGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellMouseEnter);
            // 
            // labelTotalLeagues
            // 
            this.labelTotalLeagues.AutoSize = true;
            this.labelTotalLeagues.Location = new System.Drawing.Point(910, 39);
            this.labelTotalLeagues.Name = "labelTotalLeagues";
            this.labelTotalLeagues.Size = new System.Drawing.Size(75, 13);
            this.labelTotalLeagues.TabIndex = 21;
            this.labelTotalLeagues.Text = "Total Leagues";
            // 
            // LeaguesWhitelistDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 450);
            this.Controls.Add(this.labelTotalLeagues);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.comboBoxSportType);
            this.Controls.Add(this.buttonAddLeague);
            this.Controls.Add(this.buttonShow);
            this.Name = "LeaguesWhitelistDashboard";
            this.Text = "Leagues Whitelist";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Button buttonAddLeague;
        private System.Windows.Forms.ComboBox comboBoxSportType;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label labelTotalLeagues;
    }
}