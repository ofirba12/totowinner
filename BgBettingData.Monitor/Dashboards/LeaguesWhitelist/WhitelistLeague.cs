﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class WhitelistLeague : MaterialForm
    {
        private MapperRespository mapper;
        public WhitelistLeague(BgSport sport)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.Text = $"Whitelist {sport} League";
            mapper = MonitorUtils.Mapper.GetBgMapper();
            var existing = BgDataLeaguesWhitelistServices.Instance.GetAllBgLeaguesWhitelist(sport, mapper)
                .Select(t => t.LeagueId).ToDictionary<int, int>(m => m);
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                .Where(m => m.BgType == BgMapperType.Leagues && m.BgSport == sport && !existing.ContainsKey(m.BgId))
                , this.textBoxLeague);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBoxLeague.Text))
                return;
            if (this.textBoxLeague.Text.Contains('#'))
            {
                var bgId = this.textBoxLeague.Text.Split('#')[1].Trim();
                //var item = new List<BgTeamAnalysis>() { new BgTeamAnalysis(int.Parse(bgId), 0, 0, 0) };
                BgDataPersistanceServices.Instance.MergeBgLeaguesWhitelist(int.Parse(bgId));
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

        }
    }
}
