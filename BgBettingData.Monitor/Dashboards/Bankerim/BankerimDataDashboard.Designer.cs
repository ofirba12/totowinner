﻿namespace BgBettingData.Monitor
{
    partial class BankerimDataDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewFeed = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxFilters = new System.Windows.Forms.GroupBox();
            this.buttonApplyFilter = new MaterialSkin.Controls.MaterialButton();
            this.comboBoxPrograms = new System.Windows.Forms.ComboBox();
            this.buttonFetchData = new MaterialSkin.Controls.MaterialButton();
            this.monthCalendarChooseDate = new System.Windows.Forms.MonthCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBoxFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewFeed
            // 
            this.dataGridViewFeed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFeed.Location = new System.Drawing.Point(236, 64);
            this.dataGridViewFeed.Name = "dataGridViewFeed";
            this.dataGridViewFeed.Size = new System.Drawing.Size(775, 526);
            this.dataGridViewFeed.TabIndex = 5;
            this.dataGridViewFeed.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFeed_CellMouseEnter);
            this.dataGridViewFeed.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewFeed_CellPainting);
            this.dataGridViewFeed.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewFeed_ColumnHeaderMouseClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBoxFilters);
            this.panel1.Controls.Add(this.buttonFetchData);
            this.panel1.Controls.Add(this.monthCalendarChooseDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 64);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel1.Size = new System.Drawing.Size(233, 526);
            this.panel1.TabIndex = 4;
            // 
            // groupBoxFilters
            // 
            this.groupBoxFilters.Controls.Add(this.buttonApplyFilter);
            this.groupBoxFilters.Controls.Add(this.comboBoxPrograms);
            this.groupBoxFilters.Location = new System.Drawing.Point(13, 253);
            this.groupBoxFilters.Name = "groupBoxFilters";
            this.groupBoxFilters.Size = new System.Drawing.Size(195, 112);
            this.groupBoxFilters.TabIndex = 8;
            this.groupBoxFilters.TabStop = false;
            this.groupBoxFilters.Text = "פילטר";
            // 
            // buttonApplyFilter
            // 
            this.buttonApplyFilter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonApplyFilter.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonApplyFilter.Depth = 0;
            this.buttonApplyFilter.HighEmphasis = true;
            this.buttonApplyFilter.Icon = null;
            this.buttonApplyFilter.Location = new System.Drawing.Point(63, 67);
            this.buttonApplyFilter.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonApplyFilter.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonApplyFilter.Name = "buttonApplyFilter";
            this.buttonApplyFilter.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonApplyFilter.Size = new System.Drawing.Size(97, 36);
            this.buttonApplyFilter.TabIndex = 6;
            this.buttonApplyFilter.Text = "בצע פילטור";
            this.buttonApplyFilter.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonApplyFilter.UseAccentColor = false;
            this.buttonApplyFilter.UseVisualStyleBackColor = true;
            this.buttonApplyFilter.Click += new System.EventHandler(this.buttonApplyFilter_Click);
            // 
            // comboBoxPrograms
            // 
            this.comboBoxPrograms.FormattingEnabled = true;
            this.comboBoxPrograms.Items.AddRange(new object[] {
            "All"});
            this.comboBoxPrograms.Location = new System.Drawing.Point(63, 19);
            this.comboBoxPrograms.Name = "comboBoxPrograms";
            this.comboBoxPrograms.Size = new System.Drawing.Size(126, 21);
            this.comboBoxPrograms.TabIndex = 3;
            this.comboBoxPrograms.Text = "הכל";
            this.comboBoxPrograms.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrograms_SelectedIndexChanged);
            // 
            // buttonFetchData
            // 
            this.buttonFetchData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonFetchData.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonFetchData.Depth = 0;
            this.buttonFetchData.HighEmphasis = true;
            this.buttonFetchData.Icon = null;
            this.buttonFetchData.Location = new System.Drawing.Point(62, 186);
            this.buttonFetchData.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonFetchData.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonFetchData.Name = "buttonFetchData";
            this.buttonFetchData.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonFetchData.Size = new System.Drawing.Size(106, 36);
            this.buttonFetchData.TabIndex = 4;
            this.buttonFetchData.Text = "הבא משחקים";
            this.buttonFetchData.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonFetchData.UseAccentColor = false;
            this.buttonFetchData.UseVisualStyleBackColor = true;
            this.buttonFetchData.Click += new System.EventHandler(this.buttonFetchData_Click);
            // 
            // monthCalendarChooseDate
            // 
            this.monthCalendarChooseDate.Location = new System.Drawing.Point(0, 9);
            this.monthCalendarChooseDate.Name = "monthCalendarChooseDate";
            this.monthCalendarChooseDate.TabIndex = 1;
            this.monthCalendarChooseDate.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarChooseDate_DateChanged);
            // 
            // BankerimDataDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 593);
            this.Controls.Add(this.dataGridViewFeed);
            this.Controls.Add(this.panel1);
            this.Name = "BankerimDataDashboard";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.Text = "WINNER & BGDATA TIPS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxFilters.ResumeLayout(false);
            this.groupBoxFilters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewFeed;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxFilters;
        private MaterialSkin.Controls.MaterialButton buttonApplyFilter;
        private System.Windows.Forms.ComboBox comboBoxPrograms;
        private MaterialSkin.Controls.MaterialButton buttonFetchData;
        private System.Windows.Forms.MonthCalendar monthCalendarChooseDate;
    }
}