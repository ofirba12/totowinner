﻿using System;

namespace BgBettingData.Monitor
{
    internal class BankerimKeyMatch : IEquatable<BankerimKeyMatch>
    {
        public long ProgramId { get; }
        public int TotoId { get; }

        public BankerimKeyMatch(long programId, int totoId)
        {
            this.ProgramId = programId;
            this.TotoId = totoId;
        }

        public bool Equals(BankerimKeyMatch other)
        {
            return this.ProgramId == other.ProgramId && this.TotoId == other.TotoId;
        }
        public override int GetHashCode()
        {
            //return this.ProgramId.GetHashCode() + this.TotoId.GetHashCode();
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + ProgramId.GetHashCode();
                hash = hash * 23 + TotoId.GetHashCode();
                return hash;
            }
        }

    }
}