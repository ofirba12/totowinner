﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace BgBettingData.Monitor
{
    public partial class BankerimDataDashboard : MaterialForm
    {
        WinnerDataServices winSrv = WinnerDataServices.Instance;
        BgDataServices bgSrv = BgDataServices.Instance;

        private readonly MapperRespository bgMapper;
        private DateTime winnerLineDate = DateTime.Today;
        private List<string> programs;
        private string programIdFilter;
        private Dictionary<BankerimKeyMatch, BankerimBgCombinedFeed> feed;
        private List<int> nonRepeatedRowIndexes;

        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextTeamsAnalysis = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuShowBetDetails = new ToolStripMenuItem();
        private ToolStripMenuItem contextWhyThisBet = new ToolStripMenuItem();


        public BankerimDataDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            bgMapper = MonitorUtils.Mapper.GetBgMapper();
            nonRepeatedRowIndexes = new List<int>();
        }
        #region ContextMenu
        public void AddContextMenu()
        {
            contextMenuShowBetDetails.Text = "Show Bet Details";
            contextMenuShowBetDetails.Click -= new EventHandler(contextMenuShowBetDetails_Click);
            contextMenuShowBetDetails.Click += new EventHandler(contextMenuShowBetDetails_Click);
            contextTeamsAnalysis.Text = "Show Teams Analysis";
            contextTeamsAnalysis.Click -= new EventHandler(contextMenuTeamsAnalysis_Click);
            contextTeamsAnalysis.Click += new EventHandler(contextMenuTeamsAnalysis_Click);
            contextWhyThisBet.Text = "?Why This Bet";
            contextWhyThisBet.Click -= new EventHandler(contextWhyThisBet_Click);
            contextWhyThisBet.Click += new EventHandler(contextWhyThisBet_Click);

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridViewFeed.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridViewFeed.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridViewFeed_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (BankerimBgCombinedFeedMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            if (selectedMatch.BGHomeId.HasValue && (selectedMatch.HomeMatches.HasValue || selectedMatch.AwayMatches.HasValue))
            {
                if (Program.AdminDesktops.Contains(Environment.MachineName.ToUpper()))
                    selectedRow.ContextMenuStrip.Items.Add(contextTeamsAnalysis);
                selectedRow.ContextMenuStrip.Items.Add(contextMenuShowBetDetails);
            }
            if (!string.IsNullOrEmpty(selectedMatch.RankBestFather) && selectedMatch.BestTipFather.HasValue)
                selectedRow.ContextMenuStrip.Items.Add(contextWhyThisBet);
        }
        private void contextWhyThisBet_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (BankerimBgCombinedFeedMonitor)selectedRow.Tag;
            var columnIndex = 8; /* דירוג1 */
            //if (mouseLocation.ColumnIndex >= 8 /* דירוג1 */ && mouseLocation.ColumnIndex <= 15 /*דירוג 4פלוס*/)
            if (mouseLocation.ColumnIndex >= 8 /* דירוג1 */ && mouseLocation.ColumnIndex <= 10 /*דירוג 2*/)
            {
                switch (selectedMatch.RankBestFather)
                {
                    case "HOME WIN":
                        columnIndex = 11;
                        break;
                    case "DRAW":
                        columnIndex = 12;
                        break;
                    case "AWAY WIN":
                        columnIndex = 13;
                        break;
                }
            }
            else if (mouseLocation.ColumnIndex >= 11 && mouseLocation.ColumnIndex <= 12)
            {
                switch (selectedMatch.RankBestOverUnder)
                {
                    case "UNDER 2.5":
                        columnIndex = 16;
                        break;
                    case "OVER 2.5":
                        columnIndex = 17;
                        break;
                }
            }
            else if (mouseLocation.ColumnIndex >= 13 && mouseLocation.ColumnIndex <= 15)
            { 
                switch (selectedMatch.RankBestRange)
                { 
                    case "RANGE 0-1":
                        columnIndex = 20;
                        break;
                    case "RANGE 2-3":
                        columnIndex = 21;
                        break;
                    case "RANGE 4+":
                        columnIndex = 22;
                        break;
                }
            }
            var dashboard = new WhyThisBetDashboard(
                    columnIndex,
                    (int)selectedRow.Cells[mouseLocation.ColumnIndex].Value,
                    BgSport.Soccer,
                    selectedMatch.BGHomeId.Value,
                    selectedMatch.BGHome,
                    selectedMatch.BGAwayId.Value,
                    selectedMatch.BGAway,
                    selectedMatch.HomeOdd.Value,
                    selectedMatch.DrawOdd.HasValue
                    ? selectedMatch.DrawOdd.Value
                    : 1,
                    selectedMatch.AwayOdd.Value);
            dashboard.Show();
        }
        private void contextMenuShowBetDetails_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (BankerimBgCombinedFeedMonitor)selectedRow.Tag;
            var dashboard = new BettingDataDashboard(BgSport.Soccer,
                selectedMatch.BGHomeId.Value,
                selectedMatch.BGHome,
                selectedMatch.BGAwayId.Value,
                selectedMatch.BGAway,
                selectedMatch.HomeOdd.Value,
                selectedMatch.DrawOdd.Value,
                selectedMatch.AwayOdd.Value);
            dashboard.Show();
        }
        private void contextMenuTeamsAnalysis_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (BankerimBgCombinedFeedMonitor)selectedRow.Tag;
            var dashboard = new TeamABAnalysisDashboard(selectedMatch.BGHomeId.Value,
                selectedMatch.BGHome,
                selectedMatch.BGAwayId.Value,
                selectedMatch.BGAway,
                BgSport.Soccer);
            dashboard.Show();
        }
        #endregion
        private void monthCalendarChooseDate_DateChanged(object sender, DateRangeEventArgs e)
        {
            this.winnerLineDate = e.Start.Date;
        }

        private void dataGridViewFeed_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }

        private void buttonFetchData_Click(object sender, EventArgs e)
        {
            var winnerSport = winSrv.BgSportToWinnerSport(BgSport.Soccer);
            var winnerMapper = winSrv.GetWinnerMapper();
            var winnerData = winSrv.GetWinnerData(BgSport.Soccer, winnerLineDate, winnerMapper);
            this.Text = $"בנקרים {winnerLineDate.ToString("d")} תוכניה הכל";
            var bgFeed = BgDataServices.Instance.GetBgFeedWithFilter(BgSport.Soccer,
                    winnerLineDate,
                    3,
                    null,
                    null,
                    bgMapper);
            var bgMatches = new Dictionary<BgFeedExactKey, BgMatch>();
            bgFeed.AllMatches.ForEach(m =>
            {
                var key = new BgFeedExactKey(BgSport.Soccer, winnerLineDate, m.HomeId, m.AwayId);
                if (!bgMatches.ContainsKey(key))
                {
                    bgMatches.Add(key, m);
                }
            }
            );
            var impliedStatistics = bgSrv.CreateImpliedStatistics(bgFeed.Matches, bgFeed.Statistics, bgFeed.StatisticsDetails);
            var combined = winSrv.CombineBgAndWinnerFeeds(BgSport.Soccer, winnerLineDate, winnerSport, winnerMapper, winnerData, bgMatches, impliedStatistics);
            dataGridViewFeed.Rows.Clear();
            dataGridViewFeed.Columns.Clear();
            dataGridViewFeed.Refresh();

            programs = combined.Select(f => f.WinnerMatch.ProgramId.ToString()).ToList().Distinct().ToList();
            comboBoxPrograms.Items.Clear();
            comboBoxPrograms.Items.Add("הכל");
            comboBoxPrograms.Items.AddRange(programs.ToArray());
            comboBoxPrograms.Text = "הכל";
            comboBoxPrograms_SelectedIndexChanged(comboBoxPrograms, null);
            feed = PrepareData(combined);
            MonitorUtils.BuildGrid<BankerimBgCombinedFeedMonitor>(
                feed.Values.ToList()
                .ConvertAll<BankerimBgCombinedFeedMonitor>(m => new BankerimBgCombinedFeedMonitor(m))
                .OrderBy(m => m.LeagueName)
                .ToList(),
                this.dataGridViewFeed, 
                BgSport.Soccer);
            AddContextMenu();
        }

        private Dictionary<BankerimKeyMatch, BankerimBgCombinedFeed> PrepareData(List<WinnerBgCombinedFeed> combined)
        {
            var bgFeed = BgDataServices.Instance.GetBgFeedWithFilter(BgSport.Soccer,
                    winnerLineDate,
                    3,
                    null,
                    null,
                    bgMapper);
            var matches = combined
                    .GroupBy(m => new BankerimKeyMatch(m.WinnerMatch.ProgramId, m.WinnerMatch.TotoId))
                    .ToDictionary(m => m.Key, m => m.ToList());
            var bankerim = new Dictionary<BankerimKeyMatch, BankerimBgCombinedFeed>();
            foreach (var key in matches.Keys)
            {
                if (!bankerim.ContainsKey(key))
                {
                    var parent = matches[key].FirstOrDefault(p => p.WinnerMatch.BetTypeId == BetTypes.BetSoccer1x2);
                    var child2_5 = matches[key].FirstOrDefault(p => p.WinnerMatch.BetTypeId == BetTypes.BetSoccerOverUnder2_5);
                    var childTotalScore = matches[key].FirstOrDefault(p => p.WinnerMatch.BetTypeId == BetTypes.BetSoccerTotalScores);
                    if (parent != null)
                    {
                        var row = new BankerimBgCombinedFeed(parent, child2_5, childTotalScore, bgFeed);
                        bankerim.Add(key, row);                    
                    }
                }
            }
            return bankerim;
        }

        private void buttonApplyFilter_Click(object sender, EventArgs e)
        {
            var filtered = feed.Values.Where(f =>
                (string.IsNullOrEmpty(this.programIdFilter) || f.ProgramId.ToString() == this.programIdFilter))
                .ToList();
            dataGridViewFeed.Rows.Clear();
            dataGridViewFeed.Columns.Clear();
            dataGridViewFeed.Refresh();
            this.Text = $"בנקרים {winnerLineDate.ToString("d")} תוכניה {this.programIdFilter}";
            MonitorUtils.BuildGrid<BankerimBgCombinedFeedMonitor>(
                filtered.ToList().ConvertAll<BankerimBgCombinedFeedMonitor>(m => new BankerimBgCombinedFeedMonitor(m))
                .OrderBy(m => m.LeagueName)
                .ToList(),
                this.dataGridViewFeed,
                BgSport.Soccer);
            AddContextMenu();
        }

        private void comboBoxPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.ComboBox)
            {
                var comboBox = sender as System.Windows.Forms.ComboBox;
                var programId = comboBox.Text != "הכל"
                    ? programs.First(i => i == comboBox.Text)
                    : null;
                this.programIdFilter = programId;
            }
        }

        private void dataGridViewFeed_CellPainting(object sender, DataGridViewCellPaintingEventArgs args)
        {
            args.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;

            if (args.RowIndex < 1 || args.ColumnIndex < 0)
                return;

            var isRepeatedCellValue = IsRepeatedCellValue(args.RowIndex, args.ColumnIndex);
            if (isRepeatedCellValue)
            {
                if (!nonRepeatedRowIndexes.Contains(args.RowIndex))
                    nonRepeatedRowIndexes.Add(args.RowIndex);
                args.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            }
            else if (args.ColumnIndex >= 0 && !nonRepeatedRowIndexes.Contains(args.RowIndex))
            {
                args.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.Single;
                var row = this.dataGridViewFeed.Rows[args.RowIndex];
                args.Paint(args.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Border);
                using (Pen p = new Pen(Color.LightBlue, 2))
                {
                    args.Graphics.DrawLine(p, args.CellBounds.Left, args.CellBounds.Top, args.CellBounds.Right, args.CellBounds.Top);
                }
                args.Handled = true;
            }
        }

        private bool IsRepeatedCellValue(int rowIndex, int columnIndex)
        {
            var preIndex = rowIndex > 0
                ? rowIndex - 1 
                : 0;
            var row = (DataGridViewRow)this.dataGridViewFeed.Rows[rowIndex];
            var preRow = (DataGridViewRow)this.dataGridViewFeed.Rows[preIndex];            
            if (row.Tag != null && preRow.Tag != null)
            {
                var currentObject = row.Tag as BankerimBgCombinedFeedMonitor;
                var prevObject = preRow.Tag as BankerimBgCombinedFeedMonitor;
                var sameValue = currentObject.LeagueName == prevObject.LeagueName;
                if (columnIndex == 0 && preIndex >= 0 && sameValue)
                {
                    return true;
                }
            }
            return false;
        }

        private void dataGridViewFeed_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.nonRepeatedRowIndexes.Clear();
        }
    }
}
