﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class TeamsWatcherDashboard : MaterialForm
    {
        private ShowParams showParams;
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextMenuOpenAnalysis = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuDeleteWatch = new ToolStripMenuItem();
        public TeamsWatcherDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.showParams = new ShowParams();
            this.Text = "Teams Watcher";
            this.labelTotalRows.Text = String.Empty;
            this.labelLastUpdate.Text = String.Empty;
            this.labelTotalShows.Text = String.Empty;
            comboBoxSportType_SelectedIndexChanged(this.comboBoxSportType, null);
        }

        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.showParams.Sport = sport;
                }
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            var teams = BgDataAnalysisServices.Instance.GetBgTeamAnalysisMonitor(this.showParams.Sport)
                .OrderByDescending(t => t.MissingOdds + t.MissingResult)
                .ToList();
            this.dataGridView.Rows.Clear();
            this.dataGridView.Columns.Clear();
            this.dataGridView.Refresh();
            this.labelTotalRows.Text = $"{teams.Count()} Watchers";
            this.labelLastUpdate.Text = $"Last Update: {teams.Min(t => t.LastUpdate)}";
            this.labelTotalShows.Text = $"Total Shows: {teams.Sum(t =>t.TotalShows)}";
            MonitorUtils.BuildGrid<BgTeamAnalysisMonitor>(teams, this.dataGridView, this.showParams.Sport);
            AddContextMenu();
        }
        private bool CanAddWatch()
        {
            var systemRepository = BgSystemServices.Instance.GetBgSystemRepository();
            var busy1 = systemRepository.BgDataWatcherFinishAnalysis.HasValue &&  
                systemRepository.BgDataWatcherFinishAnalysis < systemRepository.BgDataWatcherStartAnalysis;
            var busy2 = !systemRepository.BgDataWatcherFinishAnalysis.HasValue && systemRepository.BgDataWatcherStartAnalysis.HasValue;
            return !busy1 && !busy2;

        }
        private void buttonAddWatch_Click(object sender, EventArgs e)
        {
            if (CanAddWatch())
            {
                var dialog = new AddTeamWatch(this.showParams.Sport);
                if (dialog.ShowDialog() == DialogResult.OK)
                    buttonGo_Click(null, null);
            }
            else
                MessageBox.Show($"Can not add watch at the moment, please try again in couple of mintues", "System is busy", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void dataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextMenuOpenAnalysis.Text = "Open Analysis";
            contextMenuOpenAnalysis.Click -= new EventHandler(contextMenuOpenAnalysis_Click);
            contextMenuOpenAnalysis.Click += new EventHandler(contextMenuOpenAnalysis_Click);
            contextMenuDeleteWatch.Text = "Delete Watch";
            contextMenuDeleteWatch.Click -= new EventHandler(contextMenuDeleteWatch_Click);
            contextMenuDeleteWatch.Click += new EventHandler(contextMenuDeleteWatch_Click);
            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;

            foreach (DataGridViewRow row in this.dataGridView.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridView.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (BgTeamAnalysisMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            selectedRow.ContextMenuStrip.Items.Add(contextMenuOpenAnalysis);
            selectedRow.ContextMenuStrip.Items.Add(contextMenuDeleteWatch);
        }
        private void contextMenuOpenAnalysis_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var selectedTeam = (BgTeamAnalysisMonitor)selectedRow.Tag;
            //var dialog = new TeamAnalysisDashboard(selectedTeam.TeamId, selectedTeam.TeamName, this.showParams.Sport);
            var dialog = new TeamABAnalysisDashboard(selectedTeam.TeamId, selectedTeam.TeamName, this.showParams.Sport);
            dialog.ShowDialog();
        }
        private void contextMenuDeleteWatch_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var selectedTeam = (BgTeamAnalysisMonitor)selectedRow.Tag;
            BgDataPersistanceServices.Instance.DeleteBgTeamWatcher(selectedTeam.TeamId);
            buttonGo_Click(null, null);
        }

    }
}
