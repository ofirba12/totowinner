﻿using MaterialSkin;
using MaterialSkin.Controls;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class AddTeamWatch : MaterialForm
    {
        private MapperRespository mapper;
        public AddTeamWatch(BgSport sport)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.Text = $"Add Watcher For {sport} Team";
            mapper = MonitorUtils.Mapper.GetBgMapper();//BgMapperServices.Instance.GetBgMapper();
            var existing = BgDataAnalysisServices.Instance.GetBgTeamAnalysisMonitor(sport)
                .Select(t => t.TeamId).ToDictionary<int, int>(m => m);
            MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == sport && !existing.ContainsKey(m.BgId))
                ,this.textBoxTeam);
        }

        private void buttonAdd_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBoxTeam.Text))
                return;
            if (this.textBoxTeam.Text.Contains('#'))
            {
                var bgId = this.textBoxTeam.Text.Split('#')[1].Trim();
                var item = new List<BgTeamAnalysis>() { new BgTeamAnalysis(int.Parse(bgId), 0, 0, 0) };
                BgDataPersistanceServices.Instance.MergeBgTeamsWatcher(item);
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
