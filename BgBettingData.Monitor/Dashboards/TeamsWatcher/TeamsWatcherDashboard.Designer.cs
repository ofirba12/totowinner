﻿namespace BgBettingData.Monitor
{
    partial class TeamsWatcherDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.buttonGo = new System.Windows.Forms.Button();
            this.buttonAddWatch = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.labelTotalRows = new System.Windows.Forms.Label();
            this.labelLastUpdate = new System.Windows.Forms.Label();
            this.labelTotalShows = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(303, 34);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 21);
            this.comboBoxSportType.TabIndex = 14;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(443, 34);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(73, 25);
            this.buttonGo.TabIndex = 15;
            this.buttonGo.Text = "Show";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // buttonAddWatch
            // 
            this.buttonAddWatch.Location = new System.Drawing.Point(530, 34);
            this.buttonAddWatch.Name = "buttonAddWatch";
            this.buttonAddWatch.Size = new System.Drawing.Size(91, 25);
            this.buttonAddWatch.TabIndex = 16;
            this.buttonAddWatch.Text = "Add Watch";
            this.buttonAddWatch.UseVisualStyleBackColor = true;
            this.buttonAddWatch.Click += new System.EventHandler(this.buttonAddWatch_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 64);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(1159, 383);
            this.dataGridView.TabIndex = 17;
            this.dataGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellMouseEnter);
            // 
            // labelTotalRows
            // 
            this.labelTotalRows.AutoSize = true;
            this.labelTotalRows.Location = new System.Drawing.Point(819, 40);
            this.labelTotalRows.Name = "labelTotalRows";
            this.labelTotalRows.Size = new System.Drawing.Size(64, 13);
            this.labelTotalRows.TabIndex = 18;
            this.labelTotalRows.Text = "Total Rows:";
            // 
            // labelLastUpdate
            // 
            this.labelLastUpdate.AutoSize = true;
            this.labelLastUpdate.Location = new System.Drawing.Point(925, 40);
            this.labelLastUpdate.Name = "labelLastUpdate";
            this.labelLastUpdate.Size = new System.Drawing.Size(68, 13);
            this.labelLastUpdate.TabIndex = 19;
            this.labelLastUpdate.Text = "Last Update:";
            // 
            // labelTotalShows
            // 
            this.labelTotalShows.AutoSize = true;
            this.labelTotalShows.Location = new System.Drawing.Point(636, 40);
            this.labelTotalShows.Name = "labelTotalShows";
            this.labelTotalShows.Size = new System.Drawing.Size(66, 13);
            this.labelTotalShows.TabIndex = 20;
            this.labelTotalShows.Text = "Total Shows";
            // 
            // TeamsWatcherDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 450);
            this.Controls.Add(this.labelTotalShows);
            this.Controls.Add(this.labelLastUpdate);
            this.Controls.Add(this.labelTotalRows);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.buttonAddWatch);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.comboBoxSportType);
            this.Name = "TeamsWatcherDashboard";
            this.Text = "TeamsWatcherDashboard";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSportType;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Button buttonAddWatch;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label labelTotalRows;
        private System.Windows.Forms.Label labelLastUpdate;
        private System.Windows.Forms.Label labelTotalShows;
    }
}