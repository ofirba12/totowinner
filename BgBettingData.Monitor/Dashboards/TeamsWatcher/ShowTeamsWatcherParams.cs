﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    internal class ShowParams
    {
        public BgSport Sport { get; set; }
    }
}
