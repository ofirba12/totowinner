﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    internal class ShowTeamABAnalysisParams
    {
        //public BgSport Sport { get; set; }
        //public int? TeamAId{ get; set; }
        //public int? TeamBId { get; set; }

        public BgTeamsFilter TeamAFilter { get; set; }
        public BgTeamsFilter TeamBFilter { get; set; }
        public bool ShowIndexes { get; set; }
    }
}
