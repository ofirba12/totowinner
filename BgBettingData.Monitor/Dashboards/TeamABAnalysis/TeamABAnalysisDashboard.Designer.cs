﻿namespace BgBettingData.Monitor
{
    partial class TeamABAnalysisDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.textBoxTeamA = new System.Windows.Forms.TextBox();
            this.buttonGo = new System.Windows.Forms.Button();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonStatistics = new System.Windows.Forms.Button();
            this.checkBoxShowIndexes = new System.Windows.Forms.CheckBox();
            this.comboBoxTeamBFilter = new System.Windows.Forms.ComboBox();
            this.comboBoxTeamAFilter = new System.Windows.Forms.ComboBox();
            this.labelH2HValue = new System.Windows.Forms.Label();
            this.labelH2H = new System.Windows.Forms.Label();
            this.labelTeamB = new System.Windows.Forms.Label();
            this.labelTeamA = new System.Windows.Forms.Label();
            this.labelNumberOfMatchesTeamB = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTeamB = new System.Windows.Forms.TextBox();
            this.labelNumberOfMatchesTeamA = new System.Windows.Forms.Label();
            this.labelTitleNumOfMatches = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageTeamA = new System.Windows.Forms.TabPage();
            this.dataGridViewTeamA = new System.Windows.Forms.DataGridView();
            this.tabPageTeamB = new System.Windows.Forms.TabPage();
            this.dataGridViewTeamB = new System.Windows.Forms.DataGridView();
            this.tabPageH2H = new System.Windows.Forms.TabPage();
            this.dataGridViewH2H = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageTeamA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamA)).BeginInit();
            this.tabPageTeamB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamB)).BeginInit();
            this.tabPageH2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewH2H)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxTeamA
            // 
            this.textBoxTeamA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.textBoxTeamA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxTeamA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBoxTeamA.Location = new System.Drawing.Point(205, 13);
            this.textBoxTeamA.Name = "textBoxTeamA";
            this.textBoxTeamA.Size = new System.Drawing.Size(574, 24);
            this.textBoxTeamA.TabIndex = 2;
            this.textBoxTeamA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTeamA_KeyDown);
            this.textBoxTeamA.Leave += new System.EventHandler(this.textBoxTeamA_Leave);
            // 
            // buttonGo
            // 
            this.buttonGo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonGo.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.buttonGo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonGo.Location = new System.Drawing.Point(1189, 7);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(64, 72);
            this.buttonGo.TabIndex = 4;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = false;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.comboBoxSportType.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.comboBoxSportType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(12, 12);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 25);
            this.comboBoxSportType.TabIndex = 1;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panel1.Controls.Add(this.buttonStatistics);
            this.panel1.Controls.Add(this.checkBoxShowIndexes);
            this.panel1.Controls.Add(this.comboBoxTeamBFilter);
            this.panel1.Controls.Add(this.comboBoxTeamAFilter);
            this.panel1.Controls.Add(this.labelH2HValue);
            this.panel1.Controls.Add(this.labelH2H);
            this.panel1.Controls.Add(this.labelTeamB);
            this.panel1.Controls.Add(this.labelTeamA);
            this.panel1.Controls.Add(this.labelNumberOfMatchesTeamB);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.buttonGo);
            this.panel1.Controls.Add(this.textBoxTeamB);
            this.panel1.Controls.Add(this.labelNumberOfMatchesTeamA);
            this.panel1.Controls.Add(this.labelTitleNumOfMatches);
            this.panel1.Controls.Add(this.comboBoxSportType);
            this.panel1.Controls.Add(this.textBoxTeamA);
            this.panel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel1.Location = new System.Drawing.Point(6, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1454, 79);
            this.panel1.TabIndex = 14;
            // 
            // buttonStatistics
            // 
            this.buttonStatistics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonStatistics.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.buttonStatistics.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonStatistics.Location = new System.Drawing.Point(1373, 7);
            this.buttonStatistics.Name = "buttonStatistics";
            this.buttonStatistics.Size = new System.Drawing.Size(78, 69);
            this.buttonStatistics.TabIndex = 21;
            this.buttonStatistics.Text = "Statistics";
            this.buttonStatistics.UseVisualStyleBackColor = false;
            this.buttonStatistics.Click += new System.EventHandler(this.buttonStatistics_Click);
            // 
            // checkBoxShowIndexes
            // 
            this.checkBoxShowIndexes.AutoSize = true;
            this.checkBoxShowIndexes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.checkBoxShowIndexes.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.checkBoxShowIndexes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.checkBoxShowIndexes.Location = new System.Drawing.Point(12, 53);
            this.checkBoxShowIndexes.Name = "checkBoxShowIndexes";
            this.checkBoxShowIndexes.Size = new System.Drawing.Size(113, 21);
            this.checkBoxShowIndexes.TabIndex = 7;
            this.checkBoxShowIndexes.Text = "Show Indexes";
            this.checkBoxShowIndexes.UseVisualStyleBackColor = false;
            this.checkBoxShowIndexes.CheckedChanged += new System.EventHandler(this.checkBoxShowIndexes_CheckedChanged);
            // 
            // comboBoxTeamBFilter
            // 
            this.comboBoxTeamBFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.comboBoxTeamBFilter.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.comboBoxTeamBFilter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.comboBoxTeamBFilter.FormattingEnabled = true;
            this.comboBoxTeamBFilter.Items.AddRange(new object[] {
            "All-All",
            "All-Favorite",
            "All-Netral",
            "All-Underdog",
            "Home-All",
            "Home-Favorite",
            "Home-Netral",
            "Home-Underdog",
            "Away-All",
            "Away-Favorite",
            "Away-Netral",
            "Away-Underdog"});
            this.comboBoxTeamBFilter.Location = new System.Drawing.Point(1259, 51);
            this.comboBoxTeamBFilter.Name = "comboBoxTeamBFilter";
            this.comboBoxTeamBFilter.Size = new System.Drawing.Size(108, 25);
            this.comboBoxTeamBFilter.TabIndex = 6;
            this.comboBoxTeamBFilter.Text = "All-All";
            this.comboBoxTeamBFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxTeamBFilter_SelectedIndexChanged);
            // 
            // comboBoxTeamAFilter
            // 
            this.comboBoxTeamAFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.comboBoxTeamAFilter.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.comboBoxTeamAFilter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.comboBoxTeamAFilter.FormattingEnabled = true;
            this.comboBoxTeamAFilter.Items.AddRange(new object[] {
            "All-All",
            "All-Favorite",
            "All-Netral",
            "All-Underdog",
            "Home-All",
            "Home-Favorite",
            "Home-Netral",
            "Home-Underdog",
            "Away-All",
            "Away-Favorite",
            "Away-Netral",
            "Away-Underdog"});
            this.comboBoxTeamAFilter.Location = new System.Drawing.Point(1259, 8);
            this.comboBoxTeamAFilter.Name = "comboBoxTeamAFilter";
            this.comboBoxTeamAFilter.Size = new System.Drawing.Size(108, 25);
            this.comboBoxTeamAFilter.TabIndex = 5;
            this.comboBoxTeamAFilter.Text = "All-All";
            this.comboBoxTeamAFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxTeamAFilter_SelectedIndexChanged);
            // 
            // labelH2HValue
            // 
            this.labelH2HValue.AutoSize = true;
            this.labelH2HValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelH2HValue.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelH2HValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelH2HValue.Location = new System.Drawing.Point(1129, 38);
            this.labelH2HValue.Name = "labelH2HValue";
            this.labelH2HValue.Size = new System.Drawing.Size(35, 17);
            this.labelH2HValue.TabIndex = 20;
            this.labelH2HValue.Text = "###";
            // 
            // labelH2H
            // 
            this.labelH2H.AutoSize = true;
            this.labelH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelH2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelH2H.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelH2H.Location = new System.Drawing.Point(1022, 38);
            this.labelH2H.Name = "labelH2H";
            this.labelH2H.Size = new System.Drawing.Size(101, 17);
            this.labelH2H.TabIndex = 19;
            this.labelH2H.Text = "Head To Head: ";
            // 
            // labelTeamB
            // 
            this.labelTeamB.AutoSize = true;
            this.labelTeamB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelTeamB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelTeamB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelTeamB.Location = new System.Drawing.Point(133, 54);
            this.labelTeamB.Name = "labelTeamB";
            this.labelTeamB.Size = new System.Drawing.Size(57, 17);
            this.labelTeamB.TabIndex = 18;
            this.labelTeamB.Text = "Team B:";
            // 
            // labelTeamA
            // 
            this.labelTeamA.AutoSize = true;
            this.labelTeamA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelTeamA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelTeamA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelTeamA.Location = new System.Drawing.Point(133, 16);
            this.labelTeamA.Name = "labelTeamA";
            this.labelTeamA.Size = new System.Drawing.Size(57, 17);
            this.labelTeamA.TabIndex = 17;
            this.labelTeamA.Text = "Team A:";
            // 
            // labelNumberOfMatchesTeamB
            // 
            this.labelNumberOfMatchesTeamB.AutoSize = true;
            this.labelNumberOfMatchesTeamB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelNumberOfMatchesTeamB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelNumberOfMatchesTeamB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelNumberOfMatchesTeamB.Location = new System.Drawing.Point(940, 58);
            this.labelNumberOfMatchesTeamB.Name = "labelNumberOfMatchesTeamB";
            this.labelNumberOfMatchesTeamB.Size = new System.Drawing.Size(35, 17);
            this.labelNumberOfMatchesTeamB.TabIndex = 16;
            this.labelNumberOfMatchesTeamB.Text = "###";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.label2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(806, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Number of Matches: ";
            // 
            // textBoxTeamB
            // 
            this.textBoxTeamB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.textBoxTeamB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxTeamB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBoxTeamB.Location = new System.Drawing.Point(205, 51);
            this.textBoxTeamB.Name = "textBoxTeamB";
            this.textBoxTeamB.Size = new System.Drawing.Size(574, 24);
            this.textBoxTeamB.TabIndex = 3;
            this.textBoxTeamB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTeamB_KeyDown);
            this.textBoxTeamB.Leave += new System.EventHandler(this.textBoxTeamB_Leave);
            // 
            // labelNumberOfMatchesTeamA
            // 
            this.labelNumberOfMatchesTeamA.AutoSize = true;
            this.labelNumberOfMatchesTeamA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelNumberOfMatchesTeamA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelNumberOfMatchesTeamA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelNumberOfMatchesTeamA.Location = new System.Drawing.Point(940, 19);
            this.labelNumberOfMatchesTeamA.Name = "labelNumberOfMatchesTeamA";
            this.labelNumberOfMatchesTeamA.Size = new System.Drawing.Size(35, 17);
            this.labelNumberOfMatchesTeamA.TabIndex = 1;
            this.labelNumberOfMatchesTeamA.Text = "###";
            // 
            // labelTitleNumOfMatches
            // 
            this.labelTitleNumOfMatches.AutoSize = true;
            this.labelTitleNumOfMatches.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.labelTitleNumOfMatches.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelTitleNumOfMatches.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelTitleNumOfMatches.Location = new System.Drawing.Point(806, 19);
            this.labelTitleNumOfMatches.Name = "labelTitleNumOfMatches";
            this.labelTitleNumOfMatches.Size = new System.Drawing.Size(137, 17);
            this.labelTitleNumOfMatches.TabIndex = 0;
            this.labelTitleNumOfMatches.Text = "Number of Matches: ";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splitter1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.splitter1.Location = new System.Drawing.Point(3, 64);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1464, 88);
            this.splitter1.TabIndex = 15;
            this.splitter1.TabStop = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageTeamA);
            this.tabControl.Controls.Add(this.tabPageTeamB);
            this.tabControl.Controls.Add(this.tabPageH2H);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tabControl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tabControl.Location = new System.Drawing.Point(3, 152);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1464, 459);
            this.tabControl.TabIndex = 16;
            // 
            // tabPageTeamA
            // 
            this.tabPageTeamA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageTeamA.Controls.Add(this.dataGridViewTeamA);
            this.tabPageTeamA.Location = new System.Drawing.Point(4, 26);
            this.tabPageTeamA.Name = "tabPageTeamA";
            this.tabPageTeamA.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTeamA.Size = new System.Drawing.Size(1456, 429);
            this.tabPageTeamA.TabIndex = 0;
            this.tabPageTeamA.Text = "Team A";
            // 
            // dataGridViewTeamA
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTeamA.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTeamA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTeamA.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTeamA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTeamA.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dataGridViewTeamA.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTeamA.Name = "dataGridViewTeamA";
            this.dataGridViewTeamA.Size = new System.Drawing.Size(1450, 423);
            this.dataGridViewTeamA.TabIndex = 0;
            this.dataGridViewTeamA.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewTeamA_CellFormatting);
            this.dataGridViewTeamA.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTeamA_CellMouseEnter);
            // 
            // tabPageTeamB
            // 
            this.tabPageTeamB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageTeamB.Controls.Add(this.dataGridViewTeamB);
            this.tabPageTeamB.Location = new System.Drawing.Point(4, 26);
            this.tabPageTeamB.Name = "tabPageTeamB";
            this.tabPageTeamB.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTeamB.Size = new System.Drawing.Size(1456, 429);
            this.tabPageTeamB.TabIndex = 1;
            this.tabPageTeamB.Text = "Team B";
            // 
            // dataGridViewTeamB
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTeamB.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTeamB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTeamB.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTeamB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTeamB.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dataGridViewTeamB.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTeamB.Name = "dataGridViewTeamB";
            this.dataGridViewTeamB.Size = new System.Drawing.Size(1450, 423);
            this.dataGridViewTeamB.TabIndex = 0;
            this.dataGridViewTeamB.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewTeamB_CellFormatting);
            this.dataGridViewTeamB.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTeamB_CellMouseEnter);
            // 
            // tabPageH2H
            // 
            this.tabPageH2H.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tabPageH2H.Controls.Add(this.dataGridViewH2H);
            this.tabPageH2H.Location = new System.Drawing.Point(4, 26);
            this.tabPageH2H.Name = "tabPageH2H";
            this.tabPageH2H.Size = new System.Drawing.Size(1456, 429);
            this.tabPageH2H.TabIndex = 2;
            this.tabPageH2H.Text = "H2H";
            // 
            // dataGridViewH2H
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewH2H.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewH2H.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewH2H.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dataGridViewH2H.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewH2H.Name = "dataGridViewH2H";
            this.dataGridViewH2H.Size = new System.Drawing.Size(1456, 429);
            this.dataGridViewH2H.TabIndex = 0;
            this.dataGridViewH2H.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewH2H_CellFormatting);
            this.dataGridViewH2H.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewH2H_CellMouseEnter);
            // 
            // TeamABAnalysisDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1470, 614);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Name = "TeamABAnalysisDashboard";
            this.Text = "TeamAnalysisDashboard";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageTeamA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamA)).EndInit();
            this.tabPageTeamB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamB)).EndInit();
            this.tabPageH2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewH2H)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxTeamA;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.ComboBox comboBoxSportType;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label labelTitleNumOfMatches;
        private System.Windows.Forms.Label labelNumberOfMatchesTeamA;
        private System.Windows.Forms.Label labelTeamB;
        private System.Windows.Forms.Label labelTeamA;
        private System.Windows.Forms.Label labelNumberOfMatchesTeamB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTeamB;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageTeamA;
        private System.Windows.Forms.TabPage tabPageTeamB;
        private System.Windows.Forms.TabPage tabPageH2H;
        private System.Windows.Forms.DataGridView dataGridViewTeamA;
        private System.Windows.Forms.DataGridView dataGridViewTeamB;
        private System.Windows.Forms.DataGridView dataGridViewH2H;
        private System.Windows.Forms.Label labelH2HValue;
        private System.Windows.Forms.Label labelH2H;
        private System.Windows.Forms.ComboBox comboBoxTeamAFilter;
        private System.Windows.Forms.ComboBox comboBoxTeamBFilter;
        private System.Windows.Forms.CheckBox checkBoxShowIndexes;
        private System.Windows.Forms.Button buttonStatistics;
    }
}