﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class TeamsStatistics : MaterialForm
    {
        private BgSport Sport;
        private int TeamAId;
        private int TeamBId;
        private string TeamAName;
        private string TeamBName;
        private BgTeamsAnalysisRepository BgTeamsAnalysisRepository;
        private BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;
        private Dictionary<BgBetType, Tuple<DataGridView, DataGridView, DataGridView>> tabHelper = new Dictionary<BgBetType, Tuple<DataGridView, DataGridView, DataGridView>>();
        public TeamsStatistics()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            DynamicInit(BgBetType.FullTime);
            DynamicInit(BgBetType.OverUnder3_5);
            DynamicInit(BgBetType.OverUnder2_5);
            DynamicInit(BgBetType.OverUnder1_5);
            DynamicInit(BgBetType.Range);
            DynamicInit(BgBetType.BothTeamScore);
            DynamicInit(BgBetType.DrawWin);
            DynamicInit(BgBetType.NoDraw);
            DynamicInit(BgBetType.DrawLose);
        }

        private void DynamicInit(BgBetType betType)
        {
            var tabPageBetType = new System.Windows.Forms.TabPage();
            var tabControlBetType = new System.Windows.Forms.TabControl();
            var tabPageBetTypeTeamA = new System.Windows.Forms.TabPage();
            var tabPageBetTypeTeamB = new System.Windows.Forms.TabPage();
            var tabPageBetTypeH2H = new System.Windows.Forms.TabPage();
            var dataGridViewTeamA = new System.Windows.Forms.DataGridView();
            var dataGridViewTeamB = new System.Windows.Forms.DataGridView();
            var dataGridViewH2H = new System.Windows.Forms.DataGridView();

            tabPageBetType.Controls.Add(tabControlBetType);
            tabPageBetType.Location = new System.Drawing.Point(4, 22);
            tabPageBetType.Name = $"tabPage{betType}";
            tabPageBetType.Padding = new System.Windows.Forms.Padding(3);
            tabPageBetType.Size = new System.Drawing.Size(942, 468);
            tabPageBetType.TabIndex = 0;
            tabPageBetType.Text = betType.ToString();//use decriptor
            tabPageBetType.UseVisualStyleBackColor = true;
            // 
            // tabControlFullTime
            // 
            tabControlBetType.Controls.Add(tabPageBetTypeTeamA);
            tabControlBetType.Controls.Add(tabPageBetTypeTeamB);
            tabControlBetType.Controls.Add(tabPageBetTypeH2H);
            tabControlBetType.Dock = System.Windows.Forms.DockStyle.Fill;
            tabControlBetType.Location = new System.Drawing.Point(3, 3);
            tabControlBetType.Name = $"tabControl{betType}";
            tabControlBetType.SelectedIndex = 0;
            tabControlBetType.Size = new System.Drawing.Size(936, 462);
            tabControlBetType.TabIndex = 0;
            // 
            // tabPageFullTimeTeamA
            // 
            tabPageBetTypeTeamA.Controls.Add(dataGridViewTeamA);
            tabPageBetTypeTeamA.Location = new System.Drawing.Point(4, 22);
            tabPageBetTypeTeamA.Name = $"tabPage{betType}TeamA";
            tabPageBetTypeTeamA.Padding = new System.Windows.Forms.Padding(3);
            tabPageBetTypeTeamA.Size = new System.Drawing.Size(928, 436);
            tabPageBetTypeTeamA.TabIndex = 0;
            tabPageBetTypeTeamA.Text = "Team A";
            tabPageBetTypeTeamA.UseVisualStyleBackColor = true;
            // 
            // tabPageFullTimeTeamB
            // 
            tabPageBetTypeTeamB.Controls.Add(dataGridViewTeamB);
            tabPageBetTypeTeamB.Location = new System.Drawing.Point(4, 22);
            tabPageBetTypeTeamB.Name = $"tabPage{betType}TeamB";
            tabPageBetTypeTeamB.Padding = new System.Windows.Forms.Padding(3);
            tabPageBetTypeTeamB.Size = new System.Drawing.Size(928, 436);
            tabPageBetTypeTeamB.TabIndex = 1;
            tabPageBetTypeTeamB.Text = "Team B";
            tabPageBetTypeTeamB.UseVisualStyleBackColor = true;
            // 
            // tabPageFullTimeH2H
            // 
            tabPageBetTypeH2H.Controls.Add(dataGridViewH2H);
            tabPageBetTypeH2H.Location = new System.Drawing.Point(4, 22);
            tabPageBetTypeH2H.Name = $"tabPage{betType}H2H";
            tabPageBetTypeH2H.Padding = new System.Windows.Forms.Padding(3);
            tabPageBetTypeH2H.Size = new System.Drawing.Size(928, 436);
            tabPageBetTypeH2H.TabIndex = 2;
            tabPageBetTypeH2H.Text = "H2H";
            tabPageBetTypeH2H.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTeamA
            // 
            dataGridViewTeamA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewTeamA.Dock = System.Windows.Forms.DockStyle.Fill;
            dataGridViewTeamA.Location = new System.Drawing.Point(3, 3);
            dataGridViewTeamA.Name = $"dataGridView{betType}TeamA";
            dataGridViewTeamA.Size = new System.Drawing.Size(922, 430);
            dataGridViewTeamA.TabIndex = 0;
            dataGridViewTeamA.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // dataGridViewTeamB
            // 
            dataGridViewTeamB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewTeamB.Dock = System.Windows.Forms.DockStyle.Fill;
            dataGridViewTeamB.Location = new System.Drawing.Point(3, 3);
            dataGridViewTeamB.Name = $"dataGridView{betType}TeamB";
            dataGridViewTeamB.Size = new System.Drawing.Size(922, 430);
            dataGridViewTeamB.TabIndex = 0;
            dataGridViewTeamB.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            // 
            // dataGridViewH2H
            // 
            dataGridViewH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            dataGridViewH2H.Location = new System.Drawing.Point(3, 3);
            dataGridViewH2H.Name = $"dataGridView{betType}H2H";
            dataGridViewH2H.Size = new System.Drawing.Size(922, 430);
            dataGridViewH2H.TabIndex = 0;
            dataGridViewH2H.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);

            this.tabControl.Controls.Add(tabPageBetType);
            tabHelper.Add(betType, new Tuple<DataGridView, DataGridView, DataGridView>(dataGridViewTeamA, dataGridViewTeamB, dataGridViewH2H));
        }

        public TeamsStatistics(BgSport sport, int teamAId, int teamBId, BgTeamsAnalysisRepository bgTeamsAnalysisRepository) : this()
        {
            var mapper = MonitorUtils.Mapper.GetBgMapper();
            this.Sport = sport;
            this.TeamAId = teamAId;
            this.TeamBId = teamBId;
            this.TeamAName = BgDataServices.Instance.ExtractBgNameFromBgId(sport, BgMapperType.Teams, this.TeamAId, mapper);
            this.TeamBName = BgDataServices.Instance.ExtractBgNameFromBgId(sport, BgMapperType.Teams, this.TeamBId, mapper);
            this.BgTeamsAnalysisRepository = bgTeamsAnalysisRepository;
            this.Text = $"Statistics: {this.TeamAName} Vs {this.TeamBName}";
        }

        private void buttonGo_Click(object sender, System.EventArgs e)
        {
            var maxRows = (int)this.numericUpDownMaxRows.Value;
            var repo = analysisServices.PrepareTeamsStatisitcsRepository(this.BgTeamsAnalysisRepository, maxRows);
            var teamAFullTime = repo.TeamA[BgBetType.FullTime].Select(s => new MonitorFullTimeStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamFullTimeStatistics)s.Value)).ToList();
            RefreshTabData<MonitorFullTimeStatistics>(tabHelper[BgBetType.FullTime].Item1, teamAFullTime);
            var teamBFullTime = repo.TeamB[BgBetType.FullTime].Select(s => new MonitorFullTimeStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamFullTimeStatistics)s.Value)).ToList();
            RefreshTabData<MonitorFullTimeStatistics>(tabHelper[BgBetType.FullTime].Item2, teamBFullTime);
            var h2hFullTime = repo.H2H[BgBetType.FullTime].Select(s => new MonitorFullTimeStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamFullTimeStatistics)s.Value)).ToList();
            RefreshTabData<MonitorFullTimeStatistics>(tabHelper[BgBetType.FullTime].Item3, h2hFullTime);

            SetOverUnderTabsData(repo, BgBetType.OverUnder3_5);
            SetOverUnderTabsData(repo, BgBetType.OverUnder2_5);
            SetOverUnderTabsData(repo, BgBetType.OverUnder1_5);

            var teamARange = repo.TeamA[BgBetType.Range].Select(s => new MonitorRangeStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamRangeStatistics)s.Value)).ToList();
            RefreshTabData<MonitorRangeStatistics>(tabHelper[BgBetType.Range].Item1, teamARange);
            var teamBRange = repo.TeamB[BgBetType.Range].Select(s => new MonitorRangeStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamRangeStatistics)s.Value)).ToList();
            RefreshTabData<MonitorRangeStatistics>(tabHelper[BgBetType.Range].Item2, teamBRange);
            var h2hRange = repo.H2H[BgBetType.Range].Select(s => new MonitorRangeStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamRangeStatistics)s.Value)).ToList();
            RefreshTabData<MonitorRangeStatistics>(tabHelper[BgBetType.Range].Item3, h2hRange);

            SetYesNoTabsData(repo, BgBetType.BothTeamScore);
            SetYesNoTabsData(repo, BgBetType.DrawWin);
            SetYesNoTabsData(repo, BgBetType.NoDraw);
            SetYesNoTabsData(repo, BgBetType.DrawLose);
        }

        private void SetOverUnderTabsData(BgTeamsStatisticsRepository repo, BgBetType bgBetType)
        {
            var teamAOU15 = repo.TeamA[bgBetType].Select(s => new MonitorOverUnderStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamOverUnderStatistics)s.Value)).ToList();
            RefreshTabData<MonitorOverUnderStatistics>(tabHelper[bgBetType].Item1, teamAOU15);
            var teamBOU15 = repo.TeamB[bgBetType].Select(s => new MonitorOverUnderStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamOverUnderStatistics)s.Value)).ToList();
            RefreshTabData<MonitorOverUnderStatistics>(tabHelper[bgBetType].Item2, teamBOU15);
            var h2hOU15 = repo.H2H[bgBetType].Select(s => new MonitorOverUnderStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamOverUnderStatistics)s.Value)).ToList();
            RefreshTabData<MonitorOverUnderStatistics>(tabHelper[bgBetType].Item3, h2hOU15);
        }
        private void SetYesNoTabsData(BgTeamsStatisticsRepository repo, BgBetType bgBetType)
        {
            var teamA = repo.TeamA[bgBetType].Select(s => new MonitorYesNoStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamYesNoStatistics)s.Value)).ToList();
            RefreshTabData<MonitorYesNoStatistics>(tabHelper[bgBetType].Item1, teamA);
            var teamB = repo.TeamB[bgBetType].Select(s => new MonitorYesNoStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamYesNoStatistics)s.Value)).ToList();
            RefreshTabData<MonitorYesNoStatistics>(tabHelper[bgBetType].Item2, teamB);
            var h2h = repo.H2H[bgBetType].Select(s => new MonitorYesNoStatistics(MonitorUtils.BgTeamsFilterConvertor(s.Key), (BgTeamYesNoStatistics)s.Value)).ToList();
            RefreshTabData<MonitorYesNoStatistics>(tabHelper[bgBetType].Item3, h2h);
        }

        private void RefreshTabData<T>(DataGridView dgv, List<T> collection)
        {
            dgv.Rows.Clear();
            dgv.Columns.Clear();
            dgv.Refresh();
            MonitorUtils.BuildGrid<T>(collection, dgv, this.Sport);
        }
        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dgv = (DataGridView)sender;
            if (dgv.Name == "dataGridViewFullTimeTeamA" || dgv.Name == "dataGridViewFullTimeTeamB" || dgv.Name == "dataGridViewFullTimeH2H")
            {
                if (e.ColumnIndex == 9) //PowerNumber
                {
                    var powerNumberCell = dgv.Rows[e.RowIndex].Cells[9];
                    if (powerNumberCell.Value != null && decimal.TryParse(powerNumberCell.Value.ToString(), out var value))
                    {
                        if (value > 0)
                            powerNumberCell.Style.ForeColor = Color.Green;
                        else if (value < 0)
                            powerNumberCell.Style.ForeColor = Color.Red;

                    }
                }
            }
        }
    }
}

