﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    public class BaseABParams
    {
        public BgSport Sport { get; set; }
        public int? TeamAId{ get; set; }
        public int? TeamBId { get; set; }
    }
}
