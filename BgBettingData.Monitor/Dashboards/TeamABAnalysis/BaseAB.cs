﻿using MaterialSkin;
using MaterialSkin.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public class BaseAB : MaterialForm
    {
        protected BaseABParams abParams;
        protected MapperRespository mapper;
        protected BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;
        public enum Mode { ChooseTeams, UseTeam };
        public Mode DialogMode { get; protected set; }

        public BaseAB()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.abParams = new BaseABParams();
        }
        public void Init()
        {
            this.mapper = MonitorUtils.Mapper.GetBgMapper();
        }
        protected void validateTeamATextBox(object sender, TextBox textBoxTeamB)
        {
            var teamAId = MonitorUtils.SelectBgItem(((TextBox)sender).Text, BgMapperType.Teams);
            if ((this.abParams.TeamAId.HasValue && teamAId.HasValue
                && this.abParams.TeamAId.Value != teamAId)
                || !this.abParams.TeamAId.HasValue)
            {
                if (teamAId.HasValue)
                    this.abParams.TeamAId = teamAId.Value;
                var teamA = mapper.BgMapperMonitors.FirstOrDefault(i => i.BgId == this.abParams.TeamAId);
                MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.abParams.Sport)
                    .Except(new List<BgMapperMonitor>() { teamA }, new BgMapperMonitorComparer()), textBoxTeamB);
            }
            else if (!teamAId.HasValue)
                MessageBox.Show($"TeamA not selected", $"Invalid team selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        protected void validateTeamBTextBox(object sender, TextBox textBoxTeamA)
        {
            var teamBId = MonitorUtils.SelectBgItem(((TextBox)sender).Text, BgMapperType.Teams);
            if ((this.abParams.TeamBId.HasValue && teamBId.HasValue
                && this.abParams.TeamBId.Value != teamBId)
                || !this.abParams.TeamBId.HasValue)
            {
                if (this.abParams.TeamBId.HasValue)
                    this.abParams.TeamBId = teamBId;
                else
                    this.abParams.TeamBId = null;
                var teamB = mapper.BgMapperMonitors.FirstOrDefault(i => i.BgId == this.abParams.TeamBId);
                MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                    .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.abParams.Sport)
                    .Except(new List<BgMapperMonitor>() { teamB }, new BgMapperMonitorComparer()), textBoxTeamA);
            }
        }


    }
}
