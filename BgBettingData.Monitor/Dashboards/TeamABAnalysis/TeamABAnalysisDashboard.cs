﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class TeamABAnalysisDashboard : BaseAB
    {
        private ShowTeamABAnalysisParams showParams;
        private DataGridViewCellEventArgs mouseLocation;
        private DataGridView activeDataGridView;
        private ToolStripMenuItem contextMenuCloneToManual = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuUpdateMatch = new ToolStripMenuItem();
        private ToolStripMenuItem contextBlackList = new ToolStripMenuItem();
        private BgTeamsAnalysisRepository bgTeamsAnalysisRepository = null;

        public TeamABAnalysisDashboard(Mode mode = Mode.ChooseTeams) : base()
        {
            InitializeComponent();
            this.DialogMode = mode;
            this.showParams = new ShowTeamABAnalysisParams();
            this.Text = "Teams Analysis";
            this.labelNumberOfMatchesTeamA.Text = string.Empty;
            this.labelNumberOfMatchesTeamB.Text = string.Empty;
            this.labelH2HValue.Text = String.Empty;
            this.checkBoxShowIndexes.Visible = false;
            this.showParams.ShowIndexes = false;
            this.buttonStatistics.Enabled = false;
            this.showParams.TeamAFilter = BgTeamsFilter.All_All;
            this.showParams.TeamBFilter = BgTeamsFilter.All_All;
            this.Init();
            if (this.DialogMode == Mode.ChooseTeams)
            {
                comboBoxSportType_SelectedIndexChanged(this.comboBoxSportType, null);
            }
            else
                this.buttonStatistics.Visible = false;
        }

        public TeamABAnalysisDashboard(int teamId, string teamName, BgSport sport) : this(Mode.UseTeam)
        {
            this.abParams.TeamAId = teamId;
            this.abParams.Sport = sport;
            this.comboBoxSportType.Hide();
            this.textBoxTeamB.Hide();
            this.textBoxTeamA.Text = teamName;
            this.textBoxTeamA.Enabled = false;
            this.tabControl.TabPages.RemoveAt(1);
            this.tabControl.TabPages.RemoveAt(1);
            this.labelTeamB.Hide();
            this.labelH2H.Hide();
            this.buttonGo.Hide();
            this.comboBoxTeamAFilter.Hide();
            this.comboBoxTeamBFilter.Hide();
            this.Text = $"{sport} Team {teamName} Analysis";
            this.splitter1.Size = new System.Drawing.Size(this.splitter1.Size.Width, 40);
            buttonGo_Click(null, null);
        }
        public TeamABAnalysisDashboard(int teamAId, string teamAName, int teamBId, string teamBName, BgSport sport) : this(Mode.ChooseTeams)
        {
            this.abParams.TeamAId = teamAId;
            this.abParams.TeamBId = teamBId;
            this.abParams.Sport = sport;
            this.comboBoxSportType.Enabled = false;
            this.textBoxTeamA.Text = $"{teamAName} # {teamAId}";
            this.textBoxTeamA.Enabled = false;
            this.textBoxTeamB.Text = $"{teamBName} # {teamBId}";
            this.textBoxTeamB.Enabled = false;
            this.Text = $"{sport} Team {teamAName} Vs {teamBName} Analysis";
        }
        #region Context Menu
        public void AddContextMenu()
        {
            if (this.showParams.ShowIndexes)
                return;
            contextMenuCloneToManual.Text = "Clone To Manual";
            contextMenuCloneToManual.Click -= new EventHandler(contextMenuCloneToManual_Click);
            contextMenuCloneToManual.Click += new EventHandler(contextMenuCloneToManual_Click);
            contextMenuUpdateMatch.Text = "Update Match";
            contextMenuUpdateMatch.Click -= new EventHandler(contextMenuUpdateMatch_Click);
            contextMenuUpdateMatch.Click += new EventHandler(contextMenuUpdateMatch_Click);
            contextBlackList.Text = "Add Country To Black List";
            contextBlackList.Click -= new EventHandler(contextMenuBlackList_Click);
            contextBlackList.Click += new EventHandler(contextMenuBlackList_Click);

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;

            foreach (DataGridViewRow row in activeDataGridView.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (activeDataGridView.Rows.Count > 1)
            {
                mouseLocation = new DataGridViewCellEventArgs(1, 1);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = activeDataGridView.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (MonitorBgMatchTeamAnalysis)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            if (selectedMatch?.Source != BgSourceProvider.Manual)
            {
                selectedRow.ContextMenuStrip.Items.Add(contextMenuCloneToManual);
            }
            else
            {
                selectedRow.ContextMenuStrip.Items.Add(contextMenuUpdateMatch);
            }
            switch (mouseLocation.ColumnIndex)
            {
                case 2:
                    contextBlackList.Text = $"Black List Country [{selectedMatch.Country} ; {selectedMatch.CountryId}]";
                    selectedRow.ContextMenuStrip.Items.Add(contextBlackList);
                    break;
                case 3:
                    contextBlackList.Text = $"Black List League  [{selectedMatch.League} ; {selectedMatch.LeagueId}]";
                    selectedRow.ContextMenuStrip.Items.Add(contextBlackList);
                    break;
            }
        }
        private void contextMenuCloneToManual_Click(object sender, EventArgs e)
        {
            var selectedRow = activeDataGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (MonitorBgMatchTeamAnalysis)selectedRow.Tag;
            var matchToUpsert = new ManualMatch(selectedMatch);
            var manualMatch = ManualProviderServices.Instance.GetMatch(selectedMatch.CountryId.Value, selectedMatch.LeagueId, selectedMatch.HomeId, selectedMatch.AwayId, selectedMatch.GameTime);
            if (manualMatch != null)
                MessageBox.Show($"Match already exists matchId={manualMatch.MatchId} manualLatsUpdate={manualMatch.LastUpdate} countryId {selectedMatch.CountryId}, leagueId {selectedMatch.LeagueId}, homeId {selectedMatch.HomeId}, awayId {selectedMatch.AwayId}, gameTime {selectedMatch.GameDate}", "Manual match already exist !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
                ManualProviderServices.Instance.AddOrUpdateMatch(matchToUpsert);
        }
        private void contextMenuUpdateMatch_Click(object sender, EventArgs e)
        {
            var selectedRow = activeDataGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (MonitorBgMatchTeamAnalysis)selectedRow.Tag;
            var manualMatch = ManualProviderServices.Instance.GetMatch(selectedMatch.CountryId.Value, selectedMatch.LeagueId, selectedMatch.HomeId, selectedMatch.AwayId, selectedMatch.GameTime);
            if (manualMatch != null)
            {
                var f = new ManualMatch(selectedMatch);
                var monitorMatch = new ManualMatchMonitor(f.Sport,
                        f.Status,
                        f.LastUpdate,
                        f.CountryId,
                        f.Country,
                        f.League,
                        f.LeagueId,
                        f.IsCup,
                        f.GameStart,
                        f.Home,
                        f.HomeId,
                        f.Away,
                        f.AwayId,
                        f.Ignore,
                        manualMatch.MatchId.Value,
                        f.OddsAndScores.HomeOdd, f.OddsAndScores.DrawOdd, f.OddsAndScores.AwayOdd,
                        f.OddsAndScores.HomeScore, f.OddsAndScores.AwayScore,
                        f.OddsAndScores.BasketballQuatersScores);
                var dialog = new MatchDetails(monitorMatch);
                MonitorUtils.HandleMatchDetailsDialog(dialog, buttonGo_Click);
            }
            else
                MessageBox.Show($"Corresponding manual match of bgMatchId {selectedMatch.MatchId} was not found in manual repository", $"Invalid match", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        private void contextMenuBlackList_Click(object sender, EventArgs e)
        {
            var selectedRow = activeDataGridView.Rows[mouseLocation.RowIndex];
            var selectedMatch = (MonitorBgMatchTeamAnalysis)selectedRow.Tag;
            switch (mouseLocation.ColumnIndex)
            {
                case 2:
                    var confirm = MessageBox.Show($"Are you sure you want to black list country {selectedMatch.Country} for teams {selectedMatch.Home}/{selectedMatch.Away}?",
                        "Black list confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (confirm == DialogResult.Yes)
                    {
                        BgDataBlackFilterServices.Instance.AddFilter(
                            new BgTeamBlackFilter(selectedMatch.Sport,
                            selectedMatch.HomeId,
                            selectedMatch.Home,
                            BgMapperType.Countries,
                            selectedMatch.CountryId.Value,
                            selectedMatch.Country));
                        BgDataBlackFilterServices.Instance.AddFilter(
                            new BgTeamBlackFilter(selectedMatch.Sport,
                            selectedMatch.AwayId,
                            selectedMatch.Away,
                            BgMapperType.Countries,
                            selectedMatch.CountryId.Value,
                            selectedMatch.Country));
                    }
                    break;
                case 3:
                    confirm = MessageBox.Show($"Are you sure you want to black list league {selectedMatch.League} for teams {selectedMatch.Home}/{selectedMatch.Away}?",
                        "Black list confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (confirm == DialogResult.Yes)
                    {
                        BgDataBlackFilterServices.Instance.AddFilter(
                        new BgTeamBlackFilter(selectedMatch.Sport,
                        selectedMatch.HomeId,
                        selectedMatch.Home,
                        BgMapperType.Leagues,
                        selectedMatch.LeagueId,
                        selectedMatch.League));
                        BgDataBlackFilterServices.Instance.AddFilter(
                            new BgTeamBlackFilter(selectedMatch.Sport,
                            selectedMatch.AwayId,
                            selectedMatch.Away,
                            BgMapperType.Leagues,
                            selectedMatch.LeagueId,
                            selectedMatch.League));
                    }
                    break;
            }
        }
        #endregion

        #region Button Go
        private void buttonGo_Click(object sender, EventArgs e)
        {
            bgTeamsAnalysisRepository = analysisServices.PrepareTeamsRepository(this.abParams.TeamAId, this.abParams.TeamBId);
            if (this.abParams.TeamAId.HasValue)
            {
                comboBoxTeamAFilter.SelectedIndex = 0;
                comboBoxTeamAFilter_SelectedIndexChanged(comboBoxTeamAFilter, null);
                this.checkBoxShowIndexes.Visible = true;
            }
            else if (this.DialogMode == Mode.ChooseTeams)
            {
                MessageBox.Show($"It seems that team A was not selected", $"Invalid team selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (this.DialogMode == Mode.ChooseTeams)
            {
                if (this.abParams.TeamBId.HasValue)
                {
                    comboBoxTeamBFilter.SelectedIndex = 0;
                    comboBoxTeamBFilter_SelectedIndexChanged(comboBoxTeamBFilter, null);
                    this.buttonStatistics.Enabled = true;
                }
                else
                    MessageBox.Show($"It seems txhat team B was not selected", $"Invalid team selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                if (bgTeamsAnalysisRepository.H2hMatches?.Count > 0)
                    ShowGridDataForReview(this.dataGridViewH2H, this.labelH2HValue, bgTeamsAnalysisRepository.H2hMatches, bgTeamsAnalysisRepository.H2hFilteredWithIndexes[this.showParams.TeamAFilter], this.abParams.TeamAId);
                else
                {
                    this.labelH2HValue.Text = String.Empty;
                    this.dataGridViewH2H.Rows.Clear();
                    this.dataGridViewH2H.Columns.Clear();
                    this.dataGridViewH2H.Refresh();
                }
            }
        }

        #endregion

        #region ShowGridDataForReview
        private void ShowGridDataForReview(DataGridView dataGridViewTeam, Label labelNumberOfMatchesTeam,
            List<BgMatchForAnalysis> matches,
            List<BgMatchWithIndexes> bgMatchWithIndexes, int? selectedTeamId)
        {
            dataGridViewTeam.Rows.Clear();
            dataGridViewTeam.Columns.Clear();
            dataGridViewTeam.Refresh();

            if (!this.showParams.ShowIndexes)
            {
                labelNumberOfMatchesTeam.Text = matches.Count.ToString();
                var incompleteMatces = matches
                    .ConvertAll<MonitorBgMatchTeamAnalysis>(f => new MonitorBgMatchTeamAnalysis(f));
                MonitorUtils.BuildGrid<MonitorBgMatchTeamAnalysis>(incompleteMatces, dataGridViewTeam, this.abParams.Sport);
            }
            else
            {
                labelNumberOfMatchesTeam.Text = bgMatchWithIndexes.Count.ToString();
                var incompleteMatces = bgMatchWithIndexes
                    .ConvertAll<MonitorBgMatchWithIndexes>(f => new MonitorBgMatchWithIndexes(f));
                MonitorUtils.BuildGrid<MonitorBgMatchWithIndexes>(incompleteMatces, dataGridViewTeam, this.abParams.Sport);
            }
            activeDataGridView = dataGridViewTeam;
            AddContextMenu();
        }
        #endregion

        #region comboBoxSportType
        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.abParams.Sport = sport;
                    this.textBoxTeamA.Text = String.Empty;
                    this.textBoxTeamB.Text = String.Empty;
                    this.checkBoxShowIndexes.Checked = false;
                    this.checkBoxShowIndexes.Visible = false;
                    this.showParams.ShowIndexes = false;
                    this.buttonStatistics.Enabled = false;
                    MonitorUtils.SetAutoComplete(mapper.BgMapperMonitors
                        .Where(m => m.BgType == BgMapperType.Teams && m.BgSport == this.abParams.Sport), this.textBoxTeamA);
                }
            }
        }
        #endregion

        #region Auto Complete Events
        private void textBoxTeamA_Leave(object sender, EventArgs e)
        {
            validateTeamATextBox(sender, this.textBoxTeamB);
        }
        private void textBoxTeamA_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                validateTeamATextBox(sender, this.textBoxTeamB);
            }
        }
        private void textBoxTeamB_Leave(object sender, EventArgs e)
        {
            validateTeamBTextBox(sender, this.textBoxTeamA);
        }
        private void textBoxTeamB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.abParams.TeamBId = MonitorUtils.SelectBgItem(((TextBox)sender).Text, BgMapperType.Teams);
            }
        }
        #endregion

        #region Mouse Location
        private void dataGridViewTeamA_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
            activeDataGridView = this.dataGridViewTeamA;
        }
        private void dataGridViewTeamB_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
            activeDataGridView = this.dataGridViewTeamB;
        }
        private void dataGridViewH2H_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
            activeDataGridView = this.dataGridViewH2H;
        }
        #endregion

        #region ApplyFilter
        private void comboBoxTeamAFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgTeamsFilter>(comboBox.Text.Replace('-', '_'), out var filter))
                {
                    if (bgTeamsAnalysisRepository.TeamAmatchesAllAll == null)
                    {
                        MessageBox.Show($"Please select TeamA populations", $"No team selection is made", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    this.showParams.TeamAFilter = filter;
                    ShowGridDataForReview(this.dataGridViewTeamA, this.labelNumberOfMatchesTeamA, bgTeamsAnalysisRepository.TeamAmatchesFiltered[filter], bgTeamsAnalysisRepository.TeamAmatchesFilteredWithIndexes[filter], this.abParams.TeamAId);

                    if (bgTeamsAnalysisRepository.H2hMatches?.Count > 0)
                        ShowGridDataForReview(this.dataGridViewH2H, this.labelH2HValue, bgTeamsAnalysisRepository.H2hFiltered[filter], bgTeamsAnalysisRepository.H2hFilteredWithIndexes[filter], this.abParams.TeamAId);
                    else
                    {
                        this.labelH2HValue.Text = String.Empty;
                        this.dataGridViewH2H.Rows.Clear();
                        this.dataGridViewH2H.Columns.Clear();
                        this.dataGridViewH2H.Refresh();
                    }
                }
            }
        }

        private void comboBoxTeamBFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgTeamsFilter>(comboBox.Text.Replace('-', '_'), out var filter))
                {
                    if (bgTeamsAnalysisRepository.TeamBmatchesAllAll == null)
                    {
                        MessageBox.Show($"Please select TeamB populations", $"No team selection is made", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    this.showParams.TeamBFilter = filter;
                    ShowGridDataForReview(this.dataGridViewTeamB, this.labelNumberOfMatchesTeamB, bgTeamsAnalysisRepository.TeamBmatchesFiltered[filter], bgTeamsAnalysisRepository.TeamBmatchesFilteredWithIndexes[filter], this.abParams.TeamBId);
                }
            }
        }
        #endregion

        #region ShowIndexes
        private void checkBoxShowIndexes_CheckedChanged(object sender, EventArgs e)
        {
            this.showParams.ShowIndexes = ((CheckBox)sender).Checked;
            comboBoxTeamAFilter_SelectedIndexChanged(this.comboBoxTeamAFilter, null);
            if (this.DialogMode == Mode.ChooseTeams)
            {
                if (this.abParams.TeamBId.HasValue)
                {
                    comboBoxTeamBFilter_SelectedIndexChanged(comboBoxTeamBFilter, null);
                }
            }
        }
        #endregion

        #region CellFormating
        private void dataGridViewTeamA_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dgv = (DataGridView)sender;
            if (this.showParams.ShowIndexes && dgv.Rows[e.RowIndex].Tag is MonitorBgMatchWithIndexes)
                MonitorUtils.MonitorBgMatchWithIndexesCellConditionalFormating(dgv, e);
        }
        private void dataGridViewTeamB_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dgv = (DataGridView)sender;
            if (this.showParams.ShowIndexes && dgv.Rows[e.RowIndex].Tag is MonitorBgMatchWithIndexes)
                MonitorUtils.MonitorBgMatchWithIndexesCellConditionalFormating(dgv, e);
        }

        private void dataGridViewH2H_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dgv = (DataGridView)sender;
            if (this.showParams.ShowIndexes && dgv.Rows[e.RowIndex].Tag is MonitorBgMatchWithIndexes)
                MonitorUtils.MonitorBgMatchWithIndexesCellConditionalFormating(dgv, e); 
        }
        //private void CellConditionalFormating(DataGridView dgv, DataGridViewCellFormattingEventArgs e)
        //{
        //    var match = (MonitorBgMatchWithIndexes)dgv.Rows[e.RowIndex].Tag;
        //    var wdlCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex];
        //    if (dgv.Columns[e.ColumnIndex].Name == "WDL")
        //    {
        //        var homeOddCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex - 5];
        //        var drawOddCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex - 4];
        //        var awayOddCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex - 3];
        //        var oddPowerCell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex + 1];
        //        if (match.OddPower > 0)
        //            oddPowerCell.Style.ForeColor = Color.Green;
        //        else if (match.OddPower < 0)
        //            oddPowerCell.Style.ForeColor = Color.Red;
        //        var specialDrawStatus = false;
        //        switch (match.Sport)
        //        {
        //            case BgSport.Soccer:
        //                specialDrawStatus = analysisServices.SoccerSpecialDrawStatuses.Contains(match.Status.ToLower());// match.Status.ToLower() == "pen." || match.Status.ToLower() == "after over" || match.Status.ToLower() == "aet";
        //                break;
        //            case BgSport.Basketball:
        //                specialDrawStatus = false;
        //                break;
        //        }

        //        switch (wdlCell.Value.ToString())
        //        {
        //            case "W":
        //                wdlCell.Style.BackColor = Color.LightGreen;
        //                break;
        //            case "D":
        //                if (specialDrawStatus)
        //                    wdlCell.Style.BackColor = Color.Orange;
        //                else
        //                    wdlCell.Style.BackColor = Color.Yellow;
        //                break;
        //            case "L":
        //                wdlCell.Style.BackColor = Color.LightCoral;
        //                break;
        //        }

        //        if (!specialDrawStatus)
        //        {
        //            if (match.HomeScore > match.AwayScore)
        //            {
        //                homeOddCell.Style.BackColor = Color.DimGray;
        //            }
        //            else if (match.HomeScore < match.AwayScore)
        //            {
        //                awayOddCell.Style.BackColor = Color.DimGray;
        //            }
        //            else
        //                drawOddCell.Style.BackColor = Color.DimGray;
        //        }
        //        else
        //            drawOddCell.Style.BackColor = Color.Orange;
        //    }
        //}
        #endregion

        private void buttonStatistics_Click(object sender, EventArgs e)
        {
            if (!this.abParams.TeamAId.HasValue || !this.abParams.TeamBId.HasValue)
            {
                MessageBox.Show($"Please select TeamA + TeamB populations", $"Teams selection is invalid", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var statistics = new TeamsStatistics(this.abParams.Sport, this.abParams.TeamAId.Value, this.abParams.TeamBId.Value,
                bgTeamsAnalysisRepository);
            statistics.ShowDialog();
        }
    }
}
