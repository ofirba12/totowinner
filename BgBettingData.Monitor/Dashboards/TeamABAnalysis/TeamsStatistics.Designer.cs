﻿namespace BgBettingData.Monitor
{
    partial class TeamsStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDownMaxRows = new System.Windows.Forms.NumericUpDown();
            this.buttonGo = new System.Windows.Forms.Button();
            this.labelMaxRows = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageFullTime = new System.Windows.Forms.TabPage();
            this.tabControlFullTime = new System.Windows.Forms.TabControl();
            this.tabPageFullTimeTeamA = new System.Windows.Forms.TabPage();
            this.dataGridViewTeamA = new System.Windows.Forms.DataGridView();
            this.tabPageFullTimeTeamB = new System.Windows.Forms.TabPage();
            this.dataGridViewTeamB = new System.Windows.Forms.DataGridView();
            this.tabPageFullTimeH2H = new System.Windows.Forms.TabPage();
            this.dataGridViewH2H = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxRows)).BeginInit();
            this.tabPageFullTime.SuspendLayout();
            this.tabControlFullTime.SuspendLayout();
            this.tabPageFullTimeTeamA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamA)).BeginInit();
            this.tabPageFullTimeTeamB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamB)).BeginInit();
            this.tabPageFullTimeH2H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewH2H)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDownMaxRows
            // 
            this.numericUpDownMaxRows.Location = new System.Drawing.Point(784, 34);
            this.numericUpDownMaxRows.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownMaxRows.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxRows.Name = "numericUpDownMaxRows";
            this.numericUpDownMaxRows.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownMaxRows.TabIndex = 0;
            this.numericUpDownMaxRows.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownMaxRows.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(863, 32);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(75, 25);
            this.buttonGo.TabIndex = 1;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // labelMaxRows
            // 
            this.labelMaxRows.AutoSize = true;
            this.labelMaxRows.BackColor = System.Drawing.Color.Transparent;
            this.labelMaxRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelMaxRows.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelMaxRows.Location = new System.Drawing.Point(705, 37);
            this.labelMaxRows.Name = "labelMaxRows";
            this.labelMaxRows.Size = new System.Drawing.Size(65, 13);
            this.labelMaxRows.TabIndex = 2;
            this.labelMaxRows.Text = "Max Rows";
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 64);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(950, 494);
            this.tabControl.TabIndex = 3;
            // 
            // tabPageFullTime
            // 
            this.tabPageFullTime.Controls.Add(this.tabControlFullTime);
            this.tabPageFullTime.Location = new System.Drawing.Point(4, 22);
            this.tabPageFullTime.Name = "tabPageFullTime";
            this.tabPageFullTime.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFullTime.Size = new System.Drawing.Size(942, 468);
            this.tabPageFullTime.TabIndex = 0;
            this.tabPageFullTime.Text = "Full Time";
            this.tabPageFullTime.UseVisualStyleBackColor = true;
            // 
            // tabControlFullTime
            // 
            this.tabControlFullTime.Controls.Add(this.tabPageFullTimeTeamA);
            this.tabControlFullTime.Controls.Add(this.tabPageFullTimeTeamB);
            this.tabControlFullTime.Controls.Add(this.tabPageFullTimeH2H);
            this.tabControlFullTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlFullTime.Location = new System.Drawing.Point(3, 3);
            this.tabControlFullTime.Name = "tabControlFullTime";
            this.tabControlFullTime.SelectedIndex = 0;
            this.tabControlFullTime.Size = new System.Drawing.Size(936, 462);
            this.tabControlFullTime.TabIndex = 0;
            // 
            // tabPageFullTimeTeamA
            // 
            this.tabPageFullTimeTeamA.Controls.Add(this.dataGridViewTeamA);
            this.tabPageFullTimeTeamA.Location = new System.Drawing.Point(4, 22);
            this.tabPageFullTimeTeamA.Name = "tabPageFullTimeTeamA";
            this.tabPageFullTimeTeamA.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFullTimeTeamA.Size = new System.Drawing.Size(928, 436);
            this.tabPageFullTimeTeamA.TabIndex = 0;
            this.tabPageFullTimeTeamA.Text = "Team A";
            this.tabPageFullTimeTeamA.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTeamA
            // 
            this.dataGridViewTeamA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTeamA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTeamA.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTeamA.Name = "dataGridViewTeamA";
            this.dataGridViewTeamA.Size = new System.Drawing.Size(922, 430);
            this.dataGridViewTeamA.TabIndex = 0;
            // 
            // tabPageFullTimeTeamB
            // 
            this.tabPageFullTimeTeamB.Controls.Add(this.dataGridViewTeamB);
            this.tabPageFullTimeTeamB.Location = new System.Drawing.Point(4, 22);
            this.tabPageFullTimeTeamB.Name = "tabPageFullTimeTeamB";
            this.tabPageFullTimeTeamB.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFullTimeTeamB.Size = new System.Drawing.Size(928, 436);
            this.tabPageFullTimeTeamB.TabIndex = 1;
            this.tabPageFullTimeTeamB.Text = "Team B";
            this.tabPageFullTimeTeamB.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTeamB
            // 
            this.dataGridViewTeamB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTeamB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTeamB.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTeamB.Name = "dataGridViewTeamB";
            this.dataGridViewTeamB.Size = new System.Drawing.Size(922, 430);
            this.dataGridViewTeamB.TabIndex = 0;
            // 
            // tabPageFullTimeH2H
            // 
            this.tabPageFullTimeH2H.Controls.Add(this.dataGridViewH2H);
            this.tabPageFullTimeH2H.Location = new System.Drawing.Point(4, 22);
            this.tabPageFullTimeH2H.Name = "tabPageFullTimeH2H";
            this.tabPageFullTimeH2H.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFullTimeH2H.Size = new System.Drawing.Size(928, 436);
            this.tabPageFullTimeH2H.TabIndex = 2;
            this.tabPageFullTimeH2H.Text = "H2H";
            this.tabPageFullTimeH2H.UseVisualStyleBackColor = true;
            // 
            // dataGridViewH2H
            // 
            this.dataGridViewH2H.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewH2H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewH2H.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewH2H.Name = "dataGridViewH2H";
            this.dataGridViewH2H.Size = new System.Drawing.Size(922, 430);
            this.dataGridViewH2H.TabIndex = 0;
            // 
            // TeamsStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 561);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.labelMaxRows);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.numericUpDownMaxRows);
            this.Name = "TeamsStatistics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TeamsStatistics";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxRows)).EndInit();
            this.tabPageFullTime.ResumeLayout(false);
            this.tabControlFullTime.ResumeLayout(false);
            this.tabPageFullTimeTeamA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamA)).EndInit();
            this.tabPageFullTimeTeamB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeamB)).EndInit();
            this.tabPageFullTimeH2H.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewH2H)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDownMaxRows;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Label labelMaxRows;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageFullTime;
        private System.Windows.Forms.TabControl tabControlFullTime;
        private System.Windows.Forms.TabPage tabPageFullTimeTeamA;
        private System.Windows.Forms.TabPage tabPageFullTimeTeamB;
        private System.Windows.Forms.TabPage tabPageFullTimeH2H;
        private System.Windows.Forms.DataGridView dataGridViewTeamA;
        private System.Windows.Forms.DataGridView dataGridViewTeamB;
        private System.Windows.Forms.DataGridView dataGridViewH2H;
    }
}