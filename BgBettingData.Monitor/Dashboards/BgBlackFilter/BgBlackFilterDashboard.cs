﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class BgBlackFilterDashboard : MaterialForm
    {
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextDeleteBlackFilter = new ToolStripMenuItem();

        public BgBlackFilterDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            this.dataGridView.Rows.Clear();
            this.dataGridView.Columns.Clear();
            this.dataGridView.Refresh();
            var monitorData = BgDataBlackFilterServices.Instance.GetAllBgTeamsBlackFilters();
            MonitorUtils.BuildGrid<BgTeamBlackFilter>(monitorData, this.dataGridView, BgSport.Soccer /*fake*/);
            AddContextMenu();
        }

        private void AddContextMenu()
        {
            contextDeleteBlackFilter.Text = "Delete filter";
            contextDeleteBlackFilter.Click -= new EventHandler(contextDeleteFilter_Click);
            contextDeleteBlackFilter.Click += new EventHandler(contextDeleteFilter_Click);
            var rowContextMenu = new ContextMenuStrip();
            foreach (DataGridViewRow row in this.dataGridView.Rows)
            {
                row.ContextMenuStrip = rowContextMenu;
                row.ContextMenuStrip.Items.Add(contextDeleteBlackFilter);
            }
        }
        private void contextDeleteFilter_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridView.Rows[mouseLocation.RowIndex];
            var filter = (BgTeamBlackFilter)selectedRow.Tag;
            var confirm = MessageBox.Show($"Are you sure you want remove filter {filter.FilterBgType}:[{filter.FilterBgName}] for team [{filter.TeamName}]?",
                "Black list delete confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirm == DialogResult.Yes)
            {
                BgDataBlackFilterServices.Instance.DeleteFilter(filter);
                buttonShow_Click(null, null);
            }
        }

        private void dataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
    }
}
