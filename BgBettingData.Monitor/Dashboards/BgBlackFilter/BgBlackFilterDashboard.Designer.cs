﻿namespace BgBettingData.Monitor 
{
    partial class BgBlackFilterDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonShow = new MaterialSkin.Controls.MaterialButton();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonShow
            // 
            this.buttonShow.AutoSize = false;
            this.buttonShow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonShow.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonShow.Depth = 0;
            this.buttonShow.HighEmphasis = true;
            this.buttonShow.Icon = null;
            this.buttonShow.Location = new System.Drawing.Point(718, 25);
            this.buttonShow.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonShow.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonShow.Size = new System.Drawing.Size(75, 36);
            this.buttonShow.TabIndex = 0;
            this.buttonShow.Text = "Show";
            this.buttonShow.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Outlined;
            this.buttonShow.UseAccentColor = false;
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 64);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.dataGridView.Size = new System.Drawing.Size(794, 383);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellMouseEnter);
            // 
            // BgBlackFilterDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.buttonShow);
            this.Name = "BgBlackFilterDashboard";
            this.Text = "Bg Black Filter";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialButton buttonShow;
        private System.Windows.Forms.DataGridView dataGridView;
    }
}