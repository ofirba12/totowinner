﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData;

namespace BgBettingData.Monitor
{
    internal class ShowGoldserveFeedParams
    {
        public DateTime ChosenDate { get; set; }
        public GoalServeSportType Sport { get; set; }
    }
}
