﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;
using TotoWinner.Services;

namespace BgBettingData.Monitor
{
    public partial class GoalserveDashboard : MaterialForm
    {
        private GoalServeProviderServices _gsProviderSrv = GoalServeProviderServices.Instance;
        private ShowGoldserveFeedParams showFeedParams;
        public GoalserveDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.monthCalendarChooseDate.DateSelected += MonthCalendarChooseDate_DateSelected;
            this.showFeedParams = new ShowGoldserveFeedParams();
            this.showFeedParams.ChosenDate = DateTime.Today;
            this.showFeedParams.Sport = GoalServeSportType.Soccer;
        }

        private void MonthCalendarChooseDate_DateSelected(object sender, DateRangeEventArgs e)
        {
            this.showFeedParams.ChosenDate = e.Start.Date;
        }

        private void materialButtonShowFeed_Click(object sender, EventArgs e)
        {
            var days = this.showFeedParams.ChosenDate - DateTime.Today;
            var feed = _gsProviderSrv.GetMonitorFeed(this.showFeedParams.Sport, days.Days);
            dataGridViewFeed.Rows.Clear();
            dataGridViewFeed.Columns.Clear();
            dataGridViewFeed.Refresh();
            BuildGrid(feed);
        }
        private void BuildGrid(List<GoalserveMatchFeedMonitor> feed)
        {
            Type type = typeof(GoalserveMatchFeedMonitor);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            foreach (var column in columns)
            {
                this.dataGridViewFeed.Columns.Add(column.Name, column.Name);
                var style = column.GetCustomAttribute<ColumnStyleAttribute>();
                if (style != null)
                {
                    this.dataGridViewFeed.Columns[column.Name].Width = style.Width;
                    if (this.showFeedParams.Sport != GoalServeSportType.Basketball && style.BasketballScores)
                        this.dataGridViewFeed.Columns[column.Name].Visible = false;
                }
                this.dataGridViewFeed.Columns[column.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            foreach (var match in feed)
            {
                FillGrid(match, type);
            }
        }

        private void FillGrid(GoalserveMatchFeedMonitor match, Type type)
        {
            DataGridViewRow row = (DataGridViewRow)this.dataGridViewFeed.Rows[0].Clone();
            var cellIndex = 0;
            for (var colIndex = 0; colIndex < this.dataGridViewFeed.Columns.Count; colIndex++)
            {
                var methodInfo = type.GetMethod($"get_{this.dataGridViewFeed.Columns[colIndex].Name}");
                var result = methodInfo.Invoke(match, null);
                row.Cells[cellIndex].Value = result;
                cellIndex++;
            }
            this.dataGridViewFeed.Rows.Add(row);
        }

        private void comboBoxSportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;

                if (Enum.TryParse<GoalServeSportType>(comboBox.Text, out var sport))
                    this.showFeedParams.Sport = sport;
            }
        }
    }
}
