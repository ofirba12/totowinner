﻿namespace BgBettingData.Monitor
{
    partial class GoalserveDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxSportType = new System.Windows.Forms.ComboBox();
            this.monthCalendarChooseDate = new System.Windows.Forms.MonthCalendar();
            this.materialButtonShowFeed = new MaterialSkin.Controls.MaterialButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewFeed = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBoxSportType);
            this.panel1.Controls.Add(this.monthCalendarChooseDate);
            this.panel1.Controls.Add(this.materialButtonShowFeed);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 616);
            this.panel1.TabIndex = 1;
            // 
            // comboBoxSportType
            // 
            this.comboBoxSportType.FormattingEnabled = true;
            this.comboBoxSportType.Items.AddRange(new object[] {
            "Soccer",
            "Basketball",
            "Tennis",
            "Handball",
            "Football",
            "Baseball"});
            this.comboBoxSportType.Location = new System.Drawing.Point(4, 183);
            this.comboBoxSportType.Name = "comboBoxSportType";
            this.comboBoxSportType.Size = new System.Drawing.Size(102, 21);
            this.comboBoxSportType.TabIndex = 2;
            this.comboBoxSportType.Text = "Soccer";
            this.comboBoxSportType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSportType_SelectedIndexChanged);
            // 
            // monthCalendarChooseDate
            // 
            this.monthCalendarChooseDate.Location = new System.Drawing.Point(4, 9);
            this.monthCalendarChooseDate.Name = "monthCalendarChooseDate";
            this.monthCalendarChooseDate.TabIndex = 1;
            // 
            // materialButtonShowFeed
            // 
            this.materialButtonShowFeed.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButtonShowFeed.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButtonShowFeed.Depth = 0;
            this.materialButtonShowFeed.HighEmphasis = true;
            this.materialButtonShowFeed.Icon = null;
            this.materialButtonShowFeed.Location = new System.Drawing.Point(4, 213);
            this.materialButtonShowFeed.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButtonShowFeed.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButtonShowFeed.Name = "materialButtonShowFeed";
            this.materialButtonShowFeed.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButtonShowFeed.Size = new System.Drawing.Size(102, 36);
            this.materialButtonShowFeed.TabIndex = 0;
            this.materialButtonShowFeed.Text = "Show Feed";
            this.materialButtonShowFeed.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButtonShowFeed.UseAccentColor = false;
            this.materialButtonShowFeed.UseVisualStyleBackColor = true;
            this.materialButtonShowFeed.Click += new System.EventHandler(this.materialButtonShowFeed_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewFeed);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(241, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1056, 616);
            this.panel2.TabIndex = 2;
            // 
            // dataGridViewFeed
            // 
            this.dataGridViewFeed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFeed.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewFeed.Name = "dataGridViewFeed";
            this.dataGridViewFeed.Size = new System.Drawing.Size(1056, 616);
            this.dataGridViewFeed.TabIndex = 0;
            // 
            // GoalserveDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 683);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "GoalserveDashboard";
            this.Text = "Goalserve Feed";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialButton materialButtonShowFeed;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MonthCalendar monthCalendarChooseDate;
        private System.Windows.Forms.DataGridView dataGridViewFeed;
        private System.Windows.Forms.ComboBox comboBoxSportType;
    }
}

