﻿namespace BgBettingData.Monitor
{
    partial class WinnerBgDataDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkboxDisabledFilter = new System.Windows.Forms.CheckBox();
            this.groupBoxFilters = new System.Windows.Forms.GroupBox();
            this.buttonApplyFilter = new MaterialSkin.Controls.MaterialButton();
            this.comboBoxPrograms = new System.Windows.Forms.ComboBox();
            this.comboBoxSport = new System.Windows.Forms.ComboBox();
            this.buttonFetchData = new MaterialSkin.Controls.MaterialButton();
            this.monthCalendarChooseDate = new System.Windows.Forms.MonthCalendar();
            this.dataGridViewFeed = new System.Windows.Forms.DataGridView();
            this.checkBoxOnly3Bets = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.groupBoxFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkboxDisabledFilter);
            this.panel1.Controls.Add(this.groupBoxFilters);
            this.panel1.Controls.Add(this.comboBoxSport);
            this.panel1.Controls.Add(this.buttonFetchData);
            this.panel1.Controls.Add(this.monthCalendarChooseDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 561);
            this.panel1.TabIndex = 2;
            // 
            // checkboxDisabledFilter
            // 
            this.checkboxDisabledFilter.AutoSize = true;
            this.checkboxDisabledFilter.Location = new System.Drawing.Point(4, 433);
            this.checkboxDisabledFilter.Name = "checkboxDisabledFilter";
            this.checkboxDisabledFilter.Size = new System.Drawing.Size(86, 17);
            this.checkboxDisabledFilter.TabIndex = 9;
            this.checkboxDisabledFilter.Text = "Disable Filter";
            this.checkboxDisabledFilter.UseVisualStyleBackColor = true;
            this.checkboxDisabledFilter.CheckedChanged += new System.EventHandler(this.checkboxDisabledFilter_CheckedChanged);
            // 
            // groupBoxFilters
            // 
            this.groupBoxFilters.Controls.Add(this.checkBoxOnly3Bets);
            this.groupBoxFilters.Controls.Add(this.buttonApplyFilter);
            this.groupBoxFilters.Controls.Add(this.comboBoxPrograms);
            this.groupBoxFilters.Location = new System.Drawing.Point(4, 286);
            this.groupBoxFilters.Name = "groupBoxFilters";
            this.groupBoxFilters.Size = new System.Drawing.Size(164, 141);
            this.groupBoxFilters.TabIndex = 8;
            this.groupBoxFilters.TabStop = false;
            this.groupBoxFilters.Text = "Filters";
            // 
            // buttonApplyFilter
            // 
            this.buttonApplyFilter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonApplyFilter.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonApplyFilter.Depth = 0;
            this.buttonApplyFilter.HighEmphasis = true;
            this.buttonApplyFilter.Icon = null;
            this.buttonApplyFilter.Location = new System.Drawing.Point(6, 96);
            this.buttonApplyFilter.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonApplyFilter.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonApplyFilter.Name = "buttonApplyFilter";
            this.buttonApplyFilter.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonApplyFilter.Size = new System.Drawing.Size(117, 36);
            this.buttonApplyFilter.TabIndex = 6;
            this.buttonApplyFilter.Text = "Apply Filter";
            this.buttonApplyFilter.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonApplyFilter.UseAccentColor = false;
            this.buttonApplyFilter.UseVisualStyleBackColor = true;
            this.buttonApplyFilter.Click += new System.EventHandler(this.buttonApplyFilter_Click);
            // 
            // comboBoxPrograms
            // 
            this.comboBoxPrograms.FormattingEnabled = true;
            this.comboBoxPrograms.Items.AddRange(new object[] {
            "All"});
            this.comboBoxPrograms.Location = new System.Drawing.Point(6, 19);
            this.comboBoxPrograms.Name = "comboBoxPrograms";
            this.comboBoxPrograms.Size = new System.Drawing.Size(126, 21);
            this.comboBoxPrograms.TabIndex = 3;
            this.comboBoxPrograms.Text = "All";
            this.comboBoxPrograms.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrograms_SelectedIndexChanged);
            // 
            // comboBoxSport
            // 
            this.comboBoxSport.FormattingEnabled = true;
            this.comboBoxSport.Items.AddRange(new object[] {
            "Soccer",
            "Basketball"});
            this.comboBoxSport.Location = new System.Drawing.Point(4, 183);
            this.comboBoxSport.Name = "comboBoxSport";
            this.comboBoxSport.Size = new System.Drawing.Size(102, 21);
            this.comboBoxSport.TabIndex = 7;
            this.comboBoxSport.Text = "Soccer";
            this.comboBoxSport.SelectedIndexChanged += new System.EventHandler(this.comboBoxSport_SelectedIndexChanged);
            // 
            // buttonFetchData
            // 
            this.buttonFetchData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonFetchData.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.buttonFetchData.Depth = 0;
            this.buttonFetchData.HighEmphasis = true;
            this.buttonFetchData.Icon = null;
            this.buttonFetchData.Location = new System.Drawing.Point(64, 228);
            this.buttonFetchData.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buttonFetchData.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonFetchData.Name = "buttonFetchData";
            this.buttonFetchData.NoAccentTextColor = System.Drawing.Color.Empty;
            this.buttonFetchData.Size = new System.Drawing.Size(90, 36);
            this.buttonFetchData.TabIndex = 4;
            this.buttonFetchData.Text = "Get Data";
            this.buttonFetchData.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buttonFetchData.UseAccentColor = false;
            this.buttonFetchData.UseVisualStyleBackColor = true;
            this.buttonFetchData.Click += new System.EventHandler(this.buttonFetchData_Click);
            // 
            // monthCalendarChooseDate
            // 
            this.monthCalendarChooseDate.Location = new System.Drawing.Point(4, 9);
            this.monthCalendarChooseDate.Name = "monthCalendarChooseDate";
            this.monthCalendarChooseDate.TabIndex = 1;
            // 
            // dataGridViewFeed
            // 
            this.dataGridViewFeed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFeed.Location = new System.Drawing.Point(241, 64);
            this.dataGridViewFeed.Name = "dataGridViewFeed";
            this.dataGridViewFeed.Size = new System.Drawing.Size(602, 561);
            this.dataGridViewFeed.TabIndex = 3;
            // 
            // checkBoxOnly3Bets
            // 
            this.checkBoxOnly3Bets.AutoSize = true;
            this.checkBoxOnly3Bets.Location = new System.Drawing.Point(6, 60);
            this.checkBoxOnly3Bets.Name = "checkBoxOnly3Bets";
            this.checkBoxOnly3Bets.Size = new System.Drawing.Size(117, 17);
            this.checkBoxOnly3Bets.TabIndex = 10;
            this.checkBoxOnly3Bets.Text = "Only 3 Bets Soccer";
            this.checkBoxOnly3Bets.UseVisualStyleBackColor = true;
            this.checkBoxOnly3Bets.CheckedChanged += new System.EventHandler(this.checkBoxOnly3Bets_CheckedChanged);
            // 
            // WinnerBgDataDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 628);
            this.Controls.Add(this.dataGridViewFeed);
            this.Controls.Add(this.panel1);
            this.Name = "WinnerBgDataDashboard";
            this.Text = "Winner & BgData";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxFilters.ResumeLayout(false);
            this.groupBoxFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialButton buttonFetchData;
        private System.Windows.Forms.MonthCalendar monthCalendarChooseDate;
        private System.Windows.Forms.DataGridView dataGridViewFeed;
        private System.Windows.Forms.ComboBox comboBoxSport;
        private System.Windows.Forms.GroupBox groupBoxFilters;
        private MaterialSkin.Controls.MaterialButton buttonApplyFilter;
        private System.Windows.Forms.ComboBox comboBoxPrograms;
        private System.Windows.Forms.CheckBox checkboxDisabledFilter;
        private System.Windows.Forms.CheckBox checkBoxOnly3Bets;
    }
}