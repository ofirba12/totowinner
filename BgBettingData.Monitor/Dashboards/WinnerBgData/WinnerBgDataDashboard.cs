﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;


namespace BgBettingData.Monitor
{
    public partial class WinnerBgDataDashboard : MaterialForm
    {
        WinnerDataServices winSrv = WinnerDataServices.Instance;
        BgDataServices bgSrv = BgDataServices.Instance;
        private MapperRespository bgMapper;
        private TotoWinner.Data.BgBettingData.Goalserve.GoldserveMatchRepository goldserveFeedToday;
        private TotoWinner.Data.BgBettingData.Goalserve.GoldserveMatchRepository goldserveFeedYesterday;
        private TotoWinner.Data.BgBettingData.Goalserve.GoldserveMatchRepository goldserveFeedTomorrow;
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextTeamsAnalysis = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuShowBetDetails = new ToolStripMenuItem();
        private ToolStripMenuItem contextWhyThisBet = new ToolStripMenuItem();
        private ToolStripMenuItem contextShowOdds = new ToolStripMenuItem();

        private DateTime winnerLineDate = DateTime.Today;
        private BgSport bgSport = BgSport.Soccer;
        private List<WinnerBgCombinedFeedMonitor> feed;
        private List<string> programs;
        private string programIdFilter;
        private bool only3BetsFilter;
        public WinnerBgDataDashboard()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.dataGridViewFeed.CellMouseEnter += DataGridViewFeed_CellMouseEnter;
            this.monthCalendarChooseDate.DateSelected += MonthCalendarChooseDate_DateSelected;
            bgMapper = MonitorUtils.Mapper.GetBgMapper();
        }

        private void MonthCalendarChooseDate_DateSelected(object sender, DateRangeEventArgs e)
        {
            this.winnerLineDate = e.Start.Date;
        }

        private void DataGridViewFeed_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextMenuShowBetDetails.Text = "Show Bet Details";
            contextMenuShowBetDetails.Click -= new EventHandler(contextMenuShowBetDetails_Click);
            contextMenuShowBetDetails.Click += new EventHandler(contextMenuShowBetDetails_Click);
            contextTeamsAnalysis.Text = "Show Teams Analysis";
            contextTeamsAnalysis.Click -= new EventHandler(contextMenuTeamsAnalysis_Click);
            contextTeamsAnalysis.Click += new EventHandler(contextMenuTeamsAnalysis_Click);
            contextWhyThisBet.Text = "Why This Bet?";
            contextWhyThisBet.Click -= new EventHandler(contextWhyThisBet_Click);
            contextWhyThisBet.Click += new EventHandler(contextWhyThisBet_Click);
            contextShowOdds.Text = "Show Odds";
            contextShowOdds.Click -= new EventHandler(contextShowOdds_Click);
            contextShowOdds.Click += new EventHandler(contextShowOdds_Click);
            

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridViewFeed.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridViewFeed.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                DataGridViewFeed_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            var selectedMatch = (WinnerBgCombinedFeedMonitor)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            if (selectedMatch.BGHomeId.HasValue && (selectedMatch.HomeMatches.HasValue || selectedMatch.AwayMatches.HasValue))
            {
                selectedRow.ContextMenuStrip.Items.Add(contextTeamsAnalysis);
                selectedRow.ContextMenuStrip.Items.Add(contextMenuShowBetDetails);
            }
            if (!string.IsNullOrEmpty(selectedMatch.BetTip) && selectedMatch.BetMax.HasValue)
                selectedRow.ContextMenuStrip.Items.Add(contextWhyThisBet);
            if (selectedMatch.BGDate.HasValue && selectedMatch.BGHomeId.HasValue && selectedMatch.BGAwayId.HasValue)
                selectedRow.ContextMenuStrip.Items.Add(contextShowOdds);
        }
        private void contextShowOdds_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (WinnerBgCombinedFeedMonitor)selectedRow.Tag;
            var goalserveMatch = BgMapperServices.Instance.GetGoalserveMatch(selectedMatch.BGHomeId.Value, selectedMatch.BGAwayId.Value, goldserveFeedToday, bgMapper);
            if (goalserveMatch ==  null)
                goalserveMatch = BgMapperServices.Instance.GetGoalserveMatch(selectedMatch.BGHomeId.Value, selectedMatch.BGAwayId.Value, goldserveFeedYesterday, bgMapper);
            if (goalserveMatch == null)
                goalserveMatch = BgMapperServices.Instance.GetGoalserveMatch(selectedMatch.BGHomeId.Value, selectedMatch.BGAwayId.Value, goldserveFeedTomorrow, bgMapper);
            if (goalserveMatch != null)
            {
                var odds = BgDataPersistanceServices.Instance.GetMatchOdds((int)GoalServeSportType.Soccer, goalserveMatch.HomeId, goalserveMatch.AwayId, goalserveMatch.GameStart);
                var dashboard = new ShowBgWinnerOdds(odds, selectedMatch.HomeTeamName, selectedMatch.GuestTeamName, BgSport.Soccer);
                dashboard.Show();
            }
            else                
                MessageBox.Show($"Could not find goalserve match with bgHomeId={selectedMatch.BGHomeId.Value}, bgAwayId={selectedMatch.BGAwayId.Value}","", MessageBoxButtons.OK);
        }

        private void contextWhyThisBet_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (WinnerBgCombinedFeedMonitor)selectedRow.Tag;
            var columnIndex = 11; /* WEB1 */
            switch (selectedMatch.BetTip)
            {
                case "HOME WIN":
                    columnIndex = 11;
                    break;
                case "DRAW":
                    columnIndex = 12;
                    break;
                case "AWAY WIN":
                    columnIndex = 13;
                    break;
                case "UNDER 2.5":
                    columnIndex = 16;
                    break;
                case "OVER 2.5":
                    columnIndex = 17;
                    break;
                case "RANGE 0-1":
                    columnIndex = 20;
                    break;
                case "RANGE 2-3":
                    columnIndex = 21;
                    break;
                case "RANGE 4+":
                    columnIndex = 22;
                    break;


            }
            var dashboard = new WhyThisBetDashboard(
                    columnIndex,
                    selectedMatch.BetMax.Value,
                    this.bgSport,
                    selectedMatch.BGHomeId.Value,
                    selectedMatch.BGHome,
                    selectedMatch.BGAwayId.Value,
                    selectedMatch.BGAway,
                    selectedMatch.HomeOdd.Value,
                    selectedMatch.DrawOdd.HasValue
                    ? selectedMatch.DrawOdd.Value
                    : 1,
                    selectedMatch.AwayOdd.Value);
                dashboard.Show();
        }
        private void contextMenuShowBetDetails_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (WinnerBgCombinedFeedMonitor)selectedRow.Tag;
            var dashboard = new BettingDataDashboard(bgSport,
                selectedMatch.BGHomeId.Value,
                selectedMatch.BGHome,
                selectedMatch.BGAwayId.Value,
                selectedMatch.BGAway,
                selectedMatch.HomeOdd.Value,
                selectedMatch.DrawOdd.Value,
                selectedMatch.AwayOdd.Value);
            dashboard.Show();
        }
        private void contextMenuTeamsAnalysis_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewFeed.Rows[mouseLocation.RowIndex];
            var selectedMatch = (WinnerBgCombinedFeedMonitor)selectedRow.Tag;
            var dashboard = new TeamABAnalysisDashboard(selectedMatch.BGHomeId.Value,
                selectedMatch.BGHome,
                selectedMatch.BGAwayId.Value,
                selectedMatch.BGAway,
                bgSport);
            dashboard.Show();
        }
        private void buttonFetchData_Click(object sender, EventArgs e)
        {
            var winnerSport = winSrv.BgSportToWinnerSport(bgSport);
            var winnerMapper = winSrv.GetWinnerMapper();
            var winnerData = winSrv.GetWinnerData(bgSport, winnerLineDate, winnerMapper);
            var bgFeed = BgDataServices.Instance.GetBgFeedWithFilter(bgSport,
                    winnerLineDate,
                    3,
                    null,
                    null,
                    bgMapper);
            var bgMatches = new Dictionary<BgFeedExactKey, BgMatch>();
            bgFeed.AllMatches.ForEach(m =>
            {
                //could be same event/match in different programs
                var key = new BgFeedExactKey(bgSport, winnerLineDate, m.HomeId, m.AwayId);
                if (!bgMatches.ContainsKey(key))
                {
                    bgMatches.Add(key, m);
                }
            }
            );
            var impliedStatistics = bgSrv.CreateImpliedStatistics(bgFeed.Matches, bgFeed.Statistics, bgFeed.StatisticsDetails);
            var combined = winSrv.CombineBgAndWinnerFeeds(bgSport, winnerLineDate, winnerSport, winnerMapper, winnerData, bgMatches, impliedStatistics);
            feed = combined.ConvertAll<WinnerBgCombinedFeedMonitor>(m => new WinnerBgCombinedFeedMonitor(m, bgFeed));
            dataGridViewFeed.Rows.Clear();
            dataGridViewFeed.Columns.Clear();
            dataGridViewFeed.Refresh();

            programs = feed.Select(f => f.ProgramId.ToString()).ToList().Distinct().ToList();
            comboBoxPrograms.Items.Clear();
            comboBoxPrograms.Items.Add("All");
            comboBoxPrograms.Items.AddRange(programs.ToArray());

            MonitorUtils.BuildGrid<WinnerBgCombinedFeedMonitor>(feed, this.dataGridViewFeed, BgSport.Soccer);
            AddContextMenu();
            goldserveFeedToday = GoalServeProviderServices.Instance.GetRawFeed(BgDataServices.Instance.BgSportToGoalserveSport(BgSport.Soccer), winnerLineDate.Date);
            goldserveFeedYesterday = GoalServeProviderServices.Instance.GetRawFeed(BgDataServices.Instance.BgSportToGoalserveSport(BgSport.Soccer), winnerLineDate.Date.AddDays(-1));
            goldserveFeedTomorrow = GoalServeProviderServices.Instance.GetRawFeed(BgDataServices.Instance.BgSportToGoalserveSport(BgSport.Soccer), winnerLineDate.Date.AddDays(1));
        }

        private void comboBoxSport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                var comboBox = sender as ComboBox;
                if (Enum.TryParse<BgSport>(comboBox.Text, out var sport))
                {
                    this.bgSport = sport;
                }
            }
        }

        private void buttonApplyFilter_Click(object sender, EventArgs e)
        {
            Func<WinnerBgCombinedFeedMonitor, bool> only3Bets = (m) =>
            {
                var betType = (BetTypes)m.BetTypeId;
                var halfBet = betType == BetTypes.BetSoccer1x2 && !m.Name.Contains("מחצית");
                var bet2_5Only = betType == BetTypes.BetSoccerOverUnder2_5 && m.Name.Contains("2.5");
                if (m.Sport == SportIds.Soccer && (halfBet || bet2_5Only || betType == BetTypes.BetSoccerTotalScores))
                    return true;
                return false;
            };
            var filtered = feed.Where(f =>
                only3Bets(f) &&
                (string.IsNullOrEmpty(this.programIdFilter) || f.ProgramId.ToString() == this.programIdFilter))
                    .ToList();
            dataGridViewFeed.Rows.Clear();
            dataGridViewFeed.Columns.Clear();
            dataGridViewFeed.Refresh();
            MonitorUtils.BuildGrid<WinnerBgCombinedFeedMonitor>(filtered, this.dataGridViewFeed, BgSport.Soccer);
            AddContextMenu();
        }

        private void comboBoxPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.ComboBox)
            {
                var comboBox = sender as System.Windows.Forms.ComboBox;
                var programId = comboBox.Text != "All"
                    ? programs.First(i => i == comboBox.Text)
                    : null;
                this.programIdFilter = programId;

            }
        }

        private void checkboxDisabledFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkboxDisabledFilter.Checked)
            {
                this.programIdFilter = null;
                this.only3BetsFilter = false;
                comboBoxPrograms.Enabled = false;
                buttonApplyFilter.Enabled = false;
                checkBoxOnly3Bets.Enabled = false;
                buttonApplyFilter_Click(null, null);
            }
            else
            {
                comboBoxPrograms.Enabled = true;
                buttonApplyFilter.Enabled = true;
                checkBoxOnly3Bets.Enabled = true;
                this.only3BetsFilter = this.checkBoxOnly3Bets.Checked;
                if (comboBoxPrograms.Text != "All")
                    this.programIdFilter = programs.First(i => i == comboBoxPrograms.Text);
                buttonApplyFilter_Click(null, null);
            }
        }

        private void checkBoxOnly3Bets_CheckedChanged(object sender, EventArgs e)
        {
            only3BetsFilter = this.checkBoxOnly3Bets.Checked;
        }
    }
}
