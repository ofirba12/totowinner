﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BgBettingData.Monitor
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static public readonly List<string> AdminDesktops = new List<string>()
        {
            "ELOIBA-DESKTOP",
            "DESKTOP-PT812QS",
            "DESKTOP-UDGIK3B"
        };
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (AdminDesktops.Contains(Environment.MachineName.ToUpper()))
                Application.Run(new Selector());
            else
                Application.Run(new BankerimDataDashboard());
        }
    }
}
