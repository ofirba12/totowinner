﻿CREATE TYPE [dbo].[RealTimeRatesPipeTVP] AS TABLE(
	[ProgramType]	TINYINT			NOT NULL,
	[GameIndex]		TINYINT			NOT NULL,
	[Rate1]			FLOAT			NOT NULL,
	[RateX]			FLOAT			NOT NULL,
	[Rate2]			FLOAT			NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[ProgramType],[GameIndex] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)