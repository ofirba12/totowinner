﻿CREATE TABLE [dbo].[RealTimeRatesPipe]
(
	[ProgramType]	TINYINT			NOT NULL,
	[GameIndex]		TINYINT			NOT NULL,
	[Rate1]			FLOAT			NOT NULL,
	[RateX]			FLOAT			NOT NULL,
	[Rate2]			FLOAT			NOT NULL
)
