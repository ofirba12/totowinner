﻿CREATE PROCEDURE [dbo].[RealTimeRatesPipe_Merge]
	@Rates AS [RealTimeRatesPipeTVP] READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);

	MERGE dbo.[RealTimeRatesPipe] AS [Target]
	USING @Rates AS [Source]	
		ON [Source].[ProgramType] = [Target].[ProgramType]
			AND	[Source].[GameIndex] = [Target].[GameIndex]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Rate1]	= [Source].[Rate1], 
				   [Target].[RateX]	= [Source].[RateX],
				   [Target].[Rate2]	= [Source].[Rate2]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramType]
				,[GameIndex]
				,[Rate1]
				,[RateX]
				,[Rate2])
		VALUES ( [Source].[ProgramType]
				,[Source].[GameIndex]
				,[Source].[Rate1]
				,[Source].[RateX]
				,[Source].[Rate2])
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END