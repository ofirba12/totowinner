﻿CREATE PROCEDURE [dbo].[GamesValidation_Merge]
	@ProgramId		INT,			
	@Index			TINYINT,		
	@Validation		TINYINT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[GamesValidation] AS [Target]
	USING ( SELECT	 @ProgramId		AS	"ProgramId"
					,@Index			AS	"Index"
					,@Validation	AS	"Validation"
	) AS [Source]	ON [Source].[ProgramId] = [Target].[ProgramId]
					AND	[Source].[Index] = [Target].[Index]
					AND	[Source].[Validation] = [Target].[Validation]
	--WHEN MATCHED THEN 
	--	UPDATE SET [Target].[GameTime]		= [Source].[GameTime],
	--			   [Target].[Description]	= [Source].[Description]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramId]
				,[Index]
				,[Validation])
		VALUES ([Source].[ProgramId]
				,[Source].[Index]
				,[Source].[Validation])
				;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END