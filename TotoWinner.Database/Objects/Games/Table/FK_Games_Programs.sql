﻿ALTER TABLE [dbo].[Games]
	ADD CONSTRAINT [FK_Games_Programs]
	FOREIGN KEY (ProgramId)
	REFERENCES [Programs] (Id)
