﻿CREATE TABLE [dbo].[Games]
(
	[ProgramId]		INT				NOT NULL,
	[Index]			TINYINT			NOT NULL,
	[GameTime]		NVARCHAR(50)	NOT NULL,
	[Description]	NVARCHAR(500)	NOT NULL,
	[LeagueName]	NVARCHAR(50)	NOT NULL,
	[GameResult]	CHAR(10)		NOT NULL,
	[OpenForBets]	BIT				NOT NULL,
	[BetColor]		CHAR(20)		NOT NULL,
	[Rate1]			FLOAT			NOT NULL,
	[RateX]			FLOAT			NOT NULL,
	[Rate2]			FLOAT			NOT NULL,
	[Won]			CHAR(1)			NULL
)
