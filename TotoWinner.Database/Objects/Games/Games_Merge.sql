﻿CREATE PROCEDURE [dbo].[Games_Merge]
	@ProgramId		INT,			
	@Index			TINYINT,		
	@GameTime		NVARCHAR(50),
	@Description	NVARCHAR(500),
	@LeagueName		NVARCHAR(50),
	@GameResult		CHAR(10),
	@OpenForBets	BIT,
	@BetColor		CHAR(20),
	@Rate1			FLOAT,
	@RateX			FLOAT,
	@Rate2 			FLOAT,
	@Won 			CHAR(1)

AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Games] AS [Target]
	USING ( SELECT	 @ProgramId		AS	"ProgramId"
					,@Index			AS	"Index"
					,@GameTime		AS	"GameTime"
					,@Description	AS	"Description"
					,@LeagueName	AS	"LeagueName"
					,@GameResult	AS	"GameResult"
					,@OpenForBets	AS	"OpenForBets"
					,@BetColor		AS	"BetColor"
					,@Rate1			AS	"Rate1"
					,@RateX			AS	"RateX"
					,@Rate2			AS	"Rate2"
					,@Won			AS	"Won"
	) AS [Source]	ON [Source].[ProgramId] = [Target].[ProgramId]
					AND	[Source].[Index] = [Target].[Index]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[GameTime]		= [Source].[GameTime],
				   [Target].[Description]	= [Source].[Description],
				   [Target].[LeagueName]	= [Source].[LeagueName],
				   [Target].[GameResult]	= [Source].[GameResult],
				   [Target].[OpenForBets]	= [Source].[OpenForBets],
				   [Target].[BetColor]		= [Source].[BetColor],
				   [Target].[Rate1]			= [Source].[Rate1],
				   [Target].[RateX]			= [Source].[RateX],
				   [Target].[Rate2]			= [Source].[Rate2],
				   [Target].[Won]			= [Source].[Won]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramId]
				,[Index]
				,[GameTime]
				,[Description]
				,[LeagueName]
				,[GameResult]
				,[OpenForBets]
				,[BetColor]
				,[Rate1]
				,[RateX]
				,[Rate2]
				,[Won]
				)
		VALUES ([Source].[ProgramId]
				,[Source].[Index]
				,[Source].[GameTime]
				,[Source].[Description]
				,[Source].[LeagueName]
				,[Source].[GameResult]
				,[Source].[OpenForBets]
				,[Source].[BetColor]
				,[Source].[Rate1]
				,[Source].[RateX]
				,[Source].[Rate2]
				,[Source].[Won]
				)
				;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
