﻿CREATE TABLE [dbo].[TWBacktestingBaseBets]
(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[GameIndex]		TINYINT		NOT NULL,
	[Win_1]			BIT			NOT NULL,
	[Win_X]			BIT			NOT NULL,
	[Win_2]			BIT			NOT NULL,
	[Inserted]		DATETIME	NOT NULL DEFAULT getdate()
)
