﻿CREATE TYPE [dbo].[TWBacktestingBaseBetsTVP] AS TABLE(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[GameIndex]		TINYINT		NOT NULL,
	[Win_1]			BIT			NOT NULL,
	[Win_X]			BIT			NOT NULL,
	[Win_2]			BIT			NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[ExecutionId],[ProgramId],[GameIndex] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)