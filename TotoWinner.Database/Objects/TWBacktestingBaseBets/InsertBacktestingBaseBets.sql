﻿CREATE PROCEDURE [dbo].[InsertBacktestingBaseBets]
	@BaseBets AS [TWBacktestingBaseBetsTVP] READONLY
AS
	INSERT INTO [TWBacktestingBaseBets]([ExecutionId], [ProgramId], [GameIndex], [Win_1], [Win_X], [Win_2])
	SELECT [ExecutionId], [ProgramId], [GameIndex], [Win_1], [Win_X], [Win_2] FROM @BaseBets;
RETURN 0
