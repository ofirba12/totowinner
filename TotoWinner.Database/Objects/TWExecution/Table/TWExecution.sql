﻿CREATE TABLE [dbo].[TWExecution]
(
	[ExecutionId]		INT IDENTITY(10000,1)	NOT NULL PRIMARY KEY,
	[SystemType]		TINYINT					NOT NULL,
	[Status]			SMALLINT				NOT NULL,
	[Parameters]		NVARCHAR(MAX)			NOT NULL,
	[Results]			NVARCHAR(MAX),
	[Inserted]			DATETIME				NOT NULL,
	[LastUpdate]		DATETIME				NOT NULL
)
