﻿ALTER TABLE [dbo].[Programs]
	ADD CONSTRAINT [UKC_Programs]
	UNIQUE ([Type], [Year], [Number])
