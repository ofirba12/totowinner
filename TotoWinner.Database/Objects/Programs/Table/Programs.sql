﻿CREATE TABLE [dbo].[Programs]
(
	[Id]			INT IDENTITY(1,1)		NOT NULL,
	[Type]			TINYINT					NOT NULL,
	[Year]			SMALLINT				NOT NULL,
	[Number]		INT						NOT NULL,
	[EndDate]		DATETIME				NOT NULL,
	[LastUpdate]	DATETIME				NOT NULL
)
