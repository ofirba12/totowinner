﻿CREATE PROCEDURE [dbo].[Programs_Merge]
	@Type			TINYINT,
	@Year			SMALLINT,
	@Number			INT,
	@EndDate		DATETIME
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Programs] AS [Target]
	USING ( SELECT	 @Type			AS	"Type"
					,@Year			AS	"Year"
					,@Number		AS	"Number"
					,@EndDate		AS	"EndDate"
	) AS [Source]	ON [Source].[Type] = [Target].[Type]
					AND	[Source].[Year] = [Target].[Year]
					AND	[Source].[Number] = [Target].[Number]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Type]			= [Source].[Type], 
				   [Target].[Year]			= [Source].[Year],
				   [Target].[Number]		= [Source].[Number],
				   [Target].[EndDate]		= [Source].[EndDate],
				   [Target].[LastUpdate]	= GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Type]
				,[Year]
				,[Number]
				,[EndDate]
				,[LastUpdate])
		VALUES ([Source].[Type]
				,[Source].[Year]
				,[Source].[Number]
				,[Source].[EndDate],
				GetDate())
	OUTPUT Inserted.Id
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END