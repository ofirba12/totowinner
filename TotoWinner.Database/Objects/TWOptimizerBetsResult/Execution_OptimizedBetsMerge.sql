﻿CREATE PROCEDURE [dbo].[Execution_OptimizedBetsMerge]
	@ExecutionId	INT,
	@GameIndex		INT,
	@Bet			NVARCHAR(3),
	@IsFinalBet		BIT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWOptimizerBetsResult] AS [Target]
	USING ( SELECT	 @ExecutionId	AS	"ExecutionId"
					,@GameIndex		AS	"GameIndex"
					,@Bet			AS	"Bet"
					,@IsFinalBet	AS	"IsFinalBet"
	) AS [Source]	ON [Source].[ExecutionId] = [Target].[ExecutionId]
					AND [Source].[GameIndex] = [Target].[GameIndex]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Bet]		 = [Source].[Bet], 
				   [Target].[IsFinalBet] = [Source].[IsFinalBet],
				   [Target].[LastUpdate] = GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([ExecutionId]
				,[GameIndex]
				,[Bet]
				,[IsFinalBet]
				,[Inserted]
				,[LastUpdate])
		VALUES ([Source].[ExecutionId]
				,[Source].[GameIndex]
				,[Source].[Bet]
				,[Source].[IsFinalBet]
				,GetDate()
				,GetDate());

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END