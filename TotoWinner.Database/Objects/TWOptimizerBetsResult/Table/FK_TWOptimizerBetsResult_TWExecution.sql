﻿ALTER TABLE [dbo].[TWOptimizerBetsResult]
	ADD CONSTRAINT [FK_TWOptimizerBetsResult_TWExecution]
	FOREIGN KEY (ExecutionId)
	REFERENCES [TWExecution] (ExecutionId)
