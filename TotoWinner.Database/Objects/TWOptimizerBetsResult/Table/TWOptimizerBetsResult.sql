﻿CREATE TABLE [dbo].[TWOptimizerBetsResult]
(
	[ExecutionId]	INT			NOT NULL,
	[GameIndex]		INT			NOT NULL,
	[Bet]			NVARCHAR(3) NOT NULL,
	[IsFinalBet]	BIT			NOT NULL,
	[Inserted]		DATETIME	NOT NULL,
	[LastUpdate]	DATETIME	NOT NULL

)
