﻿ALTER TABLE [dbo].[TWOptimizerBetsResult]
	ADD CONSTRAINT [PK_TWOptimizerBetsResult]
	PRIMARY KEY ([ExecutionId], [GameIndex])
