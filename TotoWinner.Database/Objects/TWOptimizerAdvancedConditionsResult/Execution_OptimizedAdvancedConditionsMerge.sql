﻿CREATE PROCEDURE [dbo].[Execution_OptimizedAdvancedConditionsMerge]
	@ExecutionId	INT,
	@ConditionType	TINYINT,
	@Index			TINYINT,
	@Min			FLOAT,
	@Max			FLOAT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWOptimizerAdvancedConditionsResult] AS [Target]
	USING ( SELECT	 @ExecutionId			AS	"ExecutionId"
					,@ConditionType			AS	"ConditionType"
					,@Index					AS	"Index"
					,@Min					AS	"Min"
					,@Max					AS	"Max"
	) AS [Source]	ON [Source].[ExecutionId] = [Target].[ExecutionId]
					AND [Source].[ConditionType] = [Target].[ConditionType]
					AND [Source].[Index] = [Target].[Index]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Min] = [Source].[Min], 
				   [Target].[Max] = [Source].[Max],
				   [Target].[LastUpdate] = GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([ExecutionId]
				,[ConditionType]
				,[Index]
				,[Min]
				,[Max]
				,[Inserted]
				,[LastUpdate])
		VALUES ([Source].[ExecutionId]
				,[Source].[ConditionType]
				,[Source].[Index]
				,[Source].[Min]
				,[Source].[Max]
				,GetDate()
				,GetDate());

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END