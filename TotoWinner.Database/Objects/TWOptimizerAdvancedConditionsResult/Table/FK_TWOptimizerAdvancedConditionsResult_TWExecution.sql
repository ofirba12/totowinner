﻿ALTER TABLE [dbo].[TWOptimizerAdvancedConditionsResult]
	ADD CONSTRAINT [FK_TWOptimizerAdvancedConditionsResult_TWExecution]
	FOREIGN KEY (ExecutionId)
	REFERENCES [TWExecution] (ExecutionId)
