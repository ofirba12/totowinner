﻿CREATE TABLE [dbo].[TWOptimizerAdvancedConditionsResult]
(
	[ExecutionId]				INT			NOT NULL,
	[ConditionType]				TINYINT		NOT NULL,
	[Index]						TINYINT		NOT NULL,
	[Min]						FLOAT		NOT NULL,
	[Max]						FLOAT		NOT NULL,
	[Inserted]					DATETIME	NOT NULL,
	[LastUpdate]				DATETIME	NOT NULL
)
