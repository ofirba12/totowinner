﻿ALTER TABLE [dbo].[TWOptimizerAdvancedConditionsResult]
	ADD CONSTRAINT [PK_TWOptimizerAdvancedConditionsResult]
	PRIMARY KEY ([ExecutionId], [ConditionType], [Index])
