﻿CREATE PROCEDURE [dbo].[Execution_OptimizedAdvancedConditions_Get]
	@ExecutionId INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	SELECT * FROM TWOptimizerAdvancedConditionsResult WITH (NOLOCK)
	WHERE ExecutionId = @ExecutionId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END