﻿CREATE TABLE [dbo].[TWOptimizerConditionsResult]
(
	[ExecutionId]		INT			NOT NULL,
	[ConditionType]		TINYINT		NOT NULL,
	[ConditionTitle]	VARCHAR(1)  NOT NULL,
	[Min]				FLOAT		NOT NULL,
	[Max]				FLOAT		NOT NULL,
	[Inserted]			DATETIME	NOT NULL,
	[LastUpdate]		DATETIME	NOT NULL
)
