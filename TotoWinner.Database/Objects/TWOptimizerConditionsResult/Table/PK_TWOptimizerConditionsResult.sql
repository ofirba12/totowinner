﻿ALTER TABLE [dbo].[TWOptimizerConditionsResult]
	ADD CONSTRAINT [PK_TWOptimizerConditionsResult]
	PRIMARY KEY ([ExecutionId], [ConditionType], [ConditionTitle])
