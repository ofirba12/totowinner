﻿ALTER TABLE [dbo].[TWOptimizerConditionsResult]
	ADD CONSTRAINT [FK_TWOptimizerConditionsResult_TWExecution]
	FOREIGN KEY (ExecutionId)
	REFERENCES [TWExecution] (ExecutionId)
