﻿CREATE TABLE [dbo].[TWBacktestingResults]
(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[BetStrike]		TINYINT		NOT NULL,
	[Counter]		INT			NOT NULL,
	[Inserted]		DATETIME	NOT NULL DEFAULT getdate()
)
