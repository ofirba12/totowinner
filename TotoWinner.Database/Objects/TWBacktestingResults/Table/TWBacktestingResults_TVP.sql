﻿CREATE TYPE [dbo].[TWBacktestingResultsTVP] AS TABLE(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[BetStrike]		TINYINT		NOT NULL,
	[Counter]		INT			NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[ExecutionId],[ProgramId],[BetStrike] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)