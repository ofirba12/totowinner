﻿ALTER TABLE [dbo].[TWBacktestingResults]
	ADD CONSTRAINT [CHK_TWBacktestingResults_BetStrike]
	CHECK (BetStrike >= 0 AND BetStrike <=16)
