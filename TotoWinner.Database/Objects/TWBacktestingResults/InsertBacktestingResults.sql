﻿CREATE PROCEDURE [dbo].[InsertBacktestingResults]
	@BacktestingResults AS [TWBacktestingResultsTVP] READONLY
AS
	INSERT INTO [TWBacktestingResults]([ExecutionId], [ProgramId], [BetStrike], [Counter])
	SELECT [ExecutionId], [ProgramId], [BetStrike], [Counter] FROM @BacktestingResults;
RETURN 0
