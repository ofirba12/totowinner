﻿CREATE TABLE [dbo].[TWTemplates]
(
	[Id]			INT					IDENTITY(1,1) NOT NULL,
	[Type]			SMALLINT			NOT NULL,
    [Name]			NVARCHAR (150)		,
	[Value]			NVARCHAR (500)		NOT NULL
)
