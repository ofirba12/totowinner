﻿CREATE PROCEDURE [dbo].[System_Get]
	@Id		INT = NULL,
	@Type	SMALLINT = NULL
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	SELECT * FROM TWSystems
	WHERE	 [Id] = ISNULL(@Id, [Id])
		AND  [Type] = ISNULL(@Type, [Type])

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END