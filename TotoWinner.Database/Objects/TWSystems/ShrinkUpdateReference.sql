﻿CREATE PROCEDURE [dbo].[ShrinkUpdateReference]
	@ShrinkSystemId		INT,
	@OptimizerSystemId	INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	UPDATE [dbo].[TWSystems]
	SET [ReferenceId] = @OptimizerSystemId
	WHERE Id = @ShrinkSystemId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END