﻿CREATE TABLE [dbo].[TWSystems]
(
	[Id]			INT					IDENTITY(1,1) NOT NULL,
	[Type]			SMALLINT			NOT NULL,
    [Name]			NVARCHAR (150)		NOT NULL,
	[ProgramId]		INT					NOT NULL,
	[ProgramType]	INT					NOT NULL,
	[ReferenceId]   INT					NULL,
	[LastUpdate]	DATETIME			NOT NULL,
	[RawData]		NVARCHAR(MAX)		NOT NULL
)
