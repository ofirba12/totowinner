﻿CREATE TABLE [dbo].[TWUnionSystemResults]
(
	[Id]				INT					IDENTITY(1000,1) NOT NULL,
	[SystemIds]			NVARCHAR(100)		NOT NULL,
	[LastUpdate]		DATETIME			NOT NULL,
	[SMART]				NVARCHAR(1000)		NULL,
	[RawResultsData]	NVARCHAR(MAX)		NOT NULL
)
