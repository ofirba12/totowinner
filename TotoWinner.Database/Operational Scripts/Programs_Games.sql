USE [TotoWinner]
GO
--VIEW GAME & PROGRAMS
select * from programs order by LastUpdate desc
select * from games
select p.*, gm.*, lm.Description from gamesValidation gm
inner join [dbo].[Lookup_GamesValidation] lm on lm.ValidationId = gm.Validation
inner join programs p on p.Id = gm.ProgramId
--select * from games with (INDEX(IX_Games_OpenForBets)) where openforbets=1
--select * from games where openforbets=1

--update games
--set rate1=2.2
--where ProgramId = 519 and [Index]=1

--delete from Games where ProgramId=1
--delete from Programs where Id=1

--TRUNCATING ALL GAMES AND PROGRAMS (so the maintenance service will retrieve them all again)
USE TotoWinner
GO

TRUNCATE TABLE [Games]
TRUNCATE TABLE [GamesValidation]

ALTER TABLE [dbo].[Games]
DROP CONSTRAINT [FK_Games_Programs]
GO

TRUNCATE TABLE  [Programs]
GO
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_Games_Programs]
FOREIGN KEY (ProgramId)
REFERENCES [Programs] (Id)
GO