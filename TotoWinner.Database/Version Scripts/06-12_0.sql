﻿USE TotoWinner
GO
--DROP TABLE [Games]
--DROP TABLE [Programs]
--DROP TABLE [GamesValidation]
--DROP TABLE [Lookup_GamesValidation]

--TRUNCATE TABLE [Games]
--TRUNCATE TABLE [GamesValidation]

--ALTER TABLE [dbo].[Games]
--DROP CONSTRAINT [FK_Games_Programs]
--GO

--TRUNCATE TABLE  [Programs]
--GO
--ALTER TABLE [dbo].[Games]
--ADD CONSTRAINT [FK_Games_Programs]
--FOREIGN KEY (ProgramId)
--REFERENCES [Programs] (Id)
--GO

CREATE TABLE [dbo].[Lookup_GamesValidation]
(
	[ValidationId]	TINYINT			NOT NULL,
	[Description]	NVARCHAR(300)	NOT NULL
)

ALTER TABLE [dbo].[Lookup_GamesValidation]
	ADD CONSTRAINT [PK_Lookup_GamesValidation]
	PRIMARY KEY ([ValidationId])


CREATE TABLE [dbo].[Programs]
(
	[Id]			INT IDENTITY(1,1)		NOT NULL,
	[Type]			TINYINT					NOT NULL,
	[Year]			SMALLINT				NOT NULL,
	[Number]		INT						NOT NULL,
	[EndDate]		DATETIME				NOT NULL,
	[LastUpdate]	DATETIME				NOT NULL
)
GO

ALTER TABLE [dbo].[Programs]
	ADD CONSTRAINT [PK_Programs]
	PRIMARY KEY ([Id])
GO

ALTER TABLE [dbo].[Programs]
	ADD CONSTRAINT [UKC_Programs]
	UNIQUE ([Type], [Year], [Number])
GO

CREATE TABLE [dbo].[Games]
(
	[ProgramId]		INT				NOT NULL,
	[Index]			TINYINT			NOT NULL,
	[GameTime]		NVARCHAR(50)	NOT NULL,
	[Description]	NVARCHAR(500)	NOT NULL,
	[LeagueName]	NVARCHAR(50)	NOT NULL,
	[GameResult]	CHAR(10)		NOT NULL,
	[OpenForBets]	BIT				NOT NULL,
	[BetColor]		CHAR(20)		NOT NULL,
	[Rate1]			FLOAT			NOT NULL,
	[RateX]			FLOAT			NOT NULL,
	[Rate2]			FLOAT			NOT NULL,
	[Won]			CHAR(1)			NULL
)
GO

ALTER TABLE [dbo].[Games]
	ADD CONSTRAINT [PK_Games]
	PRIMARY KEY ([ProgramId], [Index])
GO

ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_Games_Programs]
FOREIGN KEY (ProgramId)
REFERENCES [Programs] (Id)
GO

--DROP INDEX [Games].[IX_Games_OpenForBets]
--CREATE NONCLUSTERED INDEX [IX_Games_OpenForBets]
--	ON [dbo].[Games]
--	([ProgramId], [Index], [OpenForBets])


CREATE TABLE [dbo].[GamesValidation]
(
	[ProgramId]		INT				NOT NULL,
	[Index]			TINYINT			NOT NULL,
	[Validation]	TINYINT			NOT NULL
)
GO
ALTER TABLE [dbo].[GamesValidation]
	ADD CONSTRAINT [PK_GamesValidation]
	PRIMARY KEY ([ProgramId], [Index], [Validation])
GO
ALTER TABLE [dbo].[GamesValidation]
	ADD CONSTRAINT [FK_GamesValidation_Lookup_GamesValidation]
	FOREIGN KEY ([Validation])
	REFERENCES [Lookup_GamesValidation] ([ValidationId])
GO
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (0,'ValidForCalculation')
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (1,'MissingRates')
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (2,'DummyGame')
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (3,'NoWinnerFound')
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (4,'GamePostponed')
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (5,'GameCanceled')
INSERT INTO [dbo].[Lookup_GamesValidation] ([ValidationId], [Description]) VALUES (6,'GameResultMissing')
GO

CREATE PROCEDURE [dbo].[Programs_Merge]
	@Type			TINYINT,
	@Year			SMALLINT,
	@Number			INT,
	@EndDate		DATETIME
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Programs] AS [Target]
	USING ( SELECT	 @Type			AS	"Type"
					,@Year			AS	"Year"
					,@Number		AS	"Number"
					,@EndDate		AS	"EndDate"
	) AS [Source]	ON [Source].[Type] = [Target].[Type]
					AND	[Source].[Year] = [Target].[Year]
					AND	[Source].[Number] = [Target].[Number]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Type]			= [Source].[Type], 
				   [Target].[Year]			= [Source].[Year],
				   [Target].[Number]		= [Source].[Number],
				   [Target].[EndDate]		= [Source].[EndDate],
				   [Target].[LastUpdate]	= GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Type]
				,[Year]
				,[Number]
				,[EndDate]
				,[LastUpdate])
		VALUES ([Source].[Type]
				,[Source].[Year]
				,[Source].[Number]
				,[Source].[EndDate],
				GetDate())
	OUTPUT Inserted.Id
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Games_Merge]
	@ProgramId		INT,			
	@Index			TINYINT,		
	@GameTime		NVARCHAR(50),
	@Description	NVARCHAR(500),
	@LeagueName		NVARCHAR(50),
	@GameResult		CHAR(10),
	@OpenForBets	BIT,
	@BetColor		CHAR(20),
	@Rate1			FLOAT,
	@RateX			FLOAT,
	@Rate2 			FLOAT,
	@Won 			CHAR(1)

AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[Games] AS [Target]
	USING ( SELECT	 @ProgramId		AS	"ProgramId"
					,@Index			AS	"Index"
					,@GameTime		AS	"GameTime"
					,@Description	AS	"Description"
					,@LeagueName	AS	"LeagueName"
					,@GameResult	AS	"GameResult"
					,@OpenForBets	AS	"OpenForBets"
					,@BetColor		AS	"BetColor"
					,@Rate1			AS	"Rate1"
					,@RateX			AS	"RateX"
					,@Rate2			AS	"Rate2"
					,@Won			AS	"Won"
	) AS [Source]	ON [Source].[ProgramId] = [Target].[ProgramId]
					AND	[Source].[Index] = [Target].[Index]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[GameTime]		= [Source].[GameTime],
				   [Target].[Description]	= [Source].[Description],
				   [Target].[LeagueName]	= [Source].[LeagueName],
				   [Target].[GameResult]	= [Source].[GameResult],
				   [Target].[OpenForBets]	= [Source].[OpenForBets],
				   [Target].[BetColor]		= [Source].[BetColor],
				   [Target].[Rate1]			= [Source].[Rate1],
				   [Target].[RateX]			= [Source].[RateX],
				   [Target].[Rate2]			= [Source].[Rate2],
				   [Target].[Won]			= [Source].[Won]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramId]
				,[Index]
				,[GameTime]
				,[Description]
				,[LeagueName]
				,[GameResult]
				,[OpenForBets]
				,[BetColor]
				,[Rate1]
				,[RateX]
				,[Rate2]
				,[Won]
				)
		VALUES ([Source].[ProgramId]
				,[Source].[Index]
				,[Source].[GameTime]
				,[Source].[Description]
				,[Source].[LeagueName]
				,[Source].[GameResult]
				,[Source].[OpenForBets]
				,[Source].[BetColor]
				,[Source].[Rate1]
				,[Source].[RateX]
				,[Source].[Rate2]
				,[Source].[Won]
				)
				;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

CREATE PROCEDURE [dbo].[GamesValidation_Merge]
	@ProgramId		INT,			
	@Index			TINYINT,		
	@Validation		TINYINT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[GamesValidation] AS [Target]
	USING ( SELECT	 @ProgramId		AS	"ProgramId"
					,@Index			AS	"Index"
					,@Validation	AS	"Validation"
	) AS [Source]	ON [Source].[ProgramId] = [Target].[ProgramId]
					AND	[Source].[Index] = [Target].[Index]
					AND	[Source].[Validation] = [Target].[Validation]
	--WHEN MATCHED THEN 
	--	UPDATE SET [Target].[GameTime]		= [Source].[GameTime],
	--			   [Target].[Description]	= [Source].[Description]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramId]
				,[Index]
				,[Validation])
		VALUES ([Source].[ProgramId]
				,[Source].[Index]
				,[Source].[Validation])
				;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO