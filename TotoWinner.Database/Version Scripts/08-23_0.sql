﻿USE TotoWinner
GO

--DROP TABLE [dbo].[TWTemplates]
--GO

CREATE TABLE [dbo].[TWTemplates]
(
	[Id]			INT					IDENTITY(1,1) NOT NULL,
	[Type]			SMALLINT			NOT NULL,
    [Name]			NVARCHAR (150)		,
	[Value]			NVARCHAR (500)		NOT NULL
)

GO

ALTER TABLE [dbo].[TWTemplates]
	ADD CONSTRAINT [PK_TWTemplates]
	PRIMARY KEY ([Id])
GO

CREATE PROCEDURE [dbo].[Template_Merge]
	@Type			SMALLINT,
    @Name			NVARCHAR (150),
	@Value			NVARCHAR (500)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWTemplates] AS [Target]
	USING ( SELECT	@Type			AS	"Type"
					,@Name			AS	"Name"
					,@Value			AS	"Value"
	) AS [Source]	ON [Source].[Name] = [Target].[Name]
					AND	[Source].[Type] = [Target].[Type]
					AND [Source].[Value]= [Target].[Value]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Type]			= [Source].[Type], 
				   [Target].[Name]			= [Source].[Name],
				   [Target].[Value]		= [Source].[Value]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Type]
				,[Name]
				,[Value])
		VALUES ([Source].[Type]
				,[Source].[Name]
				,[Source].[Value])
	OUTPUT Inserted.Id
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

CREATE PROCEDURE [dbo].[Template_Delete]
	@Id		INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	DELETE FROM TWTemplates
	WHERE [Id] = @Id

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[Template_Get]
	@Id		INT = NULL
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	SELECT * FROM TWTemplates
	WHERE	 [Id] = ISNULL(@Id, [Id])

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO