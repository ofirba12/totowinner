﻿USE TotoWinner
GO

CREATE TABLE [dbo].[TWBacktestingBaseBets]
(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[GameIndex]		TINYINT		NOT NULL,
	[Win_1]			BIT			NOT NULL,
	[Win_X]			BIT			NOT NULL,
	[Win_2]			BIT			NOT NULL,
	[Inserted]		DATETIME	NOT NULL DEFAULT getdate()
)
GO

ALTER TABLE [dbo].[TWBacktestingBaseBets]
	ADD CONSTRAINT [PK_TWBacktestingBaseBets]
	PRIMARY KEY ([ExecutionId], [ProgramId], [GameIndex])

GO

CREATE TYPE [dbo].[TWBacktestingBaseBetsTVP] AS TABLE(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[GameIndex]		TINYINT		NOT NULL,
	[Win_1]			BIT			NOT NULL,
	[Win_X]			BIT			NOT NULL,
	[Win_2]			BIT			NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[ExecutionId],[ProgramId],[GameIndex] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)

GO
CREATE PROCEDURE [dbo].[InsertBacktestingBaseBets]
	@BaseBets AS [TWBacktestingBaseBetsTVP] READONLY
AS
	INSERT INTO [TWBacktestingBaseBets]([ExecutionId], [ProgramId], [GameIndex], [Win_1], [Win_X], [Win_2])
	SELECT [ExecutionId], [ProgramId], [GameIndex], [Win_1], [Win_X], [Win_2] FROM @BaseBets;
RETURN 0
GO

ALTER TABLE TWExecution
ADD [SystemType] TINYINT NOT NULL
 CONSTRAINT D_TWExecution_SystemType
    DEFAULT (2)
WITH VALUES
GO

ALTER PROCEDURE [dbo].[Execution_StatusMerge]
	@ExecutionId	INT,
	@SystemType		TINYINT,
	@Status			SMALLINT,
	@Parameters		NVARCHAR(2000),
	@Results		NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWExecution] AS [Target]
	USING ( SELECT	 @ExecutionId	AS	"ExecutionId"
					,@SystemType	AS	"SystemType"
					,@Status		AS	"Status"
					,@Parameters	AS	"Parameters"
					,@Results		AS	"Results"
	) AS [Source]	ON [Source].[ExecutionId] = [Target].[ExecutionId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Status]		= [Source].[Status], 
				   [Target].[Results]		= [Source].[Results],
				   [Target].[LastUpdate]	= GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Status]
				,[Parameters]				
				,[Results]
				,[Inserted]
				,[LastUpdate]
				,[SystemType]
				)
		VALUES ([Source].[Status]
				,[Source].[Parameters]
				,NULL
				,GetDate()
				,GetDate()
				,[Source].[SystemType]
				)
	OUTPUT Inserted.ExecutionId
	;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

CREATE TABLE [dbo].[TWBacktestingResults]
(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[BetStrike]		TINYINT		NOT NULL,
	[Counter]		INT			NOT NULL,
	[Inserted]		DATETIME	NOT NULL DEFAULT getdate()
)
GO
ALTER TABLE [dbo].[TWBacktestingResults]
	ADD CONSTRAINT [PK_TWBacktestingResults]
	PRIMARY KEY ([ExecutionId],[ProgramId],[BetStrike])
GO
ALTER TABLE [dbo].[TWBacktestingResults]
	ADD CONSTRAINT [CHK_TWBacktestingResults_BetStrike]
	CHECK (BetStrike >= 0 AND BetStrike <=16)
GO
--DROP TYPE [dbo].[TWBacktestingResultsTVP]
CREATE TYPE [dbo].[TWBacktestingResultsTVP] AS TABLE(
	[ExecutionId]	INT			NOT NULL,
	[ProgramId]		INT			NOT NULL,
	[BetStrike]		TINYINT		NOT NULL,
	[Counter]		INT			NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[ExecutionId],[ProgramId],[BetStrike] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[InsertBacktestingResults]
	@BacktestingResults AS [TWBacktestingResultsTVP] READONLY
AS
	INSERT INTO [TWBacktestingResults]([ExecutionId], [ProgramId], [BetStrike], [Counter])
	SELECT [ExecutionId], [ProgramId], [BetStrike], [Counter] FROM @BacktestingResults;
RETURN 0
GO