﻿USE TotoWinner
GO

DROP TABLE [dbo].[TWSystems]
GO

CREATE TABLE [dbo].[TWSystems]
(
	[Id]			INT					IDENTITY(1,1) NOT NULL,
	[Type]			SMALLINT			NOT NULL,
    [Name]			NVARCHAR (150)		NOT NULL,
	[ProgramId]		INT					NOT NULL,
	[ProgramType]	INT					NOT NULL,
	[ReferenceId]	INT					NULL,
	[LastUpdate]	DATETIME			NOT NULL,
	[RawData]		NVARCHAR(MAX)		NOT NULL
)
GO

ALTER TABLE [dbo].[TWSystems]
	ADD CONSTRAINT [PK_TWSystem]
	PRIMARY KEY ([Id], [Type], [Name])
GO

ALTER PROCEDURE [dbo].[System_Merge]
--	@Id				INT,
	@Type			SMALLINT,
    @Name			NVARCHAR (150),
	@ProgramId		INT,
	@ProgramType	INT,
--	@ReferenceId	INT,
	@RawData		NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWSystems] AS [Target]
	USING ( SELECT	 --@Id			AS	"Id",
					@Type			AS	"Type"
					,@Name			AS	"Name"
					,@ProgramId		AS	"ProgramId"
					,@ProgramType	AS	"ProgramType"
--					,@ReferenceId	AS  "ReferenceId"
					,@RawData		AS	"RawData"
	) AS [Source]	ON [Source].[Name] = [Target].[Name]
					AND	[Source].[Type] = [Target].[Type]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Type]			= [Source].[Type], 
				   [Target].[Name]			= [Source].[Name],
				   [Target].[ProgramId]		= [Source].[ProgramId],
				   [Target].[ProgramType]	= [Source].[ProgramType],
	--			   [Target].[ReferenceId]	= [Source].[ReferenceId],
				   [Target].[RawData]		= [Source].[RawData],
				   [Target].[LastUpdate] = GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (--[Id],
				 [Type]
				,[Name]
				,[ProgramId]
				,[ProgramType]
				,[ReferenceId]
				,[RawData]
				,[LastUpdate])
		VALUES ([Source].[Type]
				,[Source].[Name]
				,[Source].[ProgramId]
				,[Source].[ProgramType]
				,NULL
				,[Source].[RawData]
				,GetDate())
	OUTPUT Inserted.Id
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

CREATE PROCEDURE [dbo].[ShrinkUpdateReference]
	@ShrinkSystemId		INT,
	@OptimizerSystemId	INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	UPDATE [dbo].[TWSystems]
	SET [ReferenceId] = @OptimizerSystemId
	WHERE Id = @ShrinkSystemId

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO

ALTER PROCEDURE [dbo].[System_Delete]
	@Id		INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	BEGIN TRAN
	
	DELETE FROM TWSystems
	WHERE [Id] = @Id

	UPDATE TWSystems
	SET [ReferenceId]=NULL
	WHERE [ReferenceId] = @Id 

	COMMIT
END TRY
BEGIN CATCH
	ROLLBACK
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END