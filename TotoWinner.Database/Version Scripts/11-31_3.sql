﻿USE [TotoWinner]
GO
ALTER PROCEDURE [dbo].[Execution_StatusMerge]
	@ExecutionId	INT,
	@SystemType		TINYINT,
	@Status			SMALLINT,
	@Parameters		NVARCHAR(MAX),
	@Results		NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWExecution] AS [Target]
	USING ( SELECT	 @ExecutionId	AS	"ExecutionId"
					,@SystemType	AS	"SystemType"
					,@Status		AS	"Status"
					,@Parameters	AS	"Parameters"
					,@Results		AS	"Results"
	) AS [Source]	ON [Source].[ExecutionId] = [Target].[ExecutionId]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Status]		= [Source].[Status], 
				   [Target].[Results]		= [Source].[Results],
				   [Target].[LastUpdate]	= GetDate()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Status]
				,[Parameters]				
				,[Results]
				,[Inserted]
				,[LastUpdate]
				,[SystemType]
				)
		VALUES ([Source].[Status]
				,[Source].[Parameters]
				,NULL
				,GetDate()
				,GetDate()
				,[Source].[SystemType]
				)
	OUTPUT Inserted.ExecutionId
	;
END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
