﻿USE [TotoWinner]
GO

CREATE TABLE [dbo].[RealTimeRatesPipe]
(
	[ProgramType]	TINYINT			NOT NULL,
	[GameIndex]		TINYINT			NOT NULL,
	[Rate1]			FLOAT			NOT NULL,
	[RateX]			FLOAT			NOT NULL,
	[Rate2]			FLOAT			NOT NULL
)
GO

ALTER TABLE [dbo].[RealTimeRatesPipe]
	ADD CONSTRAINT [PK_RealTimeRatesPipe]
	PRIMARY KEY ([ProgramType],[GameIndex])
GO
CREATE TYPE [dbo].[RealTimeRatesPipeTVP] AS TABLE(
	[ProgramType]	TINYINT			NOT NULL,
	[GameIndex]		TINYINT			NOT NULL,
	[Rate1]			FLOAT			NOT NULL,
	[RateX]			FLOAT			NOT NULL,
	[Rate2]			FLOAT			NOT NULL
	PRIMARY KEY CLUSTERED 
(
	[ProgramType],[GameIndex] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
CREATE PROCEDURE [dbo].[RealTimeRatesPipe_Merge]
	@Rates AS [RealTimeRatesPipeTVP] READONLY
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);

	MERGE dbo.[RealTimeRatesPipe] AS [Target]
	USING @Rates AS [Source]	
		ON [Source].[ProgramType] = [Target].[ProgramType]
			AND	[Source].[GameIndex] = [Target].[GameIndex]
	WHEN MATCHED THEN 
		UPDATE SET [Target].[Rate1]	= [Source].[Rate1], 
				   [Target].[RateX]	= [Source].[RateX],
				   [Target].[Rate2]	= [Source].[Rate2]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [ProgramType]
				,[GameIndex]
				,[Rate1]
				,[RateX]
				,[Rate2])
		VALUES ( [Source].[ProgramType]
				,[Source].[GameIndex]
				,[Source].[Rate1]
				,[Source].[RateX]
				,[Source].[Rate2])
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[RealTimeRatesPipe_Get]
@ProgramType	TINYINT
AS
	SELECT * FROM [RealTimeRatesPipe]
	WHERE [ProgramType]	= @ProgramType
RETURN 0

Go
DELETE FROM [RealTimeRatesPipe]
GO
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,1,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,2,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,3,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,4,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,5,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,6,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,7,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,8,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,9,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,10,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,11,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,12,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,13,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,14,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,15,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (96,16,1,1,1)
GO
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,1,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,2,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,3,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,4,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,5,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,6,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,7,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,8,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,9,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,10,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,11,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,12,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,13,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (82,14,1,1,1)
Go
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,1,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,2,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,3,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,4,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,5,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,6,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,7,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,8,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,9,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,10,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,11,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,12,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,13,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,14,1,1,1)
INSERT INTO [dbo].[RealTimeRatesPipe]([ProgramType],[GameIndex],[Rate1],[RateX],[Rate2])  VALUES (87,15,1,1,1)
go
