﻿USE [TotoWinner]
GO
CREATE TABLE [dbo].[TWUnionSystemResults]
(
	[Id]				INT					IDENTITY(1000,1) NOT NULL,
	[SystemIds]			NVARCHAR(100)		NOT NULL,
	[LastUpdate]		DATETIME			NOT NULL,
	[SMART]				NVARCHAR(1000)		NULL,
	[RawResultsData]	NVARCHAR(MAX)		NOT NULL
)
GO
ALTER TABLE [dbo].[TWUnionSystemResults]
	ADD CONSTRAINT [PK_TWUnionSystemResults]
	PRIMARY KEY ([Id])
GO
CREATE PROCEDURE [dbo].[UnionSystemResults_Merge]
	@Id				INT,			
	@SystemIds		NVARCHAR(100),
	@SMART			NVARCHAR(1000),			
	@RawResultsData	NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	MERGE dbo.[TWUnionSystemResults] AS [Target]
	USING ( SELECT	@Id					AS	"Id"
					,@SystemIds			AS	"SystemIds"
					,@SMART				AS	"SMART"
					,@RawResultsData	AS	"RawResultsData"
	) AS [Source]	ON [Source].[Id] = [Target].[Id]
	WHEN MATCHED THEN 
		UPDATE SET --[Target].[SystemIds]			= [Source].[SystemIds], 
				   [Target].[LastUpdate]		= GetDate(),
				   [Target].[SMART]				= [Source].[SMART]
				   --[Target].[RawResultsData]	= [Source].[RawResultsData]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [SystemIds]
				,[SMART]
				,[RawResultsData]
				,[LastUpdate])
		VALUES ([Source].[SystemIds]
				,[Source].[SMART]
				,[Source].[RawResultsData]
				,GetDate())
	OUTPUT Inserted.Id
	;

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END
GO
CREATE PROCEDURE [dbo].[UnionSystemResults_Get]
	@Id		INT
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ErrMSG VARCHAR(1000);
	
	SELECT * FROM TWUnionSystemResults
	WHERE	 [Id] = @Id

END TRY
BEGIN CATCH
	SET @ErrMSG = ERROR_MESSAGE();
	RAISERROR(@ErrMsg, 16, 1);
END CATCH
RETURN 0
END