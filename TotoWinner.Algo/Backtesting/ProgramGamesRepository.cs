﻿//using System;
//using System.Collections.Generic;
//using TotoWinner.Data;

//namespace TotoWinner.Algo.Backtesting
//{
//    internal class ProgramGamesRepository
//    {
//        public Dictionary<BtWinType, SortedDictionary<double, BtBaseGame>> GamesRepoLow { get; private set; }
//        public Dictionary<BtWinType, SortedDictionary<double, BtBaseGame>> GamesRepoHigh { get; private set; }

//        public ProgramGamesRepository(ProgramWithGames programWithGames)
//        {
//            try
//            {
//                this.GamesRepoHigh = new Dictionary<BtWinType, SortedDictionary<double, BtBaseGame>>();
//                this.GamesRepoLow = new Dictionary<BtWinType, SortedDictionary<double, BtBaseGame>>();

//                this.init(programWithGames);
//            }
//            catch(Exception ex)
//            {
//                throw new System.Exception(
//                    $"An error occured trying to construct type {typeof(ProgramGamesRepository)}; programWithGames=[Id: {programWithGames.Id}, number: {programWithGames.Number}, endDate: {programWithGames.EndDate}]",
//                    ex);
//            }
//        }
//        private void init(ProgramWithGames program)
//        {
//            this.GamesRepoHigh.Add(BtWinType.HomeWin, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
//            this.GamesRepoHigh.Add(BtWinType.GuestWin, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
//            this.GamesRepoHigh.Add(BtWinType.BreakEven, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
//            this.GamesRepoHigh.Add(BtWinType.Any, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));

//            this.GamesRepoLow.Add(BtWinType.HomeWin, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
//            this.GamesRepoLow.Add(BtWinType.GuestWin, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
//            this.GamesRepoLow.Add(BtWinType.BreakEven, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
//            this.GamesRepoLow.Add(BtWinType.Any, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
//            foreach (var game in program.Games)
//            {
//                var baseGame = new BtBaseGame(game.Value.Index, game.Value.Rate1, game.Value.RateX, game.Value.Rate2);
//                this.GamesRepoHigh[BtWinType.HomeWin].Add(baseGame.Rate1, baseGame);
//                this.GamesRepoHigh[BtWinType.BreakEven].Add(baseGame.RateX, baseGame);
//                this.GamesRepoHigh[BtWinType.GuestWin].Add(baseGame.Rate2, baseGame);
//                this.GamesRepoHigh[BtWinType.Any].Add(Math.Max(Math.Max(baseGame.Rate2, baseGame.Rate1), baseGame.RateX), baseGame);
//                this.GamesRepoLow[BtWinType.HomeWin].Add(baseGame.Rate1, baseGame);
//                this.GamesRepoLow[BtWinType.BreakEven].Add(baseGame.RateX, baseGame);
//                this.GamesRepoLow[BtWinType.GuestWin].Add(baseGame.Rate2, baseGame);
//                this.GamesRepoLow[BtWinType.Any].Add(Math.Min(Math.Min(baseGame.Rate2, baseGame.Rate1), baseGame.RateX), baseGame);
//            }

//        }
//        public class DuplicateDescendingKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
//        {
//            #region IComparer<TKey> Members

//            public int Compare(TKey x, TKey y)
//            {
//                int result = x.CompareTo(y);

//                if (result == 0)
//                    return 1;   // Handle equality as beeing greater
//                else
//                    return -result;
//            }

//            #endregion
//        }
//        public class DuplicateAscendingKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
//        {
//            #region IComparer<TKey> Members

//            public int Compare(TKey x, TKey y)
//            {
//                int result = x.CompareTo(y);

//                if (result == 0)
//                    return 1;   // Handle equality as beeing greater
//                else
//                    return result;
//            }

//            #endregion
//        }

//    }
//}
