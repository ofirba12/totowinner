﻿//using log4net;
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Threading;
//using TotoWinner.Data;
//using TotoWinner.Algo.Backtesting;

//namespace TotoWinner.Algo
//{
//    public class BacktestingAlgo
//    {
//        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

//        private Services.ExecutionServices _executionSrv = Services.ExecutionServices.Instance;
//        private Services.CalculationServices _calculationSrv = Services.CalculationServices.Instance;
//        private Services.SystemsServices _systemSrv = Services.SystemsServices.Instance;
//        private Services.ProgramServices _programSrv = Services.ProgramServices.Instance;
//        private Services.PersistanceServices _persistanceSrv = Services.PersistanceServices.Instance;

//        #region Main
//        public void RunAlgo(CancellationToken token, BacktestingExecutionData work, bool useInMemoryPersistance, bool matchBaseToFinalResultOnly)
//        {
//            try
//            {
//                work.Definitions.FromDate = work.Definitions.FromDate.ToLocalTime();
//                work.Definitions.ToDate = work.Definitions.ToDate.ToLocalTime();
//                var programs = _programSrv.GetPersistanceProgramsWithGamesData(work.Definitions.ProgramType, 
//                    work.Definitions.FromDate, work.Definitions.ToDate, useInMemoryPersistance);
//                var programCounter = 0;
//                do
//                {
//                    var program = programs[programCounter];
//                    bool isOpenForBets = program.Games.Values.Any(game => game.OpenForBets == true);
//                    if (isOpenForBets)
//                    {
//                        programCounter++;
//                        continue;
//                    }
//                    bool isInvalidProgram = program.Games.Values.Any(game => game.Validations.Count > 0);
//                    if (isInvalidProgram)
//                    {
//                        _log.Info($"Program id={program.Id}, number={program.Number} is not valid for backtesting. skipping");
//                        var emptyStrikeResults = GenerateEmptyBacktetingStrikesResult(work.ExecutionId, program, BtProgramResultStatus.InvalidProgram);
//                        _persistanceSrv.AddBacktestingResults(work.ExecutionId, program.Id, new Dictionary<short, Bet>(), emptyStrikeResults, useInMemoryPersistance);
//                        programCounter++;
//                        continue;
//                    }
//                    var baseBets = GenerateBaseBets(program, work.Definitions);
//                    if (useInMemoryPersistance)
//                    {
//                        if (baseBets.Count == 0)
//                            _log.Info($"Program id={program.Id}, number={program.Number}: no base Bets");
//                        else
//                            _log.Info($"Program id={program.Id}, number={program.Number}: have base Bets");
//                    }
//                    var betsCounterNumber = CountNumberOfBets(baseBets.Values.ToList());
//                    if (betsCounterNumber > 3000000)
//                    {
//                        _log.Info($"Program id={program.Id}, number={program.Number} has too many bets {betsCounterNumber} over 3 million. skipping");
//                        var emptyStrikeResults = GenerateEmptyBacktetingStrikesResult(work.ExecutionId, program, BtProgramResultStatus.TooManyBets);
//                        _persistanceSrv.AddBacktestingResults(work.ExecutionId, program.Id, new Dictionary<short, Bet>(), emptyStrikeResults, useInMemoryPersistance);
//                        programCounter++;
//                        continue;
//                    }
//                    var strikeResults = CalculatingBacktestingStrikes(work.ExecutionId, program, baseBets, token, matchBaseToFinalResultOnly);
//                    if (token.IsCancellationRequested)
//                    {
//                        _log.Info($"Execution {work.ExecutionId} stopped after CalculatingBacktestingStrikes for program {program.Number}");
//                        break;
//                    }
//                    _persistanceSrv.AddBacktestingResults(work.ExecutionId, program.Id, baseBets, strikeResults, useInMemoryPersistance);
//                    programCounter++;
//                }
//                while (!token.IsCancellationRequested && programCounter < programs.Count);
//                if (programCounter == programs.Count)
//                {
//                    _executionSrv.UpdateExecutionStatus<BacktestingResults>(work.ExecutionId, SystemType.Backtesting, ExecutionStatus.Finished, null, useInMemoryPersistance);
//                    _log.Info($"Execution {work.ExecutionId} finished successfully.");
//                }
//                else if (token.IsCancellationRequested)
//                {
//                    _executionSrv.UpdateExecutionStatus<BacktestingResults>(work.ExecutionId, SystemType.Backtesting, ExecutionStatus.Stopped, null, useInMemoryPersistance);
//                    _log.Info($"Execution {work.ExecutionId} stopped by client!!!");
//                }
//            }
//            catch (Exception ex)
//            {
//                _log.Fatal($"An error occurred trying to run backtesting algo on execution id: {work.ExecutionId}", ex);
//                _executionSrv.UpdateExecutionStatus<BacktestingResults>(work.ExecutionId, SystemType.Backtesting, Data.ExecutionStatus.Error, null, useInMemoryPersistance);
//            }
//        }
//        #endregion

//        #region Algo

//        private Dictionary<short, BacktestingStrikeResult> CalculatingBacktestingStrikes(int executionId, ProgramWithGames program,
//            Dictionary<short, Bet> baseBets, CancellationToken token, bool matchBaseToFinalResultOnly)
//        {
//            var results = new Dictionary<short, BacktestingStrikeResult>();
//            if (baseBets.Count == 0)
//            {
//                for (var index = 0; index <= program.Games.Count; index++)
//                {
//                    results.Add((short)index,
//                        new BacktestingStrikeResult() { StrikeBet = (short)index, ProgramId = program.Id, ExecutionId = executionId, Counter = 0 });
//                }

//            }
//            else
//            {
//                var gamesFinalResults = program.Games.ToDictionary(x => x.Key, y =>
//                {
//                    byte won = 0;
//                    switch (y.Value.Won)
//                    {
//                        case "1":
//                            won = 0;
//                            break;
//                        case "X":
//                            won = 1;
//                            break;
//                        case "2":
//                            won = 2;
//                            break;
//                    }
//                    return won;
//                });
//                var strikeCounters = new Dictionary<int, int>();
//                for (var index = 0; index <= program.Games.Count; index++)
//                    strikeCounters.Add(index, 0);
//                //stopwatch.Stop();                
//                //_log.Info($"Calculation time in seconds={TimeSpan.FromMilliseconds(stopwatch.ElapsedMilliseconds).Seconds} for program={program.Number} {program.EndDate}");
//                //stopwatch.Restart();
//                if (matchBaseToFinalResultOnly)
//                {
//                    var strike = 0;
//                    for (short gameInd = 1; gameInd <= baseBets.Keys.Count; gameInd++)
//                    {
//                        var win1 = gamesFinalResults[gameInd] == 0;
//                        var winx = gamesFinalResults[gameInd] == 1;
//                        var win2 = gamesFinalResults[gameInd] == 2;
//                        if ((win1 && baseBets[gameInd].bet1 == win1) ||
//                            (winx && baseBets[gameInd].betX == winx) ||
//                            (win2 && baseBets[gameInd].bet2 == win2))
//                            strike++;
//                    }
//                    strikeCounters[strike]++;
//                }
//                else
//                {
//                    var calcRequest = PrepareCalculationRequest(baseBets, program);
//                    calcRequest.Validate();
//                    var constrSettings = _calculationSrv.BuildConstrSettings(calcRequest);
//                    var probSettings = _calculationSrv.BuildProbSettings(calcRequest);
//                    var validators = _calculationSrv.BuildValidators(calcRequest, probSettings);
//                    var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, false);
//                    //var stopwatch = new Stopwatch();
//                    //stopwatch.Start();
//                    var response = _calculationSrv.CalculateBets(betInputs);
//                    foreach (var finalResult in response.FinalResults)
//                    {
//                        if (token.IsCancellationRequested)
//                        {
//                            _log.Info($"Execution {executionId} stopped while matching for program {program.Number}");
//                            break;
//                        }

//                        var strike = 0;
//                        for (var gameInd = 0; gameInd < finalResult.Result.Length; gameInd++)
//                        {
//                            if (finalResult.Result[gameInd] == gamesFinalResults[gameInd + 1])
//                                strike++;
//                        }
//                        strikeCounters[strike]++;
//                    }
//                }
//                //stopwatch.Stop();
//                //_log.Info($"Matching time in seconds={TimeSpan.FromMilliseconds(stopwatch.ElapsedMilliseconds).Seconds} for program={program.Number} {program.EndDate}");
//                foreach (var counter in strikeCounters)
//                {
//                    results.Add((short)counter.Key,
//                        new BacktestingStrikeResult() { StrikeBet = (short)counter.Key, ProgramId = program.Id, ExecutionId = executionId, Counter = counter.Value });
//                }
//            }
//            //results.Add(16, new BacktestingStrikeResult() { StrikeBet = 16, ProgramId = program.Id, ExecutionId = executionId, Counter = 0/*1 */});
//            //results.Add(10, new BacktestingStrikeResult() { StrikeBet = 10, ProgramId = program.Id, ExecutionId = executionId, Counter = 0/*10 */});
//            //results.Add(9, new BacktestingStrikeResult() { StrikeBet = 9, ProgramId = program.Id, ExecutionId = executionId, Counter = 0/*program.Id % 16*/ });
//            return results;
//        }

//        private BetsFormCreateRequest PrepareCalculationRequest(Dictionary<short, Bet> baseBets, ProgramWithGames program)
//        {
//            var request = new BetsFormCreateRequest();
//            request.EventsRates = new List<RatesLine>();
//            request.EventsBets = new List<Bet>();
//            var numberOfGames = program.Games.Count;
//            for (var index = 1; index <= numberOfGames; index++)
//            {
//                request.EventsRates.Add(new RatesLine()
//                {
//                    position = index - 1,
//                    rate1 = (float)program.Games[index].Rate1,
//                    rateX = (float)program.Games[index].RateX,
//                    rate2 = (float)program.Games[index].Rate2
//                });
//                request.EventsBets.Add(new Bet()
//                {
//                    position = index - 1,
//                    bet1 = baseBets[(short)index].bet1,
//                    betX = baseBets[(short)index].betX,
//                    bet2 = baseBets[(short)index].bet2
//                });
//            }
//            request.FormSettings = new FormSettings();
//            request.FormSettings.IncludeSettings = new IncludeSettings();
//            request.FormSettings.IncludeSettings.amountSum = new MinMax() { Min = numberOfGames, Max = 100 };
//            request.FormSettings.IncludeSettings.amount1 = new MinMax() { Min = 0, Max = numberOfGames };
//            request.FormSettings.IncludeSettings.amountX = new MinMax() { Min = 0, Max = numberOfGames };
//            request.FormSettings.IncludeSettings.amount2 = new MinMax() { Min = 0, Max = numberOfGames };

//            request.FormSettings.IncludeSettings.sequenceBreak = new MinMax() { Min = 0, Max = numberOfGames - 1 };
//            request.FormSettings.IncludeSettings.sequenceX = new MinMax() { Min = 0, Max = numberOfGames };
//            request.FormSettings.IncludeSettings.sequence1 = new MinMax() { Min = 0, Max = numberOfGames };
//            request.FormSettings.IncludeSettings.sequence2 = new MinMax() { Min = 0, Max = numberOfGames };

//            return request;
//        }

//        private Dictionary<short, BacktestingStrikeResult> GenerateEmptyBacktetingStrikesResult(int executionId, ProgramWithGames program, BtProgramResultStatus status)
//        {
//            var results = new Dictionary<short, BacktestingStrikeResult>();
//            results.Add(16, new BacktestingStrikeResult() { StrikeBet = 0, ProgramId = program.Id, ExecutionId = executionId, Counter = (int)status });
//            return results;
//        }

//        public Dictionary<short, Bet> GenerateBaseBets(ProgramWithGames program, BacktestingDefinitions definitions)
//        {
//            var gamesRepository = new ProgramGamesRepository(program);
//            var bets = new Dictionary<short, Bet>();
//            foreach (var definition in definitions.GamesDefinitions)
//            {
//                var rateType = ConvertToRateType(definition.RateType);
//                var betForType = ConvertToBetForType(definition.BetForType);
//                var gamesRepo = rateType == BtRateType.Low
//                    ? gamesRepository.GamesRepoLow[betForType]
//                    : gamesRepository.GamesRepoHigh[betForType];
//                var bankers = gamesRepo.Values.Count(item => item.Won.Count == 1);
//                var doubles = gamesRepo.Values.Count(item => item.Won.Count == 2);

//                switch (definition.RowType)
//                {
//                    case BtGameDefinitionType.Banker:
//                        var btBankerGameDef = new BtBankerGameDefinition(
//                            ConvertToBankerBetType(definition.BetType),
//                            rateType,
//                            betForType,
//                            definition.RateFrom, definition.RateTo,
//                            new MinMax(Convert.ToDecimal(definition.From), Convert.ToDecimal(definition.To))
//                            );
//                        HandleBankersDefinition(btBankerGameDef, definitions.BankersLimits, gamesRepo, bankers);
//                        break;
//                    case BtGameDefinitionType.Double:
//                        var btDoubleGameDef = new BtDoubleGameDefinition(
//                            ConvertToDoubleBetType(definition.BetType),
//                            rateType,
//                            betForType,
//                            definition.RateFrom, definition.RateTo,
//                            new MinMax(Convert.ToDecimal(definition.From), Convert.ToDecimal(definition.To))
//                            );
//                        HandleDoublesDefinition(btDoubleGameDef, definitions.DoublesLimits, gamesRepo, doubles);
//                        break;
//                }
//            }
//            var bankersCounter = gamesRepository.GamesRepoLow[BtWinType.Any].Values.Count(item => item.Won.Count == 1);
//            var doublesCounter = gamesRepository.GamesRepoLow[BtWinType.Any].Values.Count(item => item.Won.Count == 2);
//            if ((bankersCounter >= definitions.BankersLimits.Min && bankersCounter <= definitions.BankersLimits.Max) &&
//                (doublesCounter >= definitions.DoublesLimits.Min && doublesCounter <= definitions.DoublesLimits.Max))
//            {
//                foreach (var kpv in gamesRepository.GamesRepoLow[BtWinType.Any])
//                {
//                    if (kpv.Value.Won.Count == 0)
//                        kpv.Value.MarkTriple();
//                }
//                bets = GenerateBaseBetsResults(gamesRepository.GamesRepoLow[BtWinType.Any].Values.ToList());
//            }
//            return bets;
//        }

//        private void HandleDoublesDefinition(BtDoubleGameDefinition btGameDef, MinMaxLimits limits,
//            SortedDictionary<double, BtBaseGame> gamesRepo, int betsCounter)
//        {
//            int internalCounter = 0;
//            var tempRepo = new Dictionary<BtBaseGame, char[]>();
//            foreach (var kpv in gamesRepo)
//            {
//                if (betsCounter >= limits.Max)
//                    break;
//                if (kpv.Value.Won.Count > 0)
//                    continue;
//                var inRange = false;
//                //var inRange1 = kpv.Value.Rate1 >= btGameDef.FromRate && kpv.Value.Rate1 <= btGameDef.ToRate;
//                //var inRangeX = kpv.Value.RateX >= btGameDef.FromRate && kpv.Value.RateX <= btGameDef.ToRate;
//                //var inRange2 = kpv.Value.Rate2 >= btGameDef.FromRate && kpv.Value.Rate2 <= btGameDef.ToRate;
//                double chosenValue = -1;
//                if (btGameDef.BetForType == BtWinType.Any)
//                {
//                    if (btGameDef.RateType == BtRateType.Low)
//                    {
//                        chosenValue = Math.Min(kpv.Value.Rate1, Math.Min(kpv.Value.Rate2, kpv.Value.RateX));
//                    }
//                    else if (btGameDef.RateType == BtRateType.High)
//                    {
//                        chosenValue = Math.Max(kpv.Value.Rate1, Math.Max(kpv.Value.Rate2, kpv.Value.RateX));
//                    }
//                    //inRange = inRange1 || inRangeX || inRange2;
//                }
//                else
//                    chosenValue = kpv.Key;
//                    //inRange = kpv.Key >= btGameDef.FromRate && kpv.Key <= btGameDef.ToRate;

//                inRange = chosenValue >= btGameDef.FromRate && chosenValue <= btGameDef.ToRate;
//                if (inRange)
//                {
//                    char winCharFirst = ' ';
//                    char winCharSecond = ' ';
//                    switch (btGameDef.BetType)
//                    {
//                        case BtDoubleBetType.Bet_1X:
//                            winCharFirst = '1';
//                            winCharSecond = 'X';
//                            break;
//                        case BtDoubleBetType.Bet_12:
//                            winCharFirst = '1';
//                            winCharSecond = '2';
//                            break;
//                        case BtDoubleBetType.Bet_2X:
//                            winCharFirst = '2';
//                            winCharSecond = 'X';
//                            break;
//                        case BtDoubleBetType.Any:
//                            if (btGameDef.RateType == BtRateType.Low)
//                            {
//                                //1==2,1==X==2,1==X => 2X
//                                //2==X => 1X
//                                //1 Loweser => 2X
//                                //X Loweser => 12
//                                //2 Loweser => 1X
//                                if (kpv.Value.Rate1 <= kpv.Value.RateX && kpv.Value.Rate1 <= kpv.Value.Rate2)
//                                {
//                                    winCharFirst = '2';
//                                    winCharSecond = 'X';
//                                }
//                                else if (kpv.Value.RateX <= kpv.Value.Rate1 && kpv.Value.RateX <= kpv.Value.Rate2)
//                                {
//                                    winCharFirst = '1';
//                                    if (kpv.Value.RateX == kpv.Value.Rate2)
//                                        winCharSecond = 'X';
//                                    else
//                                        winCharSecond = '2';
//                                }
//                                else if (kpv.Value.Rate2 <= kpv.Value.Rate1 && kpv.Value.Rate2 <= kpv.Value.RateX)
//                                {
//                                    winCharFirst = '1';
//                                    winCharSecond = 'X';
//                                }
//                            }
//                            else if (btGameDef.RateType == BtRateType.High)
//                            {
//                                //1==2,1==X==2,2==X => 1X
//                                //1==X => 2X
//                                //1 Biggest => 2X
//                                //X Biggest => 12
//                                //2 Biggest => 1X
//                                if (kpv.Value.Rate1 >= kpv.Value.RateX && kpv.Value.Rate1 >= kpv.Value.Rate2)
//                                {
//                                    if (kpv.Value.Rate1 == kpv.Value.Rate2 ||
//                                        (kpv.Value.Rate1 == kpv.Value.Rate2 && kpv.Value.Rate1 == kpv.Value.RateX))
//                                        winCharFirst = '1';
//                                    else
//                                        winCharFirst = '2';
//                                    winCharSecond = 'X';
//                                }
//                                else if (kpv.Value.RateX >= kpv.Value.Rate1 && kpv.Value.RateX >= kpv.Value.Rate2)
//                                {
//                                    winCharFirst = '1';
//                                    if (kpv.Value.RateX == kpv.Value.Rate2)
//                                        winCharSecond = 'X';
//                                    else if (kpv.Value.RateX == kpv.Value.Rate1)
//                                    {
//                                        winCharFirst = '2';
//                                        winCharSecond = 'X';
//                                    }
//                                    else
//                                        winCharSecond = '2';
//                                }
//                                else if (kpv.Value.Rate2 >= kpv.Value.Rate1 && kpv.Value.Rate2 >= kpv.Value.RateX)
//                                {
//                                    winCharFirst = '1';
//                                    winCharSecond = 'X';
//                                }
//                            }
//                            break;
//                    }
//                    //kpv.Value.MarkDouble(winCharFirst, winCharSecond);
//                    tempRepo.Add(kpv.Value, new char[] { winCharFirst, winCharSecond });
//                    betsCounter++;
//                    internalCounter++;
//                }
//            }
//            if (internalCounter >= btGameDef.MinMax.Min)// && internalCounter <= btGameDef.MinMax.Max)
//            {
//                var counter = 0;
//                foreach (var kpv in tempRepo)
//                {
//                    if (counter >= btGameDef.MinMax.Max)
//                        break;
//                    kpv.Key.MarkDouble(kpv.Value[0], kpv.Value[1]);
//                    counter++;
//                }
//            }
//        }

//        private void HandleBankersDefinition(BtBankerGameDefinition btGameDef, MinMaxLimits limits,
//            SortedDictionary<double, BtBaseGame> gamesRepo, int betsCounter)
//        {
//            int internalCounter = 0;
//            var tempRepo = new Dictionary<BtBaseGame, char>();
//            foreach (var kpv in gamesRepo)
//            {
//                if (betsCounter >= limits.Max)
//                    break;
//                if (kpv.Value.Won.Count > 0)
//                    continue;
//                //var inRange = false;
//                //if (btGameDef.BetForType == BtWinType.Any)
//                //{
//                //    var inRange1 = kpv.Value.Rate1 >= btGameDef.FromRate && kpv.Value.Rate1 <= btGameDef.ToRate;
//                //    var inRangeX = kpv.Value.RateX >= btGameDef.FromRate && kpv.Value.RateX <= btGameDef.ToRate;
//                //    var inRange2 = kpv.Value.Rate2 >= btGameDef.FromRate && kpv.Value.Rate2 <= btGameDef.ToRate;
//                //    inRange = inRange1 || inRangeX || inRange2;
//                //}
//                //else
//                //{
//                //    inRange = kpv.Key >= btGameDef.FromRate && kpv.Key <= btGameDef.ToRate;
//                //}
//                var inRange = false;
//                double chosenValue = -1;
//                if (btGameDef.BetForType == BtWinType.Any)
//                {
//                    if (btGameDef.RateType == BtRateType.Low)
//                    {
//                        chosenValue = Math.Min(kpv.Value.Rate1, Math.Min(kpv.Value.Rate2, kpv.Value.RateX));
//                    }
//                    else if (btGameDef.RateType == BtRateType.High)
//                    {
//                        chosenValue = Math.Max(kpv.Value.Rate1, Math.Max(kpv.Value.Rate2, kpv.Value.RateX));
//                    }
//                    //inRange = inRange1 || inRangeX || inRange2;
//                }
//                else
//                    chosenValue = kpv.Key;
//                //inRange = kpv.Key >= btGameDef.FromRate && kpv.Key <= btGameDef.ToRate;

//                inRange = chosenValue >= btGameDef.FromRate && chosenValue <= btGameDef.ToRate;
//                if (inRange)
//                {
//                    char winChar = ' ';
//                    switch (btGameDef.BetForType)
//                    {
//                        case BtWinType.HomeWin:
//                            winChar = '1';
//                            break;
//                        case BtWinType.BreakEven:
//                            winChar = 'X';
//                            break;
//                        case BtWinType.GuestWin:
//                            winChar = '2';
//                            break;
//                        case BtWinType.Any:
//                            if (kpv.Key == kpv.Value.Rate1)
//                                winChar = '1';
//                            else if (kpv.Key == kpv.Value.RateX)
//                                winChar = 'X';
//                            else if (kpv.Key == kpv.Value.Rate2)
//                                winChar = '2';
//                            break;
//                    }
//                    tempRepo.Add(kpv.Value, winChar);
//                    betsCounter++;
//                    internalCounter++;
//                }
//            }
//            if (internalCounter >= btGameDef.MinMax.Min)// && internalCounter <= btGameDef.MinMax.Max)
//            {
//                var counter = 0;
//                foreach (var kpv in tempRepo)
//                {
//                    if (counter >= btGameDef.MinMax.Max)
//                        break;
//                    kpv.Key.MarkBanker(kpv.Value);
//                    counter++;
//                }
//            }

//        }

//        private Dictionary<short, Bet> GenerateBaseBetsResults(List<BtBaseGame> games)
//        {
//            var bets = new Dictionary<short, Bet>();
//            var gameOrder = games.OrderBy(item => item.Index);
//            foreach (var game in gameOrder)
//            {
//                bets.Add((short)game.Index,
//                    new Bet((short)game.Index,
//                        game.Won.Contains('1'),
//                        game.Won.Contains('X'),
//                        game.Won.Contains('2'))
//                    );
//            }
//            return bets;
//        }
//        private BtBankerBetType ConvertToBankerBetType(string from)
//        {
//            switch (from)
//            {
//                case "תיקו":
//                    return BtBankerBetType.BreakEven;
//                case "רגיל":
//                    return BtBankerBetType.Regular;
//                default:
//                    throw new Exception($"{from} can not be converted to BtBankerBetType");
//            }
//        }
//        private BtDoubleBetType ConvertToDoubleBetType(string from)
//        {
//            switch (from)
//            {
//                case "1X":
//                    return BtDoubleBetType.Bet_1X;
//                case "12":
//                    return BtDoubleBetType.Bet_12;
//                case "2X":
//                    return BtDoubleBetType.Bet_2X;
//                case "חופשי":
//                    return BtDoubleBetType.Any;
//                default:
//                    throw new Exception($"{from} can not be converted to BtDoubleBetType");
//            }
//        }
//        private BtRateType ConvertToRateType(string from)
//        {
//            switch (from)
//            {
//                case "נמוך":
//                    return BtRateType.Low;
//                case "גבוה":
//                    return BtRateType.High;
//                //case "אמצע":
//                //    return BtRateType.MIddle;
//                default:
//                    throw new Exception($"{from} can not be converted to BtRateType");
//            }
//        }
//        private BtWinType ConvertToBetForType(string from)
//        {
//            switch (from)
//            {
//                case "קבוצה בית":
//                    return BtWinType.HomeWin;
//                case "תיקו":
//                    return BtWinType.BreakEven;
//                case "קבוצה חוץ":
//                    return BtWinType.GuestWin;
//                case "חופשי":
//                    return BtWinType.Any;
//                default:
//                    throw new Exception($"{from} can not be converted to BtWinType");
//            }
//        }
//        private int CountNumberOfBets(List<Bet> bets)
//        {
//            var trianglesCounter = 0;
//            var doublesCounter = 0;
//            foreach (var bet in bets)
//            {
//                var weight = Convert.ToInt32(bet.bet1) + Convert.ToInt32(bet.betX) + Convert.ToInt32(bet.bet2);
//                if (weight == 2)
//                    doublesCounter++;
//                else if (weight == 3)
//                    trianglesCounter++;
//            }
//            var betsCounterNumber = Math.Max(1, Math.Pow(3, trianglesCounter))
//                * Math.Max(1, Math.Pow(2, doublesCounter));

//            return Convert.ToInt32(betsCounterNumber);
//        }

//        #endregion
//    }
//}
