﻿//using log4net;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using TotoWinner.Data;

//namespace TotoWinner.Algo
//{
//    public class OptimizerAlgo
//    {
//        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

//        private Services.ExecutionServices _executionSrv = Services.ExecutionServices.Instance;
//        private Services.CalculationServices _calculationSrv = Services.CalculationServices.Instance;
//        private Services.SystemsServices _systemSrv = Services.SystemsServices.Instance;
//        #region Games Algo
//        public void RunOptimizerGamesAlgo(CancellationToken token, OptimizerExecutionData work)
//        {
//            try
//            {
//                var shrinkPayload = _systemSrv.LoadSystemForOptimizationV2(work.Parameters.OptimizedSystemId);

//                var calcRequest = new BetsFormCreateRequest()
//                {
//                    FormId = work.ExecutionId,
//                    FormSettings = shrinkPayload.Definitions.Form.FormSettings,
//                    EventsBets = shrinkPayload.Definitions.Form.EventsBets,
//                    EventsRates = shrinkPayload.Definitions.Form.EventsRates,
//                    CleanOnDiff = shrinkPayload.Definitions.Form.CleanOnDiff,
//                    GenerateOutputFile = false
//                };
//                var results0 = _calculationSrv.CalculateSystem(calcRequest);
//                _log.Info($"Base staring result: {results0.ResultsStatistics.Found}/{results0.FinalResults.Count()}");
//                List<OptimizerGamesMinIterationResult> minRes = new List<OptimizerGamesMinIterationResult>();
//                PersistBetsParentBaseResult(work.ExecutionId, calcRequest.EventsBets);
//                var targetReached = false;
//                List<Tuple<Bet, Bet>> iterations = null;
//                do
//                {
//                    iterations = PrepareGamesDataForNextIteration(calcRequest.EventsBets);
//                    if (iterations.Count > 0)
//                    {
//                        var minResult = RunGamesMainIterations(iterations, calcRequest, work.ExecutionId, token);
//                        if (!token.IsCancellationRequested && minResult != null)
//                        {
//                            minRes.Add(minResult);
//                            if (minResult.ShrinkResult.ResultsStatistics.Found > work.Parameters.TargetBetsCounter)
//                            {
//                                targetReached = true;
//                                minRes.Remove(minRes.Last());
//                            }
//                            else
//                            {
//                                _log.Info($"Min result in iteration: {minResult}");
//                                _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Running, minResult.ShrinkResult);
//                                _executionSrv.SetOptimizedBetResult(work.ExecutionId, new OptimizedBet(minResult.GameIndex, minResult.Str1X2, true));
//                                calcRequest.EventsBets[minResult.GameIndex - 1] = iterations[minResult.IterationIndex].Item1; //Set new Parent
//                            }
//                        }
//                        else if (minResult == null)
//                        {
//                            _log.Info("Iteration results were all zero");
//                        }
//                    }
//                }
//                while (!token.IsCancellationRequested && !targetReached && iterations.Count > 0);

//                if (targetReached)
//                {
//                    var finalShrinkRes = minRes.Count() > 0 ? minRes.Last().ShrinkResult : results0;
//                    _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Running, finalShrinkRes);
//                    finalShrinkRes = RunOptimizerConditionAlgo(work, token, calcRequest, finalShrinkRes);
//                    _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Finished, finalShrinkRes);
//                    _log.Info($"Execution {work.ExecutionId} finished successfully.");
//                }
//                else if (token.IsCancellationRequested)
//                {
//                    _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Stopped, null);
//                    _log.Info($"Execution {work.ExecutionId} stopped by client!!!");
//                }
//                else if (iterations.Count == 0)
//                {
//                    var finalShrinkRes = minRes.Count() > 0 ? minRes.Last().ShrinkResult : null;
//                    _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Running, finalShrinkRes);
//                    finalShrinkRes = RunOptimizerConditionAlgo(work, token, calcRequest, finalShrinkRes);
//                    _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Finished, finalShrinkRes);
//                    _log.Info($"Execution {work.ExecutionId} finished, no more iterations to execute.");
//                }

//            }
//            catch (Exception ex)
//            {
//                _log.Fatal($"An error occurred trying to calc optimizer algo on execution id: {work.ExecutionId}", ex);
//                _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, Data.ExecutionStatus.Error, null);
//            }
//        }

//        private void PersistBetsParentBaseResult(int executionId, List<Bet> eventsBets)
//        {
//            foreach (var bet in eventsBets)
//                _executionSrv.SetOptimizedBetResult(executionId, new OptimizedBet(bet.position + 1,
//                    bet.ToString(),
//                    false));
//        }

//        private OptimizerGamesMinIterationResult RunGamesIteration(int i, Tuple<Bet, Bet> newOriginalBet, BetsFormCreateRequest calcRequest, int minResult)
//        {
//            OptimizerGamesMinIterationResult iterationResult = null;
//            var originalBet = newOriginalBet.Item2;
//            calcRequest.EventsBets[originalBet.position] = newOriginalBet.Item1;
//            var result = _calculationSrv.CalculateSystem(calcRequest);
//            var postCalcResult = result.ResultsStatistics.Found;
//            _log.Info($"Iteration[{i}]: from p:{newOriginalBet.Item2.position},{newOriginalBet.Item2} to p:{newOriginalBet.Item1.position},{newOriginalBet.Item1}; {postCalcResult}/{result.FinalResults.Count()}");
//            if (postCalcResult > 0 && postCalcResult < minResult)
//            {
//                iterationResult = new OptimizerGamesMinIterationResult()
//                {
//                    IterationIndex = i,
//                    GameIndex = originalBet.position + 1,
//                    ShrinkResult = result,
//                    Str1X2 = calcRequest.EventsBets[originalBet.position].ToString()
//                };
//            }
//            calcRequest.EventsBets[originalBet.position] = originalBet;
//            return iterationResult;
//        }

//        private OptimizerGamesMinIterationResult RunGamesMainIterations(List<Tuple<Bet, Bet>> newOriginalBets, BetsFormCreateRequest calcRequest, int executionId, CancellationToken token)
//        {
//            var minResult = int.MaxValue;
//            var minResultPosition = -1;
//            var str1x2 = string.Empty;
//            SearchResults minSeResult = null;
//            var minResultIterationIndex = -1;
//            for (var i = 0; i < newOriginalBets.Count && !token.IsCancellationRequested; i++)
//            {
//                var iterationResult = RunGamesIteration(i, newOriginalBets[i], calcRequest, minResult);
//                if (iterationResult != null)
//                {
//                    minSeResult = iterationResult.ShrinkResult;
//                    minResult = minSeResult.ResultsStatistics.Found;
//                    minResultIterationIndex = iterationResult.IterationIndex;
//                    minResultPosition = newOriginalBets[i].Item2.position;
//                    str1x2 = iterationResult.Str1X2;
//                }
//            }
//            OptimizerGamesMinIterationResult minRes = null;
//            if (minSeResult != null)
//            {
//                minRes = new OptimizerGamesMinIterationResult()
//                {
//                    IterationIndex = minResultIterationIndex,
//                    GameIndex = minResultPosition + 1,
//                    ShrinkResult = minSeResult,
//                    Str1X2 = str1x2
//                };
//            }
//            else
//            {
//                minRes = RunGamesAlternativeIteration(newOriginalBets, calcRequest, executionId);
//            }
//            return minRes;
//        }

//        private OptimizerGamesMinIterationResult RunGamesAlternativeIteration(List<Tuple<Bet, Bet>> newOriginalBets, BetsFormCreateRequest calcRequest, int executionId)
//        {
//            _log.Info("Running alternative iteration, since no valid results were found, only zero");
//            var altIteration = PrepareGamesIterationWithMinRate(calcRequest);
//            newOriginalBets.Add(altIteration);
//            var minResult = int.MaxValue;
//            SearchResults minSeResult = null;
//            var i = newOriginalBets.Count - 1;
//            var iterationResult = RunGamesIteration(i, newOriginalBets[i], calcRequest, minResult);
//            if (iterationResult != null)
//            {
//                minSeResult = iterationResult.ShrinkResult;
//                minResult = minSeResult.ResultsStatistics.Found;
//            }
//            var changedBet = newOriginalBets[i].Item1;
//            calcRequest.EventsBets[changedBet.position] = changedBet;
//            _executionSrv.SetOptimizedBetResult(executionId, new OptimizedBet(changedBet.position + 1, changedBet.ToString(), true));

//            return iterationResult;
//        }

//        private Tuple<Bet, Bet> PrepareGamesIterationWithMinRate(BetsFormCreateRequest calcRequest)
//        {
//            Bet betWithMinRate = null;
//            BetOutputType betTypeToSet = BetOutputType.Bet1;
//            var minVal = float.MaxValue;
//            foreach (var bet in calcRequest.EventsBets)
//            {
//                var rateLine = calcRequest.EventsRates[bet.position];
//                var availableBets = new Dictionary<BetOutputType, float>();
//                if (!bet.bet1)
//                    availableBets.Add(BetOutputType.Bet1, rateLine.rate1);
//                if (!bet.betX)
//                    availableBets.Add(BetOutputType.BetX, rateLine.rateX);
//                if (!bet.bet2)
//                    availableBets.Add(BetOutputType.Bet2, rateLine.rate2);
//                if (availableBets.Keys.Count == 1)
//                    minVal = Math.Min(minVal, availableBets.First().Value);
//                else if (availableBets.Keys.Count == 2)
//                    minVal = Math.Min(minVal, Math.Min(availableBets.ElementAt(0).Value, availableBets.ElementAt(1).Value));

//                if (availableBets.Any(b => b.Value == minVal))
//                {
//                    betWithMinRate = bet;
//                    betTypeToSet = availableBets.First(b => b.Value == minVal).Key;
//                }
//            }
//            _log.Info($"Min rate [{minVal}] was found and set in game {betWithMinRate.position + 1}");
//            var calcSet = CreateGamesCalcSetForIteration(betWithMinRate, betTypeToSet);
//            return calcSet;
//        }

//        private List<Tuple<Bet, Bet>> PrepareGamesDataForNextIteration(List<Bet> parentBets)
//        {
//            var parentIterations = new List<Tuple<Bet, Bet>>();
//            foreach (var ob in parentBets)
//            {
//                var calcSet1 = CreateGamesCalcSetForIteration(ob, BetOutputType.Bet1);
//                if (calcSet1 != null)
//                    parentIterations.Add(calcSet1);
//                var calcSetX = CreateGamesCalcSetForIteration(ob, BetOutputType.BetX);
//                if (calcSetX != null)
//                    parentIterations.Add(calcSetX);
//                var calcSet2 = CreateGamesCalcSetForIteration(ob, BetOutputType.Bet2);
//                if (calcSet2 != null)
//                    parentIterations.Add(calcSet2);
//            }
//            return parentIterations;
//        }

//        Tuple<Bet, Bet> CreateGamesCalcSetForIteration(Bet ob, BetOutputType type)
//        {
//            Tuple<Bet, Bet> newOrg = null;
//            Bet newBet = null;
//            switch (type)
//            {
//                case BetOutputType.Bet1:
//                    if (ob.bet1 == true)
//                        return null;
//                    else
//                    {
//                        newBet = new Bet(ob);
//                        newBet.bet1 = true;
//                    }
//                    break;
//                case BetOutputType.BetX:
//                    if (ob.betX == true)
//                        return null;
//                    else
//                    {
//                        newBet = new Bet(ob);
//                        newBet.betX = true;
//                    }
//                    break;
//                case BetOutputType.Bet2:
//                    if (ob.bet2 == true)
//                        return null;
//                    else
//                    {
//                        newBet = new Bet(ob);
//                        newBet.bet2 = true;
//                    }
//                    break;
//            }
//            newOrg = new Tuple<Bet, Bet>(newBet, ob);
//            return newOrg;
//        }
//        #endregion
//        #region ConditionAlgo
//        private SearchResults RunOptimizerConditionAlgo(OptimizerExecutionData work, CancellationToken token, BetsFormCreateRequest calcRequest, 
//            SearchResults finalShrinkRes)
//        {
//            double minRatesSum = 0;
//            double maxRatesSum = 0;
//            foreach (var rates in calcRequest.EventsRates)
//            {
//                minRatesSum += Math.Min(rates.rate1, Math.Min(rates.rate2, rates.rateX));
//                maxRatesSum += Math.Max(rates.rate1, Math.Max(rates.rate2, rates.rateX));
//            }
//            minRatesSum = Math.Round(minRatesSum, 2);
//            maxRatesSum = Math.Round(maxRatesSum, 2);
//            var targetReached = false;
//            List<OptimizerConditionIteration> iterations = null;
//            List<OptimizerConditionsMinIterationResult> minRes = new List<OptimizerConditionsMinIterationResult>();
//            do
//            {
//                iterations = PrepareConditionsDataForNextIteration(calcRequest, new MinMax((decimal)minRatesSum, (decimal)maxRatesSum));
//                if (iterations.Count > 0)
//                {
//                    var minResult = RunConditionMainIterations(iterations, calcRequest, work.ExecutionId, token);
//                    if (!token.IsCancellationRequested && minResult != null)
//                    {
//                        minRes.Add(minResult);
//                        if (minResult.ShrinkResult.ResultsStatistics.Found > finalShrinkRes.ResultsStatistics.Found + work.Parameters.TargetAdditionalConditionCounter)
//                        {
//                            targetReached = true;
//                            minRes.Remove(minRes.Last());
//                        }
//                        else
//                        {
//                            _log.Info($"Min result in iteration: {minResult}");
//                            var status = _executionSrv.GetExecutionStatus(work.ExecutionId);
//                            if (status.Status == ExecutionStatus.Running) //it may be stopped by client
//                            {
//                                _executionSrv.UpdateExecutionStatus<SearchResults>(work.ExecutionId, SystemType.Optimizer, ExecutionStatus.Running, minResult.ShrinkResult);
//                            }
//                            else
//                                _log.Info($"{status.Status}");

//                            if (minResult.ConditionType != ConditionType.None)
//                                _executionSrv.SetOptimizedConditionResult(work.ExecutionId, 
//                                    new OptimizedCondition(minResult.BetTitle, minResult.ConditionType, iterations[minResult.IterationIndex].AfterCondition.MinMax));
//                            else
//                                _executionSrv.SetOptimizedAdvancedConditionResult(work.ExecutionId, 
//                                    new OptimizedAdvancedCondition(minResult.AdvancedConditionType, minResult.TypeIndex, iterations[minResult.IterationIndex].AfterAdvancedCondition.MinMax));
//                            SetConditionIteration(calcRequest.FormSettings, iterations[minResult.IterationIndex], false);//Set new Parent
//                        }
//                    }
//                }
//            }
//            while (!token.IsCancellationRequested && !targetReached && iterations.Count > 0);
//            var finalResult = minRes != null && minRes.Count > 0
//                ? minRes.Last().ShrinkResult
//                : finalShrinkRes;
//            return finalResult;
//        }

//        private OptimizerConditionsMinIterationResult RunConditionMainIterations(List<OptimizerConditionIteration> iterations, BetsFormCreateRequest calcRequest, int executionId, CancellationToken token)
//        {
//            var minResult = int.MaxValue;
//            SearchResults minSeResult = null;
//            var minIterationIndex = 0;
//            OptimizerConditionIteration minIteration = null;
//            for (var i = 0; i < iterations.Count && !token.IsCancellationRequested; i++)
//            {
//                var iterationResult = RunConditionIteration(i, iterations[i], calcRequest, minResult);
//                if (iterationResult != null)
//                {
//                    minSeResult = iterationResult.ShrinkResult;
//                    minIterationIndex = i;
//                    minResult = minSeResult.ResultsStatistics.Found;
//                    minIteration = iterations[i];
//                }
//            }
//            OptimizerConditionsMinIterationResult minRes = null;
//            if (minSeResult != null)
//            {
//                if (minIteration.AfterCondition != null)
//                    minRes = new OptimizerConditionsMinIterationResult(minIterationIndex, minSeResult,
//                        minIteration.AfterCondition.Type, minIteration.AfterCondition.BetTitle);
//                else
//                    minRes = new OptimizerConditionsMinIterationResult(minIterationIndex, minSeResult,
//                        minIteration.AfterAdvancedCondition.Type, minIteration.AfterAdvancedCondition.Index);
//            }
//            return minRes;
//        }

//        private OptimizerConditionsMinIterationResult RunConditionIteration(int i, OptimizerConditionIteration iteration, BetsFormCreateRequest calcRequest, int minResult)
//        {
//            OptimizerConditionsMinIterationResult iterationResult = null;
//            SetConditionIteration(calcRequest.FormSettings, iteration, false);
//            var result = _calculationSrv.CalculateSystem(calcRequest);
//            var postCalcResult = result.ResultsStatistics.Found;
//            if (iteration.BeforeCondition != null)
//                _log.Info($"Iteration[{i}]: from {iteration.BeforeCondition.Type},{iteration.BeforeCondition.BetTitle},{iteration.BeforeCondition.MinMax.ToString()} to {iteration.AfterCondition.Type},{iteration.AfterCondition.BetTitle},{iteration.AfterCondition.MinMax.ToString()}; {postCalcResult}/{result.FinalResults.Count()}");
//            else
//                _log.Info($"Iteration[{i}]: from {iteration.BeforeAdvancedCondition.Type}[{iteration.BeforeAdvancedCondition.Index}],{iteration.BeforeAdvancedCondition.MinMax.ToString()} to {iteration.AfterAdvancedCondition.Type}[{iteration.AfterAdvancedCondition.Index}],{iteration.AfterAdvancedCondition.MinMax.ToString()}; {postCalcResult}/{result.FinalResults.Count()}");
//            if (/*postCalcResult > 0 &&*/ postCalcResult < minResult)
//            {
//                if (iteration.AfterCondition != null)
//                    iterationResult = new OptimizerConditionsMinIterationResult(i, result, iteration.AfterCondition.Type, iteration.AfterCondition.BetTitle);
//                else
//                    iterationResult = new OptimizerConditionsMinIterationResult(i, result, iteration.AfterAdvancedCondition.Type, iteration.AfterAdvancedCondition.Index);
//            }
//            SetConditionIteration(calcRequest.FormSettings, iteration, true);
//            return iterationResult;
//        }

//        private void SetConditionIteration(FormSettings formSettings, OptimizerConditionIteration iteration, bool setBeforeValues)
//        {
//            if (iteration.BeforeCondition != null && iteration.BeforeAdvancedCondition != null)
//                throw new Exception("iteration not set properlly. There should be only Condition or AdvancedCondition");
//            if (iteration.BeforeCondition != null)
//            {
//                var minMaxValue = setBeforeValues ? iteration.BeforeCondition.MinMax : iteration.AfterCondition.MinMax;
//                switch (iteration.AfterCondition.Type)
//                {
//                    case ConditionType.Amount1X2:
//                        switch (iteration.AfterCondition.BetTitle)
//                        {
//                            case ConditionBetTitleEnum.Title_1:
//                                formSettings.IncludeSettings.amount1 = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_X:
//                                formSettings.IncludeSettings.amountX = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_2:
//                                formSettings.IncludeSettings.amount2 = minMaxValue;
//                                break;
//                        }
//                        break;
//                    case ConditionType.Sequence1X2:
//                        switch (iteration.AfterCondition.BetTitle)
//                        {
//                            case ConditionBetTitleEnum.Title_1:
//                                formSettings.IncludeSettings.sequence1 = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_X:
//                                formSettings.IncludeSettings.sequenceX = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_2:
//                                formSettings.IncludeSettings.sequence2 = minMaxValue;
//                                break;
//                        }
//                        break;
//                    case ConditionType.Breaks1X2:
//                        formSettings.IncludeSettings.sequenceBreak = minMaxValue;
//                        break;
//                    case ConditionType.AmountABC:
//                        switch (iteration.AfterCondition.BetTitle)
//                        {
//                            case ConditionBetTitleEnum.Title_A:
//                                formSettings.CombinationLettersSettings.MinMaxA = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_B:
//                                formSettings.CombinationLettersSettings.MinMaxB = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_C:
//                                formSettings.CombinationLettersSettings.MinMaxC = minMaxValue;
//                                break;
//                        }
//                        break;
//                    case ConditionType.SequenceABC:
//                        switch (iteration.AfterCondition.BetTitle)
//                        {
//                            case ConditionBetTitleEnum.Title_A:
//                                formSettings.CombinationLettersSettings.MinMaxSequenceLengthA = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_B:
//                                formSettings.CombinationLettersSettings.MinMaxSequenceLengthB = minMaxValue;
//                                break;
//                            case ConditionBetTitleEnum.Title_C:
//                                formSettings.CombinationLettersSettings.MinMaxSequenceLengthC = minMaxValue;
//                                break;
//                        }
//                        break;
//                    case ConditionType.BreaksABC:
//                        formSettings.CombinationLettersSettings.MinMaxBreak = minMaxValue;
//                        break;
//                    case ConditionType.Range:
//                        formSettings.IncludeSettings.amountSum = minMaxValue;
//                        break;
//                }
//            }
//            if (iteration.BeforeAdvancedCondition != null)
//            {
//                var index = iteration.BeforeAdvancedCondition.Index - 1;
//                var minMaxValue = setBeforeValues ? iteration.BeforeAdvancedCondition.MinMax : iteration.AfterAdvancedCondition.MinMax;
//                switch (iteration.BeforeAdvancedCondition.Type)
//                {
//                    case AdvancedConditionType.Single1X2:
//                        formSettings.Combination1X2Settings.ElementAt(index).SetMinMax(minMaxValue);
//                        break;
//                    case AdvancedConditionType.SingleABC:
//                        formSettings.CombinationLettersSettings.LettersSettings.ElementAt(index).SetMinMax(minMaxValue);
//                        break;
//                    case AdvancedConditionType.PairShape1X2:
//                        formSettings.Shapes2.ElementAt(index).SetMinMax(minMaxValue);
//                        break;
//                    case AdvancedConditionType.PairShapeABC:
//                        formSettings.LettersShapes2.ElementAt(index).SetMinMax(minMaxValue);
//                        break;
//                    case AdvancedConditionType.TripletShape1X2:
//                        formSettings.Shapes3.ElementAt(index).SetMinMax(minMaxValue);
//                        break;
//                    case AdvancedConditionType.TripletShapeABC:
//                        formSettings.LettersShapes3.ElementAt(index).SetMinMax(minMaxValue);
//                        break;
//                }
//            }
//        }

//        private List<OptimizerConditionIteration> PrepareConditionsDataForNextIteration(BetsFormCreateRequest calcRequest, MinMax minMaxRates)
//        {
//            var iterations = new List<OptimizerConditionIteration>();
//            var settings1x2 = calcRequest.FormSettings.IncludeSettings;
//            var numberOfGames = calcRequest.EventsBets.Count();
//            CreateConditionIteration(ConditionType.Amount1X2, ConditionBetTitleEnum.Title_1, settings1x2.amount1, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.Amount1X2, ConditionBetTitleEnum.Title_X, settings1x2.amountX, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.Amount1X2, ConditionBetTitleEnum.Title_2, settings1x2.amount2, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.Sequence1X2, ConditionBetTitleEnum.Title_1, settings1x2.sequence1, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.Sequence1X2, ConditionBetTitleEnum.Title_X, settings1x2.sequenceX, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.Sequence1X2, ConditionBetTitleEnum.Title_2, settings1x2.sequence2, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.Breaks1X2, ConditionBetTitleEnum.None, settings1x2.sequenceBreak, numberOfGames - 1, ref iterations);

//            if (settings1x2.amountSum.Min > minMaxRates.Min)
//                iterations.Add(new OptimizerConditionIteration(
//                    new OptimizerCondition(ConditionType.Range, ConditionBetTitleEnum.None, settings1x2.amountSum.Min, settings1x2.amountSum.Max),
//                    new OptimizerCondition(ConditionType.Range, ConditionBetTitleEnum.None, settings1x2.amountSum.Min - (decimal)0.01, settings1x2.amountSum.Max)));
//            if (settings1x2.amountSum.Max < minMaxRates.Max)
//                iterations.Add(new OptimizerConditionIteration(
//                    new OptimizerCondition(ConditionType.Range, ConditionBetTitleEnum.None, settings1x2.amountSum.Min, settings1x2.amountSum.Max),
//                    new OptimizerCondition(ConditionType.Range, ConditionBetTitleEnum.None, settings1x2.amountSum.Min, settings1x2.amountSum.Max + (decimal)0.01)));

//            var settingsABC = calcRequest.FormSettings.CombinationLettersSettings;
//            CreateConditionIteration(ConditionType.AmountABC, ConditionBetTitleEnum.Title_A, settingsABC.MinMaxA, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.AmountABC, ConditionBetTitleEnum.Title_B, settingsABC.MinMaxB, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.AmountABC, ConditionBetTitleEnum.Title_C, settingsABC.MinMaxC, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.SequenceABC, ConditionBetTitleEnum.Title_A, settingsABC.MinMaxSequenceLengthA, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.SequenceABC, ConditionBetTitleEnum.Title_B, settingsABC.MinMaxSequenceLengthB, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.SequenceABC, ConditionBetTitleEnum.Title_C, settingsABC.MinMaxSequenceLengthC, numberOfGames, ref iterations);
//            CreateConditionIteration(ConditionType.BreaksABC, ConditionBetTitleEnum.None, settingsABC.MinMaxBreak, numberOfGames - 1, ref iterations);

//            CreateConditionIteration(AdvancedConditionType.Single1X2, calcRequest.FormSettings.Combination1X2Settings, numberOfGames, ref iterations);
//            CreateConditionIteration(AdvancedConditionType.SingleABC, calcRequest.FormSettings.CombinationLettersSettings.LettersSettings, numberOfGames, ref iterations);
//            CreateConditionIteration(AdvancedConditionType.PairShape1X2, calcRequest.FormSettings.Shapes2, 9, ref iterations);
//            CreateConditionIteration(AdvancedConditionType.PairShapeABC, calcRequest.FormSettings.LettersShapes2, 9, ref iterations);
//            CreateConditionIteration(AdvancedConditionType.TripletShape1X2, calcRequest.FormSettings.Shapes3, 27, ref iterations);
//            CreateConditionIteration(AdvancedConditionType.TripletShapeABC, calcRequest.FormSettings.LettersShapes3, 27, ref iterations);
//            return iterations;
//        }

//        private void CreateConditionIteration(AdvancedConditionType type, List<CombinationSetting> combinations, int maxNumberAllowed, ref List<OptimizerConditionIteration> iterations)
//        {
//            var index = 1;
//            var activeCombinations = combinations.Where(s => s.IsActive == true);
//            foreach (var combination in activeCombinations)
//            {
//                if (combination.MinMax.Min > 0)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerAdvancedCondition(type, index, combination.MinMax.Min, combination.MinMax.Max),
//                        new OptimizerAdvancedCondition(type, index, combination.MinMax.Min - 1, combination.MinMax.Max)));
//                if (combination.MinMax.Max < maxNumberAllowed)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerAdvancedCondition(type, index, combination.MinMax.Min, combination.MinMax.Max),
//                        new OptimizerAdvancedCondition(type, index, combination.MinMax.Min, combination.MinMax.Max + 1)));
//                index++;
//            }
//        }

//        private void CreateConditionIteration(AdvancedConditionType type, List<ShapesSettings> shapes, int maxNumberAllowed, ref List<OptimizerConditionIteration> iterations)
//        {
//            var index = 1;
//            var activeShapes = shapes.Where(s => s.IsActive == true);
//            foreach (var shape in activeShapes)
//            {
//                if (shape.MinMax.Min > 0)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerAdvancedCondition(type, index, shape.MinMax.Min, shape.MinMax.Max),
//                        new OptimizerAdvancedCondition(type, index, shape.MinMax.Min - 1, shape.MinMax.Max)));
//                if (shape.MinMax.Max < maxNumberAllowed)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerAdvancedCondition(type, index, shape.MinMax.Min, shape.MinMax.Max),
//                        new OptimizerAdvancedCondition(type, index, shape.MinMax.Min, shape.MinMax.Max + 1)));
//                index++;
//            }
//        }

//        private void CreateConditionIteration(ConditionType type, ConditionBetTitleEnum typeTitle, MinMax minMax, int maxNumberAllowed, ref List<OptimizerConditionIteration> iterations)
//        {
//            try
//            {
//                if (minMax.Min > 0)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerCondition(type, typeTitle, minMax.Min, minMax.Max),
//                        new OptimizerCondition(type, typeTitle, minMax.Min - 1, minMax.Max)));
//                if (minMax.Max < maxNumberAllowed)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerCondition(type, typeTitle, minMax.Min, minMax.Max),
//                        new OptimizerCondition(type, typeTitle, minMax.Min, minMax.Max + 1)));
//            }
//            catch (Exception ex)
//            {
//                throw new Exception($"An error occurred trying to create condition iteration for {type} {typeTitle} {minMax.ToString()}", ex);
//            }
//        }
//        private void CreateConditionIteration(AdvancedConditionType type, int index, MinMax minMax, int maxNumberAllowed, ref List<OptimizerConditionIteration> iterations)
//        {
//            try
//            {
//                if (minMax.Min > 0)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerAdvancedCondition(type, index, minMax.Min, minMax.Max),
//                        new OptimizerAdvancedCondition(type, index, minMax.Min - 1, minMax.Max)));
//                if (minMax.Max < maxNumberAllowed)
//                    iterations.Add(new OptimizerConditionIteration(
//                        new OptimizerAdvancedCondition(type, index, minMax.Min, minMax.Max),
//                        new OptimizerAdvancedCondition(type, index, minMax.Min, minMax.Max + 1)));
//            }
//            catch (Exception ex)
//            {
//                throw new Exception($"An error occurred trying to create condition iteration for {type}:[{index}] {minMax.ToString()}", ex);
//            }
//        }
//        #endregion


//    }
//}
