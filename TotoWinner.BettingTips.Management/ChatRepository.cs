﻿using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.BettingTips.Management.Chat;
using System.Threading.Tasks;
using log4net;

namespace TotoWinner.BettingTips.Management
{
    static class ChatRepository
    {
        private static readonly ILog _log = LogManager.GetLogger("TelegramBot");
        private static Dictionary<int, IChatItem> ChatItems = new Dictionary<int, IChatItem>();
        private static Dictionary<int, int?> ChatChildParentMessagesMap = new Dictionary<int, int?>();
        public static Dictionary<long, IRegistrationChatItem> RegistrationChatItems = new Dictionary<long, IRegistrationChatItem>();
        private static readonly object RegistrationLock = new object();
        private static readonly object ChatItemsLock = new object();
        // TODO: hold repo of <messageId, subscriberId>, when there are problem with specific message we need to log also the subscriber; should be cleared like all messages
        public static void MaintainRegistrationChat()
        {
            try
            {
                lock (RegistrationLock)
                {
                    if (RegistrationChatItems.Count() > 0)
                    {
                        foreach (var item in RegistrationChatItems.Where(kvp => kvp.Value != null && kvp.Value.IsChatOpen == false).ToList())
                        {
                            RegistrationChatItems.Remove(item.Key);
                            RegistrationChatItems[item.Key] = null;
                        }
                        if (RegistrationChatItems.All(c => c.Value == null))
                            RegistrationChatItems.Clear();
                    }
                }
            }
            catch(Exception ex)
            {
                _log.Fatal(ex);
            }
        }
        public static void AddChatItem(int? parentMessageId, int messageId, IChatItem chat)
        {
            //parentMessageId if it null => it is the parent of start chat
            lock (ChatItemsLock)
            {
                ChatItems.Add(messageId, chat);
                if (parentMessageId.HasValue)
                    ChatChildParentMessagesMap.Add(messageId, parentMessageId);
            }
        }
        public static Task GetPostUserAction(int messageId, string callbackUserChatData)
        {
            return ChatItems[messageId].PostUserAction(callbackUserChatData);
        }

        public static void ReleaseChatItems(int lastChatMessageId)
        {
            var msgToRelease = new List<int>();
            var childMessageId = lastChatMessageId;
            while (ChatChildParentMessagesMap.ContainsKey(childMessageId))
            {
                msgToRelease.Add(childMessageId);
                childMessageId = ChatChildParentMessagesMap[childMessageId].Value;
            }
            msgToRelease.Add(childMessageId);
            MaintainChatItems(msgToRelease);
        }
        public static void MaintainChatItems(List<int> messageToRelease)
        {
            lock (ChatItemsLock)
            {
                foreach (var msg in messageToRelease)
                {
                    ChatItems.Remove(msg);
                    ChatItems[msg] = null;
                }
                if (ChatItems.All(c => c.Value == null))
                {
                    ChatItems.Clear();
                    ChatChildParentMessagesMap.Clear();
                }
            }
        }
    }
}
