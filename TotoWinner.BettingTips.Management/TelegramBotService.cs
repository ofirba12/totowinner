﻿using log4net;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using Telegram.Bot;
using TotoWinner.BettingTips.Management.Chat;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Management
{
    public partial class TelegramBotService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger("TelegramBot");
        private BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        private BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        private TelegramBotServices _botSrv = TelegramBotServices.Instance;
        private TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;
        static TelegramBotClient _botClient;
        private System.Timers.Timer _autoPushTipTimer = null;
        private System.Timers.Timer _lookForVisitedTips = null;
        private string _botToken;
        private readonly object timersLock = new object();

        public TelegramBotService()
        {
            InitializeComponent();
            this.ServiceName = "TelegramBotService";
            _botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
            _botClient = new TelegramBotClient(_botToken);
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("TelegramBot service started");
            try
            {
                this._autoPushTipTimer = new System.Timers.Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
                this._autoPushTipTimer.Enabled = true;
                this._autoPushTipTimer.AutoReset = true;
                this._autoPushTipTimer.Elapsed += _pushWatcher_Elapsed;

                this._lookForVisitedTips = new System.Timers.Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
                this._lookForVisitedTips.Enabled = true;
                this._lookForVisitedTips.AutoReset = true;
                this._lookForVisitedTips.Elapsed += _lookForVisitedTips_Elapsed;
                _botClient.OnMessage += BotClient_OnMessage;
                _botClient.OnCallbackQuery += BotClient_OnCallbackQuery;
                _botClient.OnReceiveGeneralError += _botClient_OnReceiveGeneralError;
                _botClient.StartReceiving();
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start TelegramBot service", ex);
            }
        }

        private void _botClient_OnReceiveGeneralError(object sender, Telegram.Bot.Args.ReceiveGeneralErrorEventArgs e)
        {
            _log.Fatal($"{e.Exception}");
        }

        private void _lookForVisitedTips_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock (timersLock)
                {
                    this._lookForVisitedTips.Enabled = false;
                    var urls = _botPersistanceSrv.GetVisitedSubscribersUrls();
                    var subscribers = urls.Select(u => u.SubscriberId).Distinct();
                    foreach (var url in urls)
                    {
                        _log.Info($"UpdateVisitedUrlPickedByChatServer UrlId={url.UrlId}");
                        _botPersistanceSrv.UpdateVisitedUrlPickedByChatServer(url.UrlId);
                    }
                    foreach (var subscriber in subscribers)
                    {
                        _log.Info($"Start Chart with subscriber={subscriber} after url visit");
                        StartChat(subscriber);
                    }
                    this._lookForVisitedTips.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start TelegramBot service", ex);
                this._lookForVisitedTips.Enabled = true;
            }
        }
        private async void StartChat(long subscriber)
        {
            try
            {
                var tip = _botSrv.FindTipToPublish(subscriber);
                if (tip != null)
                {
                    var start = new DoYouWantMoreTips(_botClient, subscriber);
                    await start.Show();
                    ChatRepository.AddChatItem(null, start.MessageId.Value, start);
                }
                else
                {
                    SendMessage(subscriber, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*");
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to start chat with subscriber {subscriber}", ex);
            }
        }
        private void _pushWatcher_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock (timersLock)
                {
                    this._autoPushTipTimer.Enabled = false;
                    this._lookForVisitedTips.Enabled = false;
                    var pushTime = _botSrv.GetPushTipTimes();
                    var pushTips = pushTime.Any(t => t.Hour == DateTime.Now.Hour && t.Minute == DateTime.Now.Minute);
                    var isFirstPush = pushTime.FirstOrDefault()?.Hour == DateTime.Now.Hour && pushTime.FirstOrDefault()?.Minute == DateTime.Now.Minute;
                    if (pushTips)
                    {
                        var subsribers = _botSrv.GetSubsribers(true);
                        foreach (var client in subsribers)
                        {
                            var tipToPublish = _botSrv.FindTipToPublish(client.ClientChatId.Value);
                            var fullTipDescription = _botSrv.PrepareTipForPublish(tipToPublish);
                            if (!string.IsNullOrEmpty(fullTipDescription))
                            {
                                if (isFirstPush)
                                    SendMessage(client.ClientChatId.Value, $"*בוקר טוב {client.FirstName}*");
                                var urlGuid = Guid.NewGuid();
                                var tipDescription = fullTipDescription.Replace("[#GUID#]", urlGuid.ToString());
                                _botPersistanceSrv.InsertSubscriberTip(urlGuid, tipToPublish.TipId, client.ClientChatId.Value);

                                SendMessage(client.ClientChatId.Value, tipDescription);
                            }
                        }
                    }
                    this._autoPushTipTimer.Enabled = true;
                    this._lookForVisitedTips.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to watch for push messages", ex);
                this._autoPushTipTimer.Enabled = true;
                this._lookForVisitedTips.Enabled = true;
            }
        }

        private async void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            try
            {
                _log.Info($"ChatId: {e.Message.Chat.Id}, Text:{e.Message.Text}");
                if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text && e.Message.Text == @"/start")
                {//TODO: OnboardingChat should be set to ISChatOpen=false after 30 minutes, so it can be cleared
                    var welcome = new OnboardingChat(_botClient, e.Message.Chat.Id, e.Message.From.FirstName, e.Message.From.LastName);
                    await welcome.Show();
                    if (!ChatRepository.RegistrationChatItems.ContainsKey(e.Message.Chat.Id))
                        ChatRepository.RegistrationChatItems.Add(e.Message.Chat.Id, welcome);
                }
                else if (ChatRepository.RegistrationChatItems.Where(c => c.Value != null && c.Value.IsChatOpen).Count() > 0 &&
                    e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
                {
                    if (ChatRepository.RegistrationChatItems.ContainsKey(e.Message.Chat.Id) && 
                        ChatRepository.RegistrationChatItems[e.Message.Chat.Id] != null)
                    {
                        await ChatRepository.RegistrationChatItems[e.Message.Chat.Id].PostUserAction(e.Message.Text);
                        var registrationSuccess = ChatRepository.RegistrationChatItems[e.Message.Chat.Id].IsChatOpen == false;
                        ChatRepository.MaintainRegistrationChat();
                        if (registrationSuccess)
                        {
                            _log.Info($"Start Chart after success registration with subscriber={e.Message.Chat.Id}");
                            StartChat(e.Message.Chat.Id);
                        }
                    }
                }
                if (e.Message.Text == "קוד") 
                {
                    SendMessage(e.Message.Chat.Id, $"מספר לקוח: {e.Message.Chat.Id}");
                }
                else if (e.Message.Text == "המלצה") 
                {
                    var start = new DoYouWantMoreTips(_botClient, e.Message.Chat.Id);
                    await start.Show();
                    ChatRepository.AddChatItem(null, start.MessageId.Value, start);
                }
            }
            catch(Exception ex)
            {
                _log.Fatal($"Unexpected error occurred trying to recieve messages from subscriber {e.Message.Chat.Id}", ex);
            }
        }

        private static async void BotClient_OnCallbackQuery(object sender, Telegram.Bot.Args.CallbackQueryEventArgs e)
        {
            try
            {
                _log.Info($"MessageId: {e.CallbackQuery.Message.MessageId}, Data:{e.CallbackQuery.Data}");
                await ChatRepository.GetPostUserAction(e.CallbackQuery.Message.MessageId, e.CallbackQuery.Data);
                //await ChatRepository.ChatItems[e.CallbackQuery.Message.MessageId].PostUserAction(e.CallbackQuery.Data);
            }
            catch(Exception ex)
            {
                _log.Fatal($"An error occurred trying to get callback query for message {e.CallbackQuery.Message.MessageId}, data {e.CallbackQuery.Data}", ex);
            }
        }
        private async void SendMessage(long chatId, string message)
        {
            try
            {
                Task.Delay(1000).Wait();
                await _botClient.SendTextMessageAsync(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Markdown);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to send message to chatId:{chatId}, message:{Environment.NewLine}{message}", ex);
            }
        }
        protected override void OnStop()
        {
            try
            {
                _log.Info("TelegramBot service stopped");
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop TelegramBot service", ex);
            }
        }
    }
}
#region old
//private async void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
//{
//    Console.WriteLine($"{e.Message.From} :{e.Message.Text}");
//    _log.Info($"Message From {e.Message.Chat.Id}:{e.Message.From} :{e.Message.Text}");
//    if (e.Message.Text == "1")
//    {
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $"תשובה ל-{e.Message.Text}");
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $" chat id : {e.Message.Chat.Id}");

//        var keyboard = new ReplyKeyboardMarkup(new[]
//        {
//            new[] {new KeyboardButton("כן"){} },
//            new[] {new KeyboardButton("לא"){} },
//            new[] {new KeyboardButton("נא ליצור עימי קשר"){} },
//        },
//        true,
//        true);

//        await Task.Delay(500);
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $"בחר", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
//    }
//    if (e.Message.Text == "2")
//    {
//        var inlineButtonYes = InlineKeyboardButton.WithCallbackData("כן", "יופי ממשיכים");
//        var inlineButtonNo = InlineKeyboardButton.WithCallbackData("לא", "חבל");
//        var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("נא ליצור עימי קשר", "SUPPORT");
//        var inlineButton = new InlineKeyboardButton()
//        {
//            Text = "לחץ כאן",
//            Url = "https://google.com"
//        };
//        var baseMenu = new List<InlineKeyboardButton[]>();
//        baseMenu.Add(new[] { inlineButtonYes });
//        baseMenu.Add(new[] { inlineButtonNo });
//        baseMenu.Add(new[] { inlineButtonSupport });
//        //baseMenu.Add(new[] { inlineButton });
//        var gmenu = new InlineKeyboardMarkup(baseMenu.ToArray());
//        await Task.Delay(500);
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $"האם אתה מעוניין בהמלצה נוספת?", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, gmenu);
//    }
//    if (e.Message.Text == "3")
//    {
//        var inlineButton = new InlineKeyboardButton()
//        {
//            Text = "לחץ כאן",
//            Url = "http://awstoto.bankerim.co.il/BankerimTips/api/Tip/Send?guid=6a89c5ba-2c6c-4b4b-8fbf-d4282b02f1e6"
//        };
//        var baseMenu = new List<InlineKeyboardButton[]>();
//        baseMenu.Add(new[] { inlineButton });
//        var gmenu = new InlineKeyboardMarkup(baseMenu.ToArray());
//        await Task.Delay(500);
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $"המלצה להימור טניס יום שני שעה 10:55 ראונדה ATP סוסו - 10 {Environment.NewLine}למעבר להמלצה הנוכחית באתר הווינר ולקבלת המלצה נוספת,  >>>> ", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, gmenu);
//    }
//    if (e.Message.Text == "כן" || e.Message.Text == "לא" || e.Message.Text == "נא ליצור עימי קשר")
//    {
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $"תודה רבה");
//    }
//    if (e.Message.Text == "4")
//    {
//        await Task.Delay(500);
//        var url = "http://www.example.com/";
//        await _botClient.SendTextMessageAsync(e.Message.Chat.Id, $"המלצה להימור טניס יום שני שעה 10:55 ראונדה ATP סוסו - 10 {Environment.NewLine}[PUSH]({url})", Telegram.Bot.Types.Enums.ParseMode.Markdown);
//    }
//    if (e.Message.Text == "5")
//    {
//        var start = new DoYouWantMoreTips(_botClient, e.Message.Chat.Id, this._waitForAnswerInSeconds);
//        await start.Show();
//        ChatRepository.ChatItems.Add(start.MessageId.Value, start);
//    }
//}
#endregion