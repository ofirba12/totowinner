﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Management.Chat
{
    public abstract class BaseChatItem
    {
        protected static readonly ILog _log = LogManager.GetLogger("TelegramBot");
        protected TelegramBotClient BotClient;
        protected Timer WaitForAnswer;
        protected double WaitForAnswerInterval;
        public long SubscriberId;
        protected int? _messageId;
        public int? MessageId => this._messageId;
        protected InlineKeyboardMarkup Menu;
        protected int ManagerSubscriberId = 585508035;
        protected List<int> AlreadySentTipsToday;
        protected BettingTipsPersistanceServices _btPersistanceSrv = BettingTipsPersistanceServices.Instance;
        protected BettingTipsServices _btTipsSrv = BettingTipsServices.Instance;
        protected TelegramBotServices _botSrv = TelegramBotServices.Instance;
        protected TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;
        protected int NumberOfNonChatItemsInMenu;

        protected async void ShowMenu()
        {
            if (this.Menu.InlineKeyboard.Count() == this.NumberOfNonChatItemsInMenu)
                await BotClient.SendTextMessageAsync(this.SubscriberId, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*", 
                    Telegram.Bot.Types.Enums.ParseMode.Markdown);
            else
            {
                this.WaitForAnswer = new Timer(this.WaitForAnswerInterval);
                this.WaitForAnswer.Enabled = true;
                this.WaitForAnswer.Elapsed += WaitForAnswer_Elapsed;
            }
        }
        protected bool IsActiveMenu()
        {
            return this.WaitForAnswer!= null && this.WaitForAnswer.Enabled;
        }
        protected async Task HideMenu()
        {
            this.WaitForAnswer.Enabled = false;
            this.WaitForAnswer.Dispose();
            await Task.Delay(1000);
            await BotClient.EditMessageReplyMarkupAsync(this.SubscriberId, this._messageId.Value);
        }
        protected async void WaitForAnswer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                await HideMenu();
                var replyMessage = "*עקב חוסר פעילות השיחה נותקה. המלצה הבאה תעלה בהמשך*";
                await this.BotClient.SendTextMessageAsync(this.SubscriberId, replyMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);
                ChatRepository.ReleaseChatItems(this.MessageId.Value);
            }
            catch(Exception ex)
            {
                _log.Fatal(ex);
            }
        }
        //protected void SentTip(Tip tipToPublish)
        //{
        //    if (tipToPublish != null)
        //    {
        //        var fullTipDescription = _botSrv.PrepareTipForPublish(tipToPublish);
        //        if (!string.IsNullOrEmpty(fullTipDescription))
        //        {
        //            var urlGuid = Guid.NewGuid();
        //            var tipDescription = fullTipDescription.Replace("[#GUID#]", urlGuid.ToString());
        //            _botPersistanceSrv.InsertSubscriberTip(urlGuid, tipToPublish.TipId, this.SubscriberId);

        //            SendMessage(this.SubscriberId, tipDescription);
        //        }
        //    }
        //    else
        //        SendMessage(this.SubscriberId, "*אין עוד המלצות להיום. המלצות חדשות יעודכנו ויעלו מחר בבוקר*");
        //}
        //private async void SendMessage(long chatId, string message)
        //{
        //    Task.Delay(1000).Wait();
        //    await BotClient.SendTextMessageAsync(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Markdown);
        //}
    }
}