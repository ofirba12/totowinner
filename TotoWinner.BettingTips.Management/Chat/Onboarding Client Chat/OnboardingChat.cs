﻿using log4net;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Timers;
using Telegram.Bot;
using TotoWinner.Services;

namespace TotoWinner.BettingTips.Management.Chat
{
    public class OnboardingChat : BaseChatItem, IRegistrationChatItem
    {
        public string FirstName { get; }
        public string LastName { get; }
        public bool IsChatOpen { get; private set; }
        private static int WaitForAnswerInSeconds;

        public OnboardingChat(TelegramBotClient client, long subscriberId, string firstName, string lastName)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.IsChatOpen = true;
            WaitForAnswerInSeconds = int.Parse(ConfigurationManager.AppSettings["WaitForAnswerInSeconds"].ToString());
            this.WaitForAnswerInterval = TimeSpan.FromSeconds(WaitForAnswerInSeconds).TotalMilliseconds;
        }

        public async Task Show()
        {
            var welcomeMessage = $"שלום {this.FirstName} {this.LastName}, נא להקליד את קוד הלקוח שקיבלת מנציג בנקרים על מנת להמשיך בתהליך ההתחברות";
            this.WaitForAnswer = new Timer(this.WaitForAnswerInterval);
            this.WaitForAnswer.Enabled = true;
            this.WaitForAnswer.Elapsed += WaitForAnswer_Elapsed1;

            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, welcomeMessage,
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                false,
                false,
                0,
                this.Menu);
            this._messageId = message.MessageId;
        }

        private async void WaitForAnswer_Elapsed1(object sender, ElapsedEventArgs e)
        {
            try
            {
                this.WaitForAnswer.Enabled = false;
                this.WaitForAnswer.Dispose();
                var replyMessage = "*עקב חוסר פעילות השיחה נותקה.*";
                await this.BotClient.SendTextMessageAsync(this.SubscriberId, replyMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);
                this.IsChatOpen = false;
                ChatRepository.MaintainRegistrationChat();
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
        }

        public async Task PostUserAction(string callbackData)
        {
            var registerResponse = TelegramBotServices.Instance.VerifyAndRegisterSubscriber(callbackData, this.SubscriberId);
            this.IsChatOpen = !registerResponse.IsSuccess;
            await this.BotClient.SendTextMessageAsync(this.SubscriberId, registerResponse.RegistrationMarkupMessage, Telegram.Bot.Types.Enums.ParseMode.Markdown);        
            if (registerResponse.IsSuccess)
            {
                this.WaitForAnswer.Enabled = false;
                this.WaitForAnswer.Dispose();
            }
        }
    }
}
