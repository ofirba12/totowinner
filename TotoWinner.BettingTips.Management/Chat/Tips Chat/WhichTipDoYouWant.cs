﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;
using TotoWinner.Data.TelegramBot;

namespace TotoWinner.BettingTips.Management.Chat
{
    public class WhichTipDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }

        public WhichTipDoYouWant(TelegramBotClient client, long subscriberId, double waitForAnswerInterval, List<int> alreadySentTipsToday)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת לצפות בהמלצה נוספת, *אנא בחר את סוג ההמלצה הרצוי*",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                false,
                false,
                0,
                this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackData)
        {
            try
            {
                if (!IsActiveMenu())
                    return;

                await HideMenu();
                switch (callbackData)
                {
                    case "1":
                        SetButtonStatistics(BotButtonType.Favorite);
                        var filter = new BettingTipsFilter(BettingTipFilterType.Favorite);
                        var tip = _btTipsSrv.GetNextActiveTips(filter, this.AlreadySentTipsToday);
                        _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                        ChatRepository.ReleaseChatItems(this.MessageId.Value);
                        break;
                    case "2":
                        SetButtonStatistics(BotButtonType.In3HoursAndFavorite);
                        filter = new BettingTipsFilter(BettingTipFilterType.In3HoursAndFavorite);
                        tip = _btTipsSrv.GetNextActiveTips(filter, this.AlreadySentTipsToday);
                        _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                        ChatRepository.ReleaseChatItems(this.MessageId.Value);
                        break;
                    case "3":
                        SetButtonStatistics(BotButtonType.SingleHighRate);
                        filter = new BettingTipsFilter(BettingTipFilterType.SingleHighRate);
                        tip = _btTipsSrv.GetNextActiveTips(filter, this.AlreadySentTipsToday);
                        _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                        ChatRepository.ReleaseChatItems(this.MessageId.Value);
                        break;
                    case "4":
                        SetButtonStatistics(BotButtonType.Custom);
                        this.NextChat = new WhichSportDoYouWant(this.BotClient, this.SubscriberId, this.WaitForAnswerInterval, this.AlreadySentTipsToday);
                        await this.NextChat.Show();
                        ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                        break;
                    case "BACK":
                        this.NextChat = new DoYouWantMoreTips(this.BotClient, this.SubscriberId);
                        await this.NextChat.Show();
                        ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                        break;
                }
            }
            catch(Exception ex) //TODO: could be network issues, need to release chat or retry when network is available
            {
                throw new Exception($"An error occurred trying to handle user callbackData {callbackData}", ex);
            }
        }
        private void SetButtonStatistics(BotButtonType button)
        {
            try
            {
                _botSrv.SetButtonStatistics(button);
            }
            catch(Exception ex)
            {
                _log.Fatal($"An error occurred trying to set statistics for button={button}", ex);
            }
        }
        public void BuildMenu()
        {
            var inlineButton1 = InlineKeyboardButton.WithCallbackData("ההמלצה המועדפת שלנו", "1");
            var inlineButton2 = InlineKeyboardButton.WithCallbackData("המלצה למשחק שמתחיל בקרוב", "2");
            var inlineButton3 = InlineKeyboardButton.WithCallbackData("המלצה סינגל ביחס גבוה", "3");
            var inlineButton4 = InlineKeyboardButton.WithCallbackData("המלצה מותאמת אישית", "4");
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("חזרה לשאלה קודמת", "BACK");
            var baseMenu = new List<InlineKeyboardButton[]>();
            baseMenu.Add(new[] { inlineButton1 });
            baseMenu.Add(new[] { inlineButton2 });
            baseMenu.Add(new[] { inlineButton3 });
            baseMenu.Add(new[] { inlineButton4 });
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
    }
}
