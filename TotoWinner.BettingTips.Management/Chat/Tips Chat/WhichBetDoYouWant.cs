﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TotoWinner.Data;

namespace TotoWinner.BettingTips.Management.Chat
{
    public class WhichBetDoYouWant : BaseChatItem, IChatItem
    {
        public IChatItem NextChat { get; private set; }
        public SportIds Sport;
        public string Country;
        private List<Tip> CurrentStack;

        public WhichBetDoYouWant(TelegramBotClient client, long subscriberId, SportIds sport, string country, double waitForAnswerInterval, 
            List<int> alreadySentTipsToday, List<Tip> currentStack)
        {
            this.BotClient = client;
            this.SubscriberId = subscriberId;
            this.WaitForAnswerInterval = waitForAnswerInterval;
            this.NextChat = null;
            this.Sport = sport;
            this.AlreadySentTipsToday = alreadySentTipsToday;
            this.CurrentStack = currentStack;
            this.Country = country;
            BuildMenu();
        }

        public async Task Show()
        {
            ShowMenu();
            if (!IsActiveMenu())
                return;
            await Task.Delay(500);
            var message = await BotClient.SendTextMessageAsync(this.SubscriberId, $"בקשת המלצה מ{this.Country},אנא בחר המלצה של משחק ספציפי",
                Telegram.Bot.Types.Enums.ParseMode.Markdown,
                false,
                false,
                0,
                this.Menu);
            this._messageId = message.MessageId;
        }

        public async Task PostUserAction(string callbackSelectedTipId)
        {
            if (!IsActiveMenu())
                return;

            await HideMenu();
            switch (callbackSelectedTipId)
            {
                case "BACK":
                    this.NextChat = new WhichCountryDoYouWant(this.BotClient, this.SubscriberId, this.Sport, 
                        this.WaitForAnswerInterval, this.AlreadySentTipsToday, this.CurrentStack);
                    await this.NextChat.Show();
                    ChatRepository.AddChatItem(this.MessageId, this.NextChat.MessageId.Value, this.NextChat);
                    break;
                default:
                    var tipId = int.Parse(callbackSelectedTipId);
                    var tip = _btTipsSrv.GetTip(tipId);
                    _botSrv.SentTip(tip, this.SubscriberId, this.BotClient);
                    ChatRepository.ReleaseChatItems(this.MessageId.Value);
                    break;
            }
        }

        public void BuildMenu()
        {
            var filter = new BettingTipsFilter(BettingTipFilterType.Filtered);
            var bets = this.CurrentStack
                .Where(b => this.AlreadySentTipsToday != null && 
                        !this.AlreadySentTipsToday.Contains(b.TipId) && 
                        b.Sport == this.Sport && 
                        b.Country == this.Country && b.BetRate <= filter.MaxRate.Value && b.BetRate >= filter.MinRate.Value)
                .ToList();
            var baseMenu = new List<InlineKeyboardButton[]>();
            foreach (var bet in bets)
            {
                var inlineButton = InlineKeyboardButton.WithCallbackData(bet.BetName, bet.TipId.ToString());
                baseMenu.Add(new[] { inlineButton });
            }
            var inlineButtonSupport = InlineKeyboardButton.WithCallbackData("חזרה לשאלה הקודמת", "BACK");
            baseMenu.Add(new[] { inlineButtonSupport });
            this.Menu = new InlineKeyboardMarkup(baseMenu.ToArray());
            this.NumberOfNonChatItemsInMenu = 1;
        }
    }
}
