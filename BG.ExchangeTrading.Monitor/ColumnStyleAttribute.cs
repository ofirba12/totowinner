﻿using System;
using System.Drawing;

namespace BG.ExchangeTrading.Monitor
{
    public class ColumnStyleAttribute : Attribute
    {
        public int Width { get; set; }
        public string DateFormat { get; set; }
        public bool Hide { get; set; }
        public string DisplayName { get; set; }
        public bool DisableSort { get; set; }
        public bool ZebraOn { get; set; }
        public bool Freeze { get; set; }
        public bool FontBold { get; set; }
        public string FontColor { get; set; }

        //ColorAlgoLogic=1 => Number between 75-94 : Yellow, 95-100: Green
        //ColorAlgoLogic=2 => Number > 0  : Red
        public int ColorAlgoLogic { get; set; }
        //HeaderColor=1 => GreanYellow
        public int HeaderColor { get; set; }
    }
}