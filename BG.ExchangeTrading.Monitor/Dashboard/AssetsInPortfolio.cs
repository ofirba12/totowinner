﻿using BG.ExchangeTrading.Services.Persistance;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class AssetsInPortfolio : Form
    {
        private int tickerId;
        private List<PortfolioAssets_GetActive_Result> assets;

        public AssetsInPortfolio()
        {
            InitializeComponent();
        }

        public AssetsInPortfolio(int tickerId, List<PortfolioAssets_GetActive_Result> assets) : this() 
        {
            this.tickerId = tickerId;
            this.assets = assets;
            initData();
        }

        private void initData()
        {
            this.dataGridView.Rows.Clear();
            this.dataGridView.Columns.Clear();
            this.dataGridView.Refresh();
            var ticketAssets = assets.Where(x => x.TickerId == this.tickerId).ToList();
            MonitorUtils.BuildGrid<PortfolioAssets_GetActive_Result>(ticketAssets, this.dataGridView);

        }
    }
}
