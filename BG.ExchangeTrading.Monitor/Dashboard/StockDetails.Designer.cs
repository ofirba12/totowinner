﻿namespace BG.ExchangeTrading.Monitor
{
    partial class StockDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelStockTitle = new System.Windows.Forms.Label();
            this.tickerDataView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tickerDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelStockTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1093, 55);
            this.panel1.TabIndex = 0;
            // 
            // labelStockTitle
            // 
            this.labelStockTitle.AutoSize = true;
            this.labelStockTitle.Location = new System.Drawing.Point(31, 19);
            this.labelStockTitle.Name = "labelStockTitle";
            this.labelStockTitle.Size = new System.Drawing.Size(54, 13);
            this.labelStockTitle.TabIndex = 4;
            this.labelStockTitle.Text = "Stock title";
            // 
            // tickerDataView
            // 
            this.tickerDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tickerDataView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tickerDataView.Location = new System.Drawing.Point(0, 55);
            this.tickerDataView.Name = "tickerDataView";
            this.tickerDataView.Size = new System.Drawing.Size(1093, 531);
            this.tickerDataView.TabIndex = 2;
            // 
            // StockDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 586);
            this.Controls.Add(this.tickerDataView);
            this.Controls.Add(this.panel1);
            this.Name = "StockDetails";
            this.Text = "BG Exchange Trading";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tickerDataView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView tickerDataView;
        private System.Windows.Forms.Label labelStockTitle;
    }
}

