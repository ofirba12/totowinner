﻿namespace BG.ExchangeTrading.Monitor
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.labelTotalFairValue = new System.Windows.Forms.Label();
            this.labelFv = new System.Windows.Forms.Label();
            this.buttonApplyFilter = new System.Windows.Forms.Button();
            this.buttonShowAll = new System.Windows.Forms.Button();
            this.checkedListBoxGroupFilter = new System.Windows.Forms.CheckedListBox();
            this.buttonPositionsManager = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonAddTicker = new System.Windows.Forms.Button();
            this.dataGridViewStocks = new System.Windows.Forms.DataGridView();
            this.labelTotalExposure = new System.Windows.Forms.Label();
            this.labelTotalExposureValue = new System.Windows.Forms.Label();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStocks)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.labelTotalExposureValue);
            this.panelTop.Controls.Add(this.labelTotalExposure);
            this.panelTop.Controls.Add(this.labelTotalFairValue);
            this.panelTop.Controls.Add(this.labelFv);
            this.panelTop.Controls.Add(this.buttonApplyFilter);
            this.panelTop.Controls.Add(this.buttonShowAll);
            this.panelTop.Controls.Add(this.checkedListBoxGroupFilter);
            this.panelTop.Controls.Add(this.buttonPositionsManager);
            this.panelTop.Controls.Add(this.buttonRefresh);
            this.panelTop.Controls.Add(this.buttonAddTicker);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1457, 62);
            this.panelTop.TabIndex = 0;
            // 
            // labelTotalFairValue
            // 
            this.labelTotalFairValue.AutoSize = true;
            this.labelTotalFairValue.Location = new System.Drawing.Point(983, 25);
            this.labelTotalFairValue.Name = "labelTotalFairValue";
            this.labelTotalFairValue.Size = new System.Drawing.Size(31, 13);
            this.labelTotalFairValue.TabIndex = 47;
            this.labelTotalFairValue.Text = "1000";
            // 
            // labelFv
            // 
            this.labelFv.AutoSize = true;
            this.labelFv.Location = new System.Drawing.Point(903, 25);
            this.labelFv.Name = "labelFv";
            this.labelFv.Size = new System.Drawing.Size(54, 13);
            this.labelFv.TabIndex = 46;
            this.labelFv.Text = "Fair Value";
            // 
            // buttonApplyFilter
            // 
            this.buttonApplyFilter.Location = new System.Drawing.Point(689, 7);
            this.buttonApplyFilter.Name = "buttonApplyFilter";
            this.buttonApplyFilter.Size = new System.Drawing.Size(75, 49);
            this.buttonApplyFilter.TabIndex = 5;
            this.buttonApplyFilter.Text = "Apply Filter";
            this.buttonApplyFilter.UseVisualStyleBackColor = true;
            this.buttonApplyFilter.Click += new System.EventHandler(this.buttonApplyFilter_Click);
            // 
            // buttonShowAll
            // 
            this.buttonShowAll.Location = new System.Drawing.Point(792, 7);
            this.buttonShowAll.Name = "buttonShowAll";
            this.buttonShowAll.Size = new System.Drawing.Size(75, 49);
            this.buttonShowAll.TabIndex = 4;
            this.buttonShowAll.Text = "Show All";
            this.buttonShowAll.UseVisualStyleBackColor = true;
            this.buttonShowAll.Click += new System.EventHandler(this.buttonShowAll_Click);
            // 
            // checkedListBoxGroupFilter
            // 
            this.checkedListBoxGroupFilter.CheckOnClick = true;
            this.checkedListBoxGroupFilter.FormattingEnabled = true;
            this.checkedListBoxGroupFilter.HorizontalScrollbar = true;
            this.checkedListBoxGroupFilter.Items.AddRange(new object[] {
            "CM",
            "STOCK",
            "INDEX",
            "COUNTRY",
            "BOND",
            "FX",
            "VIX",
            "OTHER"});
            this.checkedListBoxGroupFilter.Location = new System.Drawing.Point(318, 7);
            this.checkedListBoxGroupFilter.MultiColumn = true;
            this.checkedListBoxGroupFilter.Name = "checkedListBoxGroupFilter";
            this.checkedListBoxGroupFilter.Size = new System.Drawing.Size(365, 49);
            this.checkedListBoxGroupFilter.TabIndex = 3;
            // 
            // buttonPositionsManager
            // 
            this.buttonPositionsManager.Location = new System.Drawing.Point(166, 13);
            this.buttonPositionsManager.Name = "buttonPositionsManager";
            this.buttonPositionsManager.Size = new System.Drawing.Size(124, 23);
            this.buttonPositionsManager.TabIndex = 2;
            this.buttonPositionsManager.Text = "Positions Manager";
            this.buttonPositionsManager.UseVisualStyleBackColor = true;
            this.buttonPositionsManager.Click += new System.EventHandler(this.buttonPositionsManager_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRefresh.Location = new System.Drawing.Point(1391, 0);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(66, 62);
            this.buttonRefresh.TabIndex = 1;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonAddTicker
            // 
            this.buttonAddTicker.Location = new System.Drawing.Point(22, 13);
            this.buttonAddTicker.Name = "buttonAddTicker";
            this.buttonAddTicker.Size = new System.Drawing.Size(124, 23);
            this.buttonAddTicker.TabIndex = 0;
            this.buttonAddTicker.Text = "Add New Ticker";
            this.buttonAddTicker.UseVisualStyleBackColor = true;
            this.buttonAddTicker.Click += new System.EventHandler(this.buttonAddTicker_Click);
            // 
            // dataGridViewStocks
            // 
            this.dataGridViewStocks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStocks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewStocks.Location = new System.Drawing.Point(0, 62);
            this.dataGridViewStocks.Name = "dataGridViewStocks";
            this.dataGridViewStocks.Size = new System.Drawing.Size(1457, 388);
            this.dataGridViewStocks.TabIndex = 1;
            this.dataGridViewStocks.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStocks_CellMouseEnter);
            // 
            // labelTotalExposure
            // 
            this.labelTotalExposure.AutoSize = true;
            this.labelTotalExposure.Location = new System.Drawing.Point(1039, 25);
            this.labelTotalExposure.Name = "labelTotalExposure";
            this.labelTotalExposure.Size = new System.Drawing.Size(78, 13);
            this.labelTotalExposure.TabIndex = 48;
            this.labelTotalExposure.Text = "Total Exposure";
            // 
            // labelTotalExposureValue
            // 
            this.labelTotalExposureValue.AutoSize = true;
            this.labelTotalExposureValue.Location = new System.Drawing.Point(1123, 25);
            this.labelTotalExposureValue.Name = "labelTotalExposureValue";
            this.labelTotalExposureValue.Size = new System.Drawing.Size(31, 13);
            this.labelTotalExposureValue.TabIndex = 49;
            this.labelTotalExposureValue.Text = "1000";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1457, 450);
            this.Controls.Add(this.dataGridViewStocks);
            this.Controls.Add(this.panelTop);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStocks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.DataGridView dataGridViewStocks;
        private System.Windows.Forms.Button buttonAddTicker;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonPositionsManager;
        private System.Windows.Forms.CheckedListBox checkedListBoxGroupFilter;
        private System.Windows.Forms.Button buttonApplyFilter;
        private System.Windows.Forms.Button buttonShowAll;
        private System.Windows.Forms.Label labelTotalFairValue;
        private System.Windows.Forms.Label labelFv;
        private System.Windows.Forms.Label labelTotalExposureValue;
        private System.Windows.Forms.Label labelTotalExposure;
    }
}