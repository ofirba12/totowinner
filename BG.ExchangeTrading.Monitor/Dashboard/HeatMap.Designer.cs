﻿namespace BG.ExchangeTrading.Monitor
{
    partial class HeatMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.RulersTab = new System.Windows.Forms.TabPage();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownBuffer = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDownSell = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkedListBoxSellSource = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxSellTarget = new System.Windows.Forms.CheckedListBox();
            this.buttonSellAddItem = new System.Windows.Forms.Button();
            this.buttonSellMoveSelected = new System.Windows.Forms.Button();
            this.buttonSellRemoveItem = new System.Windows.Forms.Button();
            this.buttonSellSelectAll = new System.Windows.Forms.Button();
            this.buttonSellDeselectAll = new System.Windows.Forms.Button();
            this.buttonSellDeselect = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericUpDownBuy = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkedListBoxBuySource = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxBuyTarget = new System.Windows.Forms.CheckedListBox();
            this.buttonBuyAddItem = new System.Windows.Forms.Button();
            this.buttonBuyMoveSelected = new System.Windows.Forms.Button();
            this.buttonBuyRemoveItem = new System.Windows.Forms.Button();
            this.buttonBuySelectAll = new System.Windows.Forms.Button();
            this.buttonBuyDeselectAll = new System.Windows.Forms.Button();
            this.buttonBuyDeselect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownMaOptions = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListBoxMASource = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxMATarget = new System.Windows.Forms.CheckedListBox();
            this.buttonMaAddItem = new System.Windows.Forms.Button();
            this.buttonMaMoveSelected = new System.Windows.Forms.Button();
            this.buttonMaRemoveItem = new System.Windows.Forms.Button();
            this.buttonMaSelectAll = new System.Windows.Forms.Button();
            this.buttonMaDeselectAll = new System.Windows.Forms.Button();
            this.buttonMaDeselect = new System.Windows.Forms.Button();
            this.HeatMapDataTab = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonGenarateView = new System.Windows.Forms.Button();
            this.HeatMapViewTab = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewHeatMapData = new System.Windows.Forms.DataGridView();
            this.dataGridViewHeatmapView = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.RulersTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBuffer)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSell)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBuy)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaOptions)).BeginInit();
            this.HeatMapDataTab.SuspendLayout();
            this.panel1.SuspendLayout();
            this.HeatMapViewTab.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHeatMapData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHeatmapView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.RulersTab);
            this.tabControl1.Controls.Add(this.HeatMapDataTab);
            this.tabControl1.Controls.Add(this.HeatMapViewTab);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(892, 582);
            this.tabControl1.TabIndex = 0;
            // 
            // RulersTab
            // 
            this.RulersTab.Controls.Add(this.buttonGenerate);
            this.RulersTab.Controls.Add(this.label7);
            this.RulersTab.Controls.Add(this.numericUpDownBuffer);
            this.RulersTab.Controls.Add(this.groupBox3);
            this.RulersTab.Controls.Add(this.groupBox2);
            this.RulersTab.Controls.Add(this.groupBox1);
            this.RulersTab.Location = new System.Drawing.Point(4, 22);
            this.RulersTab.Name = "RulersTab";
            this.RulersTab.Padding = new System.Windows.Forms.Padding(3);
            this.RulersTab.Size = new System.Drawing.Size(884, 556);
            this.RulersTab.TabIndex = 0;
            this.RulersTab.Text = "RULERS";
            this.RulersTab.UseVisualStyleBackColor = true;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(383, 503);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerate.TabIndex = 12;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(354, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Buffer";
            // 
            // numericUpDownBuffer
            // 
            this.numericUpDownBuffer.Location = new System.Drawing.Point(395, 228);
            this.numericUpDownBuffer.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownBuffer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBuffer.Name = "numericUpDownBuffer";
            this.numericUpDownBuffer.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownBuffer.TabIndex = 14;
            this.numericUpDownBuffer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownBuffer.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownSell);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.checkedListBoxSellSource);
            this.groupBox3.Controls.Add(this.checkedListBoxSellTarget);
            this.groupBox3.Controls.Add(this.buttonSellAddItem);
            this.groupBox3.Controls.Add(this.buttonSellMoveSelected);
            this.groupBox3.Controls.Add(this.buttonSellRemoveItem);
            this.groupBox3.Controls.Add(this.buttonSellSelectAll);
            this.groupBox3.Controls.Add(this.buttonSellDeselectAll);
            this.groupBox3.Controls.Add(this.buttonSellDeselect);
            this.groupBox3.Location = new System.Drawing.Point(450, 268);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(407, 204);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sell Ruler";
            // 
            // numericUpDownSell
            // 
            this.numericUpDownSell.Location = new System.Drawing.Point(32, 20);
            this.numericUpDownSell.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownSell.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownSell.Name = "numericUpDownSell";
            this.numericUpDownSell.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownSell.TabIndex = 11;
            this.numericUpDownSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownSell.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(247, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Sell Points Chosen";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Sell Points Options";
            // 
            // checkedListBoxSellSource
            // 
            this.checkedListBoxSellSource.FormattingEnabled = true;
            this.checkedListBoxSellSource.Location = new System.Drawing.Point(6, 65);
            this.checkedListBoxSellSource.Name = "checkedListBoxSellSource";
            this.checkedListBoxSellSource.Size = new System.Drawing.Size(120, 124);
            this.checkedListBoxSellSource.TabIndex = 0;
            // 
            // checkedListBoxSellTarget
            // 
            this.checkedListBoxSellTarget.FormattingEnabled = true;
            this.checkedListBoxSellTarget.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40"});
            this.checkedListBoxSellTarget.Location = new System.Drawing.Point(250, 65);
            this.checkedListBoxSellTarget.Name = "checkedListBoxSellTarget";
            this.checkedListBoxSellTarget.Size = new System.Drawing.Size(120, 124);
            this.checkedListBoxSellTarget.TabIndex = 1;
            // 
            // buttonSellAddItem
            // 
            this.buttonSellAddItem.Location = new System.Drawing.Point(101, 18);
            this.buttonSellAddItem.Name = "buttonSellAddItem";
            this.buttonSellAddItem.Size = new System.Drawing.Size(24, 23);
            this.buttonSellAddItem.TabIndex = 7;
            this.buttonSellAddItem.Text = "+";
            this.buttonSellAddItem.UseVisualStyleBackColor = true;
            this.buttonSellAddItem.Click += new System.EventHandler(this.buttonSellAddItem_Click);
            // 
            // buttonSellMoveSelected
            // 
            this.buttonSellMoveSelected.Location = new System.Drawing.Point(152, 65);
            this.buttonSellMoveSelected.Name = "buttonSellMoveSelected";
            this.buttonSellMoveSelected.Size = new System.Drawing.Size(75, 23);
            this.buttonSellMoveSelected.TabIndex = 2;
            this.buttonSellMoveSelected.Text = ">";
            this.buttonSellMoveSelected.UseVisualStyleBackColor = true;
            this.buttonSellMoveSelected.Click += new System.EventHandler(this.buttonSellMoveSelected_Click);
            // 
            // buttonSellRemoveItem
            // 
            this.buttonSellRemoveItem.Location = new System.Drawing.Point(5, 18);
            this.buttonSellRemoveItem.Name = "buttonSellRemoveItem";
            this.buttonSellRemoveItem.Size = new System.Drawing.Size(21, 23);
            this.buttonSellRemoveItem.TabIndex = 6;
            this.buttonSellRemoveItem.Text = "-";
            this.buttonSellRemoveItem.UseVisualStyleBackColor = true;
            this.buttonSellRemoveItem.Click += new System.EventHandler(this.buttonSellRemoveItem_Click);
            // 
            // buttonSellSelectAll
            // 
            this.buttonSellSelectAll.Location = new System.Drawing.Point(152, 94);
            this.buttonSellSelectAll.Name = "buttonSellSelectAll";
            this.buttonSellSelectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonSellSelectAll.TabIndex = 3;
            this.buttonSellSelectAll.Text = ">>";
            this.buttonSellSelectAll.UseVisualStyleBackColor = true;
            this.buttonSellSelectAll.Click += new System.EventHandler(this.buttonSellSelectAll_Click);
            // 
            // buttonSellDeselectAll
            // 
            this.buttonSellDeselectAll.Location = new System.Drawing.Point(152, 168);
            this.buttonSellDeselectAll.Name = "buttonSellDeselectAll";
            this.buttonSellDeselectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonSellDeselectAll.TabIndex = 5;
            this.buttonSellDeselectAll.Text = "<<";
            this.buttonSellDeselectAll.UseVisualStyleBackColor = true;
            this.buttonSellDeselectAll.Click += new System.EventHandler(this.buttonSellDeselectAll_Click);
            // 
            // buttonSellDeselect
            // 
            this.buttonSellDeselect.Location = new System.Drawing.Point(152, 139);
            this.buttonSellDeselect.Name = "buttonSellDeselect";
            this.buttonSellDeselect.Size = new System.Drawing.Size(75, 23);
            this.buttonSellDeselect.TabIndex = 4;
            this.buttonSellDeselect.Text = "<";
            this.buttonSellDeselect.UseVisualStyleBackColor = true;
            this.buttonSellDeselect.Click += new System.EventHandler(this.buttonSellDeselect_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericUpDownBuy);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.checkedListBoxBuySource);
            this.groupBox2.Controls.Add(this.checkedListBoxBuyTarget);
            this.groupBox2.Controls.Add(this.buttonBuyAddItem);
            this.groupBox2.Controls.Add(this.buttonBuyMoveSelected);
            this.groupBox2.Controls.Add(this.buttonBuyRemoveItem);
            this.groupBox2.Controls.Add(this.buttonBuySelectAll);
            this.groupBox2.Controls.Add(this.buttonBuyDeselectAll);
            this.groupBox2.Controls.Add(this.buttonBuyDeselect);
            this.groupBox2.Location = new System.Drawing.Point(8, 268);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(407, 204);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Buy Ruler";
            // 
            // numericUpDownBuy
            // 
            this.numericUpDownBuy.Location = new System.Drawing.Point(32, 20);
            this.numericUpDownBuy.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownBuy.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownBuy.Name = "numericUpDownBuy";
            this.numericUpDownBuy.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownBuy.TabIndex = 11;
            this.numericUpDownBuy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownBuy.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(247, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Buy Points Chosen";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Buy Points Options";
            // 
            // checkedListBoxBuySource
            // 
            this.checkedListBoxBuySource.FormattingEnabled = true;
            this.checkedListBoxBuySource.Location = new System.Drawing.Point(6, 65);
            this.checkedListBoxBuySource.Name = "checkedListBoxBuySource";
            this.checkedListBoxBuySource.Size = new System.Drawing.Size(120, 124);
            this.checkedListBoxBuySource.TabIndex = 0;
            // 
            // checkedListBoxBuyTarget
            // 
            this.checkedListBoxBuyTarget.FormattingEnabled = true;
            this.checkedListBoxBuyTarget.Items.AddRange(new object[] {
            "-10",
            "-15",
            "-20",
            "-25",
            "-30",
            "-35",
            "-40"});
            this.checkedListBoxBuyTarget.Location = new System.Drawing.Point(250, 65);
            this.checkedListBoxBuyTarget.Name = "checkedListBoxBuyTarget";
            this.checkedListBoxBuyTarget.Size = new System.Drawing.Size(120, 124);
            this.checkedListBoxBuyTarget.TabIndex = 1;
            // 
            // buttonBuyAddItem
            // 
            this.buttonBuyAddItem.Location = new System.Drawing.Point(101, 18);
            this.buttonBuyAddItem.Name = "buttonBuyAddItem";
            this.buttonBuyAddItem.Size = new System.Drawing.Size(24, 23);
            this.buttonBuyAddItem.TabIndex = 7;
            this.buttonBuyAddItem.Text = "+";
            this.buttonBuyAddItem.UseVisualStyleBackColor = true;
            this.buttonBuyAddItem.Click += new System.EventHandler(this.buttonBuyAddItem_Click);
            // 
            // buttonBuyMoveSelected
            // 
            this.buttonBuyMoveSelected.Location = new System.Drawing.Point(152, 65);
            this.buttonBuyMoveSelected.Name = "buttonBuyMoveSelected";
            this.buttonBuyMoveSelected.Size = new System.Drawing.Size(75, 23);
            this.buttonBuyMoveSelected.TabIndex = 2;
            this.buttonBuyMoveSelected.Text = ">";
            this.buttonBuyMoveSelected.UseVisualStyleBackColor = true;
            this.buttonBuyMoveSelected.Click += new System.EventHandler(this.buttonBuyMoveSelected_Click);
            // 
            // buttonBuyRemoveItem
            // 
            this.buttonBuyRemoveItem.Location = new System.Drawing.Point(5, 18);
            this.buttonBuyRemoveItem.Name = "buttonBuyRemoveItem";
            this.buttonBuyRemoveItem.Size = new System.Drawing.Size(21, 23);
            this.buttonBuyRemoveItem.TabIndex = 6;
            this.buttonBuyRemoveItem.Text = "-";
            this.buttonBuyRemoveItem.UseVisualStyleBackColor = true;
            this.buttonBuyRemoveItem.Click += new System.EventHandler(this.buttonBuyRemoveItem_Click);
            // 
            // buttonBuySelectAll
            // 
            this.buttonBuySelectAll.Location = new System.Drawing.Point(152, 94);
            this.buttonBuySelectAll.Name = "buttonBuySelectAll";
            this.buttonBuySelectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonBuySelectAll.TabIndex = 3;
            this.buttonBuySelectAll.Text = ">>";
            this.buttonBuySelectAll.UseVisualStyleBackColor = true;
            this.buttonBuySelectAll.Click += new System.EventHandler(this.buttonBuySelectAll_Click);
            // 
            // buttonBuyDeselectAll
            // 
            this.buttonBuyDeselectAll.Location = new System.Drawing.Point(152, 168);
            this.buttonBuyDeselectAll.Name = "buttonBuyDeselectAll";
            this.buttonBuyDeselectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonBuyDeselectAll.TabIndex = 5;
            this.buttonBuyDeselectAll.Text = "<<";
            this.buttonBuyDeselectAll.UseVisualStyleBackColor = true;
            this.buttonBuyDeselectAll.Click += new System.EventHandler(this.buttonBuyDeselectAll_Click);
            // 
            // buttonBuyDeselect
            // 
            this.buttonBuyDeselect.Location = new System.Drawing.Point(152, 139);
            this.buttonBuyDeselect.Name = "buttonBuyDeselect";
            this.buttonBuyDeselect.Size = new System.Drawing.Size(75, 23);
            this.buttonBuyDeselect.TabIndex = 4;
            this.buttonBuyDeselect.Text = "<";
            this.buttonBuyDeselect.UseVisualStyleBackColor = true;
            this.buttonBuyDeselect.Click += new System.EventHandler(this.buttonBuyDeselect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownMaOptions);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkedListBoxMASource);
            this.groupBox1.Controls.Add(this.checkedListBoxMATarget);
            this.groupBox1.Controls.Add(this.buttonMaAddItem);
            this.groupBox1.Controls.Add(this.buttonMaMoveSelected);
            this.groupBox1.Controls.Add(this.buttonMaRemoveItem);
            this.groupBox1.Controls.Add(this.buttonMaSelectAll);
            this.groupBox1.Controls.Add(this.buttonMaDeselectAll);
            this.groupBox1.Controls.Add(this.buttonMaDeselect);
            this.groupBox1.Location = new System.Drawing.Point(243, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 204);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Moving Average Ruler";
            // 
            // numericUpDownMaOptions
            // 
            this.numericUpDownMaOptions.Location = new System.Drawing.Point(32, 20);
            this.numericUpDownMaOptions.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownMaOptions.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaOptions.Name = "numericUpDownMaOptions";
            this.numericUpDownMaOptions.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownMaOptions.TabIndex = 11;
            this.numericUpDownMaOptions.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownMaOptions.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Moving Average Chosen";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Moving Average Options";
            // 
            // checkedListBoxMASource
            // 
            this.checkedListBoxMASource.FormattingEnabled = true;
            this.checkedListBoxMASource.Location = new System.Drawing.Point(6, 65);
            this.checkedListBoxMASource.Name = "checkedListBoxMASource";
            this.checkedListBoxMASource.Size = new System.Drawing.Size(120, 124);
            this.checkedListBoxMASource.TabIndex = 0;
            this.checkedListBoxMASource.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxMASource_ItemCheck);
            // 
            // checkedListBoxMATarget
            // 
            this.checkedListBoxMATarget.FormattingEnabled = true;
            this.checkedListBoxMATarget.Items.AddRange(new object[] {
            "50",
            "75",
            "100",
            "125",
            "150",
            "175",
            "200"});
            this.checkedListBoxMATarget.Location = new System.Drawing.Point(250, 65);
            this.checkedListBoxMATarget.Name = "checkedListBoxMATarget";
            this.checkedListBoxMATarget.Size = new System.Drawing.Size(120, 124);
            this.checkedListBoxMATarget.TabIndex = 1;
            // 
            // buttonMaAddItem
            // 
            this.buttonMaAddItem.Location = new System.Drawing.Point(101, 18);
            this.buttonMaAddItem.Name = "buttonMaAddItem";
            this.buttonMaAddItem.Size = new System.Drawing.Size(24, 23);
            this.buttonMaAddItem.TabIndex = 7;
            this.buttonMaAddItem.Text = "+";
            this.buttonMaAddItem.UseVisualStyleBackColor = true;
            this.buttonMaAddItem.Click += new System.EventHandler(this.buttonMaAddItem_Click);
            // 
            // buttonMaMoveSelected
            // 
            this.buttonMaMoveSelected.Location = new System.Drawing.Point(152, 65);
            this.buttonMaMoveSelected.Name = "buttonMaMoveSelected";
            this.buttonMaMoveSelected.Size = new System.Drawing.Size(75, 23);
            this.buttonMaMoveSelected.TabIndex = 2;
            this.buttonMaMoveSelected.Text = ">";
            this.buttonMaMoveSelected.UseVisualStyleBackColor = true;
            this.buttonMaMoveSelected.Click += new System.EventHandler(this.buttonMaMoveSelected_Click);
            // 
            // buttonMaRemoveItem
            // 
            this.buttonMaRemoveItem.Location = new System.Drawing.Point(5, 18);
            this.buttonMaRemoveItem.Name = "buttonMaRemoveItem";
            this.buttonMaRemoveItem.Size = new System.Drawing.Size(21, 23);
            this.buttonMaRemoveItem.TabIndex = 6;
            this.buttonMaRemoveItem.Text = "-";
            this.buttonMaRemoveItem.UseVisualStyleBackColor = true;
            this.buttonMaRemoveItem.Click += new System.EventHandler(this.buttonMaRemoveItem_Click);
            // 
            // buttonMaSelectAll
            // 
            this.buttonMaSelectAll.Location = new System.Drawing.Point(152, 94);
            this.buttonMaSelectAll.Name = "buttonMaSelectAll";
            this.buttonMaSelectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonMaSelectAll.TabIndex = 3;
            this.buttonMaSelectAll.Text = ">>";
            this.buttonMaSelectAll.UseVisualStyleBackColor = true;
            this.buttonMaSelectAll.Click += new System.EventHandler(this.buttonMaSelectAll_Click);
            // 
            // buttonMaDeselectAll
            // 
            this.buttonMaDeselectAll.Location = new System.Drawing.Point(152, 168);
            this.buttonMaDeselectAll.Name = "buttonMaDeselectAll";
            this.buttonMaDeselectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonMaDeselectAll.TabIndex = 5;
            this.buttonMaDeselectAll.Text = "<<";
            this.buttonMaDeselectAll.UseVisualStyleBackColor = true;
            this.buttonMaDeselectAll.Click += new System.EventHandler(this.buttonMaDeselectAll_Click);
            // 
            // buttonMaDeselect
            // 
            this.buttonMaDeselect.Location = new System.Drawing.Point(152, 139);
            this.buttonMaDeselect.Name = "buttonMaDeselect";
            this.buttonMaDeselect.Size = new System.Drawing.Size(75, 23);
            this.buttonMaDeselect.TabIndex = 4;
            this.buttonMaDeselect.Text = "<";
            this.buttonMaDeselect.UseVisualStyleBackColor = true;
            this.buttonMaDeselect.Click += new System.EventHandler(this.buttonMaDeselect_Click);
            // 
            // HeatMapDataTab
            // 
            this.HeatMapDataTab.Controls.Add(this.panel2);
            this.HeatMapDataTab.Controls.Add(this.panel1);
            this.HeatMapDataTab.Location = new System.Drawing.Point(4, 22);
            this.HeatMapDataTab.Name = "HeatMapDataTab";
            this.HeatMapDataTab.Padding = new System.Windows.Forms.Padding(3);
            this.HeatMapDataTab.Size = new System.Drawing.Size(884, 556);
            this.HeatMapDataTab.TabIndex = 1;
            this.HeatMapDataTab.Text = "HEATMAP DATA";
            this.HeatMapDataTab.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonGenarateView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(878, 39);
            this.panel1.TabIndex = 1;
            // 
            // buttonGenarateView
            // 
            this.buttonGenarateView.Location = new System.Drawing.Point(5, 6);
            this.buttonGenarateView.Name = "buttonGenarateView";
            this.buttonGenarateView.Size = new System.Drawing.Size(250, 23);
            this.buttonGenarateView.TabIndex = 0;
            this.buttonGenarateView.Text = "GENERATE HEATMAP VIEW";
            this.buttonGenarateView.UseVisualStyleBackColor = true;
            this.buttonGenarateView.Click += new System.EventHandler(this.buttonGenarateView_Click);
            // 
            // HeatMapViewTab
            // 
            this.HeatMapViewTab.Controls.Add(this.dataGridViewHeatmapView);
            this.HeatMapViewTab.Location = new System.Drawing.Point(4, 22);
            this.HeatMapViewTab.Name = "HeatMapViewTab";
            this.HeatMapViewTab.Size = new System.Drawing.Size(884, 556);
            this.HeatMapViewTab.TabIndex = 2;
            this.HeatMapViewTab.Text = "HEATMAP VIEW";
            this.HeatMapViewTab.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewHeatMapData);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 42);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(878, 511);
            this.panel2.TabIndex = 2;
            // 
            // dataGridViewHeatMapData
            // 
            this.dataGridViewHeatMapData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHeatMapData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewHeatMapData.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewHeatMapData.Name = "dataGridViewHeatMapData";
            this.dataGridViewHeatMapData.Size = new System.Drawing.Size(878, 511);
            this.dataGridViewHeatMapData.TabIndex = 1;
            // 
            // dataGridViewHeatmapView
            // 
            this.dataGridViewHeatmapView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHeatmapView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewHeatmapView.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewHeatmapView.Name = "dataGridViewHeatmapView";
            this.dataGridViewHeatmapView.Size = new System.Drawing.Size(884, 556);
            this.dataGridViewHeatmapView.TabIndex = 2;
            this.dataGridViewHeatmapView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHeatmapView_CellMouseEnter);
            // 
            // HeatMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 582);
            this.Controls.Add(this.tabControl1);
            this.Name = "HeatMap";
            this.Text = "HeatMap";
            this.tabControl1.ResumeLayout(false);
            this.RulersTab.ResumeLayout(false);
            this.RulersTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBuffer)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSell)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBuy)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaOptions)).EndInit();
            this.HeatMapDataTab.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.HeatMapViewTab.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHeatMapData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHeatmapView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage RulersTab;
        private System.Windows.Forms.TabPage HeatMapDataTab;
        private System.Windows.Forms.CheckedListBox checkedListBoxMATarget;
        private System.Windows.Forms.CheckedListBox checkedListBoxMASource;
        private System.Windows.Forms.Button buttonMaDeselectAll;
        private System.Windows.Forms.Button buttonMaDeselect;
        private System.Windows.Forms.Button buttonMaSelectAll;
        private System.Windows.Forms.Button buttonMaMoveSelected;
        private System.Windows.Forms.Button buttonMaAddItem;
        private System.Windows.Forms.Button buttonMaRemoveItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownMaOptions;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericUpDownBuy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox checkedListBoxBuySource;
        private System.Windows.Forms.CheckedListBox checkedListBoxBuyTarget;
        private System.Windows.Forms.Button buttonBuyAddItem;
        private System.Windows.Forms.Button buttonBuyMoveSelected;
        private System.Windows.Forms.Button buttonBuyRemoveItem;
        private System.Windows.Forms.Button buttonBuySelectAll;
        private System.Windows.Forms.Button buttonBuyDeselectAll;
        private System.Windows.Forms.Button buttonBuyDeselect;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDownSell;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox checkedListBoxSellSource;
        private System.Windows.Forms.CheckedListBox checkedListBoxSellTarget;
        private System.Windows.Forms.Button buttonSellAddItem;
        private System.Windows.Forms.Button buttonSellMoveSelected;
        private System.Windows.Forms.Button buttonSellRemoveItem;
        private System.Windows.Forms.Button buttonSellSelectAll;
        private System.Windows.Forms.Button buttonSellDeselectAll;
        private System.Windows.Forms.Button buttonSellDeselect;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownBuffer;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonGenarateView;
        private System.Windows.Forms.TabPage HeatMapViewTab;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewHeatMapData;
        private System.Windows.Forms.DataGridView dataGridViewHeatmapView;
    }
}