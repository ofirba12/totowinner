﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class StockDetails : Form
    {
        public StockDetails(TickerInfoDisplay ticker)
        {
            InitializeComponent();
            this.labelStockTitle.Text = $"{ticker.TickerId} -> {ticker.Symbol} : {ticker.FullName}";
            DisplayTickerData(ticker.TickerId);
        }

        //        var ticker = TickerServices.Instance.ConvertShortcutToTicker(this.textBoxTicker.Text);
        private void DisplayTickerData(int tickerId)
        {
            var data = TickerServices.Instance.GetTickerData(tickerId)
                .ConvertAll<TickerDataDisplay>(t => new TickerDataDisplay(t.Date,
                    t.ChangePercent, 
                    t.OpenPrice, 
                    t.ClosePrice, 
                    t.High, 
                    t.Low, 
                    t.Volume, 
                    t.LastUpdate))
                .OrderByDescending(t => t.Date)
                .ToList();
            this.tickerDataView.Rows.Clear();
            this.tickerDataView.Columns.Clear();
            this.tickerDataView.Refresh();

            MonitorUtils.BuildGrid<TickerDataDisplay>(data, this.tickerDataView);
        }
    }
}
