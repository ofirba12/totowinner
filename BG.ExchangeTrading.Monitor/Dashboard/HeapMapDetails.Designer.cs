﻿namespace BG.ExchangeTrading.Monitor
{
    partial class HeapMapDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewHeatmapDetails = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHeatmapDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewHeatmapDetails
            // 
            this.dataGridViewHeatmapDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHeatmapDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewHeatmapDetails.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewHeatmapDetails.Name = "dataGridViewHeatmapDetails";
            this.dataGridViewHeatmapDetails.Size = new System.Drawing.Size(800, 450);
            this.dataGridViewHeatmapDetails.TabIndex = 3;
            // 
            // HeapMapDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridViewHeatmapDetails);
            this.Name = "HeapMapDetails";
            this.Text = "HeapMapDetails";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHeatmapDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewHeatmapDetails;
    }
}