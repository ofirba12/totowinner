﻿using BG.ExchangeTrading.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class AddNewTicker : Form
    {
        public AddNewTicker()
        {
            InitializeComponent();
            var tickers = TickerServices.Instance.GetAvailableTickers();
            var existingTickers = TickersPersistanceServices.Instance.GetExisitingTickers();
            var newTickers = tickers.Where(t => !existingTickers.Any(a => a.Symbol == t.Symbol)).ToList();
            SetAutoComplete(newTickers, this.textBoxTicker);


        }
        private void SetAutoComplete(IEnumerable<Data.Ticker> collection, TextBox textBox)
        {
            var source = new AutoCompleteStringCollection();
            source.AddRange(collection.Select(m => $"{m.Symbol} : {m.FullName}").Distinct().ToArray());
            textBox.AutoCompleteCustomSource = source;
            textBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }
        private async void buttonAddTicker_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.textBoxTicker.Text))
                {
                    MessageBox.Show("Ticket is missing");
                    return;
                }
                var ticker = TickerServices.Instance.ConvertShortcutToTicker(this.textBoxTicker.Text);
                var symbol = TickerServices.Instance.GetFullSymbol(ticker.Symbol);
                //var symbol = ticker.Symbol == "VIX"
                //    ? $"{ticker.Symbol}.INDX"
                //    : $"{ticker.Symbol}.US";
                var yesNoDialog = MessageBox.Show($"Ticket {symbol} was chosen to be added, are you sure you want to proceed?", $"Add Ticker {symbol}", MessageBoxButtons.YesNo);
                if (yesNoDialog == DialogResult.Yes)
                {
                    var historicalStockPrice = await TickerServices.Instance.FetchEofStockPrice(symbol,
                        DateTime.Today.AddYears(-6), DateTime.Today);
                    if (historicalStockPrice.Count > 0)
                    {
                        var tickerId = TickerServices.Instance.AddTickerWithHistoricalData(ticker.Symbol, 
                            ticker.FullName, 
                            this.comboBoxType.Text,
                            this.comboBoxGroup.Text,
                            this.textBoxUrl.Text,
                            historicalStockPrice);
                        MessageBox.Show($"{historicalStockPrice.Count} historical {symbol} has been saved [{historicalStockPrice.First().Date} - {historicalStockPrice.Last().Date}]");
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                    else
                        MessageBox.Show($"No historical price data found for this ticker [{symbol}]");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Something went wrong, {ex}");
            }
        }
    }
}
