﻿using BG.ExchangeTrading.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class HeapMapDetails : Form
    {
        public HeapMapDetails()
        {
            InitializeComponent();
        }
        public HeapMapDetails(EntryPointHeatMapDisplay entry, HeatMapRepository heatMap) : this()
        {
            if (entry.BuySell == "BUY")
            {
                ShowData(heatMap.BuyTriggers[entry.MovingAverage][entry.Trigger]);
            }
            else
            {
                ShowData(heatMap.SellTriggers[entry.MovingAverage][entry.Trigger]);
            }
        }

        private void ShowData(List<HeatmapEntriesRepository> heatmapEntriesRepositories)
        {
            this.dataGridViewHeatmapDetails.Rows.Clear();
            this.dataGridViewHeatmapDetails.Columns.Clear();
            this.dataGridViewHeatmapDetails.Refresh();

            MonitorUtils.BuildGrid<HeatmapEntriesRepositoryDisplay>(
                heatmapEntriesRepositories.ConvertAll<HeatmapEntriesRepositoryDisplay>(h => new HeatmapEntriesRepositoryDisplay(h.EntryDate,
                    h.EntrySpread,
                    h.EntryPrice,
                    h.BufferPrice,
                    h.ExitDate,
                    h.ExitPrice,
                    h.Win
                    ))
                , dataGridViewHeatmapDetails);
        }
    }
}
