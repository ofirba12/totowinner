﻿namespace BG.ExchangeTrading.Monitor
{
    partial class UpdateTickerInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelTickerId = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelSymbol = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelFullName = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.labelType = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericStockRating = new System.Windows.Forms.NumericUpDown();
            this.numericCap = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericFloor = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numericMa150CapPercent = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numericMa150FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.checkBoxStockRating = new System.Windows.Forms.CheckBox();
            this.checkBoxCap = new System.Windows.Forms.CheckBox();
            this.checkBoxFloor = new System.Windows.Forms.CheckBox();
            this.checkBoxMa150Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa150Floor = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxGroup = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numericBuffer = new System.Windows.Forms.NumericUpDown();
            this.checkBoxBuffer = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericMa50CapPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa50FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.checkBoxMa50Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa50Floor = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.numericMa75CapPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa100CapPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa125CapPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa175CapPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa200CapPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa75FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa100FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa125FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa175FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.numericMa200FloorPercent = new System.Windows.Forms.NumericUpDown();
            this.checkBoxMa75Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa100Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa125Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa175Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa200Cap = new System.Windows.Forms.CheckBox();
            this.checkBoxMa75Floor = new System.Windows.Forms.CheckBox();
            this.checkBoxMa100Floor = new System.Windows.Forms.CheckBox();
            this.checkBoxMa125Floor = new System.Windows.Forms.CheckBox();
            this.checkBoxMa175Floor = new System.Windows.Forms.CheckBox();
            this.checkBoxMa200Floor = new System.Windows.Forms.CheckBox();
            this.checkBoxEditAll = new System.Windows.Forms.CheckBox();
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericStockRating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa150CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa150FloorPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBuffer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa50CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa50FloorPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa75CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa100CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa125CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa175CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa200CapPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa75FloorPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa100FloorPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa125FloorPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa175FloorPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa200FloorPercent)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TICKER ID";
            // 
            // labelTickerId
            // 
            this.labelTickerId.AutoSize = true;
            this.labelTickerId.Location = new System.Drawing.Point(146, 31);
            this.labelTickerId.Name = "labelTickerId";
            this.labelTickerId.Size = new System.Drawing.Size(68, 13);
            this.labelTickerId.TabIndex = 1000;
            this.labelTickerId.Text = "labelTickerId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "SYMBOL";
            // 
            // labelSymbol
            // 
            this.labelSymbol.AutoSize = true;
            this.labelSymbol.Location = new System.Drawing.Point(146, 56);
            this.labelSymbol.Name = "labelSymbol";
            this.labelSymbol.Size = new System.Drawing.Size(63, 13);
            this.labelSymbol.TabIndex = 300;
            this.labelSymbol.Text = "labelSymbol";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "FULL NAME";
            // 
            // labelFullName
            // 
            this.labelFullName.AutoSize = true;
            this.labelFullName.Location = new System.Drawing.Point(146, 80);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(73, 13);
            this.labelFullName.TabIndex = 500;
            this.labelFullName.Text = "labelFullName";
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Stock",
            "ETF"});
            this.comboBoxType.Location = new System.Drawing.Point(149, 109);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxType.TabIndex = 1;
            this.comboBoxType.Text = "Stock";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(39, 112);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(35, 13);
            this.labelType.TabIndex = 8;
            this.labelType.Text = "TYPE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "STOCK RATING";
            // 
            // numericStockRating
            // 
            this.numericStockRating.DecimalPlaces = 1;
            this.numericStockRating.Location = new System.Drawing.Point(149, 185);
            this.numericStockRating.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericStockRating.Name = "numericStockRating";
            this.numericStockRating.Size = new System.Drawing.Size(85, 20);
            this.numericStockRating.TabIndex = 3;
            this.numericStockRating.Enter += new System.EventHandler(this.numericStockRating_Enter);
            // 
            // numericCap
            // 
            this.numericCap.DecimalPlaces = 1;
            this.numericCap.Location = new System.Drawing.Point(149, 242);
            this.numericCap.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericCap.Name = "numericCap";
            this.numericCap.Size = new System.Drawing.Size(85, 20);
            this.numericCap.TabIndex = 7;
            this.numericCap.Enter += new System.EventHandler(this.numericCap_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(39, 242);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "CAP";
            // 
            // numericFloor
            // 
            this.numericFloor.DecimalPlaces = 1;
            this.numericFloor.Location = new System.Drawing.Point(149, 268);
            this.numericFloor.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericFloor.Name = "numericFloor";
            this.numericFloor.Size = new System.Drawing.Size(85, 20);
            this.numericFloor.TabIndex = 9;
            this.numericFloor.Enter += new System.EventHandler(this.numericFloor_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 268);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "FLOOR";
            // 
            // numericMa150CapPercent
            // 
            this.numericMa150CapPercent.DecimalPlaces = 1;
            this.numericMa150CapPercent.Location = new System.Drawing.Point(539, 238);
            this.numericMa150CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa150CapPercent.Name = "numericMa150CapPercent";
            this.numericMa150CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa150CapPercent.TabIndex = 27;
            this.numericMa150CapPercent.ValueChanged += new System.EventHandler(this.numericMa150CapPercent_ValueChanged);
            this.numericMa150CapPercent.Enter += new System.EventHandler(this.numericMa150CapPercent_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(429, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "MA150 CAP%";
            // 
            // numericMa150FloorPercent
            // 
            this.numericMa150FloorPercent.DecimalPlaces = 1;
            this.numericMa150FloorPercent.Location = new System.Drawing.Point(539, 264);
            this.numericMa150FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa150FloorPercent.Name = "numericMa150FloorPercent";
            this.numericMa150FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa150FloorPercent.TabIndex = 29;
            this.numericMa150FloorPercent.ValueChanged += new System.EventHandler(this.numericMa150FloorPercent_ValueChanged);
            this.numericMa150FloorPercent.Enter += new System.EventHandler(this.numericMa150FloorPercent_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(429, 267);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "MA150 FLOOR%";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(263, 450);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(151, 23);
            this.buttonSave.TabIndex = 40;
            this.buttonSave.Text = "SAVE";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // checkBoxStockRating
            // 
            this.checkBoxStockRating.AutoSize = true;
            this.checkBoxStockRating.Checked = true;
            this.checkBoxStockRating.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStockRating.Location = new System.Drawing.Point(263, 185);
            this.checkBoxStockRating.Name = "checkBoxStockRating";
            this.checkBoxStockRating.Size = new System.Drawing.Size(54, 17);
            this.checkBoxStockRating.TabIndex = 4;
            this.checkBoxStockRating.Text = "Unset";
            this.checkBoxStockRating.UseVisualStyleBackColor = true;
            this.checkBoxStockRating.CheckedChanged += new System.EventHandler(this.checkBoxStockRating_CheckedChanged);
            // 
            // checkBoxCap
            // 
            this.checkBoxCap.AutoSize = true;
            this.checkBoxCap.Checked = true;
            this.checkBoxCap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCap.Location = new System.Drawing.Point(263, 245);
            this.checkBoxCap.Name = "checkBoxCap";
            this.checkBoxCap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxCap.TabIndex = 8;
            this.checkBoxCap.Text = "Unset";
            this.checkBoxCap.UseVisualStyleBackColor = true;
            this.checkBoxCap.CheckedChanged += new System.EventHandler(this.checkBoxCap_CheckedChanged);
            // 
            // checkBoxFloor
            // 
            this.checkBoxFloor.AutoSize = true;
            this.checkBoxFloor.Checked = true;
            this.checkBoxFloor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFloor.Location = new System.Drawing.Point(263, 271);
            this.checkBoxFloor.Name = "checkBoxFloor";
            this.checkBoxFloor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxFloor.TabIndex = 10;
            this.checkBoxFloor.Text = "Unset";
            this.checkBoxFloor.UseVisualStyleBackColor = true;
            this.checkBoxFloor.CheckedChanged += new System.EventHandler(this.checkBoxFloor_CheckedChanged);
            // 
            // checkBoxMa150Cap
            // 
            this.checkBoxMa150Cap.AutoSize = true;
            this.checkBoxMa150Cap.Checked = true;
            this.checkBoxMa150Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa150Cap.Location = new System.Drawing.Point(653, 241);
            this.checkBoxMa150Cap.Name = "checkBoxMa150Cap";
            this.checkBoxMa150Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa150Cap.TabIndex = 28;
            this.checkBoxMa150Cap.Text = "Unset";
            this.checkBoxMa150Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa150Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa150Cap_CheckedChanged);
            // 
            // checkBoxMa150Floor
            // 
            this.checkBoxMa150Floor.AutoSize = true;
            this.checkBoxMa150Floor.Checked = true;
            this.checkBoxMa150Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa150Floor.Location = new System.Drawing.Point(653, 265);
            this.checkBoxMa150Floor.Name = "checkBoxMa150Floor";
            this.checkBoxMa150Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa150Floor.TabIndex = 30;
            this.checkBoxMa150Floor.Text = "Unset";
            this.checkBoxMa150Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa150Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa150Floor_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "GROUP";
            // 
            // comboBoxGroup
            // 
            this.comboBoxGroup.FormattingEnabled = true;
            this.comboBoxGroup.Items.AddRange(new object[] {
            "CM",
            "STOCK",
            "INDEX",
            "COUNTRY",
            "BOND",
            "FX",
            "OTHER",
            "VIX"});
            this.comboBoxGroup.Location = new System.Drawing.Point(149, 142);
            this.comboBoxGroup.Name = "comboBoxGroup";
            this.comboBoxGroup.Size = new System.Drawing.Size(121, 21);
            this.comboBoxGroup.TabIndex = 2;
            this.comboBoxGroup.Text = "CM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "BUFFER";
            // 
            // numericBuffer
            // 
            this.numericBuffer.DecimalPlaces = 1;
            this.numericBuffer.Location = new System.Drawing.Point(149, 210);
            this.numericBuffer.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericBuffer.Name = "numericBuffer";
            this.numericBuffer.Size = new System.Drawing.Size(85, 20);
            this.numericBuffer.TabIndex = 5;
            this.numericBuffer.Enter += new System.EventHandler(this.numericBuffer_Enter);
            // 
            // checkBoxBuffer
            // 
            this.checkBoxBuffer.AutoSize = true;
            this.checkBoxBuffer.Checked = true;
            this.checkBoxBuffer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBuffer.Location = new System.Drawing.Point(263, 212);
            this.checkBoxBuffer.Name = "checkBoxBuffer";
            this.checkBoxBuffer.Size = new System.Drawing.Size(54, 17);
            this.checkBoxBuffer.TabIndex = 6;
            this.checkBoxBuffer.Text = "Unset";
            this.checkBoxBuffer.UseVisualStyleBackColor = true;
            this.checkBoxBuffer.CheckedChanged += new System.EventHandler(this.checkBoxBuffer_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(429, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "MA50 CAP%";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(429, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "MA50 FLOOR%";
            // 
            // numericMa50CapPercent
            // 
            this.numericMa50CapPercent.DecimalPlaces = 1;
            this.numericMa50CapPercent.Location = new System.Drawing.Point(539, 29);
            this.numericMa50CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa50CapPercent.Name = "numericMa50CapPercent";
            this.numericMa50CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa50CapPercent.TabIndex = 11;
            this.numericMa50CapPercent.ValueChanged += new System.EventHandler(this.numericMa50CapPercent_ValueChanged);
            this.numericMa50CapPercent.Enter += new System.EventHandler(this.numericMa50CapPercent_Enter);
            // 
            // numericMa50FloorPercent
            // 
            this.numericMa50FloorPercent.DecimalPlaces = 1;
            this.numericMa50FloorPercent.Location = new System.Drawing.Point(539, 55);
            this.numericMa50FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa50FloorPercent.Name = "numericMa50FloorPercent";
            this.numericMa50FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa50FloorPercent.TabIndex = 13;
            this.numericMa50FloorPercent.ValueChanged += new System.EventHandler(this.numericMa50FloorPercent_ValueChanged);
            this.numericMa50FloorPercent.Enter += new System.EventHandler(this.numericMa50FloorPercent_Enter);
            // 
            // checkBoxMa50Cap
            // 
            this.checkBoxMa50Cap.AutoSize = true;
            this.checkBoxMa50Cap.Checked = true;
            this.checkBoxMa50Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa50Cap.Location = new System.Drawing.Point(653, 32);
            this.checkBoxMa50Cap.Name = "checkBoxMa50Cap";
            this.checkBoxMa50Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa50Cap.TabIndex = 12;
            this.checkBoxMa50Cap.Text = "Unset";
            this.checkBoxMa50Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa50Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa50Cap_CheckedChanged);
            // 
            // checkBoxMa50Floor
            // 
            this.checkBoxMa50Floor.AutoSize = true;
            this.checkBoxMa50Floor.Checked = true;
            this.checkBoxMa50Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa50Floor.Location = new System.Drawing.Point(653, 57);
            this.checkBoxMa50Floor.Name = "checkBoxMa50Floor";
            this.checkBoxMa50Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa50Floor.TabIndex = 14;
            this.checkBoxMa50Floor.Text = "Unset";
            this.checkBoxMa50Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa50Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa50Floor_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(429, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "MA75 CAP%";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(429, 134);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "MA100 CAP%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(429, 191);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "MA125 CAP%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(429, 298);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "MA175 CAP%";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(429, 355);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "MA200 CAP%";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(429, 108);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 41;
            this.label18.Text = "MA75 FLOOR%";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(429, 158);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "MA100 FLOOR%";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(429, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(88, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "MA125 FLOOR%";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(429, 321);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 44;
            this.label21.Text = "MA175 FLOOR%";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(429, 376);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 13);
            this.label22.TabIndex = 45;
            this.label22.Text = "MA200 FLOOR%";
            // 
            // numericMa75CapPercent
            // 
            this.numericMa75CapPercent.DecimalPlaces = 1;
            this.numericMa75CapPercent.Location = new System.Drawing.Point(539, 82);
            this.numericMa75CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa75CapPercent.Name = "numericMa75CapPercent";
            this.numericMa75CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa75CapPercent.TabIndex = 15;
            this.numericMa75CapPercent.ValueChanged += new System.EventHandler(this.numericMa75CapPercent_ValueChanged);
            this.numericMa75CapPercent.Enter += new System.EventHandler(this.numericMa75CapPercent_Enter);
            // 
            // numericMa100CapPercent
            // 
            this.numericMa100CapPercent.DecimalPlaces = 1;
            this.numericMa100CapPercent.Location = new System.Drawing.Point(539, 132);
            this.numericMa100CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa100CapPercent.Name = "numericMa100CapPercent";
            this.numericMa100CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa100CapPercent.TabIndex = 19;
            this.numericMa100CapPercent.ValueChanged += new System.EventHandler(this.numericMa100CapPercent_ValueChanged);
            this.numericMa100CapPercent.Enter += new System.EventHandler(this.numericMa100CapPercent_Enter);
            // 
            // numericMa125CapPercent
            // 
            this.numericMa125CapPercent.DecimalPlaces = 1;
            this.numericMa125CapPercent.Location = new System.Drawing.Point(539, 184);
            this.numericMa125CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa125CapPercent.Name = "numericMa125CapPercent";
            this.numericMa125CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa125CapPercent.TabIndex = 23;
            this.numericMa125CapPercent.ValueChanged += new System.EventHandler(this.numericMa125CapPercent_ValueChanged);
            this.numericMa125CapPercent.Enter += new System.EventHandler(this.numericMa125CapPercent_Enter);
            // 
            // numericMa175CapPercent
            // 
            this.numericMa175CapPercent.DecimalPlaces = 1;
            this.numericMa175CapPercent.Location = new System.Drawing.Point(539, 293);
            this.numericMa175CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa175CapPercent.Name = "numericMa175CapPercent";
            this.numericMa175CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa175CapPercent.TabIndex = 31;
            this.numericMa175CapPercent.ValueChanged += new System.EventHandler(this.numericMa175CapPercent_ValueChanged);
            this.numericMa175CapPercent.Enter += new System.EventHandler(this.numericMa175CapPercent_Enter);
            // 
            // numericMa200CapPercent
            // 
            this.numericMa200CapPercent.DecimalPlaces = 1;
            this.numericMa200CapPercent.Location = new System.Drawing.Point(539, 348);
            this.numericMa200CapPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa200CapPercent.Name = "numericMa200CapPercent";
            this.numericMa200CapPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa200CapPercent.TabIndex = 35;
            this.numericMa200CapPercent.ValueChanged += new System.EventHandler(this.numericMa200CapPercent_ValueChanged);
            this.numericMa200CapPercent.Enter += new System.EventHandler(this.numericMa200CapPercent_Enter);
            // 
            // numericMa75FloorPercent
            // 
            this.numericMa75FloorPercent.DecimalPlaces = 1;
            this.numericMa75FloorPercent.Location = new System.Drawing.Point(539, 106);
            this.numericMa75FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa75FloorPercent.Name = "numericMa75FloorPercent";
            this.numericMa75FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa75FloorPercent.TabIndex = 17;
            this.numericMa75FloorPercent.ValueChanged += new System.EventHandler(this.numericMa75FloorPercent_ValueChanged);
            this.numericMa75FloorPercent.Enter += new System.EventHandler(this.numericMa75FloorPercent_Enter);
            // 
            // numericMa100FloorPercent
            // 
            this.numericMa100FloorPercent.DecimalPlaces = 1;
            this.numericMa100FloorPercent.Location = new System.Drawing.Point(539, 157);
            this.numericMa100FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa100FloorPercent.Name = "numericMa100FloorPercent";
            this.numericMa100FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa100FloorPercent.TabIndex = 21;
            this.numericMa100FloorPercent.ValueChanged += new System.EventHandler(this.numericMa100FloorPercent_ValueChanged);
            this.numericMa100FloorPercent.Enter += new System.EventHandler(this.numericMa100FloorPercent_Enter);
            // 
            // numericMa125FloorPercent
            // 
            this.numericMa125FloorPercent.DecimalPlaces = 1;
            this.numericMa125FloorPercent.Location = new System.Drawing.Point(539, 209);
            this.numericMa125FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa125FloorPercent.Name = "numericMa125FloorPercent";
            this.numericMa125FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa125FloorPercent.TabIndex = 25;
            this.numericMa125FloorPercent.ValueChanged += new System.EventHandler(this.numericMa125FloorPercent_ValueChanged);
            this.numericMa125FloorPercent.Enter += new System.EventHandler(this.numericMa125FloorPercent_Enter);
            // 
            // numericMa175FloorPercent
            // 
            this.numericMa175FloorPercent.DecimalPlaces = 1;
            this.numericMa175FloorPercent.Location = new System.Drawing.Point(539, 318);
            this.numericMa175FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa175FloorPercent.Name = "numericMa175FloorPercent";
            this.numericMa175FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa175FloorPercent.TabIndex = 33;
            this.numericMa175FloorPercent.ValueChanged += new System.EventHandler(this.numericMa175FloorPercent_ValueChanged);
            this.numericMa175FloorPercent.Enter += new System.EventHandler(this.numericMa175FloorPercent_Enter);
            // 
            // numericMa200FloorPercent
            // 
            this.numericMa200FloorPercent.DecimalPlaces = 1;
            this.numericMa200FloorPercent.Location = new System.Drawing.Point(539, 374);
            this.numericMa200FloorPercent.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericMa200FloorPercent.Name = "numericMa200FloorPercent";
            this.numericMa200FloorPercent.Size = new System.Drawing.Size(66, 20);
            this.numericMa200FloorPercent.TabIndex = 37;
            this.numericMa200FloorPercent.ValueChanged += new System.EventHandler(this.numericMa200FloorPercent_ValueChanged);
            this.numericMa200FloorPercent.Enter += new System.EventHandler(this.numericMa200FloorPercent_Enter);
            // 
            // checkBoxMa75Cap
            // 
            this.checkBoxMa75Cap.AutoSize = true;
            this.checkBoxMa75Cap.Checked = true;
            this.checkBoxMa75Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa75Cap.Location = new System.Drawing.Point(653, 85);
            this.checkBoxMa75Cap.Name = "checkBoxMa75Cap";
            this.checkBoxMa75Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa75Cap.TabIndex = 16;
            this.checkBoxMa75Cap.Text = "Unset";
            this.checkBoxMa75Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa75Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa75Cap_CheckedChanged);
            // 
            // checkBoxMa100Cap
            // 
            this.checkBoxMa100Cap.AutoSize = true;
            this.checkBoxMa100Cap.Checked = true;
            this.checkBoxMa100Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa100Cap.Location = new System.Drawing.Point(653, 137);
            this.checkBoxMa100Cap.Name = "checkBoxMa100Cap";
            this.checkBoxMa100Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa100Cap.TabIndex = 20;
            this.checkBoxMa100Cap.Text = "Unset";
            this.checkBoxMa100Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa100Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa100Cap_CheckedChanged);
            // 
            // checkBoxMa125Cap
            // 
            this.checkBoxMa125Cap.AutoSize = true;
            this.checkBoxMa125Cap.Checked = true;
            this.checkBoxMa125Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa125Cap.Location = new System.Drawing.Point(653, 189);
            this.checkBoxMa125Cap.Name = "checkBoxMa125Cap";
            this.checkBoxMa125Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa125Cap.TabIndex = 24;
            this.checkBoxMa125Cap.Text = "Unset";
            this.checkBoxMa125Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa125Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa125Cap_CheckedChanged);
            // 
            // checkBoxMa175Cap
            // 
            this.checkBoxMa175Cap.AutoSize = true;
            this.checkBoxMa175Cap.Checked = true;
            this.checkBoxMa175Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa175Cap.Location = new System.Drawing.Point(653, 298);
            this.checkBoxMa175Cap.Name = "checkBoxMa175Cap";
            this.checkBoxMa175Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa175Cap.TabIndex = 32;
            this.checkBoxMa175Cap.Text = "Unset";
            this.checkBoxMa175Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa175Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa175Cap_CheckedChanged);
            // 
            // checkBoxMa200Cap
            // 
            this.checkBoxMa200Cap.AutoSize = true;
            this.checkBoxMa200Cap.Checked = true;
            this.checkBoxMa200Cap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa200Cap.Location = new System.Drawing.Point(653, 352);
            this.checkBoxMa200Cap.Name = "checkBoxMa200Cap";
            this.checkBoxMa200Cap.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa200Cap.TabIndex = 36;
            this.checkBoxMa200Cap.Text = "Unset";
            this.checkBoxMa200Cap.UseVisualStyleBackColor = true;
            this.checkBoxMa200Cap.CheckedChanged += new System.EventHandler(this.checkBoxMa200Cap_CheckedChanged);
            // 
            // checkBoxMa75Floor
            // 
            this.checkBoxMa75Floor.AutoSize = true;
            this.checkBoxMa75Floor.Checked = true;
            this.checkBoxMa75Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa75Floor.Location = new System.Drawing.Point(653, 109);
            this.checkBoxMa75Floor.Name = "checkBoxMa75Floor";
            this.checkBoxMa75Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa75Floor.TabIndex = 18;
            this.checkBoxMa75Floor.Text = "Unset";
            this.checkBoxMa75Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa75Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa75Floor_CheckedChanged);
            // 
            // checkBoxMa100Floor
            // 
            this.checkBoxMa100Floor.AutoSize = true;
            this.checkBoxMa100Floor.Checked = true;
            this.checkBoxMa100Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa100Floor.Location = new System.Drawing.Point(653, 158);
            this.checkBoxMa100Floor.Name = "checkBoxMa100Floor";
            this.checkBoxMa100Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa100Floor.TabIndex = 22;
            this.checkBoxMa100Floor.Text = "Unset";
            this.checkBoxMa100Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa100Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa100Floor_CheckedChanged);
            // 
            // checkBoxMa125Floor
            // 
            this.checkBoxMa125Floor.AutoSize = true;
            this.checkBoxMa125Floor.Checked = true;
            this.checkBoxMa125Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa125Floor.Location = new System.Drawing.Point(653, 212);
            this.checkBoxMa125Floor.Name = "checkBoxMa125Floor";
            this.checkBoxMa125Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa125Floor.TabIndex = 26;
            this.checkBoxMa125Floor.Text = "Unset";
            this.checkBoxMa125Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa125Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa125Floor_CheckedChanged);
            // 
            // checkBoxMa175Floor
            // 
            this.checkBoxMa175Floor.AutoSize = true;
            this.checkBoxMa175Floor.Checked = true;
            this.checkBoxMa175Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa175Floor.Location = new System.Drawing.Point(653, 321);
            this.checkBoxMa175Floor.Name = "checkBoxMa175Floor";
            this.checkBoxMa175Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa175Floor.TabIndex = 34;
            this.checkBoxMa175Floor.Text = "Unset";
            this.checkBoxMa175Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa175Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa175Floor_CheckedChanged);
            // 
            // checkBoxMa200Floor
            // 
            this.checkBoxMa200Floor.AutoSize = true;
            this.checkBoxMa200Floor.Checked = true;
            this.checkBoxMa200Floor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMa200Floor.Location = new System.Drawing.Point(653, 376);
            this.checkBoxMa200Floor.Name = "checkBoxMa200Floor";
            this.checkBoxMa200Floor.Size = new System.Drawing.Size(54, 17);
            this.checkBoxMa200Floor.TabIndex = 38;
            this.checkBoxMa200Floor.Text = "Unset";
            this.checkBoxMa200Floor.UseVisualStyleBackColor = true;
            this.checkBoxMa200Floor.CheckedChanged += new System.EventHandler(this.checkBoxMa200Floor_CheckedChanged);
            // 
            // checkBoxEditAll
            // 
            this.checkBoxEditAll.AutoSize = true;
            this.checkBoxEditAll.Location = new System.Drawing.Point(42, 377);
            this.checkBoxEditAll.Name = "checkBoxEditAll";
            this.checkBoxEditAll.Size = new System.Drawing.Size(93, 17);
            this.checkBoxEditAll.TabIndex = 39;
            this.checkBoxEditAll.Text = "Edit All Values";
            this.checkBoxEditAll.UseVisualStyleBackColor = true;
            this.checkBoxEditAll.CheckedChanged += new System.EventHandler(this.checkBoxEditAll_CheckedChanged);
            // 
            // textBoxUrl
            // 
            this.textBoxUrl.Location = new System.Drawing.Point(84, 412);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.Size = new System.Drawing.Size(623, 20);
            this.textBoxUrl.TabIndex = 40;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(46, 415);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 13);
            this.label23.TabIndex = 1004;
            this.label23.Text = "URL:";
            // 
            // UpdateTickerInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 511);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.textBoxUrl);
            this.Controls.Add(this.checkBoxEditAll);
            this.Controls.Add(this.checkBoxMa200Floor);
            this.Controls.Add(this.checkBoxMa175Floor);
            this.Controls.Add(this.checkBoxMa125Floor);
            this.Controls.Add(this.checkBoxMa100Floor);
            this.Controls.Add(this.checkBoxMa75Floor);
            this.Controls.Add(this.checkBoxMa200Cap);
            this.Controls.Add(this.checkBoxMa175Cap);
            this.Controls.Add(this.checkBoxMa125Cap);
            this.Controls.Add(this.checkBoxMa100Cap);
            this.Controls.Add(this.checkBoxMa75Cap);
            this.Controls.Add(this.numericMa200FloorPercent);
            this.Controls.Add(this.numericMa175FloorPercent);
            this.Controls.Add(this.numericMa125FloorPercent);
            this.Controls.Add(this.numericMa100FloorPercent);
            this.Controls.Add(this.numericMa75FloorPercent);
            this.Controls.Add(this.numericMa200CapPercent);
            this.Controls.Add(this.numericMa175CapPercent);
            this.Controls.Add(this.numericMa125CapPercent);
            this.Controls.Add(this.numericMa100CapPercent);
            this.Controls.Add(this.numericMa75CapPercent);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.checkBoxMa50Floor);
            this.Controls.Add(this.checkBoxMa50Cap);
            this.Controls.Add(this.numericMa50FloorPercent);
            this.Controls.Add(this.numericMa50CapPercent);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.checkBoxBuffer);
            this.Controls.Add(this.numericBuffer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBoxMa150Floor);
            this.Controls.Add(this.checkBoxMa150Cap);
            this.Controls.Add(this.checkBoxFloor);
            this.Controls.Add(this.checkBoxCap);
            this.Controls.Add(this.checkBoxStockRating);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.numericMa150FloorPercent);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.numericMa150CapPercent);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numericFloor);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numericCap);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericStockRating);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.labelFullName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelSymbol);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelTickerId);
            this.Controls.Add(this.label1);
            this.Name = "UpdateTickerInfo";
            this.Text = "UpdateTickerInfo";
            ((System.ComponentModel.ISupportInitialize)(this.numericStockRating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa150CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa150FloorPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBuffer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa50CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa50FloorPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa75CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa100CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa125CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa175CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa200CapPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa75FloorPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa100FloorPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa125FloorPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa175FloorPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMa200FloorPercent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTickerId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelSymbol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericStockRating;
        private System.Windows.Forms.NumericUpDown numericCap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericFloor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericMa150CapPercent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericMa150FloorPercent;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.CheckBox checkBoxStockRating;
        private System.Windows.Forms.CheckBox checkBoxCap;
        private System.Windows.Forms.CheckBox checkBoxFloor;
        private System.Windows.Forms.CheckBox checkBoxMa150Cap;
        private System.Windows.Forms.CheckBox checkBoxMa150Floor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericBuffer;
        private System.Windows.Forms.CheckBox checkBoxBuffer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericMa50CapPercent;
        private System.Windows.Forms.NumericUpDown numericMa50FloorPercent;
        private System.Windows.Forms.CheckBox checkBoxMa50Cap;
        private System.Windows.Forms.CheckBox checkBoxMa50Floor;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown numericMa75CapPercent;
        private System.Windows.Forms.NumericUpDown numericMa100CapPercent;
        private System.Windows.Forms.NumericUpDown numericMa125CapPercent;
        private System.Windows.Forms.NumericUpDown numericMa175CapPercent;
        private System.Windows.Forms.NumericUpDown numericMa200CapPercent;
        private System.Windows.Forms.NumericUpDown numericMa75FloorPercent;
        private System.Windows.Forms.NumericUpDown numericMa100FloorPercent;
        private System.Windows.Forms.NumericUpDown numericMa125FloorPercent;
        private System.Windows.Forms.NumericUpDown numericMa175FloorPercent;
        private System.Windows.Forms.NumericUpDown numericMa200FloorPercent;
        private System.Windows.Forms.CheckBox checkBoxMa75Cap;
        private System.Windows.Forms.CheckBox checkBoxMa100Cap;
        private System.Windows.Forms.CheckBox checkBoxMa125Cap;
        private System.Windows.Forms.CheckBox checkBoxMa175Cap;
        private System.Windows.Forms.CheckBox checkBoxMa200Cap;
        private System.Windows.Forms.CheckBox checkBoxMa75Floor;
        private System.Windows.Forms.CheckBox checkBoxMa100Floor;
        private System.Windows.Forms.CheckBox checkBoxMa125Floor;
        private System.Windows.Forms.CheckBox checkBoxMa175Floor;
        private System.Windows.Forms.CheckBox checkBoxMa200Floor;
        private System.Windows.Forms.CheckBox checkBoxEditAll;
        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.Label label23;
    }
}