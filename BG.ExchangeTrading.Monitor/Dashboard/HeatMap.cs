﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class HeatMap : Form
    {
        private TickerInfoDisplay Ticker;
        private List<TickerMovingAverageData> heatmapData;
        private List<int> movingAverageRuler;

        private HeatMapRepository heatmap;
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextMenuShowDetails = new ToolStripMenuItem();

        public HeatMap()
        {
            InitializeComponent();
        }
        public HeatMap(TickerInfoDisplay selectedTicker) : this()
        {
            this.Ticker = selectedTicker;
            if (selectedTicker.Buffer.HasValue)
                this.numericUpDownBuffer.Value = (decimal)selectedTicker.Buffer.Value;
        }
        #region List helpers
        private void MoveSelectedSourceToTarget(CheckedListBox source, CheckedListBox target)
        {
            foreach (var item in source.CheckedItems.OfType<string>().ToList())
            {
                target.Items.Add(item);
                source.Items.Remove(item);
            }
        }
        private void MoveAllSourceToTarget(CheckedListBox source, CheckedListBox target)
        {
            foreach (var item in source.Items.OfType<string>().ToList())
            {
                target.Items.Add(item);
                source.Items.Remove(item);
            }
        }
        private void MoveTargetToSource(CheckedListBox source, CheckedListBox target)
        {
            foreach (var item in target.CheckedItems.OfType<string>().ToList())
            {
                target.Items.Remove(item);
                source.Items.Add(item);
            }
        }
        private void MoveAllTargetToSource(CheckedListBox source, CheckedListBox target)
        {
            foreach (var item in target.Items.OfType<string>().ToList())
            {
                target.Items.Remove(item);
                source.Items.Add(item);
            }
        }
        private void RemoveOption(CheckedListBox source, NumericUpDown itemControl)
        {
            var itemIndex = source.Items.IndexOf(itemControl.Value.ToString());
            if (itemIndex >= 0)
            {
                var item = source.Items[itemIndex];
                source.Items.Remove(item);
            }
        }
        private void AddOption(CheckedListBox source, CheckedListBox target, NumericUpDown itemControl)
        {
            var itemIndexSource = source.Items.IndexOf(itemControl.Value.ToString());
            var itemIndexTarget = target.Items.IndexOf(itemControl.Value.ToString());
            if (itemIndexSource < 0 && itemIndexTarget < 0)
                source.Items.Add(itemControl.Value.ToString());
            else
                MessageBox.Show($"Item {itemControl.Value.ToString()} already exists");
        }
        #endregion
        #region MA lists
        private void buttonMaMoveSelected_Click(object sender, EventArgs e)
        {
            MoveSelectedSourceToTarget(checkedListBoxMASource, checkedListBoxMATarget);
        }

        private void buttonMaSelectAll_Click(object sender, EventArgs e)
        {
            MoveAllSourceToTarget(checkedListBoxMASource, checkedListBoxMATarget);
        }

        private void buttonMaDeselect_Click(object sender, EventArgs e)
        {
            MoveTargetToSource(checkedListBoxMASource, checkedListBoxMATarget);
        }

        private void buttonMaDeselectAll_Click(object sender, EventArgs e)
        {
            MoveAllTargetToSource(checkedListBoxMASource, checkedListBoxMATarget);
        }

        private void checkedListBoxMASource_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            this.numericUpDownMaOptions.Value = Decimal.Parse(checkedListBoxMASource.SelectedItem.ToString());
        }

        private void buttonMaRemoveItem_Click(object sender, EventArgs e)
        {
            RemoveOption(checkedListBoxMASource, numericUpDownMaOptions);
        }

        private void buttonMaAddItem_Click(object sender, EventArgs e)
        {
            AddOption(checkedListBoxMASource, checkedListBoxMATarget, numericUpDownMaOptions);
        }

        #endregion

        #region Buy list
        private void buttonBuyMoveSelected_Click(object sender, EventArgs e)
        {
            MoveSelectedSourceToTarget(checkedListBoxBuySource, checkedListBoxBuyTarget);
        }

        private void buttonBuySelectAll_Click(object sender, EventArgs e)
        {
            MoveAllSourceToTarget(checkedListBoxBuySource, checkedListBoxBuyTarget);
        }

        private void buttonBuyDeselect_Click(object sender, EventArgs e)
        {
            MoveTargetToSource(checkedListBoxBuySource, checkedListBoxBuyTarget);
        }

        private void buttonBuyDeselectAll_Click(object sender, EventArgs e)
        {
            MoveAllTargetToSource(checkedListBoxBuySource, checkedListBoxBuyTarget);
        }

        private void buttonBuyRemoveItem_Click(object sender, EventArgs e)
        {
            RemoveOption(checkedListBoxBuySource, numericUpDownBuy);
        }

        private void buttonBuyAddItem_Click(object sender, EventArgs e)
        {
            AddOption(checkedListBoxBuySource, checkedListBoxBuyTarget, numericUpDownBuy);
        }
        #endregion

        #region Sell list
        private void buttonSellMoveSelected_Click(object sender, EventArgs e)
        {
            MoveSelectedSourceToTarget(checkedListBoxSellSource, checkedListBoxSellTarget);
        }

        private void buttonSellSelectAll_Click(object sender, EventArgs e)
        {
            MoveAllSourceToTarget(checkedListBoxSellSource, checkedListBoxSellTarget);
        }

        private void buttonSellDeselect_Click(object sender, EventArgs e)
        {
            MoveTargetToSource(checkedListBoxSellSource, checkedListBoxSellTarget);
        }

        private void buttonSellDeselectAll_Click(object sender, EventArgs e)
        {
            MoveAllTargetToSource(checkedListBoxSellSource, checkedListBoxSellTarget);
        }

        private void buttonSellRemoveItem_Click(object sender, EventArgs e)
        {
            RemoveOption(checkedListBoxSellSource, numericUpDownSell);
        }

        private void buttonSellAddItem_Click(object sender, EventArgs e)
        {
            AddOption(checkedListBoxSellSource, checkedListBoxSellTarget, numericUpDownSell);
        }
        #endregion

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            var historicalData = TickerServices.Instance.GetTickerData(this.Ticker.TickerId)
                .ConvertAll<StockPrice>(p => new StockPrice(p.Date, p.OpenPrice, p.ClosePrice, p.High, p.Low, p.Volume));
            movingAverageRuler = this.checkedListBoxMATarget.Items.Cast<string>().ToList().ConvertAll<int>(m => int.Parse(m));
            heatmapData = TickerServices.Instance.GenerateHeatMapData(historicalData, movingAverageRuler);

            BuildHeatMapDataGrid(heatmapData);
            tabControl1.SelectedTab = this.HeatMapDataTab;
        }

        private void BuildHeatMapDataGrid(List<TickerMovingAverageData> hmData)
        {
            this.dataGridViewHeatMapData.Columns.Add("Date", "Date");
            dataGridViewHeatMapData.Columns[0].DefaultCellStyle.Format = "dd/MM/yyyy";
            this.dataGridViewHeatMapData.Columns.Add("ClosePrice", "ClosePrice");
            this.dataGridViewHeatMapData.Columns[1].Frozen = true;

            var maItems = hmData.First().MovingAverageItems;
            foreach (var ma in maItems)
            {
                var columnName = $"{ma.MaDays} MA";
                this.dataGridViewHeatMapData.Columns.Add(columnName, columnName);
            }
            foreach (var ma in maItems)
            {
                var columnName = $"{ma.MaDays} Spread";
                this.dataGridViewHeatMapData.Columns.Add(columnName, columnName);
            }
            foreach (var rowData in hmData)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridViewHeatMapData.Rows[0].Clone();
                var cellIndex = 0;
                row.Cells[cellIndex++].Value = rowData.Date;
                row.Cells[cellIndex++].Value = rowData.ClosePrice;
                foreach (var maData in rowData.MovingAverageItems)
                {
                    row.Cells[cellIndex++].Value = maData.MaValue.ToString("F2");
                }
                foreach (var maData in rowData.MovingAverageItems)
                {
                    row.Cells[cellIndex++].Value = maData.SpreadValue.ToString("F2");
                    if (maData.SpreadValue > 0)
                    {
                        row.Cells[cellIndex-1].Style.ForeColor = Color.Green;
                    }
                    else
                    {
                        row.Cells[cellIndex-1].Style.ForeColor = Color.OrangeRed;
                    }
                }
                row.Tag = rowData;
                dataGridViewHeatMapData.Rows.Add(row);
            }
        }

        private void buttonGenarateView_Click(object sender, EventArgs e)
        {
            var buyPoints = this.checkedListBoxBuyTarget.Items.Cast<string>().ToList().ConvertAll<int>(m => int.Parse(m));
            var sellPoints = this.checkedListBoxSellTarget.Items.Cast<string>().ToList().ConvertAll<int>(m => int.Parse(m));
            var buffer = Decimal.ToInt32(this.numericUpDownBuffer.Value);
            heatmap = TickerServices.Instance.GenerateHeatMapRepository(heatmapData, 
                buyPoints, 
                sellPoints, 
                movingAverageRuler, 
                buffer);
            this.dataGridViewHeatmapView.Rows.Clear();
            this.dataGridViewHeatmapView.Columns.Clear();
            this.dataGridViewHeatmapView.Refresh();

            MonitorUtils.BuildGrid<EntryPointHeatMapDisplay>(heatmap.Summary
                .ConvertAll<EntryPointHeatMapDisplay>(h => new EntryPointHeatMapDisplay(h.MovingAverage
                        , h.IsBuyPoint
                        , h.Trigger
                        , h.TotalPositions
                        , h.ProfitPositions
                        , h.LostPositions,
                        h.Success))
                , dataGridViewHeatmapView);
            AddContextMenu();
            tabControl1.SelectedTab = this.HeatMapViewTab;
        }
        public void AddContextMenu()
        {
            contextMenuShowDetails.Text = "Show Details";
            contextMenuShowDetails.Click -= new EventHandler(contextMenuShowDetails_Click);
            contextMenuShowDetails.Click += new EventHandler(contextMenuShowDetails_Click);

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridViewHeatmapView.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridViewHeatmapView.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridViewHeatmapView_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewHeatmapView.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            selectedRow.ContextMenuStrip.Items.Clear();
            selectedRow.ContextMenuStrip.Items.Add(contextMenuShowDetails);
        }

        private void dataGridViewHeatmapView_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        private void contextMenuShowDetails_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewHeatmapView.Rows[mouseLocation.RowIndex];
            var selectedEntry = (EntryPointHeatMapDisplay)selectedRow.Tag;
            var details = new HeapMapDetails(selectedEntry, heatmap);
            details.Show();
        }
    }
}
