﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class UpdateTickerInfo : Form
    {
        public UpdateTickerInfo(BGTickerDetails ticker)
        {
            InitializeComponent();
            InitData(ticker);
        }

        private void InitData(BGTickerDetails ticker)
        {
            this.labelTickerId.Text = ticker.TickerId.ToString();
            this.labelSymbol.Text = ticker.Symbol;
            this.labelFullName.Text = ticker.FullName;
            this.comboBoxType.Text = ticker.Type;
            this.comboBoxGroup.Text = ticker.Group;
            this.checkBoxStockRating.Checked = !ticker.StockRating.HasValue;
            this.numericStockRating.Enabled = ticker.StockRating.HasValue;
            if (ticker.StockRating.HasValue)
                this.numericStockRating.Value = (decimal)ticker.StockRating.Value;

            this.checkBoxBuffer.Checked = !ticker.Buffer.HasValue;
            this.numericBuffer.Enabled = ticker.Buffer.HasValue;
            if (ticker.Buffer.HasValue)
                this.numericBuffer.Value = (decimal)ticker.Buffer.Value;

            this.checkBoxCap.Checked = !ticker.Cap.HasValue;
            this.numericCap.Enabled = ticker.Cap.HasValue;
            if (ticker.Cap.HasValue)
                this.numericCap.Value = (decimal)ticker.Cap.Value;

            this.checkBoxFloor.Checked = !ticker.Floor.HasValue;
            this.numericFloor.Enabled = ticker.Floor.HasValue;
            if (ticker.Floor.HasValue)
                this.numericFloor.Value = (decimal)ticker.Floor.Value;

            #region checkbox50
            this.checkBoxMa50Cap.Checked = !ticker.MA50CapPercent.HasValue;
            this.numericMa50CapPercent.Enabled = ticker.MA50CapPercent.HasValue;
            if (ticker.MA50CapPercent.HasValue)
                this.numericMa50CapPercent.Value = (decimal)ticker.MA50CapPercent.Value;
            this.checkBoxMa50Floor.Checked = !ticker.MA50FloorPercent.HasValue;
            this.numericMa50FloorPercent.Enabled = ticker.MA50FloorPercent.HasValue;
            if (ticker.MA50FloorPercent.HasValue)
                this.numericMa50FloorPercent.Value = (decimal)ticker.MA50FloorPercent.Value;
            #endregion 
            #region checkbox75
            this.checkBoxMa75Cap.Checked = !ticker.MA75CapPercent.HasValue;
            this.numericMa75CapPercent.Enabled = ticker.MA75CapPercent.HasValue;
            if (ticker.MA75CapPercent.HasValue)
                this.numericMa75CapPercent.Value = (decimal)ticker.MA75CapPercent.Value;
            this.checkBoxMa75Floor.Checked = !ticker.MA75FloorPercent.HasValue;
            this.numericMa75FloorPercent.Enabled = ticker.MA75FloorPercent.HasValue;
            if (ticker.MA75FloorPercent.HasValue)
                this.numericMa75FloorPercent.Value = (decimal)ticker.MA75FloorPercent.Value;
            #endregion
            #region checkbox100
            this.checkBoxMa100Cap.Checked = !ticker.MA100CapPercent.HasValue;
            this.numericMa100CapPercent.Enabled = ticker.MA100CapPercent.HasValue;
            if (ticker.MA100CapPercent.HasValue)
                this.numericMa100CapPercent.Value = (decimal)ticker.MA100CapPercent.Value;
            this.checkBoxMa100Floor.Checked = !ticker.MA100FloorPercent.HasValue;
            this.numericMa100FloorPercent.Enabled = ticker.MA100FloorPercent.HasValue;
            if (ticker.MA100FloorPercent.HasValue)
                this.numericMa100FloorPercent.Value = (decimal)ticker.MA100FloorPercent.Value;
            #endregion
            #region checkbox125
            this.checkBoxMa125Cap.Checked = !ticker.MA125CapPercent.HasValue;
            this.numericMa125CapPercent.Enabled = ticker.MA125CapPercent.HasValue;
            if (ticker.MA125CapPercent.HasValue)
                this.numericMa125CapPercent.Value = (decimal)ticker.MA125CapPercent.Value;
            this.checkBoxMa125Floor.Checked = !ticker.MA125FloorPercent.HasValue;
            this.numericMa125FloorPercent.Enabled = ticker.MA125FloorPercent.HasValue;
            if (ticker.MA125FloorPercent.HasValue)
                this.numericMa125FloorPercent.Value = (decimal)ticker.MA125FloorPercent.Value;
            #endregion
            #region checkbox150
            this.checkBoxMa150Cap.Checked = !ticker.MA150CapPercent.HasValue;
            this.numericMa150CapPercent.Enabled = ticker.MA150CapPercent.HasValue;
            if (ticker.MA150CapPercent.HasValue)
                this.numericMa150CapPercent.Value = (decimal)ticker.MA150CapPercent.Value;
            this.checkBoxMa150Floor.Checked = !ticker.MA150FloorPercent.HasValue;
            this.numericMa150FloorPercent.Enabled = ticker.MA150FloorPercent.HasValue;
            if (ticker.MA150FloorPercent.HasValue)
                this.numericMa150FloorPercent.Value = (decimal)ticker.MA150FloorPercent.Value;
            #endregion
            #region checkbox175
            this.checkBoxMa175Cap.Checked = !ticker.MA175CapPercent.HasValue;
            this.numericMa175CapPercent.Enabled = ticker.MA175CapPercent.HasValue;
            if (ticker.MA175CapPercent.HasValue)
                this.numericMa175CapPercent.Value = (decimal)ticker.MA175CapPercent.Value;
            this.checkBoxMa175Floor.Checked = !ticker.MA175FloorPercent.HasValue;
            this.numericMa175FloorPercent.Enabled = ticker.MA175FloorPercent.HasValue;
            if (ticker.MA175FloorPercent.HasValue)
                this.numericMa175FloorPercent.Value = (decimal)ticker.MA175FloorPercent.Value;
            #endregion
            #region checkbox200
            this.checkBoxMa200Cap.Checked = !ticker.MA200CapPercent.HasValue;
            this.numericMa200CapPercent.Enabled = ticker.MA200CapPercent.HasValue;
            if (ticker.MA200CapPercent.HasValue)
                this.numericMa200CapPercent.Value = (decimal)ticker.MA200CapPercent.Value;
            this.checkBoxMa200Floor.Checked = !ticker.MA200FloorPercent.HasValue;
            this.numericMa200FloorPercent.Enabled = ticker.MA200FloorPercent.HasValue;
            if (ticker.MA200FloorPercent.HasValue)
                this.numericMa200FloorPercent.Value = (decimal)ticker.MA200FloorPercent.Value;
            #endregion 

            this.textBoxUrl.Text = ticker.Url;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            TickersPersistanceServices.Instance.UpdateTickerInfo(this.labelSymbol.Text,
                this.comboBoxType.Text,
                this.comboBoxGroup.Text,
                this.checkBoxStockRating.Checked ? (double?)null : (double)this.numericStockRating.Value,
                this.checkBoxStockRating.Checked ? (DateTime?)null : DateTime.Now,
                this.checkBoxBuffer.Checked ? (double?)null : (double)this.numericBuffer.Value,
                this.checkBoxCap.Checked ? (double?)null : (double)this.numericCap.Value,
                this.checkBoxFloor.Checked ? (double?)null : (double)this.numericFloor.Value,
                this.checkBoxMa50Cap.Checked ? (double?)null : (double)this.numericMa50CapPercent.Value,
                this.checkBoxMa50Floor.Checked ? (double?)null : (double)this.numericMa50FloorPercent.Value,
                this.checkBoxMa75Cap.Checked ? (double?)null : (double)this.numericMa75CapPercent.Value,
                this.checkBoxMa75Floor.Checked ? (double?)null : (double)this.numericMa75FloorPercent.Value,
                this.checkBoxMa100Cap.Checked ? (double?)null : (double)this.numericMa100CapPercent.Value,
                this.checkBoxMa100Floor.Checked ? (double?)null : (double)this.numericMa100FloorPercent.Value,
                this.checkBoxMa125Cap.Checked ? (double?)null : (double)this.numericMa125CapPercent.Value,
                this.checkBoxMa125Floor.Checked ? (double?)null : (double)this.numericMa125FloorPercent.Value,
                this.checkBoxMa150Cap.Checked ? (double?)null : (double)this.numericMa150CapPercent.Value,
                this.checkBoxMa150Floor.Checked ? (double?)null : (double)this.numericMa150FloorPercent.Value,
                this.checkBoxMa175Cap.Checked ? (double?)null : (double)this.numericMa175CapPercent.Value,
                this.checkBoxMa175Floor.Checked ? (double?)null : (double)this.numericMa175FloorPercent.Value,
                this.checkBoxMa200Cap.Checked ? (double?)null : (double)this.numericMa200CapPercent.Value,
                this.checkBoxMa200Floor.Checked ? (double?)null : (double)this.numericMa200FloorPercent.Value,
                this.textBoxUrl.Text
                );
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void checkBoxStockRating_CheckedChanged(object sender, EventArgs e)
        {
            this.numericStockRating.Enabled = !this.checkBoxStockRating.Checked;
        }
        private void checkBoxBuffer_CheckedChanged(object sender, EventArgs e)
        {
            this.numericBuffer.Enabled = !this.checkBoxBuffer.Checked;
        }

        private void checkBoxCap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericCap.Enabled = !this.checkBoxCap.Checked;
        }

        private void checkBoxFloor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericFloor.Enabled = !this.checkBoxFloor.Checked;
        }
        #region MA checkboxes
        private void checkBoxMa50Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa50CapPercent.Enabled = !this.checkBoxMa50Cap.Checked;
        }

        private void checkBoxMa50Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa50FloorPercent.Enabled = !this.checkBoxMa50Floor.Checked;
        }
        private void checkBoxMa75Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa75CapPercent.Enabled = !this.checkBoxMa75Cap.Checked;
        }

        private void checkBoxMa75Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa75FloorPercent.Enabled = !this.checkBoxMa75Floor.Checked;
        }

        private void checkBoxMa100Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa100CapPercent.Enabled = !this.checkBoxMa100Cap.Checked;
        }

        private void checkBoxMa100Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa100FloorPercent.Enabled = !this.checkBoxMa100Floor.Checked;
        }

        private void checkBoxMa125Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa125CapPercent.Enabled = !this.checkBoxMa125Cap.Checked;
        }

        private void checkBoxMa125Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa125FloorPercent.Enabled = !this.checkBoxMa125Floor.Checked;
        }
        private void checkBoxMa150Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa150CapPercent.Enabled = !this.checkBoxMa150Cap.Checked;
        }

        private void checkBoxMa150Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa150FloorPercent.Enabled = !this.checkBoxMa150Floor.Checked;
        }
        private void checkBoxMa175Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa175CapPercent.Enabled = !this.checkBoxMa175Cap.Checked;
        }

        private void checkBoxMa175Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa175FloorPercent.Enabled = !this.checkBoxMa175Floor.Checked;
        }

        private void checkBoxMa200Cap_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa200CapPercent.Enabled = !this.checkBoxMa200Cap.Checked;
        }

        private void checkBoxMa200Floor_CheckedChanged(object sender, EventArgs e)
        {
            this.numericMa200FloorPercent.Enabled = !this.checkBoxMa200Floor.Checked;
        }

        #endregion

        #region Enter fields
        private void numericBuffer_Enter(object sender, EventArgs e)
        {
            numericBuffer.Select(0, numericBuffer.Text.Length);
        }

        private void numericStockRating_Enter(object sender, EventArgs e)
        {
            numericStockRating.Select(0, numericStockRating.Text.Length);
        }

        private void numericCap_Enter(object sender, EventArgs e)
        {
            numericCap.Select(0, numericCap.Text.Length);
        }

        private void numericFloor_Enter(object sender, EventArgs e)
        {
            numericFloor.Select(0, numericFloor.Text.Length);
        }

        private void numericMa50CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa50CapPercent.Select(0, numericMa50CapPercent.Text.Length);
        }

        private void numericMa50FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa50FloorPercent.Select(0, numericMa50FloorPercent.Text.Length);
        }

        private void numericMa75CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa75CapPercent.Select(0, numericMa75CapPercent.Text.Length);
        }

        private void numericMa75FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa75FloorPercent.Select(0, numericMa75FloorPercent.Text.Length);
        }

        private void numericMa100CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa100CapPercent.Select(0, numericMa100CapPercent.Text.Length);
        }

        private void numericMa100FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa100FloorPercent.Select(0, numericMa100FloorPercent.Text.Length);
        }

        private void numericMa125CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa125CapPercent.Select(0, numericMa125CapPercent.Text.Length);
        }

        private void numericMa125FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa125FloorPercent.Select(0, numericMa125FloorPercent.Text.Length);
        }

        private void numericMa150CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa150CapPercent.Select(0, numericMa150CapPercent.Text.Length);
        }

        private void numericMa150FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa150FloorPercent.Select(0, numericMa150FloorPercent.Text.Length);
        }

        private void numericMa175CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa175CapPercent.Select(0, numericMa175CapPercent.Text.Length);
        }

        private void numericMa175FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa175FloorPercent.Select(0, numericMa175FloorPercent.Text.Length);
        }

        private void numericMa200CapPercent_Enter(object sender, EventArgs e)
        {
            numericMa200CapPercent.Select(0, numericMa200CapPercent.Text.Length);
        }

        private void numericMa200FloorPercent_Enter(object sender, EventArgs e)
        {
            numericMa200FloorPercent.Select(0, numericMa200FloorPercent.Text.Length);
        }
        #endregion

        private void checkBoxEditAll_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEditAll.Checked)
            {
                this.numericStockRating.Enabled = true;
                this.checkBoxStockRating.Checked = false;
                this.numericBuffer.Enabled = true;
                this.checkBoxBuffer.Checked = false;
                this.numericCap.Enabled = true;
                this.checkBoxCap.Checked = false;
                this.numericFloor.Enabled = true;
                this.checkBoxFloor.Checked = false;

                this.numericMa50CapPercent.Enabled = true;
                this.checkBoxMa50Cap.Checked = false;
                this.numericMa75CapPercent.Enabled = true;
                this.checkBoxMa75Cap.Checked = false;
                this.numericMa100CapPercent.Enabled = true;
                this.checkBoxMa100Cap.Checked = false;
                this.numericMa125CapPercent.Enabled = true;
                this.checkBoxMa125Cap.Checked = false;
                this.numericMa150CapPercent.Enabled = true;
                this.checkBoxMa150Cap.Checked = false;
                this.numericMa175CapPercent.Enabled = true;
                this.checkBoxMa175Cap.Checked = false;
                this.numericMa200CapPercent.Enabled = true;
                this.checkBoxMa200Cap.Checked = false;

                this.numericMa50FloorPercent.Enabled = true;
                this.checkBoxMa50Floor.Checked = false;
                this.numericMa75FloorPercent.Enabled = true;
                this.checkBoxMa75Floor.Checked = false;
                this.numericMa100FloorPercent.Enabled = true;
                this.checkBoxMa100Floor.Checked = false;
                this.numericMa125FloorPercent.Enabled = true;
                this.checkBoxMa125Floor.Checked = false;
                this.numericMa150FloorPercent.Enabled = true;
                this.checkBoxMa150Floor.Checked = false;
                this.numericMa175FloorPercent.Enabled = true;
                this.checkBoxMa175Floor.Checked = false;
                this.numericMa200FloorPercent.Enabled = true;
                this.checkBoxMa200Floor.Checked = false;
            }
            else
            {
                this.numericStockRating.Enabled = false;
                this.checkBoxStockRating.Checked = true;
                this.numericBuffer.Enabled = false;
                this.checkBoxBuffer.Checked = true;
                this.numericCap.Enabled = false;
                this.checkBoxCap.Checked = true;
                this.numericFloor.Enabled = false;
                this.checkBoxFloor.Checked = true;

                this.numericMa50CapPercent.Enabled = false;
                this.checkBoxMa50Cap.Checked = true;
                this.numericMa75CapPercent.Enabled = false;
                this.checkBoxMa75Cap.Checked = true;
                this.numericMa100CapPercent.Enabled = false;
                this.checkBoxMa100Cap.Checked = true;
                this.numericMa125CapPercent.Enabled = false;
                this.checkBoxMa125Cap.Checked = true;
                this.numericMa150CapPercent.Enabled = false;
                this.checkBoxMa150Cap.Checked = true;
                this.numericMa175CapPercent.Enabled = false;
                this.checkBoxMa175Cap.Checked = true;
                this.numericMa200CapPercent.Enabled = false;
                this.checkBoxMa200Cap.Checked = true;

                this.numericMa50FloorPercent.Enabled = false;
                this.checkBoxMa50Floor.Checked = true;
                this.numericMa75FloorPercent.Enabled = false;
                this.checkBoxMa75Floor.Checked = true;
                this.numericMa100FloorPercent.Enabled = false;
                this.checkBoxMa100Floor.Checked = true;
                this.numericMa125FloorPercent.Enabled = false;
                this.checkBoxMa125Floor.Checked = true;
                this.numericMa150FloorPercent.Enabled = false;
                this.checkBoxMa150Floor.Checked = true;
                this.numericMa175FloorPercent.Enabled = false;
                this.checkBoxMa175Floor.Checked = true;
                this.numericMa200FloorPercent.Enabled = false;
                this.checkBoxMa200Floor.Checked = true;
            }
        }
        private static void ChangeNumericValueColor(object sender)
        {
            NumericUpDown numericUpDown = (NumericUpDown)sender;

            // Check the value
            if (numericUpDown.Value > 0)
            {
                numericUpDown.ForeColor = Color.Green;
            }
            else if (numericUpDown.Value < 0)
            {
                numericUpDown.ForeColor = Color.Red;
            }
            else
            {
                // Set a default ForeColor (e.g., black) if value is zero
                numericUpDown.ForeColor = Color.Black;
            }
        }
        private void numericMa50CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa50FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa75CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa75FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa100CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa100FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa125CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa125FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa150CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa150FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa175CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa175FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa200CapPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }

        private void numericMa200FloorPercent_ValueChanged(object sender, EventArgs e)
        {
            ChangeNumericValueColor(sender);
        }        
    }
}
