﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class Dashboard : Form
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextMenuShowDetails = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuDeleteStock = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuUpdateStock = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuGenerateHeatmap = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuIsIn = new ToolStripMenuItem();
        private ToolStripMenuItem contextMenuTickerSummary = new ToolStripMenuItem();
        private List<BGTickerDetails> stocks;
        private List<Services.Persistance.PortfolioAssets_GetActive_Result> assets;
        public Dashboard()
        {
            InitializeComponent();
            initData();            
        }
        private int CalculateFairValue(List<Portfolio> activePortfolios)
        {
            //var portfolios = PositionServices.Instance.GetAllPortfolios()
            //    .Where(p => p.Expiry >= DateTime.Today);
            var instruments = TickerServices.Instance.GetExistingTickersWithDetails()
                    .ToDictionary(x => x.TickerId, v => v);
            var totalFv = 0;
            foreach (var p in activePortfolios)
            {
                var assets = PositionServices.Instance.GetPortfolioAssets(p.PortfolioId);
                double? lastSpot = null;
                var fv = assets.Sum(a => {
                    if (instruments[a.TickerId].LastEndOfDaySpotTimeStamp > instruments[a.TickerId].LastLiveSpotTimeStamp)
                        lastSpot = instruments[a.TickerId].LastEndOfDaySpot;
                    else
                        lastSpot = instruments[a.TickerId].LastLiveSpot;
                    return PositionServices.Instance.CalculateFairValue(a.IsCall, a.IsLong, a.Strike, a.Size, lastSpot);
                });
                totalFv += fv;
            }
            return totalFv;
        }
        private int CalculateTotalExposure(List<Portfolio> activePortfolios)
        {
            return 20000;
        }
        private void initData()
        {
            for (var index=0; index < checkedListBoxGroupFilter.Items.Count; index++)
                checkedListBoxGroupFilter.SetItemChecked(index, true);
            stocks = TickerServices.Instance.GetExistingTickersWithDetails();
            var calculatedData = TickersPersistanceServices.Instance.GetTickersCalculatedData()
                .ToDictionary(d => new CalculatedDataKey(d.TickerId, d.Mv), d => d);
            assets = PositionPersistanceServices.Instance.GetActivePortfolioAssets();
            this.dataGridViewStocks.Rows.Clear();
            this.dataGridViewStocks.Columns.Clear();
            this.dataGridViewStocks.Refresh();
            var activePortfolios = PositionServices.Instance.GetAllPortfolios()
                .Where(p => p.Expiry >= DateTime.Today)
                .ToList();
            this.labelTotalFairValue.Text = CalculateFairValue(activePortfolios).ToString("N0");
            
            this.labelTotalExposureValue.Hide();
            this.labelTotalExposure.Hide();
            //this.labelTotalExposureValue.Text = CalculateTotalExposure(activePortfolios).ToString("N0");
            var instruments = stocks.ConvertAll<TickerInfoDisplay>(s => new TickerInfoDisplay(
                s.TickerId,
                assets.Count(a => a.TickerId == s.TickerId),
                s.Symbol,
                s.FullName,
                s.Type,
                s.Group,
                s.Url,
                s.LastEndOfDaySpot,
                s.LastEndOfDaySpotTimeStamp,
                s.LastLiveSpot,
                s.LastLiveSpotTimeStamp,
                s.Buffer,
                s.LastMA50,
                s.LastMA75,
                s.LastMA100,
                s.LastMA125,
                s.LastMA150,
                s.LastMA175,
                s.LastMA200,
                s.StockRating,
                s.LastUpdateStockRating,
                s.Cap,
                s.Floor,
                s.MA50CapPercent,
                s.MA50FloorPercent,
                s.MA75CapPercent,
                s.MA75FloorPercent,
                s.MA100CapPercent,
                s.MA100FloorPercent,
                s.MA125CapPercent,
                s.MA125FloorPercent,
                s.MA150CapPercent,
                s.MA150FloorPercent,
                s.MA175CapPercent,
                s.MA175FloorPercent,
                s.MA200CapPercent,
                s.MA200FloorPercent,
                calculatedData
                ));

            MonitorUtils.BuildGrid<TickerInfoDisplay>(instruments, this.dataGridViewStocks);
            AddContextMenu();
        }

        private void dataGridViewStocks_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextMenuShowDetails.Text = "Show Details";
            contextMenuShowDetails.Click -= new EventHandler(contextMenuShowDetails_Click);
            contextMenuShowDetails.Click += new EventHandler(contextMenuShowDetails_Click);
            contextMenuDeleteStock.Text = "Delete Ticker";
            contextMenuDeleteStock.Click -= new EventHandler(contextMenuDeleteStock_Click);
            contextMenuDeleteStock.Click += new EventHandler(contextMenuDeleteStock_Click);
            contextMenuUpdateStock.Text = "Update Ticker";
            contextMenuUpdateStock.Click -= new EventHandler(contextMenuUpdateStock_Click);
            contextMenuUpdateStock.Click += new EventHandler(contextMenuUpdateStock_Click);
            contextMenuGenerateHeatmap.Text = "Generate Heatmap";
            contextMenuGenerateHeatmap.Click -= new EventHandler(contextMenuGenerateHeatmap_Click);
            contextMenuGenerateHeatmap.Click += new EventHandler(contextMenuGenerateHeatmap_Click);
            contextMenuIsIn.Text = "Is in active portfolio";
            contextMenuIsIn.Click -= new EventHandler(contextMenuIsIn_Click);
            contextMenuIsIn.Click += new EventHandler(contextMenuIsIn_Click);
            contextMenuTickerSummary.Text = "Summary";
            contextMenuTickerSummary.Click -= new EventHandler(contextMenuSummaryTicker_Click);
            contextMenuTickerSummary.Click += new EventHandler(contextMenuSummaryTicker_Click);

            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.dataGridViewStocks.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.dataGridViewStocks.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                dataGridViewStocks_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            //var selectedTicker = (BGTicker)selectedRow.Tag;
            selectedRow.ContextMenuStrip.Items.Clear();
            selectedRow.ContextMenuStrip.Items.Add(contextMenuShowDetails);
            selectedRow.ContextMenuStrip.Items.Add(contextMenuDeleteStock);
            selectedRow.ContextMenuStrip.Items.Add(contextMenuUpdateStock);
            selectedRow.ContextMenuStrip.Items.Add(contextMenuGenerateHeatmap);
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            if (selectedTicker.IsIn > 0)
                selectedRow.ContextMenuStrip.Items.Add(contextMenuIsIn);
            if (!string.IsNullOrEmpty(selectedTicker.SummaryUrl))
                selectedRow.ContextMenuStrip.Items.Add(contextMenuTickerSummary);
        }
        private void contextMenuShowDetails_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            var details = new StockDetails(selectedTicker);
            details.Show();
        }
        private void contextMenuIsIn_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            var details = new AssetsInPortfolio(selectedTicker.TickerId, assets);
            details.Show();
        }
        private void contextMenuSummaryTicker_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            Process.Start(selectedTicker.SummaryUrl);
        }
        private void contextMenuDeleteStock_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            try
            {
                var yesNoDialog = MessageBox.Show($"You are about to delete Ticket {selectedTicker.Symbol}, remove all instances from exiting portfolio and delete all its historical data, are you sure you want to proceed?", $"Delete ticket and its data {selectedTicker.Symbol}", MessageBoxButtons.YesNo);
                if (yesNoDialog == DialogResult.Yes)
                {

                    TickersPersistanceServices.Instance.DeleteTickerAndData(selectedTicker.TickerId);
                    MessageBox.Show($"Ticker {selectedTicker.Symbol} has been removed successfully");
                    initData();
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"Unexpected error occured trying to delete ticker {selectedTicker.Symbol} with Id {selectedTicker.TickerId} with its historical data", ex);
                MessageBox.Show($"Unexpected error occured trying to delete ticker {selectedTicker.Symbol} with Id {selectedTicker.TickerId} with its historical data, please check the logs");
            }
        }
        private void contextMenuUpdateStock_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            var instrument = stocks.First(s => s.TickerId == selectedTicker.TickerId);
            var dialog = new UpdateTickerInfo(instrument);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                initData();
            }
        }
        private void buttonAddTicker_Click(object sender, EventArgs e)
        {
            var dialog = new AddNewTicker();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                initData();
            }
        }
        private void contextMenuGenerateHeatmap_Click(object sender, EventArgs e)
        {
            var selectedRow = this.dataGridViewStocks.Rows[mouseLocation.RowIndex];
            var selectedTicker = (TickerInfoDisplay)selectedRow.Tag;
            var dialog = new HeatMap(selectedTicker);
            dialog.ShowDialog();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            initData();
        }

        private void buttonPositionsManager_Click(object sender, EventArgs e)
        {
            var dialog = new PositionManager();
            dialog.ShowDialog();
        }

        private void buttonApplyFilter_Click(object sender, EventArgs e)
        {
            var checkedItems = checkedListBoxGroupFilter.CheckedItems.Cast<object>()
                                      .Select(item => item.ToString())
                                      .ToList();
            foreach (DataGridViewRow row in this.dataGridViewStocks.Rows)
            {
                if (row.IsNewRow)
                    continue;
                var selectedRow = (TickerInfoDisplay)row.Tag;
                if (!checkedItems.Contains(selectedRow.Group))
                    row.Visible = false;
                else
                    row.Visible = true;
            }
        }

        private void buttonShowAll_Click(object sender, EventArgs e)
        {
            for (var index = 0; index < checkedListBoxGroupFilter.Items.Count; index++)
                checkedListBoxGroupFilter.SetItemChecked(index, true);
            buttonApplyFilter_Click(null, null);
        }
    }
}
