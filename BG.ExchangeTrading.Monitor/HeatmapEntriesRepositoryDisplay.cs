﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Monitor
{
    public class HeatmapEntriesRepositoryDisplay
    {
        [ColumnStyle(DateFormat = "dd/MM/yyyy")]
        public DateTime EntryDate { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public string EntrySpread { get; }
        public double EntryPrice { get; }
        public string BufferPrice { get; }
        [ColumnStyle(DateFormat = "dd/MM/yyyy")]
        public DateTime ExitDate { get; }
        public double ExitPrice { get; }
        [ColumnStyle(ColorAlgoLogic = 6)]
        public bool Win { get; }

        public HeatmapEntriesRepositoryDisplay(DateTime entryDate,
            double entrySpread,
            double entryPrice,
            double bufferPrice,
            DateTime exitDate,
            double exitPrice,
            bool win)
        {
            EntryDate = entryDate;
            EntrySpread = entrySpread.ToString("F2"); 
            EntryPrice = entryPrice;
            BufferPrice = bufferPrice.ToString("F2");
            ExitDate = exitDate;
            ExitPrice = exitPrice;
            Win = win;
        }
    }
}
