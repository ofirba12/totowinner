﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Monitor
{
    internal class PortfolioAssetDisplay
    {
        [ColumnStyle(Hide = true )]
        public int PortfolioId { get; }
        [ColumnStyle(Hide = true)] 
        public int AssetId { get; }
        [ColumnStyle(Hide = true)]
        public int TickerId { get; }
        public string TickerType { get; }
        public string Symbol { get; }
        public double? LastSpot { get; }
        [ColumnStyle(Width = 140)]
        public DateTime? LastSpotUpdate { get; }
        [ColumnStyle(ColorAlgoLogic = 10)] 
        public string Distance { get; }
        [ColumnStyle(DisplayName = "CALL/PUT")]
        public string CallPut { get; }
        public double Strike { get; }
        [ColumnStyle(DisplayName = "LONG/SHORT")]
        public string LongShort { get; }
        public double Premium { get; }
        public int Size { get; }
        public double Fee { get; }
        [ColumnStyle(ColorAlgoLogic = 5)]
        public int FairValue { get; }
        public double TotalPremium { get; }
        public string TotalExpose { get; }
        public double? SpotAtExpiry { get; }
        [ColumnStyle(ColorAlgoLogic = 9 )]
        public string Success { get; }
        public string TickerFullName { get; }
        [ColumnStyle(DateFormat = "dd/MM/yyyy")]
        public DateTime TickerExpiry { get; }
        [ColumnStyle(Hide = true)]
        public string Group { get; }
        public PortfolioAssetDisplay(int portfolioId,
            int assetId,
            int tickerId,
            string symbol,
            string fullName,
            string type,
            string group,
            DateTime tickerExpiry,
            bool isCall,
            double strike,
            bool isLong,
            double premium,
            int size,
            int fairValue,
            double fee,
            double? lastSpot,
            DateTime? lastSpotTimeStamp,
            double? expirySpot)
        {
            this.PortfolioId = portfolioId;
            this.AssetId = assetId;
            this.TickerId = tickerId;
            this.Symbol = symbol;
            this.TickerFullName = fullName;
            this.TickerType = type;
            this.Group = group;
            this.TickerExpiry = tickerExpiry;
            this.LongShort = isLong
                ? "LONG"
                : "SHORT";
            this.Strike = strike;
            this.CallPut = isCall 
                ? "CALL" 
                : "PUT";
            this.Premium = premium;
            this.Size = size;
            this.FairValue = fairValue;
            this.Fee = fee;
            this.TotalPremium = (premium - fee) * size * 100;
            var totalExpose = strike * size* 100;
            this.TotalExpose = totalExpose.ToString("N0"); 
            this.LastSpot = lastSpot;
            this.LastSpotUpdate = lastSpotTimeStamp;
            var distance = this.LastSpot.HasValue
                ? (this.LastSpot.Value / strike - 1) * 100
                : (double?)null;
            this.Distance = distance?.ToString("F2");
            //if (this.LastSpot.HasValue)
            //{
            //    var distance = isCall && this.LastSpot.HasValue
            //        ? (strike / this.LastSpot.Value - 1) * 100
            //        : (this.LastSpot.Value / strike - 1) * 100;
            //    this.Distance = distance.ToString("F2");
            //}
            //var eodTime = DateTime.Now.Date.AddHours(23).AddMinutes(30);
            if (expirySpot.HasValue)
            {
                this.SpotAtExpiry = expirySpot;
                var formula = expirySpot.Value - strike;
                var success = (isCall && isLong && formula > 0) ||
                              (isCall && !isLong && formula <= 0) ||
                              (!isCall && isLong && formula < 0) ||
                              (!isCall && !isLong && formula >= 0);
                this.Success = success
                    ? "Success"
                    : "Fail";
            }
        }
    }
}
