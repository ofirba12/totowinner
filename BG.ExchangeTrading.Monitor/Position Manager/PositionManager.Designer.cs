﻿namespace BG.ExchangeTrading.Monitor
{
    partial class PositionManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTopBar = new System.Windows.Forms.Panel();
            this.buttonApplyRange = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.buttonDeletePortfolio = new System.Windows.Forms.Button();
            this.buttonAddRow = new System.Windows.Forms.Button();
            this.comboBoxPortfolioExpiry = new System.Windows.Forms.ComboBox();
            this.labelChooseExpiry = new System.Windows.Forms.Label();
            this.buttonAddPortfolio = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.labelShortVix = new System.Windows.Forms.Label();
            this.labelLongVix = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.labelTotalExpose = new System.Windows.Forms.Label();
            this.labelTe = new System.Windows.Forms.Label();
            this.labelShortCountry = new System.Windows.Forms.Label();
            this.labelLongCountry = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelTotalShort = new System.Windows.Forms.Label();
            this.labelTotalLong = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelShortOthers = new System.Windows.Forms.Label();
            this.labelLongOthers = new System.Windows.Forms.Label();
            this.labelShortFx = new System.Windows.Forms.Label();
            this.labelLongFx = new System.Windows.Forms.Label();
            this.labelShortBond = new System.Windows.Forms.Label();
            this.labelLongBond = new System.Windows.Forms.Label();
            this.labelShortIndex = new System.Windows.Forms.Label();
            this.labelLongIndex = new System.Windows.Forms.Label();
            this.labelShortStock = new System.Windows.Forms.Label();
            this.labelLongStock = new System.Windows.Forms.Label();
            this.labelShortCM = new System.Windows.Forms.Label();
            this.labelLongCM = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelTotalFailed = new System.Windows.Forms.Label();
            this.labelTotalSuccess = new System.Windows.Forms.Label();
            this.labelTotalFailedStr = new System.Windows.Forms.Label();
            this.labelTotalSuccStr = new System.Windows.Forms.Label();
            this.labelRed = new System.Windows.Forms.Label();
            this.labelYellow = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelGreen = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelTotalPremium = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTotalAssets = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelPortfolio = new System.Windows.Forms.Panel();
            this.gdvAssets = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Asset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelFv = new System.Windows.Forms.Label();
            this.labelFairValue = new System.Windows.Forms.Label();
            this.panelTopBar.SuspendLayout();
            this.panelStatus.SuspendLayout();
            this.panelPortfolio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdvAssets)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTopBar
            // 
            this.panelTopBar.Controls.Add(this.buttonApplyRange);
            this.panelTopBar.Controls.Add(this.label19);
            this.panelTopBar.Controls.Add(this.label18);
            this.panelTopBar.Controls.Add(this.dateTimePickerTo);
            this.panelTopBar.Controls.Add(this.dateTimePickerFrom);
            this.panelTopBar.Controls.Add(this.buttonDeletePortfolio);
            this.panelTopBar.Controls.Add(this.buttonAddRow);
            this.panelTopBar.Controls.Add(this.comboBoxPortfolioExpiry);
            this.panelTopBar.Controls.Add(this.labelChooseExpiry);
            this.panelTopBar.Controls.Add(this.buttonAddPortfolio);
            this.panelTopBar.Controls.Add(this.buttonRefresh);
            this.panelTopBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTopBar.Location = new System.Drawing.Point(0, 0);
            this.panelTopBar.Name = "panelTopBar";
            this.panelTopBar.Size = new System.Drawing.Size(1253, 54);
            this.panelTopBar.TabIndex = 0;
            // 
            // buttonApplyRange
            // 
            this.buttonApplyRange.Location = new System.Drawing.Point(159, 9);
            this.buttonApplyRange.Name = "buttonApplyRange";
            this.buttonApplyRange.Size = new System.Drawing.Size(75, 37);
            this.buttonApplyRange.TabIndex = 10;
            this.buttonApplyRange.Text = "Apply Range";
            this.buttonApplyRange.UseVisualStyleBackColor = true;
            this.buttonApplyRange.Click += new System.EventHandler(this.buttonApplyRange_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 33);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(23, 13);
            this.label19.TabIndex = 9;
            this.label19.Text = "To:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "From:";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.Location = new System.Drawing.Point(55, 28);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(98, 20);
            this.dateTimePickerTo.TabIndex = 7;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(55, 3);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(98, 20);
            this.dateTimePickerFrom.TabIndex = 6;
            // 
            // buttonDeletePortfolio
            // 
            this.buttonDeletePortfolio.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDeletePortfolio.Location = new System.Drawing.Point(1028, 0);
            this.buttonDeletePortfolio.Name = "buttonDeletePortfolio";
            this.buttonDeletePortfolio.Size = new System.Drawing.Size(75, 54);
            this.buttonDeletePortfolio.TabIndex = 5;
            this.buttonDeletePortfolio.Text = "Delete Current Portfolio";
            this.buttonDeletePortfolio.UseVisualStyleBackColor = true;
            this.buttonDeletePortfolio.Click += new System.EventHandler(this.buttonDeletePortfolio_Click);
            // 
            // buttonAddRow
            // 
            this.buttonAddRow.Enabled = false;
            this.buttonAddRow.Location = new System.Drawing.Point(720, 9);
            this.buttonAddRow.Name = "buttonAddRow";
            this.buttonAddRow.Size = new System.Drawing.Size(75, 37);
            this.buttonAddRow.TabIndex = 4;
            this.buttonAddRow.Text = "Add Row";
            this.buttonAddRow.UseVisualStyleBackColor = true;
            this.buttonAddRow.Click += new System.EventHandler(this.buttonAddRow_Click);
            // 
            // comboBoxPortfolioExpiry
            // 
            this.comboBoxPortfolioExpiry.FormattingEnabled = true;
            this.comboBoxPortfolioExpiry.Items.AddRange(new object[] {
            "14 Nov 2023",
            "25 Nov 2023"});
            this.comboBoxPortfolioExpiry.Location = new System.Drawing.Point(356, 18);
            this.comboBoxPortfolioExpiry.Name = "comboBoxPortfolioExpiry";
            this.comboBoxPortfolioExpiry.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPortfolioExpiry.TabIndex = 3;
            this.comboBoxPortfolioExpiry.SelectedIndexChanged += new System.EventHandler(this.comboBoxPortfolioExpiry_SelectedIndexChanged);
            // 
            // labelChooseExpiry
            // 
            this.labelChooseExpiry.AutoSize = true;
            this.labelChooseExpiry.Location = new System.Drawing.Point(266, 21);
            this.labelChooseExpiry.Name = "labelChooseExpiry";
            this.labelChooseExpiry.Size = new System.Drawing.Size(74, 13);
            this.labelChooseExpiry.TabIndex = 2;
            this.labelChooseExpiry.Text = "Choose Expiry";
            // 
            // buttonAddPortfolio
            // 
            this.buttonAddPortfolio.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddPortfolio.Location = new System.Drawing.Point(1103, 0);
            this.buttonAddPortfolio.Name = "buttonAddPortfolio";
            this.buttonAddPortfolio.Size = new System.Drawing.Size(75, 54);
            this.buttonAddPortfolio.TabIndex = 1;
            this.buttonAddPortfolio.Text = "Add Portfolio";
            this.buttonAddPortfolio.UseVisualStyleBackColor = true;
            this.buttonAddPortfolio.Click += new System.EventHandler(this.buttonAddPortfolio_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRefresh.Location = new System.Drawing.Point(1178, 0);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 54);
            this.buttonRefresh.TabIndex = 0;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // panelStatus
            // 
            this.panelStatus.Controls.Add(this.labelFairValue);
            this.panelStatus.Controls.Add(this.labelFv);
            this.panelStatus.Controls.Add(this.labelShortVix);
            this.panelStatus.Controls.Add(this.labelLongVix);
            this.panelStatus.Controls.Add(this.label22);
            this.panelStatus.Controls.Add(this.labelTotalExpose);
            this.panelStatus.Controls.Add(this.labelTe);
            this.panelStatus.Controls.Add(this.labelShortCountry);
            this.panelStatus.Controls.Add(this.labelLongCountry);
            this.panelStatus.Controls.Add(this.label17);
            this.panelStatus.Controls.Add(this.labelTotalShort);
            this.panelStatus.Controls.Add(this.labelTotalLong);
            this.panelStatus.Controls.Add(this.label16);
            this.panelStatus.Controls.Add(this.labelShortOthers);
            this.panelStatus.Controls.Add(this.labelLongOthers);
            this.panelStatus.Controls.Add(this.labelShortFx);
            this.panelStatus.Controls.Add(this.labelLongFx);
            this.panelStatus.Controls.Add(this.labelShortBond);
            this.panelStatus.Controls.Add(this.labelLongBond);
            this.panelStatus.Controls.Add(this.labelShortIndex);
            this.panelStatus.Controls.Add(this.labelLongIndex);
            this.panelStatus.Controls.Add(this.labelShortStock);
            this.panelStatus.Controls.Add(this.labelLongStock);
            this.panelStatus.Controls.Add(this.labelShortCM);
            this.panelStatus.Controls.Add(this.labelLongCM);
            this.panelStatus.Controls.Add(this.label15);
            this.panelStatus.Controls.Add(this.label14);
            this.panelStatus.Controls.Add(this.label13);
            this.panelStatus.Controls.Add(this.label12);
            this.panelStatus.Controls.Add(this.label11);
            this.panelStatus.Controls.Add(this.label10);
            this.panelStatus.Controls.Add(this.label9);
            this.panelStatus.Controls.Add(this.label8);
            this.panelStatus.Controls.Add(this.labelTotalFailed);
            this.panelStatus.Controls.Add(this.labelTotalSuccess);
            this.panelStatus.Controls.Add(this.labelTotalFailedStr);
            this.panelStatus.Controls.Add(this.labelTotalSuccStr);
            this.panelStatus.Controls.Add(this.labelRed);
            this.panelStatus.Controls.Add(this.labelYellow);
            this.panelStatus.Controls.Add(this.label5);
            this.panelStatus.Controls.Add(this.label4);
            this.panelStatus.Controls.Add(this.labelGreen);
            this.panelStatus.Controls.Add(this.label3);
            this.panelStatus.Controls.Add(this.labelTotalPremium);
            this.panelStatus.Controls.Add(this.label2);
            this.panelStatus.Controls.Add(this.labelTotalAssets);
            this.panelStatus.Controls.Add(this.label1);
            this.panelStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStatus.Location = new System.Drawing.Point(0, 54);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(1253, 202);
            this.panelStatus.TabIndex = 1;
            // 
            // labelShortVix
            // 
            this.labelShortVix.AutoSize = true;
            this.labelShortVix.Location = new System.Drawing.Point(893, 143);
            this.labelShortVix.Name = "labelShortVix";
            this.labelShortVix.Size = new System.Drawing.Size(19, 13);
            this.labelShortVix.TabIndex = 44;
            this.labelShortVix.Text = "12";
            // 
            // labelLongVix
            // 
            this.labelLongVix.AutoSize = true;
            this.labelLongVix.Location = new System.Drawing.Point(806, 143);
            this.labelLongVix.Name = "labelLongVix";
            this.labelLongVix.Size = new System.Drawing.Size(19, 13);
            this.labelLongVix.TabIndex = 43;
            this.labelLongVix.Text = "12";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(726, 143);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "VIX";
            // 
            // labelTotalExpose
            // 
            this.labelTotalExpose.AutoSize = true;
            this.labelTotalExpose.Location = new System.Drawing.Point(99, 90);
            this.labelTotalExpose.Name = "labelTotalExpose";
            this.labelTotalExpose.Size = new System.Drawing.Size(25, 13);
            this.labelTotalExpose.TabIndex = 41;
            this.labelTotalExpose.Text = "153";
            // 
            // labelTe
            // 
            this.labelTe.AutoSize = true;
            this.labelTe.Location = new System.Drawing.Point(15, 90);
            this.labelTe.Name = "labelTe";
            this.labelTe.Size = new System.Drawing.Size(69, 13);
            this.labelTe.TabIndex = 40;
            this.labelTe.Text = "Total Expose";
            // 
            // labelShortCountry
            // 
            this.labelShortCountry.AutoSize = true;
            this.labelShortCountry.Location = new System.Drawing.Point(893, 72);
            this.labelShortCountry.Name = "labelShortCountry";
            this.labelShortCountry.Size = new System.Drawing.Size(19, 13);
            this.labelShortCountry.TabIndex = 39;
            this.labelShortCountry.Text = "12";
            // 
            // labelLongCountry
            // 
            this.labelLongCountry.AutoSize = true;
            this.labelLongCountry.Location = new System.Drawing.Point(806, 72);
            this.labelLongCountry.Name = "labelLongCountry";
            this.labelLongCountry.Size = new System.Drawing.Size(19, 13);
            this.labelLongCountry.TabIndex = 38;
            this.labelLongCountry.Text = "12";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(726, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "COUNTRY";
            // 
            // labelTotalShort
            // 
            this.labelTotalShort.AutoSize = true;
            this.labelTotalShort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelTotalShort.Location = new System.Drawing.Point(893, 166);
            this.labelTotalShort.Name = "labelTotalShort";
            this.labelTotalShort.Size = new System.Drawing.Size(21, 13);
            this.labelTotalShort.TabIndex = 36;
            this.labelTotalShort.Text = "12";
            // 
            // labelTotalLong
            // 
            this.labelTotalLong.AutoSize = true;
            this.labelTotalLong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelTotalLong.Location = new System.Drawing.Point(806, 166);
            this.labelTotalLong.Name = "labelTotalLong";
            this.labelTotalLong.Size = new System.Drawing.Size(21, 13);
            this.labelTotalLong.TabIndex = 35;
            this.labelTotalLong.Text = "12";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label16.Location = new System.Drawing.Point(725, 166);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "TOTALS";
            // 
            // labelShortOthers
            // 
            this.labelShortOthers.AutoSize = true;
            this.labelShortOthers.Location = new System.Drawing.Point(893, 125);
            this.labelShortOthers.Name = "labelShortOthers";
            this.labelShortOthers.Size = new System.Drawing.Size(19, 13);
            this.labelShortOthers.TabIndex = 33;
            this.labelShortOthers.Text = "12";
            // 
            // labelLongOthers
            // 
            this.labelLongOthers.AutoSize = true;
            this.labelLongOthers.Location = new System.Drawing.Point(806, 125);
            this.labelLongOthers.Name = "labelLongOthers";
            this.labelLongOthers.Size = new System.Drawing.Size(19, 13);
            this.labelLongOthers.TabIndex = 32;
            this.labelLongOthers.Text = "12";
            // 
            // labelShortFx
            // 
            this.labelShortFx.AutoSize = true;
            this.labelShortFx.Location = new System.Drawing.Point(893, 108);
            this.labelShortFx.Name = "labelShortFx";
            this.labelShortFx.Size = new System.Drawing.Size(19, 13);
            this.labelShortFx.TabIndex = 31;
            this.labelShortFx.Text = "12";
            // 
            // labelLongFx
            // 
            this.labelLongFx.AutoSize = true;
            this.labelLongFx.Location = new System.Drawing.Point(806, 108);
            this.labelLongFx.Name = "labelLongFx";
            this.labelLongFx.Size = new System.Drawing.Size(19, 13);
            this.labelLongFx.TabIndex = 30;
            this.labelLongFx.Text = "12";
            // 
            // labelShortBond
            // 
            this.labelShortBond.AutoSize = true;
            this.labelShortBond.Location = new System.Drawing.Point(893, 90);
            this.labelShortBond.Name = "labelShortBond";
            this.labelShortBond.Size = new System.Drawing.Size(19, 13);
            this.labelShortBond.TabIndex = 29;
            this.labelShortBond.Text = "12";
            // 
            // labelLongBond
            // 
            this.labelLongBond.AutoSize = true;
            this.labelLongBond.Location = new System.Drawing.Point(806, 90);
            this.labelLongBond.Name = "labelLongBond";
            this.labelLongBond.Size = new System.Drawing.Size(19, 13);
            this.labelLongBond.TabIndex = 28;
            this.labelLongBond.Text = "12";
            // 
            // labelShortIndex
            // 
            this.labelShortIndex.AutoSize = true;
            this.labelShortIndex.Location = new System.Drawing.Point(893, 54);
            this.labelShortIndex.Name = "labelShortIndex";
            this.labelShortIndex.Size = new System.Drawing.Size(19, 13);
            this.labelShortIndex.TabIndex = 27;
            this.labelShortIndex.Text = "12";
            // 
            // labelLongIndex
            // 
            this.labelLongIndex.AutoSize = true;
            this.labelLongIndex.Location = new System.Drawing.Point(806, 54);
            this.labelLongIndex.Name = "labelLongIndex";
            this.labelLongIndex.Size = new System.Drawing.Size(19, 13);
            this.labelLongIndex.TabIndex = 26;
            this.labelLongIndex.Text = "12";
            // 
            // labelShortStock
            // 
            this.labelShortStock.AutoSize = true;
            this.labelShortStock.Location = new System.Drawing.Point(893, 37);
            this.labelShortStock.Name = "labelShortStock";
            this.labelShortStock.Size = new System.Drawing.Size(19, 13);
            this.labelShortStock.TabIndex = 25;
            this.labelShortStock.Text = "12";
            // 
            // labelLongStock
            // 
            this.labelLongStock.AutoSize = true;
            this.labelLongStock.Location = new System.Drawing.Point(806, 37);
            this.labelLongStock.Name = "labelLongStock";
            this.labelLongStock.Size = new System.Drawing.Size(19, 13);
            this.labelLongStock.TabIndex = 24;
            this.labelLongStock.Text = "12";
            // 
            // labelShortCM
            // 
            this.labelShortCM.AutoSize = true;
            this.labelShortCM.Location = new System.Drawing.Point(893, 18);
            this.labelShortCM.Name = "labelShortCM";
            this.labelShortCM.Size = new System.Drawing.Size(19, 13);
            this.labelShortCM.TabIndex = 23;
            this.labelShortCM.Text = "12";
            // 
            // labelLongCM
            // 
            this.labelLongCM.AutoSize = true;
            this.labelLongCM.Location = new System.Drawing.Point(806, 18);
            this.labelLongCM.Name = "labelLongCM";
            this.labelLongCM.Size = new System.Drawing.Size(19, 13);
            this.labelLongCM.TabIndex = 22;
            this.labelLongCM.Text = "12";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(726, 125);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "OTHER";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(725, 108);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "FX";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(725, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "BOND";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(725, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "INDEX";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(725, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "STOCK";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(726, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "CM";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label9.Location = new System.Drawing.Point(885, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "SHORT";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label8.Location = new System.Drawing.Point(803, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "LONG";
            // 
            // labelTotalFailed
            // 
            this.labelTotalFailed.AutoSize = true;
            this.labelTotalFailed.Location = new System.Drawing.Point(550, 50);
            this.labelTotalFailed.Name = "labelTotalFailed";
            this.labelTotalFailed.Size = new System.Drawing.Size(19, 13);
            this.labelTotalFailed.TabIndex = 13;
            this.labelTotalFailed.Text = "12";
            // 
            // labelTotalSuccess
            // 
            this.labelTotalSuccess.AutoSize = true;
            this.labelTotalSuccess.Location = new System.Drawing.Point(550, 16);
            this.labelTotalSuccess.Name = "labelTotalSuccess";
            this.labelTotalSuccess.Size = new System.Drawing.Size(19, 13);
            this.labelTotalSuccess.TabIndex = 12;
            this.labelTotalSuccess.Text = "12";
            // 
            // labelTotalFailedStr
            // 
            this.labelTotalFailedStr.AutoSize = true;
            this.labelTotalFailedStr.Location = new System.Drawing.Point(449, 50);
            this.labelTotalFailedStr.Name = "labelTotalFailedStr";
            this.labelTotalFailedStr.Size = new System.Drawing.Size(62, 13);
            this.labelTotalFailedStr.TabIndex = 11;
            this.labelTotalFailedStr.Text = "Total Failed";
            // 
            // labelTotalSuccStr
            // 
            this.labelTotalSuccStr.AutoSize = true;
            this.labelTotalSuccStr.Location = new System.Drawing.Point(449, 16);
            this.labelTotalSuccStr.Name = "labelTotalSuccStr";
            this.labelTotalSuccStr.Size = new System.Drawing.Size(75, 13);
            this.labelTotalSuccStr.TabIndex = 10;
            this.labelTotalSuccStr.Text = "Total Success";
            // 
            // labelRed
            // 
            this.labelRed.AutoSize = true;
            this.labelRed.Location = new System.Drawing.Point(318, 50);
            this.labelRed.Name = "labelRed";
            this.labelRed.Size = new System.Drawing.Size(19, 13);
            this.labelRed.TabIndex = 9;
            this.labelRed.Text = "12";
            // 
            // labelYellow
            // 
            this.labelYellow.AutoSize = true;
            this.labelYellow.Location = new System.Drawing.Point(318, 33);
            this.labelYellow.Name = "labelYellow";
            this.labelYellow.Size = new System.Drawing.Size(19, 13);
            this.labelYellow.TabIndex = 8;
            this.labelYellow.Text = "12";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(244, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Red";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(244, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Yellow";
            // 
            // labelGreen
            // 
            this.labelGreen.AutoSize = true;
            this.labelGreen.Location = new System.Drawing.Point(318, 16);
            this.labelGreen.Name = "labelGreen";
            this.labelGreen.Size = new System.Drawing.Size(19, 13);
            this.labelGreen.TabIndex = 5;
            this.labelGreen.Text = "12";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(244, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Green";
            // 
            // labelTotalPremium
            // 
            this.labelTotalPremium.AutoSize = true;
            this.labelTotalPremium.Location = new System.Drawing.Point(99, 50);
            this.labelTotalPremium.Name = "labelTotalPremium";
            this.labelTotalPremium.Size = new System.Drawing.Size(25, 13);
            this.labelTotalPremium.TabIndex = 3;
            this.labelTotalPremium.Text = "153";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Total Premium";
            // 
            // labelTotalAssets
            // 
            this.labelTotalAssets.AutoSize = true;
            this.labelTotalAssets.Location = new System.Drawing.Point(99, 16);
            this.labelTotalAssets.Name = "labelTotalAssets";
            this.labelTotalAssets.Size = new System.Drawing.Size(19, 13);
            this.labelTotalAssets.TabIndex = 1;
            this.labelTotalAssets.Text = "12";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Assets";
            // 
            // panelPortfolio
            // 
            this.panelPortfolio.Controls.Add(this.gdvAssets);
            this.panelPortfolio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPortfolio.Location = new System.Drawing.Point(0, 256);
            this.panelPortfolio.Name = "panelPortfolio";
            this.panelPortfolio.Size = new System.Drawing.Size(1253, 384);
            this.panelPortfolio.TabIndex = 2;
            // 
            // gdvAssets
            // 
            this.gdvAssets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdvAssets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.Asset,
            this.FullName});
            this.gdvAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdvAssets.Location = new System.Drawing.Point(0, 0);
            this.gdvAssets.Name = "gdvAssets";
            this.gdvAssets.Size = new System.Drawing.Size(1253, 384);
            this.gdvAssets.TabIndex = 0;
            this.gdvAssets.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdvAssets_CellMouseEnter);
            // 
            // Index
            // 
            this.Index.HeaderText = "#";
            this.Index.Name = "Index";
            // 
            // Asset
            // 
            this.Asset.HeaderText = "AssetName";
            this.Asset.Name = "Asset";
            // 
            // FullName
            // 
            this.FullName.HeaderText = "Full Name";
            this.FullName.Name = "FullName";
            // 
            // labelFv
            // 
            this.labelFv.AutoSize = true;
            this.labelFv.Location = new System.Drawing.Point(15, 125);
            this.labelFv.Name = "labelFv";
            this.labelFv.Size = new System.Drawing.Size(54, 13);
            this.labelFv.TabIndex = 45;
            this.labelFv.Text = "Fair Value";
            // 
            // labelFairValue
            // 
            this.labelFairValue.AutoSize = true;
            this.labelFairValue.Location = new System.Drawing.Point(99, 125);
            this.labelFairValue.Name = "labelFairValue";
            this.labelFairValue.Size = new System.Drawing.Size(25, 13);
            this.labelFairValue.TabIndex = 46;
            this.labelFairValue.Text = "153";
            // 
            // PositionManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 640);
            this.Controls.Add(this.panelPortfolio);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.panelTopBar);
            this.Name = "PositionManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PositionManager";
            this.panelTopBar.ResumeLayout(false);
            this.panelTopBar.PerformLayout();
            this.panelStatus.ResumeLayout(false);
            this.panelStatus.PerformLayout();
            this.panelPortfolio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdvAssets)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTopBar;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonAddPortfolio;
        private System.Windows.Forms.Panel panelStatus;
        private System.Windows.Forms.Panel panelPortfolio;
        private System.Windows.Forms.ComboBox comboBoxPortfolioExpiry;
        private System.Windows.Forms.Label labelChooseExpiry;
        private System.Windows.Forms.DataGridView gdvAssets;
        private System.Windows.Forms.Label labelTotalPremium;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelTotalAssets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn Asset;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullName;
        private System.Windows.Forms.Button buttonAddRow;
        private System.Windows.Forms.Button buttonDeletePortfolio;
        private System.Windows.Forms.Label labelRed;
        private System.Windows.Forms.Label labelYellow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelGreen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelTotalFailed;
        private System.Windows.Forms.Label labelTotalSuccess;
        private System.Windows.Forms.Label labelTotalFailedStr;
        private System.Windows.Forms.Label labelTotalSuccStr;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelShortOthers;
        private System.Windows.Forms.Label labelLongOthers;
        private System.Windows.Forms.Label labelShortFx;
        private System.Windows.Forms.Label labelLongFx;
        private System.Windows.Forms.Label labelShortBond;
        private System.Windows.Forms.Label labelLongBond;
        private System.Windows.Forms.Label labelShortIndex;
        private System.Windows.Forms.Label labelLongIndex;
        private System.Windows.Forms.Label labelShortStock;
        private System.Windows.Forms.Label labelLongStock;
        private System.Windows.Forms.Label labelShortCM;
        private System.Windows.Forms.Label labelLongCM;
        private System.Windows.Forms.Label labelTotalShort;
        private System.Windows.Forms.Label labelTotalLong;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelLongCountry;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelShortCountry;
        private System.Windows.Forms.Label labelTe;
        private System.Windows.Forms.Label labelTotalExpose;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Button buttonApplyRange;
        private System.Windows.Forms.Label labelShortVix;
        private System.Windows.Forms.Label labelLongVix;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labelFairValue;
        private System.Windows.Forms.Label labelFv;
    }
}