﻿namespace BG.ExchangeTrading.Monitor
{
    partial class AddAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelExpiryDate = new System.Windows.Forms.Label();
            this.labelPorfolioExpiry = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelAssetFullName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.dateTimePickerAssetExpiry = new System.Windows.Forms.DateTimePicker();
            this.textBoxAsset = new System.Windows.Forms.TextBox();
            this.textBoxAssetFullName = new System.Windows.Forms.TextBox();
            this.dateTimePickerPortfolioExpiry = new System.Windows.Forms.DateTimePicker();
            this.comboBoxCallPut = new System.Windows.Forms.ComboBox();
            this.comboBoxLongShort = new System.Windows.Forms.ComboBox();
            this.numericUpDownStrike = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPremium = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSize = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFee = new System.Windows.Forms.NumericUpDown();
            this.textBoxType = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStrike)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPremium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFee)).BeginInit();
            this.SuspendLayout();
            // 
            // labelExpiryDate
            // 
            this.labelExpiryDate.AutoSize = true;
            this.labelExpiryDate.Location = new System.Drawing.Point(50, 45);
            this.labelExpiryDate.Name = "labelExpiryDate";
            this.labelExpiryDate.Size = new System.Drawing.Size(61, 13);
            this.labelExpiryDate.TabIndex = 0;
            this.labelExpiryDate.Text = "Expiry Date";
            // 
            // labelPorfolioExpiry
            // 
            this.labelPorfolioExpiry.AutoSize = true;
            this.labelPorfolioExpiry.Location = new System.Drawing.Point(370, 45);
            this.labelPorfolioExpiry.Name = "labelPorfolioExpiry";
            this.labelPorfolioExpiry.Size = new System.Drawing.Size(102, 13);
            this.labelPorfolioExpiry.TabIndex = 1;
            this.labelPorfolioExpiry.Text = "Portfolio Expiry Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Asset Name";
            // 
            // labelAssetFullName
            // 
            this.labelAssetFullName.AutoSize = true;
            this.labelAssetFullName.Location = new System.Drawing.Point(50, 116);
            this.labelAssetFullName.Name = "labelAssetFullName";
            this.labelAssetFullName.Size = new System.Drawing.Size(83, 13);
            this.labelAssetFullName.TabIndex = 3;
            this.labelAssetFullName.Text = "Asset Full Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Call/Put";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Strike";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(370, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Direction";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(370, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Premium";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(370, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Size";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Fee";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(154, 350);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(355, 23);
            this.buttonSave.TabIndex = 10;
            this.buttonSave.Text = "SAVE";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // dateTimePickerAssetExpiry
            // 
            this.dateTimePickerAssetExpiry.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerAssetExpiry.Location = new System.Drawing.Point(154, 45);
            this.dateTimePickerAssetExpiry.Name = "dateTimePickerAssetExpiry";
            this.dateTimePickerAssetExpiry.Size = new System.Drawing.Size(103, 20);
            this.dateTimePickerAssetExpiry.TabIndex = 1;
            // 
            // textBoxAsset
            // 
            this.textBoxAsset.Location = new System.Drawing.Point(154, 77);
            this.textBoxAsset.Name = "textBoxAsset";
            this.textBoxAsset.Size = new System.Drawing.Size(428, 20);
            this.textBoxAsset.TabIndex = 3;
            this.textBoxAsset.Leave += new System.EventHandler(this.textBoxAsset_Leave);
            // 
            // textBoxAssetFullName
            // 
            this.textBoxAssetFullName.Location = new System.Drawing.Point(154, 116);
            this.textBoxAssetFullName.Name = "textBoxAssetFullName";
            this.textBoxAssetFullName.Size = new System.Drawing.Size(428, 20);
            this.textBoxAssetFullName.TabIndex = 15;
            // 
            // dateTimePickerPortfolioExpiry
            // 
            this.dateTimePickerPortfolioExpiry.Enabled = false;
            this.dateTimePickerPortfolioExpiry.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerPortfolioExpiry.Location = new System.Drawing.Point(479, 42);
            this.dateTimePickerPortfolioExpiry.Name = "dateTimePickerPortfolioExpiry";
            this.dateTimePickerPortfolioExpiry.Size = new System.Drawing.Size(103, 20);
            this.dateTimePickerPortfolioExpiry.TabIndex = 2;
            // 
            // comboBoxCallPut
            // 
            this.comboBoxCallPut.FormattingEnabled = true;
            this.comboBoxCallPut.Items.AddRange(new object[] {
            "CALL",
            "PUT"});
            this.comboBoxCallPut.Location = new System.Drawing.Point(154, 188);
            this.comboBoxCallPut.Name = "comboBoxCallPut";
            this.comboBoxCallPut.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCallPut.TabIndex = 4;
            this.comboBoxCallPut.Text = "CALL";
            // 
            // comboBoxLongShort
            // 
            this.comboBoxLongShort.FormattingEnabled = true;
            this.comboBoxLongShort.Items.AddRange(new object[] {
            "LONG",
            "SHORT"});
            this.comboBoxLongShort.Location = new System.Drawing.Point(461, 188);
            this.comboBoxLongShort.Name = "comboBoxLongShort";
            this.comboBoxLongShort.Size = new System.Drawing.Size(121, 21);
            this.comboBoxLongShort.TabIndex = 5;
            this.comboBoxLongShort.Text = "LONG";
            // 
            // numericUpDownStrike
            // 
            this.numericUpDownStrike.DecimalPlaces = 2;
            this.numericUpDownStrike.Location = new System.Drawing.Point(154, 219);
            this.numericUpDownStrike.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownStrike.Name = "numericUpDownStrike";
            this.numericUpDownStrike.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownStrike.TabIndex = 6;
            this.numericUpDownStrike.Enter += new System.EventHandler(this.numericUpDownStrike_Enter);
            // 
            // numericUpDownPremium
            // 
            this.numericUpDownPremium.DecimalPlaces = 2;
            this.numericUpDownPremium.Location = new System.Drawing.Point(461, 219);
            this.numericUpDownPremium.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownPremium.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownPremium.Name = "numericUpDownPremium";
            this.numericUpDownPremium.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownPremium.TabIndex = 7;
            this.numericUpDownPremium.Enter += new System.EventHandler(this.numericUpDownPremium_Enter);
            // 
            // numericUpDownSize
            // 
            this.numericUpDownSize.Location = new System.Drawing.Point(461, 254);
            this.numericUpDownSize.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownSize.Name = "numericUpDownSize";
            this.numericUpDownSize.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownSize.TabIndex = 9;
            this.numericUpDownSize.Enter += new System.EventHandler(this.numericUpDownSize_Enter);
            // 
            // numericUpDownFee
            // 
            this.numericUpDownFee.DecimalPlaces = 3;
            this.numericUpDownFee.Location = new System.Drawing.Point(155, 254);
            this.numericUpDownFee.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownFee.Name = "numericUpDownFee";
            this.numericUpDownFee.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownFee.TabIndex = 8;
            this.numericUpDownFee.Value = new decimal(new int[] {
            25,
            0,
            0,
            196608});
            this.numericUpDownFee.Enter += new System.EventHandler(this.numericUpDownFee_Enter);
            // 
            // textBoxType
            // 
            this.textBoxType.Enabled = false;
            this.textBoxType.Location = new System.Drawing.Point(153, 150);
            this.textBoxType.Name = "textBoxType";
            this.textBoxType.Size = new System.Drawing.Size(122, 20);
            this.textBoxType.TabIndex = 40;
            // 
            // AddAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 399);
            this.Controls.Add(this.textBoxType);
            this.Controls.Add(this.numericUpDownFee);
            this.Controls.Add(this.numericUpDownSize);
            this.Controls.Add(this.numericUpDownPremium);
            this.Controls.Add(this.numericUpDownStrike);
            this.Controls.Add(this.comboBoxLongShort);
            this.Controls.Add(this.comboBoxCallPut);
            this.Controls.Add(this.dateTimePickerPortfolioExpiry);
            this.Controls.Add(this.textBoxAssetFullName);
            this.Controls.Add(this.textBoxAsset);
            this.Controls.Add(this.dateTimePickerAssetExpiry);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelAssetFullName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelPorfolioExpiry);
            this.Controls.Add(this.labelExpiryDate);
            this.Name = "AddAsset";
            this.Text = "AddAsset";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStrike)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPremium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelExpiryDate;
        private System.Windows.Forms.Label labelPorfolioExpiry;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelAssetFullName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.DateTimePicker dateTimePickerAssetExpiry;
        private System.Windows.Forms.TextBox textBoxAsset;
        private System.Windows.Forms.TextBox textBoxAssetFullName;
        private System.Windows.Forms.DateTimePicker dateTimePickerPortfolioExpiry;
        private System.Windows.Forms.ComboBox comboBoxCallPut;
        private System.Windows.Forms.ComboBox comboBoxLongShort;
        private System.Windows.Forms.NumericUpDown numericUpDownStrike;
        private System.Windows.Forms.NumericUpDown numericUpDownPremium;
        private System.Windows.Forms.NumericUpDown numericUpDownSize;
        private System.Windows.Forms.NumericUpDown numericUpDownFee;
        private System.Windows.Forms.TextBox textBoxType;
    }
}