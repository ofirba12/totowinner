﻿using BG.ExchangeTrading.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class AddPortfolio : Form
    {
        public DateTime PortfolioExpiry { get; set; }
        public AddPortfolio()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            this.PortfolioExpiry = this.dateTimePicker1.Value.Date;
            var portfolios = PositionServices.Instance.GetAllPortfolios();
            if (portfolios.FirstOrDefault(p => p.Expiry == PortfolioExpiry) != null)
                MessageBox.Show($"Expiry {PortfolioExpiry} already exists", "Portfolio already exist", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
                this.DialogResult = DialogResult.OK;
        }
    }
}
