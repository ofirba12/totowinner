﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class AddAsset : Form
    {
        private Portfolio _portfolio;
        private List<Services.Persistance.Ticker> existingTickers;
        public PortfolioAsset Asset { get; private set; }
        public AddAsset(Portfolio portfolio, bool isNew)
        {
            InitializeComponent();
            _portfolio = portfolio;
            dateTimePickerPortfolioExpiry.Value = portfolio.Expiry;
            dateTimePickerAssetExpiry.Value = portfolio.Expiry;
            existingTickers = TickersPersistanceServices.Instance.GetExisitingTickers();
            SetAutoComplete(existingTickers, this.textBoxAsset);
            if (isNew)
            {
                this.textBoxAssetFullName.Visible = false;
                this.labelAssetFullName.Text = string.Empty;
                this.comboBoxCallPut.Text = "PUT";
                this.comboBoxLongShort.Text = "SHORT";
            }
            else
            {
                this.textBoxAssetFullName.Visible = true;
                this.labelAssetFullName.Text = "Asset Full Name";
            }
        }
        private void SetAutoComplete(IEnumerable<Services.Persistance.Ticker> collection, TextBox textBox)
        {
            var source = new AutoCompleteStringCollection();
            source.AddRange(collection.Select(m => $"{m.Symbol} : {m.FullName}").Distinct().ToArray());
            textBox.AutoCompleteCustomSource = source;
            textBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!this.textBoxAsset.Text.Contains(':'))
            {
                MessageBox.Show($"Invalid Asset chosen {this.textBoxAsset.Text}", "Invalid Asset",MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var symbol = this.textBoxAsset.Text.Split(':')[0].Trim().ToUpper();
            var ticket = existingTickers.First(t => t.Symbol == symbol);
            this.Asset = new PortfolioAsset(this._portfolio.PortfolioId,
                -1,
                ticket.TickerId,
                ticket.Symbol,
                ticket.FullName,
                ticket.Type,
                ticket.Group,
                this.dateTimePickerAssetExpiry.Value,
                this.comboBoxCallPut.Text == "CALL",
                (double)this.numericUpDownStrike.Value,
                this.comboBoxLongShort.Text == "LONG",
                (double)this.numericUpDownPremium.Value,
                (int)this.numericUpDownSize.Value,
                (double)this.numericUpDownFee.Value);
            this.DialogResult = DialogResult.OK;
        }

        private void textBoxAsset_Leave(object sender, EventArgs e)
        {
            if (this.textBoxAsset.Text.Contains(':'))
            {
                var symbol = this.textBoxAsset.Text.Split(':')[0].Trim().ToUpper();
                var ticket = existingTickers.First(t => t.Symbol == symbol);
                this.textBoxType.Text = ticket.Type;
                //this.textBoxAssetFullName.Text = ticket.FullName;
            }
        }
        private void numericUpDownStrike_Enter(object sender, EventArgs e)
        {
            numericUpDownStrike.Select(0, numericUpDownStrike.Text.Length);
        }

        private void numericUpDownPremium_Enter(object sender, EventArgs e)
        {
            numericUpDownPremium.Select(0, numericUpDownPremium.Text.Length);
        }

        private void numericUpDownFee_Enter(object sender, EventArgs e)
        {
            numericUpDownFee.Select(0, numericUpDownFee.Text.Length);
        }

        private void numericUpDownSize_Enter(object sender, EventArgs e)
        {
            numericUpDownSize.Select(0, numericUpDownSize.Text.Length);
        }
    }
}
