﻿using BG.ExchangeTrading.Data;
using BG.ExchangeTrading.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    public partial class PositionManager : Form
    {
        private List<Portfolio> portfolios;
        private Dictionary<int,BGTickerDetails> instruments;
        private DataGridViewCellEventArgs mouseLocation;
        private ToolStripMenuItem contextDeleteAsset = new ToolStripMenuItem();

        public PositionManager()
        {
            InitializeComponent();
            InitPortfolios();
        }

        private void InitPortfolios()
        {
            portfolios = PositionServices.Instance.GetAllPortfolios();
            this.comboBoxPortfolioExpiry.DataSource = portfolios
                .Where(p => p.Expiry >= DateTime.Today.AddDays(-7))
                .Select(p => p.Expiry).OrderBy(p => p).ToList();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            var selectedExpiry = DateTime.Parse(this.comboBoxPortfolioExpiry.Text);
            InitPortfolios();
            this.comboBoxPortfolioExpiry.Text = selectedExpiry.ToString("d");
        }

        private void buttonAddPortfolio_Click(object sender, EventArgs e)
        {
            var dialog = new AddPortfolio();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                PositionPersistanceServices.Instance.AddPortfolio(dialog.PortfolioExpiry);
                InitPortfolios();
                var selectedPortfolio = portfolios.First(p => p.Expiry == dialog.PortfolioExpiry);
                this.comboBoxPortfolioExpiry.Text = selectedPortfolio.Expiry.ToString("d");
                RefreshPortfolio();
                //this.comboBoxPortfolioExpiry.Text = ((List<DateTime>)this.comboBoxPortfolioExpiry.DataSource).First(p => p == dialog.PortfolioExpiry).ToString("d");
            }
        }

        private void comboBoxPortfolioExpiry_SelectedIndexChanged(object sender, EventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (DateTime.TryParse(comboBox.Text, out var expiry))
            {
                RefreshPortfolio();
                this.buttonAddRow.Enabled = true;
            }
        }
        private void RefreshPortfolio()
        {
            var selectedPortfolio = GetSelectedPortoflio();
            var assets = PositionServices.Instance.GetPortfolioAssets(selectedPortfolio.PortfolioId);
            instruments = TickerServices.Instance.GetExistingTickersWithDetails()
                                .ToDictionary(x => x.TickerId, v => v);
            var tickersExpiries = assets.ConvertAll<TickerIdExpiry>(x => new TickerIdExpiry(x.TickerId, x.TickerExpiry));
            var expirySpots = PositionServices.Instance.GetLastEodSpotPerExpiry(tickersExpiries);
            
            this.gdvAssets.Rows.Clear();
            this.gdvAssets.Columns.Clear();
            this.gdvAssets.Refresh();
            var calculatedAssets = assets.ConvertAll<PortfolioAssetDisplay>(a =>
            {
                double? lastSpot = null;
                DateTime? lastSpotUpdate = null;
                if (instruments[a.TickerId].LastEndOfDaySpotTimeStamp > instruments[a.TickerId].LastLiveSpotTimeStamp)
                {
                    lastSpot = instruments[a.TickerId].LastEndOfDaySpot;
                    lastSpotUpdate = instruments[a.TickerId].LastEndOfDaySpotTimeStamp;
                }
                else
                {
                    lastSpot = instruments[a.TickerId].LastLiveSpot;
                    lastSpotUpdate = instruments[a.TickerId].LastLiveSpotTimeStamp;
                }
                var spotExpiryKey = new TickerIdExpiry(a.TickerId, a.TickerExpiry);
                return new PortfolioAssetDisplay(a.PortfolioId,
                    a.AssetId,
                    a.TickerId,
                    a.Symbol,
                    a.TickerFullName,
                    a.TickerType,
                    a.TickerGroup,
                    a.TickerExpiry,
                    a.IsCall,
                    a.Strike,
                    a.IsLong,
                    a.Premium,
                    a.Size,
                    PositionServices.Instance.CalculateFairValue(a.IsCall, a.IsLong, a.Strike, a.Size, lastSpot),
                    a.Fee,
                    lastSpot,
                    lastSpotUpdate,
                    expirySpots.ContainsKey(spotExpiryKey)
                        ? expirySpots[spotExpiryKey] 
                        : (double?)null
                    );
            });
            MonitorUtils.BuildGrid<PortfolioAssetDisplay>(calculatedAssets, this.gdvAssets);
            SetSummarySection(calculatedAssets);
            AddContextMenu();
        }

        private void SetSummarySection(List<PortfolioAssetDisplay> calculatedAssets)
        {
            this.labelTotalAssets.Text = calculatedAssets.Count.ToString();
            this.labelTotalPremium.Text = calculatedAssets.Sum(t =>  t.TotalPremium).ToString("N0");
            this.labelFairValue.Text = calculatedAssets.Sum(t => t.FairValue).ToString("N0");
            
            var rowsList = this.gdvAssets.Rows.Cast<DataGridViewRow>().ToList();
            int distanceIndex = 7;
            var greens = rowsList.Count(row => row.Cells[distanceIndex].Style.BackColor == Color.LightGreen);
            var yellow = rowsList.Count(row => row.Cells[distanceIndex].Style.BackColor == Color.Yellow);
            var red = rowsList.Count(row => row.Cells[distanceIndex].Style.BackColor == Color.OrangeRed);
            this.labelGreen.Text = greens.ToString();
            this.labelYellow.Text = yellow.ToString();
            this.labelRed.Text = red.ToString();
            var totalSucc = calculatedAssets.Count(c => c.Success == "Success");
            this.labelTotalSuccess.Text = totalSucc.ToString();
            this.labelTotalSuccess.ForeColor = totalSucc > 0
                ? Color.Green : Color.Black;
            this.labelTotalSuccStr.ForeColor = totalSucc > 0
                ? Color.Green : Color.Black;

            var totalFailed = calculatedAssets.Count(c => c.Success == "Fail");
            this.labelTotalFailed.Text = totalFailed.ToString();
            this.labelTotalFailed.ForeColor = totalFailed > 0
                ? Color.OrangeRed : Color.Black;
            this.labelTotalFailedStr.ForeColor = totalFailed > 0
                ? Color.OrangeRed : Color.Black;

            Func<string, string, bool> IsLong = (longShort, callPut) =>
            {
                if ((longShort == "LONG" && callPut == "CALL") || (longShort == "SHORT" && callPut == "PUT"))
                    return true;
                return false;
            };
            var longCm = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "CM").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongCM.Text = longCm.ToString("N0");
            var longStock = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "STOCK").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongStock.Text = longStock.ToString("N0");
            var longCountry = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "COUNTRY").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongCountry.Text = longCountry.ToString("N0");
            var longIndex = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "INDEX").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongIndex.Text = longIndex.ToString("N0");
            var longBond = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "BOND").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongBond.Text = longBond.ToString("N0");
            var longFx = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "FX").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongFx.Text = longFx.ToString("N0");
            var longVix = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "VIX").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongVix.Text = longVix.ToString("N0");
            var longOthers = calculatedAssets.Where(c => IsLong(c.LongShort, c.CallPut) && c.Group == "OTHER").Sum(a => Double.Parse(a.TotalExpose));
            this.labelLongOthers.Text = longOthers.ToString("N0");

            var shortCm = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "CM").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortCM.Text = shortCm.ToString("N0");
            var shortStock = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "STOCK").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortStock.Text = shortStock.ToString("N0");
            var shortIndex = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "INDEX").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortIndex.Text = shortIndex.ToString("N0");
            var shortCountry = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "COUNTRY").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortCountry.Text = longCountry.ToString("N0");
            var shortBond = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "BOND").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortBond.Text = shortBond.ToString("N0");
            var shortFx = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "FX").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortFx.Text = shortFx.ToString("N0");
            var shortVix = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "VIX").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortVix.Text = shortVix.ToString("N0");
            var shortOthers = calculatedAssets.Where(c => !IsLong(c.LongShort, c.CallPut) && c.Group == "OTHER").Sum(a => Double.Parse(a.TotalExpose));
            this.labelShortOthers.Text = shortOthers.ToString("N0");

            var longTotal = longCm + longStock + longIndex + longCountry + longBond + longFx + longOthers + longVix;
            var shortTotal = shortCm + shortStock + shortIndex + shortCountry + shortBond + shortFx + shortOthers + shortVix;
            this.labelTotalLong.Text = longTotal.ToString("N0");
            this.labelTotalShort.Text = shortTotal.ToString("N0");
            this.labelTotalExpose.Text = (longTotal+ shortTotal).ToString("N0");
        }

        private Portfolio GetSelectedPortoflio()
        {
            var selectedExpiry = DateTime.Parse(this.comboBoxPortfolioExpiry.Text);
            var selectedPortfolio = portfolios.First(p => p.Expiry == selectedExpiry);
            return selectedPortfolio;
        }
        private void buttonAddRow_Click(object sender, EventArgs e)
        {
            var selectedPortfolio = GetSelectedPortoflio();
            var addAssetDialog = new AddAsset(selectedPortfolio, true);
            if (addAssetDialog.ShowDialog() == DialogResult.OK)
            {
                PositionServices.Instance.AddPortfolioAsset(addAssetDialog.Asset);
                RefreshPortfolio();
            }
        }
        private void buttonDeletePortfolio_Click(object sender, EventArgs e)
        {
            var yesNoDialog = MessageBox.Show($"You are about to delete portfolio with expiry {this.comboBoxPortfolioExpiry.Text}, " +
                $"are you sure you want to proceed?", $"Delete Portfolio {this.comboBoxPortfolioExpiry.Text}",
                MessageBoxButtons.YesNo);
            if (yesNoDialog == DialogResult.Yes)
            {
                var selectedPortfolio = GetSelectedPortoflio();
                if (selectedPortfolio != null)
                {
                    PositionPersistanceServices.Instance.DeletePortfolio(selectedPortfolio.PortfolioId);
                    InitPortfolios();
                }
            }
        }

        private void gdvAssets_CellMouseEnter(object sender, DataGridViewCellEventArgs location)
        {
            mouseLocation = location;
        }
        public void AddContextMenu()
        {
            contextDeleteAsset.Text = "Delete Row";
            contextDeleteAsset.Click -= new EventHandler(contextDeleteAsset_Click);
            contextDeleteAsset.Click += new EventHandler(contextDeleteAsset_Click);
            var rowContextMenu = new ContextMenuStrip();
            rowContextMenu.Opening -= RowContextMenu_Opening;
            rowContextMenu.Opening += RowContextMenu_Opening;
            foreach (DataGridViewRow row in this.gdvAssets.Rows)
                row.ContextMenuStrip = rowContextMenu;

            if (this.gdvAssets.Rows.Count > 1)
            {
                var initlocation = new DataGridViewCellEventArgs(1, 1);
                gdvAssets_CellMouseEnter(null, initlocation);
                RowContextMenu_Opening(null, null);
            }
        }
        private void RowContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedRow = this.gdvAssets.Rows[mouseLocation.RowIndex];
            if (selectedRow.IsNewRow)
                return;
            selectedRow.ContextMenuStrip.Items.Clear();
            selectedRow.ContextMenuStrip.Items.Add(contextDeleteAsset);
        }

        private void contextDeleteAsset_Click(object sender, EventArgs e)
        {
            var selectedRow = this.gdvAssets.Rows[mouseLocation.RowIndex];
            var selectedTicker = (PortfolioAssetDisplay)selectedRow.Tag;
            try
            {
                var yesNoDialog = MessageBox.Show($"You are about to delete asset {selectedTicker.Symbol}, are you sure you want to proceed?", $"Delete asset {selectedTicker.Symbol}", MessageBoxButtons.YesNo);
                if (yesNoDialog == DialogResult.Yes)
                {

                    PositionPersistanceServices.Instance.DeletePortfolioAsset(selectedTicker.AssetId);
                    MessageBox.Show($"Asset {selectedTicker.AssetId}:{selectedTicker.Symbol} has been removed successfully");
                    RefreshPortfolio();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Unexpected error occured trying to delete ticker {selectedTicker.Symbol} with Id {selectedTicker.TickerId}. {ex}");
            }
        }

        private void buttonApplyRange_Click(object sender, EventArgs e)
        {
            if (this.dateTimePickerFrom.Value.Date > this.dateTimePickerTo.Value.Date)
            {
                MessageBox.Show($"Please make sure that From date is greater than To date", $"Invalid Range", MessageBoxButtons.OK);
                return;
            }
            this.comboBoxPortfolioExpiry.DataSource = portfolios
                .Where(p => p.Expiry >= this.dateTimePickerFrom.Value.Date && p.Expiry <= this.dateTimePickerTo.Value.Date)
                .Select(p => p.Expiry).OrderBy(p => p).ToList();
        }
    }
}
