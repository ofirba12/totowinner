﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Monitor
{
    public class EntryPointHeatMapDisplay
    {
        public int MovingAverage { get; }
        public string BuySell { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int Trigger { get; }
        public int TotalPositions { get; }
        public int ProfitPositions { get; }
        public int LostPositions { get; }
        [ColumnStyle(ColorAlgoLogic = 7)]
        public int Success { get; }

        public EntryPointHeatMapDisplay(int movingAverage,
            bool isBuyPoint,
            int trigger,
            int totalPositions,
            int profitPositions,
            int lostPositions,
            int success)
        {
            MovingAverage = movingAverage;
            BuySell = isBuyPoint
                ? "BUY"
                : "SELL";
            Trigger = trigger;
            TotalPositions = totalPositions;
            ProfitPositions = profitPositions;
            LostPositions = lostPositions;
            Success = success;
        }
    }
}