﻿using System;

namespace BG.ExchangeTrading.Monitor
{
    public class TickerDataDisplay
    {
        [ColumnStyle(DateFormat = "dd/MM/yyyy")]
        public DateTime Date { get; }
        [ColumnStyle(DisplayName = "Change %", ColorAlgoLogic = 1)]
        public string ChangePercent { get; }
        public double OpenPrice { get; }
        [ColumnStyle(FontBold = true)]
        public double ClosePrice { get; }
        public double High { get; }
        public double Low { get; }
        public string Volume { get; }
        //[ColumnStyle(DisplayName = "Spread %", ColorAlgoLogic = 1)]
        //public string Spread { get; }
        [ColumnStyle(Width = 150)]
        public DateTime LastUpdate { get; }

        public TickerDataDisplay(DateTime date,
            double changePercent,
            double openPrice,
            double closePrice,
            double high,
            double low,
            long  volume,
            DateTime lastUpdate
            )
        {
            this.Date = date;
            this.ChangePercent = changePercent.ToString("F2");
            this.OpenPrice = openPrice;
            this.ClosePrice = closePrice;
            this.High = high;
            this.Low = low;
            this.Volume = volume.ToString("N0");
            //this.MA150 = ma150?.ToString("F2");
            //this.Spread = spread?.ToString("F2");
            this.LastUpdate = lastUpdate;
        }
    }
}
