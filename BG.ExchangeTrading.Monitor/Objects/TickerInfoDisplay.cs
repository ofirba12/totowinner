﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BG.ExchangeTrading.Monitor
{
    public class TickerInfoDisplay
    {
        [ColumnStyle(DisplayName = "BGTickerID", Width = 70)]
        public int TickerId { get; }
        [ColumnStyle(Width = 30)]
        public int IsIn { get; } //1
        [ColumnStyle(Width = 70, ColorAlgoLogic = 13)]
        public string Symbol { get; }
        [ColumnStyle(Freeze = true, Width = 250, ColorAlgoLogic = 8)]
        public string FullName { get; }
        [ColumnStyle(DisplayName = "LastPrice", FontBold = true, Width = 70)]
        public double? LastLiveSpot { get; } //3->4
        [ColumnStyle(DisplayName = "Change%", ColorAlgoLogic = 5, FontBold = true, Width = 70)]
        public string DailyChange { get; }
        [ColumnStyle(DisplayName = "BUY RATE", ColorAlgoLogic = 12, Width = 70)]
        public string BuyRating { get; } //5 ->6
        [ColumnStyle(DisplayName = "SELL RATE", ColorAlgoLogic = 12, Width = 70)]
        public string SellRating { get; } //6 ->7
        [ColumnStyle(ColorAlgoLogic = 4, Width = 70)]
        public double? Floor { get; } //7->8
        [ColumnStyle(ColorAlgoLogic = 2, Width = 70)]
        public double? Cap { get; } // 8->9
        [ColumnStyle(DateFormat = "dd/MM/yyyy HH:mm:ss", DisplayName = "Last Live Update", Width = 130)]
        public DateTime? LastLiveSpotTimeStamp { get; }
        [ColumnStyle(DisplayName = "AnalystRating", FontColor = "BLUE", Width = 75)]
        public double? StockRating { get; }

        [ColumnStyle(Width = 70)]
        public string Type { get; }
        [ColumnStyle(Width = 70)]
        public string Group { get; }
        [ColumnStyle(DisplayName = "EODSpot", FontBold = true, Width = 70)]
        public double? LastEndOfDaySpot { get; } //13->14

        [ColumnStyle(FontColor = "BLUE", Width = 70)]
        public double? Buffer { get; }
        [ColumnStyle(DisplayName = "MA50", Width = 70)]
        public string LastMA50 { get; } 
        [ColumnStyle(DisplayName = "MA75", Width = 70)]
        public string LastMA75 { get; }
        [ColumnStyle(DisplayName = "MA100", Width = 70)]
        public string LastMA100 { get; }
        [ColumnStyle(DisplayName = "MA125", Width = 70)]
        public string LastMA125 { get; }
        [ColumnStyle(DisplayName = "MA150", Width = 70)]
        public string LastMA150 { get; }
        [ColumnStyle(DisplayName = "MA175", Width = 70)]
        public string LastMA175 { get; }
        [ColumnStyle(DisplayName = "MA200", Width = 70)]
        public string LastMA200 { get; }
        [ColumnStyle(DateFormat = "dd/MM/yyyy", DisplayName = "Last Rating Update")]
        public DateTime? LastUpdateStockRating { get; }

        [ColumnStyle(DisplayName = "Spread MA50", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA50 { get; } //23->24
        [ColumnStyle(DisplayName = "Spread MA75", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA75 { get; } //24->25
        [ColumnStyle(DisplayName = "Spread MA100", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA100 { get; }//25->26
        [ColumnStyle(DisplayName = "Spread MA125", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA125 { get; }
        [ColumnStyle(DisplayName = "Spread MA150", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA150 { get; }
        [ColumnStyle(DisplayName = "Spread MA175", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA175 { get; }
        [ColumnStyle(DisplayName = "Spread MA200", ColorAlgoLogic = 3, Width = 70)]
        public string SpreadMA200 { get; }//29->30

        [ColumnStyle(Hide = true)]
        public bool CalcMA50IsBuy { get; }
        [ColumnStyle(DisplayName = "CALC MA50", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA50Rate { get; }
        [ColumnStyle(DisplayName = "MA50 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA50Positions { get; }
        [ColumnStyle(Hide = true)]
        public bool CalcMA75IsBuy { get; }
        [ColumnStyle(DisplayName = "CALC MA75", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA75Rate { get; }
        [ColumnStyle(DisplayName = "MA75 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA75Positions { get; }
        [ColumnStyle(Hide = true)]
        public bool CalcMA100IsBuy { get; }

        [ColumnStyle(DisplayName = "CALC MA100", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA100Rate { get; }
        [ColumnStyle(DisplayName = "MA100 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA100Positions { get; }
        [ColumnStyle(Hide = true)]
        public bool CalcMA125IsBuy { get; }

        [ColumnStyle(DisplayName = "CALC MA125", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA125Rate { get; }
        [ColumnStyle(DisplayName = "MA125 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA125Positions { get; }
        [ColumnStyle(Hide = true)]
        public bool CalcMA150IsBuy { get; }

        [ColumnStyle(DisplayName = "CALC MA150", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA150Rate { get; }
        [ColumnStyle(DisplayName = "MA150 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA150Positions { get; }
        [ColumnStyle(Hide = true)]
        public bool CalcMA175IsBuy { get; }

        [ColumnStyle(DisplayName = "CALC MA175", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA175Rate { get; }
        [ColumnStyle(DisplayName = "MA175 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA175Positions { get; }
        [ColumnStyle(Hide = true)]
        public bool CalcMA200IsBuy { get; }

        [ColumnStyle(DisplayName = "CALC MA200", ColorAlgoLogic = 11, Width = 70)]
        public string CalcMA200Rate { get; }
        [ColumnStyle(DisplayName = "MA200 POSITIONS", ColorAlgoLogic = 0, Width = 70)]
        public string CalcMA200Positions { get; }

        [ColumnStyle(DateFormat = "dd/MM/yyyy", DisplayName = "Last Spot Time")]
        public DateTime? LastEndOfDaySpotTimeStamp { get; }
        [ColumnStyle(Hide = true)]
        public string SummaryUrl { get; }

        private double? _ma50CapPercent;
        private double? _ma50FloorPercent;
        private double? _ma75CapPercent;
        private double? _ma75FloorPercent;
        private double? _ma100CapPercent;
        private double? _ma100FloorPercent;
        private double? _ma125CapPercent;
        private double? _ma125FloorPercent;
        private double? _ma150CapPercent;
        private double? _ma150FloorPercent;
        private double? _ma175CapPercent;
        private double? _ma175FloorPercent;
        private double? _ma200CapPercent;
        private double? _ma200FloorPercent;
        public TickerInfoDisplay(int tickerId,
            int isIn,
            string symbol,
            string fullName,
            string type,
            string group,
            string summaryUrl,
            double? lastEndOfDaySpot,
            DateTime? lastEndOfDaySpotTimeStamp,
            double? lastLiveSpot,
            DateTime? lastLiveSpotTimeStamp,
            double? buffer,
            double? lastMA50,
            double? lastMA75,
            double? lastMA100,
            double? lastMA125,
            double? lastMA150,
            double? lastMA175,
            double? lastMA200,
            double? stockRating,
            DateTime? lastUpdateStockRating,
            double? cap,
            double? floor,
            double? ma50CapPercent,
            double? ma50FloorPercent,
            double? ma75CapPercent,
            double? ma75FloorPercent,
            double? ma100CapPercent,
            double? ma100FloorPercent,
            double? ma125CapPercent,
            double? ma125FloorPercent,
            double? ma150CapPercent,
            double? ma150FloorPercent,
            double? ma175CapPercent,
            double? ma175FloorPercent,
            double? ma200CapPercent,
            double? ma200FloorPercent,
            Dictionary<CalculatedDataKey, Data.MovingAverageSuccessRate> calculatedData)
        {
            this.TickerId = tickerId;
            this.IsIn = isIn;
            this.Symbol = symbol;
            this.FullName = fullName;
            this.Type = type;
            this.Group = group;
            this.SummaryUrl = summaryUrl;
            this.LastEndOfDaySpot = lastEndOfDaySpot;
            this.LastEndOfDaySpotTimeStamp = lastEndOfDaySpotTimeStamp;
            this.LastLiveSpot = lastLiveSpot;
            this.LastLiveSpotTimeStamp = lastLiveSpotTimeStamp;
            var dailyChange = ((lastLiveSpot / lastEndOfDaySpot - 1) * 100);
            this.DailyChange = dailyChange?.ToString("F2");
            this.LastMA50 = lastMA50?.ToString("F2");
            this.LastMA75 = lastMA75?.ToString("F2");
            this.LastMA100 = lastMA100?.ToString("F2");
            this.LastMA125 = lastMA125?.ToString("F2");
            this.LastMA150 = lastMA150?.ToString("F2");
            this.LastMA175 = lastMA175?.ToString("F2");
            this.LastMA200 = lastMA200?.ToString("F2");
            this.StockRating = stockRating;
            this.LastUpdateStockRating = lastUpdateStockRating;
            this.Cap = cap;
            this.Floor = floor;
            this.Buffer = buffer;

            this.SpreadMA50 = ((lastEndOfDaySpot / lastMA50 - 1) * 100)?.ToString("F2");
            this.SpreadMA75 = ((lastEndOfDaySpot / lastMA75 - 1) * 100)?.ToString("F2");
            this.SpreadMA100 = ((lastEndOfDaySpot / lastMA100 - 1) * 100)?.ToString("F2");
            this.SpreadMA125 = ((lastEndOfDaySpot / lastMA125 - 1) * 100)?.ToString("F2");
            this.SpreadMA150 = ((lastEndOfDaySpot / lastMA150 - 1) * 100)?.ToString("F2");
            this.SpreadMA175 = ((lastEndOfDaySpot / lastMA175 - 1) * 100)?.ToString("F2");
            this.SpreadMA200 = ((lastEndOfDaySpot / lastMA200 - 1) * 100)?.ToString("F2");
            
            var key50 = new CalculatedDataKey(tickerId, 50);
            this.CalcMA50IsBuy = calculatedData.ContainsKey(key50) ? calculatedData[key50].IsBuy : false;
            this.CalcMA50Rate = calculatedData.ContainsKey(key50) ? calculatedData[key50].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA50Positions = calculatedData.ContainsKey(key50) ? calculatedData[key50].TotalPosition.ToString() : string.Empty;
            var key75 = new CalculatedDataKey(tickerId, 75);
            this.CalcMA75IsBuy = calculatedData.ContainsKey(key75) ? calculatedData[key75].IsBuy : false;
            this.CalcMA75Rate = calculatedData.ContainsKey(key75) ? calculatedData[key75].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA75Positions = calculatedData.ContainsKey(key75) ? calculatedData[key75].TotalPosition.ToString() : string.Empty;
            var key100 = new CalculatedDataKey(tickerId, 100);
            this.CalcMA100IsBuy = calculatedData.ContainsKey(key100) ? calculatedData[key100].IsBuy : false;
            this.CalcMA100Rate = calculatedData.ContainsKey(key100) ? calculatedData[key100].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA100Positions = calculatedData.ContainsKey(key100) ? calculatedData[key100].TotalPosition.ToString() : string.Empty;
            var key125 = new CalculatedDataKey(tickerId, 125);
            this.CalcMA125IsBuy = calculatedData.ContainsKey(key125) ? calculatedData[key125].IsBuy : false;
            this.CalcMA125Rate = calculatedData.ContainsKey(key125) ? calculatedData[key125].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA125Positions = calculatedData.ContainsKey(key125) ? calculatedData[key125].TotalPosition.ToString() : string.Empty;
            var key150 = new CalculatedDataKey(tickerId, 150);
            this.CalcMA150IsBuy = calculatedData.ContainsKey(key150) ? calculatedData[key150].IsBuy : false;
            this.CalcMA150Rate = calculatedData.ContainsKey(key150) ? calculatedData[key150].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA150Positions = calculatedData.ContainsKey(key150) ? calculatedData[key150].TotalPosition.ToString() : string.Empty;
            var key175 = new CalculatedDataKey(tickerId, 175);
            this.CalcMA175IsBuy = calculatedData.ContainsKey(key175) ? calculatedData[key175].IsBuy : false;
            this.CalcMA175Rate = calculatedData.ContainsKey(key175) ? calculatedData[key175].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA175Positions = calculatedData.ContainsKey(key175) ? calculatedData[key175].TotalPosition.ToString() : string.Empty;
            var key200 = new CalculatedDataKey(tickerId, 200);
            this.CalcMA200IsBuy = calculatedData.ContainsKey(key200) ? calculatedData[key200].IsBuy : false;
            this.CalcMA200Rate = calculatedData.ContainsKey(key200) ? calculatedData[key200].SuccessRate.ToString("F2") : string.Empty;
            this.CalcMA200Positions = calculatedData.ContainsKey(key200) ? calculatedData[key200].TotalPosition.ToString() : string.Empty;

            var buyRatingColl = calculatedData.Where(d => d.Value.TickerId == tickerId && d.Value.IsBuy).Where(r => r.Value.SuccessRate != 0);
            if (buyRatingColl.Count() > 0)
            {
                var buyRating = (double)buyRatingColl.Sum(r => r.Value.SuccessRate * r.Value.TotalPosition) / buyRatingColl.Sum(r => r.Value.TotalPosition);
                this.BuyRating = buyRating.ToString("F2");
            }
            else
                this.BuyRating = string.Empty;
            
            var sellRatingColl = calculatedData.Where(d => d.Value.TickerId == tickerId && !d.Value.IsBuy).Where(r => r.Value.SuccessRate != 0);
            if (sellRatingColl.Count() > 0)
            {
                var sellRating = (double)sellRatingColl.Sum(r => r.Value.SuccessRate * r.Value.TotalPosition) / sellRatingColl.Sum(r => r.Value.TotalPosition);
                this.SellRating = sellRating.ToString("F2");
            }
            else
                this.SellRating = string.Empty;

            _ma50CapPercent = ma50CapPercent;
            _ma50FloorPercent = ma50FloorPercent;
            _ma75CapPercent = ma75CapPercent;
            _ma75FloorPercent = ma75FloorPercent;
            _ma100CapPercent = ma100CapPercent;
            _ma100FloorPercent = ma100FloorPercent;
            _ma125CapPercent = ma125CapPercent;
            _ma125FloorPercent = ma125FloorPercent;
            _ma150CapPercent = ma150CapPercent;
            _ma150FloorPercent = ma150FloorPercent;
            _ma175CapPercent = ma175CapPercent;
            _ma175FloorPercent = ma175FloorPercent;
            _ma200CapPercent = ma200CapPercent;
            _ma200FloorPercent = ma200FloorPercent;

        }

        internal decimal? GetMaCapValue(int maSize)
        {
            switch (maSize)
            {
                case 50:
                    return (decimal?)_ma50CapPercent;
                case 75:
                    return (decimal?)_ma75CapPercent;
                case 100:
                    return (decimal?)_ma100CapPercent;
                case 125:
                    return (decimal?)_ma125CapPercent;
                case 150:
                    return (decimal?)_ma150CapPercent;
                case 175:
                    return (decimal?)_ma175CapPercent;
                case 200:
                    return (decimal?)_ma200CapPercent;
            }
            throw new Exception($"Cap size {maSize} not supported");
        }

        internal decimal? GetMaFloorValue(int maSize)
        {
            switch (maSize)
            {
                case 50:
                    return (decimal?)_ma50FloorPercent;
                case 75:
                    return (decimal?)_ma75FloorPercent;
                case 100:
                    return (decimal?)_ma100FloorPercent;
                case 125:
                    return (decimal?)_ma125FloorPercent;
                case 150:
                    return (decimal?)_ma150FloorPercent;
                case 175:
                    return (decimal?)_ma175FloorPercent;
                case 200:
                    return (decimal?)_ma200FloorPercent;
            }
            throw new Exception($"Floor size {maSize} not supported");
        }
    }
}
