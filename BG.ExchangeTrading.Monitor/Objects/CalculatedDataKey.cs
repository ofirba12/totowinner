﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Monitor
{
    public class CalculatedDataKey : IComparable<CalculatedDataKey>
    {
        public int TickerId { get; }
        public int MV { get; }

        public CalculatedDataKey(int TickerId, int MV)
        {
            this.TickerId = TickerId;
            this.MV = MV;
        }

        public int CompareTo(CalculatedDataKey other)
        {
            if (this.GetHashCode() == other.GetHashCode())
                return 0;
            if (this.GetHashCode() > other.GetHashCode())
                return -1; //because of desc mode
            return 1;
        }
        public override bool Equals(object obj)
        {
            var candidate = obj as CalculatedDataKey;
            return candidate != null
                ? this.TickerId == candidate.TickerId && this.MV == candidate.MV
                : false;
        }
        public override int GetHashCode()
        {
            return (this.TickerId + this.MV).GetHashCode();
        }
    }
}
