﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace BG.ExchangeTrading.Monitor
{
    internal static class MonitorUtils
    {
        #region BuildGrid
        internal static void BuildGrid<T>(List<T> monitorData, DataGridView dgv)
        {
            Type type = typeof(T);
            var columns = type.GetMembers().Where(m => m.MemberType == MemberTypes.Property);
            var columnIndex = 0;
            var zebraCells = new List<int>();
            var colorAlgoLogic_1Cells = new List<int>();
            var colorAlgoLogic_2Cells = new List<int>();
            var colorAlgoLogic_3Cells = new List<int>();
            var colorAlgoLogic_4Cells = new List<int>();
            var colorAlgoLogic_5Cells = new List<int>();
            var colorAlgoLogic_6Cells = new List<int>();
            var colorAlgoLogic_7Cells = new List<int>();
            var colorAlgoLogic_8Cells = new List<int>();
            var colorAlgoLogic_9Cells = new List<int>();
            var colorAlgoLogic_10Cells = new List<int>();
            var colorAlgoLogic_11Cells = new List<int>();
            var colorAlgoLogic_12Cells = new List<int>();
            var colorAlgoLogic_13Cells = new List<int>();
            dgv.EnableHeadersVisualStyles = false;
            CultureInfo culture = new CultureInfo("en-US");
            culture.NumberFormat.NumberGroupSeparator = ",";
            foreach (var column in columns)
            {
                dgv.Columns.Add(column.Name, column.Name);
                var style = column.GetCustomAttribute<ColumnStyleAttribute>();
                if (style != null)
                {
                    dgv.Columns[column.Name].Visible = !style.Hide;
                    if (style.Width > 0)
                        dgv.Columns[column.Name].Width = style.Width;
                    if (!string.IsNullOrEmpty(style.DisplayName))
                    {
                        dgv.Columns[column.Name].HeaderText = style.DisplayName;
                        //dgv.Columns[column.Name].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    }
                    if (!string.IsNullOrEmpty(style.DateFormat))
                    {
                        dgv.Columns[column.Name].DefaultCellStyle.Format = style.DateFormat;
                    }
                    if (style.FontBold)
                    {
                        dgv.Columns[column.Name].DefaultCellStyle.Font = new Font(dgv.Font, FontStyle.Bold);
                    }
                    if (style.FontColor == "BLUE")
                    {
                        dgv.Columns[column.Name].DefaultCellStyle.ForeColor = Color.Blue;
                    }
                    if (style.DisableSort)
                        dgv.Columns[column.Name].SortMode = DataGridViewColumnSortMode.NotSortable;
                    if (style.ZebraOn)
                        zebraCells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 1)
                        colorAlgoLogic_1Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 2)
                        colorAlgoLogic_2Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 3)
                        colorAlgoLogic_3Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 4)
                        colorAlgoLogic_4Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 5)
                        colorAlgoLogic_5Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 6)
                        colorAlgoLogic_6Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 7)
                        colorAlgoLogic_7Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 8)
                        colorAlgoLogic_8Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 9)
                        colorAlgoLogic_9Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 10)
                        colorAlgoLogic_10Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 11)
                        colorAlgoLogic_11Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 12)
                        colorAlgoLogic_12Cells.Add(columnIndex);
                    if (style.ColorAlgoLogic == 13)
                        colorAlgoLogic_13Cells.Add(columnIndex);
                    if (style.HeaderColor == 1)
                        dgv.Columns[column.Name].HeaderCell.Style.BackColor = Color.GreenYellow;
                    if (style.Freeze)
                        dgv.Columns[column.Name].Frozen = true;
                }
                dgv.Columns[column.Name].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgv.Columns[column.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columnIndex++;
            }
            foreach (var match in monitorData)
            {
                var row = CreateGridRow<T>(match, type, dgv,
                    zebraCells,
                    colorAlgoLogic_1Cells,
                    colorAlgoLogic_2Cells,
                    colorAlgoLogic_3Cells,
                    colorAlgoLogic_4Cells,
                    colorAlgoLogic_5Cells,
                    colorAlgoLogic_6Cells,
                    colorAlgoLogic_7Cells,
                    colorAlgoLogic_8Cells,
                    colorAlgoLogic_9Cells,
                    colorAlgoLogic_10Cells,
                    colorAlgoLogic_11Cells,
                    colorAlgoLogic_12Cells,
                    colorAlgoLogic_13Cells);
                dgv.Rows.Add(row);
            }
        }
        #endregion

        #region CreateGridRow
        internal static DataGridViewRow CreateGridRow<T>(T match,
            Type type,
            DataGridView dgv,
            List<int> zebraCells,
            List<int> colorAlgoLogic_1Cells,
            List<int> colorAlgoLogic_2Cells,
            List<int> colorAlgoLogic_3Cells,
            List<int> colorAlgoLogic_4Cells,
            List<int> colorAlgoLogic_5Cells,
            List<int> colorAlgoLogic_6Cells,
            List<int> colorAlgoLogic_7Cells,
            List<int> colorAlgoLogic_8Cells,
            List<int> colorAlgoLogic_9Cells,
            List<int> colorAlgoLogic_10Cells,
            List<int> colorAlgoLogic_11Cells,
            List<int> colorAlgoLogic_12Cells,
            List<int> colorAlgoLogic_13Cells)
        {
            DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
            var cellIndex = 0;
            var ignore = false;
            var rankItem = 0;
            for (var colIndex = 0; colIndex < dgv.Columns.Count; colIndex++)
            {
                var property = dgv.Columns[colIndex].Name;
                var methodInfo = type.GetMethod($"get_{property}");
                var result = methodInfo.Invoke(match, null);
                row.Cells[cellIndex].Value = result;
                if (property.ToLower() == "ignore")
                    ignore = (bool)result;
                cellIndex++;
            }
            row.Tag = match;
            StyleGridRow(dgv, row, ignore, rankItem, zebraCells,
                colorAlgoLogic_1Cells,
                colorAlgoLogic_2Cells,
                colorAlgoLogic_3Cells,
                colorAlgoLogic_4Cells,
                colorAlgoLogic_5Cells,
                colorAlgoLogic_6Cells,
                colorAlgoLogic_7Cells,
                colorAlgoLogic_8Cells,
                colorAlgoLogic_9Cells,
                colorAlgoLogic_10Cells,
                colorAlgoLogic_11Cells,
                colorAlgoLogic_12Cells,
                colorAlgoLogic_13Cells);
            return row;
        }
        #endregion

        #region StyleGridRow
        private static void StyleGridRow(DataGridView dgv,
            DataGridViewRow row,
            bool ignore,
            int rankItem,
            List<int> zebraCells,
            List<int> colorAlgoLogic_1Cells,
            List<int> colorAlgoLogic_2Cells,
            List<int> colorAlgoLogic_3Cells,
            List<int> colorAlgoLogic_4Cells,
            List<int> colorAlgoLogic_5Cells,
            List<int> colorAlgoLogic_6Cells,
            List<int> colorAlgoLogic_7Cells,
            List<int> colorAlgoLogic_8Cells,
            List<int> colorAlgoLogic_9Cells,
            List<int> colorAlgoLogic_10Cells,
            List<int> colorAlgoLogic_11Cells,
            List<int> colorAlgoLogic_12Cells,
            List<int> colorAlgoLogic_13Cells)
        {
            row.DefaultCellStyle.BackColor = ignore
                ? Color.LightGray
                : Color.White;
            //var eodSpotIndex = 13;
            foreach (var cellIndex in colorAlgoLogic_7Cells)
            {
                var success = Convert.ToInt16(row.Cells[cellIndex].Value);
                if (success >= 87)
                    row.DefaultCellStyle.Font = new Font(dgv.Font, FontStyle.Bold);
                else
                    row.DefaultCellStyle.Font = new Font(dgv.Font, FontStyle.Regular);
            }
            foreach (var cellIndex in zebraCells)
                row.Cells[cellIndex].Style.BackColor = Color.LightGray;
            var IsInIndex = 1;
            foreach (var cellIndex in colorAlgoLogic_13Cells)
            {
                var isIn = Convert.ToInt16(row.Cells[IsInIndex].Value);
                if (isIn > 0)
                    row.Cells[cellIndex].Style.Font = new Font(dgv.Font, FontStyle.Bold);
                else
                    row.Cells[cellIndex].Style.Font = new Font(dgv.Font, FontStyle.Regular);
            }
            foreach (var cellIndex in colorAlgoLogic_1Cells)
            {
                var cellVal = Convert.ToDouble(row.Cells[cellIndex].Value);
                if (cellVal > 0)
                {
                    row.Cells[cellIndex].Style.ForeColor = Color.Green;
                    row.Cells[cellIndex - 1].Style.ForeColor = Color.Green;
                }
                else if (cellVal < 0)
                {
                    row.Cells[cellIndex].Style.ForeColor = Color.OrangeRed;
                    row.Cells[cellIndex - 1].Style.ForeColor = Color.OrangeRed;
                }
                else
                {
                    row.Cells[cellIndex].Style.ForeColor = Color.Black;
                    row.Cells[cellIndex - 1].Style.ForeColor = Color.Black;
                }
            }
            var lastPriceIndex = 4;
            foreach (var cellIndex in colorAlgoLogic_2Cells)
            {
                row.Cells[cellIndex].Style.BackColor = Color.White;
                if (row.Cells[cellIndex].Value != null && Decimal.TryParse(row.Cells[cellIndex].Value.ToString(), out var capCellValue) &&
                    row.Cells[lastPriceIndex].Value != null && Decimal.TryParse(row.Cells[lastPriceIndex].Value.ToString(), out var lastPriceCellValue))
                {
                    if (lastPriceCellValue >= capCellValue)
                        row.Cells[cellIndex].Style.BackColor = Color.PaleVioletRed;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_3Cells)
            {
                if (row.Cells[cellIndex].Value != null && Decimal.TryParse(row.Cells[cellIndex].Value.ToString(), out var cellValue))
                {
                    if (cellValue > 0)
                        row.Cells[cellIndex].Style.ForeColor = Color.Green;
                    else
                        row.Cells[cellIndex].Style.ForeColor = Color.OrangeRed;

                    if (row.Tag is TickerInfoDisplay)
                    {
                        var rowObj = (TickerInfoDisplay)row.Tag;
                        int maSize = 50;
                        switch (cellIndex)
                        {
                            case 24:
                                maSize = 50;
                                break;
                            case 25:
                                maSize = 75;
                                break;
                            case 26:
                                maSize = 100;
                                break;
                            case 27:
                                maSize = 125;
                                break;
                            case 28:
                                maSize = 150;
                                break;
                            case 29:
                                maSize = 175;
                                break;
                            case 30:
                                maSize = 200;
                                break;
                        }
                        var cap = rowObj.GetMaCapValue(maSize);
                        var floor = rowObj.GetMaFloorValue(maSize);
                        if (cap.HasValue && cellValue > cap.Value)
                            row.Cells[cellIndex].Style.BackColor = Color.LightPink;
                        else if (floor.HasValue && cellValue < floor.Value)
                            row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                        else
                            row.Cells[cellIndex].Style.BackColor = Color.White;
                    }
                }
            }
            foreach (var cellIndex in colorAlgoLogic_4Cells)
            {
                row.Cells[cellIndex].Style.BackColor = Color.White;
                if (row.Cells[cellIndex].Value != null && Decimal.TryParse(row.Cells[cellIndex].Value.ToString(), out var floorCellValue) &&
                    row.Cells[lastPriceIndex].Value != null && Decimal.TryParse(row.Cells[lastPriceIndex].Value.ToString(), out var lastPriceCellValue))
                {
                    if (lastPriceCellValue <= floorCellValue)
                        row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_5Cells)
            {
                var cellVal = Convert.ToDouble(row.Cells[cellIndex].Value);
                if (cellVal > 0)
                {
                    row.Cells[cellIndex].Style.ForeColor = Color.Green;
                }
                else if (cellVal < 0)
                {
                    row.Cells[cellIndex].Style.ForeColor = Color.OrangeRed;
                }
                else
                {
                    row.Cells[cellIndex].Style.ForeColor = Color.Black;
                }
            }

            foreach (var cellIndex in colorAlgoLogic_6Cells)
            {
                row.Cells[cellIndex].Style.BackColor = Color.White;
                if (row.Cells[cellIndex].Value != null && Boolean.TryParse(row.Cells[cellIndex].Value.ToString(), out var boolCellValue))
                {
                    if (boolCellValue)
                        row.Cells[cellIndex].Style.ForeColor = Color.Green;
                    else
                        row.Cells[cellIndex].Style.ForeColor = Color.OrangeRed;
                }
            }

            foreach (var cellIndex in colorAlgoLogic_9Cells)
            {
                row.Cells[cellIndex].Style.BackColor = Color.White;
                if (row.Cells[cellIndex].Value != null)
                {
                    if (row.Cells[cellIndex].Value.ToString() == "Success")
                        row.Cells[cellIndex].Style.ForeColor = Color.Green;
                    else if (row.Cells[cellIndex].Value.ToString() == "Fail")
                        row.Cells[cellIndex].Style.ForeColor = Color.OrangeRed;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_10Cells)
            {
                var rowObj = (PortfolioAssetDisplay)row.Tag;
                row.Cells[cellIndex].Style.BackColor = Color.White;
                if (row.Cells[cellIndex].Value != null && !string.IsNullOrEmpty(rowObj.Distance))
                {
                    var distance = double.Parse(rowObj.Distance);
                    if (rowObj.CallPut == "CALL" && rowObj.LongShort == "LONG")
                    {
                        if (distance > 5)
                            row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                        else if (distance > 0 && distance <= 5)
                            row.Cells[cellIndex].Style.BackColor = Color.Yellow;
                        else if (distance <= 0)
                            row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                    }
                    else if (rowObj.CallPut == "CALL" && rowObj.LongShort == "SHORT")
                    {
                        if (distance < -5)
                            row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                        else if (distance > -5 && distance <= 0)
                            row.Cells[cellIndex].Style.BackColor = Color.Yellow;
                        else if (distance > 0)
                            row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                    }
                    else if (rowObj.CallPut == "PUT" && rowObj.LongShort == "LONG")
                    {
                        if (distance < -5)
                            row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                        else if (distance > -5 && distance < 0)
                            row.Cells[cellIndex].Style.BackColor = Color.Yellow;
                        else if (distance >= 0)
                            row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                    }
                    else if (rowObj.CallPut == "PUT" && rowObj.LongShort == "SHORT")
                    {
                        if (distance > 5)
                            row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                        else if (distance > 0 && distance <= 5)
                            row.Cells[cellIndex].Style.BackColor = Color.Yellow;
                        else if (distance <= 0)
                            row.Cells[cellIndex].Style.BackColor = Color.OrangeRed;
                    }
                }
            }
            foreach (var cellIndex in colorAlgoLogic_11Cells)
            {
                row.Cells[cellIndex].Style.ForeColor = Color.Black;
                var rowObj = (TickerInfoDisplay)row.Tag;
                var val = row.Cells[cellIndex].Value != null && !string.IsNullOrEmpty(row.Cells[cellIndex].Value.ToString())
                    ? double.Parse(row.Cells[cellIndex].Value.ToString())
                    : 0;
                if (val != 0)
                {
                    var isBuy = Boolean.Parse(row.Cells[cellIndex - 1].Value.ToString());
                    if (isBuy)
                        row.Cells[cellIndex].Style.ForeColor = Color.OrangeRed;
                    else
                        row.Cells[cellIndex].Style.ForeColor = Color.Green;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_12Cells)
            {
                row.Cells[cellIndex].Style.BackColor = Color.White;
                var rowObj = (TickerInfoDisplay)row.Tag;
                var isBuyRate = cellIndex == 6;
                var isSellRate = cellIndex == 7;
                var val = row.Cells[cellIndex].Value != null && !string.IsNullOrEmpty(row.Cells[cellIndex].Value.ToString())
                    ? double.Parse(row.Cells[cellIndex].Value.ToString())
                    : 0;
                if (val >= 85)
                {
                    if (isBuyRate)
                        row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                    else if (isSellRate)
                        row.Cells[cellIndex].Style.BackColor = Color.PaleVioletRed;
                }
            }
            foreach (var cellIndex in colorAlgoLogic_8Cells) //MUST BE LAST - FullName
            {
                var buyRateIndex = 6;
                var sellRateIndex = 7;
                var capIndex = 9;
                var floorIndex = 8;
                if (row.Cells[cellIndex].Value != null && row.Tag is TickerInfoDisplay)
                {
                    var rowObj = (TickerInfoDisplay)row.Tag;
                    var buyRateColor = row.Cells[buyRateIndex].Style.BackColor;
                    var sellRateColor = row.Cells[sellRateIndex].Style.BackColor;
                    var capColor = row.Cells[capIndex].Style.BackColor;
                    var floorColor = row.Cells[floorIndex].Style.BackColor;
                    var columnsColors = new List<Color>() { buyRateColor, sellRateColor, capColor, floorColor };
                    row.Cells[cellIndex].Style.BackColor = Color.White;
                    if (columnsColors.Count(c => c == Color.LightGreen) > 1)
                        row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                    else if (columnsColors.Count(c => c == Color.PaleVioletRed) > 1)
                        row.Cells[cellIndex].Style.BackColor = Color.PaleVioletRed;

                    //if (columnsColors.Any(c => c == Color.LightGreen) && columnsColors.All(c => c != Color.PaleVioletRed))
                    //    row.Cells[cellIndex].Style.BackColor = Color.LightGreen;
                    //else if (columnsColors.Any(c => c == Color.PaleVioletRed) && columnsColors.All(c => c != Color.LightGreen))
                    //    row.Cells[cellIndex].Style.BackColor = Color.PaleVioletRed;
                    //else if (columnsColors.Any(c => c == Color.PaleVioletRed) && columnsColors.Any(c => c == Color.LightGreen))
                    //    row.Cells[cellIndex].Style.BackColor = Color.Yellow;
                }
            }

        }
        #endregion
    }
}
