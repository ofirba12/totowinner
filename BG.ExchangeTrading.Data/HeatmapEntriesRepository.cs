﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class HeatmapEntriesRepository
    {
        public DateTime EntryDate { get; }
        public double EntrySpread { get; }
        public double EntryPrice { get; }
        public double BufferPrice { get; }
        public DateTime ExitDate { get; }
        public double ExitPrice { get; }
        public bool Win { get; }

        public HeatmapEntriesRepository(DateTime entryDate,
            double entrySpread,
            double entryPrice,
            double bufferPrice,
            DateTime exitDate,
            double exitPrice,
            bool win)
        {
            EntryDate = entryDate;
            EntrySpread = entrySpread; 
            EntryPrice = entryPrice;
            BufferPrice = bufferPrice;
            ExitDate = exitDate;
            ExitPrice = exitPrice;
            Win = win;
        }
    }
}
