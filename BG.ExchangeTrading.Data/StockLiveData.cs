﻿using System;

namespace BG.ExchangeTrading.Data
{
    public class StockLiveData
    {
        public int TickerId { get; }
        public double Spot { get; }
        public DateTime LastUpdate { get; }

        public StockLiveData(int tickerId,
            double spot,
            DateTime lastUpdate
           )
        {
            TickerId = tickerId;
            Spot = spot;
            LastUpdate = lastUpdate;
        }
    }
}
