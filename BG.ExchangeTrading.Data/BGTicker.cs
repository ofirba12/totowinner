﻿using System;

namespace BG.ExchangeTrading.Data
{
    public class BGTicker
    {
        public int TickerId { get; }
        public string Symbol { get; }
        public string FullName { get; }
        public string Type { get; set; }
        public string Group { get; set; }
        public Nullable<double> StockRating { get;  }
        public Nullable<DateTime> LastUpdateStockRating { get;  }
        public Nullable<double> Cap { get;  }
        public Nullable<double> Floor { get;  }
        public string Url { get; }


        public BGTicker(int tickerId,
            string symbol,
            string fullName,
            string type,
            string group,
            double? stockRating,
            DateTime? lastUpdateStockRating,
            double? cap,
            double? floor,
            string url
            )
        {
            this.TickerId = tickerId;
            this.Symbol = symbol;
            this.FullName = fullName;
            this.Type = type;
            this.Group = group;
            this.StockRating = stockRating;
            this.LastUpdateStockRating = lastUpdateStockRating;
            this.Cap = cap;
            this.Floor = floor;
            this.Url = url;
        }
    }
}

