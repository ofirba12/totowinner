﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class PortfolioAsset
    {
        public int PortfolioId { get; }
        public int AssetId { get; }
        public int TickerId { get; }
        public string Symbol { get; }
        public string TickerFullName { get; }
        public string TickerType { get; }
        public string TickerGroup { get; }
        public DateTime TickerExpiry { get; }
        public bool IsCall { get; }
        public double Strike { get; }
        public bool IsLong { get; }
        public double Premium { get; }
        public int Size { get; }
        public double Fee { get; }

        public PortfolioAsset(int portfolioId,
            int assetId,
            int tickerId,
            string symbol,
            string fullName,
            string type,
            string group,
            DateTime tickerExpiry,
            bool isCall,
            double strike,
            bool isLong,
            double premium,
            int size,
            double fee
            )
        {
            this.PortfolioId = portfolioId;
            this.AssetId = assetId;
            this.TickerId = tickerId;
            this.Symbol = symbol;
            this.TickerFullName = fullName;
            this.TickerType = type;
            this.TickerGroup = group;
            this.TickerExpiry = tickerExpiry;
            this.IsLong = isLong;
            this.Strike = strike;
            this.IsCall = isCall;
            this.Premium = premium;
            this.Size = size;
            this.Fee = fee;
        }
        public override string ToString()
        {
            var str = $"PortfolioId={PortfolioId} | TickerId={TickerId} | TickerExpiry={TickerExpiry} | IsLong={IsLong} | Strike={Strike} | IsCall={IsCall} | Premium={Premium} | Size={Size} | Fee={Fee}";
            return str;
        }
    }
}
