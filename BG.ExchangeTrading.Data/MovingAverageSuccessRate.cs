﻿namespace BG.ExchangeTrading.Data
{
    public class MovingAverageSuccessRate
    {
        public int TickerId { get; }
        public int Mv { get; }
        public bool IsBuy { get; }
        public double SuccessRate { get; }
        public int TotalPosition { get; }

        public MovingAverageSuccessRate(int tickerId, int mv, bool isBuy, double successRate, int totalPosition)
        {
            this.TickerId = tickerId;
            this.Mv = mv;
            this.IsBuy = isBuy;
            this.SuccessRate = successRate;
            this.TotalPosition = totalPosition;
        }
    }
}