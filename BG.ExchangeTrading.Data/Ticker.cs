﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class Ticker
    {
        public string Symbol { get; }
        public string FullName { get; }
        public string Exchange { get; }
        public string Type { get; }
        public string Index { get; }
        public double? MarketCap { get; }
        public string Options { get; }

        public Ticker(string symbol,
            string fullName,
            string exchange,
            string type,
            string index,
            double? marketCap,
            string options)
        {
            this.Symbol = symbol;
            this.FullName = fullName;
            this.Exchange = exchange;
            this.Type = type;
            this.Index = index;
            this.MarketCap = marketCap;
            this.Options = options;
        }
    }
}
