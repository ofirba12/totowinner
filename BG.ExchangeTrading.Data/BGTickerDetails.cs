﻿using System;

namespace BG.ExchangeTrading.Data
{
    public class BGTickerDetails : BGTicker
    {
        public Nullable<double> LastLiveSpot { get; }
        public Nullable<DateTime> LastLiveSpotTimeStamp { get; }
        public Nullable<double> LastEndOfDaySpot { get; }
        public Nullable<DateTime> LastEndOfDaySpotTimeStamp { get; }
        public Nullable<double> Buffer { get; }
        public Nullable<double> MA50CapPercent { get; }
        public Nullable<double> MA50FloorPercent { get; }
        public Nullable<double> LastMA50 { get; }
        public Nullable<double> MA75CapPercent { get; }
        public Nullable<double> MA75FloorPercent { get; }
        public Nullable<double> LastMA75 { get; }
        public Nullable<double> MA100CapPercent { get; }
        public Nullable<double> MA100FloorPercent { get; }
        public Nullable<double> LastMA100 { get; }
        public Nullable<double> MA125CapPercent { get; }
        public Nullable<double> MA125FloorPercent { get; }
        public Nullable<double> LastMA125 { get; }
        public Nullable<double> MA150CapPercent { get; }
        public Nullable<double> MA150FloorPercent { get; }
        public Nullable<double> LastMA150 { get; }
        public Nullable<double> MA175CapPercent { get; }
        public Nullable<double> MA175FloorPercent { get; }
        public Nullable<double> LastMA175 { get; }
        public Nullable<double> MA200CapPercent { get; }
        public Nullable<double> MA200FloorPercent { get; }
        public Nullable<double> LastMA200 { get; }
        public BGTickerDetails(int tickerId,
            string symbol,
            string fullName,
            string type,
            string group,
            double? stockRating,
            DateTime? lastUpdateStockRating,
            double? cap,
            double? floor,
            double? lastLiveSpot,
            DateTime? lastLiveSpotTS,
            double? lastEndOfDaySpot,
            DateTime? lastEndOfDaySpotTS,
            double? buffer,
            double? ma50CapPercent,
            double? ma50FloorPercent,
            double? lastMA50,
            double? ma75CapPercent,
            double? ma75FloorPercent,
            double? lastMA75,
            double? ma100CapPercent,
            double? ma100FloorPercent,
            double? lastMA100,
            double? ma125CapPercent,
            double? ma125FloorPercent,
            double? lastMA125,
            double? ma150CapPercent,
            double? ma150FloorPercent,
            double? lastMA150,
            double? ma175CapPercent,
            double? ma175FloorPercent,
            double? lastMA175,
            double? ma200CapPercent,
            double? ma200FloorPercent,
            double? lastMA200,
            string url
            ) : base(tickerId, symbol, fullName, type, group, stockRating, lastUpdateStockRating, cap, floor, url)
        {
            this.LastLiveSpot = lastLiveSpot;
            this.LastLiveSpotTimeStamp = lastLiveSpotTS;
            this.LastEndOfDaySpot = lastEndOfDaySpot;
            this.LastEndOfDaySpotTimeStamp = lastEndOfDaySpotTS;
            this.Buffer = buffer;
            this.LastMA50 = lastMA50;
            this.MA50CapPercent = ma50CapPercent;
            this.MA50FloorPercent = ma50FloorPercent;
            this.LastMA75 = lastMA75;
            this.MA75CapPercent = ma75CapPercent;
            this.MA75FloorPercent = ma75FloorPercent;
            this.LastMA100 = lastMA100;
            this.MA100CapPercent = ma100CapPercent;
            this.MA100FloorPercent = ma100FloorPercent;
            this.LastMA125 = lastMA125;
            this.MA125CapPercent = ma125CapPercent;
            this.MA125FloorPercent = ma125FloorPercent;
            this.LastMA150 = lastMA150;
            this.MA150CapPercent = ma150CapPercent;
            this.MA150FloorPercent = ma150FloorPercent;
            this.LastMA175 = lastMA175;
            this.MA175CapPercent = ma175CapPercent;
            this.MA175FloorPercent = ma175FloorPercent;
            this.LastMA200 = lastMA200;
            this.MA200CapPercent = ma200CapPercent;
            this.MA200FloorPercent = ma200FloorPercent;
        }
    }
}
