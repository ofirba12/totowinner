﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class Portfolio
    {
        public int PortfolioId { get; }
        public DateTime Expiry { get; }
        public Portfolio(int portfolioId, DateTime expiry)
        {
            this.PortfolioId = portfolioId;
            this.Expiry = expiry;
        }
    }
}
