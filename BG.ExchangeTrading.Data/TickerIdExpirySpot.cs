﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class TickerIdExpirySpot : TickerIdExpiry
    {
        public double ClosePrice { get; }

        public TickerIdExpirySpot(int tickerId, DateTime expiry, double spot) : base(tickerId, expiry)
        {
            this.ClosePrice = spot;
        }
    }
}
