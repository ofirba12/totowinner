﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class HeatMapRepository
    {
        public List<EntryPointHeatMap> Summary { get; }
        public Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>> BuyTriggers { get; }
        public Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>> SellTriggers { get; }

        public HeatMapRepository(List<EntryPointHeatMap> summary,
            Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>> buyTriggers,
            Dictionary<int, Dictionary<int, List<HeatmapEntriesRepository>>> sellTriggers)
        {
            Summary = summary;
            BuyTriggers = buyTriggers;
            SellTriggers = sellTriggers;
        }
    }
}
