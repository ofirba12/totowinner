﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class StockEodData
    {
        public DateTime Date { get; }
        public double OpenPrice { get; }
        public double ClosePrice { get; }
        public double ChangePercent { get; }
        public double High { get; }
        public double Low { get; }
        public long Volume { get; }
        public MovingAverageItem MA50 { get; }
        public MovingAverageItem MA75 { get; }
        public MovingAverageItem MA100 { get; }
        public MovingAverageItem MA125 { get; }
        public MovingAverageItem MA150 { get; }
        public MovingAverageItem MA175 { get; }
        public MovingAverageItem MA200 { get; }

        public StockEodData(DateTime date,
            double openPrice,
            double closePrice,
            double changePercent,
            double high,
            double low,
            long volume,
            MovingAverageItem ma50,
            MovingAverageItem ma75,
            MovingAverageItem ma100,
            MovingAverageItem ma125,
            MovingAverageItem ma150,
            MovingAverageItem ma175,
            MovingAverageItem ma200
           )
        {
            this.Date = date;
            this.OpenPrice = openPrice;
            this.ClosePrice = closePrice;
            this.ChangePercent = changePercent;
            this.High = high;
            this.Low = low;
            this.Volume = volume;
            this.MA50 = ma50;
            this.MA75 = ma75;
            this.MA100 = ma100;
            this.MA125 = ma125;
            this.MA150 = ma150;
            this.MA175 = ma175;
            this.MA200 = ma200;
            //this.MA150 = ma150;
            //this.Spread = ma150 != 0 
            //    ? (closePrice / ma150 - 1) *100
            //    : (double?)null;
        }
    }
}
