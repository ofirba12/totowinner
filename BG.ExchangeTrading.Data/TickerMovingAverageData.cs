﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class TickerMovingAverageData
    {
        public DateTime Date { get; }
        public double ClosePrice { get; }
        public List<MovingAverageData> MovingAverageItems { get; }

        public TickerMovingAverageData(DateTime date, double closePrice, List<MovingAverageData> movingAverageItems)
        {
            this.Date = date;
            this.ClosePrice = closePrice;
            this.MovingAverageItems = movingAverageItems;
        }
    }
}
