﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class TickerIdExpiry
    {
        public int TickerId { get; }
        public DateTime Expiry { get; }

        public TickerIdExpiry(int tickerId, DateTime expiry)
        {
            this.TickerId = tickerId;
            this.Expiry = expiry;
        }
        // Override GetHashCode and Equals for custom comparison
        public override int GetHashCode()
        {
            // Combine hash codes of TickerId and Expiry
            return TickerId.GetHashCode() ^ Expiry.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            TickerIdExpiry other = (TickerIdExpiry)obj;
            return TickerId == other.TickerId && Expiry == other.Expiry;
        }
    }
}
