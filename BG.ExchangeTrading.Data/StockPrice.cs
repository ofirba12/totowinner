﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class StockPrice 
    {
        public DateTime? Date { get; }
        public double? OpenPrice { get; }
        public double? ClosePrice { get; }
        public double? High { get; }
        public double? Low { get; }
        public long? Volume { get; }

        public StockPrice(DateTime? date,
            double? openPrice,
            double? closePrice,
            double? high,
            double? low,
            long? volume
            )
        {
            this.Date = date;
            this.OpenPrice = openPrice;
            this.ClosePrice = closePrice;
            this.High = high;
            this.Low = low;
            this.Volume = volume;
        }
    }
}
