﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class EntryPointHeatMap
    {
        public int MovingAverage { get; }
        public bool IsBuyPoint { get; }
        public int Trigger { get; }
        public int TotalPositions { get; }
        public int ProfitPositions { get; }
        public int LostPositions { get; }
        public int Success { get; }

        public EntryPointHeatMap(int movingAverage,
            bool isBuyPoint,
            int trigger,
            int totalPositions,
            int profitPositions,
            int lostPositions)
        {
            MovingAverage = movingAverage;
            IsBuyPoint = isBuyPoint;
            Trigger = trigger;
            TotalPositions = totalPositions;
            ProfitPositions = profitPositions;
            LostPositions = lostPositions;
            Success = totalPositions > 0
                ? (int)((double)profitPositions / totalPositions * 100)
                : 0;
        }
    }
}
