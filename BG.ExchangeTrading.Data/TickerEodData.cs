﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class TickerEodData
    {
        public int TickerId { get; }
        public DateTime Date { get; }
        public double OpenPrice { get; }
        public double ClosePrice { get; }
        public double ChangePercent { get; }
        public double High { get; }
        public double Low { get; }
        public long Volume { get; }
        public double? MA150 { get; }
        public double? Spread { get; }
        public DateTime LastUpdate { get; }

        public TickerEodData(int tickerId,
            DateTime date,
            double openPrice,
            double closePrice,
            double changePercent,
            double high,
            double low,
            long volume,
            double? ma150,
            DateTime lastUpdate
           )
        {
            this.TickerId = tickerId;
            this.Date = date;
            this.OpenPrice = openPrice;
            this.ClosePrice = closePrice;
            this.ChangePercent = changePercent;
            this.High = high;
            this.Low = low;
            this.Volume = volume;
            this.MA150 = ma150;
            this.Spread = ma150 != 0 
                ? (closePrice / ma150 - 1) *100
                : (double?)null;
            this.LastUpdate = lastUpdate;
        }
    }
}
