﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class MovingAverageItem
    {
        public int Size { get; }
        public double? Value { get; }
        public double? Spread { get; }

        public MovingAverageItem(int size, double? value, double? closePrice)
        {
            this.Size = size;
            this.Spread = value.HasValue && closePrice.HasValue && value != 0
                ? (closePrice / value - 1) * 100
                : (double?)null;
            this.Value = value;
        }
    }
}
