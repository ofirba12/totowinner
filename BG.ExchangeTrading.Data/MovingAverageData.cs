﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BG.ExchangeTrading.Data
{
    public class MovingAverageData
    {
        public int MaDays { get; }
        public double MaValue { get; }
        public double SpreadValue { get; }

        public MovingAverageData(int maDays, double maValue, double spread)
        {
            this.MaDays = maDays;
            this.MaValue = maValue;
            this.SpreadValue = spread;
        }
    }
}
