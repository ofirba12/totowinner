﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgData.Generators
{
    internal static class GoldserveGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger("GoldserveGenerator");
        private static GoalServeProviderServices _goalServeSrv = GoalServeProviderServices.Instance;
        private static Dictionary<ExecutionType, BgGoalServeExecution> executions = new Dictionary<ExecutionType, BgGoalServeExecution>();
        private static void LogAndDump(string msg)
        {
            _log.Info(msg);
            Console.WriteLine(msg);
        }

        internal static void Prerequisite(GoalServeSportType sport, bool execute)
        {
            try
            {
                _goalServeSrv.ResetDumper();
                for (var dDay = -1 /* Yesterday */; dDay < 3; dDay++)
                {
                    try
                    {
                        var fetchDay = (GoalServeMatchDayFetch)dDay;
                        if (sport == GoalServeSportType.Handball && fetchDay != GoalServeMatchDayFetch.Today)
                            continue;
                        var execution = new ExecutionType(sport, fetchDay);
                        executions.Add(execution, new BgGoalServeExecution(sport, fetchDay));
                        if (execute)
                            executions[execution].Execute();
                    }
                    catch(Exception ex)
                    {
                        _log.Error($"Error occurred prerquiste: {sport} {(GoalServeMatchDayFetch)dDay}, execution will be skipped", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error($"Failed to execute prerequisite for sport {sport}", ex);
            }
        }
        internal static int Executes()
        {
            var toExecute = new List<BgGoalServeExecution>();
            foreach (var exe in executions.Values)
            {
                if (exe.CanExecute())
                    toExecute.Add(exe);
            }
            if (toExecute.Count > 0)
            {
                var systemRepository = BgSystemServices.Instance.GetBgSystemRepository();
                BgSystemServices.Instance.SetSystemField(BgSystemField.GoalserveStartGeneration, DateTime.Now);
                _goalServeSrv.ResetDumper();
            }
            foreach (var exe in toExecute)
                exe.Execute();
            return toExecute.Count();
        }
        internal static BgGoalServeRepository GenerateSportData(BgGoalServeFetchRequest fetchRequest)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.White;
                LogAndDump($"Fetching {fetchRequest.Sport} matches data for {fetchRequest.Type}:{fetchRequest.Date.ToShortDateString()}...{DateTime.Now}");
                var json = _goalServeSrv.FetchRawData(fetchRequest);
                LogAndDump($"Fetching {fetchRequest.Type} odds and parsing Data...");
                var skippedInfo = new List<string>();
                var repository = _goalServeSrv.ParseCategories(json, fetchRequest, skippedInfo);
                if (repository != null)
                {
                    if (skippedInfo.Count == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        LogAndDump($"{fetchRequest.Type}: Parsing process finished successfully");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        LogAndDump($"{fetchRequest.Type}: Parsing process finished successfully with some issues");
                        foreach (var issue in skippedInfo)
                            _log.Warn(issue);
                    }
                    _goalServeSrv.DumpToCSV(repository, fetchRequest);
                    Console.ForegroundColor = ConsoleColor.Green;
                    LogAndDump($"{fetchRequest.Type}: CSV file created successfully");
                    var persistanceFeedType = _goalServeSrv.GetPersistanceMatches(fetchRequest.Sport, repository.Matches.Keys.Select(i => i.MatchId).ToList());
                    _goalServeSrv.AddOrUpdate(repository, fetchRequest.Sport, persistanceFeedType);
                    LogAndDump($"{fetchRequest.Type}: Updated Database successfully. {DateTime.Now}");

                    return repository;
                }
                else
                    LogAndDump($"{fetchRequest.Type}: ## NO Data found for this day ##");
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to gennerate sport data for {fetchRequest.Type}:{fetchRequest.Date.ToShortDateString()}", ex);
            }
            return null;
        }
    }
}
