﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData;

namespace BgData.Generators
{
    internal class HomeAwayContainer
    {
        public int Count { get; }
        public int? TotalRowsHome { get; }
        public int? TotalRowsAway { get; }
        public BgStatisticsDetailType HomeType { get; }
        public BgStatisticsDetailType AwayType { get; }
        public int? HomePercent { get; }
        public int? AwayPercent { get; }
        public BgTeamsFilter HomeFilter { get; }
        public BgTeamsFilter AwayFilter { get; }
        public HomeAwayContainer(int count, 
            int? totalRowsHome, 
            int? totalRowsAway, 
            BgStatisticsDetailType homeType, 
            BgStatisticsDetailType awayType, 
            int? homePercent, 
            int? awayPercent, 
            BgTeamsFilter homeFilter, 
            BgTeamsFilter awayFilter)
        {
            Count = count;
            TotalRowsHome = totalRowsHome;
            TotalRowsAway = totalRowsAway;
            HomeType = homeType;
            AwayType = awayType;
            HomePercent = homePercent;
            AwayPercent = awayPercent;
            HomeFilter = homeFilter;
            AwayFilter = awayFilter;
        }
    }
}
