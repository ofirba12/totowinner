﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.BgDataApi;
using TotoWinner.Services;

namespace BgData.Generators
{
    internal static class BgMatchStatisticsGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger("BgMatchStatisticsGenerator");
        private static BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;
        private static BgDataServices bgDataServices = BgDataServices.Instance;
        private static MatchesCashRepository teamsMatches = new MatchesCashRepository(_log);
        private static BgDataPersistanceServices persistance = BgDataPersistanceServices.Instance;

        internal static void Executes()
        {
            try
            {
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgMatchStatisticsStartGeneration, DateTime.Now);
                var mapper = BgMapperServices.Instance.GetBgMapper();
                var matches = FindMatchesForCycle();
                var maxRows = 6;
                var analysisServices = BgDataAnalysisServices.Instance;
                teamsMatches.Maintain();
                var statistics = new List<BgMatchStatistics>();
                var details = new List<BgMatchStatisticsDetails>();
                var info = new List<BgMatchInfo>();
                var tips = new List<BgMatchTips>();
                var statisticsDate = DateTime.Now;
                foreach (var match in matches)
                {
                    try
                    {
                        var teamAmatchesAllAll = teamsMatches.GetMatches(match.HomeId);
                        var teamBmatchesAllAll = teamsMatches.GetMatches(match.AwayId);
                        var reCalcRepo = teamsMatches.Recalc(match.MatchId, match.HomeId, match.AwayId, new OddsOnly(match.HomeOdd.Value, match.DrawOdd.Value, match.AwayOdd.Value));
                        if (reCalcRepo.Recalc)
                        {
                            try
                            {
                                var matchOdds = new BgMatchOdds(match.Sport, match.HomeId, match.AwayId, match.HomeOdd.Value, match.DrawOdd.Value, match.AwayOdd.Value);
                                var bgTeamsAnalysisRepository = analysisServices.PrepareTeamsRepository(match.HomeId, match.AwayId, teamAmatchesAllAll, teamBmatchesAllAll);
                                var bettingDataRepo = analysisServices.PrepareTeamsBettingDataRepository(bgTeamsAnalysisRepository, maxRows);
                                var bgTeamsStatisitcsRepo = analysisServices.PrepareTeamsStatisitcsRepository(bgTeamsAnalysisRepository, maxRows);
                                var allBetsRepo = analysisServices.PrepareAllBetsRepository(matchOdds, bgTeamsStatisitcsRepo);

                                var home = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.HomeId, mapper);
                                var away = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.AwayId, mapper);
                                _log.Info($"Calculated, Match id={match.MatchId} [{home} # {match.HomeId}], [{away} # {match.AwayId}] starting at {match.GameStart} [{match.HomeOdd}/{match.DrawOdd}/{match.AwayOdd}], homeGames={teamAmatchesAllAll.Count()} awayGames={teamBmatchesAllAll.Count()}, {reCalcRepo}");

                                AddMatchInfo(info, statisticsDate, match, bgTeamsAnalysisRepository);
                                AddStatisticsTotals(statistics, statisticsDate, match, allBetsRepo);
                                AddStatisticsDetails(details, statisticsDate, match, allBetsRepo);
                                AddMatchTips(tips, statisticsDate, match, bgTeamsAnalysisRepository, mapper);
                            }
                            catch (Exception ex)
                            {
                                _log.Error($"An error occurred trying to calc all bets for {match.ToString()}", ex);
                            }
                        }
                        else
                        {
                            var home = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.HomeId, mapper);
                            var away = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.AwayId, mapper);
                            _log.Info($"Skipped, Match id={match.MatchId} [{home} # {match.HomeId}], [{away} # {match.AwayId}] starting at {match.GameStart}, {reCalcRepo}");
                        }
                    }
                    catch (Exception ex)
                    {
                        var home = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.HomeId, mapper);
                        var away = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.AwayId, mapper);
                        _log.Error($"unexpected error found with team [{home} # {match.HomeId}], [{away} # {match.AwayId}], [{match.HomeOdd}/{match.DrawOdd}/{match.AwayOdd}]", ex);
                    }
                }
                _log.Info("Saving to database");
                persistance.MergeBgFeedInfo(info);
                persistance.MergeBgFeedStatistics(statistics);
                persistance.MergeBgFeedStatisticsDetails(details);
                var bestTips = PrepareTips(tips)
                    .OrderByDescending(t => t.Value)
                    .Take(20)
                    .ToList();
                persistance.MergeBgFeedTips(bestTips);
                _log.Info($"Finished Successfully, {Environment.NewLine}Teams Collection Counter {teamsMatches.TeamsCollectionCounter()}{Environment.NewLine}Teams Match Counter {teamsMatches.TeamsMatchCounter()}{Environment.NewLine}Last EOD Fetched Data Counter {teamsMatches.LastEodFetchedDataCounter()}{Environment.NewLine}Matches Odds Counter {teamsMatches.MatchesOddsCounter()}");
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgMatchStatisticsFinishGeneration, DateTime.Now);
            }
        }

        private static List<BgMatchTypedTip> PrepareTips(List<BgMatchTips> tips)
        {
            var typedTips = new List<BgMatchTypedTip>();
            foreach (var tip in tips)
            {
                var fullTime = new BgMatchTypedTip(tip.MatchId, BgTipType.FullTime, tip.FullTimeTitle, tip.FullTimeValue, tip.FullTimeStars, tip.FullTimeTipDescription, false, tip.LastUpdate);
                typedTips.Add(fullTime);
                var ov2_5 = new BgMatchTypedTip(tip.MatchId, BgTipType.OverUnder2_5, tip.OverUnder2_5Title, tip.OverUnder2_5Value, tip.OverUnder2_5Stars, tip.OverUnder2_5TipDescription, false, tip.LastUpdate);
                typedTips.Add(ov2_5);
                var range = new BgMatchTypedTip(tip.MatchId, BgTipType.Range, tip.RangeTitle, tip.RangeValue, tip.RangeStars, tip.RangeTipDescription, false, tip.LastUpdate);
                typedTips.Add(range);
            }
            return typedTips;
        }

        private static void AddMatchTips(List<BgMatchTips> tips, DateTime statisticsDate, BgRawMatch match, BgTeamsAnalysisRepository bgTeamsAnalysisRepository, MapperRespository mapper)
        {
            var home = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.HomeId, mapper);
            var away = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.AwayId, mapper);
            var league = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Leagues, match.LeagueId, mapper);
            var country = bgDataServices.ExtractBgNameFromBgId(match.Sport, BgMapperType.Countries, match.CountryId.Value, mapper);
            var bgMatch = new BgMatch(match, home, away, league, country);
            var matchTips = BgDataScoresServices.Instance.GetMatchTips(bgMatch, bgTeamsAnalysisRepository);
            if (matchTips != null)
                tips.Add(new BgMatchTips(match.MatchId, statisticsDate, matchTips));
        }

        #region FindMatchesForCycle
        private static List<BgRawMatch> FindMatchesForCycle()
        {
            var activeDateRange = new List<DateTime>() { DateTime.Today.Date, DateTime.Today.Date.AddDays(1), DateTime.Today.Date.AddDays(2) };
            var sports = new List<BgSport>() { BgSport.Soccer };

            var matches = new List<BgRawMatch>();
            Func<BgRawMatch, bool> isValidCandidate = (match) =>
            {
                if (match.HomeOdd.HasValue &&
                    match.DrawOdd.HasValue &&
                    match.AwayOdd.HasValue &&
                    !match.HomeScore.HasValue &&
                    !match.AwayScore.HasValue &&
                    match.Status.Contains(":") &&
                    match.Status.Count() == 5) // time of the game 13:00
                    return true;
                return false;
            };

            foreach (var sport in sports)
            {
                foreach (var fetchDate in activeDateRange)
                {
                    var feed = BgDataServices.Instance.GetBgRawFeedWithFilter(sport,
                        fetchDate,
                        0,
                        null,
                        null);
                    var candidates = feed.Where(m => isValidCandidate(m));
                    matches.AddRange(candidates);
                    _log.Info($"{candidates.Count()} valid candidates found out of {feed.Count()} for date {fetchDate}, for {sport}");
                }
            }
            _log.Info($"Found {matches.Count()} valid matches for this cycle");
            return matches;
        }
        #endregion

        #region AddMatchInfo
        private static void AddMatchInfo(List<BgMatchInfo> info, DateTime infoDate, BgRawMatch match, BgTeamsAnalysisRepository bgTeamsAnalysisRepository)
        {
            info.Add(new BgMatchInfo(
                match.MatchId,
                bgTeamsAnalysisRepository.TeamAmatchesAllAll.Count(t => t.MissingOdds || t.MissingScores || t.MissingQuarters)
                +
                bgTeamsAnalysisRepository.TeamBmatchesAllAll.Count(t => t.MissingOdds || t.MissingScores || t.MissingQuarters),
                bgTeamsAnalysisRepository.TeamAmatchesAllAll.Count,
                bgTeamsAnalysisRepository.TeamBmatchesAllAll.Count,
                bgTeamsAnalysisRepository.H2hMatches.Count,
                infoDate
            ));
        }
        #endregion

        #region AddStatisticsDetails
        private static void AddStatisticsDetails(List<BgMatchStatisticsDetails> details, DateTime statisticsDate, BgRawMatch match, BgAllBetsRepository allBetsRepo)
        {
            try
            {
                for (int index = 0; index < 4; index++)
                {
                    #region FullData
                    GenerateDataHomeAwayStatisticsDetails<BgFullTimeHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.FullTimeHomeWinAwayLose,
                        allBetsRepo.FullData.HomeWinAwayLose);
                    GenerateDataH2HStatisticsDetails<BgFullTimeH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.FullTimeHomeWinAwayLose,
                        allBetsRepo.FullData.HomeWinH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgFullTimeHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.FullTimeDraw,
                        allBetsRepo.FullData.HomeDrawAwayDraw);
                    GenerateDataH2HStatisticsDetails<BgFullTimeH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.FullTimeDraw,
                        allBetsRepo.FullData.DrawH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgFullTimeHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.FullTimeAwayWinHomeLose,
                        allBetsRepo.FullData.AwayWinHomeLose);
                    GenerateDataH2HStatisticsDetails<BgFullTimeH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.FullTimeAwayWinHomeLose,
                        allBetsRepo.FullData.AwayWinH2H);
                    #endregion
                    #region Over/Under 3.5
                    GenerateDataHomeAwayStatisticsDetails<BgOverUnderHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Over3_5HomeAway,
                        allBetsRepo.OverUnder35BetData.HomeOverAwayOver);
                    GenerateDataH2HStatisticsDetails<BgOverUnderH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Over3_5HomeAway,
                        allBetsRepo.OverUnder35BetData.OverH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgOverUnderHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Under3_5HomeAway,
                        allBetsRepo.OverUnder35BetData.HomeUnderAwayUnder);
                    GenerateDataH2HStatisticsDetails<BgOverUnderH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Under3_5HomeAway,
                        allBetsRepo.OverUnder35BetData.UnderH2H);
                    #endregion
                    #region Over/Under 2.5
                    GenerateDataHomeAwayStatisticsDetails<BgOverUnderHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Over2_5HomeAway,
                        allBetsRepo.OverUnder25BetData.HomeOverAwayOver);
                    GenerateDataH2HStatisticsDetails<BgOverUnderH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Over2_5HomeAway,
                        allBetsRepo.OverUnder25BetData.OverH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgOverUnderHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Under2_5HomeAway,
                        allBetsRepo.OverUnder25BetData.HomeUnderAwayUnder);
                    GenerateDataH2HStatisticsDetails<BgOverUnderH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Under2_5HomeAway,
                        allBetsRepo.OverUnder25BetData.UnderH2H);
                    #endregion
                    #region Over/Under 1.5
                    GenerateDataHomeAwayStatisticsDetails<BgOverUnderHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Over1_5HomeAway,
                        allBetsRepo.OverUnder15BetData.HomeOverAwayOver);
                    GenerateDataH2HStatisticsDetails<BgOverUnderH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Over1_5HomeAway,
                        allBetsRepo.OverUnder15BetData.OverH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgOverUnderHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Under1_5HomeAway,
                        allBetsRepo.OverUnder15BetData.HomeUnderAwayUnder);
                    GenerateDataH2HStatisticsDetails<BgOverUnderH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Under1_5HomeAway,
                        allBetsRepo.OverUnder15BetData.UnderH2H);
                    #endregion
                    #region Range 0-1
                    GenerateDataHomeAwayStatisticsDetails<BgRangeHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Range0_1HomeAway,
                        allBetsRepo.RangeBetData.RangeBetween0and1);
                    GenerateDataH2HStatisticsDetails<BgRangeH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Range0_1HomeAway,
                        allBetsRepo.RangeBetData.RangeBetween0and1H2H);
                    #endregion
                    #region Range 2-3
                    GenerateDataHomeAwayStatisticsDetails<BgRangeHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Range2_3HomeAway,
                        allBetsRepo.RangeBetData.RangeBetween2and3);
                    GenerateDataH2HStatisticsDetails<BgRangeH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Range2_3HomeAway,
                        allBetsRepo.RangeBetData.RangeBetween2and3H2H);
                    #endregion
                    #region Range 4Plus
                    GenerateDataHomeAwayStatisticsDetails<BgRangeHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Range4PlusHomeAway,
                        allBetsRepo.RangeBetData.Range4Plus);
                    GenerateDataH2HStatisticsDetails<BgRangeH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.Range4PlusHomeAway,
                        allBetsRepo.RangeBetData.Range4PlusH2H);
                    #endregion
                    #region BothScore
                    GenerateDataHomeAwayStatisticsDetails<BgYesNoHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.BothScoreYes,
                        allBetsRepo.BothScoreBetData.BothScoreYes);
                    GenerateDataH2HStatisticsDetails<BgYesNoH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.BothScoreYes,
                        allBetsRepo.BothScoreBetData.BothScoreYesH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgYesNoHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.BothScoreNo,
                        allBetsRepo.BothScoreBetData.BothScoreNo);
                    GenerateDataH2HStatisticsDetails<BgYesNoH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.BothScoreNo,
                        allBetsRepo.BothScoreBetData.BothScoreNoH2H);
                    #endregion
                    #region Win/Draw/Lose
                    GenerateDataHomeAwayStatisticsDetails<BgYesNoHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.WinOrDraw,
                        allBetsRepo.WinDrawLoseBetData.WinDraw);
                    GenerateDataH2HStatisticsDetails<BgYesNoH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.WinOrDraw,
                        allBetsRepo.WinDrawLoseBetData.WinDrawH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgYesNoHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.HomeAwayNoDraw,
                        allBetsRepo.WinDrawLoseBetData.NoDraw);
                    GenerateDataH2HStatisticsDetails<BgYesNoH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.HomeAwayNoDraw,
                        allBetsRepo.WinDrawLoseBetData.NoDrawH2H);
                    GenerateDataHomeAwayStatisticsDetails<BgYesNoHomeAwayBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.DrawOrLose,
                        allBetsRepo.WinDrawLoseBetData.DrawLose);
                    GenerateDataH2HStatisticsDetails<BgYesNoH2HBetData>(details,
                        statisticsDate,
                        match,
                        index,
                        BgBetTypeComponent.DrawOrLose,
                        allBetsRepo.WinDrawLoseBetData.DrawLoseH2H);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to add statistical details", ex);
            }
        }
        #endregion

        #region GenerateDataH2HStatisticsDetails
        private static void GenerateDataH2HStatisticsDetails<T>(List<BgMatchStatisticsDetails> details,
            DateTime statisticsDate,
            BgRawMatch match,
            int index,
            BgBetTypeComponent type,
            T data) where T : IBetDataH2HStatistics
        {
            var item = BetTypeFactory.GetStatisticsH2HRepo(data, index, type);
            var h2H = new BgMatchStatisticsDetails(match.MatchId,
                item.Type,
                index,
                item.Filter,
                item.TotalRows,
                item.Percent,
                statisticsDate);
            details.Add(h2H);
        }
        #endregion

        #region GenerateDataHomeAwayStatisticsDetails
        private static void GenerateDataHomeAwayStatisticsDetails<T>(List<BgMatchStatisticsDetails> details,
            DateTime statisticsDate,
            BgRawMatch match,
            int index,
            BgBetTypeComponent type,
            T data) where T : IBetDataHomeAwayStatistics
        {
            var item = BetTypeFactory.GetStatisticsHomeAwayRepo(data, index, type);
            var fullDataHome = new BgMatchStatisticsDetails(match.MatchId,
                item.HomeType,
                index,
                item.HomeFilter,
                item.TotalRowsHome,
                item.HomePercent,
                statisticsDate);
            details.Add(fullDataHome);

            var fullDataAway = new BgMatchStatisticsDetails(match.MatchId,
                item.AwayType,
                index,
                item.AwayFilter,
                item.TotalRowsAway,
                item.AwayPercent,
                statisticsDate);
            details.Add(fullDataAway);
        }
        #endregion

        #region AddStatisticsTotals
        private static void AddStatisticsTotals(List<BgMatchStatistics> statistics, DateTime statisticsDate, BgRawMatch match, BgAllBetsRepository allBetsRepo)
        {
            for (int index = 0; index < 4; index++)
            {
                statistics.Add(new BgMatchStatistics(
                    match.MatchId,
                    index,
                #region FullData
                    allBetsRepo.FullData.HomeWinAwayLose.Totals.Count > index && allBetsRepo.FullData.HomeWinAwayLose.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.FullData.HomeWinAwayLose.Totals[index]?.TotalPercent
                        : null,
                    allBetsRepo.FullData.HomeDrawAwayDraw.Totals.Count > index && allBetsRepo.FullData.HomeDrawAwayDraw.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.FullData.HomeDrawAwayDraw.Totals[index]?.TotalPercent : null,
                    allBetsRepo.FullData.AwayWinHomeLose.Totals.Count > index && allBetsRepo.FullData.AwayWinHomeLose.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.FullData.AwayWinHomeLose.Totals[index]?.TotalPercent : null,
                    allBetsRepo.FullData.HomeWinH2H.H2HCandidates.Count > index && allBetsRepo.FullData.HomeWinH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.FullData.HomeWinH2H.H2HCandidates[index]?.TotalIndexPercentW : null,
                    allBetsRepo.FullData.DrawH2H.H2HCandidates.Count > index && allBetsRepo.FullData.DrawH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.FullData.DrawH2H.H2HCandidates[index]?.TotalIndexPercentD : null,
                    allBetsRepo.FullData.AwayWinH2H.H2HCandidates.Count > index && allBetsRepo.FullData.AwayWinH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.FullData.AwayWinH2H.H2HCandidates[index]?.TotalIndexPercentL : null,
                #endregion
                #region OverUnder35BetData
                    allBetsRepo.OverUnder35BetData.HomeOverAwayOver.Totals.Count > index && allBetsRepo.OverUnder35BetData.HomeOverAwayOver.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.OverUnder35BetData.HomeOverAwayOver.Totals[index]?.TotalPercent : null,
                    allBetsRepo.OverUnder35BetData.HomeUnderAwayUnder.Totals.Count > index && allBetsRepo.OverUnder35BetData.HomeUnderAwayUnder.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.OverUnder35BetData.HomeUnderAwayUnder.Totals[index]?.TotalPercent : null,
                    allBetsRepo.OverUnder35BetData.OverH2H.H2HCandidates.Count > index && allBetsRepo.OverUnder35BetData.OverH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.OverUnder35BetData.OverH2H.H2HCandidates[index]?.TotalOverPercent : null,
                    allBetsRepo.OverUnder35BetData.UnderH2H.H2HCandidates.Count > index && allBetsRepo.OverUnder35BetData.UnderH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.OverUnder35BetData.UnderH2H.H2HCandidates[index]?.TotalUnderPercent : null,
                #endregion
                #region OverUnder25BetData
                    allBetsRepo.OverUnder25BetData.HomeOverAwayOver.Totals.Count > index && allBetsRepo.OverUnder25BetData.HomeOverAwayOver.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.OverUnder25BetData.HomeOverAwayOver.Totals[index]?.TotalPercent : null,
                    allBetsRepo.OverUnder25BetData.HomeUnderAwayUnder.Totals.Count > index && allBetsRepo.OverUnder25BetData.HomeUnderAwayUnder.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.OverUnder25BetData.HomeUnderAwayUnder.Totals[index]?.TotalPercent : null,
                    allBetsRepo.OverUnder25BetData.OverH2H.H2HCandidates.Count > index && allBetsRepo.OverUnder25BetData.OverH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.OverUnder25BetData.OverH2H.H2HCandidates[index]?.TotalOverPercent : null,
                    allBetsRepo.OverUnder25BetData.UnderH2H.H2HCandidates.Count > index && allBetsRepo.OverUnder25BetData.UnderH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.OverUnder25BetData.UnderH2H.H2HCandidates[index]?.TotalUnderPercent : null,
                #endregion
                #region OverUnder15BetData
                    allBetsRepo.OverUnder15BetData.HomeOverAwayOver.Totals.Count > index && allBetsRepo.OverUnder15BetData.HomeOverAwayOver.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.OverUnder15BetData.HomeOverAwayOver.Totals[index]?.TotalPercent : null,
                    allBetsRepo.OverUnder15BetData.HomeUnderAwayUnder.Totals.Count > index && allBetsRepo.OverUnder15BetData.HomeUnderAwayUnder.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.OverUnder15BetData.HomeUnderAwayUnder.Totals[index]?.TotalPercent : null,
                    allBetsRepo.OverUnder15BetData.OverH2H.H2HCandidates.Count > index && allBetsRepo.OverUnder15BetData.OverH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.OverUnder15BetData.OverH2H.H2HCandidates[index]?.TotalOverPercent : null,
                    allBetsRepo.OverUnder15BetData.UnderH2H.H2HCandidates.Count > index && allBetsRepo.OverUnder15BetData.UnderH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.OverUnder15BetData.UnderH2H.H2HCandidates[index]?.TotalUnderPercent : null,
                #endregion
                #region RangeBetData
                    allBetsRepo.RangeBetData.RangeBetween0and1.Totals.Count > index && allBetsRepo.RangeBetData.RangeBetween0and1.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.RangeBetData.RangeBetween0and1.Totals[index]?.TotalPercent : null,
                    allBetsRepo.RangeBetData.RangeBetween2and3.Totals.Count > index && allBetsRepo.RangeBetData.RangeBetween2and3.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.RangeBetData.RangeBetween2and3.Totals[index]?.TotalPercent : null,
                    allBetsRepo.RangeBetData.Range4Plus.Totals.Count > index && allBetsRepo.RangeBetData.Range4Plus.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.RangeBetData.Range4Plus.Totals[index]?.TotalPercent : null,
                    allBetsRepo.RangeBetData.RangeBetween0and1H2H.H2HCandidates.Count > index && allBetsRepo.RangeBetData.RangeBetween0and1H2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.RangeBetData.RangeBetween0and1H2H.H2HCandidates[index]?.TotalBetween0and1Percent : null,
                    allBetsRepo.RangeBetData.RangeBetween2and3H2H.H2HCandidates.Count > index && allBetsRepo.RangeBetData.RangeBetween2and3H2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.RangeBetData.RangeBetween2and3H2H.H2HCandidates[index]?.TotalBetween2and3Percent : null,
                    allBetsRepo.RangeBetData.Range4PlusH2H.H2HCandidates.Count > index && allBetsRepo.RangeBetData.Range4PlusH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.RangeBetData.Range4PlusH2H.H2HCandidates[index]?.Total4PlusPercent : null,
                #endregion
                #region BothScoreBetData
                    allBetsRepo.BothScoreBetData.BothScoreYes.Totals.Count > index && allBetsRepo.BothScoreBetData.BothScoreYes.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.BothScoreBetData.BothScoreYes.Totals[index]?.TotalPercent : null,
                    allBetsRepo.BothScoreBetData.BothScoreNo.Totals.Count > index && allBetsRepo.BothScoreBetData.BothScoreNo.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.BothScoreBetData.BothScoreNo.Totals[index]?.TotalPercent : null,
                    allBetsRepo.BothScoreBetData.BothScoreYesH2H.H2HCandidates.Count > index && allBetsRepo.BothScoreBetData.BothScoreYesH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.BothScoreBetData.BothScoreYesH2H.H2HCandidates[index]?.YesPercent : null,
                    allBetsRepo.BothScoreBetData.BothScoreNoH2H.H2HCandidates.Count > index && allBetsRepo.BothScoreBetData.BothScoreNoH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.BothScoreBetData.BothScoreNoH2H.H2HCandidates[index]?.NoPercent : null,
                #endregion
                #region WinDrawLoseBetData
                    allBetsRepo.WinDrawLoseBetData.WinDraw.Totals.Count > index && allBetsRepo.WinDrawLoseBetData.WinDraw.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.WinDrawLoseBetData.WinDraw.Totals[index]?.TotalPercent : null,
                    allBetsRepo.WinDrawLoseBetData.NoDraw.Totals.Count > index && allBetsRepo.WinDrawLoseBetData.NoDraw.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.WinDrawLoseBetData.NoDraw.Totals[index]?.TotalPercent : null,
                    allBetsRepo.WinDrawLoseBetData.DrawLose.Totals.Count > index && allBetsRepo.WinDrawLoseBetData.DrawLose.Totals.First()?.TotalRows > 5
                        ? allBetsRepo.WinDrawLoseBetData.DrawLose.Totals[index]?.TotalPercent : null,
                    allBetsRepo.WinDrawLoseBetData.WinDrawH2H.H2HCandidates.Count > index && allBetsRepo.WinDrawLoseBetData.WinDrawH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.WinDrawLoseBetData.WinDrawH2H.H2HCandidates[index]?.YesPercent : null,
                    allBetsRepo.WinDrawLoseBetData.NoDrawH2H.H2HCandidates.Count > index && allBetsRepo.WinDrawLoseBetData.NoDrawH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.WinDrawLoseBetData.NoDrawH2H.H2HCandidates[index]?.YesPercent : null,
                    allBetsRepo.WinDrawLoseBetData.DrawLoseH2H.H2HCandidates.Count > index && allBetsRepo.WinDrawLoseBetData.DrawLoseH2H.H2HCandidates.First()?.TotalRows > 2
                        ? allBetsRepo.WinDrawLoseBetData.DrawLoseH2H.H2HCandidates[index]?.YesPercent : null,
                #endregion
                    allBetsRepo.FullData.HomeWinAwayLose.HomeCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber,
                    allBetsRepo.FullData.HomeWinAwayLose.AwayCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber,
                    statisticsDate
                ));
            }
        }
        #endregion
    }
}
