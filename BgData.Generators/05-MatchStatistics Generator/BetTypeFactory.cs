﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgData.Generators
{
    internal static class BetTypeFactory
    {
        internal static HomeAwayContainer GetStatisticsHomeAwayRepo<T>(T data, int index, BgBetTypeComponent type) where T : IBetDataHomeAwayStatistics
        {
            HomeAwayContainer container = null;

            if (data is BgFullTimeHomeAwayBetData)
            {
                container = GetHomeAwayFullTimeRepo(data as BgFullTimeHomeAwayBetData, index, type);
            }
            else if (data is BgOverUnderHomeAwayBetData)
            {
                container = GetHomeAwayOverUnderRepo(data as BgOverUnderHomeAwayBetData, index, type);
            }
            else if (data is BgRangeHomeAwayBetData)
            {
                container = GetHomeAwayRangeRepo(data as BgRangeHomeAwayBetData, index, type);
            }
            else if (data is BgYesNoHomeAwayBetData)
            {
                container = GetHomeAwayYesNoRepo(data as BgYesNoHomeAwayBetData, index, type);
            }
            return container;
        }

        #region GetHomeAwayFullTimeRepo
        private static HomeAwayContainer GetHomeAwayFullTimeRepo(BgFullTimeHomeAwayBetData item, int index, BgBetTypeComponent type)
        {
            var detailHomeType = BgStatisticsDetailType.FullTime__HomeWin__HomeWin;
            var detailAwayType = BgStatisticsDetailType.FullTime__HomeWin__AwayLoose;
            var homePercent = (byte?)0;
            var awayPercent = (byte?)0;
            var hasValueHome = item.Totals.Count > index && item.HomeCandidates[index].TotalRows > 0;
            var hasValueAway = item.Totals.Count > index && item.AwayCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.FullTimeHomeWinAwayLose:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalIndexPercentW) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalIndexPercentL) : (byte?)null;
                    detailHomeType = BgStatisticsDetailType.FullTime__HomeWin__HomeWin;
                    detailAwayType = BgStatisticsDetailType.FullTime__HomeWin__AwayLoose;
                    break;
                case BgBetTypeComponent.FullTimeDraw:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalIndexPercentD) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalIndexPercentD) : (byte?)null;
                    detailHomeType = BgStatisticsDetailType.FullTime__Draw__HomeDraw;
                    detailAwayType = BgStatisticsDetailType.FullTime__Draw__AwayDraw;
                    break;
                case BgBetTypeComponent.FullTimeAwayWinHomeLose:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalIndexPercentL) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalIndexPercentW) : (byte?)null;
                    detailHomeType = BgStatisticsDetailType.FullTime__AwayWin__HomeLoose;
                    detailAwayType = BgStatisticsDetailType.FullTime__AwayWin__AwayWin;
                    break;
            }
            var container = new HomeAwayContainer(item.Totals.Count,
                hasValueHome ? item.HomeCandidates[index].TotalRows : (int?)null,
                hasValueAway ? item.AwayCandidates[index].TotalRows : (int?)null,
                detailHomeType,
                detailAwayType,
                homePercent,
                awayPercent,
                item.HomeCandidates[index].Filter,
                item.AwayCandidates[index].Filter);
            return container;
        }
        #endregion

        #region GetHomeAwayOverUnderRepo
        private static HomeAwayContainer GetHomeAwayOverUnderRepo(BgOverUnderHomeAwayBetData item, int index, BgBetTypeComponent type)
        {
            var detailHomeType = BgStatisticsDetailType.FullTime__HomeWin__HomeWin;
            var detailAwayType = BgStatisticsDetailType.FullTime__HomeWin__AwayLoose;
            var homePercent = (byte?)0;
            var awayPercent = (byte?)0;
            var hasValueHome = item.Totals.Count > index && item.HomeCandidates[index].TotalRows > 0;
            var hasValueAway = item.Totals.Count > index && item.AwayCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.Over3_5HomeAway:
                case BgBetTypeComponent.Over2_5HomeAway:
                case BgBetTypeComponent.Over1_5HomeAway:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalOverPercent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalOverPercent) : (byte?)null;
                    switch (type)
                    {
                        case BgBetTypeComponent.Over3_5HomeAway:
                            detailHomeType = BgStatisticsDetailType.OverUnder__Over3_5__HomeOver;
                            detailAwayType = BgStatisticsDetailType.OverUnder__Over3_5__AwayOver;
                            break;
                        case BgBetTypeComponent.Over2_5HomeAway:
                            detailHomeType = BgStatisticsDetailType.OverUnder__Over2_5__HomeOver;
                            detailAwayType = BgStatisticsDetailType.OverUnder__Over2_5__AwayOver;
                            break;
                        case BgBetTypeComponent.Over1_5HomeAway:
                            detailHomeType = BgStatisticsDetailType.OverUnder__Over1_5__HomeOver;
                            detailAwayType = BgStatisticsDetailType.OverUnder__Over1_5__AwayOver;
                            break;
                    }
                    break;
                case BgBetTypeComponent.Under3_5HomeAway:
                case BgBetTypeComponent.Under2_5HomeAway:
                case BgBetTypeComponent.Under1_5HomeAway:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalUnderPercent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalUnderPercent) : (byte?)null;
                    switch (type)
                    {
                        case BgBetTypeComponent.Under3_5HomeAway:
                            detailHomeType = BgStatisticsDetailType.OverUnder__Under3_5__HomeUnder;
                            detailAwayType = BgStatisticsDetailType.OverUnder__Under3_5__AwayUnder;
                            break;
                        case BgBetTypeComponent.Under2_5HomeAway:
                            detailHomeType = BgStatisticsDetailType.OverUnder__Under2_5__HomeUnder;
                            detailAwayType = BgStatisticsDetailType.OverUnder__Under2_5__AwayUnder;
                            break;
                        case BgBetTypeComponent.Under1_5HomeAway:
                            detailHomeType = BgStatisticsDetailType.OverUnder__Under1_5__HomeUnder;
                            detailAwayType = BgStatisticsDetailType.OverUnder__Under1_5__AwayUnder;
                            break;
                    }
                    break;
            }
            var container = new HomeAwayContainer(item.Totals.Count,
                hasValueHome ? item.HomeCandidates[index].TotalRows : (int?)null,
                hasValueAway ? item.AwayCandidates[index].TotalRows : (int?)null,
                detailHomeType,
                detailAwayType,
                homePercent,
                awayPercent,
                item.HomeCandidates[index].Filter,
                item.AwayCandidates[index].Filter);
            return container;
        }
        #endregion

        #region GetHomeAwayRangeRepo
        private static HomeAwayContainer GetHomeAwayRangeRepo(BgRangeHomeAwayBetData item, int index, BgBetTypeComponent type)
        {
            var detailHomeType = BgStatisticsDetailType.FullTime__HomeWin__HomeWin;
            var detailAwayType = BgStatisticsDetailType.FullTime__HomeWin__AwayLoose;
            var homePercent = (byte?)0;
            var awayPercent = (byte?)0;
            var hasValueHome = item.Totals.Count > index && item.HomeCandidates[index].TotalRows > 0;
            var hasValueAway = item.Totals.Count > index && item.AwayCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.Range0_1HomeAway:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalBetween0and1Percent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalBetween0and1Percent) : (byte?)null;
                    detailHomeType = BgStatisticsDetailType.Range__InRange0_1__Home;
                    detailAwayType = BgStatisticsDetailType.Range__InRange0_1__Away;
                    break;
                case BgBetTypeComponent.Range2_3HomeAway:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].TotalBetween2and3Percent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].TotalBetween2and3Percent) : (byte?)null;
                    detailHomeType = BgStatisticsDetailType.Range__InRange2_3__Home;
                    detailAwayType = BgStatisticsDetailType.Range__InRange2_3__Away;
                    break;
                case BgBetTypeComponent.Range4PlusHomeAway:
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].Total4PlusPercent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].Total4PlusPercent) : (byte?)null;
                    detailHomeType = BgStatisticsDetailType.Range__InRange4Pplus__Home;
                    detailAwayType = BgStatisticsDetailType.Range__InRange4Pplus__Away;
                    break;
            }
            var container = new HomeAwayContainer(item.Totals.Count,
                hasValueHome ? item.HomeCandidates[index].TotalRows : (int?)null,
                hasValueAway ? item.AwayCandidates[index].TotalRows : (int?)null,
                detailHomeType,
                detailAwayType,
                homePercent,
                awayPercent,
                item.HomeCandidates[index].Filter,
                item.AwayCandidates[index].Filter);
            return container;
        }
        #endregion

        #region GetHomeAwayBothScoreRepo
        private static HomeAwayContainer GetHomeAwayYesNoRepo(BgYesNoHomeAwayBetData item, int index, BgBetTypeComponent type)
        {
            var detailHomeType = BgStatisticsDetailType.FullTime__HomeWin__HomeWin;
            var detailAwayType = BgStatisticsDetailType.FullTime__HomeWin__AwayLoose;
            var homePercent = (byte?)0;
            var awayPercent = (byte?)0;
            var hasValueHome = item.Totals.Count > index && item.HomeCandidates[index].TotalRows > 0;
            var hasValueAway = item.Totals.Count > index && item.AwayCandidates[index].TotalRows > 0;
            switch(type)
            {
                case BgBetTypeComponent.BothScoreYes:
                case BgBetTypeComponent.WinOrDraw:
                case BgBetTypeComponent.HomeAwayNoDraw:
                case BgBetTypeComponent.DrawOrLose:
                    switch (type)
                    {
                        case BgBetTypeComponent.BothScoreYes:
                            detailHomeType = BgStatisticsDetailType.BothScore__Yes__Home;
                            detailAwayType = BgStatisticsDetailType.BothScore__Yes__Away;
                            break;
                        case BgBetTypeComponent.WinOrDraw:
                            detailHomeType = BgStatisticsDetailType.WinDrawLose__WinDraw__HomeWinDraw;
                            detailAwayType = BgStatisticsDetailType.WinDrawLose__WinDraw__AwayDrawLose;
                            break;
                        case BgBetTypeComponent.HomeAwayNoDraw:
                            detailHomeType = BgStatisticsDetailType.WinDrawLose__NoDraw__Home;
                            detailAwayType = BgStatisticsDetailType.WinDrawLose__NoDraw__Away;
                            break;
                        case BgBetTypeComponent.DrawOrLose:
                            detailHomeType = BgStatisticsDetailType.WinDrawLose__DrawLose__Home;
                            detailAwayType = BgStatisticsDetailType.WinDrawLose__DrawLose__Away;
                            break;  
                    }
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].YesPercent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].YesPercent) : (byte?)null;
                    break;
                case BgBetTypeComponent.BothScoreNo:
                    detailHomeType = BgStatisticsDetailType.BothScore__No__Home;
                    detailAwayType = BgStatisticsDetailType.BothScore__No__Away;
                    homePercent = hasValueHome ? Decimal.ToByte(item.HomeCandidates[index].NoPercent) : (byte?)null;
                    awayPercent = hasValueAway ? Decimal.ToByte(item.AwayCandidates[index].NoPercent) : (byte?)null;
                    break;
            }
            var container = new HomeAwayContainer(item.Totals.Count,
                hasValueHome ? item.HomeCandidates[index].TotalRows : (int?)null,
                hasValueAway ? item.AwayCandidates[index].TotalRows : (int?)null,
                detailHomeType,
                detailAwayType,
                homePercent,
                awayPercent,
                item.HomeCandidates[index].Filter,
                item.AwayCandidates[index].Filter);
            return container;
        }
        #endregion
        internal static H2HContainer GetStatisticsH2HRepo<T>(T data, int index, BgBetTypeComponent type) where T : IBetDataH2HStatistics
        {
            H2HContainer container = null;
            if (data is BgFullTimeH2HBetData)
            {
                container = GetH2HFullTimeRepo(data as BgFullTimeH2HBetData, index, type);
            }
            else if (data is BgOverUnderH2HBetData)
            {
                container = GetH2HOverUnderRepo(data as BgOverUnderH2HBetData, index, type);
            }
            else if (data is BgRangeH2HBetData)
            {
                container = GetH2HRangeRepo(data as BgRangeH2HBetData, index, type);
            }
            else if (data is BgYesNoH2HBetData)
            {
                container = GetH2HYesNoRepo(data as BgYesNoH2HBetData, index, type);
            }
            return container;
        }

        #region GetH2HFullTimeRepo
        private static H2HContainer GetH2HFullTimeRepo(BgFullTimeH2HBetData item, int index, BgBetTypeComponent type)
        {
            var percent = (byte?)0;
            var detailType = BgStatisticsDetailType.FullTime__HomeWin__H2H;
            var hasValue = item.H2HCandidates.Count > index && item.H2HCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.FullTimeHomeWinAwayLose:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalIndexPercentW) : (byte?)null;
                    detailType = BgStatisticsDetailType.FullTime__HomeWin__H2H;
                    break;
                case BgBetTypeComponent.FullTimeDraw:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalIndexPercentD) : (byte?)null;
                    detailType = BgStatisticsDetailType.FullTime__Draw__H2H;
                    break;
                case BgBetTypeComponent.FullTimeAwayWinHomeLose:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalIndexPercentL) : (byte?)null;
                    detailType = BgStatisticsDetailType.FullTime__AwayWin__H2H;
                    break;

            }
            var container = new H2HContainer(item.H2HCandidates.Count,
                hasValue ? item.H2HCandidates[index].TotalRows : (int?)null,
                detailType,
                percent,
                item.H2HCandidates[index].Filter);
            return container;
        }
        #endregion

        #region GetH2HOverUnderRepo
        private static H2HContainer GetH2HOverUnderRepo(BgOverUnderH2HBetData item, int index, BgBetTypeComponent type)
        {
            var percent = (byte?)0;
            var detailType = BgStatisticsDetailType.FullTime__HomeWin__H2H;
            var hasValue = item.H2HCandidates.Count > index && item.H2HCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.Over3_5HomeAway:
                case BgBetTypeComponent.Over2_5HomeAway:
                case BgBetTypeComponent.Over1_5HomeAway:
                    switch (type)
                    {
                        case BgBetTypeComponent.Over3_5HomeAway:
                            detailType = BgStatisticsDetailType.OverUnder__Over3_5__H2H;
                            break;
                        case BgBetTypeComponent.Over2_5HomeAway:
                            detailType = BgStatisticsDetailType.OverUnder__Over2_5__H2H;
                            break;
                        case BgBetTypeComponent.Over1_5HomeAway:
                            detailType = BgStatisticsDetailType.OverUnder__Over1_5__H2H;
                            break;
                    }
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalOverPercent) : (byte?)null;
                    break;
                case BgBetTypeComponent.Under3_5HomeAway:
                case BgBetTypeComponent.Under2_5HomeAway:
                case BgBetTypeComponent.Under1_5HomeAway:
                    switch (type)
                    {
                        case BgBetTypeComponent.Under3_5HomeAway:
                            detailType = BgStatisticsDetailType.OverUnder__Under3_5__H2H;
                            break;
                        case BgBetTypeComponent.Under2_5HomeAway:
                            detailType = BgStatisticsDetailType.OverUnder__Under2_5__H2H;
                            break;
                        case BgBetTypeComponent.Under1_5HomeAway:
                            detailType = BgStatisticsDetailType.OverUnder__Under1_5__H2H;
                            break;
                    }
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalUnderPercent) : (byte?)null;
                    break;
            }
            var container = new H2HContainer(item.H2HCandidates.Count,
                hasValue ? item.H2HCandidates[index].TotalRows : (int?)null,
                detailType,
                percent,
                item.H2HCandidates[index].Filter);
            return container;
        }
        #endregion

        #region GetH2HRangeRepo
        private static H2HContainer GetH2HRangeRepo(BgRangeH2HBetData item, int index, BgBetTypeComponent type)
        {
            var percent = (byte?)0;
            var detailType = BgStatisticsDetailType.FullTime__HomeWin__H2H;
            var hasValue = item.H2HCandidates.Count > index && item.H2HCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.Range0_1HomeAway:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalBetween0and1Percent) : (byte?)null;
                    detailType = BgStatisticsDetailType.Range__InRange0_1__H2H;
                    break;
                case BgBetTypeComponent.Range2_3HomeAway:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].TotalBetween2and3Percent) : (byte?)null;
                    detailType = BgStatisticsDetailType.Range__InRange2_3__H2H;
                    break;
                case BgBetTypeComponent.Range4PlusHomeAway:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].Total4PlusPercent) : (byte?)null;
                    detailType = BgStatisticsDetailType.Range__InRange4Pplus__H2H;
                    break;
            }
            var container = new H2HContainer(item.H2HCandidates.Count,
                hasValue ? item.H2HCandidates[index].TotalRows : (int?)null,
                detailType,
                percent,
                item.H2HCandidates[index].Filter);
            return container;
        }
        #endregion

        #region GetH2HYesNoRepo
        private static H2HContainer GetH2HYesNoRepo(BgYesNoH2HBetData item, int index, BgBetTypeComponent type)
        {
            var percent = (byte?)0;
            var detailType = BgStatisticsDetailType.FullTime__HomeWin__H2H;
            var hasValue = item.H2HCandidates.Count > index && item.H2HCandidates[index].TotalRows > 0;
            switch (type)
            {
                case BgBetTypeComponent.BothScoreYes:
                case BgBetTypeComponent.WinOrDraw:
                case BgBetTypeComponent.HomeAwayNoDraw:
                case BgBetTypeComponent.DrawOrLose:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].YesPercent) : (byte?)null;
                    switch (type)
                    {
                        case BgBetTypeComponent.BothScoreYes:
                            detailType = BgStatisticsDetailType.BothScore__Yes__H2H;
                            break;
                        case BgBetTypeComponent.WinOrDraw:
                            detailType = BgStatisticsDetailType.WinDrawLose__WinDraw__H2H;
                            break;
                        case BgBetTypeComponent.HomeAwayNoDraw:
                            detailType = BgStatisticsDetailType.WinDrawLose__NoDraw__H2H;
                            break;
                        case BgBetTypeComponent.DrawOrLose:
                            detailType = BgStatisticsDetailType.WinDrawLose__DrawLose__H2H;
                            break;
                    }
                    break;
                case BgBetTypeComponent.BothScoreNo:
                    percent = hasValue ? Decimal.ToByte(item.H2HCandidates[index].NoPercent) : (byte?)null;
                    detailType = BgStatisticsDetailType.BothScore__No__H2H;
                    break;
            }
            var container = new H2HContainer(item.H2HCandidates.Count,
                hasValue ? item.H2HCandidates[index].TotalRows : (int?)null,
                detailType,
                percent,
                item.H2HCandidates[index].Filter);
            return container;
        }
        #endregion
    }
}
