﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData;

namespace BgData.Generators
{
    internal class H2HContainer
    {
        public int Count { get; }
        public int? TotalRows { get; }
        public BgStatisticsDetailType Type { get; }
        public int? Percent { get; }
        public BgTeamsFilter Filter { get; }
        public H2HContainer(int count, 
            int? totalRows, 
            BgStatisticsDetailType type, 
            int? percent, 
            BgTeamsFilter filter)
        {
            Count = count;
            TotalRows = totalRows;
            Type = type;
            Percent = percent;
            Filter = filter;
        }
    }
}
