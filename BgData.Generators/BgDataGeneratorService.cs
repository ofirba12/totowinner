﻿using log4net;
using System;
using System.ServiceProcess;
using System.Timers;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgData.Generators
{
    public partial class BgDataGeneratorService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Timer timerDataGenerator = new Timer();
        private static Timer timerWinnerGenerator = new Timer();

        public BgDataGeneratorService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
#if DEBUG
                var execute = true;
                GoldserveGenerator.Prerequisite(GoalServeSportType.Soccer, execute);
                //GoldserveGenerator.Prerequisite(GoalServeSportType.Basketball, execute);
                //GoldserveGenerator.Prerequisite(GoalServeSportType.Tennis, execute);
                //GoldserveGenerator.Prerequisite(GoalServeSportType.Handball, execute);
                //GoldserveGenerator.Prerequisite(GoalServeSportType.Football, execute);
                //GoldserveGenerator.Prerequisite(GoalServeSportType.Baseball, execute);
                var goldserveExecutions = GoldserveGenerator.Executes();
                BgDataGenerator.Executes();
#else
                var execute = false;
                GoldserveGenerator.Prerequisite(GoalServeSportType.Soccer, execute);
                GoldserveGenerator.Prerequisite(GoalServeSportType.Basketball, execute);
                GoldserveGenerator.Prerequisite(GoalServeSportType.Tennis, execute);
                GoldserveGenerator.Prerequisite(GoalServeSportType.Handball, execute);
                GoldserveGenerator.Prerequisite(GoalServeSportType.Football, execute);
                GoldserveGenerator.Prerequisite(GoalServeSportType.Baseball, execute);
                timerDataGenerator.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
                timerDataGenerator.Enabled = true;
                timerDataGenerator.Elapsed += DataGenerationExecution;
                timerWinnerGenerator.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
                timerWinnerGenerator.Enabled = true;
                timerWinnerGenerator.Elapsed += WinnerGenerationExecution;
#endif
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to parse categories", ex);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine($"Parsing process has failed with unexpected error {Environment.NewLine}{ex}");
            }
            finally
            {
                DataGenerationExecution(null, null);
                Console.WriteLine("Press any key to close");
                Console.ReadLine();
            }
        }
        private static void DataGenerationExecution(object sender, ElapsedEventArgs e)
        {
            timerDataGenerator.Enabled = false;
            try
            {
                var goldserveExecutions = GoldserveGenerator.Executes();
                if (goldserveExecutions > 0)
                {
                    var systemRepository = BgSystemServices.Instance.GetBgSystemRepository();
                    BgSystemServices.Instance.SetSystemField(BgSystemField.GoalserveFinishGeneration, DateTime.Now);

                    ManualGenerator.Executes();
                    BgDataGenerator.Executes();
                    BgDataWatcherAndAutoIgnore.Executes();
                    BgMatchStatisticsGenerator.Executes();
                }
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                timerDataGenerator.Enabled = true;
            }
        }
        private static void WinnerGenerationExecution(object sender, ElapsedEventArgs e)
        {
            timerWinnerGenerator.Enabled = false;
            try
            {
                WinnerDataGenerator.Executes();
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                timerWinnerGenerator.Enabled = true;
            }
        }

        protected override void OnStop()
        {
            try
            {
                _log.Info("Generation service stopped");
                timerDataGenerator.Stop();
                timerDataGenerator.Close();
                timerWinnerGenerator.Stop();
                timerWinnerGenerator.Close();
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop genration service", ex);
            }

        }
    }
}
