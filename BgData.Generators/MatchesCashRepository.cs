﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgData.Generators
{
    internal class MatchesCashRepository
    {
        private BgDataAnalysisServices analysisServices = BgDataAnalysisServices.Instance;
        private Dictionary<int, List<BgMatchForAnalysis>> collection;
        private Dictionary<int, int> teamsMatchCounter;
        private Dictionary<int, DateTime> lastEodFetchedData;
        private Dictionary<long, OddsOnly> matchesOdds;

        private ILog _log { get; }

        internal MatchesCashRepository(ILog log)
        {
            this.collection = new Dictionary<int, List<BgMatchForAnalysis>>();
            this.lastEodFetchedData = new Dictionary<int, DateTime>();
            this.matchesOdds = new Dictionary<long, OddsOnly>();
            this.teamsMatchCounter = new Dictionary<int, int>();
            _log = log;

        }
        #region Maintain
        public void Maintain()
        {
            var teamIds = lastEodFetchedData.Where(i => i.Value < DateTime.Now).Select(i => i.Key).ToList();
            foreach (var teamId in teamIds)
            {
                foreach (var match in this.collection[teamId])
                    TryClear(match.MatchId, match.HomeId, match.AwayId);
                this.collection.Remove(teamId);
                this.teamsMatchCounter.Remove(teamId);
                this.lastEodFetchedData.Remove(teamId);
                _log.Info($"teamId removed: {teamId}");
            }
        }
        #endregion

        #region GetMatches
        public List<BgMatchForAnalysis> GetMatches(int teamId)
        {
            TryClear(teamId);
            if (!collection.ContainsKey(teamId))
            {
                var teamCollection = analysisServices.GetTeamMatchesForAnalysis(teamId)
                                .OrderByDescending(m => m.GameStart)
                                .ToList();
                collection.Add(teamId, teamCollection);
                if (teamCollection.Count > 0)
                {
                    if (!this.teamsMatchCounter.ContainsKey(teamId))
                        this.teamsMatchCounter.Add(teamId, teamCollection.Count);
                    lastEodFetchedData.Add(teamId, DateTime.Now.Date.AddDays(1).AddTicks(-1));
                }
            }
            return collection[teamId];
        }
        #endregion

        #region MatchesCounterChanged
        public bool MatchesCounterChanged(int teamAId, int teamBId)
        {
            var result = true;
            if (this.teamsMatchCounter.ContainsKey(teamAId) &&
                this.collection.ContainsKey(teamAId) &&
                this.collection[teamAId].Count == this.teamsMatchCounter[teamAId])
            {
                result = false;
            }
            else if (this.teamsMatchCounter.ContainsKey(teamBId) &&
                this.collection.ContainsKey(teamBId) &&
                this.collection[teamBId].Count == this.teamsMatchCounter[teamBId])
            {
                result = false;
            }
            return result;
        }
        #endregion

        #region TeamsCounter
        public int TeamsCollectionCounter()
        {
            return this.collection.Keys.Count;
        }
        #endregion

        #region TeamsMatchCounter
        public int TeamsMatchCounter()
        {
            return this.teamsMatchCounter.Keys.Count;
        }
        #endregion
        #region LastEodFetchedDataCounter
        public int LastEodFetchedDataCounter()
        {
            return this.lastEodFetchedData.Keys.Count;
        }
        #endregion
        #region MatchesOddsCounter
        public int MatchesOddsCounter()
        {
            return this.matchesOdds.Keys.Count;
        }
        #endregion

        #region TryClear
        private void TryClear(int teamId)
        {
            if (this.lastEodFetchedData.ContainsKey(teamId) && this.collection.ContainsKey(teamId))
            {
                if (DateTime.Now > this.lastEodFetchedData[teamId])
                {
                    this.collection.Remove(teamId);
                    this.lastEodFetchedData.Remove(teamId);
                    this.teamsMatchCounter.Remove(teamId);
                    _log.Info($"teamId removed {teamId}");
                }
            }
        }
        private void TryClear(long matchId, int teamAId, int teamBId)
        {
            if (this.matchesOdds.ContainsKey(matchId))
            {
                if (this.lastEodFetchedData.ContainsKey(teamAId) && DateTime.Now > this.lastEodFetchedData[teamAId])
                {
                    this.matchesOdds.Remove(matchId);
                    _log.Info($"matchId removed {matchId} from Odds, as HomeId EOD fetched time passed");
                }
                else if (this.lastEodFetchedData.ContainsKey(teamBId) && DateTime.Now > this.lastEodFetchedData[teamBId])
                {
                    this.matchesOdds.Remove(matchId);
                    _log.Info($"matchId removed {matchId} from Odds, as AwayId EOD fetched time passed");
                }
            }
        }
        #endregion

        #region OddsChanged
        internal bool OddsChanged(long matchId, int homeId, int awayId, OddsOnly oddsOnly)
        {
            TryClear(matchId, homeId, awayId);
            var result = true;
            if (!this.matchesOdds.ContainsKey(matchId))
                this.matchesOdds.Add(matchId, oddsOnly);
            else if (this.matchesOdds[matchId].Equals(oddsOnly))
                result = false;
            return result;
        }
        #endregion

        #region Recalc
        internal BgStatisticsRecalcRepository Recalc(long matchId, int homeId, int awayId, OddsOnly oddsOnly)
        {
            var recalc = false;
            var oddsChanged = false;
            var matchCounterChange = false;
            if (this.collection.ContainsKey(homeId) && this.collection[homeId].Count > 0 &&
                this.collection.ContainsKey(awayId) && this.collection[awayId].Count > 0)
            {
                oddsChanged = OddsChanged(matchId, homeId, awayId, oddsOnly);
                matchCounterChange = MatchesCounterChanged(homeId, awayId);
                recalc = oddsChanged || matchCounterChange;
            }

            var repo = new BgStatisticsRecalcRepository(recalc,
                oddsChanged,
                matchCounterChange,
                this.lastEodFetchedData.ContainsKey(homeId)
                    ? this.lastEodFetchedData[homeId]
                    : default(DateTime?),
                this.lastEodFetchedData.ContainsKey(awayId)
                    ? this.lastEodFetchedData[awayId]
                    : default(DateTime?),
                this.matchesOdds.ContainsKey(matchId)
                    ? this.matchesOdds[matchId]
                    : null);
            return repo;
        }
        #endregion
    }
}
