﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgData.Generators
{
    internal static class BgDataWatcherAndAutoIgnore
    {
        private static readonly ILog _log = LogManager.GetLogger("BgDataWatcher");
        private static readonly Dictionary<BgSport, List<int>> specialAutoIngoreCountries = new Dictionary<BgSport, List<int>>()
        { 
            { BgSport.Soccer, new List<int>(){ 165, 350 }   },
            { BgSport.Basketball, new List<int>(){ 298 }    }
        };
        internal static void Executes()
        {
            try
            {
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgDataWatcherStartAnalysis, DateTime.Now);
                var sports = new List<BgSport>() { BgSport.Soccer, BgSport.Basketball };
                var bgAnalysisMatches = new List<BgTeamAnalysis>();
                foreach (var sport in sports)
                {
                    var teams = BgDataAnalysisServices.Instance.GetBgTeamAnalysis(sport);
                    foreach (var team in teams)
                    {
                        var bgTeamAnalysisRepo = PrepareTeamAnalysis(team.TeamId);
                        if (bgTeamAnalysisRepo != null)
                        {
                            bgAnalysisMatches.Add(bgTeamAnalysisRepo.WatchFindings);
                            if (bgTeamAnalysisRepo.IgnoreSpecialFindings.Count > 0)
                            {
                                CreateManualWithIgnore(bgTeamAnalysisRepo);
                            }
                        }
                    }
                }
                BgDataPersistanceServices.Instance.MergeBgTeamsWatcher(bgAnalysisMatches);
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgDataWatcherFinishAnalysis, DateTime.Now);
            }
        }

        private static void CreateManualWithIgnore(BgTeamAnalysisRepository bgTeamAnalysisRepo)
        {
            _log.Debug($"Found {bgTeamAnalysisRepo.IgnoreSpecialFindings.Count} ignore matches:");
            //bgTeamAnalysisRepo.IgnoreSpecialFindings.ForEach(x => _log.Debug(x.ToString()));

            foreach (var match in bgTeamAnalysisRepo.IgnoreSpecialFindings)
            {
                try
                {
                    var matchToUpsert = new ManualMatch(match, true);
                    var manualMatch = ManualProviderServices.Instance.GetMatch(match.CountryId.Value, match.LeagueId, match.HomeId, match.AwayId, match.GameStart);
                    if (manualMatch != null)
                        _log.Debug($"Match already exists matchId={manualMatch.MatchId} manualLatsUpdate={manualMatch.LastUpdate} countryId {match.CountryId}, leagueId {match.LeagueId}, homeId {match.HomeId}, awayId {match.AwayId}, gameTime {match.GameStart}");
                    else
                    {
                        ManualProviderServices.Instance.AddOrUpdateMatch(matchToUpsert);
                        _log.Debug($"Created with Ignore: {match.ToString()}");
                    }
                }
                catch (Exception ex)
                {
                    _log.Fatal(ex);
                }
            }
        }

        private static BgTeamAnalysisRepository PrepareTeamAnalysis(int teamId)
        {
            try
            {
                var matches = BgDataAnalysisServices.Instance.GetTeamMatchesForAnalysis(teamId);
                var totalShows = matches.Count();
                var missingResult = matches.Where(m => m.MissingScores || m.MissingQuarters).Count();
                var missingOdds = matches.Where(m => m.MissingOdds).Count();
                var findings = new BgTeamAnalysis(teamId, totalShows, missingOdds, missingResult);
                var ignoreSpecialFindings = matches
                    .Where(m => m.Sport == BgSport.Soccer && 
                            m.Source != BgSourceProvider.Manual &&
                            m.CountryId.HasValue && specialAutoIngoreCountries[m.Sport].Contains(m.CountryId.Value) &&
                            (m.MissingOdds || m.MissingScores)
                            )
                    .ToList();
                var result = new BgTeamAnalysisRepository(findings, ignoreSpecialFindings);
                return result;
            }
            catch(Exception ex)
            {
                _log.Error($"An error occurred trying to prepare team analysis for team {teamId}", ex);
            }
            return null;
        }
    }
}