﻿using log4net;
using System;
using System.Collections.Generic;
using TotoWinner.Data.BgBettingData;

namespace BgData.Generators
{
    public class BgGoalServeExecution
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private GoalServeSportType Sport;
        private GoalServeMatchDayFetch DayFetchType;
        private List<BgBettingDataFetchTime> Schedulers;
        public BgGoalServeRepository PrevExecutionRepository { get; private set; }
        public  BgGoalServeFetchRequest Request { get; private set; }
        public BgGoalServeExecution(GoalServeSportType sport, GoalServeMatchDayFetch dDay)
        {
            this.Sport = sport;
            this.DayFetchType = dDay;
            this.Schedulers = new List<BgBettingDataFetchTime>();
            this.PrevExecutionRepository = null;
            switch (this.DayFetchType)
            {
                case GoalServeMatchDayFetch.Today:
                    for (var h = 0; h <= 23; h++)
                        this.Schedulers.Add(new BgBettingDataFetchTime(h, 0));
                    break;
                case GoalServeMatchDayFetch.D_minus_1:
                    this.Schedulers.Add(new BgBettingDataFetchTime(0, 0));
                    this.Schedulers.Add(new BgBettingDataFetchTime(6, 0));
                    this.Schedulers.Add(new BgBettingDataFetchTime(18, 0));
                    break;
                case GoalServeMatchDayFetch.D_plus_1:
                    for (var h = 0; h <= 23; h++)
                        this.Schedulers.Add(new BgBettingDataFetchTime(h, 0));
                    break;
                case GoalServeMatchDayFetch.D_plus_2:
                    for (var h = 0; h <= 23; h++)
                        this.Schedulers.Add(new BgBettingDataFetchTime(h, 0));
                    break;
            }
        }
        public bool CanExecute()
        {
            var currentTime = new BgBettingDataFetchTime(DateTime.Now.Hour, DateTime.Now.Minute);
            if (this.Schedulers.Contains(currentTime))
                return true;
            return false;
        }
        public void Execute()
        {
            try
            {
                this.Request = new BgGoalServeFetchRequest(this.Sport,
                    this.DayFetchType,
                    GoalServeUrlType.Matches,
                    this.PrevExecutionRepository);
                var executionRepo = GoldserveGenerator.GenerateSportData(this.Request);
                this.PrevExecutionRepository = executionRepo ?? this.PrevExecutionRepository;
            }
            catch(Exception ex)
            {
                _log.Fatal($"An error occured while executing", ex);
                throw new Exception($"An error occured while executing", ex);
            }
        }

    }
}
