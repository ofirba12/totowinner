﻿using System;
using TotoWinner.Data.BgBettingData;

namespace BgData.Generators
{
    internal class ExecutionType :IEquatable<ExecutionType>
    {
        public GoalServeSportType Sport { get; }
        public GoalServeMatchDayFetch DayFetchType { get; }

        public ExecutionType(GoalServeSportType sport, GoalServeMatchDayFetch dDay)
        {
            this.Sport = sport;
            this.DayFetchType = dDay;
        }

        public bool Equals(ExecutionType other)
        {
            return this.Sport == other.Sport && this.DayFetchType == other.DayFetchType;
        }
        public override int GetHashCode()
        {
            return this.Sport.GetHashCode() + this.DayFetchType.GetHashCode();
        }
    }
}
