﻿using System;
using System.Collections.Generic;
using TotoWinner.Data.BgBettingData;

namespace BgData.Generators
{
    public class WinnerDataExecution
    {
        private List<BgBettingDataFetchTime> schedulers;
        public WinnerDataExecution()
        {
            schedulers = new List<BgBettingDataFetchTime>()
            {
                new BgBettingDataFetchTime(2, 30),
                new BgBettingDataFetchTime(4, 30),
                new BgBettingDataFetchTime(6, 30),
                new BgBettingDataFetchTime(8, 30),
                new BgBettingDataFetchTime(10, 30),
                new BgBettingDataFetchTime(12, 30),
                new BgBettingDataFetchTime(14, 30),
                new BgBettingDataFetchTime(16, 30),
                new BgBettingDataFetchTime(18, 30),
                new BgBettingDataFetchTime(20, 30),
                new BgBettingDataFetchTime(22, 30),
                new BgBettingDataFetchTime(0, 30)
            };
        }
        public bool CanExecute()
        {
            var currentTime = new BgBettingDataFetchTime(DateTime.Now.Hour, DateTime.Now.Minute);
            if (this.schedulers.Contains(currentTime))
                return true;
            return false;
        }
    }
}
