﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgData.Generators
{
    internal static class WinnerDataGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger("WinnerDataGenerator");
        private static WinnerDataExecution executions = new WinnerDataExecution();
        private static BgDataWinnerPersistanceServices persistance = BgDataWinnerPersistanceServices.Instance;
        private static BettingTipsServices winnerSrv = BettingTipsServices.Instance;
        internal static void Executes()
        {
            var canExecute = executions.CanExecute();
            try
            {
                if (!canExecute)
                    return;
                var systemRepository = BgSystemServices.Instance.GetBgSystemRepository();
                BgSystemServices.Instance.SetSystemField(BgSystemField.WinnerDataStartGeneration, DateTime.Now);
                var winnerLineDates = new List<DateTime>() { DateTime.Today, DateTime.Today.AddDays(1), DateTime.Today.AddDays(2), DateTime.Today.AddDays(3) };
                foreach (var winnerLineDate in winnerLineDates)
                {
                    try
                    {
                        FetchAndSetWinnerData(winnerLineDate);
                    }
                    catch (Exception ex)
                    {
                        _log.Error($"Winner Data for [{winnerLineDate}] was not fetched", ex);
                    }
                }
            }
            catch (Exception ex) 
            {
                _log.Error($"Failed to execute winner data generator", ex);
            }
            finally
            {
                if (canExecute)
                    BgSystemServices.Instance.SetSystemField(BgSystemField.WinnerDataFinishGeneration, DateTime.Now);
            }
        }

        private static void FetchAndSetWinnerData(DateTime winnerLineDate)
        {
            try
            {
                var winnerLineJson = winnerSrv.GetWinnerLineResponse(winnerLineDate, false);
                var marketData = WebApiInvoker.JsonDeserializeDynamic(winnerLineJson);
                var dRounds = marketData["rounds"];
                var feed = new List<BgWinnerFeed>();
                var update = DateTime.Now;
                var sportsToParse = new List<SportIds>() { SportIds.Soccer, SportIds.Basketball, SportIds.Tennis, SportIds.Handball, SportIds.Baseball, SportIds.Football };
                foreach (var dRound in dRounds)
                {
                    var programId = BettingTipsServicesHelpers.DynamicResolver<long>(dRound, null, "id", ResolveEnum.LONG);
                    var repository = winnerSrv.ParseWinnerLineResponse(marketData, programId, winnerLineDate, false, sportsToParse);
                    var feedData = (repository.Events as Dictionary<int, BettingTipsParentEvent>)
                        .Values.ToList()
                        .Where(m => Enum.TryParse<BetTypes>(m.Bet.BetTypeId.ToString(), out var betType))
                        .ToList()
                        .ConvertAll<BgWinnerFeed>(m =>
                                    new BgWinnerFeed(
                                    m.EventId,
                                    m.RoundId,
                                    m.SportId,
                                    m.TotoId,
                                    m.HomeId,
                                    m.HomeTeamName,
                                    m.GuestId,
                                    m.GuestTeamName,
                                    (BetTypes)m.Bet.BetTypeId,
                                    m.Place,
                                    m.Bet.Name,
                                    m.Time,
                                    m.Status,
                                    m.Bet.Home != -1 ? m.Bet.Home : (decimal?)null,
                                    m.Bet.Draw != -1 ? m.Bet.Draw : (decimal?)null,
                                    m.Bet.Away != -1 ? m.Bet.Away : (decimal?)null,
                                    m.Country,
                                    m.LeagueName,
                                    update)
                        );
                    feed.AddRange(feedData);
                }
                persistance.WinnerFeedMerge(feed);
                GenerateMapper(feed);
            }
            catch(Exception ex)
            {
                throw new Exception($"and error occurred trying to fetch winner data for {winnerLineDate}", ex);
            }
        }

        private static void GenerateMapper(List<BgWinnerFeed> feed)
        {
            var currentMapper = persistance.GetWinnerMapper()
                .ToDictionary(m => m.WinnerId, m => m);
            var mapper = new Dictionary<int, BgWinnerMapper>();
            feed.ForEach(f =>
            {
                if (!mapper.ContainsKey(f.HomeId) && !currentMapper.ContainsKey(f.HomeId))
                    mapper.Add(f.HomeId, new BgWinnerMapper(f.SportId, f.HomeId, f.HomeTeam, null, null));
                if (!mapper.ContainsKey(f.GuestId) && !currentMapper.ContainsKey(f.GuestId))
                    mapper.Add(f.GuestId, new BgWinnerMapper(f.SportId, f.GuestId, f.GuestTeam, null, null));
            });
            if (mapper.Values.Count > 0)
            {
                persistance.MergeMappers(mapper.Values.ToList());
            }
        }
    }
}
