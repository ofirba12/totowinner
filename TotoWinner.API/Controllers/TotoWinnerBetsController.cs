﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.API
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/TotoWinnerBets")]
    public class TotoWinnerBetsController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region CalculateBets
        [Route("CalculateBets")]
        [HttpPost]
        public IHttpActionResult CalculateBets([FromBody] BetsFormCreateRequest request)
        {
            try
            {
                var calcService = CalculationServices.Instance;
                var seResults = calcService.CalculateSystem(request);
                //This section should be removed, but I leave it for future usage; GenerateOutputFile should be sent false
                var urlToFile = string.Empty; //Do not generate more than 3000 bet count, client validation
                if (request.GenerateOutputFile && seResults.ResultsStatistics.Found <= 3000)
                {
                    var filename = $"/Output/{DateTime.Now.ToString("ddMMyyyy.HHmmss.fff")}_טופס_מצומצם.xlsx";
                    urlToFile = $"{HttpContext.Current.Request.ApplicationPath}{filename}";
                    var fullFilename = HttpContext.Current.Server.MapPath($"~{filename}");
                    var excelTemplatePath = HttpContext.Current.Server.MapPath($"~/Content/ExcelTemplate/OutputTemplate.xlsx");
                    var filePath = calcService.ResultsToExcel(seResults, request.CleanOnDiff, fullFilename, excelTemplatePath);
                }

                BetsCalculationResultsResponse response = new BetsCalculationResultsResponse(seResults, urlToFile);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to calc bets.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region ResultsToExcel
        [Route("ResultsToExcel")]
        [HttpPost]
        public IHttpActionResult ResultsToExcel([FromBody] ResultsToExcelRequest request)
        {
            ResultsToExcelResponse response = null;
            try
            {
                var calcService = CalculationServices.Instance;
                var urlToExcelFile = string.Empty;
                var urlToPlayerFile = string.Empty;
                //Do not generate more than 3000 bet count, client validation
                if (request.SeResults.ResultsStatistics.Found <= 3000)
                {
                    var filenamePrefix = $"{DateTime.Now.ToString("ddMMyyyy.HHmmss.fff")}";

                    var excelFilename = $"/Output/{filenamePrefix}_טופס_מצומצם.xlsx";
                    urlToExcelFile = $"{HttpContext.Current.Request.ApplicationPath}{excelFilename}";
                    var excelFullPath = HttpContext.Current.Server.MapPath($"~{excelFilename}");
                    var excelTemplatePath = HttpContext.Current.Server.MapPath($"~/Content/ExcelTemplate/OutputTemplate.xlsx");
                    var excelFilePath = calcService.ResultsToExcel(request.SeResults, request.CleanOneDiff, excelFullPath, excelTemplatePath);

                    var playerFilename = $"/Output/{filenamePrefix}_טופס_ניגון.txt";
                    urlToPlayerFile = $"{HttpContext.Current.Request.ApplicationPath}{playerFilename}";
                    var playerFullPath = HttpContext.Current.Server.MapPath($"~{playerFilename}");
                    calcService.GeneratePlayerFile(request.SeResults, request.CleanOneDiff, playerFullPath);
                }

                response = new ResultsToExcelResponse(urlToExcelFile, urlToPlayerFile, true, null);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to create excel from results.", ex);
                response = new ResultsToExcelResponse(null, null, false, ex.ToString());
                return Ok(response);
            }
        }
        #endregion

        #region SaveShrinkSystem
        [Route("SaveShrinkSystem")]
        [HttpPost]
        public IHttpActionResult SaveShrinkSystem([FromBody] ShrinkSystemPayload payload)
        {
            try
            {
                var services = SystemsServices.Instance;
                var key = services.SaveSystem(payload.UniqueId, payload.Name, payload);
                SaveShrinkSystemResponse response = new SaveShrinkSystemResponse(key);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to save shrink system payload.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region LoadShrinkSystem
        [Route("LoadShrinkSystem")]
        [HttpPost]
        public IHttpActionResult LoadShrinkSystem([FromBody] LoadShrinkSystemRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                var data = services.LoadSystem<ShrinkSystemPayload>(request.SystemId);
                ShrinkSystemLoadResponse response = new ShrinkSystemLoadResponse(data);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to load shrink system data with id {request.SystemId}.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region LoadSystemForOptimization
        [Route("LoadSystemForOptimization")]
        [HttpPost]
        public IHttpActionResult LoadSystemForOptimization([FromBody] LoadShrinkSystemRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                var shrinkSystem = services.LoadSystemForOptimizationV2(request.SystemId);
                ShrinkSystemLoadResponse response = new ShrinkSystemLoadResponse(shrinkSystem);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to load shrink system data for optimization with id {request.SystemId}.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region DeleteSystem
        [Route("DeleteSystem")]
        [HttpPost]
        public IHttpActionResult DeleteSystem([FromBody] LoadShrinkSystemRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                services.DeleteSystem(request.SystemId);
                return Ok(true);
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to delete system.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region GetAllSystems
        [Route("GetAllSystems")]
        [HttpPost]
        public IHttpActionResult GetAllSystems([FromBody] GetAllSystemsRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                var data = services.GetAllSystemsKeys(request.type);
                var response = new GetAllSystemsResponse(data);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get all systems of type {request.type}.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

    }
}