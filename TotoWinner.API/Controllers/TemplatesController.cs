﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Algo;
using TotoWinner.Data;
using TotoWinner.Services;
using TotoWinner.Services.Algo;

namespace TotoWinner.API
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/Templates")]
    public class TemplatesController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region GetAllTemplates
        [Route("GetAllTemplates")]
        [HttpPost]
        public IHttpActionResult GetAllTemplates()
        {
            try
            {
                var services = TemplatesServices.Instance;
                var data = services.GetAllTemplate();
                var response = new GetAllTemplatesResponse(data);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get all templates", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region AddTemplate
        [Route("AddTemplate")]
        [HttpPost]
        public IHttpActionResult AddTemplate([FromBody] TemplateRequest request)
        {
            try
            {
                var services = TemplatesServices.Instance;
                var templateId = services.AddTemplate(request.TemplateInfo);
                var newTemplate = new Template(templateId,
                    request.TemplateInfo.Type,
                    request.TemplateInfo.Name,
                    request.TemplateInfo.Value);
                var response = new TemplateAddResponse(newTemplate);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to add template request: {request.ToString()}", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region DeleteTemplate
        [Route("DeleteTemplate")]
        [HttpPost]
        public IHttpActionResult DeleteTemplate([FromBody] TemplateRequest request)
        {
            try
            {
                var services = TemplatesServices.Instance;
                services.DeleteTemplate(request.TemplateInfo);
                var response = new TemplateDeleteResponse(request.TemplateInfo.Id);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to delete system request: {request.ToString()}.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region WatchTemplates
        [Route("WatchTemplates")]
        [HttpPost]
        public IHttpActionResult WatchTemplates([FromBody] WatchTemplatesRequest request)
        {
            try
            {
                var services = TemplatesServices.Instance;
                var games = new Dictionary<int, GameWithValidations>();
                foreach (var rates in request.Rates)
                {
                    var game = new GameWithValidations(rates.GameIndex, "", rates.Rate1, rates.RateX, rates.Rate2, "", "", true, null);
                    games.Add(rates.GameIndex, game);
                }
                var program = new ProgramWithGames(1,1,DateTime.Today,games);
                var result = new Dictionary<int, WatchTemplatesBetsResult>();
                foreach (var template in request.Templates)
                {
                    try
                    {
                        TemplatesServices.Instance.TemplateWatcher(template, program, result);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"An error occurred watching template with id {template.Id}, {template.Name}, {template.Value}", ex);
                    }
                }
                var response = new WatchTemplatesResponse(result);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to add template request: {request.ToString()}", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
//var result = new Dictionary<int, WatchTemplatesBetsResult>();
//var baseBet1 = new Dictionary<short, Bet>()
//{
//    { 1, new Bet(1,true, false, false) },
//    { 2, new Bet(2,false, true, false) },
//    { 3, new Bet(3,false, false, true) },
//    { 4, new Bet(4,true, false, false) },
//    { 5, new Bet(5,false, true, false) },
//    { 6, new Bet(6,false, false, true) },
//    { 7, new Bet(7,true, false, false) },
//    { 8, new Bet(8,false, true, false) },
//    { 9, new Bet(9,false, false, true) },
//    { 10, new Bet(10,true, false, false) },
//    { 11, new Bet(11,false, true, false) },
//    { 12, new Bet(12,false, false, true) },
//    { 13, new Bet(13,true, false, false) },
//    { 14, new Bet(14,false, true, false) },
//    { 15, new Bet(15,false, false, true) },
//    { 16, new Bet(16,false, false, true) }
//};
//var baseBet2 = new Dictionary<short, Bet>()
//{
//    { 1, new Bet(1,false, true, false) },
//    { 2, new Bet(2,false, true, false) },
//    { 3, new Bet(3,false, false, true) },
//    { 4, new Bet(4,true, false, false) },
//    { 5, new Bet(5,false, true, false) },
//    { 6, new Bet(6,false, false, true) },
//    { 7, new Bet(7,true, false, false) },
//    { 8, new Bet(8,false, true, false) },
//    { 9, new Bet(9,false, false, true) },
//    { 10, new Bet(10,true, false, false) },
//    { 11, new Bet(11,false, true, false) },
//    { 12, new Bet(12,false, false, true) },
//    { 13, new Bet(13,true, false, false) },
//    { 14, new Bet(14,false, true, false) },
//    { 15, new Bet(15,false, false, true) },
//    { 16, new Bet(16,false, false, true) }
//};
//var betsResult1 = new WatchTemplatesBetsResult()
//{
//    BaseBets = baseBet1,
//    Status = TemplateBaseBetsStatus.Success
//};
//result.Add(request.Templates[0].Id, betsResult1);
//var betsResult2 = new WatchTemplatesBetsResult()
//{
//    BaseBets = null,
//    Status = TemplateBaseBetsStatus.NoBaseFound
//};
//result.Add(request.Templates[1].Id, betsResult2);
//var betsResult3 = new WatchTemplatesBetsResult()
//{
//    BaseBets = baseBet2,
//    Status = TemplateBaseBetsStatus.InvalidProgram
//};
//result.Add(29, betsResult3);
//result.Add(request.Templates[1], new Dictionary<short, Bet>());
//var backtesting = new BacktestingAlgo();
//foreach (var template in request.Templates)
//{
//    var analyzeTemplate = TemplatesServices.Instance.AnalyzeTemplate(template.Value);
//    var definitions = TemplatesServices.Instance.CreateBacktestingDefinitions(analyzeTemplate.BankerPattern, 
//        analyzeTemplate.DoublePattern);
//    var res = backtesting.GenerateBaseBets(program, definitions);
//    result.Add(template.Id, new WatchTemplatesBetsResult()
//    {
//        BaseBets = res,
//        Status = TemplateBaseBetsStatus.Success
//    });
//}
