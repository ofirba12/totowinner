﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.API.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/StatisticalData")]
    public class StatisticalDataController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region GetData
        [Route("GetData")]
        [HttpPost]
        public IHttpActionResult GetData([FromBody] GetStatisticalDataRequest request)
        {
            try
            {
                var programSrv = ProgramServices.Instance;
                var staticCalcSrv = StatisticalDataServices.Instance;
                var beginDate = DateTime.Parse($"1 Jan {request.Year}").Date;
                var endDate = DateTime.Parse($"1 Jan {request.Year + 1}").Date.AddDays(-1);
                var programs = programSrv.GetPersistanceProgramsWithGamesData(request.Type, beginDate, endDate)
                    .OrderByDescending(p => p.EndDate).ToList();
                var amount1X2 = new List<BasicData3Response>();
                var amountABC = new List<BasicData3Response>();
                var sequenceLength1X2 = new List<BasicData3Response>();
                var sequenceLengthABC = new List<BasicData3Response>();
                var breaks1X2 = new List<BasicData1Response>();
                var breaksABC = new List<BasicData1Response>();
                var shapes2Data1X2 = new List<Shapes2Data1X2Response>();
                var shapes2DataABC = new List<Shapes2DataABCResponse>();
                var shapesGroups2Data1X2 = new List<GroupsBasicDataResponse>();
                var shapesGroups2DataABC = new List<GroupsBasicDataResponse>();
                var shapes3Data1X2 = new List<Shapes3Data1X2Response>();
                var shapes3DataABC = new List<Shapes3DataABCResponse>();
                var shapesGroups3Data1X2 = new List<GroupsBasicDataResponse>();
                var shapesGroups3DataABC = new List<GroupsBasicDataResponse>();
                var relationsData = new List<RelationsDataResponse>();

                foreach (var program in programs)
                {
                    if (program.EndDate.Date > DateTime.Today)
                        continue;
                    if (program.Games.Any(g => g.Value.Won == null) ||
                        program.Games.Any(g => g.Value.Validations.Count > 0))
                    {
                        amount1X2.Add(new BasicData3Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        amountABC.Add(new BasicData3Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        sequenceLength1X2.Add(new BasicData3Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        sequenceLengthABC.Add(new BasicData3Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        breaks1X2.Add(new BasicData1Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        breaksABC.Add(new BasicData1Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapes2Data1X2.Add(new Shapes2Data1X2Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapes2DataABC.Add(new Shapes2DataABCResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapesGroups2Data1X2.Add(new GroupsBasicDataResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapesGroups2DataABC.Add(new GroupsBasicDataResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapes3Data1X2.Add(new Shapes3Data1X2Response() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapes3DataABC.Add(new Shapes3DataABCResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapesGroups3Data1X2.Add(new GroupsBasicDataResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        shapesGroups3DataABC.Add(new GroupsBasicDataResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                        relationsData.Add(new RelationsDataResponse() { RoundId = program.Number, ProgramEndDate = program.EndDate });
                    }
                    else
                    {
                        var data = staticCalcSrv.Generate(program);
                        amount1X2.Add(data.Amount1X2.ToBasicData3Response(program));
                        amountABC.Add(data.AmountABC.ToBasicData3Response(program));
                        sequenceLength1X2.Add(data.SequenceLength1X2.ToBasicData3Response(program));
                        sequenceLengthABC.Add(data.SequenceLengthABC.ToBasicData3Response(program));
                        breaks1X2.Add(data.Breaks1X2.ToBasicData1Response(program));
                        breaksABC.Add(data.BreaksABC.ToBasicData1Response(program));
                        shapes2Data1X2.Add(data.Shapes2Data1X2.ToShapes2Data1X2Response(program));
                        shapes2DataABC.Add(data.Shapes2DataABC.ToShapes2DataABCResponse(program));
                        shapesGroups2Data1X2.Add(data.ShapesGroups2Data1X2.ToGroupsBasicDataResponse(program));
                        shapesGroups2DataABC.Add(data.ShapesGroups2DataABC.ToGroupsBasicDataResponse(program));
                        shapes3Data1X2.Add(data.Shapes3Data1X2.ToShapes3Data1X2Response(program));
                        shapes3DataABC.Add(data.Shapes3DataABC.ToShapes3DataABCResponse(program));
                        shapesGroups3Data1X2.Add(data.ShapesGroups3Data1X2.ToGroupsBasicDataResponse(program));
                        shapesGroups3DataABC.Add(data.ShapesGroups3DataABC.ToGroupsBasicDataResponse(program));
                        relationsData.Add(data.RelationsData.ToRelationsDataResponse(program));
                    }
                }
                var response = new GetStatisticalDataResponse(amount1X2, amountABC, sequenceLength1X2, sequenceLengthABC,
                    breaks1X2, breaksABC, shapes2Data1X2, shapes2DataABC, shapesGroups2Data1X2, shapesGroups2DataABC,
                    shapes3Data1X2, shapes3DataABC, shapesGroups3Data1X2, shapesGroups3DataABC,
                    relationsData);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to get hitorical data for year {request?.Year} and type {request?.Type}";
                _log.Fatal(error, ex);
                var response = new GetStatisticalDataResponse(error);
                return Ok(response);
            }
        }
        #endregion
    }
    //var amount1X2 = new List<BasicData3>()
    //{
    //    new BasicData3() { ProgramEndDate = DateTime.Today,               RoundId= 191501, Amount1 = 10, Amount2 = 5, Amount3 = 1},
    //    new BasicData3() { ProgramEndDate = DateTime.Today.AddDays(-7),   RoundId= 191502, Amount1 = 5, Amount2 = 9, Amount3 = 2}
    //};
    //var amountABC = new List<BasicData3>()
    //{
    //    new BasicData3() { ProgramEndDate = DateTime.Today,               RoundId= 191501, Amount1 = 11, Amount2 = 4, Amount3 = 1},
    //    new BasicData3() { ProgramEndDate = DateTime.Today.AddDays(-7),   RoundId= 191502, Amount1 = 6, Amount2 = 10, Amount3 = 2}
    //};

}