﻿using log4net;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Data;
using TotoWinner.Data.Optimizer;
using TotoWinner.Services;

namespace TotoWinner.API
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/Optimizer")]
    public class OptimizerController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region GetExecutionStatus
        [Route("GetExecutionStatus")]
        [HttpPost]
        public IHttpActionResult GetExecutionStatus([FromBody] ExecutionOptimizerRequest request)
        {
            try
            {
                var service = ExecutionServices.Instance;
                var state = service.GetExecutionStatus(request.ExecutionId);
                var response = new ExecutionOptimizerResponse(state);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to get execution status for execution {request.ExecutionId}.";
                _log.Fatal(error, ex);
                var response = new ExecutionOptimizerResponse(error);
                return Ok(response);
            }
        }
        #endregion

        #region StartOptimizer
        [Route("StartOptimizer")]
        [HttpPost]
        public IHttpActionResult StartOptimizer([FromBody] StartOptimizerRequest request)
        {
            try
            {
                var service = ExecutionServices.Instance;
                var executionId = service.AddOptimizerExecution(request.Parameters);
                var response = new StartOptimizerResponse(executionId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to start execution for system {request.Parameters.ToString()}.";
                _log.Fatal(error, ex);
                var response = new StartOptimizerResponse(error);
                return Ok(response);
            }
        }
        #endregion

        #region StopOptimizer
        [Route("StopOptimizer")]
        [HttpPost]
        public IHttpActionResult StopOptimizer([FromBody] StopOptimizerRequest request)
        {
            try
            {
                var service = ExecutionServices.Instance;
                service.UpdateExecutionStatus<SearchResults>(request.ExecutionId, SystemType.Optimizer, ExecutionStatus.Stopped, null);
                _log.Info("Optimizer was stopped by client");
                var response = new StopOptimizerResponse();
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to stop execution {request.ExecutionId}.";
                _log.Fatal(error, ex);
                var response = new StartOptimizerResponse(error);
                return Ok(response);
            }
        }
        #endregion

        #region ResultsToExcel
        [Route("ResultsToExcel")]
        [HttpPost]
        public IHttpActionResult ResultsToExcel([FromBody] OptimizerResultsToExcelRequest request)
        {
            ResultsToExcelResponse response = null;
            try
            {
                var systemSrv = SystemsServices.Instance;
                var shrinkSystem = systemSrv.LoadSystem<ShrinkSystemPayload>(request.OptimizerSystemId);
                var cleanOneDiff = shrinkSystem.Definitions.Form.CleanOnDiff;
                var executionSrv = ExecutionServices.Instance;
                var seResults = executionSrv.GetOptimizerExecutionShrinkResult(request.ExecutionId);
                var urlToExcelFile = string.Empty; 
                var urlToPlayerFile = string.Empty;
                //Do not generate more than 3000 bet count, client validation
                if (seResults.ResultsStatistics.Found <= 3000)
                {
                    var calcService = CalculationServices.Instance;
                    var filenamePrefix = $"{DateTime.Now.ToString("ddMMyyyy.HHmmss.fff")}";

                    var excelFilename = $"/Output/{filenamePrefix}_טופס_אופטימיזציה.xlsx";
                    urlToExcelFile = $"{HttpContext.Current.Request.ApplicationPath}{excelFilename}";
                    var excelFullPath = HttpContext.Current.Server.MapPath($"~{excelFilename}");
                    var excelTemplatePath = HttpContext.Current.Server.MapPath($"~/Content/ExcelTemplate/OutputTemplate.xlsx");
                    var excelFilePath = calcService.ResultsToExcel(seResults, cleanOneDiff, excelFullPath, excelTemplatePath);

                    var playerFilename = $"/Output/{filenamePrefix}_טופס_ניגון.txt";
                    urlToPlayerFile = $"{HttpContext.Current.Request.ApplicationPath}{playerFilename}";
                    var playerFullPath = HttpContext.Current.Server.MapPath($"~{playerFilename}");
                    calcService.GeneratePlayerFile(seResults, cleanOneDiff, playerFullPath);

                    //var filename = $"/Output/{DateTime.Now.ToString("ddMMyyyy.HHmmss.fff")}_טופס_אופטימיזציה.xlsx";
                    //urlToFile = $"{HttpContext.Current.Request.ApplicationPath}{filename}";
                    //var fullFilename = HttpContext.Current.Server.MapPath($"~{filename}");
                    //var excelTemplatePath = HttpContext.Current.Server.MapPath($"~/Content/ExcelTemplate/OutputTemplate.xlsx");
                    //var filePath = calcService.ResultsToExcel(seResults, cleanOneDiff, fullFilename, excelTemplatePath);
                }

                response = new ResultsToExcelResponse(urlToExcelFile, urlToPlayerFile, true, null);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to create excel from results.", ex);
                response = new ResultsToExcelResponse(null, null, false, ex.ToString());
                return Ok(response);
            }
        }
        #endregion

        #region SaveOptimizerSystem
        [Route("SaveOptimizerSystem")]
        [HttpPost]
        public IHttpActionResult SaveOptimizerSystem([FromBody] SaveOptimizerSystemRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                var key = services.SaveSystem(request.UniqueId, request.SystemType, request.Name,request);
                var response = new SaveOptimizerSystemResponse(key);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occured trying to save optimizer system form.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region LoadOptimizerSystem
        [Route("LoadOptimizerSystem")]
        [HttpPost]
        public IHttpActionResult LoadOptimizerSystem([FromBody] LoadOptimizerSystemRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                var data = services.LoadSystem<SaveOptimizerSystemRequest>(request.SystemId);
                var shrinkData = services.LoadSystemForOptimizationV2(data.OptimizedSystemId);
                var response = new LoadOptimizerSystemResponse(data, shrinkData.Name);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to load optimizer system data with id {request.SystemId}.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

    }
}