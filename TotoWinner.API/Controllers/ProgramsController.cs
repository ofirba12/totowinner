﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.API.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/Programs")]
    public class ProgramsController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region GetProgram
        [Route("GetProgram")]
        [HttpPost]
        public IHttpActionResult GetProgram([FromBody] ProgramDataRequest request)
        {
            try
            {
                var programService = ProgramServices.Instance;
                var programDataResult = programService.GetProgramData(request.ProgramEndDate, request.ProgramType);
                var response = new ProgramDataResponse(programDataResult.Details, request.ProgramType, programDataResult.FinalResult);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get program {request.ProgramType} | {request.ProgramEndDate}.", ex);
                return Ok(ex.Message);
            }
        }
        #endregion

        #region SyncProgram
        [Route("SyncProgram")]
        [HttpPost]
        public IHttpActionResult SyncProgram([FromBody] ProgramDataSyncRequest request)
        {
            try
            {
                var programService = ProgramServices.Instance;
                var games = programService.GetProgramData(request.ProgramEndDate, request.ProgramType);
                programService.UpsertProgramData(games, request.ProgramType, request.ProgramEndDate, request.TotoRoundId);
                var response = new ResponseBase(true);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to sync program {request.ProgramType} | {request.ProgramEndDate} | {request.TotoRoundId}.", ex);
                var response = new ResponseBase(false, ex.Message);
                return Ok(response);
            }
        }
        #endregion

        #region SetRealTimePipe
        [Route("SetRealTimePipe")]
        [HttpPost]
        public IHttpActionResult SetRealTimePipe([FromBody] RealTimePipeSetRequest request)
        {
            try
            {
                var persistanceService = PersistanceServices.Instance;
                persistanceService.RealTimeRatesPipeUsert(request.ProgramType, request.Rates);
                var response = new ResponseBase(true);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to set real time pipe for {request.ProgramType}.", ex);
                var response = new ResponseBase(false, ex.Message);
                return Ok(response);
            }
        }
        #endregion

        #region GetRealTimePipe
        [Route("GetRealTimePipe")]
        [HttpPost]
        public IHttpActionResult GetRealTimePipe([FromBody] RealTimePipeGetRequest request)
        {
            try
            {
                var persistanceService = PersistanceServices.Instance;
                var rates = persistanceService.GetRealTimeRatesPipe(request.ProgramType);
                var response = new RealTimePipeGetResponse(rates);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get real time pipe for {request.ProgramType}.", ex);
                var response = new ResponseBase(false, ex.Message);
                return Ok(response);
            }
        }
        #endregion
    }
}
