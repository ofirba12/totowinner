﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Data;
using TotoWinner.Data.Optimizer;
using TotoWinner.Services;

namespace TotoWinner.API
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/Backtesting")]
    public class BacktestingController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region RunBacktesting
        [Route("RunBacktesting")]
        [HttpPost]
        public IHttpActionResult RunBacktesting([FromBody] RunBacktestingRequest request)
        {
            try
            {
                var service = ExecutionServices.Instance;
                var executionId = service.AddBacktestingExecution(request.Definitions);
                var response = new RunBacktestingResponse(executionId, ExecutionStatus.Queue);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to run backtesting. {request.ToString()}"; 
                _log.Fatal(error, ex);
                var response = new RunBacktestingResponse(error);
                return Ok(response);
            }
        }
        #endregion

        #region StopBacktesting
        [Route("StopBacktesting")]
        [HttpPost]
        public IHttpActionResult StopBacktesting([FromBody] StopBacktestingRequest request)
        {
            try
            {
                var service = ExecutionServices.Instance;
                service.UpdateExecutionStatus<BacktestingResults>(request.ExecutionId, SystemType.Backtesting, ExecutionStatus.Stopped, null);
                var response = new StopBacktestingResponse();
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to stop backtesting."; //TODO: Add request to sting
                _log.Fatal(error, ex);
                var response = new StopBacktestingResponse(error);
                return Ok(response);
            }
        }
        #endregion

        #region GetExecutionStatus
        [Route("GetExecutionStatus")]
        [HttpPost]
        public IHttpActionResult GetExecutionStatus([FromBody] BacktestingExecutionRequest request)
        {
            try
            {
                var service = ExecutionServices.Instance;
                var results  = service.GetBacktestingResults(request.ExecutionId, request.FromIndex);
                var exeStatus = service.GetBacktestingExecutionStatus(request.ExecutionId);
                var response = new BacktestingExecutionResponse(request.ExecutionId, exeStatus.Status,
                    results.Programs,
                    results.Totals,
                    results.Summary);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to get execution status for {request?.ExecutionId}"; 
                _log.Fatal(error, ex);
                var response = new BacktestingExecutionResponse(error);
                return Ok(response);
            }
        }
        #endregion

        #region GetPastProgram
        [Route("GetPastProgram")]
        [HttpPost]
        public IHttpActionResult GetPastProgram([FromBody] GetPastProgramRequest request)
        {
            try
            {
                var executionSrv = ExecutionServices.Instance;
                var programSrv = ProgramServices.Instance;
                var program = programSrv.GetPersistanceProgramsWithGamesData(new List<int>() { request.ProgramId }).First();
                var baseBetData = executionSrv.FetchBacktestingBaseBets(request.ExecutionId, program.Id, program);
                var winBetData = programSrv.PrepareProgramBetsCollection(program);
                var response = new GetPastProgramResponse(request.ProgramId, program.Number, baseBetData, winBetData);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"An error occured trying to get past program base+win bets for execution {request?.ExecutionId}, programId={request?.ProgramId}";
                _log.Fatal(error, ex);
                var response = new GetPastProgramResponse(error);
                return Ok(response);
            }
        }
        #endregion
        
    }
}