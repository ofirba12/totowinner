﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.API.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/DispatchForm")]
    public class DispatchFormController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region GetAllSystemsForUnion
        [Route("GetAllSystemsForUnion")]
        [HttpPost]
        public IHttpActionResult GetAllSystemsForUnion([FromBody] GetAllSystemsForUnionRequest request)
        {
            try
            {
                var services = SystemsServices.Instance;
                var shrinkSystems = services.GetAllSystemsForUnion(SystemType.Shrink, request.Type);
                var optimizerSystems = services.GetAllSystemsForUnion(SystemType.Optimizer, request.Type);
                shrinkSystems.AddRange(optimizerSystems);
                var response = new GetAllSystemsWithFinalResultResponse(shrinkSystems);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get all systems with final result of type {request.Type}.", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region UnionSystems
        [Route("UnionSystems")]
        [HttpPost]
        public IHttpActionResult UnionSystems([FromBody] UnionSystemsRequest request)
        {
            UnionSystemsResponse response = null;
            try
            {
                var calcService = CalculationServices.Instance;
                var systemsService = SystemsServices.Instance;
                var unionResult = systemsService.UnionSystems(request.SystemIds);
                var serializedResults = JsonConvert.SerializeObject(unionResult.UnionResult);
                var unionResultId = PersistanceServices.Instance.UnionSystemResultsInsert(string.Join(",", request.SystemIds), serializedResults);
                response = new UnionSystemsResponse(unionResultId, unionResult.CounterBeforeUnion, unionResult.CounterAfterUnion,
                    unionResult.Counters, unionResult.CountersPercent);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to union systems {string.Join(",", request.SystemIds)}.", ex);
                response = new UnionSystemsResponse(ex.ToString());
                return Ok(response);
            }
        }
        #endregion

        #region GenerateFormToDownload
        [Route("GenerateFormToDownload")]
        [HttpPost]
        public IHttpActionResult GenerateFormToDownload([FromBody] GenerateFormToDownloadRequest request)
        {
            GenerateFormToDownloadResponse response = null;
            try
            {
                var calcService = CalculationServices.Instance;
                var urlToFile = string.Empty;
                var rawUnionResults = PersistanceServices.Instance.UnionSystemResultsGetResults(request.UnionResultId);
                var unionResults = JsonConvert.DeserializeObject<IEnumerable<CombinedResult>>(rawUnionResults);
                //if (request.SeResults.ResultsStatistics.Found <= 3000)
                var filenamePrefix = $"{DateTime.Now.ToString("ddMMyyyy.HHmmss.fff")}";
                switch (request.FormType)
                {
                    case FormTypeGeneration.Excel:
                        var excelFilename = $"/Output/{filenamePrefix}_טופס_מצומצם.xlsx";
                        urlToFile = $"{HttpContext.Current.Request.ApplicationPath}{excelFilename}";
                        var excelFullPath = HttpContext.Current.Server.MapPath($"~{excelFilename}");
                        var excelTemplatePath = HttpContext.Current.Server.MapPath($"~/Content/ExcelTemplate/OutputTemplate.xlsx");
                        calcService.GenerateExcelFile(unionResults, excelFullPath, excelTemplatePath);
                        break;
                    case FormTypeGeneration.Player:
                        var playerFilename = $"/Output/{filenamePrefix}_טופס_ניגון.txt";
                        urlToFile = $"{HttpContext.Current.Request.ApplicationPath}{playerFilename}";
                        var playerFullPath = HttpContext.Current.Server.MapPath($"~{playerFilename}");
                        calcService.GeneratePlayerFile(unionResults, playerFullPath);
                        break;
                    case FormTypeGeneration.SMART:
                        var zipFilename = $"/Output/{filenamePrefix}_SMART_{unionResults.Count()}.zip";
                        urlToFile = $"{HttpContext.Current.Request.ApplicationPath}{zipFilename}";
                        var zipFullPath = HttpContext.Current.Server.MapPath($"~{zipFilename}");
                        var srtFilename = $"{filenamePrefix}_SMART_{unionResults.Count()}.STR";
                        var srtFilePath = $"/Output/{srtFilename}";
                        var srtFullPath = HttpContext.Current.Server.MapPath($"~{srtFilePath}");
                        calcService.GenerateSmartFile(unionResults, srtFullPath);
                        Utilities.CompressWithZipFile(zipFullPath, srtFullPath, srtFilename);
                        PersistanceServices.Instance.UnionSystemResultsUpdate(request.UnionResultId, urlToFile);
                        break;
                }

                response = new GenerateFormToDownloadResponse(urlToFile, true);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to create result to player form for persistance union system id {request.UnionResultId}.", ex);
                response = new GenerateFormToDownloadResponse(ex.ToString());
                return Ok(response);
            }
        }
        #endregion

        #region GetSmartFiles
        [Route("GetSmartFiles")]
        [HttpPost]
        public IHttpActionResult GetSmartFiles()
        {
            GetSmartFilesResponse response = null;
            try
            {
                var files = PersistanceServices.Instance.GetSmartFilesToDownload();

                response = new GetSmartFilesResponse(files);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get all smart files.", ex);
                response = new GetSmartFilesResponse(ex.ToString());
                return Ok(response);
            }
        }
        #endregion

        #region DeleteSmartFile
        [Route("DeleteSmartFile")]
        [HttpPost]
        public IHttpActionResult DeleteSmartFile([FromBody] DeleteSmartFileRequest request)
        {
            ResponseBase response = null;
            try
            {
                PersistanceServices.Instance.UnionSystemResultsUpdate(request.Id, null);
                response = new ResponseBase(true);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to delete smart file with id {request.Id}.", ex);
                response = new ResponseBase(false, ex.ToString());
                return Ok(response);
            }
        }
        #endregion
    }
}
