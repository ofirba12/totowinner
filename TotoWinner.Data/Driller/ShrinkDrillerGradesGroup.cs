﻿using System;

namespace TotoWinner.Data
{
    public class ShrinkDrillerGradesGroup
    {
        public MinMax StrongRange { get; }
        public MinMax MediumRange { get; }
        public MinMax LowRange { get; }

        public ShrinkDrillerGradesGroup(MinMax strong, MinMax medium, MinMax low)
        {
            try
            {
                this.StrongRange = strong;
                this.MediumRange = medium;
                this.LowRange = low;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ShrinkDrillerGradesGroup)}", ex);
            }
        }
    }
}
