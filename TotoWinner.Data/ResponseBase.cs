﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class ResponseBase
    {
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }

        public ResponseBase(bool isSuccess, string error = null)
        {
            this.IsSuccess = isSuccess;
            this.ErrorMessage = error;
        }

    }
}
