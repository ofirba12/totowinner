﻿namespace TotoWinner.Data
{
    public class BacktestingExecutionData
    {
        public int ExecutionId { get; set; }
        public ExecutionStatus Status { get; set; }
        public BacktestingDefinitions Definitions { get; set; }

        public override string ToString()
        {
            return $"ExecutionId: {ExecutionId} | Status: {Status} | Definitions: [{Definitions}]";
        }
    }
}
