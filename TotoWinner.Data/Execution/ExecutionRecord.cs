﻿namespace TotoWinner.Data
{
    public class ExecutionRecord
    {
        public int ExecutionId { get; set; }
        public SystemType SystemType { get; set; }
        public ExecutionStatus Status { get; set; }
        public string Parameters { get; set; }
        public string Results { get; set; }
    }
}
