﻿namespace TotoWinner.Data
{
    public interface IMinIterationResult
    {
        int IterationIndex { get; }
        SearchResults ShrinkResult { get; }
    }
}
