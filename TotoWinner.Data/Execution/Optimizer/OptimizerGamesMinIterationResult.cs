﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class OptimizerGamesMinIterationResult : IMinIterationResult
    {
        public int IterationIndex { get; set; }
        public int GameIndex { get; set; }
        public SearchResults ShrinkResult { get; set; }
        public string Str1X2 { get; set; }

        public override string ToString()
        {
            return $"IterationIndex: {IterationIndex} | GameIndex: {GameIndex} | Str1X2: {Str1X2} | Result: {ShrinkResult.ResultsStatistics.Found}/{ShrinkResult.FinalResults.Count()}";
        }
    }
}
