﻿namespace TotoWinner.Data
{
    public class OptimizerConditionIteration
    {
        public OptimizerCondition BeforeCondition { get; private set; }
        public OptimizerCondition AfterCondition { get; private set; }
        public OptimizerAdvancedCondition BeforeAdvancedCondition { get; private set; }
        public OptimizerAdvancedCondition AfterAdvancedCondition { get; private set; }

        public OptimizerConditionIteration(OptimizerCondition before, OptimizerCondition after)
        {
            this.BeforeCondition = before;
            this.AfterCondition = after;
            this.BeforeAdvancedCondition = null;
            this.AfterAdvancedCondition = null;
        }
        public OptimizerConditionIteration(OptimizerAdvancedCondition before, OptimizerAdvancedCondition after)
        {
            this.BeforeCondition = null;
            this.AfterCondition = null;
            this.BeforeAdvancedCondition = before;
            this.AfterAdvancedCondition = after;
        }
    }
}
