﻿namespace TotoWinner.Data
{
    public class ExecutionOptimizerParameters //: IExecutionParameters
    {
        public int OptimizedSystemId { get; private set; }
        public int TargetBetsCounter { get; private set; }
        public int TargetAdditionalConditionCounter { get; private set; }

        public ExecutionOptimizerParameters(int optimizedSystemId, int targetBetsCounter, int targetAdditionalConditionCounter)
        {
            this.OptimizedSystemId = optimizedSystemId;
            this.TargetBetsCounter = targetBetsCounter;
            this.TargetAdditionalConditionCounter = targetAdditionalConditionCounter;
        }
        public override string ToString()
        {
            return $"OptimizedSystemId: {OptimizedSystemId} | TargetBetsCounter: {TargetBetsCounter} | TargetAdditionalConditionCounter: {TargetAdditionalConditionCounter}";
        }
    }
}
