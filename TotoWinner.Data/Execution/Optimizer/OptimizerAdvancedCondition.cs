﻿using System;

namespace TotoWinner.Data
{
    public class OptimizerAdvancedCondition
    {
        public AdvancedConditionType Type { get; private set; }
        public int Index { get; private set; }
        public MinMax MinMax { get; private set; }

        public OptimizerAdvancedCondition(AdvancedConditionType type, int index, decimal min, decimal max)
        {
            try
            {
                if (index == 0)
                    throw new Exception("Advanced Condition index could not be zero, must be >0");
                this.Type = type;
                this.Index = index;
                this.MinMax = new MinMax(min, max);
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(OptimizerAdvancedCondition)} with combination type {type}, index {index}", ex);
            }
        }

    }
}
