﻿namespace TotoWinner.Data
{
    public class OptimizerExecutionData
    {
        public int ExecutionId { get; set; }
        public ExecutionStatus Status { get; set; }
        public ExecutionOptimizerParameters Parameters { get; set; }

        public override string ToString()
        {
            return $"ExecutionId: {ExecutionId} | Status: {Status} | Parameters: [{Parameters}]";
        }
    }
}
