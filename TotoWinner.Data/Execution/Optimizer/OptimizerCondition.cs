﻿using System;

namespace TotoWinner.Data
{
    public class OptimizerCondition
    {
        public ConditionType Type { get; private set; }
        public ConditionBetTitleEnum BetTitle { get; private set; }
        public MinMax MinMax { get; private set; }

        public OptimizerCondition(ConditionType type, ConditionBetTitleEnum betTitle, decimal min, decimal max)
        {
            try
            {
                this.Type = type;
                this.BetTitle = betTitle;
                this.MinMax = new MinMax(min, max);
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(OptimizerCondition)} with combination type {type}", ex);
            }
        }
    }
}
