﻿using System.Linq;

namespace TotoWinner.Data
{
    public class OptimizerConditionsMinIterationResult : IMinIterationResult
    {
        public int IterationIndex { get; private set; }
        public SearchResults ShrinkResult { get; private set; }

        public ConditionType ConditionType { get; private set; }
        public ConditionBetTitleEnum BetTitle { get; private set; }

        public AdvancedConditionType AdvancedConditionType { get; private set; }
        public int TypeIndex { get; private set; }
        public OptimizerConditionsMinIterationResult(int iterationIndex, SearchResults results, ConditionType type, ConditionBetTitleEnum betTitle)
        {
            this.IterationIndex = iterationIndex;
            this.ShrinkResult = results;
            this.ConditionType = type;
            this.BetTitle = betTitle;
            this.AdvancedConditionType = AdvancedConditionType.None;
        }
        public OptimizerConditionsMinIterationResult(int iterationIndex, SearchResults results, AdvancedConditionType type, int typeIndex)
        {
            this.IterationIndex = iterationIndex;
            this.ShrinkResult = results;
            this.AdvancedConditionType = type;
            this.TypeIndex = typeIndex;
            this.ConditionType = ConditionType.None;
        }
        public override string ToString()
        {
            if (ConditionType != ConditionType.None)
                return $"IterationIndex: {IterationIndex} | ConditionType: {ConditionType} | BetTitle: {BetTitle} | Result: {ShrinkResult.ResultsStatistics.Found}/{ShrinkResult.FinalResults.Count()}";
            else
                return $"IterationIndex: {IterationIndex} | AdvancedConditionType: {AdvancedConditionType} | TypeIndex: {TypeIndex} | Result: {ShrinkResult.ResultsStatistics.Found}/{ShrinkResult.FinalResults.Count()}";
        }

    }
}
