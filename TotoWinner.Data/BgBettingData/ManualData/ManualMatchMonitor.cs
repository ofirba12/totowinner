﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class ManualMatchMonitor
    {
        public string Country { get; }
        public string League { get; }
        [ColumnStyle(Width = 75)] 
        public string Status { get; }
        [ColumnStyle(Width = 50)]
        public string Time { get; }
        public string Home { get; }
        public string Away { get; }
        [ColumnStyle(Width = 75)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Width = 75)]
        public int? HomeScore { get; }
        [ColumnStyle(Width = 75)]
        public int? AwayScore { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ4 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ4 { get; }
        [ColumnStyle(Width = 150)]
        public DateTime LastUpdate { get; }
        public int? CountryId { get; }
        public int? LeagueId { get; }
        public bool IsCup { get; }
        public int? HomeId { get; }
        public int? AwayId { get; }
        public BgSport Sport { get; }
        [ColumnStyle(Hide = true)]
        public DateTime GameStart { get; }
        [ColumnStyle(Hide = true)] 
        public bool Ignore { get; }
        public long MatchId { get; }
        public ManualMatchMonitor(BgSport sport,
            string status,
            DateTime lastUpdate,
            int? countryId,
            string country,
            string league,
            int? leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int? homeId,
            string away,
            int? awayId,
            bool ignore,
            long matchId,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            BasketballQuatersScores basketballQuatersScores = null)
        {
            this.Sport = sport;
            this.Status = status;
            this.LastUpdate = lastUpdate;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.Time = gameStart.ToString("HH:mm");
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.Ignore = ignore;
            this.MatchId = matchId;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.HQ1 = basketballQuatersScores?.LocalTeamQ1Score;
            this.AQ1 = basketballQuatersScores?.AwayTeamQ1Score;
            this.HQ2 = basketballQuatersScores?.LocalTeamQ2Score;
            this.AQ2 = basketballQuatersScores?.AwayTeamQ2Score;
            this.HQ3 = basketballQuatersScores?.LocalTeamQ3Score;
            this.AQ3 = basketballQuatersScores?.AwayTeamQ3Score;
            this.HQ4 = basketballQuatersScores?.LocalTeamQ4Score;
            this.AQ4 = basketballQuatersScores?.AwayTeamQ4Score;
        }
    }
}