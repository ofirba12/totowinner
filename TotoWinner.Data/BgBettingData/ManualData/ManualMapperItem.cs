﻿namespace TotoWinner.Data.BgBettingData
{
    public class ManualMapperItem
    {
        public int? BgId { get; }
        public BgMapperType BgType { get; }
        public string DisplayName { get; }
        public ManualMapperItem(int? bgId,
            BgMapperType type,
            string displayName)
        {
            this.BgId = bgId;
            this.DisplayName = displayName;
            this.BgType = type;
        }        
    }
}