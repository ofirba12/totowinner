﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgManualMapper : IEquatable<BgManualMapper>, IEqualityComparer<BgManualMapper>
    {
        public int BgId { get; }
        public string DisplayName { get; }
        public BgManualMapper(int bgId,
            string displayName)
        {
            this.BgId = bgId;
            this.DisplayName = displayName;
        }

        public bool Equals(BgManualMapper other)
        {
            return this.BgId == other.BgId &&
                this.DisplayName == other.DisplayName;
        }

        public bool Equals(BgManualMapper x, BgManualMapper y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(BgManualMapper obj)
        {
            return obj.BgId.GetHashCode() ^ obj.DisplayName.GetHashCode();
        }
    }
}
