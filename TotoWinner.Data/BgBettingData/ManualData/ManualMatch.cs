﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class ManualMatch : IEquatable<ManualMatch>
    {
        public BgSport Sport { get; private set; }
        public string Status { get; private set; }
        public int? CountryId { get; private set; }
        public string Country { get; private set; }
        public string League { get; private set; }
        public int? LeagueId { get; private set; }
        public bool IsCup { get; private set; }
        public DateTime GameStart { get; private set; }
        public string Home { get; private set; }
        public int? HomeId { get; private set; }
        public string Away { get; private set; }
        public int? AwayId { get; private set; }
        public OddsAndScores OddsAndScores { get; private set; }
        public DateTime LastUpdate { get; private set; }
        public bool Ignore { get; set; }
        public long? MatchId { get; set; }
        public ManualMatch(BgSport sport,
            string status,
            int? countryId,
            string country,
            string league,
            int? leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int? homeId,
            string away,
            int? awayId,
            bool ignore,
            long? matchId,
            OddsAndScores oddsAndScores,
            DateTime lastUpdate)
        {
            this.Sport = sport;
            this.Status = status;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.LastUpdate = lastUpdate;
            this.OddsAndScores = oddsAndScores;
            this.Ignore = ignore;
            this.MatchId = matchId;
        }
        public ManualMatch(ManualMatchMonitor monitorMatch)
        {
            this.Sport = monitorMatch.Sport;
            this.Status = monitorMatch.Status;
            this.CountryId = monitorMatch.CountryId;
            this.Country = monitorMatch.Country;
            this.League = monitorMatch.League;
            this.LeagueId = monitorMatch.LeagueId;
            this.IsCup = monitorMatch.IsCup;
            this.GameStart = monitorMatch.GameStart;
            this.Home = monitorMatch.Home;
            this.HomeId = monitorMatch.HomeId;
            this.Away = monitorMatch.Away;
            this.AwayId = monitorMatch.AwayId;
            this.LastUpdate = DateTime.Now;
            this.OddsAndScores = new OddsAndScores(monitorMatch.HomeOdd,
                monitorMatch.DrawOdd,
                monitorMatch.AwayOdd,
                monitorMatch.HomeScore,
                monitorMatch.AwayScore,
                this.Sport ==  BgSport.Basketball && monitorMatch.HQ1.HasValue
                ? new BasketballQuatersScores(monitorMatch.HQ1.Value,
                    monitorMatch.HQ2.Value,
                    monitorMatch.HQ3.Value,
                    monitorMatch.HQ4.Value,
                    monitorMatch.AQ1.Value,
                    monitorMatch.AQ2.Value,
                    monitorMatch.AQ3.Value,
                    monitorMatch.AQ4.Value
                    )
                : null
                );
            this.Ignore = monitorMatch.Ignore;
            this.MatchId = monitorMatch.MatchId;
        }
        public ManualMatch(MonitorBgMatchTeamAnalysis monitorMatch)
        {
            this.Sport = monitorMatch.Sport;
            this.Status = monitorMatch.Status;
            this.CountryId = monitorMatch.CountryId;
            this.Country = monitorMatch.Country;
            this.League = monitorMatch.League;
            this.LeagueId = monitorMatch.LeagueId;
            this.IsCup = monitorMatch.IsCup;
            this.GameStart = monitorMatch.GameTime;
            this.Home = monitorMatch.Home;
            this.HomeId = monitorMatch.HomeId;
            this.Away = monitorMatch.Away;
            this.AwayId = monitorMatch.AwayId;
            this.LastUpdate = DateTime.Now;
            this.OddsAndScores = new OddsAndScores(monitorMatch.HomeOdd,
                monitorMatch.DrawOdd,
                monitorMatch.AwayOdd,
                monitorMatch.HomeScore,
                monitorMatch.AwayScore,
                this.Sport == BgSport.Basketball && monitorMatch.HQ1.HasValue
                ? new BasketballQuatersScores(monitorMatch.HQ1.Value,
                    monitorMatch.HQ2.Value,
                    monitorMatch.HQ3.Value,
                    monitorMatch.HQ4.Value,
                    monitorMatch.AQ1.Value,
                    monitorMatch.AQ2.Value,
                    monitorMatch.AQ3.Value,
                    monitorMatch.AQ4.Value
                    )
                : null
                );
            this.Ignore = false;
            this.MatchId = monitorMatch.MatchId;
        }

        public ManualMatch(BgMatchForAnalysis match, bool ignore)
        {
            this.Sport = match.Sport;
            this.Status = match.Status;
            this.CountryId = match.CountryId;
            this.Country = match.Country;
            this.League = match.League;
            this.LeagueId = match.LeagueId;
            this.IsCup = match.IsCup;
            this.GameStart = match.GameStart;
            this.Home = match.Home;
            this.HomeId = match.HomeId;
            this.Away = match.Away;
            this.AwayId = match.AwayId;
            this.LastUpdate = DateTime.Now;
            this.OddsAndScores = new OddsAndScores(match.HomeOdd,
                match.DrawOdd,
                match.AwayOdd,
                match.HomeScore,
                match.AwayScore,
                this.Sport == BgSport.Basketball && match.HQ1.HasValue
                ? new BasketballQuatersScores(match.HQ1.Value,
                    match.HQ2.Value,
                    match.HQ3.Value,
                    match.HQ4.Value,
                    match.AQ1.Value,
                    match.AQ2.Value,
                    match.AQ3.Value,
                    match.AQ4.Value
                    )
                : null
                );
            this.Ignore = ignore;
            this.MatchId = match.MatchId;
        }

        public void Update(ManualMatch match)
        {
            this.Sport = match.Sport;
            this.Status = match.Status;
            this.CountryId = match.CountryId;
            this.Country = match.Country;
            this.League = match.League;
            this.LeagueId = match.LeagueId;
            this.IsCup = match.IsCup;
            this.GameStart = match.GameStart;
            this.Home = match.Home;
            this.HomeId = match.HomeId;
            this.Away = match.Away;
            this.AwayId = match.AwayId;
            this.LastUpdate = DateTime.Now;
            this.OddsAndScores = match.OddsAndScores;
            this.Ignore = match.Ignore;
            this.MatchId = match.MatchId;
        }
        public void UpdateCountry(string country, int? countryId)
        {
            this.Country = country;
            this.CountryId = countryId;
            this.LastUpdate = DateTime.Now;
        }
        public void UpdateLeague(string league, int? leagueId)
        {
            this.League = league;
            this.LeagueId = leagueId;
            this.LastUpdate = DateTime.Now;
        }
        public void UpdateHomeTeam(string team, int? teamId)
        {
            this.Home = team;
            this.HomeId = teamId;
            this.LastUpdate = DateTime.Now;
        }
        public void UpdateAwayTeam(string team, int? teamId)
        {
            this.Away = team;
            this.AwayId = teamId;
            this.LastUpdate = DateTime.Now;
        }
        public bool Equals(ManualMatch other)
        {
            return this.MatchId == other.MatchId;
            //return this.Sport == other.Sport
            //    && this.LeagueId == other.LeagueId
            //    && this.HomeId == other.HomeId
            //    && this.AwayId == other.AwayId
            //    && this.GameStart == other.GameStart;
        }
        public override string ToString()
        {
            return $"MatchId:{this.MatchId} Sport:{this.Sport}, LeagueId:{this.LeagueId}, HomeId:{this.HomeId}, AwayId:{this.AwayId}, GameStart:{this.GameStart}";
        }
    }

}
