﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgSystemRepository
    {
        public DateTime? ManualProviderStartGeneration { get; }
        public DateTime? ManualProviderFinishGeneration { get; }
        public DateTime? BgFeedStartGeneration { get; }
        public DateTime? BgFeedFinishGeneration { get; }
        public DateTime? BgDataWatcherStartAnalysis { get; }
        public DateTime? BgDataWatcherFinishAnalysis { get; }

        public BgSystemRepository(DateTime? manualProviderStartGeneration, 
            DateTime? manualProviderFinishGeneration, 
            DateTime? bgFeedStartGeneration, 
            DateTime? bgFeedFinishGeneration,
            DateTime? bgDataWatcherStartAnalysis,
            DateTime? bgDataWatcherFinishAnalysis)        
        {
            this.ManualProviderStartGeneration = manualProviderStartGeneration;
            this.ManualProviderFinishGeneration = manualProviderFinishGeneration;
            this.BgFeedStartGeneration = bgFeedStartGeneration;
            this.BgFeedFinishGeneration = bgFeedFinishGeneration;
            this.BgDataWatcherStartAnalysis = bgDataWatcherStartAnalysis;
            this.BgDataWatcherFinishAnalysis = bgDataWatcherFinishAnalysis;
        }
    }
}
