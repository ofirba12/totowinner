﻿using System;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data.BgBettingData.Goalserve
{
    public class GoalserveMatchFeedMonitor
    {
        public string Country { get; }
        public string League { get; }
        [ColumnStyle(Width = 75)] 
        public string Status { get; }
        [ColumnStyle(Width = 50)]
        public string Time { get; }
        public string Home { get; }
        public string Away { get; }
        [ColumnStyle(Width = 75)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Width = 75)]
        public int? HomeScore { get; }
        [ColumnStyle(Width = 75)]
        public int? AwayScore { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ4 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ4 { get; }
        [ColumnStyle(Width = 150)]
        public DateTime LastUpdate { get; }
        public int MatchId { get; }
        public int? CountryId { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public int HomeId { get; }
        public int AwayId { get; }
        public GoalServeSportType Sport { get; }
        public GoalserveMatchFeedMonitor(GoalServeSportType sport,
            int matchId,
            string status,
            DateTime lastUpdate,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int homeId,
            string away,
            int awayId,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null
            )
        {
            this.Sport = sport;
            this.MatchId = matchId;
            this.Status = status;
            this.LastUpdate = lastUpdate;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.Time = gameStart.ToString("HH:mm");
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
        }
    }
}