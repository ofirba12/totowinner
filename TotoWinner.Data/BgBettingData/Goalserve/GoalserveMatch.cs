﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace TotoWinner.Data.BgBettingData.Goalserve
{
    public class GoalserveMatch : IEquatable<GoalserveMatch>    
    {
        public int SportId { get; }
        public int MatchId { get; }
        public string Status { get; }
        public int? CountryId { get; }
        public string Country { get; }
        public string League { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public DateTime GameStart { get; }
        public string Home { get; }
        public int HomeId { get; }
        public string Away { get; }
        public int AwayId { get; }
        public OddsAndScores OddsAndScores { get; private set; }
        public GoalserveMatch(int sportId,
            int matchId,
            string status,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int homeId,
            string away,
            int awayId,
            OddsAndScores oddsAndScores)
        {
            this.SportId = sportId;
            this.MatchId = matchId;
            this.Status = status;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.OddsAndScores = oddsAndScores;
        }

        public GoalserveMatch(GoalServeSportType sport, IBgGoalServeMatch match, IReadOnlyDictionary<int, BgGoalServeLeague> leagues,
           OddsAndScores oddsAndScores)
        {
            this.SportId = (int)sport;
            this.MatchId = match.MatchId;
            this.CountryId = leagues[match.LeagueId].CountryId;
            this.Country = leagues[match.LeagueId].Country;
            this.League = leagues[match.LeagueId].LeagueName;
            this.LeagueId = match.LeagueId;
            this.IsCup = leagues[match.LeagueId].IsCup;
            var gameTime = TimeSpan.ParseExact(match.MatchTime, "h\\:mm", CultureInfo.CurrentCulture);
            this.GameStart = match.MatchDate.Add(gameTime);
            this.Status = match.Status;
            this.Home = match.LocalTeam;
            this.HomeId = match.LocalTeamId;
            this.Away = match.VisitorTeam;
            this.AwayId = match.VisitorTeamId;
            this.OddsAndScores = oddsAndScores;
        }

        public bool Equals(GoalserveMatch other)
        {
            return this.SportId == other.SportId
                && this.MatchId == other.MatchId
                && this.LeagueId == other.LeagueId
                && this.HomeId == other.HomeId
                && this.AwayId == other.AwayId;
                //&& this.GameStart == other.GameStart;
        }
    }
    
}
