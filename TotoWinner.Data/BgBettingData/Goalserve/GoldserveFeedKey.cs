﻿using System;

namespace TotoWinner.Data.BgBettingData.Goalserve
{
    public class GoldserveFeedKey : IEquatable<GoldserveFeedKey>
    {
        public GoalServeSportType Sport { get; set; }
        public DateTime GameTime { get; set; }
        public int HomeTeamId { get; set; }
        public int AwayTeamId { get; set; }
        public GoldserveFeedKey(GoalServeSportType sport,
            DateTime gameTime,
            int homeId,
            int awayId)
        {
            this.Sport = sport;
            this.GameTime = gameTime;
            this.HomeTeamId = homeId;
            this.AwayTeamId = awayId;
        }

        public bool Equals(GoldserveFeedKey other)
        {
            return this.GameTime.Date == other.GameTime.Date &&
                this.Sport == other.Sport &&    
                    (this.HomeTeamId == other.HomeTeamId && this.AwayTeamId == other.AwayTeamId) ||
                    (this.HomeTeamId == other.AwayTeamId && this.AwayTeamId == other.HomeTeamId);
        }

        public bool Equals(GoldserveFeedKey x, GoldserveFeedKey y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.GameTime.Date.GetHashCode() ^ this.Sport.GetHashCode() ^ (this.HomeTeamId.GetHashCode() + this.AwayTeamId.GetHashCode());
        }
        public override string ToString()
        {
            return $"Sport:{this.Sport}, HomeTeamId:{this.HomeTeamId}, AwayTeamId:{this.AwayTeamId}, GameTime:{this.GameTime}";
        }
    }
}
