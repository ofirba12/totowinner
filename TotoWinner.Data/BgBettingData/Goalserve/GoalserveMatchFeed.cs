﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.Goalserve
{
    public class GoalserveMatchFeed : IEquatable<GoalserveMatchFeed>
    {
        public GoalServeSportType Sport { get; }
        public int MatchId { get; }
        public string Status { get; }
        public int? CountryId { get; }
        public string Country { get; }
        public string League { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public DateTime GameStart { get; }
        public string Home { get; }
        public int HomeId { get; }
        public string Away { get; }
        public int AwayId { get; }
        public decimal? HomeOdd { get; }
        public decimal? DrawOdd { get; }
        public decimal? AwayOdd { get; }
        public int? HomeScore { get; }
        public int? AwayScore { get; }
        public int? HQ1 { get; }
        public int? AQ1 { get; }
        public int? HQ2 { get; }
        public int? AQ2 { get; }
        public int? HQ3 { get; }
        public int? AQ3 { get; }
        public int? HQ4 { get; }
        public int? AQ4 { get; }
        public DateTime LastUpdate { get; }
        public GoalserveMatchFeed(GoalServeSportType sport,
            int matchId,
            string status,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int homeId,
            string away,
            int awayId,
            DateTime lastUpdate,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null)
        {
            this.Sport = sport;
            this.MatchId = matchId;
            this.Status = status;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.LastUpdate = lastUpdate;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
        }

        public bool Equals(GoalserveMatchFeed other)
        {
            return this.Sport == other.Sport
                && this.MatchId == other.MatchId
                && this.LeagueId == other.LeagueId
                && this.HomeId == other.HomeId
                && this.AwayId == other.AwayId;
            //&& this.GameStart == other.GameStart;
        }
        public override string ToString()
        {
            return $"MatchId:{this.MatchId} Sport:{this.Sport}, LeagueId:{this.LeagueId}, HomeId:{this.HomeId}, AwayId:{this.AwayId}, GameStart:{this.GameStart}";
        }
    }

}

