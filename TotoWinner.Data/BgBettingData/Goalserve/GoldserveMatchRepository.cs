﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.Goalserve
{
    public class GoldserveMatchRepository
    {
        public List<GoalserveMatchFeed> Feed { get; }
        public IReadOnlyDictionary<GoldserveFeedKey, List<GoalserveMatchFeed>> Dumplicates { get;}

        public GoldserveMatchRepository(List<GoalserveMatchFeed> feed)
        {
            this.Feed = feed;
            var dumplicates = new Dictionary<GoldserveFeedKey, List<GoalserveMatchFeed>>();
            foreach (var feedItem in feed)
            {
                var key = new GoldserveFeedKey(feedItem.Sport, feedItem.GameStart, feedItem.HomeId, feedItem.AwayId);
                if (!dumplicates.ContainsKey(key))
                    dumplicates.Add(key, new List<GoalserveMatchFeed>());
                dumplicates[key].Add(feedItem);
            }
            this.Dumplicates = dumplicates.Where(x => x.Value.Count > 1)
                .ToDictionary(i => i.Key, i => i.Value);
        }
    }
}
