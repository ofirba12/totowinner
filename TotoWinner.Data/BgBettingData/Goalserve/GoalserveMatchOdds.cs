﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace TotoWinner.Data.BgBettingData.Goalserve
{
    public class GoalserveMatchOdds 
    {
        public int SportId { get; }
        public int MatchId { get; }
        public string Bookmaker { get; }
        public decimal? HomeOdd { get; }
        public decimal? DrawOdd { get; }
        public decimal? AwayOdd { get; }

        public GoalserveMatchOdds(int sportId,
            int matchId,
            string bookmaker,
             decimal? homeOdd,
             decimal? drawOdd,
             decimal? awayOdd)
        {
            this.SportId = sportId;
            this.MatchId = matchId;
            this.Bookmaker = bookmaker;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
        }
    }

}
