﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class MatchKey : IEquatable<MatchKey>
    {
        public int MatchId { get; }
        public int LeagueId { get; }
        public int HomeTeamId { get; }
        public int AwayTeamId { get; }

        public MatchKey(int matchId, int leagueId, int homeTeamId, int awayTeamId)
        {
            this.MatchId = matchId;
            this.LeagueId = leagueId;
            this.HomeTeamId = homeTeamId;
            this.AwayTeamId = awayTeamId;
        }

        public bool Equals(MatchKey other)
        {
            return this.MatchId == other.MatchId
                && this.LeagueId == other.LeagueId
                && this.HomeTeamId == other.HomeTeamId
                && this.AwayTeamId == other.AwayTeamId;
        }
        public override int GetHashCode()
        {
            return this.MatchId.GetHashCode() +
                this.LeagueId.GetHashCode() +
                this.HomeTeamId.GetHashCode() +
                this.AwayTeamId.GetHashCode();
        }
        public override string ToString()
        {
            return $"MatchId={this.MatchId}, LeagueId={this.LeagueId}, HomeTeamId={this.HomeTeamId}, AwayTeamId={this.AwayTeamId}";
        }
    }
}
