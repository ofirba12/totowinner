﻿using System;
using System.IO;
using TotoWinner.Data.BgBettingData.Goalserve;

namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeFetchRequest
    {
        public GoalServeSportType Sport { get; }
        public GoalServeMatchDayFetch Type { get; }
        public GoalServeUrlType UrlType { get; }
        public DateTime Date { get; }
        public bool MatchesExists { get; }
        public int? LeagueId { get; }
        public string Url { get; }
        public string CashFilePath { get; }
        public BgGoalServeRepository PreviousRunRepository { get; }

        public BgGoalServeFetchRequest(GoalServeSportType sport,
            GoalServeMatchDayFetch type,
            GoalServeUrlType urlType,
            BgGoalServeRepository prevRepo) : this(sport, type, urlType, prevRepo, null)
        {}
        public BgGoalServeFetchRequest(GoalServeSportType sport, 
            GoalServeMatchDayFetch type,
            GoalServeUrlType urlType,
            BgGoalServeRepository prevRepo,
            int? leagueId)
        {
            try
            {
                this.Sport = sport;
                this.Type = type;
                this.PreviousRunRepository = prevRepo;
                var urlDateItem = string.Empty;
                switch (type)
                {
                    case GoalServeMatchDayFetch.Today:
                        this.Date = DateTime.Today;
                        urlDateItem = "home";
                        break;
                    case GoalServeMatchDayFetch.D_minus_1:
                        this.Date = DateTime.Today.AddDays(-1);
                        urlDateItem = "d-1";
                        break;
                    case GoalServeMatchDayFetch.D_plus_1:
                        this.Date = DateTime.Today.AddDays(1);
                        urlDateItem = "d1";
                        break;
                    case GoalServeMatchDayFetch.D_plus_2:
                        this.Date = DateTime.Today.AddDays(2);
                        urlDateItem = "d2";
                        break;

                }
                this.UrlType = urlType;
                this.LeagueId = leagueId;
                if (this.UrlType == GoalServeUrlType.Odds && !this.LeagueId.HasValue)
                    throw new Exception($"Please supply leagueId when using url type {this.UrlType}");

                var sportUrlItem = string.Empty;
                switch (sport)
                {
                    case GoalServeSportType.Soccer:
                        if (this.UrlType == GoalServeUrlType.Matches)
                            sportUrlItem = "soccernew";
                        else if (this.UrlType == GoalServeUrlType.Odds)
                            sportUrlItem = "soccer?cat=soccer_10";
                        break;
                    case GoalServeSportType.Basketball:
                        if (this.UrlType == GoalServeUrlType.Matches)
                            sportUrlItem = "bsktbl";
                        else if (this.UrlType == GoalServeUrlType.Odds)
                            sportUrlItem = "soccer?cat=basket_10";
                        break;
                    case GoalServeSportType.Tennis:
                        if (this.UrlType == GoalServeUrlType.Matches)
                            sportUrlItem = "tennis_scores";
                        else if (this.UrlType == GoalServeUrlType.Odds)
                            sportUrlItem = "soccer?cat=tennis_10";
                        break;
                    case GoalServeSportType.Handball:
                        if (this.UrlType == GoalServeUrlType.Matches)
                            sportUrlItem = "handball";
                        else if (this.UrlType == GoalServeUrlType.Odds)
                            sportUrlItem = "soccer?cat=handball_10";
                        break;
                    case GoalServeSportType.Football:
                        if (this.UrlType == GoalServeUrlType.Matches)
                            sportUrlItem = "football";
                        else if (this.UrlType == GoalServeUrlType.Odds)
                            sportUrlItem = "soccer?cat=football_10";
                        break;
                    case GoalServeSportType.Baseball:
                        if (this.UrlType == GoalServeUrlType.Matches)
                            sportUrlItem = "baseball";
                        else if (this.UrlType == GoalServeUrlType.Odds)
                            sportUrlItem = "soccer?cat=baseball_10";
                        break;
                }
                var repDirectory = Path.Combine(BettingTipsConstants.RepositoryPath, $"{this.Date.Day}_{this.Date.Month}_{this.Date.Year}");
                if (!Directory.Exists(repDirectory))
                    Directory.CreateDirectory(repDirectory);
                var tsItem = string.Empty;
                if (this.UrlType == GoalServeUrlType.Odds && this.PreviousRunRepository != null)
                {
                    var key = new SportLeagueKey(this.Sport, this.LeagueId.Value);
                    var tsFromLastRun = this.PreviousRunRepository.OddsFetcherLastUpdateTimestamp.ContainsKey(key)
                        ? this.PreviousRunRepository.OddsFetcherLastUpdateTimestamp[key]
                        : (long?)null;
                    tsItem = tsFromLastRun.HasValue
                        ? $"&ts={tsFromLastRun.Value.ToString()}"
                        : string.Empty;
                }
                switch (this.UrlType)
                {
                    case GoalServeUrlType.Matches:
                        this.Url = $"https://www.goalserve.com/getfeed/bf4b0ff10a454bb6af954861483bc02d/{sportUrlItem}/{urlDateItem}?json=1";
                        this.CashFilePath = Path.Combine(repDirectory, $"GoalServeMatches_{this.Sport}__{this.Date.Day}_{this.Date.Month}_{this.Date.Year}_#T{DateTime.Now.Hour.ToString("D2")}{DateTime.Now.Minute.ToString("D2")}.json");
                        break;
                    case GoalServeUrlType.Odds:
                        this.Url = $"https://www.goalserve.com/getfeed/bf4b0ff10a454bb6af954861483bc02d/getodds/{sportUrlItem}&league={this.LeagueId.Value}&json=1{tsItem}";
                        this.CashFilePath = Path.Combine(repDirectory, $"GoalServeOdds_{this.Sport}_{this.LeagueId.Value}__{this.Date.Day}_{this.Date.Month}_{this.Date.Year}_#T{DateTime.Now.Hour.ToString("D2")}{DateTime.Now.Minute.ToString("D2")}.json");
                        break;
                }
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BgGoalServeFetchRequest)}", ex);
            }
        }
        public override string ToString()
        {
            var str = $"Sport={this.Sport}, Type={this.Type}, UrlType={this.UrlType}, Date={Date.ToString("d")}";
            return str;
        }
    }
}
