﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeRepository
    {
        public IReadOnlyDictionary<MatchKey, IBgGoalServeMatch> Matches { get; }
        public IReadOnlyDictionary<int, List<BgGoalServeBookmaker>> Odds { get; }
        public IReadOnlyDictionary<int, BgGoalServeLeague> Leagues { get; }
        public IReadOnlyDictionary<SportLeagueKey, long> OddsFetcherLastUpdateTimestamp {get;}
        public BgGoalServeRepository(Dictionary<MatchKey, IBgGoalServeMatch> matches, 
            Dictionary<int, List<BgGoalServeBookmaker>> odds, 
            Dictionary<int, BgGoalServeLeague> leagues,
            Dictionary<SportLeagueKey, long> oddsFetcherLastUpdateTimestamp)
        {
            this.Matches = matches;
            this.Odds = odds;
            this.Leagues = leagues;
            this.OddsFetcherLastUpdateTimestamp = oddsFetcherLastUpdateTimestamp;
        }
        public BgGoalServeRepository(Dictionary<MatchKey, IBgGoalServeMatch> matches,
            IReadOnlyDictionary<int, List<BgGoalServeBookmaker>> odds,
            IReadOnlyDictionary<int, BgGoalServeLeague> leagues,
            IReadOnlyDictionary<SportLeagueKey, long> oddsFetcherLastUpdateTimestamp)
        {
            this.Matches = matches;
            this.Odds = odds;
            this.Leagues = leagues;
            this.OddsFetcherLastUpdateTimestamp = oddsFetcherLastUpdateTimestamp;
        }
    }
}
