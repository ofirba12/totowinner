﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeMatchBasketballResult : IBgGoalServeMatchResult
    {
        public string LocalTeamScores { get; }
        public string VisitorTeamScores { get; }

        public BasketballQuatersScores QuartersScores { get; }

        public BgGoalServeMatchBasketballResult(string localTeamScores, string visitorTeamScores, BasketballQuatersScores quatersScores)
        {
            this.LocalTeamScores = string.IsNullOrEmpty(localTeamScores)
                ? null
                : localTeamScores;
            this.VisitorTeamScores = string.IsNullOrEmpty(visitorTeamScores)
                ? null
                : visitorTeamScores;
            this.QuartersScores = quatersScores;
        }
    }
}
