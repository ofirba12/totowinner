﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public interface IBgGoalServeMatch
    {
        string LocalTeam { get; }
        int LocalTeamId { get; }
        string VisitorTeam { get; }
        int VisitorTeamId { get; }
        string Status { get; }
        int LeagueId { get; }
        DateTime MatchDate { get; }
        string MatchTime { get; }
        int MatchId { get; }
        IBgGoalServeMatchResult MatchResult { get; }
    }
}
