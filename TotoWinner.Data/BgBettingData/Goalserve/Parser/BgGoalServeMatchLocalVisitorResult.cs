﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeMatchLocalVisitorResult : IBgGoalServeMatchResult
    {
        public string LocalTeamScores { get; }
        public string VisitorTeamScores { get; }

        public BgGoalServeMatchLocalVisitorResult(string localTeamGoals, string visitorTeamGoals)
        {
            this.LocalTeamScores = localTeamGoals == "?"
                ? null
                : localTeamGoals;
            this.VisitorTeamScores = visitorTeamGoals == "?"
                ? null
                : visitorTeamGoals;
        }
    }
}
