﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class SportLeagueKey : IEquatable<SportLeagueKey>
    {
        public GoalServeSportType Sport { get; }
        public int LeagueId { get; }

        public SportLeagueKey(GoalServeSportType sport, int leagueId)
        {
            this.Sport = sport;
            this.LeagueId = leagueId;
        }

        public bool Equals(SportLeagueKey other)
        {
            return this.Sport == other.Sport && this.LeagueId == other.LeagueId;
        }
        public override int GetHashCode()
        {
            return this.Sport.GetHashCode() + this.LeagueId.GetHashCode();
        }
    }
}
