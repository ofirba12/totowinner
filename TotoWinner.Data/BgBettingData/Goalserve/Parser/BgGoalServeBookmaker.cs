﻿using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeBookmaker
    {
        public string Name { get; }
        public Dictionary<string, decimal> Odds { get; }

        public BgGoalServeBookmaker(string name, Dictionary<string, decimal> odds)
        {
            this.Name = name;
            this.Odds = odds;
        }
        public override string ToString()
        {
            var oddsStr = string.Empty;
            foreach (var odd in this.Odds)
                oddsStr = $"{oddsStr}[{odd.Key} {odd.Value}] ";
            return $"Name={this.Name}, {oddsStr}";
        }
    }
}
