﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeMatch : IBgGoalServeMatch
    {
        public string LocalTeam { get; }
        public int LocalTeamId { get; }
        public string VisitorTeam { get; }
        public int VisitorTeamId { get; }
        public string Status { get; }
        public int LeagueId { get; }
        public DateTime MatchDate { get; }
        public string MatchTime { get; }
        public int MatchId { get; }
        public IBgGoalServeMatchResult MatchResult { get; }

        public BgGoalServeMatch(string localTeam, 
            int localTeamId,
            string visitorTeam, 
            int visitorTeamId,
            string status,
            int leagueId,
            DateTime date,
            string time,
            int matchId,
            IBgGoalServeMatchResult matchResult)
        {
            this.LocalTeam = localTeam;
            this.LocalTeamId = localTeamId;
            this.VisitorTeam = visitorTeam;
            this.VisitorTeamId = visitorTeamId;
            this.Status = status;
            this.LeagueId = leagueId;
            this.MatchDate = date;
            this.MatchTime = time;
            this.MatchId = matchId;
            this.MatchResult = matchResult;
        }
        public override string ToString()
        {
            return $"MatchId={this.MatchId}, LeagueId={this.LeagueId}, LocalTeam={this.LocalTeam}, LocalTeamId={this.LocalTeamId}, VisitorTeam={this.VisitorTeam}, VisitorTeamId={this.VisitorTeamId}, Status={this.Status}, MatchDate={this.MatchDate}";
        }
    }
}
