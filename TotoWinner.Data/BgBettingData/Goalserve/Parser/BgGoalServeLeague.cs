﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalServeLeague
    {
        public int LeagueId { get; }
        public string LeagueName { get; }
        public int? CountryId { get; }
        public string Country { get; }
        public bool IsCup { get; }

        public BgGoalServeLeague(int leagureId, string leagueName, string country, int? countryId, bool isCup)
        {
            this.LeagueId = leagureId;
            this.LeagueName = leagueName;
            this.CountryId = countryId;
            this.Country = country;
            this.IsCup = isCup;
        }
    }
}
