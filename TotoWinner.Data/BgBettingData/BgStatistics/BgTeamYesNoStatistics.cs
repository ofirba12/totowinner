﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamYesNoStatistics : ITeamStatistics
    {
        public int TotalRows { get; }
        public int Yes { get; }
        public int No { get; }
        public decimal YesPercent { get; }
        public decimal NoPercent { get; }

        public BgTeamYesNoStatistics(int totalRows,
            int totalYes,
            int totalNo)
        {
            TotalRows = totalRows;
            Yes = totalYes;
            No = totalNo;
            YesPercent = decimal.Round(((decimal)totalYes / totalRows) * 100, 0);
            NoPercent = decimal.Round(((decimal)totalNo / totalRows) * 100, 0);
        }
    }
}
