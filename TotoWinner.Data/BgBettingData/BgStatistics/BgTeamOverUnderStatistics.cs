﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamOverUnderStatistics : ITeamStatistics
    {
        public int TotalRows { get; }
        public int TotalIndexOver { get; }
        public int TotalIndexUnder { get; }
        public decimal TotalOverPercent { get; }
        public decimal TotalUnderPercent { get; }

        public BgTeamOverUnderStatistics(int totalRows, 
            int totalOver, 
            int totalUnder)
        {
            TotalRows = totalRows;
            TotalIndexOver = totalOver;
            TotalIndexUnder = totalUnder;
            TotalOverPercent = decimal.Round(((decimal)totalOver / totalRows) * 100, 0);
            TotalUnderPercent = decimal.Round(((decimal)totalUnder / totalRows) * 100, 0);
        }
    }
}
