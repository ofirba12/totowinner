﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamRangeStatistics : ITeamStatistics
    {
        public int TotalRows { get; }
        public int TotalBetween0and1 { get; }
        public int TotalBetween2and3 { get; }
        public int Total4Plus { get; }
        public decimal TotalBetween0and1Percent { get; }
        public decimal TotalBetween2and3Percent { get; }
        public decimal Total4PlusPercent { get; }

        public BgTeamRangeStatistics(int totalRows,
            int totalBetween0and1,
            int totalBetween2and3,
            int total4Plus
)
        {
            TotalRows = totalRows;
            TotalBetween0and1 = totalBetween0and1;
            TotalBetween2and3 = totalBetween2and3;
            Total4Plus = total4Plus;
            TotalBetween0and1Percent = decimal.Round(((decimal)totalBetween0and1 / totalRows) * 100, 0);
            TotalBetween2and3Percent = decimal.Round(((decimal)totalBetween2and3 / totalRows) * 100, 0);
            Total4PlusPercent = decimal.Round(((decimal)total4Plus / totalRows) * 100, 0);
        }
    }
}
