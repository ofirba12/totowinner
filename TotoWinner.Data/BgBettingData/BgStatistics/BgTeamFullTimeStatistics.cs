﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamFullTimeStatistics : ITeamStatistics
    {
        public int TotalRows { get; }
        public int TotalIndexW { get; }
        public int TotalIndexD { get; }
        public int TotalIndexL { get; }
        public decimal TotalIndexPercentW { get; }
        public decimal TotalIndexPercentD { get; }
        public decimal TotalIndexPercentL { get; }
        public decimal PowerNumber { get; }

        public BgTeamFullTimeStatistics(int totalRows, 
            int totalIndexW, 
            int totalIndexD, 
            int totalIndexL, 
            decimal powerNumber)
        {
            TotalRows = totalRows;
            TotalIndexW = totalIndexW;
            TotalIndexD = totalIndexD;
            TotalIndexL = totalIndexL;
            TotalIndexPercentW = decimal.Round(((decimal)totalIndexW / totalRows) * 100, 0);
            TotalIndexPercentD = decimal.Round(((decimal)totalIndexD / totalRows) * 100, 0);
            TotalIndexPercentL = decimal.Round(((decimal)totalIndexL / totalRows) * 100, 0);
            PowerNumber = powerNumber;
        }
    }
}
