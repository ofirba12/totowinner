﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgStatisticsRecalcRepository
    {
        public bool Recalc { get; }
        public bool OddsChanged { get; }
        public bool MatchCounterChange { get; }
        public DateTime? LastEodFetchedHomeTeam { get; }
        public DateTime? LastEodFetchedAwayTeam { get; }
        public OddsOnly Odds { get; }
        public BgStatisticsRecalcRepository(bool recalc, 
            bool oddsChanged, 
            bool matchCounterChange, 
            DateTime? lastEodFetchedHomeTeam, 
            DateTime? lastEodFetchedAwayTeam, 
            OddsOnly oddsOnly)
        {
            this.Recalc = recalc;
            this.OddsChanged = oddsChanged;
            this.MatchCounterChange = matchCounterChange;
            this.LastEodFetchedHomeTeam = lastEodFetchedHomeTeam;
            this.LastEodFetchedAwayTeam = lastEodFetchedAwayTeam;
            this.Odds = oddsOnly;
        }
        public override string ToString()
        {
            var str = $"Recalc={Recalc}, MatchCounterChange={MatchCounterChange}, OddsChanged={OddsChanged}, {Odds?.ToString()}, LastEodFetchedHomeTeam={LastEodFetchedHomeTeam}, LastEodFetchedAwayTeam={LastEodFetchedAwayTeam}";
            return str;
        }
    }
}
