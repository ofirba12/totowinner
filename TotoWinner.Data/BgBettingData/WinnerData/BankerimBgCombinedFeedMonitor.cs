﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class BankerimBgCombinedFeedMonitor
    {
        [ColumnStyle(DisplayName = "ליגה")]
        public string LeagueName { get; }
        [ColumnStyle(DisplayName = "משחק", Width = 50)]
        public int Place { get; }
        [ColumnStyle(DisplayName = "סטטוס", Width = 50)]
        public string Status { get; }
        [ColumnStyle(DisplayName = "קבוצה בית")]
        public string HomeTeamName { get; }
        [ColumnStyle(DisplayName = "קבוצה חוץ")]
        public string GuestTeamName { get; }
        [ColumnStyle(DisplayName = "יחס בית", Width = 50, FontBold = true)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(DisplayName = "יחס תיקו", Width = 50, FontBold = true)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(DisplayName = "יחס חוץ", Width = 50, FontBold = true)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Hide = true)]
        public long ProgramId { get; }
        //public int TotoId { get; }

        //Statistics
        [ColumnStyle(DisplayName = "דרוג 1", Width = 50, ZebraOn = true, ColorAlgoLogic = 5)]
        public int? BGRank1 { get; }
        [ColumnStyle(DisplayName = "X דרוג", Width = 50, ZebraOn = true, ColorAlgoLogic = 5)]
        public int? BGRankX { get; }
        [ColumnStyle(DisplayName = "דרוג 2", Width = 50, ZebraOn = true, ColorAlgoLogic = 5)]
        public int? BGRank2 { get; }
        [ColumnStyle(DisplayName = "דרוג מתחת 2.5", Width = 50, ColorAlgoLogic = 5)]
        public int? BGRankUnder2_5 { get; }
        [ColumnStyle(DisplayName = "דרוג מעל 2.5", Width = 50, ColorAlgoLogic = 5)]
        public int? BGRankOver2_5 { get; }
        [ColumnStyle(DisplayName = "דרוג 0-1", Width = 50, ZebraOn = true, ColorAlgoLogic = 5)]
        public int? BGRank0_1 { get; }
        [ColumnStyle(DisplayName = "דרוג 2-3", Width = 50, ZebraOn = true, ColorAlgoLogic = 5)]
        public int? BGRankOver2_3 { get; }
        [ColumnStyle(DisplayName = "דרוג +4", Width = 50, ZebraOn = true, ColorAlgoLogic = 5)]
        public int? BGRank4Plus { get; }
        [ColumnStyle(DisplayName = "המלצה אבא")]
        public string RankBestFather { get; }
        [ColumnStyle(DisplayName = "דרוג אבא", Width = 50, ColorAlgoLogic = 5)]
        public int? BestTipFather { get; }
        [ColumnStyle(DisplayName = "המלצה מעל מתחת")]
        public string RankBestOverUnder { get; }
        [ColumnStyle(DisplayName = "דרוג מעל מתחת", Width = 50, ColorAlgoLogic = 5)]
        public int? BestTipOverUnder { get; }
        [ColumnStyle(DisplayName = "המלצה טווח")]
        public string RankBestRange { get; }
        [ColumnStyle(DisplayName = "דרוג טווח", Width = 50, ColorAlgoLogic = 5)]
        public int? BestRange { get; }
        [ColumnStyle(Hide = true)]
        public int? BGHomeId { get; }
        [ColumnStyle(Hide = true)] 
        public string BGHome { get; }
        [ColumnStyle(Hide = true)]
        public int? BGAwayId { get; }
        [ColumnStyle(Hide = true)]
        public string BGAway { get; }
        [ColumnStyle(Hide = true)] 
        public int? BadMatches { get; }
        [ColumnStyle(Hide = true)]
        public int? HomeMatches { get; }
        [ColumnStyle(Hide = true)]
        public int? AwayMatches { get; }
        [ColumnStyle(Hide = true)]
        public int? Head2HeadMatches { get; }
        public BankerimBgCombinedFeedMonitor(BankerimBgCombinedFeed m) 
        {
            this.Place = m.Place;
            this.Status = m.Status;
            //this.Country = m.Country;
            this.LeagueName = m.LeagueName;            
            //this.BetType = m.BetType;
            //this.WinnerTime = m.WinnerTime;
            this.HomeTeamName = m.HomeTeamName;
            this.GuestTeamName = m.GuestTeamName;
            this.HomeOdd = m.HomeOdd;
            this.DrawOdd = m.DrawOdd;
            this.AwayOdd = m.AwayOdd;
            this.ProgramId = m.ProgramId;
            //this.TotoId = m.TotoId;
            this.BGRank1 = m.BGRank1;
            this.BGRankX = m.BGRankX;
            this.BGRank2 = m.BGRank2;
            this.BGRankOver2_5 = m.BGRankOver2_5;
            this.BGRankUnder2_5 = m.BGRankUnder2_5;
            this.BGRank0_1 = m.BGRank0_1;
            this.BGRankOver2_3  = m.BGRankOver2_3;
            this.BGRank4Plus = m.BGRank4Plus;
            this.BestTipFather = m.BestTipFather;
            this.RankBestFather = m.RankBestFather;
            this.BestTipOverUnder = m.BestTipOverUnder;
            this.RankBestOverUnder = m.RankBestOverUnder;
            this.BestRange = m.BestRange;
            this.RankBestRange = m.RankBestRange;
            this.BGHomeId = m.BGHomeId;
            this.BGHome = m.BGHome;
            this.BGAwayId = m.BGAwayId;
            this.BGAway = m.BGAway;
            this.BadMatches = m.BadMatches;
            this.HomeMatches = m.HomeMatches;
            this.AwayMatches = m.AwayMatches;
            this.Head2HeadMatches = m.Head2HeadMatches;
        }
    }
}
