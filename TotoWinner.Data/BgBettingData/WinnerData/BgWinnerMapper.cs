﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgWinnerMapper : IEquatable<BgWinnerMapper>, IEqualityComparer<BgWinnerMapper>
    {
        public SportIds Sport { get; }
        public int WinnerId { get; }
        public string WinnerName { get; }
        public int? BgId { get; }
        public int? GoalserveId { get; }
        public BgWinnerMapper(SportIds sport, int winnerId, string winnerName, int? bgId, int? goalserveId)
        {
            Sport = sport;
            WinnerId = winnerId;
            WinnerName = winnerName;
            BgId = bgId;
            GoalserveId = goalserveId;
        }

        public bool Equals(BgWinnerMapper other)
        {
            return this.Sport == other.Sport &&
                this.WinnerId == other.WinnerId &&
                this.WinnerName == other.WinnerName &&
                this.BgId == other.BgId &&
                this.GoalserveId == other.GoalserveId;
        }

        public bool Equals(BgWinnerMapper x, BgWinnerMapper y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(BgWinnerMapper obj)
        {
            return obj.BgId.GetHashCode() ^ obj.GoalserveId.GetHashCode() ^ obj.Sport.GetHashCode() + obj.WinnerId.GetHashCode() + obj.WinnerName.GetHashCode();
        }
        public override string ToString()
        {
            return $"Sport={Sport}, WinnerId={WinnerId}, WinnerName={WinnerName}, BgId={BgId}, GoalserveId={GoalserveId}";
        }
    }
}
