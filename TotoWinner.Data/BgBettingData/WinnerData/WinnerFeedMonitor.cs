﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class WinnerFeedMonitor
    {
        public int Place { get; }
        public SportIds Sport { get; }
        [ColumnStyle(ColorAlgoLogic = 3)]
        public int HomeId { get; }
        [ColumnStyle(Hide = true)]
        public bool HomeIdMapped { get; }
        [ColumnStyle(ColorAlgoLogic = 3)]
        public int GuestId { get; }
        [ColumnStyle(Hide = true)]
        public bool GuestIdMapped { get; }
        public string Result { get; }
        public DateTime Time { get; }
        public string Country { get; }
        public string LeagueName { get; }
        public string Name { get; }
        public string HomeTeamName { get; }
        public string GuestTeamName { get; }
        [ColumnStyle(Hide = true)]
        public bool HomeWon { get; }
        [ColumnStyle(ColorAlgoLogic = 6)]
        public decimal Home { get; }
        [ColumnStyle(Hide = true)]
        public bool DrawWon { get; }
        [ColumnStyle(ColorAlgoLogic = 6)]
        public decimal Draw { get; }
        [ColumnStyle(Hide = true)]
        public bool AwayWon { get; }
        [ColumnStyle(ColorAlgoLogic = 6)]
        public decimal Away { get; }
        public string Status { get; }
        public bool IsFather { get; }
        public int BetTypeId { get; }
        public string BetName { get; }
        public int EventId { get; }
        public int TotoId { get; }
        [ColumnStyle(DisplayName = "S/D")]
        public string SD { get; }
        public WinnerFeedMonitor(bool isFather,
            SportIds sport,
            int betTypeId,
            string betTypeName,
            int eventId, 
            int place, 
            int totoId, 
            DateTime time, 
            string status, 
            string name, 
            string homeTeamName, 
            int homeId, 
            bool homeIdMappedToBg,
            string guestTeamName, 
            int guestId,
            bool guestIdMappedToBg,
            string leagueName, 
            string country, 
            decimal home, 
            decimal draw, 
            decimal away, 
            string sd,
            string result,
            bool homeWon,
            bool drawWon,
            bool awayWon)
        {
            IsFather = isFather;
            Sport = sport;
            BetTypeId = betTypeId;
            BetName = betTypeName;
            EventId = eventId;
            Place = place;
            TotoId = totoId;
            Time = time;
            Status = status;
            Name = name;
            HomeTeamName = homeTeamName;
            HomeId = homeId;
            HomeIdMapped = homeIdMappedToBg;
            GuestTeamName = guestTeamName;
            GuestId = guestId;
            GuestIdMapped = guestIdMappedToBg;
            LeagueName = leagueName;
            Country = country;
            Home = home;
            Draw = draw;
            Away = away;
            SD = sd;
            this.Result = result;
            this.HomeWon = homeWon;
            this.DrawWon = drawWon;
            this.AwayWon = awayWon;
        }
    }
}
