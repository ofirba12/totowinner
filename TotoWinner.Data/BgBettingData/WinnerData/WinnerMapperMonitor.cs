﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class WinnerMapperMonitor
    {
        public SportIds Sport { get; }
        public int WinnerId { get; }
        public string WinnerName { get; }
        public int? BgId { get; }
        public string BgName { get; }
        //public int? GoalserveId { get; }
        public WinnerMapperMonitor(SportIds sport, int winnerId, string winnerName, int? bgId, string bgName)//, int? goalserveId)
        {
            Sport = sport;
            WinnerId = winnerId;
            WinnerName = winnerName;
            BgId = bgId;
            BgName = bgName;
            //GoalserveId = goalserveId;
        }
    }
}
