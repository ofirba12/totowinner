﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class WinnerBgOddsMonitor
    {
        public string Bookmaker { get; set; }
        [ColumnStyle(Width = 50)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 50)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 50)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Width = 200)]
        public DateTime Inserted { get; }
        [ColumnStyle(Width = 200)]
        public DateTime? LastUpdate { get; }

        public WinnerBgOddsMonitor(string bookmaker,
            decimal? homeOdd,
            decimal? drawOdd,
            decimal? awayOdd,
            DateTime inserted,
            DateTime? lastUpdate)
        {
            this.Bookmaker = bookmaker;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.Inserted = inserted;
            this.LastUpdate = lastUpdate;
        }
    }    
}