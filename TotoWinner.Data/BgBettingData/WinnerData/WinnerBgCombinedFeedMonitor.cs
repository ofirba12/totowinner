﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class WinnerBgCombinedFeedMonitor
    {
        [ColumnStyle(Width = 50)]
        public int Place { get; }
        [ColumnStyle(Width = 50)]
        public string Status { get; }
        public string Country { get; }
        public string LeagueName { get; }
        [ColumnStyle(Width = 180)]
        public string BetName { get; }
        public string HomeTeamName { get; }
        public string GuestTeamName { get; }
        [ColumnStyle(Width = 50)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 50)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 50)]
        public decimal? AwayOdd { get; }
        public int? BetMax { get; }
        [ColumnStyle(ColorAlgoLogic = 4)]
        public string BetTip { get; }
        [ColumnStyle(ColorAlgoLogic = 2, Width = 50)]
        public int? BadMatches { get; }
        [ColumnStyle(HeaderColor = 1, Width = 50)]
        public int? HomeMatches { get; }
        [ColumnStyle(HeaderColor = 1, Width = 50)]
        public int? AwayMatches { get; }
        [ColumnStyle(HeaderColor = 1, Width = 50)]
        public int? Head2HeadMatches { get; }

        /// <summary>
        /// 
        /// </summary>
        public SportIds Sport { get; }
        public long ProgramId { get; }
        public int EventId { get; }
        public DateTime WinnerTime { get; }
        public int WinnerHomeId { get; }
        public int WinnerGuestId { get; }
        public int BetTypeId { get; }
        public int TotoId { get; }
        [ColumnStyle(Width = 400)]
        public string Name { get; }

        // BG
        public DateTime? BGDate { get; }
        public string BGTime { get; }
        public string BGHome { get; }
        public string BGAway { get; }
        public int? BGHomeId { get; }
        public int? BGAwayId { get; }
        public long? BGMatchId { get; }

        //Statistics
        public int? BGRank1 { get; }
        public int? BGRankX { get; }
        public int? BGRank2 { get; }

        public bool IsFather { get; }
        public WinnerBgCombinedFeedMonitor(WinnerBgCombinedFeed m, BgFeedFullRepository bgFeed)
        {
            this.Sport = m.WinnerMatch.SportId;
            this.ProgramId = m.WinnerMatch.ProgramId;
            this.BetTypeId = (int)m.WinnerMatch.BetTypeId;
            this.BetName = m.WinnerMatch.BetTypeId.ToString();
            this.EventId = m.WinnerMatch.EventId;
            this.Place = m.WinnerMatch.Place;
            this.TotoId = m.WinnerMatch.TotoId;
            this.WinnerTime = m.WinnerMatch.GameStart;
            this.Status = m.WinnerMatch.Status;
            this.Name = m.WinnerMatch.Name;
            this.HomeTeamName = m.WinnerMatch.HomeTeam;
            this.WinnerHomeId = m.WinnerMatch.HomeId;
            this.GuestTeamName = m.WinnerMatch.GuestTeam;
            this.WinnerGuestId = m.WinnerMatch.GuestId;
            this.LeagueName = m.WinnerMatch.League;
            this.Country = m.WinnerMatch.Country;
            this.HomeOdd = m.WinnerMatch.HomeOdd;
            this.DrawOdd = m.WinnerMatch.DrawOdd;
            this.AwayOdd = m.WinnerMatch.AwayOdd;
            if (m.BgMatch != null)
            {
                this.BGDate = m.BgMatch.GameStart;
                this.BGTime = m.BgMatch.Status;
                this.BGHome = m.BgMatch.Home;
                this.BGAway = m.BgMatch.Away;
                this.BGHomeId = m.BgMatch.HomeId;
                this.BGAwayId = m.BgMatch.AwayId;
                this.BGMatchId = m.BgMatch.MatchId;
                if (bgFeed.Info.ContainsKey(m.BgMatch.MatchId))
                {
                    this.BadMatches = bgFeed.Info.ContainsKey(m.BgMatch.MatchId) ? bgFeed.Info[m.BgMatch.MatchId].BadMatches : default(int?);
                    this.HomeMatches = bgFeed.Info.ContainsKey(m.BgMatch.MatchId) ? bgFeed.Info[m.BgMatch.MatchId]?.HomeMatches : default(int?);
                    this.AwayMatches = bgFeed.Info.ContainsKey(m.BgMatch.MatchId) ? bgFeed.Info[m.BgMatch.MatchId]?.AwayMatches : default(int?);
                    this.Head2HeadMatches = bgFeed.Info.ContainsKey(m.BgMatch.MatchId) ? bgFeed.Info[m.BgMatch.MatchId]?.Head2HeadMatches : default(int?);
                }
            }
            if (m.BgMatchImpliedStatistics != null)
            {
                var tips = new List<Tuple<int, string>>();
                switch (this.BetTypeId)
                {
                    case 3:
                    case 9:
                    case 15:
                        this.BGRank1 = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_1];
                        this.BGRankX = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_X];
                        this.BGRank2 = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_2];
                        tips.Add(new Tuple<int, string>(this.BGRank1.Value, "HOME WIN"));
                        tips.Add(new Tuple<int, string>(this.BGRankX.Value, "DRAW"));
                        tips.Add(new Tuple<int, string>(this.BGRank2.Value, "AWAY WIN"));
                        break;
                    case 5:
                        this.BGRank1 = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_Over2_5];
                        this.BGRank2 = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_Under2_5];
                        tips.Add(new Tuple<int, string>(this.BGRank1.Value, "OVER 2.5"));
                        tips.Add(new Tuple<int, string>(this.BGRank2.Value, "UNDER 2.5"));
                        break;
                    case 11:
                        this.BGRank1 = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_0_1];
                        this.BGRankX = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_2_3];
                        this.BGRank2 = m.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_4Plus];
                        tips.Add(new Tuple<int, string>(this.BGRank1.Value, "RANGE 0-1"));
                        tips.Add(new Tuple<int, string>(this.BGRankX.Value, "RANGE 2-3"));
                        tips.Add(new Tuple<int, string>(this.BGRank2.Value, "RANGE 4+"));
                        break;
                    case 16:
                        break;
                }
                if (tips.Count > 0)
                {
                    var maxBet = tips.OrderByDescending(x => x.Item1).First();
                    this.BetMax = maxBet.Item1;
                    this.BetTip = maxBet.Item2;
                }
            }





        }
    }
}
/*
        this.Web1 = implied?.Items[BgImpliedStatisticType.Web_1];
        this.WebX = implied?.Items[BgImpliedStatisticType.Web_X];
        this.Web2 = implied?.Items[BgImpliedStatisticType.Web_2];
        this.WebUnder2_5 = implied?.Items[BgImpliedStatisticType.Web_Under2_5];
        this.WebOver2_5 = implied?.Items[BgImpliedStatisticType.Web_Over2_5];
        this.WebRange0_1 = implied?.Items[BgImpliedStatisticType.Web_0_1];
        this.WebRange2_3 = implied?.Items[BgImpliedStatisticType.Web_2_3];
        this.WebRange4Plus = implied?.Items[BgImpliedStatisticType.Web_4Plus];
 */