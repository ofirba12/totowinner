﻿namespace TotoWinner.Data.BgBettingData
{
    public class WinnerBgCombinedFeed
    {
        public BgWinnerFeed WinnerMatch { get; }
        public BgMatch BgMatch { get; }
        public BgMatchImpliedStatistics BgMatchImpliedStatistics { get; }

        public WinnerBgCombinedFeed(BgWinnerFeed winnerMatch, 
            BgMatch bgMatch, 
            BgMatchImpliedStatistics bgMatchImpliedStatistics)
        {
            WinnerMatch = winnerMatch;
            BgMatch = bgMatch;
            BgMatchImpliedStatistics = bgMatchImpliedStatistics;
        }
    }
}
