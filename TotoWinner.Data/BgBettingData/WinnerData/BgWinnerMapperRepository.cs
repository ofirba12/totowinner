﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgWinnerMapperRepository
    {
        public IReadOnlyList<BgWinnerMapper> Collection { get; }
        public IReadOnlyDictionary<WinnerMapperKey, BgWinnerMapper> Mapper { get;}

        public BgWinnerMapperRepository(List<BgWinnerMapper> collection, Dictionary<WinnerMapperKey, BgWinnerMapper> mapper)
        {
            Collection = collection;
            Mapper = mapper;
        }
    }
}
