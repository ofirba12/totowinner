﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class BankerimBgCombinedFeed
    {
        public int Place { get; }
        public string Status { get; }
        public string Country { get; }
        public string LeagueName { get; }
        public BetTypes BetType { get; }
        public DateTime WinnerTime { get; }
        public string HomeTeamName { get; }
        public string GuestTeamName { get; }
        public decimal? HomeOdd { get; }
        public decimal? DrawOdd { get; }
        public decimal? AwayOdd { get; }
        public long ProgramId { get; }
        public int TotoId { get; }

        //Statistics
        public int? BGRank1 { get; }
        public int? BGRankX { get; }
        public int? BGRank2 { get; }
        public int? BGRankOver2_5 { get; }
        public int? BGRankUnder2_5 { get; }
        public int? BGRank0_1 { get; }
        public int? BGRankOver2_3 { get; }
        public int? BGRank4Plus { get; }
        public int? BestTipFather { get; }
        public string RankBestFather { get; }
        public int? BestTipOverUnder { get; }
        public string RankBestOverUnder { get; }
        public int? BestRange { get; }
        public string RankBestRange { get; }
        public int? BGHomeId { get; }
        public string BGHome { get; }
        public int? BGAwayId { get; }
        public string BGAway { get; }
        public int? BadMatches { get; }
        public int? HomeMatches { get; }
        public int? AwayMatches { get; }
        public int? Head2HeadMatches { get; }

        public BankerimBgCombinedFeed(WinnerBgCombinedFeed parent, 
            WinnerBgCombinedFeed child2_5, 
            WinnerBgCombinedFeed childTotalScore,
            BgFeedFullRepository bgFeed)
        {
            this.ProgramId = parent.WinnerMatch.ProgramId;
            this.BetType = parent.WinnerMatch.BetTypeId;
            this.Place = parent.WinnerMatch.Place;
            this.TotoId = parent.WinnerMatch.TotoId;
            this.WinnerTime = parent.WinnerMatch.GameStart;
            this.Status = parent.WinnerMatch.Status;
            this.HomeTeamName = parent.WinnerMatch.HomeTeam;
            this.GuestTeamName = parent.WinnerMatch.GuestTeam;
            this.LeagueName = parent.WinnerMatch.League;
            this.Country = parent.WinnerMatch.Country;
            this.HomeOdd = parent.WinnerMatch.HomeOdd;
            this.DrawOdd = parent.WinnerMatch.DrawOdd;
            this.AwayOdd = parent.WinnerMatch.AwayOdd;
            if (parent.BgMatch != null)
            {
                this.BGHomeId = parent.BgMatch.HomeId;
                this.BGHome = parent.BgMatch.Home;
                this.BGAwayId = parent.BgMatch.AwayId;
                this.BGAway = parent.BgMatch.Away;
                if (bgFeed.Info.ContainsKey(parent.BgMatch.MatchId))
                {
                    this.BadMatches = bgFeed.Info.ContainsKey(parent.BgMatch.MatchId) ? bgFeed.Info[parent.BgMatch.MatchId].BadMatches : default(int?);
                    this.HomeMatches = bgFeed.Info.ContainsKey(parent.BgMatch.MatchId) ? bgFeed.Info[parent.BgMatch.MatchId]?.HomeMatches : default(int?);
                    this.AwayMatches = bgFeed.Info.ContainsKey(parent.BgMatch.MatchId) ? bgFeed.Info[parent.BgMatch.MatchId]?.AwayMatches : default(int?);
                    this.Head2HeadMatches = bgFeed.Info.ContainsKey(parent.BgMatch.MatchId) ? bgFeed.Info[parent.BgMatch.MatchId]?.Head2HeadMatches : default(int?);
                }
            }
            if (parent.BgMatchImpliedStatistics != null)
            {
                var tips = new List<Tuple<int, string>>();
                this.BGRank1 = parent.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_1];
                this.BGRankX = parent.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_X];
                this.BGRank2 = parent.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_2];
                tips.Add(new Tuple<int, string>(this.BGRank1.Value, "HOME WIN"));
                tips.Add(new Tuple<int, string>(this.BGRankX.Value, "DRAW"));
                tips.Add(new Tuple<int, string>(this.BGRank2.Value, "AWAY WIN"));
                var maxBet = tips.OrderByDescending(x => x.Item1).First();
                this.BestTipFather = maxBet.Item1;
                this.RankBestFather = maxBet.Item2;
            }
            if (child2_5?.BgMatchImpliedStatistics != null)
            {
                var tips = new List<Tuple<int, string>>();
                this.BGRankOver2_5 = child2_5.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_Over2_5];
                this.BGRankUnder2_5 = child2_5.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_Under2_5];
                tips.Add(new Tuple<int, string>(this.BGRankOver2_5.Value, "OVER 2.5"));
                tips.Add(new Tuple<int, string>(this.BGRankUnder2_5.Value, "UNDER 2.5"));
                var maxBet = tips.OrderByDescending(x => x.Item1).First();
                this.BestTipOverUnder = maxBet.Item1;
                this.RankBestOverUnder = maxBet.Item2;
            }
            if (childTotalScore?.BgMatchImpliedStatistics != null)
            {
                var tips = new List<Tuple<int, string>>();
                this.BGRank0_1 = childTotalScore.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_0_1];
                this.BGRankOver2_3 = childTotalScore.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_2_3];
                this.BGRank4Plus = childTotalScore.BgMatchImpliedStatistics.Items[BgImpliedStatisticType.Web_4Plus];
                tips.Add(new Tuple<int, string>(this.BGRank0_1.Value, "RANGE 0-1"));
                tips.Add(new Tuple<int, string>(this.BGRankOver2_3.Value, "RANGE 2-3"));
                tips.Add(new Tuple<int, string>(this.BGRank4Plus.Value, "RANGE 4+"));
                var maxBet = tips.OrderByDescending(x => x.Item1).First();
                this.BestRange = maxBet.Item1;
                this.RankBestRange = maxBet.Item2;
            }
        }
    }
}
