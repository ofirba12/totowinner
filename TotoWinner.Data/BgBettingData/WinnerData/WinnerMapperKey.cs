﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class WinnerMapperKey : IEquatable<WinnerMapperKey>
    {
        public SportIds Sport { get; }
        public int WinnerId { get; }

        public WinnerMapperKey(SportIds sport, int winnerId)
        {
            Sport = sport;
            WinnerId = winnerId;
        }

        public bool Equals(WinnerMapperKey other)
        {
            return this.Sport == other.Sport &&
                this.WinnerId == other.WinnerId;
        }
        public bool Equals(ProviderName x, ProviderName y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.Sport.GetHashCode() ^ this.WinnerId.GetHashCode();
        }
    }
}