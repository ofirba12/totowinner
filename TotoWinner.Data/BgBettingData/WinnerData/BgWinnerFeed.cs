﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgWinnerFeed
    {
        public int EventId { get; }
        public long ProgramId { get; }
        public SportIds SportId { get; }
        public int TotoId { get; }
        public int HomeId { get; }
        public string HomeTeam { get; }
        public int GuestId { get; }
        public string GuestTeam { get; }
        public BetTypes BetTypeId { get; }
        public int Place { get; }
        public string Name { get; }
        public DateTime GameStart { get; }
        public string Status { get; }
        public decimal? HomeOdd { get; }
        public decimal? DrawOdd { get; }
        public decimal? AwayOdd { get; }
        public string Country { get; }
        public string League { get; }
        public DateTime LastUpdate { get; }

        public BgWinnerFeed(int eventId, 
            long programId,
            SportIds sportId, 
            int totoId, 
            int homeId, 
            string homeTeam, 
            int guestId, 
            string guestTeam,
            BetTypes betTypeId, 
            int place, 
            string name, 
            DateTime gameStart, 
            string status, 
            decimal? homeOdd, 
            decimal? drawOdd, 
            decimal? awayOdd, 
            string country, 
            string league, 
            DateTime lastUpdate)
        {
            EventId = eventId;
            ProgramId = programId;
            SportId = sportId;
            TotoId = totoId;
            HomeId = homeId;
            HomeTeam = homeTeam;
            GuestId = guestId;
            GuestTeam = guestTeam;
            BetTypeId = betTypeId;
            Place = place;
            Name = name;
            GameStart = gameStart;
            Status = status;
            HomeOdd = homeOdd;
            DrawOdd = drawOdd;
            AwayOdd = awayOdd;
            Country = country;
            League = league;
            LastUpdate = lastUpdate;
        }
    }
}
