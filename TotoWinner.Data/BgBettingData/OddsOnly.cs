﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class OddsOnly : IEquatable<OddsOnly>
    {
        public decimal HomeOdd { get; }
        public decimal DrawOdd { get; }
        public decimal AwayOdd { get; }
        public OddsOnly(decimal homeOdd,
            decimal drawOdd,
            decimal awayOdd)
        {
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
        }

        public bool Equals(OddsOnly other)
        {
            var odds = this.HomeOdd == other.HomeOdd
                && this.DrawOdd == other.DrawOdd
                && this.AwayOdd == other.AwayOdd;
            return odds;
        }
        public override int GetHashCode()
        {
            var code = this.HomeOdd.GetHashCode() ^ this.DrawOdd.GetHashCode() ^ this.AwayOdd.GetHashCode();
            return code;
        }
        public override string ToString()
        {
            var str = $"[{HomeOdd}/{DrawOdd}/{AwayOdd}]";
            return str;
        }
    }
}
