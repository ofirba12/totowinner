﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgDataScoreResponse
    {
        public DateTime FetchedTime { get; set; }
        public int GmtAdjustment { get; set; }
        public List<BgDataLeague> Leagues { get; set; }
        public BgDataScoreResponse(DateTime fetchedDate, int gmtAdjustment, List<BgDataLeague> leagues)
        {
            this.FetchedTime = fetchedDate;
            this.GmtAdjustment = gmtAdjustment;
            this.Leagues= leagues;
        }
    }
}
