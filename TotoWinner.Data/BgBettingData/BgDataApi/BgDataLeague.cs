﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgDataLeague
    {
        public string Country { get; set; }
        public int CountryId { get; set; }
        public string FlagSymbol { get; set; }
        public int LeagueId { get; set; }
        public string LeagueName { get; set; }
        public List<BgDataMatch> Matches { get; set; }

        public BgDataLeague(string country, int countryId, string flagSymbol, int leagueId, string leagueName, List<BgDataMatch> matches)
        {
            Country = country;
            CountryId = countryId;
            FlagSymbol = flagSymbol;
            LeagueId = leagueId;
            LeagueName = leagueName;
            Matches = matches;
        }
    }
}
