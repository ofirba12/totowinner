﻿namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgDataMatchIndexes
    {
        public string OverUnder1_5 { get; set; }
        public string OverUnder2_5 { get; set; }
        public string OverUnder3_5 { get; set; }
        public string Range { get; set; }
        public string BothScore { get; set; }
        public string DrawWin { get; set; }
        public string NoDraw { get; set; }
        public string LoseDraw { get; set; }
        public decimal OddPower { get; }

        public BgDataMatchIndexes(string overUnder1_5, 
            string overUnder2_5, 
            string overUnder3_5, 
            string range, 
            string bothScore, 
            string drawWin, 
            string noDraw, 
            string loseDraw,
            decimal oddPower)
        {
            this.OverUnder1_5 = overUnder1_5;
            this.OverUnder2_5 = overUnder2_5;
            this.OverUnder3_5 = overUnder3_5;
            this.Range = range;
            this.BothScore = bothScore;
            this.DrawWin = drawWin;
            this.NoDraw = noDraw;
            this.LoseDraw = loseDraw;
            this.OddPower = oddPower;

        }
    }
}
