﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgDataBetTips
    {
        public string FullTimeTitle { get; }
        public int FullTimeValue { get; }
        public int FullTimeQuality{ get; }
        public int FullTimeStars { get; }
        public List<string> FullTimeTipDescription { get; set; }
        public string OverUnder1_5Title { get; }
        public int OverUnder1_5Value { get; }
        public int OverUnder1_5Quality { get; }
        public int OverUnder1_5Stars { get; }
        public List<string> OverUnder1_5TipDescription { get; set; }
        public string OverUnder2_5Title { get; }
        public int OverUnder2_5Value { get; }
        public int OverUnder2_5Quality { get; }
        public int OverUnder2_5Stars { get; }
        public List<string> OverUnder2_5TipDescription { get; set; }
        public string OverUnder3_5Title { get; }
        public int OverUnder3_5Value { get; }
        public int OverUnder3_5Quality { get; }
        public int OverUnder3_5Stars { get; }
        public List<string> OverUnder3_5TipDescription { get; set; }
        public string RangeTitle { get; }
        public int RangeValue { get; }
        public int RangeQuality { get; }
        public int RangeStars { get; }
        public List<string> RangeTipDescription { get; set; }
        public string BothScoreTitle { get; }
        public int BothScoreValue { get; }
        public int BothScoreQuality { get; }
        public int BothScoreStars { get; }
        public List<string> BothScoreTipDescription { get; set; }
        public string DoubleTitle { get; }
        public int DoubleValue { get; }
        public int DoubleQuality { get; }
        public int DoubleStars { get; }
        public List<string> DoubleTipDescription { get; set; }

        public BgDataBetTips(string fullTimeTitle,
            int fullTimeValue, 
            int fullTimeQuality, 
            int fullTimeStars, 
            string overUnder1_5Title, 
            int overUnder1_5Value, 
            int overUnder1_5Quality, 
            int overUnder1_5Stars, 
            string overUnder2_5Title, 
            int overUnder2_5Value, 
            int overUnder2_5Quality, 
            int overUnder2_5Stars, 
            string overUnder3_5Title, 
            int overUnder3_5Value, 
            int overUnder3_5Quality, 
            int overUnder3_5Stars, 
            string rangeTitle, 
            int rangeValue, 
            int rangeQuality, 
            int rangeStars, 
            string bothScoreTitle, 
            int bothScoreValue, 
            int bothScoreQuality, 
            int bothScoreStars, 
            string doubleTitle, 
            int doubleValue, 
            int doubleQuality, 
            int doubleStars)
        {
            FullTimeTitle = fullTimeTitle;
            FullTimeValue = fullTimeValue;
            FullTimeQuality = fullTimeQuality;
            FullTimeStars = fullTimeStars;
            OverUnder1_5Title = overUnder1_5Title;
            OverUnder1_5Value = overUnder1_5Value;
            OverUnder1_5Quality = overUnder1_5Quality;
            OverUnder1_5Stars = overUnder1_5Stars;
            OverUnder2_5Title = overUnder2_5Title;
            OverUnder2_5Value = overUnder2_5Value;
            OverUnder2_5Quality = overUnder2_5Quality;
            OverUnder2_5Stars = overUnder2_5Stars;
            OverUnder3_5Title = overUnder3_5Title;
            OverUnder3_5Value = overUnder3_5Value;
            OverUnder3_5Quality = overUnder3_5Quality;
            OverUnder3_5Stars = overUnder3_5Stars;
            RangeTitle = rangeTitle;
            RangeValue = rangeValue;
            RangeQuality = rangeQuality;
            RangeStars = rangeStars;
            BothScoreTitle = bothScoreTitle;
            BothScoreValue = bothScoreValue;
            BothScoreQuality = bothScoreQuality;
            BothScoreStars = bothScoreStars;
            DoubleTitle = doubleTitle;
            DoubleValue = doubleValue;
            DoubleQuality = doubleQuality;
            DoubleStars = doubleStars;
        }
        public BgDataBetTips(BgDataBetTips tips)
        {
            FullTimeTitle = tips.FullTimeTitle;
            FullTimeValue = tips.FullTimeValue;
            FullTimeQuality = tips.FullTimeQuality;
            FullTimeStars = tips.FullTimeStars;
            FullTimeTipDescription = tips.FullTimeTipDescription;
            OverUnder1_5Title = tips.OverUnder1_5Title;
            OverUnder1_5Value = tips.OverUnder1_5Value;
            OverUnder1_5Quality = tips.OverUnder1_5Quality;
            OverUnder1_5Stars = tips.OverUnder1_5Stars;
            OverUnder1_5TipDescription = tips.OverUnder1_5TipDescription;
            OverUnder2_5Title = tips.OverUnder2_5Title;
            OverUnder2_5Value = tips.OverUnder2_5Value;
            OverUnder2_5Quality = tips.OverUnder2_5Quality;
            OverUnder2_5Stars = tips.OverUnder2_5Stars;
            OverUnder2_5TipDescription = tips.OverUnder2_5TipDescription;
            OverUnder3_5Title = tips.OverUnder3_5Title;
            OverUnder3_5Value = tips.OverUnder3_5Value;
            OverUnder3_5Quality = tips.OverUnder3_5Quality;
            OverUnder3_5Stars = tips.OverUnder3_5Stars;
            OverUnder3_5TipDescription = tips.OverUnder3_5TipDescription;
            RangeTitle = tips.RangeTitle;
            RangeValue = tips.RangeValue;
            RangeQuality = tips.RangeQuality;
            RangeStars = tips.RangeStars;
            RangeTipDescription = tips.RangeTipDescription;
            BothScoreTitle = tips.BothScoreTitle;
            BothScoreValue = tips.BothScoreValue;
            BothScoreQuality = tips.BothScoreQuality;
            BothScoreStars = tips.BothScoreStars;
            BothScoreTipDescription = tips.BothScoreTipDescription;
            DoubleTitle = tips.DoubleTitle;
            DoubleValue = tips.DoubleValue;
            DoubleQuality = tips.DoubleQuality;
            DoubleStars = tips.DoubleStars;
            DoubleTipDescription = tips.DoubleTipDescription;
        }
    }
}
