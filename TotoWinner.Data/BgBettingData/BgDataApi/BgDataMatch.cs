﻿namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgDataMatch
    {
        public long MatchId { get; set; }
        public BgSport Sport { get; set; }
        public int HomeTeamId { get; set; }
        public string HomeTeam { get; set; }
        public string HomeTeamSymbol { get; set; }
        public int AwayTeamId { get; set; }
        public string AwayTeam { get; set; }
        public string AwayTeamSymbol { get; set; }
        public string GameTime { get; set; }
        public string Status { get; set; }
        public double? HomeOdd { get; set; }
        public double? DrawOdd { get; set; }
        public double? AwayOdd { get; set; }
        public string HomeScore { get; set; }
        public string AwayScore { get; set; }
        public string BetResult { get; set; }
        public BgDataBetTips BetTips { get; set; }

    public BgDataMatch(long matchId,
            BgSport sport,
            int homeTeamId, 
            string homeTeam, 
            string homeTeamSymbol, 
            int awayTeamId, 
            string awayTeam, 
            string awayTeamSymbol, 
            string gameTime, 
            string status, 
            double? homeOdd, 
            double? drawOdd, 
            double? awayOdd, 
            string homeScore, 
            string awayScore, 
            string betResult,
            BgDataBetTips betTips)
        {
            this.MatchId = matchId;
            this.Sport = sport;
            this.HomeTeamId = homeTeamId;
            this.HomeTeam = homeTeam;
            this.HomeTeamSymbol = homeTeamSymbol;
            this.AwayTeamId = awayTeamId;
            this.AwayTeam = awayTeam;
            this.AwayTeamSymbol = awayTeamSymbol;
            this.GameTime = gameTime;
            this.Status = status;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.BetResult = betResult;
            this.BetTips = betTips;
        }
    }
}
