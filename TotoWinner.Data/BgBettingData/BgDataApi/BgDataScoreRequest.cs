﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgDataScoreRequest
    {
        public BgSport Sport { get; set; }
        public DateTime FetchDate { get; set; }
        public int TimezoneOffset { get; set; }
        public BgDataScoreRequest(BgSport sport, DateTime fetchDate, int timezoneOffset)
        {
            this.Sport= sport;
            this.FetchDate = fetchDate;
            this.TimezoneOffset = timezoneOffset;
        }
    }
}
