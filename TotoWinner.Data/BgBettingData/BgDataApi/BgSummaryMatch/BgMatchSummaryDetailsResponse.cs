﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgMatchSummaryDetailsResponse
    {
        public BgSummaryMatchDetails Details { get; }
        public BgMatchSummaryDetailsResponse(BgSummaryMatchDetails details)
        {
            this.Details = details;
        }
    }
}
