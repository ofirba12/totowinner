﻿using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class MatchTipsDescriptions
    {
        public List<string> FullTimeTipDescription { get; }
        public List<string> OverUnder1_5TipDescription { get; }
        public List<string> OverUnder2_5TipDescription { get; }
        public List<string> OverUnder3_5TipDescription { get; }
        public List<string> RangeTipDescription { get; }
        public List<string> BothScoreTipDescription { get; }
        public List<string> DoubleTipDescription { get; }

        public MatchTipsDescriptions(List<string> fullTimeTipDescription,
            List<string> overUnder1_5TipDescription,
            List<string> overUnder2_5TipDescription,
            List<string> overUnder3_5TipDescription,
            List<string> rangeTipDescription,
            List<string> bothScoreTipDescription,
            List<string> doubleTipDescription)
        {
            this.FullTimeTipDescription = fullTimeTipDescription;
            this.OverUnder1_5TipDescription = overUnder1_5TipDescription;
            this.OverUnder2_5TipDescription = overUnder2_5TipDescription;
            this.OverUnder3_5TipDescription = overUnder3_5TipDescription;
            this.RangeTipDescription = rangeTipDescription;
            this.BothScoreTipDescription = bothScoreTipDescription;
            this.DoubleTipDescription = doubleTipDescription;
        }
    }
}