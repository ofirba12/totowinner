﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgSummaryMatchInfo
    {
        public long MatchId { get; set; }
        public BgSport Sport { get; set; }
        public string Country { get; set; }
        public int CountryId { get; set; }
        public string FlagSymbol { get; set; }
        public int LeagueId { get; set; }
        public string LeagueName { get; set; }
        public int HomeTeamId { get; set; }
        public string HomeTeam { get; set; }
        public string HomeTeamSymbol { get; set; }
        public int AwayTeamId { get; set; }
        public string AwayTeam { get; set; }
        public string AwayTeamSymbol { get; set; }
        public string GameDayOfWeek { get; set; }
        public string GameDate { get; set; }
        public string GameTime { get; set; }
        public string Status { get; set; }
        public double? HomeOdd { get; set; }
        public double? DrawOdd { get; set; }
        public double? AwayOdd { get; set; }
        public string HomeScore { get; set; }
        public string AwayScore { get; set; }
        public string BetResult { get; set; }

        public BgSummaryMatchInfo(long matchId,
            BgSport sport,
            string country,
            int countryId,
            string flagSymbol,
            int leagueId,
            string leagueName,
            int homeTeamId,
            string homeTeam,
            string homeTeamSymbol,
            int awayTeamId,
            string awayTeam,
            string awayTeamSymbol,
            string gameDayOfWeek,
            string gameDate,
            string gameTime,
            string status,
            double? homeOdd,
            double? drawOdd,
            double? awayOdd,
            string homeScore,
            string awayScore,
            string betResult)
        {
            this.MatchId = matchId;
            this.Sport = sport;
            this.Country = country;
            this.CountryId = countryId;
            this.FlagSymbol = flagSymbol;
            this.LeagueId = leagueId;
            this.LeagueName = leagueName;
            this.HomeTeamId = homeTeamId;
            this.HomeTeam = homeTeam;
            this.HomeTeamSymbol = homeTeamSymbol;
            this.AwayTeamId = awayTeamId;
            this.AwayTeam = awayTeam;
            this.AwayTeamSymbol = awayTeamSymbol;
            this.GameDayOfWeek = gameDayOfWeek;
            this.GameDate = gameDate;
            this.GameTime = gameTime;
            this.Status = status;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.BetResult = betResult;
        }
    }
}
