﻿namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgMatchSummaryDetailsRequest
    {
        public long MatchId { get; set; }
        public int TimezoneOffset { get; set; }
        public BgMatchSummaryDetailsRequest(long matchId, int timezoneOffset)
        {
            this.MatchId = matchId;
            this.TimezoneOffset = timezoneOffset;
        }
    }
}
