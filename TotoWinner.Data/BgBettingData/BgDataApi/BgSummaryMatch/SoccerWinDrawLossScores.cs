﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class SoccerWinDrawLossScores
    {
        public int Wins { get; }
        public int Draws { get; }
        public int Losses { get; }
        public int NoGoals { get; }
        public int Goals1 { get; }
        public int Goals2 { get; }
        public int Goals3 { get; }
        public int Goals4Plus { get; }
        public List<OddsPowerDetails> OddsPowerDetails { get; }
        public SoccerWinDrawLossScores(int wins, 
            int draws, 
            int losses, 
            int noGoals, 
            int goals1, 
            int goals2, 
            int goals3, 
            int goals4Plus,
            List<OddsPowerDetails> oddsPowerDetails)
        {
            Wins = wins;
            Draws = draws;
            Losses = losses;
            NoGoals = noGoals;
            Goals1 = goals1;
            Goals2 = goals2;
            Goals3 = goals3;
            Goals4Plus = goals4Plus;
            OddsPowerDetails = oddsPowerDetails;
        }
    }
}
