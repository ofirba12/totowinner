﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgSummaryMatchAnalysis
    {
        public CategorizedMatches LastGamesHome { get; }
        public CategorizedMatches LastGamesAway { get; }
        public CategorizedMatches Head2HeadGamesHome { get; }
        public CategorizedMatches Head2HeadGamesAway { get; }

        public BgSummaryMatchAnalysis(CategorizedMatches lastGamesHome, 
            CategorizedMatches lastGamesAway, 
            CategorizedMatches head2HeadGamesHome, 
            CategorizedMatches head2HeadGamesAway)
        {
            LastGamesHome = lastGamesHome;
            LastGamesAway = lastGamesAway;
            Head2HeadGamesHome = head2HeadGamesHome;
            Head2HeadGamesAway = head2HeadGamesAway;
        }
    }
}
