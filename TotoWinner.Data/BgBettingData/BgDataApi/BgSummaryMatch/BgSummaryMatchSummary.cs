﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgSummaryMatchSummary
    {
        public BgMatchSummaryOverview Overview { get; set; }
        public SummaryDigestData Data { get; set; }
        public BgSummaryMatchSummary(BgMatchSummaryOverview overview, SummaryDigestData data)
        {
            this.Overview = overview;
            this.Data = data;
        }
    }
}
