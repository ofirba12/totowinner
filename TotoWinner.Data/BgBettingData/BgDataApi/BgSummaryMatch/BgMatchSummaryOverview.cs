﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgMatchSummaryOverview
    {
        public BgMatchSummaryOverviewParagraph OverviewParagraph { get; }
        public BgMatchSummaryOverviewParagraph OddsParagraph { get; }
        public BgMatchSummaryOverviewParagraph HomeLastGameParagraph { get; }
        public BgMatchSummaryOverviewParagraph HomeLast3GameParagraph { get; }
        public BgMatchSummaryOverviewParagraph AwayLastGameParagraph { get; }
        public BgMatchSummaryOverviewParagraph AwayLast3GameParagraph { get; }
        public BgMatchSummaryOverviewParagraph H2HGameParagraph { get; }
        public BgMatchSummaryOverviewParagraph HomeStatisticsParagraph { get; }
        public BgMatchSummaryOverviewParagraph HomePlayedAtHomeStatisticsParagraph { get; }
        public BgMatchSummaryOverviewParagraph AwayStatisticsParagraph { get; }
        public BgMatchSummaryOverviewParagraph AwayPlayedAwayStatisticsParagraph { get; }

        public BgMatchSummaryOverview(BgMatchSummaryOverviewParagraph overviewParagraph, 
            BgMatchSummaryOverviewParagraph oddsParagraph, 
            BgMatchSummaryOverviewParagraph homeLastGameParagraph, 
            BgMatchSummaryOverviewParagraph homeLast3GameParagraph, 
            BgMatchSummaryOverviewParagraph awayLastGameParagraph, 
            BgMatchSummaryOverviewParagraph awayLast3GameParagraph,
            BgMatchSummaryOverviewParagraph h2hGameParagraph,
            BgMatchSummaryOverviewParagraph homeStatisticsParagraph,
            BgMatchSummaryOverviewParagraph homePlayedAtHomeStatisticsParagraph,
            BgMatchSummaryOverviewParagraph awayStatisticsParagraph,
            BgMatchSummaryOverviewParagraph awayPlayedAwayStatisticsParagraph)
        {
            OverviewParagraph = overviewParagraph;
            OddsParagraph = oddsParagraph;
            HomeLastGameParagraph = homeLastGameParagraph;
            HomeLast3GameParagraph = homeLast3GameParagraph;
            AwayLastGameParagraph = awayLastGameParagraph;
            AwayLast3GameParagraph = awayLast3GameParagraph;
            H2HGameParagraph = h2hGameParagraph;
            HomeStatisticsParagraph = homeStatisticsParagraph;
            HomePlayedAtHomeStatisticsParagraph = homePlayedAtHomeStatisticsParagraph;
            AwayStatisticsParagraph = awayStatisticsParagraph;
            AwayPlayedAwayStatisticsParagraph = awayPlayedAwayStatisticsParagraph;
        }
    }
}
