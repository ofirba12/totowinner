﻿using System;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgCategoryMatch 
    {
        public DateTime ActualGameDate { get; }
        public string GameDate { get; }
        public int Index{ get; }
        public long MatchId { get; private set; }
        public int LeagueId { get; }
        public string League { get; }
        public string LeagueSymbol { get; }
        public int HomeTeamId { get; }
        public string HomeTeam { get; }
        public string HomeTeamSymbol { get; }
        public int AwayTeamId { get; }
        public string AwayTeam { get; }
        public string AwayTeamSymbol { get; }
        public int HomeScore { get;}
        public int AwayScore { get; }
        public string WdlResult { get; private set; }
        public decimal HomeOdd { get; }
        public decimal DrawOdd { get; }
        public decimal AwayOdd { get; }
        public int? HQ1 { get; }
        public int? AQ1 { get; }
        public int? HQ2 { get; }
        public int? AQ2 { get; }
        public int? HQ3 { get; }
        public int? AQ3 { get; }
        public int? HQ4 { get; }
        public int? AQ4 { get; }
        public BgDataMatchIndexes BetIndexes { get; }
        public BgCategoryMatch(DateTime actualGameDate,
            string gameDate,
            int index,
            long matchId,
            int leagueId,
            string league,
            string leagueSymbol,
            int homeId,
            string home,
            string homeSymbol,
            int awayId,
            string away,
            string awaySymbol,
            int homeScore,
            int awayScore,
            string wdlResult,
            decimal homeOdd,
            decimal drawOdd,
            decimal awayOdd,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null,
            BgDataMatchIndexes betIndexes = null)
        {
            this.Index = index;
            this.ActualGameDate = actualGameDate;
            this.GameDate = gameDate;
            this.MatchId = matchId;
            this.League = league;
            this.LeagueId = leagueId;
            this.LeagueSymbol = leagueSymbol;
            this.HomeTeamId = homeId;
            this.HomeTeam = home;
            this.HomeTeamSymbol = homeSymbol;
            this.AwayTeamId = awayId;
            this.AwayTeam = away;
            this.AwayTeamSymbol = awaySymbol;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.WdlResult = wdlResult;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
            this.BetIndexes = betIndexes;    
        }
        public void OvverideWDL(string wdl)
        {
            this.WdlResult = wdl;
        }
    }
}
