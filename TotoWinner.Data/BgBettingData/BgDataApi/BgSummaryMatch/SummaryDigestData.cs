﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class SummaryDigestData
    {
        public DigestData Home { get; }
        public DigestData Away { get; }

        public SummaryDigestData(DigestData home, DigestData away)
        {
            this.Home = home;
            this.Away = away;
        }
    }
}
