﻿namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgSummaryMatchDetails
    {
        public BgSummaryMatchInfo Info { get; }
        public BgSummaryMatchSummary Summary { get; }
        public BgSummaryMatchAnalysis Analysis { get; }
        public BgDataBetTips Tips { get; }
        public BgSummaryMatchDetails(BgSummaryMatchInfo info, 
            BgSummaryMatchSummary summary,
            BgSummaryMatchAnalysis analysis,
            BgDataBetTips tips)
        {
            this.Info = info;
            this.Summary = summary;
            this.Analysis = analysis;
            this.Tips = tips;
        }
    }
}
