﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class CategorizedMatches
    {
        public List<BgCategoryMatch> All { get; }
        public List<BgCategoryMatch> Favorite { get; }
        public List<BgCategoryMatch> Netral { get; }
        public List<BgCategoryMatch> Underdog { get; }
        public List<BgCategoryMatch> Home { get; }
        public List<BgCategoryMatch> HomeFavorite { get; }
        public List<BgCategoryMatch> HomeNetral { get; }
        public List<BgCategoryMatch> HomeUnderdog { get; }
        public List<BgCategoryMatch> Away { get; }
        public List<BgCategoryMatch> AwayFavorite { get; }
        public List<BgCategoryMatch> AwayNetral { get; }
        public List<BgCategoryMatch> AwayUnderdog { get; }

        public CategorizedMatches(List<BgCategoryMatch> all, 
            List<BgCategoryMatch> favorite, 
            List<BgCategoryMatch> netral, 
            List<BgCategoryMatch> underdog, 
            List<BgCategoryMatch> home, 
            List<BgCategoryMatch> homeFavorite, 
            List<BgCategoryMatch> homeNetral, 
            List<BgCategoryMatch> homeUnderdog, 
            List<BgCategoryMatch> away, 
            List<BgCategoryMatch> awayFavorite, 
            List<BgCategoryMatch> awayNetral, 
            List<BgCategoryMatch> awayUnderdog)
        {
            this.All = all;
            this.Favorite = favorite;
            this.Netral = netral;
            this.Underdog = underdog;
            this.Home = home;
            this.HomeFavorite = homeFavorite;
            this.HomeNetral = homeNetral;
            this.HomeUnderdog = homeUnderdog;
            this.Away = away;
            this.AwayFavorite = awayFavorite;
            this.AwayNetral = awayNetral;
            this.AwayUnderdog = awayUnderdog;
        }
    }
}
