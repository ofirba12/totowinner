﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class BgMatchSummaryOverviewParagraph
    {
        public List<string> Sentences { get; }

        public BgMatchSummaryOverviewParagraph(List<string> sentences)
        {
            Sentences = sentences;
        }
    }
}
