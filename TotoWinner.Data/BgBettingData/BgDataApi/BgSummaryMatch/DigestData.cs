﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class DigestData
    {
        public Dictionary<string, SoccerWinDrawLossScores> All { get; } //#last games, #wins,#draw,#loss
        public Dictionary<string, SoccerWinDrawLossScores> Favorite { get; }
        public Dictionary<string, SoccerWinDrawLossScores> Netral { get; }
        public Dictionary<string, SoccerWinDrawLossScores> Underdog { get; }
        public Dictionary<string, SoccerWinDrawLossScores> Home { get; }
        public Dictionary<string, SoccerWinDrawLossScores> HomeFavorite { get; }
        public Dictionary<string, SoccerWinDrawLossScores> HomeNetral { get; }
        public Dictionary<string, SoccerWinDrawLossScores> HomeUnderdog { get; }
        public Dictionary<string, SoccerWinDrawLossScores> Away { get; }
        public Dictionary<string, SoccerWinDrawLossScores> AwayFavorite { get; }
        public Dictionary<string, SoccerWinDrawLossScores> AwayNetral { get; }
        public Dictionary<string, SoccerWinDrawLossScores> AwayUnderdog { get; }

        public DigestData(Dictionary<string, SoccerWinDrawLossScores> all, 
            Dictionary<string, SoccerWinDrawLossScores> favorite, 
            Dictionary<string, SoccerWinDrawLossScores> netral, 
            Dictionary<string, SoccerWinDrawLossScores> underdog, 
            Dictionary<string, SoccerWinDrawLossScores> home, 
            Dictionary<string, SoccerWinDrawLossScores> homeFavorite, 
            Dictionary<string, SoccerWinDrawLossScores> homeNetral, 
            Dictionary<string, SoccerWinDrawLossScores> homeUnderdog, 
            Dictionary<string, SoccerWinDrawLossScores> away, 
            Dictionary<string, SoccerWinDrawLossScores> awayFavorite, 
            Dictionary<string, SoccerWinDrawLossScores> awayNetral, 
            Dictionary<string, SoccerWinDrawLossScores> awayUnderdog)
        {
            All = all;
            Favorite = favorite;
            Netral = netral;
            Underdog = underdog;
            Home = home;
            HomeFavorite = homeFavorite;
            HomeNetral = homeNetral;
            HomeUnderdog = homeUnderdog;
            Away = away;
            AwayFavorite = awayFavorite;
            AwayNetral = awayNetral;
            AwayUnderdog = awayUnderdog;
        }
    }
}
