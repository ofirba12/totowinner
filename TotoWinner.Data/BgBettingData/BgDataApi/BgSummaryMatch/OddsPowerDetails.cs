﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData.BgDataApi
{
    public class OddsPowerDetails
    {
        public decimal OddsPower { get; }
        public string Wdl { get; }
        public string GameDate { get; }

        public OddsPowerDetails(decimal oddsPower, string wdl, string gameDate)
        {
            OddsPower = oddsPower;
            Wdl = wdl;
            var date = DateTime.ParseExact(gameDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            GameDate = date.ToString("dd.MM");
        }
    }
}
