﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFullTimeH2HBetData : IBetDataH2HStatistics
    {
        public List<BgTeamFullTimeStatisticsEx> H2HCandidates { get; }
        public BgFullTimeH2HBetData(List<ITeamStatistics> h2hCandidates)
        {
            this.H2HCandidates = h2hCandidates.ConvertAll<BgTeamFullTimeStatisticsEx>(i => (BgTeamFullTimeStatisticsEx)i);
        }
    }
}
