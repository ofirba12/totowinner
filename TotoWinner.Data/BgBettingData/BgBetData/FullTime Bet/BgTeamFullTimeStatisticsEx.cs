﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamFullTimeStatisticsEx : BgTeamFullTimeStatistics
    {
        public BgTeamsFilter Filter { get; }
        public BgTeamFullTimeStatisticsEx(ITeamStatistics bgTeamFullTimeStatistics,
            BgTeamsFilter filter) : base(((BgTeamFullTimeStatistics)bgTeamFullTimeStatistics).TotalRows,
                ((BgTeamFullTimeStatistics)bgTeamFullTimeStatistics).TotalIndexW,
                ((BgTeamFullTimeStatistics)bgTeamFullTimeStatistics).TotalIndexD,
                ((BgTeamFullTimeStatistics)bgTeamFullTimeStatistics).TotalIndexL,
                ((BgTeamFullTimeStatistics)bgTeamFullTimeStatistics).PowerNumber)
        {
            this.Filter = filter;
        }
        public BgTeamFullTimeStatisticsEx(BgTeamsFilter filter) : base(-1, 0, 0, 0, 0)
        {
            Filter = filter;
        }
    }
}
