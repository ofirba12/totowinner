﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFullTimeHomeAwayBetData : IBetDataHomeAwayStatistics
    {
        public List<BgTeamFullTimeStatisticsEx> HomeCandidates { get; }
        public List<BgTeamFullTimeStatisticsEx> AwayCandidates { get; }
        public List<BgBetTotal> Totals { get; }
        public BgFullTimeHomeAwayBetData(List<ITeamStatistics> homeCandidates,
            List<ITeamStatistics> awayCandidates,
            List<BgBetTotal> totals)
        {
            this.HomeCandidates = homeCandidates.ConvertAll<BgTeamFullTimeStatisticsEx>(i => (BgTeamFullTimeStatisticsEx)i);
            this.AwayCandidates = awayCandidates.ConvertAll<BgTeamFullTimeStatisticsEx>(i => (BgTeamFullTimeStatisticsEx)i); 
            this.Totals = totals;
        }
    }
}
