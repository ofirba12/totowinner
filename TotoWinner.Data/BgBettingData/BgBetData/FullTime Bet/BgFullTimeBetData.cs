﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFullTimeBetData
    {
        public BgFullTimeHomeAwayBetData HomeWinAwayLose { get; }
        public BgFullTimeH2HBetData HomeWinH2H{ get; }
        public BgFullTimeHomeAwayBetData HomeDrawAwayDraw { get; }
        public BgFullTimeH2HBetData DrawH2H { get; }
        public BgFullTimeHomeAwayBetData AwayWinHomeLose {  get; }
        public BgFullTimeH2HBetData AwayWinH2H { get; }

        public BgFullTimeBetData(BgFullTimeHomeAwayBetData homeWinAwayLose,
            BgFullTimeH2HBetData homeWinH2H,
            BgFullTimeHomeAwayBetData homeDrawAwayDraw,
            BgFullTimeH2HBetData drawH2H,
            BgFullTimeHomeAwayBetData awayWinHomeLose,
            BgFullTimeH2HBetData awayWinH2H
            )
        {
            this.HomeWinAwayLose = homeWinAwayLose;
            this.HomeWinH2H = homeWinH2H;
            this.HomeDrawAwayDraw = homeDrawAwayDraw;
            this.DrawH2H = drawH2H;
            this.AwayWinHomeLose = awayWinHomeLose;
            this.AwayWinH2H = awayWinH2H;

        }
    }
}
