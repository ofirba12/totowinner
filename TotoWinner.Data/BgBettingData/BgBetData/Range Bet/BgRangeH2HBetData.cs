﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgRangeH2HBetData : IBetDataH2HStatistics
    {
        public List<BgTeamRangeStatisticsEx> H2HCandidates { get; }
        public BgRangeH2HBetData(List<ITeamStatistics> h2hCandidates)
        {
            this.H2HCandidates = h2hCandidates.ConvertAll<BgTeamRangeStatisticsEx>(i => (BgTeamRangeStatisticsEx)i);
        }
    }
}
