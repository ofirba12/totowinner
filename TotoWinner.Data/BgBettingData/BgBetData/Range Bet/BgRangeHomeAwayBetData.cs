﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgRangeHomeAwayBetData : IBetDataHomeAwayStatistics
    { 
        public List<BgTeamRangeStatisticsEx> HomeCandidates { get; }
        public List<BgTeamRangeStatisticsEx> AwayCandidates { get; }
        public List<BgBetTotal> Totals { get; }
        public BgRangeHomeAwayBetData(List<ITeamStatistics> homeCandidates,
            List<ITeamStatistics> awayCandidates,
            List<BgBetTotal> totals)
        {
            this.HomeCandidates = homeCandidates.ConvertAll<BgTeamRangeStatisticsEx>(i => (BgTeamRangeStatisticsEx)i);
            this.AwayCandidates = awayCandidates.ConvertAll<BgTeamRangeStatisticsEx>(i => (BgTeamRangeStatisticsEx)i); 
            this.Totals = totals;
        }
    }
}
