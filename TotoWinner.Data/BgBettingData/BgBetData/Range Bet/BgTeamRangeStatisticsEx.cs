﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamRangeStatisticsEx : BgTeamRangeStatistics
    {
        public BgTeamsFilter Filter { get; }
        public BgTeamRangeStatisticsEx(ITeamStatistics bgTeamStatistics,
            BgTeamsFilter filter) : base(((BgTeamRangeStatistics)bgTeamStatistics).TotalRows,
                ((BgTeamRangeStatistics)bgTeamStatistics).TotalBetween0and1,
                ((BgTeamRangeStatistics)bgTeamStatistics).TotalBetween2and3,
                ((BgTeamRangeStatistics)bgTeamStatistics).Total4Plus)
        {
            this.Filter = filter;
        }
        public BgTeamRangeStatisticsEx(BgTeamsFilter filter) : base(-1, 0, 0, 0)
        {
            Filter = filter;
        }
    }
}
