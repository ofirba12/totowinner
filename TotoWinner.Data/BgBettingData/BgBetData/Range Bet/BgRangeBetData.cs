﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgRangeBetData
    {
        public BgRangeHomeAwayBetData RangeBetween0and1 { get; }
        public BgRangeHomeAwayBetData RangeBetween2and3 { get; }
        public BgRangeHomeAwayBetData Range4Plus { get; }
        public BgRangeH2HBetData RangeBetween0and1H2H { get; }
        public BgRangeH2HBetData RangeBetween2and3H2H { get; }
        public BgRangeH2HBetData Range4PlusH2H { get; }

        public BgRangeBetData(BgRangeHomeAwayBetData rangeBetween0and1,
            BgRangeHomeAwayBetData rangeBetween2and3,
            BgRangeHomeAwayBetData range4Plus,
            BgRangeH2HBetData rangeBetween0and1H2H,
            BgRangeH2HBetData rangeBetween2and3H2H,
            BgRangeH2HBetData range4PlusH2H)
        {
            this.RangeBetween0and1 = rangeBetween0and1;
            this.RangeBetween2and3 = rangeBetween2and3;
            this.Range4Plus = range4Plus;
            this.RangeBetween0and1H2H = rangeBetween0and1H2H;
            this.RangeBetween2and3H2H = rangeBetween2and3H2H;
            this.Range4PlusH2H = range4PlusH2H;
        }
    }
}
