﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgWinDrawLoseBetData
    {
        public BgYesNoHomeAwayBetData WinDraw { get; }
        public BgYesNoHomeAwayBetData NoDraw { get; }
        public BgYesNoHomeAwayBetData DrawLose { get; }
        public BgYesNoH2HBetData WinDrawH2H { get; }
        public BgYesNoH2HBetData NoDrawH2H { get; }
        public BgYesNoH2HBetData DrawLoseH2H { get; }

        public BgWinDrawLoseBetData(BgYesNoHomeAwayBetData winDraw, 
            BgYesNoHomeAwayBetData noDraw,
            BgYesNoHomeAwayBetData drawLose,
            BgYesNoH2HBetData winDrawH2H,
            BgYesNoH2HBetData noDrawNoH2H,
            BgYesNoH2HBetData drawLoseH2H)
        {
            this.WinDraw = winDraw;
            this.NoDraw = noDraw;
            this.DrawLose = drawLose;
            this.WinDrawH2H = winDrawH2H;
            this.NoDrawH2H = noDrawNoH2H;
            this.DrawLoseH2H = drawLoseH2H;
        }
    }
}
