﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgBothScoreBetData
    {
        public BgYesNoHomeAwayBetData BothScoreYes { get; }
        public BgYesNoHomeAwayBetData BothScoreNo { get; }
        public BgYesNoH2HBetData BothScoreYesH2H { get; }
        public BgYesNoH2HBetData BothScoreNoH2H { get; }

        public BgBothScoreBetData(BgYesNoHomeAwayBetData bothYes,
            BgYesNoHomeAwayBetData bothNo,
            BgYesNoH2HBetData yesH2H,
            BgYesNoH2HBetData noH2H)
        {
            this.BothScoreYes = bothYes;
            this.BothScoreNo = bothNo;
            this.BothScoreYesH2H = yesH2H;
            this.BothScoreNoH2H = noH2H;
        }
    }
}
