﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgYesNoHomeAwayBetData : IBetDataHomeAwayStatistics
    {
        public List<BgTeamYesNoStatisticsEx> HomeCandidates { get; }
        public List<BgTeamYesNoStatisticsEx> AwayCandidates { get; }
        public List<BgBetTotal> Totals { get; }
        public BgYesNoHomeAwayBetData(List<ITeamStatistics> homeCandidates,
            List<ITeamStatistics> awayCandidates,
            List<BgBetTotal> totals)
        {
            this.HomeCandidates = homeCandidates.ConvertAll<BgTeamYesNoStatisticsEx>(i => (BgTeamYesNoStatisticsEx)i);
            this.AwayCandidates = awayCandidates.ConvertAll<BgTeamYesNoStatisticsEx>(i => (BgTeamYesNoStatisticsEx)i); 
            this.Totals = totals;
        }
    }
}
