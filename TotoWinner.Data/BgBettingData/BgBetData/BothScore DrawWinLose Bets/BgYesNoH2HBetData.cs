﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgYesNoH2HBetData : IBetDataH2HStatistics
    {
        public List<BgTeamYesNoStatisticsEx> H2HCandidates { get; }
        public BgYesNoH2HBetData(List<ITeamStatistics> h2hCandidates)
        {
            this.H2HCandidates = h2hCandidates.ConvertAll<BgTeamYesNoStatisticsEx>(i => (BgTeamYesNoStatisticsEx)i);
        }
    }
}
