﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamYesNoStatisticsEx : BgTeamYesNoStatistics
    {
        public BgTeamsFilter Filter { get; }
        public BgTeamYesNoStatisticsEx(ITeamStatistics bgTeamStatistics,
            BgTeamsFilter filter) : base(((BgTeamYesNoStatistics)bgTeamStatistics).TotalRows,
                ((BgTeamYesNoStatistics)bgTeamStatistics).Yes,
                ((BgTeamYesNoStatistics)bgTeamStatistics).No)
        {
            this.Filter = filter;
        }
        public BgTeamYesNoStatisticsEx(BgTeamsFilter filter) : base(-1, 0, 0)
        {
            Filter = filter;
        }
    }
}
