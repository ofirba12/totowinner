﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgBetTotal 
    {
        public int TotalRows { get; }
        public decimal TotalPercent { get; }

        public BgBetTotal(int totalRows, 
            decimal totalPercent)
        {
            this.TotalRows = totalRows;
            this.TotalPercent = totalPercent;
        }
    }
}
