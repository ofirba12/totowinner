﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgAllBetsRepository
    {
        public BgFullTimeBetData FullData { get; }
        public BgOverUnderBetData OverUnder35BetData { get; }
        public BgOverUnderBetData OverUnder25BetData { get; }
        public BgOverUnderBetData OverUnder15BetData { get; }
        public BgRangeBetData RangeBetData { get; }
        public BgBothScoreBetData BothScoreBetData { get; }
        public BgWinDrawLoseBetData WinDrawLoseBetData { get; }

        public BgAllBetsRepository(BgFullTimeBetData fullData, 
            BgOverUnderBetData overUnder35BetData, 
            BgOverUnderBetData overUnder25BetData, 
            BgOverUnderBetData overUnder15BetData, 
            BgRangeBetData rangeBetData, 
            BgBothScoreBetData bothScoreBetData, 
            BgWinDrawLoseBetData winDrawLoseBetData) 
        {
            this.FullData = fullData;
            this.OverUnder35BetData = overUnder35BetData;
            this.OverUnder25BetData = overUnder25BetData;
            this.OverUnder15BetData = overUnder15BetData;
            this.RangeBetData = rangeBetData;
            this.BothScoreBetData = bothScoreBetData;
            this.WinDrawLoseBetData = winDrawLoseBetData;
        }
    }
}
