﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgOverUnderBetData
    {
        public BgOverUnderHomeAwayBetData HomeOverAwayOver { get; }
        public BgOverUnderHomeAwayBetData HomeUnderAwayUnder { get; }
        public BgOverUnderH2HBetData OverH2H { get; }
        public BgOverUnderH2HBetData UnderH2H { get; }

        public BgOverUnderBetData(BgOverUnderHomeAwayBetData homeOverAwayOver,
            BgOverUnderHomeAwayBetData homeUnderAwayUnder,
            BgOverUnderH2HBetData overH2H,
            BgOverUnderH2HBetData underH2H)
        {
            this.HomeOverAwayOver = homeOverAwayOver;
            this.HomeUnderAwayUnder = homeUnderAwayUnder;
            this.OverH2H = overH2H;
            this.UnderH2H = underH2H;
        }
    }
}
