﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgOverUnderHomeAwayBetData : IBetDataHomeAwayStatistics
    {
        public List<BgTeamOverUnderStatisticsEx> HomeCandidates { get; }
        public List<BgTeamOverUnderStatisticsEx> AwayCandidates { get; }
        public List<BgBetTotal> Totals { get; }
        public BgOverUnderHomeAwayBetData(List<ITeamStatistics> homeCandidates,
            List<ITeamStatistics> awayCandidates,
            List<BgBetTotal> totals)
        {
            this.HomeCandidates = homeCandidates.ConvertAll<BgTeamOverUnderStatisticsEx>(i => (BgTeamOverUnderStatisticsEx)i);
            this.AwayCandidates = awayCandidates.ConvertAll<BgTeamOverUnderStatisticsEx>(i => (BgTeamOverUnderStatisticsEx)i); 
            this.Totals = totals;
        }
    }
}
