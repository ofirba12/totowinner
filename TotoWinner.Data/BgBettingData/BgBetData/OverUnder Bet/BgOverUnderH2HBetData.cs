﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgOverUnderH2HBetData : IBetDataH2HStatistics
    {
        public List<BgTeamOverUnderStatisticsEx> H2HCandidates { get; }
        public BgOverUnderH2HBetData(List<ITeamStatistics> h2hCandidates)
        {
            this.H2HCandidates = h2hCandidates.ConvertAll<BgTeamOverUnderStatisticsEx>(i => (BgTeamOverUnderStatisticsEx)i);
        }
    }
}
