﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamOverUnderStatisticsEx : BgTeamOverUnderStatistics
    {
        public BgTeamsFilter Filter { get; }
        public BgTeamOverUnderStatisticsEx(ITeamStatistics bgTeamStatistics,
            BgTeamsFilter filter) : base(((BgTeamOverUnderStatistics)bgTeamStatistics).TotalRows,
                ((BgTeamOverUnderStatistics)bgTeamStatistics).TotalIndexOver,
                ((BgTeamOverUnderStatistics)bgTeamStatistics).TotalIndexUnder)
        {
            this.Filter = filter;
        }
        public BgTeamOverUnderStatisticsEx(BgTeamsFilter filter) : base(-1, 0, 0)
        {
            Filter = filter;
        }
    }
}
