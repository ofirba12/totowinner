﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BasketballQuatersScores : IEquatable<BasketballQuatersScores>
    {
        public int LocalTeamQ1Score { get; }
        public int LocalTeamQ2Score { get; }
        public int LocalTeamQ3Score { get; }
        public int LocalTeamQ4Score { get; }
        public int AwayTeamQ1Score { get; }
        public int AwayTeamQ2Score { get; }
        public int AwayTeamQ3Score { get; }
        public int AwayTeamQ4Score { get; }

        public BasketballQuatersScores(int localTeamQ1,
            int localTeamQ2,
            int localTeamQ3,
            int localTeamQ4,
            int AwayTeamQ1,
            int AwayTeamQ2,
            int AwayTeamQ3,
            int AwayTeamQ4)
        {
            this.LocalTeamQ1Score = localTeamQ1;
            this.LocalTeamQ2Score = localTeamQ2;
            this.LocalTeamQ3Score = localTeamQ3;
            this.LocalTeamQ4Score = localTeamQ4;
            this.AwayTeamQ1Score = AwayTeamQ1;
            this.AwayTeamQ2Score = AwayTeamQ2;
            this.AwayTeamQ3Score = AwayTeamQ3;
            this.AwayTeamQ4Score = AwayTeamQ4;
        }

        public bool Equals(BasketballQuatersScores other)
        {
            return this.LocalTeamQ1Score == other.LocalTeamQ1Score
                && this.LocalTeamQ2Score == other.LocalTeamQ2Score
                && this.LocalTeamQ3Score == other.LocalTeamQ3Score
                && this.LocalTeamQ4Score == other.LocalTeamQ4Score
                && this.AwayTeamQ1Score == other.AwayTeamQ1Score
                && this.AwayTeamQ2Score == other.AwayTeamQ2Score
                && this.AwayTeamQ3Score == other.AwayTeamQ3Score
                && this.AwayTeamQ4Score == other.AwayTeamQ4Score;
        }
        public override string ToString()
        {
            var msg = $"LocalTeamQ1Score={LocalTeamQ1Score},LocalTeamQ2Score={LocalTeamQ2Score},LocalTeamQ3Score={LocalTeamQ3Score},LocalTeamQ4Score={LocalTeamQ4Score}";
            msg = $"{msg},AwayTeamQ1Score={AwayTeamQ1Score},AwayTeamQ2Score={AwayTeamQ2Score},AwayTeamQ3Score={AwayTeamQ3Score},AwayTeamQ4Score={AwayTeamQ4Score}";
            return msg;
        }
    }
}
