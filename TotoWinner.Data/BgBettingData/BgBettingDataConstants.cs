﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public static class BgBettingDataConstants
    {
        public const string RepositoryPath = "Repository";
        public const string ResultPath = "Result";
    }
    #region Goalserve Parser
    public enum GoalServeMatchDayFetch
    {
        D_minus_1 = -1,
        Today = 0,
        D_plus_1 = 1,
        D_plus_2 = 2
    }
    public enum GoalServeSportType
    {
        Soccer = 0,
        Basketball,
        Tennis,
        Baseball,
        Football,
        Handball
    }
    public enum GoalServeUrlType
    {
        Matches,
        Odds
    }
    public enum GoalServeDataAttributes
    {
        LeagueName,
        LeagueId,
        CountryName,
        CountryId,
        IsCup,
        MatchId,
        MatchDate,
        MatchStatus,
        MatchTime,
        MatchTeamName,
        MatchTeamId,
        MatchTeamTotalScore,
        localTeam,
        VisitorTeam,
        Q1Score,
        Q2Score,
        Q3Score,
        Q4Score
    }
    #endregion
    public enum BgSourceProvider
    {
        Goldserve = 1,
        Manual = 2
    }
    public enum BgSport
    {
        Soccer = 0,
        Basketball = 1,
        Tennis = 2,
        Baseball = 3,
        Football = 4,
        Handball = 5
    }
    public enum BgMapperType
    {
        Countries = 1,
        Leagues = 2,
        Teams = 3
    }
    public enum BgMapperItems
    {
        Country,
        League,
        HomeTeam,
        AwayTeam
    }
    #region BgSystem
    public enum BgSystemField
    {
        GoalserveStartGeneration,
        GoalserveFinishGeneration,
        ManualProviderStartGeneration,
        ManualProviderFinishGeneration,
        BgFeedStartGeneration,
        BgFeedFinishGeneration,
        BgDataWatcherStartAnalysis,
        BgDataWatcherFinishAnalysis,
        BgMatchStatisticsStartGeneration,
        BgMatchStatisticsFinishGeneration,
        WinnerDataStartGeneration,
        WinnerDataFinishGeneration
    }
    public enum BgSystemFieldType
    {
        DateTime = 1,
        Int = 2,
        Bool = 3
    }
    public enum BgTeamsFilter
    {
        All_All = 1,
        All_Favorite = 2,
        All_Netral = 3,
        All_Underdog = 4,
        Home_All = 5,
        Home_Favorite = 6,
        Home_Netral = 7,
        Home_Underdog = 8,
        Away_All = 9,
        Away_Favorite = 10,
        Away_Netral = 11,
        Away_Underdog = 12
    }
    public enum BgTeamClasification
    {
        Favorite,
        Netral,
        Underdog
    }
    public enum BgWDLIndex
    {
        W,
        D,
        L
    }
    public enum BgOverUnderIndex
    {
        Over,
        Under
    }
    public enum BgRangeIndex
    {
        [Description("0-1")]
        Range_0_1,
        [Description("2-3")]
        Range_2_3,
        [Description("4+")]
        Range_4Plus
    }
    public enum BgBetType
    {
        FullTime,
        OverUnder3_5,
        OverUnder2_5,
        OverUnder1_5,
        Range,
        BothTeamScore,
        DrawWin,
        NoDraw,
        DrawLose
    }
    public enum BgBetTypeSide
    {
        Home,
        Away,
        H2H
    }
    public enum BgBetTypeComponent
    {
        FullTimeHomeWinAwayLose,
        FullTimeDraw,
        FullTimeAwayWinHomeLose,
        Over3_5HomeAway,
        Under3_5HomeAway,
        Over2_5HomeAway,
        Under2_5HomeAway,
        Over1_5HomeAway,
        Under1_5HomeAway,
        Range0_1HomeAway,
        Range2_3HomeAway,
        Range4PlusHomeAway,
        BothScoreYes,
        BothScoreNo,
        WinHomeDrawAway,
        WinOrDraw,
        HomeAwayNoDraw,
        DrawOrLose
    }
    public enum BgStatisticsDetailType
    {
        FullTime__HomeWin__HomeWin = 1,
        FullTime__HomeWin__AwayLoose = 2,
        FullTime__HomeWin__H2H = 3,
        FullTime__Draw__HomeDraw,
        FullTime__Draw__AwayDraw,
        FullTime__Draw__H2H,
        FullTime__AwayWin__HomeLoose,
        FullTime__AwayWin__AwayWin,
        FullTime__AwayWin__H2H,
        OverUnder__Over3_5__HomeOver,
        OverUnder__Over3_5__AwayOver,
        OverUnder__Under3_5__HomeUnder,
        OverUnder__Under3_5__AwayUnder,
        OverUnder__Over3_5__H2H,
        OverUnder__Under3_5__H2H,
        OverUnder__Over2_5__HomeOver,
        OverUnder__Over2_5__AwayOver,
        OverUnder__Under2_5__HomeUnder,
        OverUnder__Under2_5__AwayUnder,
        OverUnder__Over2_5__H2H,
        OverUnder__Under2_5__H2H,
        OverUnder__Over1_5__HomeOver,
        OverUnder__Over1_5__AwayOver,
        OverUnder__Under1_5__HomeUnder,
        OverUnder__Under1_5__AwayUnder,
        OverUnder__Over1_5__H2H,
        OverUnder__Under1_5__H2H,
        Range__InRange0_1__Home,
        Range__InRange0_1__Away,
        Range__InRange0_1__H2H,
        Range__InRange2_3__Home,
        Range__InRange2_3__Away,
        Range__InRange2_3__H2H,
        Range__InRange4Pplus__Home,
        Range__InRange4Pplus__Away,
        Range__InRange4Pplus__H2H,
        BothScore__Yes__Home,
        BothScore__Yes__Away,
        BothScore__Yes__H2H,
        BothScore__No__Home,
        BothScore__No__Away,
        BothScore__No__H2H,
        WinDrawLose__WinDraw__HomeWinDraw,
        WinDrawLose__WinDraw__AwayDrawLose,
        WinDrawLose__WinDraw__H2H,
        WinDrawLose__NoDraw__Home,
        WinDrawLose__NoDraw__Away,
        WinDrawLose__NoDraw__H2H,
        WinDrawLose__DrawLose__Home,
        WinDrawLose__DrawLose__Away,
        WinDrawLose__DrawLose__H2H = 51
    }
    public enum BgImpliedStatisticType
    {
        All_All_1,
        All_All_X,
        All_All_2,
        All_All_Under1_5,
        All_All_Over1_5,
        All_All_Under2_5,
        All_All_Over2_5,
        All_All_Under3_5,
        All_All_Over3_5,
        All_All_0_1,
        All_All_2_3,
        All_All_4Plus,
        All_All_BothScore,
        All_All_NoBothScore,
        All_All_1X,
        All_All_12,
        All_All_2X,
        Avg_1,
        Avg_X,
        Avg_2,
        Avg_Under1_5,
        Avg_Over1_5,
        Avg_Under2_5,
        Avg_Over2_5,
        Avg_Under3_5,
        Avg_Over3_5,
        Avg_0_1,
        Avg_2_3,
        Avg_4Plus,
        Avg_BothScore,
        Avg_BothNoScore,
        Avg_1X,
        Avg_12,
        Avg_2X,
        Web_1,
        Web_X,
        Web_2,
        Web_Under1_5,
        Web_Over1_5,
        Web_Under2_5,
        Web_Over2_5,
        Web_Under3_5,
        Web_Over3_5,
        Web_0_1,
        Web_2_3,
        Web_4Plus,
        Web_BothScore,
        Web_BothNoScore,
        Web_1X,
        Web_12,
        Web_2X
    }
    public enum BgTipType
    {
        FullTime,
        OverUnder3_5,
        OverUnder2_5,
        OverUnder1_5,
        Range,
        BothTeamScore,
        DoubleBet
    }
    #endregion
}
