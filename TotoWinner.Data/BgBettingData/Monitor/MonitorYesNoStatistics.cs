﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorYesNoStatistics
    {
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterLeft { get; }
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterRight { get; }

        [ColumnStyle(Width = 75, DisplayName = "Total Rows")]
        public string TotalRows { get; }
        [ColumnStyle(Width = 50, DisplayName = "Yes")]
        public string TotalYes { get; }
        [ColumnStyle(Width = 50, DisplayName = "No")]
        public string TotalNo { get; }
        [ColumnStyle(Width = 50, DisplayName = "Yes %")]
        public string TotalYesPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "No %")]
        public string TotalNoPercent { get; }
        [ColumnStyle(Hide = true)]
        public bool DataFound { get; }

        public MonitorYesNoStatistics(Tuple<string, string> filter, BgTeamYesNoStatistics value)
        {
            this.DataFound = value != null;
            this.FilterLeft = filter.Item1;
            this.FilterRight = filter.Item2;

            if (value != null)
            {
                this.TotalRows = value.TotalRows.ToString();
                this.TotalYes = value.Yes.ToString();
                this.TotalNo = value.No.ToString();
                this.TotalYesPercent = Decimal.ToInt32(value.YesPercent).ToString();//.ToString("0.##");
                this.TotalNoPercent = Decimal.ToInt32(value.NoPercent).ToString();//.ToString("0.##");
            }
        }
    }
}
