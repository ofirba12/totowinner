﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorOverUnderStatistics
    {
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterLeft { get; }
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterRight { get; }

        [ColumnStyle(Width = 75, DisplayName = "Total Rows")]
        public string TotalRows { get; }
        [ColumnStyle(Width = 50, DisplayName = "Over")]
        public string TotalOver { get; }
        [ColumnStyle(Width = 50, DisplayName = "Under")]
        public string TotalUnder { get; }
        [ColumnStyle(Width = 50, DisplayName = "Over%")]
        public string TotalOverPercent { get; }
        [ColumnStyle(Width = 55, DisplayName = "Under%")]
        public string TotalUnderPercent { get; }
        [ColumnStyle(Hide = true)]
        public bool DataFound { get; }

        public MonitorOverUnderStatistics(Tuple<string, string> filter, BgTeamOverUnderStatistics value)
        {
            this.DataFound = value != null;
            this.FilterLeft = filter.Item1;
            this.FilterRight = filter.Item2;

            if (value != null)
            {
                this.TotalRows = value.TotalRows.ToString();
                this.TotalOver = value.TotalIndexOver.ToString();
                this.TotalUnder = value.TotalIndexUnder.ToString();
                this.TotalOverPercent = Decimal.ToInt32(value.TotalOverPercent).ToString();//.ToString("0.##");
                this.TotalUnderPercent = Decimal.ToInt32(value.TotalUnderPercent).ToString();//.ToString("0.##");
            }
        }
    }
}
