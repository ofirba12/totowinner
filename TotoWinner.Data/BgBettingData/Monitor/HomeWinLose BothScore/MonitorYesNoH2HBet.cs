﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorYesNoH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "Populate", DisableSort = true)]
        public string Populate { get; }
        [ColumnStyle(Width = 50, DisplayName = "Lines", DisableSort = true)]
        public string Lines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Bet %", DisableSort = true)]
        public string Percent { get; }

        public MonitorYesNoH2HBet(BgBetTypeComponent type, BgYesNoH2HBetData data, int index)
        {
            this.Populate = data.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.H2HCandidates[index].TotalRows > 0)
            {
                this.Lines = data.H2HCandidates[index].TotalRows.ToString();
                switch (type)
                {
                    case BgBetTypeComponent.BothScoreYes:
                        this.Percent = data.H2HCandidates[index].YesPercent.ToString();
                        break;
                    case BgBetTypeComponent.BothScoreNo:
                        this.Percent = data.H2HCandidates[index].NoPercent.ToString();
                        break;
                    default:
                        this.Percent = data.H2HCandidates[index].YesPercent.ToString();
                        break;
                }
            }
            else
            {
                this.Lines = this.Percent = string.Empty;
            }
        }
    }
}
