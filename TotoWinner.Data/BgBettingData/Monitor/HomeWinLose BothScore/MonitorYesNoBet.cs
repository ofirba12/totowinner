﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorYesNoBet
    {
        [ColumnStyle(Width = 120, DisplayName = "H Populate", DisableSort = true)]
        public string HomePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "H Lines", DisableSort = true)]
        public string HomeLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Home %", DisableSort = true)]
        public string HomePercent { get; }
        [ColumnStyle(Width = 120, DisplayName = "A Populate", DisableSort = true)]
        public string AwayPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "A Lines", DisableSort = true)]
        public string AwayLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Away %", DisableSort = true)]
        public string AwayPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 60, DisplayName = "Total %", DisableSort = true)]
        public string TotalPercent { get; }

        public MonitorYesNoBet(BgBetTypeComponent type, BgYesNoHomeAwayBetData data, int index)
        {
            this.HomePopulate = data.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeLines = data.HomeCandidates[index].TotalRows.ToString();
                switch(type)
                {
                    case BgBetTypeComponent.BothScoreYes:
                        this.HomePercent = data.HomeCandidates[index].YesPercent.ToString();
                        break;
                    case BgBetTypeComponent.BothScoreNo:
                        this.HomePercent = data.HomeCandidates[index].NoPercent.ToString();
                        break;
                    default:
                        this.HomePercent = data.HomeCandidates[index].YesPercent.ToString();
                        break;
                }
            }
            else
            {
                this.HomePopulate = this.HomeLines = this.HomePercent = string.Empty; 
            }
            this.AwayPopulate = data.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLines = data.AwayCandidates[index].TotalRows.ToString();
                switch(type)
                {
                    case BgBetTypeComponent.BothScoreYes:
                        this.AwayPercent = data.AwayCandidates[index].YesPercent.ToString();
                        break;
                    case BgBetTypeComponent.BothScoreNo:
                        this.AwayPercent = data.AwayCandidates[index].NoPercent.ToString();
                        break;
                    default:
                        this.AwayPercent = data.AwayCandidates[index].YesPercent.ToString();
                        break;
                }
            }
            else
            {
                this.AwayPopulate = this.AwayLines = this.AwayPercent = string.Empty; 
            }
            this.TotalLine = data.Totals[index].TotalRows.ToString();
            this.TotalPercent = data.Totals[index].TotalPercent.ToString();
        }
    }
}
