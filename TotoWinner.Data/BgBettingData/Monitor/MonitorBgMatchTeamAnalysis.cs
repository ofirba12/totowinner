﻿using System;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorBgMatchTeamAnalysis
    {
        public DateTime GameDate { get; }
        [ColumnStyle(Width = 50)]
        public string Time { get; }
        public string Country { get; }
        public string League { get; }
        [ColumnStyle(Width = 75)]
        public string Status { get; }
        public string Home { get; }
        public string Away { get; }
        [ColumnStyle(Width = 75)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Width = 75)]
        public int? HomeScore { get; }
        [ColumnStyle(Width = 75)]
        public int? AwayScore { get; }
        public BgSourceProvider Source { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ4 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ4 { get; }
        [ColumnStyle(Width = 150)]
        public DateTime LastUpdate { get; }
        public long? MatchId { get; }
        public int? CountryId { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public int HomeId { get; }
        public int AwayId { get; }
        public BgSport Sport { get; }
        public bool MissingOdds { get; }
        public bool MissingScores { get; }
        public bool MissingQuarters { get; }
        public DateTime GameTime { get; }
        public MonitorBgMatchTeamAnalysis(BgSourceProvider source,
            BgSport sport,
            long? matchId,
            string status,
            DateTime lastUpdate,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameDate,
            string home,
            int homeId,
            string away,
            int awayId,
            bool missingOdds,
            bool missingScores,
            bool missingQuarters,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null
            )
        {
            this.Source = source;
            this.Sport = sport;
            this.MatchId = matchId;
            this.Status = status;
            this.LastUpdate = lastUpdate;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.Time = gameDate.ToString("HH:mm");
            this.GameDate = gameDate.Date;
            this.GameTime = gameDate;//.ToString("dd/MM/yyyy");
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.MissingOdds = missingOdds;
            this.MissingScores = missingScores;
            this.MissingQuarters = missingQuarters;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
        }
        public MonitorBgMatchTeamAnalysis(BgMatchForAnalysis bgMatchForAnalysis) : this (bgMatchForAnalysis.Source,
            bgMatchForAnalysis.Sport,
            bgMatchForAnalysis.MatchId,
            bgMatchForAnalysis.Status,
            bgMatchForAnalysis.LastUpdate,
            bgMatchForAnalysis.CountryId,
            bgMatchForAnalysis.Country,
            bgMatchForAnalysis.League,
            bgMatchForAnalysis.LeagueId,
            bgMatchForAnalysis.IsCup,
            bgMatchForAnalysis.GameStart,
            bgMatchForAnalysis.Home,
            bgMatchForAnalysis.HomeId,
            bgMatchForAnalysis.Away,
            bgMatchForAnalysis.AwayId,
            bgMatchForAnalysis.MissingOdds,
            bgMatchForAnalysis.MissingScores,
            bgMatchForAnalysis.MissingQuarters,
            bgMatchForAnalysis.HomeOdd, bgMatchForAnalysis.DrawOdd, bgMatchForAnalysis.AwayOdd,
            bgMatchForAnalysis.HomeScore, bgMatchForAnalysis.AwayScore,
            bgMatchForAnalysis.HQ1, bgMatchForAnalysis.AQ1,
            bgMatchForAnalysis.HQ2, bgMatchForAnalysis.AQ2,
            bgMatchForAnalysis.HQ3, bgMatchForAnalysis.AQ3,
            bgMatchForAnalysis.HQ4, bgMatchForAnalysis.AQ4
            )
        {
        }
    }
}
