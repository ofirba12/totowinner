﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorRangeHomeAwayBet
    {
        [ColumnStyle(Width = 120, DisplayName = "HO Populate", DisableSort = true)]
        public string HomePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HO Lines", DisableSort = true)]
        public string HomeLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HO InRange%", DisableSort = true)]
        public string HomePercent { get; }
        [ColumnStyle(Width = 120, DisplayName = "AO Populate", DisableSort = true)]
        public string AwayPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AO Lines", DisableSort = true)]
        public string AwayLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AO InRange%", DisableSort = true)]
        public string AwayPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 60, DisplayName = "Total InRange%", DisableSort = true)]
        public string TotalPercent { get; }

        public MonitorRangeHomeAwayBet(BgBetTypeComponent type, BgRangeHomeAwayBetData data, int index)
        {
            this.HomePopulate = data.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeLines = data.HomeCandidates[index].TotalRows.ToString();
                switch (type)
                {
                    case BgBetTypeComponent.Range0_1HomeAway:
                        this.HomePercent = data.HomeCandidates[index].TotalBetween0and1Percent.ToString();
                        break;
                    case BgBetTypeComponent.Range2_3HomeAway:
                        this.HomePercent = data.HomeCandidates[index].TotalBetween2and3Percent.ToString();
                        break;
                    case BgBetTypeComponent.Range4PlusHomeAway:
                        this.HomePercent = data.HomeCandidates[index].Total4PlusPercent.ToString();
                        break;
                }
            }
            else
            {
                this.HomePopulate = this.HomeLines = this.HomePercent = string.Empty; 
            }
            this.AwayPopulate = data.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLines = data.AwayCandidates[index].TotalRows.ToString();
                switch (type)
                {
                    case BgBetTypeComponent.Range0_1HomeAway:
                        this.AwayPercent = data.AwayCandidates[index].TotalBetween0and1Percent.ToString();
                        break;
                    case BgBetTypeComponent.Range2_3HomeAway:
                        this.AwayPercent = data.AwayCandidates[index].TotalBetween2and3Percent.ToString();
                        break;
                    case BgBetTypeComponent.Range4PlusHomeAway:
                        this.AwayPercent = data.AwayCandidates[index].Total4PlusPercent.ToString();
                        break;
                }
            }
            else
            {
                this.AwayPopulate = this.AwayLines = this.AwayPercent = string.Empty; 
            }
            this.TotalLine = data.Totals[index].TotalRows.ToString();
            this.TotalPercent = data.Totals[index].TotalPercent.ToString();
        }
    }
}
