﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorRangeH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "Populate", DisableSort = true)]
        public string Populate { get; }
        [ColumnStyle(Width = 50, DisplayName = "Lines", DisableSort = true)]
        public string Lines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Range%", DisableSort = true)]
        public string Percent { get; }

        public MonitorRangeH2HBet(BgBetTypeComponent type, BgRangeH2HBetData data, int index)
        {
            this.Populate = data.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.H2HCandidates[index].TotalRows > 0)
            {
                this.Lines = data.H2HCandidates[index].TotalRows.ToString();
                switch (type)
                {
                    case BgBetTypeComponent.Range0_1HomeAway:
                        this.Percent = data.H2HCandidates[index].TotalBetween0and1Percent.ToString();
                        break;
                    case BgBetTypeComponent.Range2_3HomeAway:
                        this.Percent = data.H2HCandidates[index].TotalBetween2and3Percent.ToString();
                        break;
                    case BgBetTypeComponent.Range4PlusHomeAway:
                        this.Percent = data.H2HCandidates[index].Total4PlusPercent.ToString();
                        break;
                }
            }
            else
            {
                this.Lines = this.Percent = string.Empty;
            }
        }
    }
}
