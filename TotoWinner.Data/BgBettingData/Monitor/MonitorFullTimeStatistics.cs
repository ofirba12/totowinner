﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeStatistics
    {
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterLeft { get; }
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterRight { get; }

        [ColumnStyle(Width = 75, DisplayName = "Total Rows")]
        public string TotalRows { get; }
        [ColumnStyle(Width = 50, DisplayName = "W")]
        public string TotalIndexW { get; }
        [ColumnStyle(Width = 50, DisplayName = "D")]
        public string TotalIndexD { get; }
        [ColumnStyle(Width = 50, DisplayName = "L")]
        public string TotalIndexL { get; }
        [ColumnStyle(Width = 50, DisplayName = "W%")]
        public string TotalIndexPercentW { get; }
        [ColumnStyle(Width = 50, DisplayName = "D%")]
        public string TotalIndexPercentD { get; }
        [ColumnStyle(Width = 50, DisplayName = "L%")]
        public string TotalIndexPercentL { get; }
        [ColumnStyle(Width = 75, DisplayName = "Power Number")]
        public string PowerNumber { get; }
        [ColumnStyle(Hide = true)]
        public bool DataFound { get; }

        public MonitorFullTimeStatistics(Tuple<string, string> filter, BgTeamFullTimeStatistics value)
        {
            this.DataFound = value != null;
            this.FilterLeft = filter.Item1;
            this.FilterRight = filter.Item2;
            if (value != null)
            {
                this.TotalRows = value.TotalRows.ToString();
                this.TotalIndexW = value.TotalIndexW.ToString();
                this.TotalIndexD = value.TotalIndexD.ToString();
                this.TotalIndexL = value.TotalIndexL.ToString();
                this.TotalIndexPercentW = Decimal.ToInt32(value.TotalIndexPercentW).ToString();//.ToString("0.##");
                this.TotalIndexPercentD = Decimal.ToInt32(value.TotalIndexPercentD).ToString();//.ToString("0.##");
                this.TotalIndexPercentL = Decimal.ToInt32(value.TotalIndexPercentL).ToString();//.ToString("0.##");
                this.PowerNumber = value.PowerNumber.ToString();
            }
        }
    }
}
