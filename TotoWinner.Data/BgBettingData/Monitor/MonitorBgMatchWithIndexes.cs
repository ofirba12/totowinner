﻿using System;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorBgMatchWithIndexes
    {
        public DateTime GameDate { get; }
        [ColumnStyle(Width = 50)]
        public string Time { get; }
        public string Country { get; }
        public string League { get; }
        [ColumnStyle(Width = 75)] 
        public string Status { get; }
        public string Home { get; }
        public string Away { get; }
        [ColumnStyle(Width = 75)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Width = 75)]
        public int? HomeScore { get; }
        [ColumnStyle(Width = 75)]
        public int? AwayScore { get; }
        [ColumnStyle(Width = 50)]
        public BgWDLIndex WDL { get; }
        public decimal OddPower { get; }
        [ColumnStyle(Width = 75, DisplayName = "OU 3.5")]
        public BgOverUnderIndex OverUnder3_5Index { get; }
        [ColumnStyle(Width = 75, DisplayName = "OU 2.5")] 
        public BgOverUnderIndex OverUnder2_5Index { get; }
        [ColumnStyle(Width = 75, DisplayName = "OU 1.5")]
        public BgOverUnderIndex OverUnder1_5Index { get; }
        [ColumnStyle(Width = 100, DisplayName = "Range 2-3")]
        public BgRangeIndex RangeIndex { get; }
        [ColumnStyle(Width = 110, DisplayName = "Both Score")]
        public bool BothScoreIndex { get; }
        [ColumnStyle(Width = 120, DisplayName = "Draw & Win")]
        public bool DrawWinIndex { get; }
        [ColumnStyle(Width = 100, DisplayName = "No Draw")]
        public bool NoDrawIndex { get; }
        [ColumnStyle(Width = 120, DisplayName = "Lose & Draw")]
        public bool LoseDrawIndex { get; }
        [ColumnStyle(Hide = true)]
        public BgSport Sport { get; }
        public MonitorBgMatchWithIndexes(BgSport sport,
            string status,
            string country,
            string league,
            DateTime gameDate,
            string home,
            string away,
            decimal? homeOdd,
            decimal? drawOdd,
            decimal? awayOdd,
            int? homeScore,
            int? awayScore,
            BgWDLIndex wDL,
            decimal oddPower,
            BgOverUnderIndex overUnder3_5Index,
            BgOverUnderIndex overUnder2_5Index,
            BgOverUnderIndex overUnder1_5Index,
            BgRangeIndex rangeIndex,
            bool bothScoreIndex,
            bool drawWinIndex,
            bool noDrawIndex,
            bool loseDrawIndex
            )
        {
            this.Status = status;
            this.Country = country;
            this.League = league;
            this.Time = gameDate.ToString("HH:mm");
            this.GameDate = gameDate.Date;
            this.Home = home;
            this.Away = away;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.WDL = wDL;
            this.OddPower = oddPower;
            this.OverUnder3_5Index = overUnder3_5Index;
            this.OverUnder2_5Index = overUnder2_5Index;
            this.OverUnder1_5Index = overUnder1_5Index;
            this.RangeIndex = rangeIndex;
            this.BothScoreIndex = bothScoreIndex;
            this.DrawWinIndex = drawWinIndex;
            this.NoDrawIndex = noDrawIndex;
            this.LoseDrawIndex = loseDrawIndex;
            this.Sport = sport;
        }
        public MonitorBgMatchWithIndexes(BgMatchWithIndexes bgMatchWithIndexes) : this(bgMatchWithIndexes.Sport,
                        bgMatchWithIndexes.Status,
                        bgMatchWithIndexes.Country,
                        bgMatchWithIndexes.League,
                        bgMatchWithIndexes.GameStart,
                        bgMatchWithIndexes.Home,
                        bgMatchWithIndexes.Away,
                        bgMatchWithIndexes.HomeOdd, bgMatchWithIndexes.DrawOdd, bgMatchWithIndexes.AwayOdd,
                        bgMatchWithIndexes.HomeScore, bgMatchWithIndexes.AwayScore,
                        bgMatchWithIndexes.Indexes.WDL,
                        bgMatchWithIndexes.Indexes.OddPower,
                        bgMatchWithIndexes.Indexes.OverUnder3_5Index,
                        bgMatchWithIndexes.Indexes.OverUnder2_5Index,
                        bgMatchWithIndexes.Indexes.OverUnder1_5Index,
                        bgMatchWithIndexes.Indexes.RangeIndex,
                        bgMatchWithIndexes.Indexes.BothScoreIndex,
                        bgMatchWithIndexes.Indexes.DrawWinIndex,
                        bgMatchWithIndexes.Indexes.NoDrawIndex,
                        bgMatchWithIndexes.Indexes.LoseDrawIndex
            )
        {}
    }
}
