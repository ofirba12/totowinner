﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorUnderH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "Under Populate", DisableSort = true)]
        public string Populate { get; }
        [ColumnStyle(Width = 50, DisplayName = "Under Lines", DisableSort = true)]
        public string Lines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Under%", DisableSort = true)]
        public string Percent { get; }

        public MonitorUnderH2HBet(BgOverUnderBetData data, int index)
        {
            this.Populate = data.UnderH2H.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.UnderH2H.H2HCandidates[index].TotalRows > 0)
            {
                this.Lines = data.UnderH2H.H2HCandidates[index].TotalRows.ToString();
                this.Percent = data.UnderH2H.H2HCandidates[index].TotalUnderPercent.ToString();
            }
            else
            {
                this.Lines = this.Percent = string.Empty; 
            }
        }
    }
}
