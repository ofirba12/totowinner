﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorOverH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "Over Populate", DisableSort = true)]
        public string Populate { get; }
        [ColumnStyle(Width = 50, DisplayName = "Over Lines", DisableSort = true)]
        public string Lines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Over%", DisableSort = true)]
        public string Percent { get; }
        //[ColumnStyle(Width = 50, DisplayName = "Draw Power", DisableSort = true)]
        //public string Power { get; }
        //[ColumnStyle(Width = 50, DisplayName = "Draw Power All/All", DisableSort = true)]
        //public string PowerAllALL { get; }

        public MonitorOverH2HBet(BgOverUnderBetData data, int index)
        {
            this.Populate = data.OverH2H.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.OverH2H.H2HCandidates[index].TotalRows > 0)
            {
                this.Lines = data.OverH2H.H2HCandidates[index].TotalRows.ToString();
                this.Percent = data.OverH2H.H2HCandidates[index].TotalOverPercent.ToString();
                //this.Power = data.OverH2H.H2HCandidates[index].PowerNumber.ToString();
                //this.PowerAllALL = data.OverH2H.H2HCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.Lines = this.Percent = string.Empty; // this.Power = this.PowerAllALL = string.Empty;
            }
        }
    }
}
