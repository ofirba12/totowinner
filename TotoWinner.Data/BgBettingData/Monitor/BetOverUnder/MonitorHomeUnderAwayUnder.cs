﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorHomeUnderAwayUnder
    {
        [ColumnStyle(Width = 120, DisplayName = "HU Populate", DisableSort = true)]
        public string HomePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HU Lines", DisableSort = true)]
        public string HomeLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HU Under%", DisableSort = true)]
        public string HomePercent { get; }
        //[ColumnStyle(Width = 50, DisplayName = "HO Power", DisableSort = true)]
        //public string HomePower { get; }
        //[ColumnStyle(Width = 50, DisplayName = "HO Power All/All", DisableSort = true)]
        //public string HomePowerAllALL { get; }
        [ColumnStyle(Width = 120, DisplayName = "AU Populate", DisableSort = true)]
        public string AwayPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AU Lines", DisableSort = true)]
        public string AwayLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AU Under%", DisableSort = true)]
        public string AwayPercent { get; }
        //[ColumnStyle(Width = 50, DisplayName = "AO Power", DisableSort = true)]
        //public string AwayPower { get; }
        //[ColumnStyle(Width = 50, DisplayName = "AO Power All/All", DisableSort = true)]
        //public string AwayPowerAllALL { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 55, DisplayName = "Total Under%", DisableSort = true)]
        public string TotalPercent { get; }

        public MonitorHomeUnderAwayUnder(BgOverUnderBetData data, int index)
        {
            this.HomePopulate = data.HomeUnderAwayUnder.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeUnderAwayUnder.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeLines = data.HomeUnderAwayUnder.HomeCandidates[index].TotalRows.ToString();
                this.HomePercent = data.HomeUnderAwayUnder.HomeCandidates[index].TotalUnderPercent.ToString();
                //this.HomePower = data.HomeUnderAwayUnder.HomeCandidates[index].PowerNumber.ToString();
                //this.HomePowerAllALL = data.HomeUnderAwayUnder.HomeCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.HomePopulate = this.HomeLines = this.HomePercent = string.Empty; //this.HomePower = this.HomePowerAllALL = 
            }
            this.AwayPopulate = data.HomeUnderAwayUnder.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeUnderAwayUnder.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLines = data.HomeUnderAwayUnder.AwayCandidates[index].TotalRows.ToString();
                this.AwayPercent = data.HomeUnderAwayUnder.AwayCandidates[index].TotalUnderPercent.ToString();
                //this.AwayPower = data.HomeUnderAwayUnder.AwayCandidates[index].PowerNumber.ToString();
                //this.AwayPowerAllALL = data.HomeUnderAwayUnder.AwayCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.AwayPopulate = this.AwayLines = this.AwayPercent = string.Empty; //this.AwayPower = this.AwayPowerAllALL = string.Empty;
            }
            this.TotalLine = data.HomeUnderAwayUnder.Totals[index].TotalRows.ToString();
            this.TotalPercent = data.HomeUnderAwayUnder.Totals[index].TotalPercent.ToString();
        }
    }
}
