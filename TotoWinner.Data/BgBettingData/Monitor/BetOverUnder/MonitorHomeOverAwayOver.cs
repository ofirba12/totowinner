﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorHomeOverAwayOver
    {
        [ColumnStyle(Width = 120, DisplayName = "HO Populate", DisableSort = true)]
        public string HomePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HO Lines", DisableSort = true)]
        public string HomeLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HO Over%", DisableSort = true)]
        public string HomePercent { get; }
        [ColumnStyle(Width = 120, DisplayName = "AO Populate", DisableSort = true)]
        public string AwayPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AO Lines", DisableSort = true)]
        public string AwayLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AO Over%", DisableSort = true)]
        public string AwayPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Over%", DisableSort = true)]
        public string TotalPercent { get; }

        public MonitorHomeOverAwayOver(BgOverUnderBetData data, int index)
        {
            this.HomePopulate = data.HomeOverAwayOver.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeOverAwayOver.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeLines = data.HomeOverAwayOver.HomeCandidates[index].TotalRows.ToString();
                this.HomePercent = data.HomeOverAwayOver.HomeCandidates[index].TotalOverPercent.ToString();
            }
            else
            {
                this.HomePopulate = this.HomeLines = this.HomePercent = string.Empty; 
            }
            this.AwayPopulate = data.HomeOverAwayOver.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeOverAwayOver.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLines = data.HomeOverAwayOver.AwayCandidates[index].TotalRows.ToString();
                this.AwayPercent = data.HomeOverAwayOver.AwayCandidates[index].TotalOverPercent.ToString();
            }
            else
            {
                this.AwayPopulate = this.AwayLines = this.AwayPercent = string.Empty;
            }
            this.TotalLine = data.HomeOverAwayOver.Totals[index].TotalRows.ToString();
            this.TotalPercent = data.HomeOverAwayOver.Totals[index].TotalPercent.ToString();
        }
    }
}
