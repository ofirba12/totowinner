﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorRangeStatistics
    {
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterLeft { get; }
        [ColumnStyle(Width = 75, DisplayName = " ")]
        public string FilterRight { get; }

        [ColumnStyle(Width = 75, DisplayName = "Total Rows")]
        public string TotalRows { get; }
        [ColumnStyle(Width = 50, DisplayName = "Range 0-1")]
        public string TotalBetween0and1 { get; }
        [ColumnStyle(Width = 50, DisplayName = "Range 2-3")]
        public string TotalBetween2and3 { get; }
        [ColumnStyle(Width = 50, DisplayName = "Range 4+")]
        public string Total4Plus { get; }
        [ColumnStyle(Width = 50, DisplayName = "Range 0-1 %")]
        public string TotalBetween0and1Percent { get; }
        [ColumnStyle(Width = 55, DisplayName = "Range 2-3 %")]
        public string TotalBetween2and3Percent { get; }
        [ColumnStyle(Width = 55, DisplayName = "Range 4+ %")]
        public string Total4PlusPercent { get; }
        [ColumnStyle(Hide = true)]
        public bool DataFound { get; }

        public MonitorRangeStatistics(Tuple<string, string> filter, BgTeamRangeStatistics value)
        {
            this.DataFound = value != null;
            this.FilterLeft = filter.Item1;
            this.FilterRight = filter.Item2;

            if (value != null)
            {
                this.TotalRows = value.TotalRows.ToString();
                this.TotalBetween0and1 = value.TotalBetween0and1.ToString();
                this.TotalBetween2and3 = value.TotalBetween2and3.ToString();
                this.Total4Plus = value.Total4Plus.ToString();
                this.TotalBetween0and1Percent = Decimal.ToInt32(value.TotalBetween0and1Percent).ToString();//.ToString("0.##");
                this.TotalBetween2and3Percent = Decimal.ToInt32(value.TotalBetween2and3Percent).ToString();//.ToString("0.##");
                this.Total4PlusPercent = Decimal.ToInt32(value.Total4PlusPercent).ToString();//.ToString("0.##");
            }
        }
    }
}
