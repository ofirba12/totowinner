﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeDrawH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "Draw Populate", DisableSort = true)]
        public string Populate { get; }
        [ColumnStyle(Width = 50, DisplayName = "Draw Lines", DisableSort = true)]
        public string Lines { get; }
        [ColumnStyle(Width = 75, DisplayName = "Draw Win%", DisableSort = true)]
        public string Percent { get; }
        [ColumnStyle(Width = 50, DisplayName = "Draw Power", DisableSort = true)]
        public string Power { get; }
        [ColumnStyle(Width = 50, DisplayName = "Draw Power All/All", DisableSort = true)]
        public string PowerAllALL { get; }

        public MonitorFullTimeDrawH2HBet(BgFullTimeBetData data, int index)
        {
            this.Populate = data.DrawH2H.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.DrawH2H.H2HCandidates[index].TotalRows > 0)
            {
                this.Lines = data.DrawH2H.H2HCandidates[index].TotalRows.ToString();
                this.Percent = data.DrawH2H.H2HCandidates[index].TotalIndexPercentD.ToString();
                this.Power = data.DrawH2H.H2HCandidates[index].PowerNumber.ToString();
                this.PowerAllALL = data.DrawH2H.H2HCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.Lines = this.Percent = this.Power = this.PowerAllALL = string.Empty;
            }
        }
    }
}
