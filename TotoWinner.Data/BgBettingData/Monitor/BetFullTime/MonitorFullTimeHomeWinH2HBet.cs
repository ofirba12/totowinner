﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeHomeWinH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "HW Populate", DisableSort = true)]
        public string HomeWinPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Lines", DisableSort = true)]
        public string HomeWinLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HW Win%", DisableSort = true)]
        public string HomeWinWinPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Power", DisableSort = true)]
        public string HomeWinPower { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Power All/All", DisableSort = true)]
        public string HomeWinPowerAllALL { get; }

        public MonitorFullTimeHomeWinH2HBet(BgFullTimeBetData data, int index)
        {
            this.HomeWinPopulate = data.HomeWinH2H.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeWinH2H.H2HCandidates[index].TotalRows > 0)
            {
                this.HomeWinLines = data.HomeWinH2H.H2HCandidates[index].TotalRows.ToString();
                this.HomeWinWinPercent = data.HomeWinH2H.H2HCandidates[index].TotalIndexPercentW.ToString();
                this.HomeWinPower = data.HomeWinH2H.H2HCandidates[index].PowerNumber.ToString();
                this.HomeWinPowerAllALL = data.HomeWinH2H.H2HCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.HomeWinLines = this.HomeWinWinPercent = this.HomeWinPower = this.HomeWinPowerAllALL = string.Empty;
            }
        }
    }
}
