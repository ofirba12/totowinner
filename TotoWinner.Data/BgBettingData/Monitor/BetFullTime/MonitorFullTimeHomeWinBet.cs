﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeHomeWinBet
    {
        [ColumnStyle(Width = 120, DisplayName = "HW Populate", DisableSort = true)]
        public string HomeWinPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Lines", DisableSort = true)]
        public string HomeWinLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HW Win%", DisableSort = true)]
        public string HomeWinWinPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Power", DisableSort = true)]
        public string HomeWinPower { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Power All/All", DisableSort = true)]
        public string HomeWinPowerAllALL { get; }
        [ColumnStyle(Width = 120, DisplayName = "AL Populate", DisableSort = true)]
        public string AwayLosePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AL Lines", DisableSort = true)]
        public string AwayLoseLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AL Lose%", DisableSort = true)]
        public string AwayLosePercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "AL Power", DisableSort = true)]
        public string AwayLosePower { get; }
        [ColumnStyle(Width = 50, DisplayName = "AL Power All/All", DisableSort = true)]
        public string AwayLosePowerAllALL { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Win%", DisableSort = true)]
        public string TotalWinPercenty { get; }

        public MonitorFullTimeHomeWinBet(BgFullTimeBetData data, int index)
        {
            this.HomeWinPopulate = data.HomeWinAwayLose.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeWinAwayLose.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeWinLines = data.HomeWinAwayLose.HomeCandidates[index].TotalRows.ToString();
                this.HomeWinWinPercent = data.HomeWinAwayLose.HomeCandidates[index].TotalIndexPercentW.ToString();
                this.HomeWinPower = data.HomeWinAwayLose.HomeCandidates[index].PowerNumber.ToString();
                this.HomeWinPowerAllALL = data.HomeWinAwayLose.HomeCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.HomeWinLines = this.HomeWinWinPercent = this.HomeWinPower = this.HomeWinPowerAllALL = string.Empty;
            }
            this.AwayLosePopulate = data.HomeWinAwayLose.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeWinAwayLose.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLoseLines = data.HomeWinAwayLose.AwayCandidates[index].TotalRows.ToString();
                this.AwayLosePercent = data.HomeWinAwayLose.AwayCandidates[index].TotalIndexPercentL.ToString();
                this.AwayLosePower = data.HomeWinAwayLose.AwayCandidates[index].PowerNumber.ToString();
                this.AwayLosePowerAllALL = data.HomeWinAwayLose.AwayCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.AwayLoseLines = this.AwayLosePercent = this.AwayLosePower = this.AwayLosePowerAllALL = string.Empty;
            }
            this.TotalLine = data.HomeWinAwayLose.Totals[index].TotalRows.ToString();
            this.TotalWinPercenty = data.HomeWinAwayLose.Totals[index].TotalPercent.ToString();
        }
    }
}
