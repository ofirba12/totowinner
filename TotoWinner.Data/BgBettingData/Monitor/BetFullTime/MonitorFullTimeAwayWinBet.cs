﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeAwayWinBet
    {
        [ColumnStyle(Width = 120, DisplayName = "HW Populate", DisableSort = true)]
        public string HomePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Lines", DisableSort = true)]
        public string HomeLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HW Lose%", DisableSort = true)]
        public string HomePercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Power", DisableSort = true)]
        public string HomePower { get; }
        [ColumnStyle(Width = 50, DisplayName = "HW Power All/All", DisableSort = true)]
        public string HomePowerAllALL { get; }
        [ColumnStyle(Width = 120, DisplayName = "AL Populate", DisableSort = true)]
        public string AwayPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AL Lines", DisableSort = true)]
        public string AwayLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AL Win%", DisableSort = true)]
        public string AwayPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "AL Power", DisableSort = true)]
        public string AwayPower { get; }
        [ColumnStyle(Width = 50, DisplayName = "AL Power All/All", DisableSort = true)]
        public string AwayPowerAllALL { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Win%", DisableSort = true)]
        public string TotalWinPercenty { get; }

        public MonitorFullTimeAwayWinBet(BgFullTimeBetData data, int index)
        {
            this.HomePopulate = data.AwayWinHomeLose.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.AwayWinHomeLose.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeLines = data.AwayWinHomeLose.HomeCandidates[index].TotalRows.ToString();
                this.HomePercent = data.AwayWinHomeLose.HomeCandidates[index].TotalIndexPercentL.ToString();
                this.HomePower = data.AwayWinHomeLose.HomeCandidates[index].PowerNumber.ToString();
                this.HomePowerAllALL = data.AwayWinHomeLose.HomeCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.HomeLines = this.HomePercent = this.HomePower = this.HomePowerAllALL = string.Empty;
            }
            this.AwayPopulate = data.AwayWinHomeLose.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.AwayWinHomeLose.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLines = data.AwayWinHomeLose.AwayCandidates[index].TotalRows.ToString();
                this.AwayPercent = data.AwayWinHomeLose.AwayCandidates[index].TotalIndexPercentW.ToString();
                this.AwayPower = data.AwayWinHomeLose.AwayCandidates[index].PowerNumber.ToString();
                this.AwayPowerAllALL = data.AwayWinHomeLose.AwayCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.AwayLines = this.AwayPercent = this.AwayPower = this.AwayPowerAllALL = string.Empty;
            }
            this.TotalLine = data.AwayWinHomeLose.Totals[index].TotalRows.ToString();
            this.TotalWinPercenty = data.AwayWinHomeLose.Totals[index].TotalPercent.ToString();
        }
    }
}
