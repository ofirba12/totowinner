﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeHomeDrawAwayDraw
    {
        [ColumnStyle(Width = 120, DisplayName = "HD Populate", DisableSort = true)]
        public string HomePopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "HD Lines", DisableSort = true)]
        public string HomeLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "HD Draw%", DisableSort = true)]
        public string HomePercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "HD Power", DisableSort = true)]
        public string HomePower { get; }
        [ColumnStyle(Width = 50, DisplayName = "HD Power All/All", DisableSort = true)]
        public string HomePowerAllALL { get; }
        [ColumnStyle(Width = 120, DisplayName = "AD Populate", DisableSort = true)]
        public string AwayPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AD Lines", DisableSort = true)]
        public string AwayLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AD Draw%", DisableSort = true)]
        public string AwayPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "AD Power", DisableSort = true)]
        public string AwayPower { get; }
        [ColumnStyle(Width = 50, DisplayName = "AD Power All/All", DisableSort = true)]
        public string AwayPowerAllALL { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Line", DisableSort = true)]
        public string TotalLine { get; }
        [ColumnStyle(Width = 50, DisplayName = "Total Draw%", DisableSort = true)]
        public string TotalPercent { get; }

        public MonitorFullTimeHomeDrawAwayDraw(BgFullTimeBetData data, int index)
        {
            this.HomePopulate = data.HomeDrawAwayDraw.HomeCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeDrawAwayDraw.HomeCandidates[index].TotalRows > 0)
            {
                this.HomeLines = data.HomeDrawAwayDraw.HomeCandidates[index].TotalRows.ToString();
                this.HomePercent = data.HomeDrawAwayDraw.HomeCandidates[index].TotalIndexPercentD.ToString();
                this.HomePower = data.HomeDrawAwayDraw.HomeCandidates[index].PowerNumber.ToString();
                this.HomePowerAllALL = data.HomeDrawAwayDraw.HomeCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.HomePopulate = this.HomeLines = this.HomePercent = this.HomePower = this.HomePowerAllALL = string.Empty;
            }
            this.AwayPopulate = data.HomeDrawAwayDraw.AwayCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.HomeDrawAwayDraw.AwayCandidates[index].TotalRows > 0)
            {
                this.AwayLines = data.HomeDrawAwayDraw.AwayCandidates[index].TotalRows.ToString();
                this.AwayPercent = data.HomeDrawAwayDraw.AwayCandidates[index].TotalIndexPercentD.ToString();
                this.AwayPower = data.HomeDrawAwayDraw.AwayCandidates[index].PowerNumber.ToString();
                this.AwayPowerAllALL = data.HomeDrawAwayDraw.AwayCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.AwayPopulate = this.AwayLines = this.AwayPercent = this.AwayPower = this.AwayPowerAllALL = string.Empty;
            }
            this.TotalLine = data.HomeDrawAwayDraw.Totals[index].TotalRows.ToString();
            this.TotalPercent = data.HomeDrawAwayDraw.Totals[index].TotalPercent.ToString();
        }
    }
}
