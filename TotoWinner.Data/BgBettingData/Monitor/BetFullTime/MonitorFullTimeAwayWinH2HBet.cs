﻿using System;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class MonitorFullTimeAwayWinH2HBet
    {
        [ColumnStyle(Width = 120, DisplayName = "AW Populate", DisableSort = true)]
        public string WinPopulate { get; }
        [ColumnStyle(Width = 50, DisplayName = "AW Lines", DisableSort = true)]
        public string WinLines { get; }
        [ColumnStyle(Width = 75, DisplayName = "AW Win%", DisableSort = true)]
        public string WinPercent { get; }
        [ColumnStyle(Width = 50, DisplayName = "AW Power", DisableSort = true)]
        public string WinPower { get; }
        [ColumnStyle(Width = 50, DisplayName = "AW Power All/All", DisableSort = true)]
        public string WinPowerAllALL { get; }

        public MonitorFullTimeAwayWinH2HBet(BgFullTimeBetData data, int index)
        {
            this.WinPopulate = data.AwayWinH2H.H2HCandidates[index].Filter.ToString().Replace('_', '/');
            if (data.AwayWinH2H.H2HCandidates[index].TotalRows > 0)
            {
                this.WinLines = data.AwayWinH2H.H2HCandidates[index].TotalRows.ToString();
                this.WinPercent = data.AwayWinH2H.H2HCandidates[index].TotalIndexPercentL.ToString();
                this.WinPower = data.AwayWinH2H.H2HCandidates[index].PowerNumber.ToString();
                this.WinPowerAllALL = data.AwayWinH2H.H2HCandidates.Where(d => d.Filter == BgTeamsFilter.All_All).First().PowerNumber.ToString();
            }
            else
            {
                this.WinLines = this.WinPercent = this.WinPower = this.WinPowerAllALL = string.Empty;
            }
        }
    }
}
