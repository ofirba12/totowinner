﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class OddsAndScores : IEquatable<OddsAndScores>
    {
        public decimal? HomeOdd { get; private set; }
        public decimal? DrawOdd { get; private set; }
        public decimal? AwayOdd { get; private set; }
        public int? HomeScore { get; private set; }
        public int? AwayScore { get; private set; }
        public BasketballQuatersScores BasketballQuatersScores { get; private set; }
        public OddsAndScores(decimal? homeOdd,
            decimal? drawOdd,
            decimal? awayOdd,
            int? homeScore,
            int? awayScore,
            BasketballQuatersScores bbQuarters)
        {
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.BasketballQuatersScores = bbQuarters;
        }
        public void SetEmptyOdds()
        {
            this.HomeOdd = null;
            this.DrawOdd = null;
            this.AwayOdd = null;
        }
        public void SetOdds(decimal homeOdd,
            decimal drawOdd,
            decimal awayOdd)
        {
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
        }
        public void SetScores(int homeScores,
            int awayScores)
        {
            this.HomeScore = homeScores;
            this.AwayScore = awayScores;
        }
        public bool Equals(OddsAndScores other)
        {
            var basic = this.HomeOdd == other.HomeOdd
                && this.DrawOdd == other.DrawOdd
                && this.AwayOdd == other.AwayOdd
                && this.HomeScore == other.HomeScore
                && this.AwayScore == other.AwayScore;
            var basketball = (this.BasketballQuatersScores == null && other.BasketballQuatersScores == null) ||
                (this.BasketballQuatersScores != null && other.BasketballQuatersScores != null
                    && this.BasketballQuatersScores.Equals(other.BasketballQuatersScores));
            return basic && basketball;
        }
        public override int GetHashCode()
        {
            var code =  this.HomeScore?.GetHashCode() ^ this.AwayScore?.GetHashCode()
                + this.HomeOdd?.GetHashCode()
                + this.DrawOdd?.GetHashCode()
                + this.AwayOdd?.GetHashCode();
            if (!code.HasValue)
                return base.GetHashCode();
            return code.Value;
        }
        public override string ToString()
        {
            var str = $"HomeOdd={HomeOdd},DrawOdd={DrawOdd},AwayOdd={AwayOdd};;HomeScore={HomeScore},AwayScore={AwayScore}";
            if (BasketballQuatersScores != null)
                str = $"{str}__{BasketballQuatersScores.ToString()}";
            return str;
        }
    }
}
