﻿using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data
{
    public class BgTeamBlackFilter
    {
        public BgSport Sport { get; }
        public int TeamId { get; }
        public string TeamName { get; }
        public BgMapperType FilterBgType { get; }
        public int FilterBgId { get; }
        public string FilterBgName { get; }
        public BgTeamBlackFilter(
           BgSport bgSport,
           int teamId,
           string teamName,
           BgMapperType filterBgType,
           int filterBgId,
           string filterBgName)
        {
            this.Sport = bgSport;
            this.TeamId = teamId;
            this.TeamName = teamName;
            this.FilterBgType = filterBgType;
            this.FilterBgId = filterBgId;
            this.FilterBgName = filterBgName;
        }
    }
}
