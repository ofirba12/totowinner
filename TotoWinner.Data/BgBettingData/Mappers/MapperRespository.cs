﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class MapperRespository
    {
        public Dictionary<int, List<BgMapperItem>> BgIdBgMap { get; }
        public Dictionary<ProviderKey, string> BgIdItemName { get; }
        public Dictionary<ProviderData, ProviderKey> GoldserveDataBgKey { get; }
        public Dictionary<ProviderData, ProviderKey> ManualDataBgKey { get; }
        public Dictionary<ProviderName, int> BgCountriesNames { get; }
        public Dictionary<ProviderData, int?> GoldserveCountries { get; }
        public Dictionary<ProviderData, ProviderKey> ManualCountries { get; }
        public BgObjectId NextBgCountryId { get; set; }
        public Dictionary<ProviderName, int> BgLeaguesNames { get; }
        public Dictionary<ProviderData, int?> GoldserveLeaguesIds { get; }
        public Dictionary<ProviderData, ProviderKey> ManualLeagues { get; }
        public BgObjectId NextBgLeagueId { get; set; }
        public Dictionary<ProviderName, int> BgTeamsNames { get; }
        public Dictionary<ProviderData, int?> GoldserveTeamsIds { get; }
        public Dictionary<ProviderData, ProviderKey> ManualTeams { get; }
        public BgObjectId NextBgTeamId { get; set; }
        public List<BgMapperMonitor> BgMapperMonitors { get; }

        public MapperRespository(Dictionary<int, List<BgMapperItem>> bgIdBgMap, 
            Dictionary<ProviderData, ProviderKey> goldserveDataBgKey,
            Dictionary<ProviderData, ProviderKey> manualDataBgKey,
            Dictionary<ProviderKey, string> bgIdItemName)
        {
            this.BgIdBgMap = bgIdBgMap;
            this.GoldserveDataBgKey = goldserveDataBgKey;
            this.ManualDataBgKey = manualDataBgKey;
            this.BgIdItemName = bgIdItemName;
            this.BgMapperMonitors = new List<BgMapperMonitor>();
            this.BgCountriesNames = this.BgIdItemName
                .Where(m => m.Key.ItemType == BgMapperType.Countries)
                .ToDictionary(m => new ProviderName(m.Key.Sport,m.Value), m => m.Key.ProviderId);
            
            this.GoldserveCountries = this.GoldserveDataBgKey.Where(m => m.Value.ItemType == BgMapperType.Countries).ToDictionary(m => m.Key, m => m.Key.ProviderId);
            this.ManualCountries = this.ManualDataBgKey.Where(m => m.Value.ItemType == BgMapperType.Countries).ToDictionary(m => m.Key, m => m.Value);
            this.NextBgCountryId = new BgObjectId(this.GoldserveCountries.Count + this.ManualCountries.Count > 0
                ? this.BgCountriesNames.Values.Max() + 1
                : 100);

            this.BgLeaguesNames = this.BgIdItemName
                .Where(m => m.Key.ItemType == BgMapperType.Leagues)
                .ToDictionary(m => new ProviderName(m.Key.Sport, m.Value), m => m.Key.ProviderId);
            this.GoldserveLeaguesIds = this.GoldserveDataBgKey.Where(m => m.Value.ItemType == BgMapperType.Leagues).ToDictionary(m => m.Key, m => m.Key.ProviderId);
            this.ManualLeagues = this.ManualDataBgKey.Where(m => m.Value.ItemType == BgMapperType.Leagues).ToDictionary(m => m.Key, m => m.Value);
            this.NextBgLeagueId = new BgObjectId(this.GoldserveLeaguesIds.Count > 0
                ? this.BgLeaguesNames.Values.Max() + 1
                : 10000);

            this.BgTeamsNames = this.BgIdItemName
                .Where(m => m.Key.ItemType == BgMapperType.Teams)
                .ToDictionary(m => new ProviderName(m.Key.Sport, m.Value), m => m.Key.ProviderId);
            this.GoldserveTeamsIds = this.GoldserveDataBgKey.Where(m => m.Value.ItemType == BgMapperType.Teams).ToDictionary(m => m.Key, m => m.Key.ProviderId);
            this.ManualTeams = this.ManualDataBgKey.Where(m => m.Value.ItemType == BgMapperType.Teams).ToDictionary(m => m.Key, m => m.Value);
            this.NextBgTeamId = new BgObjectId(this.GoldserveTeamsIds.Count > 0
                ? this.BgTeamsNames.Values.Max() + 1
                : 100000);
            foreach (var map in this.BgIdBgMap)
            {
                foreach (var mapItem in map.Value)
                {
                    var item = new BgMapperMonitor(mapItem.BgId, 
                        mapItem.Sport, 
                        mapItem.BgType, 
                        mapItem.BgName, 
                        mapItem.GoalserveId, 
                        mapItem.GoalserveName, 
                        mapItem.ManualName);
                    this.BgMapperMonitors.Add(item);
                }
            }
        }
    }
}
