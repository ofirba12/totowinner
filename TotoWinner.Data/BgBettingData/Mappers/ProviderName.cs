﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class ProviderName : IEquatable<ProviderName>
    {
        public BgSport Sport { get; }
        public string ItemName { get; }
        public ProviderName(BgSport sport,
            string data)
        {
            this.Sport = sport;
            this.ItemName = data;
        }

        public bool Equals(ProviderName other)
        {
            return this.Sport == other.Sport &&
                this.ItemName == other.ItemName;
        }

        public bool Equals(ProviderName x, ProviderName y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.Sport.GetHashCode() ^ this.ItemName.GetHashCode();
        }
    }
}
