﻿using System;
using System.Collections.Generic;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMapperMonitor
    {
        public int BgId { get; }
        public BgSport BgSport { get; }
        [ColumnStyle (Hide = true)]
        public BgMapperType BgType { get; }
        public string BgName { get; private set; }
        public int? GoalserveId { get; }
        public string GoalserveName { get; }
        public string ManualName { get; }
        public BgMapperMonitor(int bgId,
            BgSport sport,
            BgMapperType type,
            string bgName,
            int? goalserveId,
            string goalserveName,
            string manualName)
        {
            this.BgId = bgId;
            this.BgSport = sport;
            this.BgName = bgName;
            this.BgType = type;
            this.GoalserveId = goalserveId;
            this.GoalserveName = goalserveName;
            this.ManualName = manualName;
        }
        
        public void SetBgName(string bgName)
        {
            this.BgName = bgName;
        }
    }
}