﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMapperMonitorComparer : IEqualityComparer<BgMapperMonitor>
    {
        public bool Equals(BgMapperMonitor x, BgMapperMonitor y)
        {
            return x.BgId == y.BgId;
        }

        public int GetHashCode(BgMapperMonitor obj)
        {
            return obj.BgId.GetHashCode();
        }
    }
}