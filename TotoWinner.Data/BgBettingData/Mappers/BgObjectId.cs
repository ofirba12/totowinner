﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgObjectId
    {
        public int NextId { get; private set; }
        public BgObjectId(int initialValue)
        {
            this.NextId = initialValue;
        }
        public static BgObjectId operator++(BgObjectId bgObjectId)
        {
            bgObjectId.NextId++;
            return bgObjectId;
        }
    }
}
