﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMapper : IEquatable<BgMapper>, IEqualityComparer<BgMapper>
    {
        public int BgId { get; }
        public BgSport Sport { get; }
        public BgMapperType MapperType { get; }
        public string BgName { get; }
        public BgMapper(int bgId,
            BgSport sport,
            BgMapperType mapperType,
            string bgName)
        {
            this.BgId = bgId;
            this.Sport = sport;
            this.MapperType = mapperType;
            this.BgName = bgName;
        }

        public bool Equals(BgMapper other)
        {
            return this.BgId == other.BgId &&
                this.Sport == other.Sport &&
                this.MapperType == other.MapperType &&
                this.BgName == other.BgName;
        }

        public bool Equals(BgMapper x, BgMapper y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(BgMapper obj)
        {
            return obj.BgId.GetHashCode() ^ obj.Sport.GetHashCode() + obj.BgName.GetHashCode() + obj.MapperType.GetHashCode();
        }
    }
}
