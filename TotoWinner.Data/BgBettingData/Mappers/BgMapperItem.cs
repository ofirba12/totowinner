﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgMapperItem
    {
        public int BgId { get;  }
        public BgSport Sport { get;  }
        public BgMapperType BgType { get;  }
        public string BgName { get; }
        public int? GoalserveId { get;  }
        public string GoalserveName { get;  }
        public string ManualName { get; }
        public BgMapperItem(int bgId,
            byte sport,
            byte bgType,
            string bgName,
            int? goalserveId,
            string goalserveName,
            string manualName)
        {
            this.BgId = bgId;
            this.Sport = (BgSport)sport;
            this.BgType = (BgMapperType)bgType;
            this.BgName = bgName;
            this.GoalserveId = goalserveId;
            this.GoalserveName = goalserveName;
            this.ManualName = manualName;
        }
    }
}
