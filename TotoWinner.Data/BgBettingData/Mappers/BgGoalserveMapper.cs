﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgGoalserveMapper : IEquatable<BgGoalserveMapper>, IEqualityComparer<BgGoalserveMapper>
    {
        public int BgId { get; }
        public int? GoalserveId { get; }
        public string GoalserveName { get; }
        public BgGoalserveMapper(int bgId,
            int? goalserveId,
            string goalserveName)
        {
            this.BgId = bgId;
            this.GoalserveId = goalserveId;
            this.GoalserveName = goalserveName;
        }

        public bool Equals(BgGoalserveMapper other)
        {
            return this.BgId == other.BgId &&
                Nullable.Equals<int>(this.GoalserveId, other.GoalserveId) &&
                this.GoalserveName == other.GoalserveName;
        }

        public bool Equals(BgGoalserveMapper x, BgGoalserveMapper y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(BgGoalserveMapper obj)
        {
            return obj.BgId.GetHashCode() + 
                (obj.GoalserveId.HasValue
                ? obj.GoalserveId.Value.GetHashCode()
                : 0) + 
                obj.GoalserveName.GetHashCode();
        }
    }
}
