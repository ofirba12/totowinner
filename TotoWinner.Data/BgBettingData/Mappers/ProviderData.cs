﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class ProviderData : IEquatable<ProviderData>
    {
        public int? ProviderId { get; }
        public BgSport Sport { get; }
        public BgMapperType DataType { get; }
        public string Data { get; }
        public ProviderData(int? id,
            BgSport sport,
            BgMapperType dataType,
            string data)
        {
            this.ProviderId = id;
            this.Sport = sport;
            this.DataType = dataType;
            this.Data = data;
        }

        public bool Equals(ProviderData other)
        {
            return Nullable<int>.Equals(this.ProviderId, other.ProviderId) &&
                this.Sport == other.Sport &&
                this.DataType == other.DataType &&
                this.Data == other.Data;
        }

        public bool Equals(ProviderData x, ProviderData y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.Sport.GetHashCode() ^ this.DataType.GetHashCode() ^ this.Data.GetHashCode() 
                + (this.ProviderId.HasValue
                ? this.ProviderId.Value.GetHashCode() 
                : 0);

        }
    }
}
