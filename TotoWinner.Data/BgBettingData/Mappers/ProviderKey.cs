﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class ProviderKey : IEquatable<ProviderKey>
    {
        public int ProviderId { get; set; }
        public BgSport Sport { get; set; }
        public BgMapperType ItemType { get; set; }
        public ProviderKey(int id,
            BgSport sport,
            BgMapperType itemType)
        {
            this.ProviderId = id;
            this.Sport = sport;
            this.ItemType = itemType;
        }

        public bool Equals(ProviderKey other)
        {
            return this.ProviderId == other.ProviderId &&
                this.Sport == other.Sport &&    
                this.ItemType == other.ItemType;
        }

        public bool Equals(ProviderKey x, ProviderKey y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.ProviderId.GetHashCode() ^ this.Sport.GetHashCode() ^ this.ItemType.GetHashCode();
        }
    }
}
