﻿using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFeedFullRepository
    {
        public Dictionary<long, BgMatchInfo> Info;
        public List<BgMatch> AllMatches { get; }
        public Dictionary<long, BgMatch> Matches { get; }
        public Dictionary<long, List<BgMatchStatistics>> Statistics { get; }
        public Dictionary<long, List<BgMatchStatisticsDetails>> StatisticsDetails { get; }

        public BgFeedFullRepository(List<BgMatch> matches, 
            List<BgMatchStatistics> statistics,
            Dictionary<long, List<BgMatchStatisticsDetails>> statisticsDetails,
            List<BgMatchInfo> info)
        {
            this.AllMatches = matches;
            this.Matches = matches.ToDictionary(m => m.MatchId, m => m);
            this.Statistics = new Dictionary<long, List<BgMatchStatistics>>();
            foreach (var item in statistics)
            {
                if (!this.Statistics.ContainsKey(item.MatchId))
                    this.Statistics.Add(item.MatchId, new List<BgMatchStatistics>());
                this.Statistics[item.MatchId].Add(item);
            }
            if (info != null)
                this.Info = info.ToDictionary(m => m.MatchId, m => m);
            this.StatisticsDetails = statisticsDetails;
        }
    }
}
