﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgDataRepository
    {
        public Dictionary<BgFeedKey, BgMatch> FeedWithNoDuplicates { get; }
        public Dictionary<BgFeedKey, List<BgMatch>> FeedWithDuplicates { get; } // Should not exists
        public List<BgMatch> All { get; }

        public BgDataRepository(Dictionary<BgFeedKey, BgMatch> feedWithNoDuplicates, 
            Dictionary<BgFeedKey, List<BgMatch>> feedWithDuplicates, 
            List<BgMatch> all)
        {
            this.FeedWithNoDuplicates = feedWithNoDuplicates;
            this.FeedWithDuplicates = feedWithDuplicates;
            All = all;
        }
    }
}
