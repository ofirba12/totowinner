﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchIndexes
    {
        public BgWDLIndex WDL { get; }
        public decimal OddPower { get; }
        public BgOverUnderIndex OverUnder3_5Index { get; }
        public BgOverUnderIndex OverUnder2_5Index { get; }
        public BgOverUnderIndex OverUnder1_5Index { get; }
        public BgRangeIndex RangeIndex { get; }
        public bool BothScoreIndex { get; }
        public bool DrawWinIndex { get; }
        public bool NoDrawIndex { get; }
        public bool LoseDrawIndex { get; }

        public BgMatchIndexes(BgWDLIndex wDL, 
            decimal oddPower, 
            BgOverUnderIndex overUnder1_5Index, 
            BgOverUnderIndex overUnder2_5Index, 
            BgOverUnderIndex overUnder3_5Index, 
            BgRangeIndex rangeIndex, 
            bool bothScoreIndex, 
            bool drawWinIndex, 
            bool noDrawIndex, 
            bool loseDrawIndex)
        {
            this.WDL = wDL;
            this.OddPower = oddPower;
            this.OverUnder1_5Index = overUnder1_5Index;
            this.OverUnder2_5Index = overUnder2_5Index;
            this.OverUnder3_5Index = overUnder3_5Index;
            this.RangeIndex = rangeIndex;
            this.BothScoreIndex = bothScoreIndex;
            this.DrawWinIndex = drawWinIndex;
            this.NoDrawIndex = noDrawIndex;
            this.LoseDrawIndex = loseDrawIndex;
        }
    }
}
