﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamAnalysisRepository
    {
        public BgTeamAnalysis WatchFindings { get; }
        public List<BgMatchForAnalysis> IgnoreSpecialFindings { get; }

        public BgTeamAnalysisRepository(BgTeamAnalysis watchFindings, List<BgMatchForAnalysis> ignoreSpecialFindings) 
        {
            WatchFindings = watchFindings;
            IgnoreSpecialFindings = ignoreSpecialFindings;
        }
    }
}
