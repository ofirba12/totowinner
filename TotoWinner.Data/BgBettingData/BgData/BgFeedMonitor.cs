﻿using System;
using System.Collections.Generic;
using System.Drawing;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFeedMonitor
    {
        public string Country { get; }
        public string League { get; }
        [ColumnStyle(Width = 75)]
        public string Status { get; }
        [ColumnStyle(Width = 50)]
        public string Time { get; }
        public string Home { get; }
        [ColumnStyle(Freeze = true)]
        public string Away { get; }
        [ColumnStyle(Width = 75)]
        public decimal? HomeOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? DrawOdd { get; }
        [ColumnStyle(Width = 75)]
        public decimal? AwayOdd { get; }
        [ColumnStyle(Width = 75)]
        public int? HomeScore { get; }
        [ColumnStyle(Width = 75)]
        public int? AwayScore { get; }

        [ColumnStyle(DisplayName ="Web 1", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)] 
        public int? Web1 { get; }
        [ColumnStyle(DisplayName ="Web X", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? WebX { get; }
        [ColumnStyle(DisplayName = "Web 2", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? Web2 { get; }
        [ColumnStyle(DisplayName = "WebUnder 1.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? WebUnder1_5 { get; }
        [ColumnStyle(DisplayName = "WebOver 1.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? WebOver1_5 { get; }
        [ColumnStyle(DisplayName = "WebUnder 2.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? WebUnder2_5 { get; }
        [ColumnStyle(DisplayName = "WebOver 2.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? WebOver2_5 { get; }
        [ColumnStyle(DisplayName = "WebUnder 3.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? WebUnder3_5 { get; }
        [ColumnStyle(DisplayName = "WebOver 3.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? WebOver3_5 { get; }
        [ColumnStyle(DisplayName = "Web 0-1", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? WebRange0_1 { get; }
        [ColumnStyle(DisplayName = "Web 2-3", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? WebRange2_3 { get; }
        [ColumnStyle(DisplayName = "Web 4 Plus", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? WebRange4Plus { get; }
        [ColumnStyle(DisplayName = "Web Both Score", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? WebBothScore { get; }
        [ColumnStyle(DisplayName = "Web No Both Score", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? WebNoBothScore { get; }
        [ColumnStyle(DisplayName = "Web 1X", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? Web1X { get; }
        [ColumnStyle(DisplayName = "Web 12", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? Web12 { get; }
        [ColumnStyle(DisplayName = "Web 2X", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? Web2X { get; }

        [ColumnStyle(DisplayName = "Avg 1", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? Avg1 { get; }
        [ColumnStyle(DisplayName = "Avg X", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgX { get; }
        [ColumnStyle(DisplayName = "Avg 2", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? Avg2 { get; }
        [ColumnStyle(DisplayName = "AvgUnder 1.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgUnder1_5 { get; }
        [ColumnStyle(DisplayName = "AvgOver 1.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgOver1_5 { get; }
        [ColumnStyle(DisplayName = "AvgUnder 2.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgUnder2_5 { get; }
        [ColumnStyle(DisplayName = "AvgOver 2.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgOver2_5 { get; }
        [ColumnStyle(DisplayName = "AvgUnder 3.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgUnder3_5 { get; }
        [ColumnStyle(DisplayName = "AvgOver 3.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgOver3_5 { get; }
        [ColumnStyle(DisplayName = "Avg 0-1", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgRange0_1 { get; }
        [ColumnStyle(DisplayName = "Avg 2-3", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgRange2_3 { get; }
        [ColumnStyle(DisplayName = "Avg 4 Plus", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgRange4Plus { get; }
        [ColumnStyle(DisplayName = "Avg Both Score", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgBothScore { get; }
        [ColumnStyle(DisplayName = "Avg No Both Score", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AvgNoBothScore { get; }
        [ColumnStyle(DisplayName = "Avg 1X", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? Avg1X { get; }
        [ColumnStyle(DisplayName = "Avg 12", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? Avg12 { get; }
        [ColumnStyle(DisplayName = "Avg 2X", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? Avg2X { get; }


        [ColumnStyle(DisplayName = "AllAll 1", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAll1 { get; }
        [ColumnStyle(DisplayName = "AllAll X", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllX { get; }
        [ColumnStyle(DisplayName = "AllAll 2", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAll2 { get; }
        [ColumnStyle(DisplayName = "AllAllUnder 1.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllUnder1_5 { get; }
        [ColumnStyle(DisplayName = "AllAllOver 1.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllOver1_5 { get; }
        [ColumnStyle(DisplayName = "AllAllUnder 2.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllUnder2_5 { get; }
        [ColumnStyle(DisplayName = "AllAllOver 2.5", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllOver2_5 { get; }
        [ColumnStyle(DisplayName = "AllAllUnder 3.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllUnder3_5 { get; }
        [ColumnStyle(DisplayName = "AllAllOver 3.5", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllOver3_5 { get; }
        [ColumnStyle(DisplayName = "AllAll 0-1", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllRange0_1 { get; }
        [ColumnStyle(DisplayName = "AllAll 2-3", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllRange2_3 { get; }
        [ColumnStyle(DisplayName = "AllAll 4 Plus", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllRange4Plus { get; }
        [ColumnStyle(DisplayName = "AllAll Both Score", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllBothScore { get; }
        [ColumnStyle(DisplayName = "AllAll No Both Score", ZebraOn = false, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAllNoBothScore { get; }
        [ColumnStyle(DisplayName = "AllAll 1X", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAll1X { get; }
        [ColumnStyle(DisplayName = "AllAll 12", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAll12 { get; }
        [ColumnStyle(DisplayName = "AllAll 2X", ZebraOn = true, ColorAlgoLogic = 1, Width = 60)]
        public int? AllAll2X { get; }


        public BgSourceProvider Source { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ1 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ2 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ3 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? HQ4 { get; }
        [ColumnStyle(Width = 50, BasketballScores = true)]
        public int? AQ4 { get; }
        [ColumnStyle(Width = 150)]
        public DateTime FeedLastUpdate { get; }
        [ColumnStyle(ColorAlgoLogic = 2)]
        public int? BadMatches { get; }
        [ColumnStyle(HeaderColor = 1)]
        public int? HomeMatches { get; }
        [ColumnStyle(HeaderColor = 1)]
        public int? AwayMatches { get; }
        [ColumnStyle(HeaderColor = 1)]
        public int? Head2HeadMatches { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? FullTimeHomeWin { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? FullTimeDraw { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? FullTimeAwayWin { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? FullTimeHomeWinH2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? FullTimeDrawH2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? FullTimeAwayWinH2H { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Over3_5 { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Under3_5 { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Over3_5H2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Under3_5H2H { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Over2_5 { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Under2_5 { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Over2_5H2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Under2_5H2H { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Over1_5 { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Under1_5 { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Over1_5H2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Under1_5H2H { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Range0_1 { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Range2_3 { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? Range4Plus { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Range0_1H2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Range2_3H2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? Range4PlusH2H { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? BothScore { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? NoBothScore { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? BothScoreH2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? NoBothScoreH2H { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? HomeDraw { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? NoDraw { get; }
        [ColumnStyle(ZebraOn = true, ColorAlgoLogic = 1)]
        public int? DrawAway { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? HomeDrawH2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? NoDrawH2H { get; }
        [ColumnStyle(ColorAlgoLogic = 1)]
        public int? DrawAwayH2H { get; }

        [ColumnStyle(ZebraOn = true)]
        public decimal? HomePower { get; }
        [ColumnStyle(ZebraOn = true)]
        public decimal? AwayPower { get; }
        [ColumnStyle(ZebraOn = true)]
        public DateTime? StatisticLastUpdate { get; }
        public long? MatchId { get; }
        public int? CountryId { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public int HomeId { get; }
        public int AwayId { get; }
        public BgSport Sport { get; }
        public DateTime GameTime { get; }
        public BgFeedMonitor(BgSourceProvider source,
            BgSport sport,
            long? matchId,
            string status,
            DateTime feedLastUpdate,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int homeId,
            string away,
            int awayId,
            int? badMatches,
            int? homeMatches,
            int? awayMatches,
            int? head2HeadMatches,
            DateTime? statisticsLastUpdate,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null,
            BgMatchImpliedStatistics implied = null,
            int? fullTimeHomeWin = null,
            int? fullTimeDraw = null,
            int? fullTimeAwayWin = null,
            int? fullTimeHomeWinH2H = null,
            int? fullTimeDrawH2H = null,
            int? fullTimeAwayWinH2H = null,
            int? over3_5 = null,
            int? under3_5 = null,
            int? over3_5H2H = null,
            int? under3_5H2H = null,
            int? over2_5 = null,
            int? under2_5 = null,
            int? over2_5H2H = null,
            int? under2_5H2H = null,
            int? over1_5 = null,
            int? under1_5 = null,
            int? over1_5H2H = null,
            int? under1_5H2H = null,
            int? range0_1 = null,
            int? range2_3 = null,
            int? range4Plus = null,
            int? range0_1H2H = null,
            int? range2_3H2H = null,
            int? range4PlusH2H = null,
            int? bothScore = null,
            int? noBothScore = null,
            int? bothScoreH2H = null,
            int? noBothScoreH2H = null,
            int? homeDraw = null,
            int? noDraw = null,
            int? drawAway = null,
            int? homeDrawH2H = null,
            int? noDrawH2H = null,
            int? drawAwayH2H = null,
            decimal? homePower = null,
            decimal? awayPower = null
            )
        {
            this.Source = source;
            this.Sport = sport;
            this.MatchId = matchId;
            this.Status = status;
            this.FeedLastUpdate = feedLastUpdate;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.Time = gameStart.ToString("HH:mm");
            this.GameTime = gameStart;
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
            this.BadMatches = badMatches;
            this.HomeMatches = homeMatches;
            this.AwayMatches = awayMatches;
            this.Head2HeadMatches = head2HeadMatches;
            this.Web1 = implied?.Items[BgImpliedStatisticType.Web_1];
            this.WebX = implied?.Items[BgImpliedStatisticType.Web_X];
            this.Web2 = implied?.Items[BgImpliedStatisticType.Web_2];
            this.WebUnder1_5 = implied?.Items[BgImpliedStatisticType.Web_Under1_5];
            this.WebOver1_5 = implied?.Items[BgImpliedStatisticType.Web_Over1_5];
            this.WebUnder2_5 = implied?.Items[BgImpliedStatisticType.Web_Under2_5]; 
            this.WebOver2_5 = implied?.Items[BgImpliedStatisticType.Web_Over2_5];
            this.WebUnder3_5 = implied?.Items[BgImpliedStatisticType.Web_Under3_5];
            this.WebOver3_5 = implied?.Items[BgImpliedStatisticType.Web_Over3_5];
            this.WebRange0_1 = implied?.Items[BgImpliedStatisticType.Web_0_1]; 
            this.WebRange2_3 = implied?.Items[BgImpliedStatisticType.Web_2_3]; 
            this.WebRange4Plus = implied?.Items[BgImpliedStatisticType.Web_4Plus];
            this.WebBothScore = implied?.Items[BgImpliedStatisticType.Web_BothScore];
            this.WebNoBothScore = implied?.Items[BgImpliedStatisticType.Web_BothNoScore];
            this.Web1X = implied?.Items[BgImpliedStatisticType.Web_1X];
            this.Web12 = implied?.Items[BgImpliedStatisticType.Web_12];
            this.Web2X = implied?.Items[BgImpliedStatisticType.Web_2X];

            this.Avg1 = implied?.Items[BgImpliedStatisticType.Avg_1];
            this.AvgX = implied?.Items[BgImpliedStatisticType.Avg_X];
            this.Avg2 = implied?.Items[BgImpliedStatisticType.Avg_2];
            this.AvgUnder1_5 = implied?.Items[BgImpliedStatisticType.Avg_Under1_5];
            this.AvgOver1_5 = implied?.Items[BgImpliedStatisticType.Avg_Over1_5];
            this.AvgUnder2_5 = implied?.Items[BgImpliedStatisticType.Avg_Under2_5];
            this.AvgOver2_5 = implied?.Items[BgImpliedStatisticType.Avg_Over2_5];
            this.AvgUnder3_5 = implied?.Items[BgImpliedStatisticType.Avg_Under3_5];
            this.AvgOver3_5 = implied?.Items[BgImpliedStatisticType.Avg_Over3_5];
            this.AvgRange0_1 = implied?.Items[BgImpliedStatisticType.Avg_0_1];
            this.AvgRange2_3 = implied?.Items[BgImpliedStatisticType.Avg_2_3];
            this.AvgRange4Plus = implied?.Items[BgImpliedStatisticType.Avg_4Plus];
            this.AvgBothScore = implied?.Items[BgImpliedStatisticType.Avg_BothScore];
            this.AvgNoBothScore = implied?.Items[BgImpliedStatisticType.Avg_BothNoScore];
            this.Avg1X = implied?.Items[BgImpliedStatisticType.Avg_1X];
            this.Avg12 = implied?.Items[BgImpliedStatisticType.Avg_12];
            this.Avg2X = implied?.Items[BgImpliedStatisticType.Avg_2X];

            this.AllAll1 = implied?.Items[BgImpliedStatisticType.All_All_1];
            this.AllAllX = implied?.Items[BgImpliedStatisticType.All_All_X];
            this.AllAll2 = implied?.Items[BgImpliedStatisticType.All_All_2];
            this.AllAllUnder1_5 = implied?.Items[BgImpliedStatisticType.All_All_Under1_5];
            this.AllAllOver1_5 = implied?.Items[BgImpliedStatisticType.All_All_Over1_5];
            this.AllAllUnder2_5 = implied?.Items[BgImpliedStatisticType.All_All_Under2_5];
            this.AllAllOver2_5 = implied?.Items[BgImpliedStatisticType.All_All_Over2_5];
            this.AllAllUnder3_5 = implied?.Items[BgImpliedStatisticType.All_All_Under3_5];
            this.AllAllOver3_5 = implied?.Items[BgImpliedStatisticType.All_All_Over3_5];
            this.AllAllRange0_1 = implied?.Items[BgImpliedStatisticType.All_All_0_1];
            this.AllAllRange2_3 = implied?.Items[BgImpliedStatisticType.All_All_2_3];
            this.AllAllRange4Plus = implied?.Items[BgImpliedStatisticType.All_All_4Plus];
            this.AllAllBothScore = implied?.Items[BgImpliedStatisticType.All_All_BothScore];
            this.AllAllNoBothScore = implied?.Items[BgImpliedStatisticType.All_All_NoBothScore];
            this.AllAll1X = implied?.Items[BgImpliedStatisticType.All_All_1X];
            this.AllAll12 = implied?.Items[BgImpliedStatisticType.All_All_12];
            this.AllAll2X = implied?.Items[BgImpliedStatisticType.All_All_2X];

            this.FullTimeHomeWin = fullTimeHomeWin;
            this.FullTimeDraw = fullTimeDraw;
            this.FullTimeAwayWin = fullTimeAwayWin;
            this.FullTimeHomeWinH2H = fullTimeHomeWinH2H;
            this.FullTimeDrawH2H = fullTimeDrawH2H;
            this.FullTimeAwayWinH2H = fullTimeAwayWinH2H;
            this.Over3_5 = over3_5;
            this.Under3_5 = under3_5;
            this.Over3_5H2H = over3_5H2H;
            this.Under3_5H2H = under3_5H2H;
            this.Over2_5 = over2_5;
            this.Under2_5 = under2_5;
            this.Over2_5H2H = over2_5H2H;
            this.Under2_5H2H = under2_5H2H;
            this.Over1_5 = over1_5;
            this.Under1_5 = under1_5;
            this.Over1_5H2H = over1_5H2H;
            this.Under1_5H2H = under1_5H2H;
            this.Range0_1 = range0_1;
            this.Range2_3 = range2_3;
            this.Range4Plus = range4Plus;
            this.Range0_1H2H = range0_1H2H;
            this.Range2_3H2H = range2_3H2H;
            this.Range4PlusH2H = range4PlusH2H;
            this.BothScore = bothScore;
            this.NoBothScore = noBothScore;
            this.BothScoreH2H = bothScoreH2H;
            this.NoBothScoreH2H = noBothScoreH2H;
            this.HomeDraw = homeDraw;
            this.NoDraw = noDraw;
            this.DrawAway = drawAway;
            this.HomeDrawH2H = homeDrawH2H;
            this.NoDrawH2H = noDrawH2H;
            this.DrawAwayH2H = drawAwayH2H;
            this.HomePower = homePower;
            this.AwayPower = awayPower;
            this.StatisticLastUpdate = statisticsLastUpdate;
        }
    }
}