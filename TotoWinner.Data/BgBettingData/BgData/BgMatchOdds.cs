﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchOdds 
    {
        public BgSport Sport { get; }
        public int HomeId { get; }
        public int AwayId { get; }
        public decimal HomeOdd { get; }
        public decimal DrawOdd { get; }
        public decimal AwayOdd { get; }
        public BgMatchOdds(BgSport sport,
            int homeId,
            int awayId,
            decimal homeOdd,
            decimal drawOdd,
            decimal awayOdd)
        {
            this.Sport = sport;
            this.HomeId = homeId;
            this.AwayId = awayId;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
        }
        public override string ToString()
        {
            return $"Sport:{this.Sport} HomeId:{this.HomeId} AwayId:{this.AwayId}, HomeOdds:{this.HomeOdd} DrawOdds:{this.DrawOdd} AwayOdds:{this.AwayOdd}";
        }
    }
}
