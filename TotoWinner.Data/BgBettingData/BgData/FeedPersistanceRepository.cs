﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class FeedPersistanceRepository
    {
        public List<BgMatch> BgFeedAddOrUpdate { get; }
        public List<BgMatch> BgFeedRemove { get; }
        public FeedPersistanceRepository(List<BgMatch> bgFeedAddOrUpdate, List<BgMatch> bgFeedRemove)
        {
            BgFeedAddOrUpdate = bgFeedAddOrUpdate;
            BgFeedRemove = bgFeedRemove;
        }
    }
}
