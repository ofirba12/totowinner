﻿using System;
using System.Text.RegularExpressions;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatch : IEquatable<BgMatch>    
    {
        public BgSourceProvider Source { get; }
        public BgSport Sport { get; }
        public long MatchId { get; private set; }
        public string Status { get; }
        public int? CountryId { get; }
        public string Country { get; }
        public string League { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public DateTime GameStart { get; }
        public string Home { get; }
        public int HomeId { get; }
        public string Away { get; }
        public int AwayId { get; }
        public decimal? HomeOdd { get; private set; }
        public decimal? DrawOdd { get; private set; }
        public decimal? AwayOdd { get; private set; }
        public int? HomeScore { get; private set; }
        public int? AwayScore { get; private set; }
        public int? HQ1 { get; private set; }
        public int? AQ1 { get; private set; }
        public int? HQ2 { get; private set; }
        public int? AQ2 { get; private set; }
        public int? HQ3 { get; private set; }
        public int? AQ3 { get; private set; }
        public int? HQ4 { get; private set; }
        public int? AQ4 { get; private set; }
        public DateTime LastUpdate { get; }
        public BgMatch(BgSourceProvider source,
            BgSport sport,
            long matchId,
            string status,
            int? countryId,
            string country,
            string league,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            string home,
            int homeId,
            string away,
            int awayId,
            DateTime lastUpdate,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null)
        {
            this.Source = source;
            this.Sport = sport;
            this.MatchId = matchId;
            this.Status = status;
            this.CountryId = countryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.Home = home;
            this.HomeId = homeId;
            this.Away = away;
            this.AwayId = awayId;
            this.LastUpdate = lastUpdate;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
        }
        public BgMatch(BgRawMatch match, string home, string away, string league, string country) 
        {
            this.Source = match.Source;
            this.Sport = match.Sport;
            this.MatchId = match.MatchId;
            this.Status = match.Status;
            this.CountryId = match.CountryId;
            this.Country = country;
            this.League = league;
            this.LeagueId = match.LeagueId;
            this.IsCup = match.IsCup;
            this.GameStart = match.GameStart;
            this.Home = home;
            this.HomeId = match.HomeId;
            this.Away = away;
            this.AwayId = match.AwayId;
            this.LastUpdate = match.LastUpdate;
            this.HomeOdd = match.HomeOdd;
            this.DrawOdd = match.DrawOdd;
            this.AwayOdd = match.AwayOdd;
            this.HomeScore = match.HomeScore;
            this.AwayScore = match.AwayScore;
            this.HQ1 = match.HQ1;
            this.AQ1 = match.AQ1;
            this.HQ2 = match.HQ2;
            this.AQ2 = match.AQ2;
            this.HQ3 = match.HQ3;
            this.AQ3 = match.AQ3;
            this.HQ4 = match.HQ4;
            this.AQ4 = match.AQ4;
        }

        public bool Equals(BgMatch other)
        {
            return this.Sport == other.Sport
                //&& this.MatchId == other.MatchId
                //&& this.LeagueId == other.LeagueId
                && this.HomeId == other.HomeId
                && this.AwayId == other.AwayId
                && this.GameStart.Date == other.GameStart.Date;
        }
        public void SetOdds(decimal homeOdd,
            decimal drawOdd,
            decimal awayOdd)
        {
            this.HomeOdd = homeOdd;
            this.DrawOdd= drawOdd;
            this.AwayOdd = awayOdd;
        }
        public void SetScores(int homeScores,
            int awayScores)
        {
            this.HomeScore = homeScores;
            this.AwayScore = awayScores;
        }
        public void SetMatchId(long matchId)
        {
            this.MatchId = matchId;
        }
        //public void SetQuaters(int homeQ1,
        //    int awayQ1,
        //    int homeQ2,
        //    int awayQ2,
        //    int homeQ3,
        //    int awayQ3,
        //    int homeQ4,
        //    int awayQ4)
        //{
        //    this.HQ1 = homeQ1;
        //    this.AQ1 = awayQ1;
        //    this.HQ2 = homeQ2;
        //    this.AQ2 = awayQ2;
        //    this.HQ3 = homeQ3;
        //    this.AQ3 = awayQ3;
        //    this.HQ4 = homeQ4;
        //    this.AQ4 = awayQ4;
        //}
        public override string ToString()
        {
            return $"MatchId:{this.MatchId} Source:{this.Source} Sport:{this.Sport} CountryId:{this.CountryId} LeagueId:{this.LeagueId} HomeId:{this.HomeId} AwayId:{this.AwayId} GameStart:{this.GameStart} LastUpdate:{this.LastUpdate}";
        }

    }

}
