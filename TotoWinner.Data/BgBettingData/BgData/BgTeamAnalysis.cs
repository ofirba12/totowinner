﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamAnalysis : IEquatable<BgTeamAnalysis>
    {
        public int TeamId { get; }
        public int TotalShows { get; }
        public int MissingOdds { get; }
        public int MissingResult { get; }
        public BgTeamAnalysis(
           int teamId,
           int totalShows,
           int missingOdds,
           int missingResult)
        {
            this.TeamId = teamId;
            this.TotalShows = totalShows;
            this.MissingOdds = missingOdds;
            this.MissingResult = missingResult;
        }

        public bool Equals(BgTeamAnalysis other)
        {
            return this.TeamId == other.TeamId;
        }
    }
}
