﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamsAnalysisRepository
    {
        public List<BgMatchForAnalysis> TeamAmatchesAllAll { get; }
        public List<BgMatchForAnalysis> TeamBmatchesAllAll { get; }
        public List<BgMatchForAnalysis> H2hMatches { get; }
        public Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>> TeamAmatchesFiltered { get; }
        public Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>> TeamAmatchesFilteredWithIndexes { get; }
        public Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>> TeamBmatchesFiltered { get; }
        public Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>> TeamBmatchesFilteredWithIndexes { get; }
        public Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>> H2hFiltered { get; }
        public Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>> H2hFilteredWithIndexes { get; }
        public BgTeamsAnalysisRepository(List<BgMatchForAnalysis> teamAmatchesAllAll, 
            List<BgMatchForAnalysis> teamBmatchesAllAll,
            List<BgMatchForAnalysis> h2hMatches,
            Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>> teamAmatchesFiltered, 
            Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>> teamAmatchesFilteredWithIndexes, 
            Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>> teamBmatchesFiltered, 
            Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>> teamBmatchesFilteredWithIndexes,
            Dictionary<BgTeamsFilter, List<BgMatchForAnalysis>> h2hFiltered,
            Dictionary<BgTeamsFilter, List<BgMatchWithIndexes>> h2hFilteredWithIndexes)
        {
            this.TeamAmatchesAllAll = teamAmatchesAllAll;
            this.TeamBmatchesAllAll = teamBmatchesAllAll;
            this.H2hMatches = h2hMatches;
            this.TeamAmatchesFiltered = teamAmatchesFiltered;
            this.TeamBmatchesFiltered = teamBmatchesFiltered;
            this.TeamAmatchesFilteredWithIndexes = teamAmatchesFilteredWithIndexes;
            this.TeamBmatchesFilteredWithIndexes = teamBmatchesFilteredWithIndexes;
            this.H2hFiltered = h2hFiltered;
            this.H2hFilteredWithIndexes = h2hFilteredWithIndexes;
        }
    }
}
