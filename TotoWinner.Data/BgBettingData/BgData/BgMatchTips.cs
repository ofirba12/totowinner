﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData.BgDataApi;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchTips : BgDataBetTips
    {
        public long MatchId { get; }
        public DateTime LastUpdate { get; }
        public BgMatchTips(long matchId, DateTime lastUpdate, BgDataBetTips tips) : base(tips)
        {
            this.MatchId = matchId;
            this.LastUpdate = lastUpdate;
        }
    }
}
