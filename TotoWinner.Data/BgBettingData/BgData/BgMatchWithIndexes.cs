﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchWithIndexes : BgMatch, IEquatable<BgMatch>
    {
        public BgMatchIndexes Indexes { get;}
        public BgMatchWithIndexes(BgSourceProvider source,
           BgSport sport,
           long matchId,
           string status,
           int? countryId,
           string country,
           string league,
           int leagueId,
           bool isCup,
           DateTime gameStart,
           string home,
           int homeId,
           string away,
           int awayId,
           DateTime lastUpdate,
           BgMatchIndexes indexes,
           decimal? homeOdd = null,
           decimal? drawOdd = null,
           decimal? awayOdd = null,
           int? homeScore = null,
           int? awayScore = null,
           int? homeQ1 = null,
           int? awayQ1 = null,
           int? homeQ2 = null,
           int? awayQ2 = null,
           int? homeQ3 = null,
           int? awayQ3 = null,
           int? homeQ4 = null,
           int? awayQ4 = null) :
                base(source, sport, matchId, status, countryId, country, league, leagueId, isCup, gameStart, home, homeId, away, awayId,
                    lastUpdate, homeOdd, drawOdd, awayOdd, homeScore, awayScore, homeQ1, awayQ1, homeQ2, awayQ2, homeQ3, awayQ3, homeQ4, awayQ4)
        {
            this.Indexes = indexes;
        }
        public BgMatchWithIndexes(BgMatchForAnalysis bgMatchForAnalysis, BgMatchIndexes indexes) : this (bgMatchForAnalysis.Source,
                        bgMatchForAnalysis.Sport,
                        bgMatchForAnalysis.MatchId,
                        bgMatchForAnalysis.Status,
                        bgMatchForAnalysis.CountryId,
                        bgMatchForAnalysis.Country,
                        bgMatchForAnalysis.League,
                        bgMatchForAnalysis.LeagueId,
                        bgMatchForAnalysis.IsCup,
                        bgMatchForAnalysis.GameStart,
                        bgMatchForAnalysis.Home,
                        bgMatchForAnalysis.HomeId,
                        bgMatchForAnalysis.Away,
                        bgMatchForAnalysis.AwayId,
                        bgMatchForAnalysis.LastUpdate,
                        indexes,
                        bgMatchForAnalysis.HomeOdd, bgMatchForAnalysis.DrawOdd, bgMatchForAnalysis.AwayOdd,
                        bgMatchForAnalysis.HomeScore, bgMatchForAnalysis.AwayScore,
                        bgMatchForAnalysis.HQ1, bgMatchForAnalysis.AQ1,
                        bgMatchForAnalysis.HQ2, bgMatchForAnalysis.AQ2,
                        bgMatchForAnalysis.HQ3, bgMatchForAnalysis.AQ3,
                        bgMatchForAnalysis.HQ4, bgMatchForAnalysis.AQ4)
        {
        }
    }
}
