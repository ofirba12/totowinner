﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data.BgBettingData.BgDataApi;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchTypedTip
    {
        public long MatchId { get; }
        public BgTipType Type { get; }
        public string Title { get; }
        public int Value { get; }
        public int Stars { get; }
        public List<string> Descriptions { get; }
        public bool FreezeTip { get; }
        public DateTime LastUpdate { get; }

        public BgMatchTypedTip(long matchId, 
            BgTipType type, 
            string title, 
            int value, 
            int stars, 
            List<string> description, 
            bool freezeTip,
            DateTime lastUpdate)
        {
            this.MatchId = matchId;
            this.Type = type;
            this.Title = title;
            this.Value = value;
            this.Stars = stars;
            this.Descriptions = description;
            this.FreezeTip = freezeTip;
            this.LastUpdate = lastUpdate;
        }
    }
}
