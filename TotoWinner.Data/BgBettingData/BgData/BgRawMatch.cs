﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgRawMatch : IEquatable<BgRawMatch>    
    {
        public BgSourceProvider Source { get; }
        public BgSport Sport { get; }
        public long MatchId { get; private set; }
        public string Status { get; }
        public int? CountryId { get; }
        public int LeagueId { get; }
        public bool IsCup { get; }
        public DateTime GameStart { get; }
        public int HomeId { get; }
        public int AwayId { get; }
        public decimal? HomeOdd { get; private set; }
        public decimal? DrawOdd { get; private set; }
        public decimal? AwayOdd { get; private set; }
        public int? HomeScore { get; private set; }
        public int? AwayScore { get; private set; }
        public int? HQ1 { get; private set; }
        public int? AQ1 { get; private set; }
        public int? HQ2 { get; private set; }
        public int? AQ2 { get; private set; }
        public int? HQ3 { get; private set; }
        public int? AQ3 { get; private set; }
        public int? HQ4 { get; private set; }
        public int? AQ4 { get; private set; }
        public DateTime LastUpdate { get; }
        public BgRawMatch(BgSourceProvider source,
            BgSport sport,
            long matchId,
            string status,
            int? countryId,
            int leagueId,
            bool isCup,
            DateTime gameStart,
            int homeId,
            int awayId,
            DateTime lastUpdate,
            decimal? homeOdd = null,
            decimal? drawOdd = null,
            decimal? awayOdd = null,
            int? homeScore = null,
            int? awayScore = null,
            int? homeQ1 = null,
            int? awayQ1 = null,
            int? homeQ2 = null,
            int? awayQ2 = null,
            int? homeQ3 = null,
            int? awayQ3 = null,
            int? homeQ4 = null,
            int? awayQ4 = null)
        {
            this.Source = source;
            this.Sport = sport;
            this.MatchId = matchId;
            this.Status = status;
            this.CountryId = countryId;
            this.LeagueId = leagueId;
            this.IsCup = isCup;
            this.GameStart = gameStart;
            this.HomeId = homeId;
            this.AwayId = awayId;
            this.LastUpdate = lastUpdate;
            this.HomeOdd = homeOdd;
            this.DrawOdd = drawOdd;
            this.AwayOdd = awayOdd;
            this.HomeScore = homeScore;
            this.AwayScore = awayScore;
            this.HQ1 = homeQ1;
            this.AQ1 = awayQ1;
            this.HQ2 = homeQ2;
            this.AQ2 = awayQ2;
            this.HQ3 = homeQ3;
            this.AQ3 = awayQ3;
            this.HQ4 = homeQ4;
            this.AQ4 = awayQ4;
        }

        public bool Equals(BgRawMatch other)
        {
            return this.Sport == other.Sport
                //&& this.MatchId == other.MatchId
                //&& this.LeagueId == other.LeagueId
                && this.HomeId == other.HomeId
                && this.AwayId == other.AwayId
                && this.GameStart.Date == other.GameStart.Date;
        }
        public override string ToString()
        {
            return $"MatchId:{this.MatchId} Source:{this.Source} Sport:{this.Sport} CountryId:{this.CountryId} LeagueId:{this.LeagueId} HomeId:{this.HomeId} AwayId:{this.AwayId} GameStart:{this.GameStart} LastUpdate:{this.LastUpdate}";
        }

    }

}
