﻿namespace TotoWinner.Data.BgBettingData
{
    public class BgLeague
    {
        public int LeagueId { get; }
        public string LeagueName { get; }

        public BgLeague(int leagureId, string leagueName)
        {
            this.LeagueId = leagureId;
            this.LeagueName = leagueName;
        }
    }
}
