﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchInfo
    {
        public long MatchId { get; set; }
        public int BadMatches { get; set; }
        public int HomeMatches { get; set; }
        public int AwayMatches { get; set; }
        public int Head2HeadMatches { get; set; }
        public DateTime LastUpdate { get; set; }

        public BgMatchInfo(long matchId,
            int badMatches, 
            int homeMatches, 
            int awayMatches,
            int head2HeadMatches,
            DateTime lastUpdate)
        {
            this.MatchId = matchId;
            this.BadMatches = badMatches;
            this.HomeMatches = homeMatches;
            this.AwayMatches = awayMatches;
            this.Head2HeadMatches = head2HeadMatches;
            this.LastUpdate = lastUpdate;    
        }
    }
}
