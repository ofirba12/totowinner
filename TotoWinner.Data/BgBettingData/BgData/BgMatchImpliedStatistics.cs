﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchImpliedStatistics
    {
        public ReadOnlyDictionary<BgImpliedStatisticType, int> Items { get; }
        public BgMatchImpliedStatistics(Dictionary<BgImpliedStatisticType, int> items)
        {
            Items = new ReadOnlyDictionary<BgImpliedStatisticType, int>(items);
        }
    }
}
