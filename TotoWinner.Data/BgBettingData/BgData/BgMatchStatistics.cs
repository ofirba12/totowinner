﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchStatistics    
    {
        public long MatchId { get; set; }
        public int StatisticIndex { get; set; }
        public int? FullTimeHomeWin { get; set; }
        public int? FullTimeDraw { get; set; }
        public int? FullTimeAwayWin { get; set; }
        public int? FullTimeHomeWinH2H { get; set; }
        public int? FullTimeDrawH2H { get; set; }
        public int? FullTimeAwayWinH2H { get; set; }
        public int? Over3_5 { get; set; }
        public int? Under3_5 { get; set; }
        public int? Over3_5H2H { get; set; }
        public int? Under3_5H2H { get; set; }
        public int? Over2_5 { get; set; }
        public int? Under2_5 { get; set; }
        public int? Over2_5H2H { get; set; }
        public int? Under2_5H2H { get; set; }
        public int? Over1_5 { get; set; }
        public int? Under1_5 { get; set; }
        public int? Over1_5H2H { get; set; }
        public int? Under1_5H2H { get; set; }
        public int? Range0_1 { get; set; }
        public int? Range2_3 { get; set; }
        public int? Range4Plus { get; set; }
        public int? Range0_1H2H { get; set; }
        public int? Range2_3H2H { get; set; }
        public int? Range4PlusH2H { get; set; }
        public int? BothScore { get; set; }
        public int? NoBothScore { get; set; }
        public int? BothScoreH2H { get; set; }
        public int? NoBothScoreH2H { get; set; }
        public int? HomeDraw { get; set; }
        public int? NoDraw { get; set; }
        public int? DrawAway { get; set; }
        public int? HomeDrawH2H { get; set; }
        public int? NoDrawH2H { get; set; }
        public int? DrawAwayH2H { get; set; }
        public decimal? HomePower { get; set; }
        public decimal? AwayPower { get; set; }
        public DateTime LastUpdate { get; set; }

        public BgMatchStatistics(long matchId,
            int statisticIndex,
            decimal? fullTimeHomeWin, 
            decimal? fullTimeDraw, 
            decimal? fullTimeAwayWin, 
            decimal? fullTimeHomeWinH2H, 
            decimal? fullTimeDrawH2H, 
            decimal? fullTimeAwayWinH2H, 
            decimal? over3_5, 
            decimal? under3_5, 
            decimal? over3_5H2H, 
            decimal? under3_5H2H, 
            decimal? over2_5, 
            decimal? under2_5, 
            decimal? over2_5H2H, 
            decimal? under2_5H2H, 
            decimal? over1_5, 
            decimal? under1_5, 
            decimal? over1_5H2H, 
            decimal? under1_5H2H, 
            decimal? range0_1, 
            decimal? range2_3, 
            decimal? range4Plus, 
            decimal? range0_1H2H, 
            decimal? range2_3H2H, 
            decimal? range4PlusH2H, 
            decimal? bothScore, 
            decimal? noBothScore, 
            decimal? bothScoreH2H, 
            decimal? noBothScoreH2H, 
            decimal? homeDraw, 
            decimal? noDraw, 
            decimal? drawAway, 
            decimal? homeDrawH2H, 
            decimal? noDrawH2H,
            decimal? drawAwayH2H, 
            decimal? homePower, 
            decimal? awayPower, 
            DateTime lastUpdate)
        {
            this.MatchId = matchId;
            this.StatisticIndex = statisticIndex;
            this.FullTimeHomeWin = fullTimeHomeWin.HasValue ? Decimal.ToInt32(fullTimeHomeWin.Value) : default(Nullable<int>);
            this.FullTimeDraw = fullTimeDraw.HasValue ? Decimal.ToInt32(fullTimeDraw.Value) : default(Nullable<int>);
            this.FullTimeAwayWin = fullTimeAwayWin.HasValue ? Decimal.ToInt32(fullTimeAwayWin.Value) : default(Nullable<int>);
            this.FullTimeHomeWinH2H = fullTimeHomeWinH2H.HasValue ? Decimal.ToInt32(fullTimeHomeWinH2H.Value) : default(Nullable<int>);
            this.FullTimeDrawH2H = fullTimeDrawH2H.HasValue ? Decimal.ToInt32(fullTimeDrawH2H.Value) : default(Nullable<int>);
            this.FullTimeAwayWinH2H = fullTimeAwayWinH2H.HasValue ? Decimal.ToInt32(fullTimeAwayWinH2H.Value) : default(Nullable<int>);
            this.Over3_5 = over3_5.HasValue ? Decimal.ToInt32(over3_5.Value) : default(Nullable<int>);
            this.Under3_5 = under3_5.HasValue ? Decimal.ToInt32(under3_5.Value) : default(Nullable<int>);
            this.Over3_5H2H = over3_5H2H.HasValue ? Decimal.ToInt32(over3_5H2H.Value) : default(Nullable<int>);
            this.Under3_5H2H = under3_5H2H.HasValue ? Decimal.ToInt32(under3_5H2H.Value) : default(Nullable<int>);
            this.Over2_5 = over2_5.HasValue ? Decimal.ToInt32(over2_5.Value) : default(Nullable<int>);
            this.Under2_5 = under2_5.HasValue ? Decimal.ToInt32(under2_5.Value) : default(Nullable<int>);
            this.Over2_5H2H = over2_5H2H.HasValue ? Decimal.ToInt32(over2_5H2H.Value) : default(Nullable<int>);
            this.Under2_5H2H = under2_5H2H.HasValue ? Decimal.ToInt32(under2_5H2H.Value) : default(Nullable<int>);
            this.Over1_5 = over1_5.HasValue ? Decimal.ToInt32(over1_5.Value) : default(Nullable<int>);
            this.Under1_5 = under1_5.HasValue ? Decimal.ToInt32(under1_5.Value) : default(Nullable<int>);
            this.Over1_5H2H = over1_5H2H.HasValue ? Decimal.ToInt32(over1_5H2H.Value) : default(Nullable<int>);
            this.Under1_5H2H = under1_5H2H.HasValue ? Decimal.ToInt32(under1_5H2H.Value) : default(Nullable<int>);
            this.Range0_1 = range0_1.HasValue ? Decimal.ToInt32(range0_1.Value) : default(Nullable<int>);
            this.Range2_3 = range2_3.HasValue ? Decimal.ToInt32(range2_3.Value) : default(Nullable<int>);
            this.Range4Plus = range4Plus.HasValue ? Decimal.ToInt32(range4Plus.Value) : default(Nullable<int>);
            this.Range0_1H2H = range0_1H2H.HasValue ? Decimal.ToInt32(range0_1H2H.Value) : default(Nullable<int>);
            this.Range2_3H2H = range2_3H2H.HasValue ? Decimal.ToInt32(range2_3H2H.Value) : default(Nullable<int>);
            this.Range4PlusH2H = range4PlusH2H.HasValue ? Decimal.ToInt32(range4PlusH2H.Value) : default(Nullable<int>);
            this.BothScore = bothScore.HasValue ? Decimal.ToInt32(bothScore.Value) : default(Nullable<int>);
            this.NoBothScore = noBothScore.HasValue ? Decimal.ToInt32(noBothScore.Value) : default(Nullable<int>);
            this.BothScoreH2H = bothScoreH2H.HasValue ? Decimal.ToInt32(bothScoreH2H.Value) : default(Nullable<int>);
            this.NoBothScoreH2H = noBothScoreH2H.HasValue ? Decimal.ToInt32(noBothScoreH2H.Value) : default(Nullable<int>);
            this.HomeDraw = homeDraw.HasValue ? Decimal.ToInt32(homeDraw.Value) : default(Nullable<int>);
            this.NoDraw = noDraw.HasValue ? Decimal.ToInt32(noDraw.Value) : default(Nullable<int>);
            this.DrawAway = drawAway.HasValue ? Decimal.ToInt32(drawAway.Value) : default(Nullable<int>);
            this.HomeDrawH2H = homeDrawH2H.HasValue ? Decimal.ToInt32(homeDrawH2H.Value) : default(Nullable<int>);
            this.NoDrawH2H = noDrawH2H.HasValue ? Decimal.ToInt32(noDrawH2H.Value) : default(Nullable<int>);
            this.DrawAwayH2H = drawAwayH2H.HasValue ? Decimal.ToInt32(drawAwayH2H.Value) : default(Nullable<int>);
            this.HomePower = homePower;
            this.AwayPower = awayPower;
            this.LastUpdate = lastUpdate;    
        }
    }
}
