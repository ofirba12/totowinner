﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFeedKey : IEquatable<BgFeedKey>
    {
        public BgSport Sport { get; set; }
        public DateTime GameTime { get; set; }
        public int HomeTeamId { get; set; }
        public int AwayTeamId { get; set; }
        public BgFeedKey(BgSport sport,
            DateTime gameTime,
            int homeId,
            int awayId)
        {
            this.Sport = sport;
            this.GameTime = gameTime;
            this.HomeTeamId = homeId;
            this.AwayTeamId = awayId;
        }

        public bool Equals(BgFeedKey other)
        {
            return this.GameTime.Date == other.GameTime.Date &&
                this.Sport == other.Sport &&    
                    (this.HomeTeamId == other.HomeTeamId && this.AwayTeamId == other.AwayTeamId) ||
                    (this.HomeTeamId == other.AwayTeamId && this.AwayTeamId == other.HomeTeamId);
        }

        public bool Equals(BgFeedKey x, BgFeedKey y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.GameTime.Date.GetHashCode() ^ this.Sport.GetHashCode() ^ (this.HomeTeamId.GetHashCode() + this.AwayTeamId.GetHashCode());
        }
    }
}
