﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class MergeFeedResult
    {
        public List<BgMatch> FinalFeedToAddOrUpdate { get; }
        public List<BgMatch> FeedToRemove { get; }

        //public BgDataRepository(Dictionary<BgFeedKey, BgMatch> feedWithNoDuplicates, Dictionary<BgFeedKey, List<BgMatch>> feedWithDuplicates)
        //{
        //    this.FeedWithNoDuplicates = feedWithNoDuplicates;
        //    this.FeedWithDuplicates = feedWithDuplicates;
        //}
    }
}
