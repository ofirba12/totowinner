﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgFeedExactKey : IEquatable<BgFeedExactKey>
    {
        public BgSport Sport { get; set; }
        public DateTime GameTime { get; set; }
        public int HomeTeamId { get; set; }
        public int AwayTeamId { get; set; }
        public BgFeedExactKey(BgSport sport,
            DateTime gameTime,
            int homeId,
            int awayId)
        {
            this.Sport = sport;
            this.GameTime = gameTime;
            this.HomeTeamId = homeId;
            this.AwayTeamId = awayId;
        }

        public bool Equals(BgFeedExactKey other)
        {
            return this.GameTime.Date == other.GameTime.Date &&
                this.Sport == other.Sport && this.HomeTeamId == other.HomeTeamId && this.AwayTeamId == other.AwayTeamId;
        }

        public bool Equals(BgFeedExactKey x, BgFeedExactKey y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return this.GameTime.Date.GetHashCode() ^ this.Sport.GetHashCode() ^ (this.HomeTeamId.GetHashCode() + this.AwayTeamId.GetHashCode());
        }
    }
}
