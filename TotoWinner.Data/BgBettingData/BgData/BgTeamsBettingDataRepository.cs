﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamsBettingDataRepository
    {
        public int GamesATotal { get; }
        public int GamesBTotal { get; }
        public int GamesH2HTotal { get; }
        public int RedLinesA { get; }
        public int RedLinesB { get; }

        public BgTeamsBettingDataRepository(int gamesATotal, int gamesBTotal, int gamesH2HTotal, int redLinesA, int redLinesB)
        {
            this.GamesATotal = gamesATotal;
            this.GamesBTotal = gamesBTotal;
            this.GamesH2HTotal = gamesH2HTotal;
            this.RedLinesA = redLinesA;
            this.RedLinesB = redLinesB;
        }
    }
}
