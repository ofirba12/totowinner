﻿using System;
using System.Drawing;
using TotoWinner.Data.BgBettingData;

namespace TotoWinner.Data.BgBettingData
{
    public class BgLeaguesWhitelistMonitor
    {
        public int LeagueId { get; }
        public string League { get; }
        public BgLeaguesWhitelistMonitor(int leagueId,
            string leagueName
            )
        {
            this.League = leagueName;
            this.LeagueId = leagueId;
        }
    }
}