﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.BgBettingData
{
    public class BgMatchForAnalysis : BgMatch, IEquatable<BgMatch>
    {
        public bool MissingOdds { get; }
        public bool MissingScores { get; }
        public bool MissingQuarters { get; }
        public BgMatchForAnalysis(BgSourceProvider source,
           BgSport sport,
           long matchId,
           string status,
           int? countryId,
           string country,
           string league,
           int leagueId,
           bool isCup,
           DateTime gameStart,
           string home,
           int homeId,
           string away,
           int awayId,
           DateTime lastUpdate,
           decimal? homeOdd = null,
           decimal? drawOdd = null,
           decimal? awayOdd = null,
           int? homeScore = null,
           int? awayScore = null,
           int? homeQ1 = null,
           int? awayQ1 = null,
           int? homeQ2 = null,
           int? awayQ2 = null,
           int? homeQ3 = null,
           int? awayQ3 = null,
           int? homeQ4 = null,
           int? awayQ4 = null) : 
                base(source, sport, matchId, status, countryId, country, league, leagueId, isCup, gameStart, home, homeId, away, awayId,
                    lastUpdate, homeOdd, drawOdd, awayOdd, homeScore, awayScore, homeQ1, awayQ1, homeQ2, awayQ2, homeQ3, awayQ3, homeQ4, awayQ4)
        {
            this.MissingOdds = !this.HomeOdd.HasValue
                || !this.DrawOdd.HasValue
                || !this.AwayOdd.HasValue;
            this.MissingScores = !this.HomeScore.HasValue
                || !this.AwayScore.HasValue;
            this.MissingQuarters = sport == BgSport.Basketball && (!this.HQ1.HasValue
                || !this.AQ1.HasValue
                || !this.HQ2.HasValue
                || !this.AQ2.HasValue
                || !this.HQ3.HasValue
                || !this.AQ3.HasValue
                || !this.HQ4.HasValue
                || !this.AQ4.HasValue);
        }
    }
}
