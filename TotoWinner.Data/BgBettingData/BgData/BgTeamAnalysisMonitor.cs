﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamAnalysisMonitor : BgTeamAnalysis
    {
        public string TeamName { get; set; }
        public DateTime LastUpdate{ get; set; }
        public BgTeamAnalysisMonitor(int teamId, string team, int totalShows, int missingOdds, int missingResult, DateTime lastUpdate) 
            :base (teamId, totalShows, missingOdds,missingResult)
        {
            this.TeamName = team;
            this.LastUpdate = lastUpdate;
        }
    }
}
