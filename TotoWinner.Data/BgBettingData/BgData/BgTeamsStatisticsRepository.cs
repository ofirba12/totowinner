﻿using System.Collections.Generic;

namespace TotoWinner.Data.BgBettingData
{
    public class BgTeamsStatisticsRepository
    {
        public BgTeamsStatisticsRepository(Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>> TeamA,
            Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>> TeamB,
            Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>> H2H)
        {
            this.TeamA = TeamA;
            this.TeamB = TeamB;
            this.H2H = H2H;
        }

        public Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>> TeamA { get; }
        public Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>> TeamB { get; }
        public Dictionary<BgBetType, Dictionary<BgTeamsFilter, ITeamStatistics>> H2H { get; }
    }
}
