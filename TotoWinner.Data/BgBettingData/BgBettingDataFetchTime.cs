﻿using System;

namespace TotoWinner.Data.BgBettingData
{
    public class BgBettingDataFetchTime : IEquatable<BgBettingDataFetchTime>    
    {
        public int Hour { get; }
        public int Minute { get; }

        public BgBettingDataFetchTime(int hour, int minute)
        {
            try
            {
                if (hour < 0 || hour > 23)
                    throw new Exception($"Illegal hour found: {hour}");
                if (minute < 0 || minute > 59)
                    throw new Exception($"Illegal minute found: {minute}");
                this.Hour = hour;
                this.Minute = minute;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BgBettingDataFetchTime)}", ex);
            }
        }

        public bool Equals(BgBettingDataFetchTime other)
        {
            return this.Hour == other.Hour && this.Minute == other.Minute;
        }
        public override int GetHashCode()
        {
            return Hour * 100 + Minute;
        }
    }
}
