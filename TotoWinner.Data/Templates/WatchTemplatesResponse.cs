﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class WatchTemplatesBetsResult
    {
        public Dictionary<short, Bet> BaseBets { get; set; }
        public TemplateBaseBetsStatus Status { get; set; }
    }
    public class WatchTemplatesResponse
    {
        //public Dictionary<int, Dictionary<short, Bet>> TemplatesBaseBets { get; set; }
        public Dictionary<int, WatchTemplatesBetsResult> TemplatesBaseBets { get; set; }

        public WatchTemplatesResponse(Dictionary<int, WatchTemplatesBetsResult> input)
        {
            this.TemplatesBaseBets = input;
        }
        public override string ToString()
        {
            var templatesStr = new StringBuilder();
            foreach (var t in this.TemplatesBaseBets.Keys)
                templatesStr.AppendLine(t.ToString());
            return $"Templates: {templatesStr.ToString()}";
        }
    }
}
