﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GetAllTemplatesResponse
    {
        public List<Template> Templates { get; set; }
        public GetAllTemplatesResponse(List<Template> templates)
        {
            this.Templates = templates;
        }
    }
}
