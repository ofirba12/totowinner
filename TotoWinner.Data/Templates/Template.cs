﻿using System;

namespace TotoWinner.Data
{
    public class Template
    {
        public int Id { get; private set; }
        public ProgramType Type { get; private set; }
        public string Name { get; private set; }
        public string Value { get; private set; }

        public Template(int id, ProgramType type, string name, string value)
        {
            try
            {
                this.Id = id;
                this.Type = type;
                this.Name = name;
                this.Value = value;
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(Template)};", ex);
            }
        }
        public override string ToString()
        {
            return $"Id: {this.Id} | Type: {this.Type} | Name: {this.Name} | Value: {this.Value}";
        }
    }
}
