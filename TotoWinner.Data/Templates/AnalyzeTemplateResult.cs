﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class AnalyzeTemplateResult
    {
        public List<string> BankerPattern { get; }
        public int BankerSize { get; }
        public List<string> DoublePattern { get; }
        public int DoubleSize { get; }

        public AnalyzeTemplateResult(List<string> bPattern, int bankerSize, List<string> dPattern, int doubleSize)
        {
            try
            {
                this.BankerPattern = bPattern;
                this.BankerSize = bankerSize;
                this.DoublePattern = dPattern;
                this.DoubleSize = doubleSize;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(AnalyzeTemplateResult)};", ex);
            }
        }
    }
}