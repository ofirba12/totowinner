﻿namespace TotoWinner.Data
{
    public class TemplateDeleteResponse
    {
        public int TemplateId { get; set; }
        public TemplateDeleteResponse(int id)
        {
            this.TemplateId = id;
        }
    }
}
