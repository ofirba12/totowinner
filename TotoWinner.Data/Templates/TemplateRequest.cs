﻿using System;

namespace TotoWinner.Data
{
    public class TemplateRequest
    {
        public Template TemplateInfo { get;  set; }
        public override string ToString()
        {
            return $"Template: {this.TemplateInfo.ToString()}";
        }
    }
}
