﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class WatchTemplatesRequest
    {
        public class RatesInfo
        {
            public int GameIndex { get; set; }
            public double Rate1 { get; set; }
            public double RateX { get; set; }
            public double Rate2 { get; set; }
        }
        public List<Template> Templates { get; set; }
        //public Dictionary<int, Dictionary<BetOutputType, BetDetails>> Bets { get; set; }
        public List<RatesInfo> Rates { get; set; }
        //public int ProgramId { get; set; }

        public override string ToString()
        {
            var templatesStr = new StringBuilder();
            this.Templates.ForEach(t => templatesStr.AppendLine(t.Value));
            return $"Templates: {templatesStr.ToString()}";
        }
    }
}
