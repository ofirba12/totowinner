﻿namespace TotoWinner.Data
{
    public class TemplateAddResponse
    {
        public Template TemplateInfo { get; set; }
        public TemplateAddResponse(Template template)
        {
            this.TemplateInfo = template;
        }
    }
}
