﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GetAllSystemsWithFinalResultResponse
    {
        public List<SystemParametersWithFinalResult> Systems { get; set; }
        public GetAllSystemsWithFinalResultResponse(List<SystemParametersWithFinalResult> systems)
        {
            this.Systems = systems;
        }
    }
}
