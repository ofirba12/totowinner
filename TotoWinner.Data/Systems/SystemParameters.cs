﻿using System;

namespace TotoWinner.Data
{
    public class SystemParameters
    {
        public int SystemId { get; set;}
        public SystemType Type { get; set; } 
        public int ProgramId { get; set; }
        public ProgramType ProgramType { get; set; }
        public string Name { get; set; }
        public string SystemPayload { get; set; }
        public int? RefrenceSystemId { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
