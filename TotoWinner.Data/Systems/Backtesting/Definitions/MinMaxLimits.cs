﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class MinMaxLimits
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public override string ToString()
        {
            return $"Min: {Min} | Max: {Max}";
        }

    }
}
