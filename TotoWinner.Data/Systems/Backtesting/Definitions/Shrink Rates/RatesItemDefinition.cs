﻿namespace TotoWinner.Data
{
    public class RatesItemDefinition
    {
        public BtRatesShrinkFieldType Type { get; set; }
        public double From { get; set; }
        public double To { get; set; }
        public bool Valid { get; set; }
        public bool Selected { get; set; }
        public override string ToString()
        {
            var trace = $"Type={Type} | From=[{From}] | To=[{To}] | Valid=[{Valid}] | Selected=[{Selected}]";
            return trace;
        }

    }
}
