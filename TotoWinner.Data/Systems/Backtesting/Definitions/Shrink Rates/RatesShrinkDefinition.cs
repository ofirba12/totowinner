﻿using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class RatesShrinkDefinition
    {
        public List<RatesItemDefinition> Collection { get; set; }
        public override string ToString()
        {
            var trace = new StringBuilder();
            trace.AppendLine("RatesShrinkDefinition:");
            this.Collection.ForEach(item => trace.AppendLine(item.ToString()));
            return trace.ToString();
        }

    }
}
