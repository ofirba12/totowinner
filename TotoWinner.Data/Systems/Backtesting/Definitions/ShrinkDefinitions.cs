﻿using System.Text;

namespace TotoWinner.Data
{
    public class ShrinkDefinitions
    {
        public BasicShrinkDefinition Basic { get; set; }
        public ShapesShrinkDefinition Shapes { get; set; }
        public RatesShrinkDefinition Rates { get; set; }
        public override string ToString()
        {
            var trace = new StringBuilder();
            trace.AppendLine(Basic.ToString());
            trace.AppendLine(Shapes.ToString());
            trace.AppendLine(Rates.ToString());
            return trace.ToString();
        }
    }
}
