﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GamesDefinition
    {
        public int Id { get; set; }
        public BtGameDefinitionType RowType { get; set; }
        public string BetType { get; set; }
        public string RateType { get; set; }
        public string BetForType { get; set; }
        public double RateFrom { get; set; }
        public double RateTo { get; set; }
        public double From { get; set; }
        public double To { get; set; }
        public bool ErrorFound { get; set; }
        public override string ToString()
        {
            var trace = $"Id={Id} | RowType=[{RowType}] | BetType=[{BetType}] | RateType=[{RateType}] | BetForType=[{BetForType}] | RateFrom=[{RateFrom}] | RateTo=[{RateTo}] | From=[{From}] | To[{To}] | ErrorFound=[{ErrorFound}]";
            return trace;
        }
    }
}
