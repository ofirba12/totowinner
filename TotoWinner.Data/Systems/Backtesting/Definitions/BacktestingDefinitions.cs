﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class BacktestingDefinitions
    {
        public ProgramType ProgramType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public MinMaxLimits BankersLimits { get; set; }
        public MinMaxLimits DoublesLimits { get; set; }
        public List<GamesDefinition> GamesDefinitions { get; set; }
        public ShrinkDefinitions ShrinkDefinitions { get; set; }
        public override string ToString()
        {
            var trace = new StringBuilder();
            trace.AppendLine();
            trace.AppendLine($"ProgramType: {ProgramType} | FromDate: {FromDate.Date.ToLocalTime()} | ToDate: {ToDate.Date.ToLocalTime()} | BankersLimits: {BankersLimits.ToString()} | DoublesLimits: {DoublesLimits.ToString()} | GamesDefinitions#: {GamesDefinitions.Count}");
            trace.AppendLine("Games Definitions:");
            this.GamesDefinitions.ForEach(game => trace.AppendLine(game.ToString()));
            if (this.ShrinkDefinitions != null)
                trace.AppendLine(ShrinkDefinitions.ToString());
            return trace.ToString();
        }
    }

}
