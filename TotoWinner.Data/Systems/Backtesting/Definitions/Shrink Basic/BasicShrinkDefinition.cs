﻿using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class BasicShrinkDefinition
    {
        public List<BasicItemDefinition> Collection { get; set; }
        public override string ToString()
        {
            var trace = new StringBuilder();
            trace.AppendLine("BasicShrinkDefinition:");
            this.Collection.ForEach(item => trace.AppendLine(item.ToString()));
            return trace.ToString();
        }
    }
}
