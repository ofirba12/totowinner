﻿namespace TotoWinner.Data
{ 
    public class BasicItemDefinition : BaseIntRangeDefinition
    {
        public BtBasicShrinkFieldType Type { get; set; }
        public override string ToString()
        {
            var trace = $"Type={Type} | From=[{From}] | To=[{To}]";
            return trace;
        }

    }
}
