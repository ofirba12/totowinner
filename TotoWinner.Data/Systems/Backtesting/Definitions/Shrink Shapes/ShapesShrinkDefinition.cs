﻿using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class ShapesShrinkDefinition
    {
        public List<ShapesItemDefinition> Collection { get; set; }
        public override string ToString()
        {
            var trace = new StringBuilder();
            trace.AppendLine("ShapesShrinkDefinition:");
            this.Collection.ForEach(item => trace.AppendLine(item.ToString()));
            return trace.ToString();
        }
    }
}
