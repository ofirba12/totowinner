﻿namespace TotoWinner.Data
{
    public class ShapesItemDefinition : BaseIntRangeDefinition
    {
        public BtShapesShrinkFieldType Type { get; set; }
        public override string ToString()
        {
            var trace = $"Type={Type} | From=[{From}] | To=[{To}]";
            return trace;
        }
    }
}
