﻿namespace TotoWinner.Data
{
    public abstract class BaseIntRangeDefinition
    {
        public int From { get; set; }
        public int To { get; set; }
    }
}
