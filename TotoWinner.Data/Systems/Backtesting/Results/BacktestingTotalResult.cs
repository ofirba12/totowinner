﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BacktestingTotalResult
    {
        public int Bets { get; private set; }
        public List<int> Results { get; private set; }
        public int TotalProgramsWithBaseBet { get; private set; }
        public BacktestingTotalResult(int bets, List<int> results, int totalProgramsWithBaseBet)
        {
            try
            {
                this.Bets = bets;
                this.Results = results;
                this.TotalProgramsWithBaseBet = totalProgramsWithBaseBet;
            }
            catch (Exception ex)
            {
                var resultsStr = results != null ? string.Join(",", results) : "N/A";
                throw new System.Exception(
                    $"An error occured trying to construct type {typeof(BacktestingTotalResult)}; bets={bets} | results={resultsStr}",
                    ex);
            }
        }
    }
}
