﻿using System;

namespace TotoWinner.Data
{
    public class BacktestingSummaryResult
    {
        public int ProgramsCounter { get; private set; }
        public int NotValidProgramCounter { get; private set; }
        public int NotExistsProgramCounter { get; private set; }

        public BacktestingSummaryResult(int programsCounter, int notValidProgramCounter, int notExistsProgramCounter)
        {
            try
            {
                this.ProgramsCounter = programsCounter;
                this.NotValidProgramCounter = notValidProgramCounter;
                this.NotExistsProgramCounter = notExistsProgramCounter;
            }
            catch (Exception ex)
            {
                throw new System.Exception(
                    $"An error occured trying to construct type {typeof(BacktestingSummaryResult)}; programsCounter={programsCounter} | notValidProgramCounter={notValidProgramCounter} | notExistsProgramCounter={notExistsProgramCounter}",
                    ex);
            }
        }
    }
}

