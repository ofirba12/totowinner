﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BacktestingFinalResult
    {
        public string Template { get; }
        public Dictionary<int, List<string>> WinningPrograms { get; }
        public BacktestingSummaryResult Summary { get; }
        public int NumOfAllBets { get; }
        public int TotalProgramsWithBaseBet { get; }
        public BacktestingFinalResult(string template, Dictionary<int, List<string>> winningPrograms, BacktestingSummaryResult summary, int numOfAllBets, int totalProgramsWithBaseBet)
        {
            try
            {
                if (winningPrograms.Keys.Count == 0)
                    throw new Exception("the specified winningPrograms collection has no programs");
                this.Template = template;
                this.WinningPrograms = winningPrograms;
                this.Summary = summary;
                this.NumOfAllBets = numOfAllBets;
                this.TotalProgramsWithBaseBet = totalProgramsWithBaseBet;
            }
            catch (Exception ex)
            {
                throw new System.Exception(
                    $"An error occured trying to construct type {typeof(BacktestingFinalResult)}; template={template} | winningPrograms of size={winningPrograms.Keys.Count}",
                    ex);
            }
        }
    }
}
