﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BacktestingResults
    {
        public List<BacktestingProgramResult> Programs { get; private set; }
        public BacktestingTotalResult Totals { get; private set; }
        public BacktestingSummaryResult Summary { get; private set; }

        public BacktestingResults(List<BacktestingProgramResult> programs,
            BacktestingTotalResult totals,
            BacktestingSummaryResult summary)
        {
            try
            {
                if (programs == null)
                    throw new Exception("The specified programs result can not be null");
                if (totals == null)
                    throw new Exception("The specified totals result can not be null");
                if (summary == null)
                    throw new Exception("The specified summary result can not be null");
                this.Programs = programs;
                this.Totals = totals;
                this.Summary = summary;
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occured trying to construct type {typeof(BacktestingResults)};", ex);
            }
        }
    }
}
