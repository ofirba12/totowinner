﻿namespace TotoWinner.Data
{
    public class BacktestingResultKey
    {
        public int ExecutionId { get; }
        public int ProgramId { get; }

        public BacktestingResultKey(int executionId, int programId)
        {
            this.ExecutionId = executionId;
            this.ProgramId = programId;
        }
        public override bool Equals(object obj)
        {
            var candidate = obj as BacktestingResultKey;
            return candidate != null
                ? this.ExecutionId == candidate.ExecutionId && this.ProgramId == candidate.ProgramId
                : false;
        }
        public override int GetHashCode()
        {
            return this.ExecutionId * 1000000 + this.ProgramId;
        }

    }
}
