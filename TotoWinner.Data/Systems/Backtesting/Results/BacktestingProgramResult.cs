﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BacktestingProgramResult : BacktestingTotalResult
    {
        public int Triplets { get; private set; }
        public int Doubles { get; private set; }
        public int Bankers { get; private set; }
        public string ProgramDescription { get; private set; }
        public DateTime ProgramEndDate { get; private set; }
        public int ProgramNumber { get; private set; }
        public int ProgramPersistanceId { get; private set; }

        public BacktestingProgramResult(int programNumber, int programPersistanceId, 
            string programDescription, DateTime programEndDate, int triplets, int doubles, int bankers, int bets, List<int> results, int totalProgramsWithBaseBets) : base(bets, results, totalProgramsWithBaseBets)
        {
            try
            {
                if (!string.IsNullOrEmpty(programDescription) &&
                    programDescription != "אין טור" &&
                    programDescription != "ת.ל.ת" &&
                    programDescription != "גדול מ-15 מליון")
                {
                    throw new Exception($"The specified program value is invalid: {programDescription}");
                }
                this.ProgramPersistanceId = programPersistanceId;
                this.ProgramNumber = programNumber;
                this.ProgramDescription = string.IsNullOrEmpty(programDescription)
                    ? programNumber.ToString()
                    : programDescription; 
                this.Triplets = triplets;
                this.Doubles = doubles;
                this.Bankers = bankers;
                this.ProgramEndDate = programEndDate;
            }
            catch (Exception ex)
            {
                var resultsStr = string.Join(",", results);
                throw new System.Exception(
                    $"An error occured trying to construct type {typeof(BacktestingProgramResult)}; programNumber={programNumber} | programPersistanceId={programPersistanceId} | programDescription={programDescription} | triplets={triplets} | doubles={doubles} | bankers={bankers} | bets={bets} | results={resultsStr}",
                    ex);
            }
        }
    }
}
