﻿using System;

namespace TotoWinner.Data
{
    public class BacktestingFinalResultKey : IComparable
    {
        public int FirstWinCounter { get; }
        public int SecondWinCounter { get; }
        public int ThirdWinCounter { get; }
        public int ExecutionId { get; }

        public BacktestingFinalResultKey(int firstWinCounter, int secondWinCounter, int thirdWinCounter, int executionId)
        {
            try
            {
                if (firstWinCounter == 0 && secondWinCounter == 0 && thirdWinCounter == 0)
                    throw new System.Exception("The specified winning counters can not be zero");
                this.FirstWinCounter = firstWinCounter;
                this.SecondWinCounter = secondWinCounter;
                this.ThirdWinCounter = thirdWinCounter;
                this.ExecutionId = executionId;
            }
            catch (Exception ex)
            {
                throw new System.Exception(
                    $"An error occured trying to construct type {typeof(BacktestingFinalResultKey)}; firstWinCounter={firstWinCounter} | secondWinCounter={secondWinCounter} | thirdWinCounter={thirdWinCounter}",
                    ex);
            }
        }
        public override bool Equals(object obj)
        {
            var candidate = obj as BacktestingFinalResultKey;
            return candidate != null
                ? this.ExecutionId == candidate.ExecutionId && this.FirstWinCounter == candidate.FirstWinCounter && this.SecondWinCounter == candidate.SecondWinCounter && this.ThirdWinCounter == candidate.ThirdWinCounter
                : false;
        }
        public override int GetHashCode()
        {
            return /*this.ExecutionId * 1000000 + */this.FirstWinCounter*10000+ this.SecondWinCounter*100+this.ThirdWinCounter;
        }

        public int CompareTo(object obj)
        {
            if (this.GetHashCode() == obj.GetHashCode())
                return 0;
             if (this.GetHashCode() > obj.GetHashCode())
                return -1; //becaouse of desc mode
            return 1;
        }
    }
}
