﻿namespace TotoWinner.Data
{
    public class StopBacktestingResponse : ResponseBase
    {
        public ExecutionStatus Status { get; private set; }

        public StopBacktestingResponse() : base(true)
        {
            this.Status = ExecutionStatus.Stopped;
        }
        public StopBacktestingResponse(string error) : base(false, error) { }
    }
}
