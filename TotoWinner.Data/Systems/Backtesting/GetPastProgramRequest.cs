﻿namespace TotoWinner.Data
{
    public class GetPastProgramRequest
    {
        public int ExecutionId { get; set; }
        public int ProgramId { get; set; }
    }
}
