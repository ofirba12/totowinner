﻿using System;

namespace TotoWinner.Data
{
    public class RunBacktestingResponse : ResponseBase
    {
        public int ExecutionId { get; private set; }
        public ExecutionStatus Status { get; private set; }


        public RunBacktestingResponse(int executionId, ExecutionStatus status) : base(true)
        {
            try
            {
                if (executionId == 0)
                    throw new Exception("the specified executionId is zero");
                this.ExecutionId = executionId;
                this.Status = status;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(RunBacktestingResponse)};", ex);
            }
        }
        public RunBacktestingResponse(string error) : base(false, error) { }
    }
}
