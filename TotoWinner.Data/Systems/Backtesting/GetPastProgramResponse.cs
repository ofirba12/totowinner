﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GetPastProgramResponse : ResponseBase
    {
        public int ProgramId { get; set; }
        public int ProgramNumber { get; set; }
        public List<ProgramBetData> BaseBetData { get; private set; }
        public List<ProgramBetData> WinBetData { get; private set; }

        public GetPastProgramResponse(int programId, int programNumber, List<ProgramBetData> baseBetData,
            List<ProgramBetData> winBetData) : base(true)
        {
            try
            {
                if (baseBetData == null)
                    throw new Exception($"The specified baseBetData collection is null");
                if (winBetData == null)
                    throw new Exception($"The specified winBetData collection is null");
                if (baseBetData.Count > 16)
                    throw new Exception($"The specified baseBetData collection has too many records {baseBetData.Count}");
                if (winBetData.Count > 16)
                    throw new Exception($"The specified winBetData collection has too many records {winBetData.Count}");
                this.WinBetData = winBetData;
                this.BaseBetData = baseBetData;
                this.ProgramId = programId;
                this.ProgramNumber = programNumber;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(GetPastProgramResponse)} for programid {programId}", ex);
            }
        }
        public GetPastProgramResponse(string error) : base(false, error) { }
    }
}
