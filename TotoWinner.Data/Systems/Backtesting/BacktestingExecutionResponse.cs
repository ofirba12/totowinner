﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BacktestingExecutionResponse : ResponseBase
    {
        public int ExecutionId { get; set; }
        public ExecutionStatus Status { get; private set; }
        public List<BacktestingProgramResult> Results { get; private set; }
        public BacktestingTotalResult Totals { get; private set; }
        public BacktestingSummaryResult Summary { get; private set; }

        public BacktestingExecutionResponse(int executionId, ExecutionStatus status, 
            List<BacktestingProgramResult> results, BacktestingTotalResult totals,
            BacktestingSummaryResult summary) : base(true)
        {
            try
            {
                this.ExecutionId = executionId;
                this.Status = status;
                this.Results = results;
                this.Totals = totals;
                this.Summary = summary;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BacktestingExecutionResponse)};", ex);
            }
        }
        public BacktestingExecutionResponse(string error) : base(false, error) { }
    }
}
