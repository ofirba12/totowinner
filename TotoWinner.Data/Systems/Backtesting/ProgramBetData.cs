﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ProgramBetData
    {
        public List<float> Rates { get; private set; }
        public List<bool> Selection { get; private set; }

        public ProgramBetData(List<float> rates, List<bool> selection)
        {
            try
            {
                if (rates.Count != 3)
                    throw new Exception($"The specified rates collection must be of size 3, currently {rates.Count}");
                if (selection.Count != 3)
                    throw new Exception($"The specified selection collection must be of size 3, currently {rates.Count}");
                this.Rates = rates;
                this.Selection = selection;
            }
            catch (Exception ex)
            {
                var ratesStr = rates != null ? string.Join(",", rates) : "N/A";
                var selectionStr = selection != null ? string.Join(",", selection) : "N/A";
                throw new System.Exception(
                    $"An error occured trying to construct type {typeof(ProgramBetData)}; rates={ratesStr} | selection={selectionStr}",
                    ex);
            }
        }
    }
}
