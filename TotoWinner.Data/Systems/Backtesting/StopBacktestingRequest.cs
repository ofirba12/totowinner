﻿using System;

namespace TotoWinner.Data
{
    public class StopBacktestingRequest
    {
        public int ExecutionId { get; private set; }

        public StopBacktestingRequest(int executionId)
        {
            try
            {
                this.ExecutionId = executionId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(StopBacktestingRequest)};", ex);
            }
        }

    }
}
