﻿namespace TotoWinner.Data
{
    public class BaseBet
    {
        public int ExecutionId { get; set; }
        public int ProgramId { get; set; }
        public short GameIndex { get; set; }
        public bool Win_1 { get; set; }
        public bool Win_X { get; set; }
        public bool Win_2 { get; set; }
    }
}
