﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BtBaseGame
    {
        public int Index { get; private set; }
        public double Rate1 { get; private set; }
        public double RateX { get; private set; }
        public double Rate2 { get; private set; }
        public HashSet<char> Won { get; private set; }

        public BtBaseGame(int index, double rate1, double ratex, double rate2)
        {
            try
            {
                this.Index = index;
                this.Rate1 = rate1;
                this.RateX = ratex;
                this.Rate2 = rate2;
                this.Won = new HashSet<char>();
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BtBaseGame)} with index={index}, rate1={rate1}, rateX={ratex}, rate2={rate2}", ex);
            }
        }
        public void MarkBanker(char win)
        {
            try
            {
                if (!this.Won.Add(win))
                    throw new Exception($"The specified win [{win}], already present for game index {this.Index}");
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occurred trying to mark banker[{win}]", ex);
            }
        }
        public void MarkDouble(char win1, char win2)
        {
            try
            {
                if (!this.Won.Add(win1))
                    throw new Exception($"The specified win [{win1}], already present for game index {this.Index}");
                if (!this.Won.Add(win2))
                    throw new Exception($"The specified win [{win2}], already present for game index {this.Index}");
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to mark double [{win1},{win2}]", ex);
            }
        }
        public void MarkTriple()
        {
            try
            {
                this.Won.Add('1');
                this.Won.Add('X');
                this.Won.Add('2');
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to mark triple", ex);
            }
        }
    }
}
