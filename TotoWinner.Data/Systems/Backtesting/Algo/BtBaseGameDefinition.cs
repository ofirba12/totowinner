﻿using System;

namespace TotoWinner.Data
{
    public class BtBaseGameDefinition 
    {
        public BtRateType RateType { get; private set; }
        public BtWinType BetForType { get; private set; }
        public double FromRate { get; private set; }
        public double ToRate { get; private set; }
        public MinMax MinMax { get; private set; }

        public BtBaseGameDefinition(BtRateType rateType, BtWinType betForType, double fromRate, double toRate, MinMax minMax)
        {
            try
            {
                if (fromRate < 0)
                    throw new Exception($"The specified from {fromRate} must be >=0");
                if (toRate < 0)
                    throw new Exception($"The specified from {toRate} must be >=0");
                this.RateType = rateType;
                this.BetForType = betForType;
                this.FromRate = fromRate;
                this.ToRate = toRate;
                this.MinMax = minMax;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BtBaseGameDefinition)} with rateType={rateType}, betForType={betForType}, from={fromRate}, to={toRate}", ex);
            }

        }

    }
}

