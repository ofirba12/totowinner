﻿using System;

namespace TotoWinner.Data
{
    public class BtBankerGameDefinition : BtBaseGameDefinition
    {
        public BtBankerBetType BetType { get; private set; }

        public BtBankerGameDefinition(BtBankerBetType betType, BtRateType rateType, BtWinType betForType, double fromRate, double toRate, MinMax minMax) :
            base(rateType, betForType, fromRate, toRate, minMax)
        {
            try
            {
                this.BetType = betType;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BtBankerGameDefinition)} with betType={betType}", ex);
            }
        }
    }
}

