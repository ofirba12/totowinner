﻿using System;

namespace TotoWinner.Data
{
    public class BtDoubleGameDefinition : BtBaseGameDefinition
    {
        public BtDoubleBetType BetType { get; private set; }

        public BtDoubleGameDefinition(BtDoubleBetType betType, BtRateType rateType, BtWinType betForType, double fromRate, double toRate, MinMax minMax) :
            base(rateType, betForType, fromRate, toRate, minMax)
        {
            try
            {
                this.BetType = betType;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BtDoubleGameDefinition)} with betType={betType}", ex);
            }
        }
    }
}

