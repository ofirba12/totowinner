﻿namespace TotoWinner.Data
{
    public class BacktestingStrikeResult
    {
        public int ExecutionId { get; set; }
        public int ProgramId { get; set; }
        public short StrikeBet { get; set; }
        public int Counter { get; set; }
    }
}
