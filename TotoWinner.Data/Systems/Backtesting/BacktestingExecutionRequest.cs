﻿namespace TotoWinner.Data
{
    public class BacktestingExecutionRequest
    {
        public int ExecutionId { get; set; }
        public int FromIndex { get; set; }
    }
}
