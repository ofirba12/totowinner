﻿namespace TotoWinner.Data
{
    public class RunBacktestingRequest
    {
        public BacktestingDefinitions Definitions { get; set; }

        public override string ToString()
        {
            return $"Definitions: [{Definitions}]";
        }

    }
}
