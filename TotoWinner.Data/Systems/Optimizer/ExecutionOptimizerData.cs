﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ExecutionOptimizerData
    {
        public int ParentBetsCounter { get; private set; }
        public int ShrinkedBetsCounter { get; private set; }
        
        public List<OptimizedBet> Bets { get; private set; }
        public List<OptimizedCondition> Conditions { get; private set; }
        public List<OptimizedAdvancedCondition> AdvancedConditions { get; private set; }

        public ExecutionOptimizerData(List<OptimizedBet> optimizedBets, 
            List<OptimizedCondition> optimizedConditions,
            List<OptimizedAdvancedCondition> advancedConditions,
            int parentBetsCounter,
            int shrinkedBetsCounter)
        {
            try
            {
                this.Bets = optimizedBets;
                this.Conditions = optimizedConditions;
                this.AdvancedConditions = advancedConditions;
                this.ParentBetsCounter = parentBetsCounter;
                this.ShrinkedBetsCounter = shrinkedBetsCounter;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ExecutionOptimizerData)};", ex);
            }
        }
    }
}
