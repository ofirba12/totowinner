﻿using System;

namespace TotoWinner.Data
{
    public class OptimizedBet
    {
        public int GameIndex { get; private set; }
        public string Bet { get; private set; }
        public bool IsFinalBet { get; private set; }

        public OptimizedBet(int gameIndex, string optimizedBet, bool isFinalBet)
        {
            try
            {
                this.GameIndex = gameIndex;
                this.Bet = optimizedBet;
                this.IsFinalBet = isFinalBet;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(OptimizedBet)};", ex);
            }
        }
        public override string ToString()
        {
            return $"GameIndex: {GameIndex} | Bet: {Bet} | IsFinalBet: {IsFinalBet}";
        }
    }
}
