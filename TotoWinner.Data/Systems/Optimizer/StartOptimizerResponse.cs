﻿using System;

namespace TotoWinner.Data.Optimizer
{
    public class StartOptimizerResponse : ResponseBase
    {
        public int ExecutionId { get; private set; }

        public StartOptimizerResponse(int executionId) : base(true)
        {
            try
            {
                if (executionId == 0)
                    throw new Exception("the specified executionId is zero");
                this.ExecutionId = executionId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(StartOptimizerResponse)};", ex);
            }
        }
        public StartOptimizerResponse(string error) : base(false, error) { }
    }
}
