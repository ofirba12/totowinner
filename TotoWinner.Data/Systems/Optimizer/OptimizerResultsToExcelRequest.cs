﻿using System;

namespace TotoWinner.Data
{
    public class OptimizerResultsToExcelRequest
    {
        public int ExecutionId { get; private set; }
        public int OptimizerSystemId { get; private set; }

        public OptimizerResultsToExcelRequest(int executionId, int optimizerSystemId)
        {
            try
            {
                this.ExecutionId = executionId;
                this.OptimizerSystemId = optimizerSystemId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(OptimizerResultsToExcelRequest)}", ex);
            }
        }
    }
}