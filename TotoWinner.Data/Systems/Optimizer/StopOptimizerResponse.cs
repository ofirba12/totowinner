﻿using System;

namespace TotoWinner.Data
{
    public class StopOptimizerResponse : ResponseBase
    {
        public StopOptimizerResponse() : base(true)
        {
        }
        public StopOptimizerResponse(string error) : base(false, error) { }
    }
}
