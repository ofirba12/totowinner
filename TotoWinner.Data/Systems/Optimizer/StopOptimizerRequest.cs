﻿using System;

namespace TotoWinner.Data
{
    public class StopOptimizerRequest
    {
        public int ExecutionId { get; private set; }

        public StopOptimizerRequest(int executionId)
        {
            try
            {
                this.ExecutionId = executionId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(StopOptimizerRequest)};", ex);
            }
        }

    }
}
