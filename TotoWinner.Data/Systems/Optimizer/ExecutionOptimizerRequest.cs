﻿using System;

namespace TotoWinner.Data.Optimizer
{
    public class ExecutionOptimizerRequest
    {
        public int ExecutionId { get; private set; }

        public ExecutionOptimizerRequest(int executionId)
        {
            try
            {
                this.ExecutionId = executionId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ExecutionOptimizerRequest)};", ex);
            }
        }

    }
}
