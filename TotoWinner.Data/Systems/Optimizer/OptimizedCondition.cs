﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TotoWinner.Data
{
    public class OptimizedCondition
    {
        public string Title { get; private set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ConditionType Type { get; private set; }
        public MinMax Condition { get; private set; }

        public OptimizedCondition(string title, ConditionType type, MinMax condition)
        {
            try
            {
                Regex strPattern = null;
                if ((type == ConditionType.Breaks1X2 || 
                    type == ConditionType.BreaksABC || 
                    type == ConditionType.Range) && !string.IsNullOrEmpty(title))
                    throw new Exception($"Optimized condition of {type} should not have a title");
                if (type == ConditionType.AmountABC || type == ConditionType.SequenceABC)
                    strPattern = new Regex(@"\b(A|B|C)\b");
                else if (type == ConditionType.Amount1X2 || type == ConditionType.Sequence1X2)
                    strPattern = new Regex(@"\b(1|X|2)\b");
                if (strPattern != null && !strPattern.IsMatch(title))
                    throw new Exception($"Optimized condition title {title} does not match its type {type}");
                this.Title = title; 
                this.Type = type;
                this.Condition = condition;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(OptimizedCondition)};", ex);
            }
        }
        public OptimizedCondition(ConditionBetTitleEnum title, ConditionType type, MinMax condition)
        {
            this.Condition = condition;
            this.Type = type;
            switch(title)
            {
                case ConditionBetTitleEnum.Title_1:
                    this.Title = "1";
                    break;
                case ConditionBetTitleEnum.Title_X:
                    this.Title = "X";
                    break;
                case ConditionBetTitleEnum.Title_2:
                    this.Title = "2";
                    break;
                case ConditionBetTitleEnum.Title_A:
                    this.Title = "A";
                    break;
                case ConditionBetTitleEnum.Title_B:
                    this.Title = "B";
                    break;
                case ConditionBetTitleEnum.Title_C:
                    this.Title = "C";
                    break;
                case ConditionBetTitleEnum.None:
                    this.Title = "";
                    break;
            }
        }
        public override string ToString()
        {
            return $"Type: {Type} | Title: {Title} | Condition: {Condition.ToString()}";
        }

    }
}