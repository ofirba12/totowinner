﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.Optimizer
{
    public class OptimizedGame
    {
        public int gameIndex { get; set; }
        public string betStatus { get; set; }
        public StatusStyle statusStyle { get; set; }
        public string gameTime { get; set; }
        public string leagueName { get; set; }
        public string gameName { get; set; }
        public List<Bet> bets { get; set; }
        public List<BetsProgramDetail> betsProgramDetail { get; set; }
        public string gameResult { get; set; }
        public string bet1X2String { get; set; }
        public List<bool> betsSelection { get; set; }
        public Validations validations { get; set; }
        public List<int> selectedRates { get; set; }
}
    public class StatusStyle
    {
        public string color { get; set; }
    }

    public class Bet
    {
        public string label { get; set; }
        public int value { get; set; }
        public bool disabled { get; set; }
        public string styleClass { get; set; }
    }

    public class BetsProgramDetail
    {
        public double rate { get; set; }
        public bool won { get; set; }
        public string rateLabel { get; set; }
    }

    public class Validations
    {
        public string validForCalculation { get; set; }
    }

}
