﻿using System;

namespace TotoWinner.Data
{
    public class StartOptimizerRequest
    {
        public ExecutionOptimizerParameters Parameters { get; private set; }

        public StartOptimizerRequest(ExecutionOptimizerParameters parameters)
        {
            try
            {
                this.Parameters = parameters;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(StartOptimizerRequest)};", ex);
            }
        }

    }
}
