﻿namespace TotoWinner.Data
{
    public class SaveOptimizerSystemRequest : ISaveSystemRequest
    {
        public string UniqueId { get; set; }
        public SystemType SystemType { get; set; }
        public string Name { get; set; }
        public int OptimizedSystemId { get; set; }
        public string OptimizerData { get; set; }
        public OptimizerExecutionState Execution { get; set; }
    }
}
