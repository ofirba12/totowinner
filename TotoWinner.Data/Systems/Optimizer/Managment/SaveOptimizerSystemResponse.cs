﻿using System;

namespace TotoWinner.Data
{
    public class SaveOptimizerSystemResponse
    {
        public int SystemId { get; set; }
        public SaveOptimizerSystemResponse(int systemId)
        {
            this.SystemId = systemId;
        }
    }
}
