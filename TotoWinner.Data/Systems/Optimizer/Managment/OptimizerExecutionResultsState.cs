﻿namespace TotoWinner.Data
{
    public class OptimizerExecutionResultsState
    {
        public int parentBetsCounter { get; set; }
        public int shrinkedBetsCounter { get; set; }
    }
}
