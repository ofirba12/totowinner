﻿namespace TotoWinner.Data
{
    public class LoadOptimizerSystemResponse
    {
        public SaveOptimizerSystemRequest SystemData { get; set; }
        public string OptimizedSystemName { get; set; }
        public LoadOptimizerSystemResponse(SaveOptimizerSystemRequest systemData, string shrinkSystemName)
        {
            this.SystemData = systemData;
            this.OptimizedSystemName = shrinkSystemName;
        }
    }
}
