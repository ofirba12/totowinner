﻿namespace TotoWinner.Data
{
    public class StartCalcOptimizerParam
    {
        public int TargetBetsCounter { get; set; }
        public int TargetConditionCounter { get; set; }
    }
}
