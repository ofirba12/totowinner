﻿namespace TotoWinner.Data
{
    public class OptimizerExecutionState 
    {
        public StartCalcOptimizerParam Parameters { get; set; }
        public int ExecutionId { get; set; }
        public ExecutionStatus Status { get; set; }
        public OptimizerExecutionResultsState Results { get; set; }
    }
}
