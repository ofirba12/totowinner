﻿using System;

namespace TotoWinner.Data
{
    public class LoadOptimizerSystemRequest
    {
        public int SystemId { get; set; }
    }
}
