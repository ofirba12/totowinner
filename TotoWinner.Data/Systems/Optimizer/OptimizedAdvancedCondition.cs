﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TotoWinner.Data
{
    public class OptimizedAdvancedCondition
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public AdvancedConditionType Type { get; private set; }
        public int IndexPerType { get; private set; }
        public MinMax Condition { get; private set; }

        public OptimizedAdvancedCondition(AdvancedConditionType type, int indexPerType, MinMax condition)
        {
            try
            {
                this.Type = type;
                this.IndexPerType = indexPerType;
                this.Condition = condition;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(OptimizedAdvancedCondition)};", ex);
            }
        }
        public override string ToString()
        {
            return $"Type: {Type} | IndexPerType: {IndexPerType} | Condition: {Condition.ToString()}";
        }

    }
}