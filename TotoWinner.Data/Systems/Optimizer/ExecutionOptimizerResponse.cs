﻿using System;

namespace TotoWinner.Data.Optimizer
{
    public class ExecutionOptimizerResponse : ResponseBase
    {
        public ExecutionState State { get; private set; }

        public ExecutionOptimizerResponse(ExecutionState state) : base(true)
        {
            try
            {
                if (state == null)
                    throw new Exception("the specified execution state is null");
                this.State = state;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ExecutionOptimizerResponse)};", ex);
            }
        }
        public ExecutionOptimizerResponse(string error) : base(false, error) { }

    }
}
