﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GetAllSystemsResponse
    {
        public List<SystemParameters> Systems { get; set; }
        public GetAllSystemsResponse(List<SystemParameters> systems)
        {
            this.Systems = systems;
        }
    }
}
