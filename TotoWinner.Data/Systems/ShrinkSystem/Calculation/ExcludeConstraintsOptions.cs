﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ExcludeConstraintsOptions
    {
        public List<string[]> MinMaxSettings { get; set; }
        public List<string> SequenciesLengths { get; set; }
        public string[] BreakMinMax { get; set; }
        public bool isExOneActive { get; set; }
        public bool isExXActive { get; set; }
        public bool isExTwoActive { get; set; }
        public bool isExOneSActive { get; set; }
        public bool isExXSActive { get; set; }
        public bool isExTwoSActive { get; set; }
        public bool isExBreakActive { get; set; }
        public bool isExSumActive { get; set; }

        public ExcludeConstraintsOptions()
        {
            MinMaxSettings = new List<string[]>();
            SequenciesLengths = new List<string>();
            BreakMinMax = new string[2];
        }
    }
}
