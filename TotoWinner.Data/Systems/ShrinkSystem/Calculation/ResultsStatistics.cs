﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ResultsStatistics
    {
        public int Found { get; set; }
        public Dictionary<byte, int> TestsCounters { get; set; }
    }
}
