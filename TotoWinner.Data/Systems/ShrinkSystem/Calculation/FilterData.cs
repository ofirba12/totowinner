﻿namespace TotoWinner.Data
{
    public class FilterData
    {
        public string FilterMin { get; set; }
        public string FilterMax { get; set; }
        public string[] FilterDataRow { get; set; }
        public ShapeFilterType ShapeFilterType { get; set; }
        public bool IsActive { get; set; }

        public FilterData()
        {
            ShapeFilterType = ShapeFilterType.NotShapesFilter;
        }
    }
}
