﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public interface IValidator
    {
        bool CheckResult(Dictionary<byte, byte> result);
    }
}
