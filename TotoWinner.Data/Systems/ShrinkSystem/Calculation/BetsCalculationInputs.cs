﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BetsCalculationInputs
    {
        public Dictionary<byte, decimal[]> ProbSettings { get; private set; }
        public ConstraintsSettings ConstrSettings { get; private set; }
        public List<IValidator> Validators { get; private set; }
        public bool CleanOneDiff { get; private set; }

        public BetsCalculationInputs(Dictionary<byte, decimal[]> probSettings, 
            ConstraintsSettings constrSettings,
            List<IValidator> validators,
            bool cleanOneDiff)
        {
            try
            {
                if (probSettings == null)
                    throw new Exception("The specified probSettings is null");
                if (constrSettings == null)
                    throw new Exception("The specified constrSettings is null");
                this.ProbSettings = probSettings;
                this.ConstrSettings = constrSettings;
                this.Validators = validators;
                this.CleanOneDiff = cleanOneDiff;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BetsCalculationInputs)}", ex);
            }
        }
    }
}
