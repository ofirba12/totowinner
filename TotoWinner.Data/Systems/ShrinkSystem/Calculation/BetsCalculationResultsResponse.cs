﻿using System;

namespace TotoWinner.Data
{
    public class BetsCalculationResultsResponse
    {
        public SearchResults seResults { get; private set; }
        public string OutputFile { get; private set; }

        public BetsCalculationResultsResponse(SearchResults seResults, string outputFile)
        {
            try
            {
                if (seResults == null)
                    throw new Exception("The specified seResults is null");
                this.seResults = seResults;
                this.OutputFile = outputFile;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BetsCalculationResultsResponse)}", ex);
            }
        }
    }
}