﻿using System;

namespace TotoWinner.Data
{
    public class ResultsToExcelRequest
    {
        public SearchResults SeResults { get; private set; }
        public bool CleanOneDiff { get; private set; }

        public ResultsToExcelRequest(SearchResults seResults, bool cleanOneDiff)
        {
            try
            {
                this.SeResults = seResults ?? throw new Exception("The specified seResults is null");
                this.CleanOneDiff = cleanOneDiff;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ResultsToExcelRequest)}", ex);
            }
        }
    }
}