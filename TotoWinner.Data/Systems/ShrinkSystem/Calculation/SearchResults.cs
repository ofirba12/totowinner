﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class SearchResults
    {
        public IEnumerable<CombinedResult> FinalResults { get; set; }
        public ResultsStatistics ResultsStatistics { get;  set; }
    }
}
