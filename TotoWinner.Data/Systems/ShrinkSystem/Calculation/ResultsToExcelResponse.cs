﻿using System;

namespace TotoWinner.Data
{
    public class ResultsToExcelResponse
    {
        public bool IsSuccess { get; private set; }
        public string Message { get; private set; }
        public string OutputFile { get; private set; }
        public string PlayerFile { get; private set; }

        public ResultsToExcelResponse(string outputExcelFile, string outputPlayerFile, bool isSuccess, string errorMessage)
        {
            try
            {
                if (isSuccess == true && string.IsNullOrEmpty(outputExcelFile))
                    throw new Exception($"outputExcelFile file empty, operation failed");
                if (isSuccess == true && string.IsNullOrEmpty(outputPlayerFile))
                    throw new Exception($"outputPlayerFile file empty, operation failed");
                this.IsSuccess = isSuccess;
                this.Message = errorMessage;
                this.OutputFile = outputExcelFile;
                this.PlayerFile = outputPlayerFile;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ResultsToExcelResponse)}", ex);
            }
        }
    }
}