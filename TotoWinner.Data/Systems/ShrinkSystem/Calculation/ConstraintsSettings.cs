﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ConstraintsSettings
    {
        public decimal MinSum { get; set; }
        public decimal MaxSum { get; set; }
        public Dictionary<int, bool[]> Options;
        public Dictionary<byte, byte[]> MinMax;
        public Dictionary<byte, byte[]> SeqLengths;
        public byte BreakMin { get; set; }
        public byte BreakMax { get; set; }


        public ConstraintsSettings()
        {
            SeqLengths = new Dictionary<byte, byte[]>();
            for (byte seqLenId = 0; seqLenId < 3; seqLenId++) SeqLengths.Add(seqLenId, new byte[] { 0, 16 });

            MinMax = new Dictionary<byte, byte[]>();
            for (byte minMaxId = 0; minMaxId < 3; minMaxId++) MinMax.Add(minMaxId, new byte[] { 0, 16 });

            Options = new Dictionary<int, bool[]>();
        }
    }
}
