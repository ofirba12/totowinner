﻿using System.Collections.Generic;
using System.Text;

namespace TotoWinner.Data
{
    public class CombinedResult
    {
        public int NumOfPointers { get { return _pointersToResults.Count; } }
        public byte[] Result { get { return _resultColumn; } }
        public bool IsInMap { get; set; }
        public bool IsSkipped { get { return _isSkipped; } }
        public bool IsIncludedInResults { get { return _isIncludedInResults; } }

        public bool _isSkipped;
        public bool _isIncludedInResults;
        public byte[] _resultColumn;

        public List<CombinedResult> _pointersToResults;

        public int TestResult { get; private set; }

        public CombinedResult()//FOR SERIALIZATION ONLY
        { }

        public CombinedResult(Dictionary<byte, byte> result, int testResult)
        {
            _pointersToResults = new List<CombinedResult>();

            _resultColumn = new byte[result.Count];
            foreach (byte key in result.Keys)
            {
                _resultColumn[key] = result[key];
            }

            TestResult = testResult;

        }

        //public void InsertIntoMap(CombinedResult refResult, byte maxNumOfDiffs)
        //{
        //    if (this == refResult) return;

        //    byte numOfDiffs = 0;
        //    for (byte key = 0; key < _resultColumn.Length; key++)
        //    {
        //        if (_resultColumn[key] != refResult._resultColumn[key]) numOfDiffs++;

        //        if (numOfDiffs > maxNumOfDiffs) return;
        //    }

        //    _pointersToResults.Add(refResult);

        //    refResult._pointersToResults.Add(this);

        //}




        //internal void IncludeInResults()
        //{
        //    _isIncludedInResults = true;

        //    foreach(var pointedResult in _pointersToResults)
        //    {
        //        foreach(var x in pointedResult._pointersToResults)
        //        {
        //            if (x == this) continue;
        //            x._pointersToResults.Remove(pointedResult);
        //        }

        //        pointedResult._isSkipped = true;
        //        pointedResult._pointersToResults.Clear();
        //    }

        //    _pointersToResults.Clear();
        //}
    }
}
