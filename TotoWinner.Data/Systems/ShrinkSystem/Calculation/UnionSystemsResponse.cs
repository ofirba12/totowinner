﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class UnionSystemsResponse : ResponseBase
    {
        public int UnionResultId { get; }
        public int CounterBeforeUnion { get; }
        public int CounterAfterUnion { get; }
        public IReadOnlyDictionary<int, Container1X2> CounterCoverage { get; }
        public IReadOnlyDictionary<int, Container1X2> CounterPercentCoverage { get; }


        public UnionSystemsResponse(int UnionResultId, 
            int counterBeforeUnion,
            int counterAfterUnion,
            IReadOnlyDictionary<int, Container1X2> counterCoverage,
            IReadOnlyDictionary<int, Container1X2> counterPercentCoverage,
            bool isSuccess = true, string errorMessage = null):
            base(isSuccess, errorMessage)
        {
            try
            {
                if (!isSuccess && string.IsNullOrEmpty(errorMessage))
                    throw new Exception("The specified error message is empty while isSuccess is false");
                this.UnionResultId = UnionResultId;
                this.CounterBeforeUnion = counterBeforeUnion;
                this.CounterAfterUnion = counterAfterUnion;
                if (counterAfterUnion > 0 && counterCoverage.Count == 0)
                    throw new Exception($"The specified counter coverage collection is empty while there are {counterAfterUnion} results");
                this.CounterCoverage = counterCoverage;
                this.CounterPercentCoverage = counterPercentCoverage;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(UnionSystemsResponse)}", ex);
            }
        }
        public UnionSystemsResponse(string errorMessage) :
            base(false, errorMessage)
        { }
    }
}