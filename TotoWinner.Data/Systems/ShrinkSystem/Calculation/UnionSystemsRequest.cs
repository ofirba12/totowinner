﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class UnionSystemsRequest
    {
        public List<int> SystemIds { get; private set; }

        public UnionSystemsRequest(List<int> systemIds)
        {
            try
            {
                this.SystemIds = systemIds;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(UnionSystemsRequest)}", ex);
            }
        }
    }
}