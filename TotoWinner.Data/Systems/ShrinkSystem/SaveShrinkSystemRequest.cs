﻿using System.Collections.Generic;
using TotoWinner.Data.Optimizer;

namespace TotoWinner.Data
{
    public class SaveShrinkSystemRequest : ISaveSystemRequest
    {
        public string UniqueId { get; set; }
        public SystemType SystemType { get; set; }
        public string Name { get; set; }
        public BetsFormCreateRequest System { get; set; }
        public List<OptimizedGame> Games { get; set; }
        public ShrinkSystemState State { get; set; }
        public string UiState { get; set; }
    }
}
