﻿namespace TotoWinner.Data
{
    public class ShrinkSystemDefinitions
    {
        public ProgramData ProgramData { get; set; }
        public bool IsProgramDataDirty { get; set; }
        public BetsFormCreateRequest Form { get; set; }
        public bool IsFormDirty { get; set; }

        public ShrinkSystemDefinitions()
        {
            this.ProgramData = new ProgramData();
        }
    }
}
