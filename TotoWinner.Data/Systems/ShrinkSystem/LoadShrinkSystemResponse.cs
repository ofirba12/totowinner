﻿namespace TotoWinner.Data
{
    public class LoadShrinkSystemResponse
    {
        public SaveShrinkSystemRequest SystemData { get; set; }
        public LoadShrinkSystemResponse(SaveShrinkSystemRequest systemData)
        {
            this.SystemData = systemData;
        }
    }
}
