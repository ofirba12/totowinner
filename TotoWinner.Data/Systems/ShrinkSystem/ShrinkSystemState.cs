﻿using System;

namespace TotoWinner.Data.Optimizer
{
    public class ShrinkSystemState
    {
        public string ProgramTitle { get; set; }
        public double BetsCounter { get; set; }
        public double ShrinkedBetsCounter { get; set; }
        public double FinalResult { get; set; }
        //* This section is not needed for optimization, it is more of a state data
        public bool openForBets { get; set; }
        public DateTime programDate { get; set; }
        public ProgramType programType { get; set; }
        public SearchResults calculationResult { get; set; }
}
}
