﻿using System;

namespace TotoWinner.Data
{
    public class SaveShrinkSystemResponse
    {
        public int SystemId { get; set; }
        public SaveShrinkSystemResponse(int systemId)
        {
            this.SystemId = systemId;
        }
    }
}
