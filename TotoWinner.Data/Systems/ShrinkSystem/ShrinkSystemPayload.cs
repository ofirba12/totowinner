﻿namespace TotoWinner.Data
{
    public class ShrinkSystemPayload : ISaveSystemRequest
    {
        public string Name { get; set; }
        public string UniqueId
        {
            get
            {
                return this.Definitions.ProgramData.UniqueId;
            }
            set
            {
                this.Definitions.ProgramData.UniqueId = value;
            }
        }
        public int Year { get; set; }
        public ShrinkSystemDefinitions Definitions { get; set; }
        public SearchResults Result { get; set; }

        public ShrinkSystemPayload()
        {
            this.Definitions = new ShrinkSystemDefinitions();
        }
    }
}
