﻿using System;

namespace TotoWinner.Data
{
    public class LoadShrinkSystemRequest
    {
        public int SystemId { get; set; }
    }
}
