﻿namespace TotoWinner.Data
{
    public class ShrinkSystemLoadResponse
    {
        public ShrinkSystemPayload ShrinkPayload { get; set; }
        public ShrinkSystemLoadResponse(ShrinkSystemPayload payload)
        {
            this.ShrinkPayload = payload;
        }
    }
}
