﻿using System;
using System.Collections.Generic;
using TotoWinner.Data.Optimizer;

namespace TotoWinner.Data
{
    public class ProgramData
    {
        public List<OptimizedGame> ProgramGames { get; set; }
        public string ProgramTitle { get; set; }
        public double FinalResult { get; set; }
        public bool OpenForBets { get; set; }
        public DateTime ProgramDate { get; set; }
        public ProgramType ProgramType { get; set; }
        public string UniqueId { get; set; }

        //public double BetsCounter { get; set; }
        //public double ShrinkedBetsCounter { get; set; }
        ////* This section is not needed for optimization, it is more of a state data
        //public bool openForBets { get; set; }
        //public DateTime programDate { get; set; }
        //public ProgramType programType { get; set; }
        //public SearchResults calculationResult { get; set; }
    }
}
