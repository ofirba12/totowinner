﻿using System;

namespace TotoWinner.Data
{
    public class SystemParametersWithFinalResult : SystemParameters
    {
        public int FinalResult { get; set; }

    }
}
