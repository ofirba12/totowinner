﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace TotoWinner.Data
{
    public class ExecutionState
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ExecutionStatus Status { get; private set; }
        public ExecutionOptimizerData Results { get; private set; }
        public ExecutionState(ExecutionStatus status, ExecutionOptimizerData results)
        {
            try
            {
                this.Status = status;
                this.Results = results;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ExecutionState)}; status={status}", ex);
            }
        }
    }
}
