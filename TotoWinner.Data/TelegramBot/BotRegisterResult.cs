﻿namespace TotoWinner.Data.TelegramBot
{
    public class BotRegisterResult
    {
        public string RegistrationMarkupMessage { get; }
        public string RegistrationCode { get; }
        public bool IsSuccess { get; }

        public BotRegisterResult(string registrationCode, string message, bool isSuccess)
        {
            this.RegistrationCode = registrationCode;
            this.RegistrationMarkupMessage = message;
            this.IsSuccess = isSuccess;
        }
    }
}
