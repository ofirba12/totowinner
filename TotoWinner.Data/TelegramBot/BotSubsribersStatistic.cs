﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.TelegramBot
{
    public class BotSubsribersStatistic
    {
        public long SubscriberId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string CellNumber { get; }
        public string Active { get; }
        public int UrlVisitedCounter { get; set; }
        public DateTime LastActivity { get; set; }
        public int PushedUrlsCounter { get; set; }
        public double DaysNotActive { get; set; }

        public BotSubsribersStatistic(long chatId, string firstName, string lastName, string cellNumber, string active)
        {
            this.SubscriberId = chatId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.CellNumber = cellNumber;
            this.Active = active;
        }
    }
}
