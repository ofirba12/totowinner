﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data.TelegramBot
{
    public class BotSubsribersStatistics
    {
        public Dictionary<long,BotSubsribersStatistic> Statistics { get; } //subscriberId, object
        public int TotalVisited { get; }

        public BotSubsribersStatistics(Dictionary<long, BotSubsribersStatistic> statistics)
        {
            this.Statistics = statistics;
            this.TotalVisited = this.Statistics.Values.Sum(s => s.UrlVisitedCounter);
        }
    }
}
