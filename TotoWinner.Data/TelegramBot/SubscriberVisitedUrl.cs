﻿using System;

namespace TotoWinner.Data.TelegramBot
{
    public class SubscriberVisitedUrl
    {
        public int UrlId { get; }
        public long SubscriberId { get; }

        public SubscriberVisitedUrl(int urlId, long subscriberId)
        {
            try
            {
                this.UrlId = urlId;
                this.SubscriberId = subscriberId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(SubscriberVisitedUrl)}", ex);
            }
        }
    }
}
