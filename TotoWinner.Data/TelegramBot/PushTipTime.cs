﻿using System;

namespace TotoWinner.Data.TelegramBot
{
    public class PushTipTime
    {
        public int Hour { get; }
        public int Minute { get; }

        public PushTipTime(int hour, int minute)
        {
            try
            {
                if (hour < 0 || hour > 23)
                    throw new Exception($"Illegal push hour found: {hour}");
                if (minute < 0 || minute > 59)
                    throw new Exception($"Illegal push minute found: {minute}");
                this.Hour = hour;
                this.Minute = minute;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(PushTipTime)}", ex);
            }
        }
    }
}
