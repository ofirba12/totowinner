﻿namespace TotoWinner.Data.TelegramBot
{
    public enum BotButtonType
    {
        Favorite = 1,
        In3HoursAndFavorite = 2,
        SingleHighRate = 3,
        Custom = 4,
        Tippers = 5
    }
}
