﻿using System;

namespace TotoWinner.Data.TelegramBot
{
    public class BotSubsriber
    {
        public long? ClientChatId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string CellNumber { get; }
        public string Mail { get; }
        public int RegistrationId { get; }
        public DateTime RegisteredDate { get; }
        public string Remarks { get; }
        public DateTime? BlockedDate { get; }
        public string BlockageReason { get; }

        public BotSubsriber(long? clientChatId, 
            string firstName, 
            string lastName, 
            string cell, 
            string mail, 
            int registrationId, 
            DateTime registeredDate,
            string remarks,
            DateTime? blockedDate,
            string blockageReason)
        {
            this.ClientChatId = clientChatId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.CellNumber = cell;
            this.Mail = mail;
            this.RegistrationId = registrationId;
            this.RegisteredDate = registeredDate;
            this.Remarks = remarks;
            this.BlockedDate = blockedDate;
            this.BlockageReason = blockageReason;
        }
    }
}
