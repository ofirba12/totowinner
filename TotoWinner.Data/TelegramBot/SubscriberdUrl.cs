﻿using System;

namespace TotoWinner.Data.TelegramBot
{
    public class SubscriberdUrl
    {
        public int UrlId { get; }
        public long SubscriberId { get; }
        public int TipId { get; }
        public DateTime? VisitedTS { get; }

        public SubscriberdUrl(int urlId, long subscriberId, int tipId, DateTime? visited)
        {
            try
            {
                this.UrlId = urlId;
                this.SubscriberId = subscriberId;
                this.TipId = tipId;
                this.VisitedTS = visited;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(SubscriberdUrl)}", ex);
            }
        }
    }
}
