﻿using System;
namespace TotoWinner.Data
{
    public class ProgramUniqueId
    {
        public int TotoRoundId { get; private set; }
        public ProgramType ProgramType { get; private set; }

        public ProgramUniqueId(int totoRoundId, ProgramType programType)
        {
            try
            {
                this.TotoRoundId = totoRoundId;
                this.ProgramType = programType;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ProgramUniqueId)}; totoRoundId={totoRoundId}, programType={programType}", ex);
            }
        }
        public override string ToString()
        {
            return $"totoRoundId={this.TotoRoundId} | programType={this.ProgramType}";
        }
    }
}
