﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GameWithValidations
    {
        public int Index { get; private set; }
        public string Description { get; private set; }
        public double Rate1 { get; set; }
        public double RateX { get; set; }
        public double Rate2 { get; set; }
        public string Won { get; set; }
        public string GameResult { get; set; }
        public bool OpenForBets { get; set; }
        public List<ProgramValidationStatus> Validations { get; private set; }

        public GameWithValidations(int index, string description, double rate1, double rateX, double rate2, 
            string won, string result, bool openForBets, List<ProgramValidationStatus> validation)
        {
            try
            {
                this.Index = index;
                this.Description = description;
                this.Rate1 = rate1;
                this.RateX = rateX;
                this.Rate2 = rate2;
                this.Won = won;
                this.GameResult = string.IsNullOrWhiteSpace(result)?null : result.Trim();
                this.OpenForBets = openForBets;
                this.Validations = validation;
            }
            catch(Exception ex)
            {
                var validationStr = string.Join(",", validation);
                throw new Exception($"An error occured trying to construct type {typeof(GameWithValidations)}; index={index} | description={description} | rate1,X,2={rate1},{rateX},{rate2} | validation={validationStr}", ex);
            }
        }
    }
}
