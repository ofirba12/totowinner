﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ProgramsResult
    {
        public Dictionary<ProgramUniqueId, ProgramMetaData> Collection { get; private set; }

        public ProgramsResult(Dictionary<ProgramUniqueId, ProgramMetaData> collection)
        {
            try
            {
                if (collection == null)
                    throw new Exception("The specified collection can not be null");
                this.Collection = collection;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ProgramDataResult)}", ex);
            }
        }
    }
}
