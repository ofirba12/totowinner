﻿namespace TotoWinner.Data
{
    public class RatesPipe
    {
        public byte GameIndex { get; set; }
        public double Rate1 { get; set; }
        public double RateX { get; set; }
        public double Rate2 { get; set; }
    }
}
