﻿using System;

namespace TotoWinner.Data
{
    public class BetDetails
    {
        public double Rate { get; private set; }
        public bool Won { get; set; }

        public BetDetails(double rate, bool won)
        {
            try
            {
                if (rate <= 0)
                    throw new Exception("Rate must be positive");
                this.Rate = rate;
                this.Won = won;
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BetDetails)} with rate={rate}, won={won}", ex);
            }
        }
    }
}
