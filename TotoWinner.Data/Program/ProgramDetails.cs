﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ProgramDetails
    {
        public int GameIndex { get; private set; }
        public string BetColor { get; private set; }
        public string GameTime { get; private set; }
        public string LeagueName { get; private set; }
        public string GameName { get; private set; }
        public string GameResult { get; private set; }
        public BetsRepository Bets { get; private set; }
        public Dictionary<ProgramValidationStatus, string> Validations { get; set; }
        //public string ValidationMessage
        //{
        //    get
        //    {
        //        return ValidationStatus.ToString();
        //    }
        //}

        public ProgramDetails(int gameIndex, string betColor, string gameTime, string leagueName,
            string gameName, string gameResult, BetsRepository bets)
        {
            try
            {
                this.GameIndex = gameIndex;
                this.BetColor = betColor;
                this.GameTime = gameTime;
                this.LeagueName = leagueName;
                this.GameName = gameName;
                this.GameResult = gameResult;
                this.Bets = bets;
                this.Validations = new Dictionary<ProgramValidationStatus, string>()
                {
                    { ProgramValidationStatus.ValidForCalculation, ProgramValidationStatus.ValidForCalculation.ToString() }
                };
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ProgramDetails)}, gameIndex={gameIndex} | gameTime={gameTime}", ex);
            }
        }
        public void SetBetColor(string color)
        {
            //blue, black, green, red
            this.BetColor = color;
        }
    }
}
