﻿
using System;

namespace TotoWinner.Data
{
    public class ProgramMetaData
    {
        public DateTime BeginDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public int ExternalId { get; private set; }

        public ProgramMetaData(DateTime beginDate, DateTime endDate, int externalId)
        {
            try
            {
                this.BeginDate = beginDate;
                this.EndDate = endDate;
                this.ExternalId = externalId;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ProgramMetaData)}; beginDate={beginDate}, endDate={endDate}, externalId={externalId}", ex);
            }
        }
        public override string ToString()
        {
            return $"beginDate={this.BeginDate} | endDate={this.EndDate} | externalId={this.ExternalId}";
        }

    }
}
