﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data
{
    public class ProgramDataResponse
    {
        public bool IsSuccess { get; private set;}
        public string Message { get; private set; }
        public Dictionary<int, ProgramDetails> Details { get; private set; }
        public double? FinalResult { get; private set; }

        public ProgramDataResponse(Dictionary<int, ProgramDetails> details, ProgramType programType, double? finalResult)
        {
            try
            {
                this.IsSuccess = true;
                this.Message = "תוכניה הובאה בהצלחה";

                this.Details = details;
                this.FinalResult = finalResult;
            }
            catch(Exception ex)
            {
                this.IsSuccess = false;
                this.Message = "תהליך הבאת תוכניה נכשל";
                throw new System.Exception($"An error occured trying to construct type {typeof(ProgramDataResponse)} from programType={programType}", ex);
            }
        }
    }
}
