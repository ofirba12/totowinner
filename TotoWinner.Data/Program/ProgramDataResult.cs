﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ProgramDataResult
    {
        public Dictionary<int, ProgramDetails> Details { get; private set; }
        public double? FinalResult { get; private set; }

        public ProgramDataResult(Dictionary<int, ProgramDetails> details, double? finalResult)
        {
            try
            {
                this.Details = details;
                this.FinalResult = finalResult;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ProgramDataResult)}", ex);
            }
        }
    }
}
