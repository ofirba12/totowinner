﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class RealTimePipeGetRequest
    {
        public ProgramType ProgramType { get; set; }
    }
}
