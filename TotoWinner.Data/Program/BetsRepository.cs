﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data
{
    public class BetsRepository
    {
        public bool OpenForBets { get; private set; }
        public Dictionary<BetOutputType, BetDetails> Details { get; private set; }

        public BetsRepository(bool openForBets, Dictionary<BetOutputType, BetDetails> details, 
            bool isDataAmended = false)
        {
            try
            {
                this.OpenForBets = openForBets;
                if (details.Keys.Count != 3)
                    throw new Exception("Bets details must have 3 object {1,X,2}");
                this.Details = details;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(BetsRepository)}", ex);
            }
        }
    }
}
