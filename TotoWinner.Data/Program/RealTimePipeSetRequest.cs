﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class RealTimePipeSetRequest
    {
        public ProgramType ProgramType { get; set; }
        public List<RatesPipe> Rates { get; set; }
    }
}
