﻿using System;

namespace TotoWinner.Data
{
    public class ProgramDataSyncRequest : ProgramDataRequest
    {
        public int TotoRoundId { get; set; }
    }
}
