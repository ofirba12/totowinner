﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class RealTimePipeGetResponse : ResponseBase
    {
        public List<RatesPipe> Rates { get; }

        public RealTimePipeGetResponse(List<RatesPipe> rates) :
            base(true, string.Empty)
        {
            try
            {
                this.Rates = rates;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(RealTimePipeGetResponse)}", ex);
            }
        }

        public RealTimePipeGetResponse(string errorMessage) :
            base(false, errorMessage)
        { }

    }
}
