﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class ProgramWithGames
    {
        public int Id { get; private set; }
        public int Number { get; private set; }
        public DateTime EndDate { get; private set; }
        public Dictionary<int,GameWithValidations> Games { get; private set; }
        public ProgramWithGames(int id, int number, DateTime endDate, Dictionary<int, GameWithValidations> games)
        {
            try
            {
                this.Id = id;
                this.Number = number;
                this.EndDate = endDate;
                this.Games = games;
            }
            catch(Exception ex)
            {
                throw new Exception($"An error occured trying to construct type {typeof(ProgramWithGames)}; id={id} | number={number} | endDate={endDate}", ex);
            }
        }

    }
}
