﻿using System;

namespace TotoWinner.Data
{
    public class ProgramDataRequest
    {
        public DateTime ProgramEndDate { get; set; }
        public ProgramType ProgramType { get; set; }
    }
}
