﻿namespace TotoWinner.Data
{
    public class Shapes3DataABC : StatisticalDataBase
    {
        public Shapes2DataABC AmountA { get; set; } // AAA, AAB, AAC, ABA, ABB, ABC, ACA, ACB, ACC
        public Shapes2DataABC AmountB { get; set; } // BAA, BAB, BAC, BBA, BBB, BBC, BCA, BCB, BCC
        public Shapes2DataABC AmountC { get; set; } // CAA, CAB, CAC, CBA, CBB, CBC, CCA, CCB, CCC

        public Shapes3DataABC()
        {
            this.AmountA = new Shapes2DataABC();
            this.AmountB = new Shapes2DataABC();
            this.AmountC = new Shapes2DataABC();
        }
    }
}
