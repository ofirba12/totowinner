﻿using System;

namespace TotoWinner.Data
{
    public abstract class StatisticalDataBase
    {
        public DateTime ProgramEndDate;
        public int RoundId;
    }
}
