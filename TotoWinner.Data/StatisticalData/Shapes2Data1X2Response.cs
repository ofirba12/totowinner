﻿namespace TotoWinner.Data
{
    public class Shapes2Data1X2Response : StatisticalDataBase
    {
        public int? Amount11 { get; set; }
        public int? Amount1X { get; set; }
        public int? Amount12 { get; set; }
        public int? AmountX1 { get; set; }
        public int? AmountXX { get; set; }
        public int? AmountX2 { get; set; }
        public int? Amount21 { get; set; }
        public int? Amount2X { get; set; }
        public int? Amount22 { get; set; }
    }
}
