﻿namespace TotoWinner.Data
{
    public class RelationsData
    {
        public decimal Min { get; set; }
        public decimal Middle { get; set; }
        public decimal Max { get; set; }
        public decimal Home { get; set; }
        public decimal Draft { get; set; }
        public decimal Away { get; set; }
        public decimal Sum { get; set; }
        public decimal FinalResult { get; set; }
        public decimal DeltaMin { get; set; }
        public decimal DeltaMiddle { get; set; }
        public decimal DeltaMax { get; set; }
        public decimal DeltaHome { get; set; }
        public decimal DeltaDraft { get; set; }
        public decimal DeltaAway { get; set; }
    }
}
