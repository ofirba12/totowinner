﻿namespace TotoWinner.Data
{
    public class GetStatisticalDataRequest
    {
        public int Year { get; set; }
        public ProgramType Type { set; get; }
    }
}
