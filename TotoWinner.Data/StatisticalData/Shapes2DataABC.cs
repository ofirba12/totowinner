﻿namespace TotoWinner.Data
{
    public class Shapes2DataABC : StatisticalDataBase
    {
        public int? AmountAA { get; set; }
        public int? AmountAB { get; set; }
        public int? AmountAC { get; set; }
        public int? AmountBA { get; set; }
        public int? AmountBB { get; set; }
        public int? AmountBC { get; set; }
        public int? AmountCA { get; set; }
        public int? AmountCB { get; set; }
        public int? AmountCC { get; set; }
    }
}
