﻿namespace TotoWinner.Data
{
    public class GroupsBasicDataResponse : StatisticalDataBase
    {
        public int? Amount0 { get; set; }
        public int? Amount1 { get; set; }
        public int? Amount2 { get; set; }
        public int? Amount3Plus { get; set; }
    }
}
