﻿namespace TotoWinner.Data
{
    public class Shapes3Data1X2 : StatisticalDataBase
    {
        public Shapes2Data1X2 Amount1 { get; set; } // 111, 11X, 112, 1X1, 1XX, 1X2, 121, 12X, 122
        public Shapes2Data1X2 AmountX { get; set; } // X11, X1X, X12, XX1, XXX, XX2, X21, X2X, X22
        public Shapes2Data1X2 Amount2 { get; set; } // 211, 21X, 212, 2X1, 2XX, 2X2, 221, 22X, 222

        public Shapes3Data1X2()
        {
            this.Amount1 = new Shapes2Data1X2();
            this.AmountX = new Shapes2Data1X2();
            this.Amount2 = new Shapes2Data1X2();
        }
    }
}
