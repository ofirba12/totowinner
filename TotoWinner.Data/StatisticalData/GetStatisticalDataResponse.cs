﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GetStatisticalDataResponse : ResponseBase
    {
        public List<BasicData3Response> Amount1X2 { get; }
        public List<BasicData3Response> AmountABC { get; }
        public List<BasicData3Response> SequenceLength1X2 { get; }
        public List<BasicData3Response> SequenceLengthABC { get; }
        public List<BasicData1Response> Breaks1X2 { get; }
        public List<BasicData1Response> BreaksABC { get; }
        public List<Shapes2Data1X2Response> Shapes2Data1X2 { get; }
        public List<Shapes2DataABCResponse> Shapes2DataABC { get; }
        public List<GroupsBasicDataResponse> ShapesGroups2Data1X2 { get; }
        public List<GroupsBasicDataResponse> ShapesGroups2DataABC { get; }
        public List<Shapes3Data1X2Response> Shapes3Data1X2 { get; }
        public List<Shapes3DataABCResponse> Shapes3DataABC { get; }
        public List<GroupsBasicDataResponse> ShapesGroups3Data1X2 { get; }
        public List<GroupsBasicDataResponse> ShapesGroups3DataABC { get; }
        public List<RelationsDataResponse> RelationsData { get; }

        public GetStatisticalDataResponse(List<BasicData3Response> amount1X2,
            List<BasicData3Response> amountABC,
            List<BasicData3Response> sequenceLength1X2,
            List<BasicData3Response> sequenceLengthABC,
            List<BasicData1Response> breaks1X2,
            List<BasicData1Response> breaksABC,
            List<Shapes2Data1X2Response> shapes2Data1X2,
            List<Shapes2DataABCResponse> shapes2DataABC,
            List<GroupsBasicDataResponse> shapesGroups2Data1X2,
            List<GroupsBasicDataResponse> shapesGroups2DataABC,
            List<Shapes3Data1X2Response> shapes3Data1X2,
            List<Shapes3DataABCResponse> shapes3DataABC,
            List<GroupsBasicDataResponse> shapesGroups3Data1X2,
            List<GroupsBasicDataResponse> shapesGroups3DataABC,
            List<RelationsDataResponse> relationsData) : base(true)
        {
            this.Amount1X2 = amount1X2;
            this.AmountABC = amountABC;
            this.SequenceLength1X2 = sequenceLength1X2;
            this.SequenceLengthABC = sequenceLengthABC;
            this.Breaks1X2 = breaks1X2;
            this.BreaksABC = breaksABC;
            this.Shapes2Data1X2 = shapes2Data1X2;
            this.Shapes2DataABC = shapes2DataABC;
            this.ShapesGroups2Data1X2 = shapesGroups2Data1X2;
            this.ShapesGroups2DataABC = shapesGroups2DataABC;
            this.Shapes3Data1X2 = shapes3Data1X2;
            this.Shapes3DataABC = shapes3DataABC;
            this.ShapesGroups3Data1X2 = shapesGroups3Data1X2;
            this.ShapesGroups3DataABC = shapesGroups3DataABC;
            this.RelationsData = relationsData;
        }
        public GetStatisticalDataResponse(string error) : base(false, error) { }

    }
}
