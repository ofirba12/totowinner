﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class CalculatedProgramStatistics
    {
        public List<int> Options { get;}
        public List<char> LettersOptions { get; }
        public List<decimal> ProbOptions { get; }
        public BasicData3 Amount1X2 { get; }
        public BasicData3 AmountABC { get; }
        public BasicData3 SequenceLength1X2 { get; }
        public BasicData3 SequenceLengthABC { get; }
        public BasicData1 Breaks1X2 { get; }
        public BasicData1 BreaksABC { get; }
        public Shapes2Data1X2 Shapes2Data1X2 { get; }
        public Shapes2DataABC Shapes2DataABC { get; }
        public GroupsBasicData ShapesGroups2Data1X2 { get; }
        public GroupsBasicData ShapesGroups2DataABC { get; }
        public Shapes3Data1X2 Shapes3Data1X2 { get; }
        public Shapes3DataABC Shapes3DataABC { get; }
        public GroupsBasicData ShapesGroups3Data1X2 { get; }
        public GroupsBasicData ShapesGroups3DataABC { get; }
        public RelationsData RelationsData { get; }

        public CalculatedProgramStatistics(
            ProgramWithGames program,
            List<int> options, 
            List<char> lettersOptions, 
            BasicData3 amount1X2,
            BasicData3 amountABC,
            BasicData3 sequenceLength1X2,
            BasicData3 sequenceLengthABC,
            BasicData1 breaks1X2,
            BasicData1 breaksABC,
            Shapes2Data1X2 shapes2Data1X2,
            GroupsBasicData shapesGroups2Data1X2,
            Shapes2DataABC shapes2DataABC,
            GroupsBasicData shapesGroups2DataABC,
            Shapes3Data1X2 shapes3Data1X2,
            GroupsBasicData shapesGroups3Data1X2,
            Shapes3DataABC shapes3DataABC,
            GroupsBasicData shapesGroups3DataABC,
            RelationsData relationsData
            )
        {
            this.Options = options;
            this.LettersOptions = lettersOptions;

            this.Amount1X2 = amount1X2;
            this.AmountABC = amountABC;

            this.SequenceLength1X2 = sequenceLength1X2;
            this.SequenceLengthABC = sequenceLengthABC;

            this.Breaks1X2 = breaks1X2;
            this.Breaks1X2.ProgramEndDate = program.EndDate;
            this.Breaks1X2.RoundId = program.Number;

            this.BreaksABC = breaksABC;
            this.BreaksABC.ProgramEndDate = program.EndDate;
            this.BreaksABC.RoundId = program.Number;

            this.Shapes2Data1X2 = shapes2Data1X2;
            this.Shapes2Data1X2.ProgramEndDate = program.EndDate;
            this.Shapes2Data1X2.RoundId = program.Number;

            this.ShapesGroups2Data1X2 = shapesGroups2Data1X2;
            this.ShapesGroups2Data1X2.ProgramEndDate = program.EndDate;
            this.ShapesGroups2Data1X2.RoundId = program.Number;

            this.Shapes2DataABC = shapes2DataABC;
            this.Shapes2DataABC.ProgramEndDate = program.EndDate;
            this.Shapes2DataABC.RoundId = program.Number;

            this.ShapesGroups2DataABC = shapesGroups2DataABC;
            this.ShapesGroups2DataABC.ProgramEndDate = program.EndDate;
            this.ShapesGroups2DataABC.RoundId = program.Number;

            this.Shapes3Data1X2 = shapes3Data1X2;
            this.Shapes3Data1X2.ProgramEndDate = program.EndDate;
            this.Shapes3Data1X2.RoundId = program.Number;

            this.ShapesGroups3Data1X2 = shapesGroups3Data1X2;
            this.ShapesGroups3Data1X2.ProgramEndDate = program.EndDate;
            this.ShapesGroups3Data1X2.RoundId = program.Number;

            this.Shapes3DataABC = shapes3DataABC;
            this.Shapes3DataABC.ProgramEndDate = program.EndDate;
            this.Shapes3DataABC.RoundId = program.Number;

            this.ShapesGroups3DataABC = shapesGroups3DataABC;
            this.ShapesGroups3DataABC.ProgramEndDate = program.EndDate;
            this.ShapesGroups3DataABC.RoundId = program.Number;

            this.RelationsData = relationsData;

        }
    }
}
