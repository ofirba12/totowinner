﻿namespace TotoWinner.Data
{
    public static class StatisticalDataExentions
    {
        public static BasicData3Response ToBasicData3Response(this BasicData3 data, ProgramWithGames program)
        {
            var result = new BasicData3Response()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                Amount1 = data.Amount1,
                Amount2 = data.Amount2,
                Amount3 = data.Amount3
            };
            return result;
        }
        public static BasicData1Response ToBasicData1Response(this BasicData1 data, ProgramWithGames program)
        {
            var result = new BasicData1Response()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                Amount = data.Amount
            };
            return result;
        }
        public static Shapes2Data1X2Response ToShapes2Data1X2Response(this Shapes2Data1X2 data, ProgramWithGames program)
        {
            var result = new Shapes2Data1X2Response()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                Amount11 = data.Amount11,
                Amount1X = data.Amount1X,
                Amount12 = data.Amount12,
                AmountX1 = data.AmountX1,
                AmountXX = data.AmountXX,
                AmountX2 = data.AmountX2,
                Amount21 = data.Amount21,
                Amount2X = data.Amount2X,
                Amount22 = data.Amount22
            };
            return result;
        }
        public static Shapes2DataABCResponse ToShapes2DataABCResponse(this Shapes2DataABC data, ProgramWithGames program)
        {
            var result = new Shapes2DataABCResponse()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                AmountAA = data.AmountAA,
                AmountAB = data.AmountAB,
                AmountAC = data.AmountAC,
                AmountBA = data.AmountBA,
                AmountBB = data.AmountBB,
                AmountBC = data.AmountBC,
                AmountCA = data.AmountCA,
                AmountCB = data.AmountCB,
                AmountCC = data.AmountCC
            };
            return result;
        }
        public static GroupsBasicDataResponse ToGroupsBasicDataResponse(this GroupsBasicData data, ProgramWithGames program)
        {
            var result = new GroupsBasicDataResponse()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                Amount0 = data.Amount0,
                Amount1 = data.Amount1,
                Amount2 = data.Amount2,
                Amount3Plus = data.Amount3Plus

            };
            return result;
        }
        public static Shapes3Data1X2Response ToShapes3Data1X2Response(this Shapes3Data1X2 data, ProgramWithGames program)
        {
            var result = new Shapes3Data1X2Response()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                Amount1 = data.Amount1,
                AmountX = data.AmountX,
                Amount2 = data.Amount2

            };
            return result;
        }
        public static Shapes3DataABCResponse ToShapes3DataABCResponse(this Shapes3DataABC data, ProgramWithGames program)
        {
            var result = new Shapes3DataABCResponse()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                AmountA = data.AmountA,
                AmountB = data.AmountB,
                AmountC = data.AmountC

            };
            return result;
        }
        public static RelationsDataResponse ToRelationsDataResponse(this RelationsData data, ProgramWithGames program)
        {
            var result = new RelationsDataResponse()
            {
                ProgramEndDate = program.EndDate,
                RoundId = program.Number,
                Min = data.Min,
                Middle = data.Middle,
                Max = data.Max,
                Home = data.Home,
                Draft = data.Draft,
                Away = data.Away,
                Sum = data.Sum,
                FinalResult = data.FinalResult,
                DeltaMin = data.DeltaMin,
                DeltaMiddle = data.DeltaMiddle,
                DeltaMax = data.DeltaMax,
                DeltaHome = data.DeltaHome,
                DeltaDraft = data.DeltaDraft,
                DeltaAway = data.DeltaAway
            };
            return result;
        }
    }
}
