﻿using System;

namespace TotoWinner.Data
{
    public class DeleteSmartFileRequest
    {
        public int Id { get; }

        public DeleteSmartFileRequest(int id)
        {
            try
            {
                if (id == 0)
                    throw new Exception("The specified id is not set");
                this.Id = id;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(DeleteSmartFileRequest)}", ex);
            }
        }
    }
}