﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class GetSmartFilesResponse : ResponseBase
    {
        public List<SmartFileContainer> Files { get; }

        public GetSmartFilesResponse(List<SmartFileContainer> files) :
            base(true, string.Empty)
        {
            try
            {
                this.Files = files;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(GetSmartFilesResponse)}", ex);
            }
        }
        public GetSmartFilesResponse(string errorMessage) : 
            base(false, errorMessage)
        { }
    }
}