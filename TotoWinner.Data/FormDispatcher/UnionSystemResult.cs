﻿
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class UnionSystemResult
    {
        public IEnumerable<CombinedResult> UnionResult { get;}
        public int CounterBeforeUnion { get; }
        public int CounterAfterUnion { get; }
        public IReadOnlyDictionary<int, Container1X2> Counters { get; }
        public IReadOnlyDictionary<int, Container1X2> CountersPercent { get; }

        public UnionSystemResult(IEnumerable<CombinedResult> unionResult, int counterBefore, int counterAfter, 
            Dictionary<int, Container1X2> counters, 
            Dictionary<int, Container1X2> countersPercent)
        {
            this.UnionResult = unionResult;
            this.CounterBeforeUnion = counterBefore;
            this.CounterAfterUnion = counterAfter;
            this.Counters = counters;
            this.CountersPercent = countersPercent;
        }
    }
}
