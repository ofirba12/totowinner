﻿using System;
using System.IO;

namespace TotoWinner.Data
{
    public class SmartFileContainer
    {
        public int Id { get; }
        public string SystemIds { get; }
        public System.DateTime LastUpdate { get; }
        public string DownloadUrl { get; }
        public string Filename { get; }
        public  SmartFileContainer(int id, string systemIds, DateTime lastUpdate, string downloadUrl)
        {
            try
            {
                this.Id = id;
                this.SystemIds = systemIds;
                this.LastUpdate = lastUpdate;
                this.DownloadUrl = downloadUrl;
                this.Filename = Path.GetFileName(downloadUrl);
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(SmartFileContainer)} from id={id}, systemIds={systemIds}, lastUpdate={lastUpdate}, downloadUrl={downloadUrl}", ex);
            }
        }

    }
}
