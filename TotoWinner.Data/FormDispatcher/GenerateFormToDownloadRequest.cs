﻿using System;

namespace TotoWinner.Data
{
    public class GenerateFormToDownloadRequest
    {
        public int UnionResultId { get; }
        public FormTypeGeneration FormType { get;  }

        public GenerateFormToDownloadRequest(int unionResultId, FormTypeGeneration formType)
        {
            try
            {
                if (formType == FormTypeGeneration.None)
                    throw new Exception("The specified form type generation was not set");
                this.UnionResultId = unionResultId;
                this.FormType = formType;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(GenerateFormToDownloadRequest)}", ex);
            }
        }
    }
}