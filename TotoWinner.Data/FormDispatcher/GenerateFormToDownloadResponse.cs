﻿using System;

namespace TotoWinner.Data
{
    public class GenerateFormToDownloadResponse : ResponseBase
    {
        public string OutputFile { get; private set; }

        public GenerateFormToDownloadResponse(string outputFile, bool isSuccess, string errorMessage = null) :
            base(isSuccess, errorMessage)
        {
            try
            {
                if (isSuccess == true && string.IsNullOrEmpty(outputFile))
                    throw new Exception($"outputFile file empty, operation failed");
                this.OutputFile = outputFile;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(GenerateFormToDownloadResponse)}", ex);
            }
        }
        public GenerateFormToDownloadResponse(string errorMessage) : 
            base(false, errorMessage)
        { }
    }
}