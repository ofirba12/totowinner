﻿namespace TotoWinner.Data
{
    public enum SystemType
    {
        Shrink = 1,
        Optimizer = 2,
        Backtesting = 3
    }
    public enum CombinationType
    {
        Pattern1X2,
        PatternLetters
    }
    public enum ExecutionStatus
    {
        None = 0,
        Queue = 1,
        Running = 2,
        Stopped = 3,
        Finished = 4,
        Error = 5
    }
    public enum ShapeFilterType
    {
        RegularShapes2,
        RegularShapes3,
        LetterShapes2,
        LetterShapes3,
        NotShapesFilter
    }
    public enum ProgramType
    {
        WinnerHalf = 82,
        WinnerWorld = 87,
        Winner16 = 96,
    }
    public enum BetOutputType
    {
        Bet1,
        BetX,
        Bet2
    }
    public enum ProgramValidationStatus
    {
        ValidForCalculation,
        MissingRates,
        DummyGame,
        NoWinnerFound,
        GamePostponed,
        GameCanceled,
        GameResultMissing
    }

    public enum ConditionType
    {
        Amount1X2,
        Sequence1X2,
        Breaks1X2,
        AmountABC,
        SequenceABC,
        BreaksABC,
        Range,
        None
    }
    public enum ConditionBetTitleEnum
    {
        None,
        Title_1,
        Title_X,
        Title_2,
        Title_A,
        Title_B,
        Title_C
    }
    public enum AdvancedConditionType
    {
        Single1X2,
        SingleABC,
        PairShape1X2,
        PairShapeABC,
        TripletShape1X2,
        TripletShapeABC,
        None
    }
    public enum BtGameDefinitionType
    {
        Banker = 1,
        Double = 2
    }
    public enum BtWinType
    {
        HomeWin = 1,
        BreakEven = 2,
        GuestWin = 3,
        Any = 4
    }
    public enum BtRateType
    {
        Low = 1,
        High = 2
//        MIddle = 3
    }
    public enum BtBankerBetType
    {
        BreakEven = 1,
        Regular = 2
    }
    public enum BtDoubleBetType
    {
        Bet_1X = 1,
        Bet_12 = 2,
        Bet_2X = 3,
        Any = 4
    }
    public enum BtProgramResultStatus
    {
        OK = 0,
        InvalidProgram = -1,
        TooManyBets = -2
    }
    public enum TemplateBaseBetsStatus
    {
        Success = 1,
        InvalidTemplate = 2,
        NoBaseFound = 3
    }
    public enum BtBasicShrinkFieldType
    {
        Amount1X2_1,
        Amount1X2_X,
        Amount1X2_2,
        AmountABC_A,
        AmountABC_B,
        AmountABC_C,
        AmountSeq1X2_1,
        AmountSeq1X2_X,
        AmountSeq1X2_2,
        AmountSeqABC_A,
        AmountSeqABC_B,
        AmountSeqABC_C,
        Break1X2,
        BreakABC
    }
    public enum BtShapesShrinkFieldType
    {
        Shapes2_1X2_0,
        Shapes2_1X2_1,
        Shapes2_1X2_2,
        Shapes2_1X2_3Plus,
        Shapes2_ABC_0,
        Shapes2_ABC_1,
        Shapes2_ABC_2,
        Shapes2_ABC_3Plus,
        Shapes3_1X2_0,
        Shapes3_1X2_1,
        Shapes3_1X2_2,
        Shapes3_1X2_3Plus,
        Shapes3_ABC_0,
        Shapes3_ABC_1,
        Shapes3_ABC_2,
        Shapes3_ABC_3Plus
    }
    public enum BtRatesShrinkFieldType
    {
        Minimum,
        Middle,
        Maximum,
        Home,
        Draft,
        Away
    }
    public enum DrillerShrinkTemplateType
    {
        HowMany1,
        HowManyX,
        HowMany2,
        HowManyA,
        HowManyB,
        HowManyC,
        HowManySeq1,
        HowManySeqX,
        HowManySeq2,
        HowManySeqA,
        HowManySeqB,
        HowManySeqC,
        Breaks1X2,
        BreaksABC,
        Shape2_1X2_0,
        Shape2_1X2_1,
        Shape2_1X2_2,
        Shape2_1X2_3Plus,
        Shape2_ABC_0,
        Shape2_ABC_1,
        Shape2_ABC_2,
        Shape2_ABC_3Plus,
        Shape3_1X2_0,
        Shape3_1X2_1,
        Shape3_1X2_2,
        Shape3_1X2_3Plus,
        Shape3_ABC_0,
        Shape3_ABC_1,
        Shape3_ABC_2,
        Shape3_ABC_3Plus,
        DeltaMin,
        DeltaAvg,
        DeltaMax,
        DeltaHome,
        DeltaDraft,
        DeltaAway
    }
    public enum TemplateQualification
    {
        Low,
        Medium,
        Strong
    }    public enum FormTypeGeneration
    {
        None = 0,
        Player = 1,
        Excel = 2,
        SMART = 3
    }


}
