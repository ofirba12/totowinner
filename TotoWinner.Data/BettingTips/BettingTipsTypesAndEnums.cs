﻿namespace TotoWinner.Data
{
    public static class BettingTipsConstants
    {
        public const string RepositoryPath = "Repository";
        public const string ResultPath = "Result";
    }
    public enum ResolveEnum
    {
        INT,
        BOOL,
        DATE,
        DECIMAL,
        STRING,
        INT_NULLABLE,
        LONG, 
        DATE2
    }
    public enum GameResult
    {
        Win = 0,
        Draw = 1,
        Lost = 2,
    }
    public enum FileType
    {
        SoccerFather,
        SoccerChild23Gols,
        SoccerChild2point5Gols,
        BasketballBarrier,
        BasketBallFatherExtraPoints
    }
    public enum GameType
    {
        Home,
        Guest
    }
    public enum GameLocation
    {
        HomeGame = 0,
        Outside
    }
    public enum SportIds
    {
        Tennis = 1,
        Soccer = 2,
        Baseball = 3,
        Handball = 4,
        Basketball = 5,
        Football = 7
    }
    public enum BetTypes
    {
        #region Tennis
        BetTennis1 = 1,                         //מנצחת
        BetTennis25 = 25,                       //הימור עם יתרון משחקונים
        BetTennis32 = 32,                       //הימור עם יתרון משחקונים
        BetTennis36 = 36,                       //מעל מתחת משחקונים
        BetTennis51 = 51,                       //תוצאה מדויקת
        BetTennis54 = 54,                       //סהכ מערכות
        BetTennis75 = 75,                       //הימור עם יתרון מערכות
        BetTennis91 = 91,                       //מעל מתחת אייסים לשחקן
        BetTennis92 = 92,                       //מעל מתחת אייסים
        #endregion

        #region Baseball
        BetBaseball6 = 6,                       //מנצחת
        BetBaseball7 = 7,                       //מעל מתחת ריצות
        BetBaseball53 = 53,                     //הימור יתרון
        #endregion

        #region Handball
        BetHandball44 = 44,                     //סהכ שערים
        BetHandball42 = 42,                     //הימור יתרון
        BetHandball38 = 38,                     //מעל מתחת שערים ללא תיקו
        BetHandball2 = 2,                       //יתרון בלי תיקו
        #endregion

        #region Football
        BetFootball64 = 64,                     //מעל מתחת יארדים
        BetFootball63 = 63,                     //מעל מתחת טאצדאון
        BetFootball56 = 56,                     //סהכ יארדים ריצה
        BetFootball55 = 55,                     //סהכ יארדים מסירה
        BetFootball46 = 46,                     //סהכ נקודות
        BetFootball45 = 45,                     //הימור יתרון
        BetFootball43 = 43,                     //המנצח
        BetFootball41 = 41,                     //מעל מתחת נקודות
        BetFootball40 = 40,                     //יתרון בלי תיקו
        #endregion

        #region Soccer
        BetSoccer1x2 = 3,                       //1X2
        BetSoccerAdvance = 4,                   //הימור יתרון
        BetSoccerOverUnder2_5 = 5,              //מעל מתחת שערים
        BetSoccerTotalScores = 11,              //סך הכל טווח 2-3 שערים
        BetSoccerOther97 = 97,                  //בעיטות למסגרת
        BetSoccerOther96 = 96,                  //סהכ בעיטות
        BetSoccerOther66 = 66,                  //איזה קבוצה תבקיע יותר עד דקה 15
        BetSoccerOther29 = 29,                  //אירוע ראשון במשחק
        BetSoccerOther12 = 12,                  //באיזו מחצית יותר שערים
        BetSoccerOther30 = 30,                  //בעיטת פתיחה
        BetSoccerOther69 = 69,                  //האם יהיה פנדל
        BetSoccerOther48 = 48,                  //האם כל קבוצה תבקיע
        BetSoccerOther26 = 26,                  //הזוכה בגביע
        BetSoccerOther37 = 37,                  //הימור יתרון
        BetSoccerOther35 = 35,                  //המנצחת
        BetSoccerOther39 = 39,                  //העולה לשלב הבא
        BetSoccerOther73 = 73,                  //הראשונה ל-5 קרנות
        BetSoccerOther70 = 70,                  //הראשונה ל-3 קרנות
        BetSoccerOther20 = 20,                  //חילוף ראשון
        BetSoccerOther13 = 13,                  //טווח קרנות
        BetSoccerOther33 = 33,                  //יתרון קרנות
        BetSoccerOther71 = 71,                  //מעל מתחת קרן אחת  דקה 15
        BetSoccerOther72 = 72,                  //מעל מתחת 0.5 שערים דקה 15
        BetSoccerOther99 = 99,                  //מעל מתחת איומים
        BetSoccerOther95 = 95,                  //מעל מתחת איומים אורחת
        BetSoccerOther93 = 93,                  //מעל מתחת איומים לשער
        BetSoccerOther94 = 94,                  //מעל מתחת איומים אורחת עם הארכה
        BetSoccerOther98 = 98,                  //מעל מתחת איומים עם הארכה
        BetSoccerOther27 = 27,                  //מעל מתחת נבדלים
        BetSoccerOther52 = 52,                  //מעל מתחת קרנות
        BetSoccerOther68 = 68,                  //מעל מתחת קרנות עד דקה 15
        BetSoccerOther8 = 8,                    //מעל מתחת שערים
        BetSoccerOther67 = 67,                  //מעל מתחת שערים עד דקה 15
        BetSoccerOther14 = 14,                  //מצאפ שערים
        BetSoccerOther28 = 28,                  //נבדל ראשון
        BetSoccerOther61 = 61,                  //קבוצת הבית תשמור על שער נקי
        BetSoccerOther62 = 62,                  //קבוצת החוץ תשמור על שער נקי
        BetSoccerOther31 = 31,                  //קרן אחרונה
        BetSoccerOther21 = 21,                  //קרן ראשונה
        BetSoccerOther49 = 49,                  //שער אחרון במשחק
        BetSoccerOther22 = 22,                  //שער עד דקה
        BetSoccerOther47 = 47,                  //שער ראשון במשחק
        #endregion
        #region Basketball
        BetBasketballAdvanceWithoutDraw = 9,    //יתרון בלי תיקו
        BetBasketballOverUnder1 = 10,           //מעל מתחת נקודות
        BetBasketballAdvance = 15,              //הימור יתרון
        BetBasketballOverUnder2 = 16,           //מעל מתחת נקודות
        BetBasketballOther58 = 58,              //הראשונה ל-10 נקודות
        BetBasketballOther57 = 57,              //הראשונה ל-20 נקודות
        BetBasketballOther65 = 65,              //כוכב הפנטזי
        BetBasketballOther60 = 60,              //מאצאפ ריבאנדים
        BetBasketballOther50 = 50,              //מעל מתחת אסיסטים שחקן
        BetBasketballOther24 = 24,              //מעל מתחת ריבאונדים שחקן
        BetBasketballOther18 = 18,              //מעל מתחת נקודות שחקן
        BetBasketballOther19 = 19,              //מעל מתחת שלשות
        BetBasketballOther17 = 17,              //מצאפ נקודות
        BetBasketballOther59 = 59,              //נקודות אי זוגי זוגי
        BetBasketballOther23 = 23,              //סל ראשון
        BetBasketballOther74 = 74               //אבא ללא תוספת נקודות
        #endregion
    }
    public enum BetType
    {
        Home,
        Draw,
        Away
    }
    public enum IndexType
    {
        HomeLastHome,
        HomeLastDraw,
        HomeLastAway,
        GuestLastHome,
        GuestLastDraw,
        GuestLastAway,
        HomeLastExactHome,
        HomeLastExactDraw,
        HomeLastExactAway,
        GuestLastExactHome,
        GuestLastExactDraw,
        GuestLastExactAway,
        Head2HeadHome,
        Head2HeadDraw,
        Head2HeadAway,
        Head2HeadExactHome,
        Head2HeadExactDraw,
        Head2HeadExactAway
    }
    public enum BettingTipFilterType
    {
        Favorite,
        In3HoursAndFavorite,
        SingleHighRate,
        Filtered,
        NoFilter,
        WideFilter
    }
}
