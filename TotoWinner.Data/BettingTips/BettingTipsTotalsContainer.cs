﻿using System;
using System.Linq;

namespace TotoWinner.Data
{
    public class BettingTipsTotalsGamesContainer
    {
        public int Home { get; }
        public int HomeMomentum { get; }
        public decimal HomeWinPercent { get; }
        public decimal HomeMomentumWinPercent { get; }
        public int[] VerboseHome { get; }
        public int HomePowertip{ get; }

        public int Draw { get; }
        public int DrawMomentum { get; }
        public decimal DrawWinPercent { get; }
        public decimal DrawMomentumWinPercent { get; }
        public int[] VerboseDraw { get; }
        public int DrawPowertip { get; }

        public int Away { get; }
        public int AwayMomentum { get; }
        public decimal AwayWinPercent { get; }
        public decimal AwayMomentumWinPercent { get; }
        public int[] VerboseAway { get; }
        public int AwayPowertip { get; }

        public int HomeExact { get; }
        public int HomeExactMomentum { get; }
        public decimal HomeExactWinPercent { get; }
        public decimal HomeExactMomentumWinPercent { get; }
        public int[] VerboseExactHome { get; }
        public int HomeExactPowertip { get; }

        public int DrawExact { get; }
        public int DrawExactMomentum { get; }
        public decimal DrawExactWinPercent { get; }
        public decimal DrawExactMomentumWinPercent { get; }
        public int[] VerboseExactDraw { get; }
        public int DrawExactPowertip { get; }

        public int AwayExact { get; }
        public int AwayExactMomentum { get; }
        public decimal AwayExactWinPercent { get; }
        public decimal AwayExactMomentumWinPercent { get; }
        public int[] VerboseExactAway { get; }
        public int AwayExactPowertip { get; }

        public BettingTipsTotalsGamesContainer(int momentum, 
            int[] verboseHome, int[] verboseDraw, int[] verboseAway, 
            int[] verboseExactHome, int[] verboseExactDraw, int[] verboseExactAway)
        {
            this.Home = verboseHome.Sum();
            this.HomeMomentum = verboseHome.Take(momentum).Sum();
            this.VerboseHome = verboseHome;
            this.HomeWinPercent = verboseHome.Count() > 0 
                ? (decimal)this.Home / verboseHome.Count() * 100
                : 0;
            var actualMomentum = Math.Min(momentum, verboseHome.Take(momentum).Count());
            this.HomeMomentumWinPercent = (decimal)this.HomeMomentum / Math.Max(1,actualMomentum) * 100;
            this.HomePowertip = this.HomeWinPercent >= 50 && this.HomeMomentumWinPercent >= 75
                ? 1 
                : 0;

            this.Draw = verboseDraw.Sum();
            this.DrawMomentum = verboseDraw.Take(momentum).Sum();
            this.VerboseDraw = verboseDraw;
            this.DrawWinPercent = verboseDraw.Count() > 0
                ? (decimal)this.Draw / verboseDraw.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseDraw.Take(momentum).Count());
            this.DrawMomentumWinPercent = (decimal)this.DrawMomentum / Math.Max(1, actualMomentum) * 100;
            this.DrawPowertip = this.DrawWinPercent >= 50 && this.DrawMomentumWinPercent >= 75
                ? 1
                : 0;

            this.Away = verboseAway.Sum();
            this.AwayMomentum = verboseAway.Take(momentum).Sum();
            this.VerboseAway = verboseAway;
            this.AwayWinPercent = verboseAway.Count() > 0
                ? (decimal)this.Away / verboseAway.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseAway.Take(momentum).Count());
            this.AwayMomentumWinPercent = (decimal)this.AwayMomentum / Math.Max(1, actualMomentum) * 100;
            this.AwayPowertip = this.AwayWinPercent >= 50 && this.AwayMomentumWinPercent >= 75
                ? 1
                : 0;

            this.HomeExact = verboseExactHome.Sum();
            this.HomeExactMomentum = verboseExactHome.Take(momentum).Sum();
            this.VerboseExactHome = verboseExactHome;
            this.HomeExactWinPercent = verboseExactHome.Count() > 0
                ? (decimal)this.HomeExact / verboseExactHome.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseExactHome.Take(momentum).Count());
            this.HomeExactMomentumWinPercent = (decimal)this.HomeExactMomentum / Math.Max(1, actualMomentum) * 100;
            this.HomeExactPowertip = this.HomeExactWinPercent >= 50 && this.HomeExactMomentumWinPercent >= 75
                ? 1
                : 0;

            this.DrawExact = verboseExactDraw.Sum();
            this.DrawExactMomentum = verboseExactDraw.Take(momentum).Sum();
            this.VerboseExactDraw = verboseExactDraw;
            this.DrawExactWinPercent = verboseExactDraw.Count() > 0
                ? (decimal)this.DrawExact / verboseExactDraw.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseExactDraw.Take(momentum).Count());
            this.DrawExactMomentumWinPercent = (decimal)this.DrawExactMomentum / Math.Max(1, actualMomentum) * 100;
            this.DrawExactPowertip = this.DrawExactWinPercent >= 50 && this.DrawExactMomentumWinPercent >= 75
                ? 1
                : 0;

            this.AwayExact = verboseExactAway.Sum();
            this.AwayExactMomentum = verboseExactAway.Take(momentum).Sum();
            this.VerboseExactAway = verboseExactAway;
            this.AwayExactWinPercent = verboseExactAway.Count() > 0
                ? (decimal)this.AwayExact / verboseExactAway.Count() * 100
                : 0;
            actualMomentum = Math.Min(momentum, verboseExactAway.Take(momentum).Count());
            this.AwayExactMomentumWinPercent = (decimal)this.AwayExactMomentum / Math.Max(1, actualMomentum) * 100;
            this.AwayExactPowertip = this.AwayExactWinPercent >= 50 && this.AwayExactMomentumWinPercent >= 75
                ? 1
                : 0;
        }
    }
}
