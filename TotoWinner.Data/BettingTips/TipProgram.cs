﻿using System;

namespace TotoWinner.Data.BettingTips
{
    public class TipProgram
    {
        public long ProgramId { get; }
        public DateTime ProgramDate { get; }
        public TipProgram(long id, DateTime date)
        {
            this.ProgramId = id;
            this.ProgramDate = date;
        }
    }
}
