﻿using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BettingTipsLastGames
    {
        public Dictionary<int, List<BettingTipsGameInfo>> Games { get; }

        public BettingTipsLastGames(int homeId, int guestId, 
            List<BettingTipsGameInfo> homeGames, 
            List<BettingTipsGameInfo> guestGames)
        {
            this.Games = new Dictionary<int, List<BettingTipsGameInfo>>();
            this.Games.Add(homeId, homeGames);
            this.Games.Add(guestId, guestGames);
        }
    }
}