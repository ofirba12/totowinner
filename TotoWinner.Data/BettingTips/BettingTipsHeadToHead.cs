﻿using System;

namespace TotoWinner.Data
{
    public class BettingTipsHeadToHead
    {
        public DateTime Date { get; }
        public string Home { get; }
        public string Guest { get; }
        public int HomeScore { get; }
        public int GuestScore { get; }
        public string League { get; }

        public BettingTipsHeadToHead(DateTime date, string home, string guest, string score, string league)
        {
            try
            {
                this.Date = date;
                this.Home = home;
                this.Guest = guest;
                var scores = score.Split(':');
                this.HomeScore = int.Parse(scores[0]);
                this.GuestScore = int.Parse(scores[1]);
                this.League = league;
            }
            catch(Exception ex)
            {
                throw new Exception($"Error occured for date={date}, home={guest}, score={score}, league={league}", ex);
            }
        }
    }
}