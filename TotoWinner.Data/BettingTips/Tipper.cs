﻿
namespace TotoWinner.Data.BettingTips
{
    public class Tipper
    {
        public int TipperId { get; }
        public string TipperName { get; }
        public Tipper(int tipperId, string tipperName)
        {
            this.TipperId = tipperId;
            this.TipperName = tipperName;   
        }
    }
}
