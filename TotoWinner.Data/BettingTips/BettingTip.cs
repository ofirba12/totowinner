﻿namespace TotoWinner.Data
{
    public class BettingTip
    {
        public string Time { get; }
        public string Content { get; }
        public string Marking { get; }
        public string PrefixURL { get; }
        public string URL { get; }

        public BettingTip(string timeTip, string contentTip, string markingTip, string urlPrefixTip, string urlTip)
        {
            this.Time = timeTip;
            this.Content = contentTip;
            this.Marking = markingTip;
            this.PrefixURL = urlPrefixTip;
            this.URL = urlTip;
        }
        public override string ToString()
        {
            var tip = $"{Time} {Content} {Marking}; {PrefixURL}{URL}";
            return tip;
        }
    }
}
