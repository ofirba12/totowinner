﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class BuildTipRequest
    {
        public SportIds Sport { get; }
        public DateTime GameStart { get; }
        public int GameIndex { get; }
        public int TotoId { get; }
        public string HomeTeam { get; }
        public string GuestTeam { get; }
        public string League { get; }
        public int TotalGames { get; }
        public decimal WinPercent { get; }
        public decimal MomentumWinPercent { get; }
        public int VerboseTotal { get; }
        public int VerboseMomentumTotal { get; }
        public int Momentum { get; }
        public IndexType? IndexType { get; }
        public BetType BetType { get; }
        public FileType FileType { get; }
        public string ExtraPointsString { get; set; }
        public BettingTipsParentEvent EventData { get; }

        public BuildTipRequest(BettingTipsParentEvent eventData,
            SportIds sport, 
            DateTime gameStart, 
            int gameIndex,
            int totoId,
            string homeTeam, 
            string guestTeam, 
            string league, 
            int totalGames, 
            decimal winPercent, 
            decimal momentumWinPercent, 
            int verboseTotal, 
            int verboseMomentumTotal,
            int momentum,
            string extraPointsString,
            IndexType? indexType,
            BetType betType,
            FileType fileType)
        {
            this.Sport = sport;
            this.GameStart = gameStart;
            this.GameIndex = gameIndex;
            this.TotoId = totoId;
            this.HomeTeam = homeTeam;
            this.GuestTeam = guestTeam;
            this.League = league;
            this.TotalGames = totalGames;
            this.WinPercent = winPercent;
            this.MomentumWinPercent = momentumWinPercent;
            this.VerboseTotal = verboseTotal;
            this.VerboseMomentumTotal = verboseMomentumTotal;
            this.Momentum = momentum;
            this.ExtraPointsString = extraPointsString;
            this.IndexType = indexType;
            this.BetType = betType;
            this.FileType = fileType;
            this.EventData = eventData;
        }
    }
}
