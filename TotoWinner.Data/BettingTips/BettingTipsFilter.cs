﻿using System;

namespace TotoWinner.Data
{
    public class BettingTipsFilter
    {
        public decimal? MaxRate { get;}
        public decimal? MinRate { get; }
        public BettingTipFilterType FilterType { get; }

        public BettingTipsFilter(BettingTipFilterType filterType)
        {
            this.FilterType = filterType;
            switch (filterType)
            {
                case BettingTipFilterType.In3HoursAndFavorite:
                case BettingTipFilterType.Favorite:
                    this.MinRate = (decimal)1.5;
                    this.MaxRate = (decimal)2.1;
                    break;
                case BettingTipFilterType.SingleHighRate:
                    this.MinRate = 2;
                    this.MaxRate = 100;
                    break;
                case BettingTipFilterType.NoFilter:
                case BettingTipFilterType.Filtered:
                    this.MinRate = (decimal)1.5;
                    this.MaxRate = (decimal)2.2;
                    break;
                case BettingTipFilterType.WideFilter:
                    this.MinRate = (decimal)1;
                    this.MaxRate = (decimal)100;
                    break;
                default:
                    throw new Exception($"betting tip filter type {filterType} for this constuctor is not valid");
            }
        }
    }
}
