﻿using System;

namespace TotoWinner.Data
{
    public class BettingTipTotalColumn : IEquatable<BettingTipTotalColumn>
    {
        public IndexType IndexType { get; }
        public decimal WinPercent { get; }
        public decimal WinMomentumPercent { get; }
        public BettingTipTotalColumn(IndexType type, decimal winPercent, decimal winExactPercent)
        {
            this.IndexType = type;
            this.WinPercent = winPercent;
            this.WinMomentumPercent = winExactPercent;
        }

        public override int GetHashCode()
        {
            return (int)(this.WinPercent + this.WinMomentumPercent) * (1 + (int)this.IndexType);
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as BettingTipTotalColumn);
        }
        public bool Equals(BettingTipTotalColumn other)
        {
            return other != null && 
                other.WinPercent == this.WinPercent &&
                other.WinMomentumPercent == this.WinMomentumPercent &&
                other.IndexType == this.IndexType;
        }
    }
}
