﻿namespace TotoWinner.Data
{
    public class BettingTipsTotalsRepository
    {
        public BettingTipsTotalsGamesContainer HomeLastGameTotals { get; }
        public BettingTipsTotalsGamesContainer GuestLastGameTotals { get; }
        public BettingTipsTotalsGamesContainer Head2HeadTotals { get; }
        public int HomePowertip { get; }
        public int DrawPowertip { get; }
        public int AwayPowertip { get; }

        public BettingTipsTotalsRepository(BettingTipsTotalsGamesContainer homeLastGameTotals,
            BettingTipsTotalsGamesContainer guestLastGameTotals,
            BettingTipsTotalsGamesContainer head2HeadTotals)
        {
            this.HomeLastGameTotals = homeLastGameTotals;
            this.GuestLastGameTotals = guestLastGameTotals;
            this.Head2HeadTotals = head2HeadTotals;

            this.HomePowertip = this.HomeLastGameTotals.HomePowertip +
                this.GuestLastGameTotals.HomePowertip +
                this.HomeLastGameTotals.HomeExactPowertip +
                this.GuestLastGameTotals.HomeExactPowertip +
                this.Head2HeadTotals.HomePowertip +
                this.Head2HeadTotals.HomeExactPowertip;
            this.DrawPowertip = this.HomeLastGameTotals.DrawPowertip +
                this.GuestLastGameTotals.DrawPowertip +
                this.HomeLastGameTotals.DrawExactPowertip +
                this.GuestLastGameTotals.DrawExactPowertip +
                this.Head2HeadTotals.DrawPowertip +
                this.Head2HeadTotals.DrawExactPowertip;
            this.AwayPowertip = this.HomeLastGameTotals.AwayPowertip +
                this.GuestLastGameTotals.AwayPowertip +
                this.HomeLastGameTotals.AwayExactPowertip +
                this.GuestLastGameTotals.AwayExactPowertip +
                this.Head2HeadTotals.AwayPowertip +
                this.Head2HeadTotals.AwayExactPowertip;
        }
    }
}
