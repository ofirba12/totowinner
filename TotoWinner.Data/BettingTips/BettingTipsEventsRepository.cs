﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class BettingTipsEventsRepository
    {
        public Dictionary<int, BettingTipsParentEvent> Events { get; }
        public List<string> SkippedEventsInfo { get; }
        public DateTime ProgramDate { get; }
        public long RoundId { get; }

        public BettingTipsEventsRepository(Dictionary<int, BettingTipsParentEvent> events, 
            List<string> skippedEventsInfo,
            long roundId, 
            DateTime winnerLineDate)
        {
            this.Events = events;
            this.SkippedEventsInfo = skippedEventsInfo;
            this.RoundId = roundId;
            this.ProgramDate = winnerLineDate;
        }
    }
}
