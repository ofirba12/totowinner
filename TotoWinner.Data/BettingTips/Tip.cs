﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class Tip
    {
        public int TipId { get; }
        public long ProgramId { get; }
        public int GameIndex { get; }
        public SportIds Sport { get; }
        public int TotoId { get; }
        public DateTime GameStart { get; }
        public bool IsAutomaticTip { get; }
        public int PowerTip { get; }
        public string BetName { get; }
        public string BetLeague { get; }
        public string Country { get; }
        public bool IsParentBet { get; }
        public char BetMark { get; }
        public bool IsSingle { get; }
        public decimal BetRate { get; }
        public string Status { get; }
        public string TipTime { get; }
        public string TipContent { get; }
        public string TipMarks { get; }
        public string TipUrl { get; }
        public string TipOfficialResult { get; set; }
        public string TipOfficialMark { get; set; }
        public string TipManualResult { get; set; }
        public string TipManualMark { get; set; }
        public string Bankerim { get; }
        public bool IsActive { get; }
        public int TipperId { get; }

        public Tip(int tipId, long programId, int powertip, SportIds sport, int totoId, string country, int gameIndex, DateTime gameStart, bool isAutomaticTip,
            string betName, string betLeague, bool isFather, bool isSingle, char betMark, decimal betRate,
            string tipTime, string tipContent, string tipMarking, string tipUrl,
            string tipOfficialResult, string tipOfficialMark, string tipManualResult, string tipManualMark, bool isActive, int tipperId)
        {
            this.TipId = tipId;
            this.ProgramId = programId;
            this.PowerTip = powertip;
            this.Sport = sport;
            this.TotoId = totoId;
            this.Country = country;
            this.GameIndex = gameIndex;
            this.GameStart = gameStart;
            this.IsAutomaticTip = isAutomaticTip;
            TimeSpan span = (this.GameStart - DateTime.Now);
            if (span.TotalMinutes > 10)
                this.Status = "פתוח";
            else if (span.TotalMinutes <= 10)
                this.Status = "סגור";

            this.BetName = betName;
            this.BetLeague = betLeague;
            this.IsParentBet = isFather;
            this.IsSingle = isSingle;
            this.BetMark = betMark;
            this.BetRate = betRate;

            this.TipTime = tipTime;
            this.TipContent = tipContent;
            this.TipMarks = tipMarking;
            this.TipUrl = tipUrl;

            this.TipOfficialResult = tipOfficialResult;
            this.TipOfficialMark = tipOfficialMark;
            this.TipManualResult = tipManualResult;
            this.TipManualMark = tipManualMark;

            this.IsActive = isActive;

            this.Bankerim = $"מאת אתר בנקרים.{Environment.NewLine}{betName} במסגרת {betLeague}.{Environment.NewLine}{tipContent}{Environment.NewLine}{tipMarking}";
            this.TipperId = tipperId;
        }

        public Tip(int tipId, long programId, int powertip, SportIds sport, int totoId, string country, int gameIndex, DateTime gameStart, bool isAutomaticTip,
            string betStatus, string betName, string betLeague, bool isFather, bool isSingle, char betMark, decimal betRate, 
            string tipTime, string tipContent, string tipMarking, string tipUrl,
            string tipOfficialResult, string tipOfficialMark, string tipManualResult, string tipManualMark, bool isActive, int tipperId) : this(
                tipId,
                programId,
                powertip,
                sport,
                totoId, 
                country, 
                gameIndex, 
                gameStart, 
                isAutomaticTip, 
                betName, 
                betLeague, 
                isFather, 
                isSingle, 
                betMark,
                betRate,
                tipTime, 
                tipContent, 
                tipMarking, 
                tipUrl,
                tipOfficialResult, 
                tipOfficialMark, 
                tipManualResult, 
                tipManualMark,
                isActive,
                tipperId
                )
        {
            this.Status = betStatus;
        }
    }
}
