﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class BettingTipsParentEvent
    {
        public int EventId { get; }
        public int TotoId { get; }
        public SportIds SportId { get; }
        public long RoundId { get; }
        public int Place { get; }
        public string Country { get; }
        public string LeagueName { get; }
        public bool IsFather { get; }
        public DateTime Time { get; }
        public string Status { get; }
        public int HomeId { get; }
        public string HomeTeamName { get; }
        public int GuestId { get; }
        public string GuestTeamName { get; }
        public BettingTipsBet Bet { get; }
        public BettingTipsLastGames LastGames { get; }
        public List<BettingTipsHeadToHead> Head2Head { get; }
        public string GameResult { get; }

        public BettingTipsParentEvent(
            int eventId,
            int totoId,
            int sportId,
            long roundId,
            int place,
            string country,
            string leagueName,
            bool isFather,
            DateTime time,
            string status,
            int homeId,
            string homeTeamName,
            int guestId,
            string guestTeamName,
            BettingTipsBet bet,
            BettingTipsLastGames lastGames,
            List<BettingTipsHeadToHead> h2h,
            string gameResult)
        {
            this.EventId = eventId;
            this.TotoId = totoId;
            this.SportId = (SportIds)sportId;
            this.RoundId = roundId;
            this.Place = place;
            this.Country = country;
            this.LeagueName = leagueName;
            this.IsFather = isFather;
            this.Time = time;
            this.Status = status;
            this.HomeId = homeId;
            this.HomeTeamName = homeTeamName;
            this.GuestId = guestId;
            this.GuestTeamName = guestTeamName;
            this.Bet = bet;
            this.LastGames = lastGames;
            this.Head2Head = h2h;
            this.GameResult = gameResult;
        }
    }
}