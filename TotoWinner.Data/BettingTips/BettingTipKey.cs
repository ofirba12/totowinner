﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class BettingTipKey: IEquatable<BettingTipKey>
    {
        public int EventId { get; }
        public BetType Type { get; }

        public BettingTipKey(int eventId, BetType type)
        {
            this.EventId = eventId;
            this.Type = type;
        }

        public override int GetHashCode()
        {
            return this.EventId * (1 + (int)this.Type);
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as BettingTipKey);
        }
        public bool Equals(BettingTipKey other)
        {
            return other != null && other.EventId == this.EventId && other.Type == this.Type;
        }
    }
}
