﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Data
{
    public class BettingTipsStacks
    {
        public Dictionary<int, BettingTipsTotalsRepository> EventTotalData { get; }
        public Dictionary<int, List<BettingTipKey>> PowertipStacks { get; }
        public Dictionary<BettingTipKey, BettingTip> TipDescription { get; }
        public List<string> Errors { get; }

        public BettingTipsStacks(Dictionary<int, BettingTipsTotalsRepository> eventTotalData, 
            Dictionary<int, List<BettingTipKey>> powertipStacks, 
            Dictionary<BettingTipKey, BettingTip> tipDescription,
            List<string> errors)
        {
            this.EventTotalData = eventTotalData;
            this.PowertipStacks = powertipStacks;
            this.TipDescription = tipDescription;
            this.Errors = errors;
        }
    }
}
