﻿namespace TotoWinner.Data
{
    public class BettingTipsBet
    {
        public int BetTypeId { get; }
        public string BetTypeName { get; }
        public decimal Home { get; }
        public decimal Draw { get; }
        public decimal Away { get; }
        public string Name { get; }
        public string Period { get; }
        public decimal? BarrierHome { get; }
        public decimal? BarrierGuest { get; }
        public bool Single { get; }
        public bool Double { get; }
        public bool FinalHomeWin { get; }
        public bool FinalDraw { get; }
        public bool FinalAwayWin { get; }

        public BettingTipsBet(bool isSingle, 
            bool isDouble,
            int typeId,
            string typeName,
            decimal homeBet, 
            decimal drawBet, 
            decimal awayBet, 
            string name, 
            string period, 
            decimal? barrierHome, 
            decimal? barrierGuest,
            bool finalHomeWin,
            bool finalDraw,
            bool finalAwayWin)
        {
            this.Single = isSingle;
            this.Double = isDouble;
            this.BetTypeId= typeId;
            this.BetTypeName = typeName;
            this.Home = homeBet;
            this.Draw = drawBet;
            this.Away = awayBet;
            this.Name = name;
            this.Period = period;
            this.BarrierHome = barrierHome;
            this.BarrierGuest = barrierGuest;
            this.FinalHomeWin = finalHomeWin;
            this.FinalDraw = finalDraw;
            this.FinalAwayWin = finalAwayWin;
        }
    }
}