﻿using System;

namespace TotoWinner.Data
{
    public class MonitorTip
    {
        public int TipId { get; }
        public long ProgramId { get; }
        public int GameIndex { get; }
        public int Counter { get; }
        public SportIds Sport { get; }
        public int TotoId { get; }
        public DateTime GameStart { get; }
        public bool IsAutomaticTip { get; }
        public int PowerTip { get; }
        public string BetName { get; }
        public string BetLeague { get; }
        public string Country { get; }
        public bool IsParentBet { get; }
        public char BetMark { get; }
        public bool IsSingle { get; }
        public decimal BetRate { get; }
        public string Status { get; }
        public string TipTime { get; }
        public string TipContent { get; }
        public string TipMarks { get; }
        public string TipUrl { get; }
        public string TipOfficialResult { get; }
        public string TipOfficialMark { get; }
        public string TipManualResult { get; }
        public string TipManualMark { get; }
        public string Bankerim { get; }
        public bool IsActive { get; }
        public string Tipper { get; }

        public MonitorTip(Tip tip, string tipperName, int counter)
        {
            this.TipId = tip.TipId;
            this.ProgramId = tip.ProgramId;
            this.GameIndex = tip.GameIndex;
            this.Counter = counter;
            this.Sport = tip.Sport;
            this.TotoId = tip.TotoId;
            this.GameStart = tip.GameStart;
            this.IsAutomaticTip = tip.IsAutomaticTip;
            this.PowerTip = tip.PowerTip;
            this.BetName = tip.BetName;
            this.BetLeague = tip.BetLeague;
            this.Country = tip.Country;
            this.IsParentBet = tip.IsParentBet;
            this.BetMark = tip.BetMark;
            this.IsSingle = tip.IsSingle;
            this.BetRate = tip.BetRate;
            this.Status = tip.Status;
            this.TipTime = tip.TipTime;
            this.TipContent = tip.TipContent;
            this.TipMarks = tip.TipMarks;
            this.TipUrl = tip.TipUrl;
            this.TipOfficialResult = tip.TipOfficialResult;
            this.TipOfficialMark = tip.TipOfficialMark;
            this.TipManualResult = tip.TipManualResult;
            this.TipManualMark = tip.TipManualMark;
            this.Bankerim = tip.Bankerim;
            this.IsActive = tip.IsActive;
            this.Tipper = tipperName;
        }
    }
}
