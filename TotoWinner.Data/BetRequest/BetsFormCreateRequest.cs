﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotoWinner.Data
{
    public class BetsFormCreateRequest
    {
        public int FormId { get; set; }
        public List<RatesLine> EventsRates { get; set; }
        public List<Bet> EventsBets { get; set; }
        public FormSettings FormSettings { get; set; }
        public bool CleanOnDiff { get; set; }
        public bool GenerateOutputFile { get; set; }

        public void Validate()
        {
            if (EventsRates.Count != EventsBets.Count)
                throw new Exception("Number of EventsRatesList not equal to EventsBets");

            for (int position = 0; position < EventsRates.Count; position++)
            {
                var eventRate = EventsRates.FirstOrDefault(er => er.position == position);
                if (eventRate == null) throw new Exception("Event Rate " + position + " is missing");
                eventRate.Validate();

                var eventBet = EventsBets.FirstOrDefault(eb => eb.position == position);
                if (eventBet == null) throw new Exception("Event Bet " + position + " is missing");
                eventBet.Validate();

            }

            FormSettings.Validate(EventsRates.Count);
        }
    }
}
