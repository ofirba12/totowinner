﻿using System;

namespace TotoWinner.Data
{
    public class IncludeSettings
    {
        public MinMax amountSum { get; set; }
        public MinMax amount1 { get; set; }
        public MinMax amountX { get; set; }
        public MinMax amount2 { get; set; }
        public MinMax sequence1 { get; set; }
        public MinMax sequenceX { get; set; }
        public MinMax sequence2 { get; set; }
        public MinMax sequenceBreak { get; set; }


        internal void Validate(int numOfMembers)
        {
            amountSum.Validate("MinMaxSettings-AmountSum");

            amount1.Validate("MinMaxSettings-Amount1");
            amountX.Validate("MinMaxSettings-AmountX");
            amount2.Validate("MinMaxSettings-Amount2");

            sequenceBreak.Validate("MinMaxSettings-SequenceBreak");

            sequence1.Validate("MinMaxSettings-Sequence1");
            sequenceX.Validate("MinMaxSettings-SequenceX");
            sequence2.Validate("MinMaxSettings-Sequence2");

            if (amountSum.Min < numOfMembers || amountSum.Max > 100)
                throw new Exception("AmountSum should be between " + numOfMembers + " and 100");

            ValidateAmount(amount1, numOfMembers, "MinMaxSettings-Amount1");
            ValidateAmount(amountX, numOfMembers, "MinMaxSettings-AmountX");
            ValidateAmount(amount2, numOfMembers, "MinMaxSettings-Amount2");

            ValidateAmount(sequence1, numOfMembers, "MinMaxSettings-Sequence1");
            ValidateAmount(sequenceX, numOfMembers, "MinMaxSettings-SequenceX");
            ValidateAmount(sequence2, numOfMembers, "MinMaxSettings-Sequence2");

            ValidateAmount(sequenceBreak, numOfMembers - 1, "MinMaxSettings-SequenceBreak");
        }

        private void ValidateAmount(MinMax propertyToCheck, int numOfMembers, string paramName)
        {
            if (propertyToCheck.Max > numOfMembers)
                throw new Exception("The " + paramName + " should be between 0 and " + numOfMembers);
        }
    }
}
