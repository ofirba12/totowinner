﻿using System;
using System.Text.RegularExpressions;

namespace TotoWinner.Data
{
    public class ShapesSettings
    {
        public MinMax[] Combinations { get; private set; }
        public MinMax MinMax { get; private set; }
        public bool IsActive { get; private set; }
        public ShapeFilterType Type { get; private set; }

        public ShapesSettings(MinMax[] combinations, MinMax minMax, bool isActive, ShapeFilterType type)
        {
            try
            {
                var length = 0;
                var maxConstraintValue = 0;
                var maxMatchesValue = 0;
                switch (type)
                {
                    case ShapeFilterType.RegularShapes2:
                    case ShapeFilterType.LetterShapes2:
                        maxConstraintValue = 15;
                        length = maxMatchesValue= 9;
                        break;
                    case ShapeFilterType.LetterShapes3:
                    case ShapeFilterType.RegularShapes3:
                        maxConstraintValue = 14;
                        length = maxMatchesValue = 27;
                        break;
                }
                if (combinations.Length > length)
                    throw new Exception($"The specified combination item of type {type} has length {combinations.Length}. it should be {length}");
                if (!(minMax.Min >= 0 && minMax.Max <= maxMatchesValue))
                    throw new Exception($"The specified {minMax.ToString()} {type} should be between 0 and {maxMatchesValue}");

                for (int i = 0; i < length; i++)
                {
                    combinations[i] = new MinMax(combinations[i].Min, combinations[i].Max, 
                        0, (uint)maxConstraintValue);
                }
                this.Combinations = combinations;
                this.IsActive = isActive;
                this.MinMax = minMax;
                this.Type = type;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ShapesSettings)} with combination type {type}", ex);
            }
        }
        public void SetMinMax(MinMax minMax)
        {
            if (minMax == null)
                throw new Exception("Can set null into minMax");
            this.MinMax = minMax;
        }
    }
}
