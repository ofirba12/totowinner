﻿using System;

namespace TotoWinner.Data
{
    public class Bet
    {
        public int position { get; set; }
        public bool bet1 { get; set; }
        public bool betX { get; set; }
        public bool bet2 { get; set; }

        internal void Validate()
        {
            if (!bet1 && !betX && !bet2) throw new 
                Exception("Bet " + position + ": at leas one option should be selected (true)");
        }
        public Bet() { }
        public Bet(int pos, bool b1, bool bx, bool b2)
        {
            this.position = pos;
            this.bet1 = b1;
            this.betX = bx;
            this.bet2 = b2;
        }
        public Bet(Bet b)
        {
            this.position = b.position;
            this.bet1 = b.bet1;
            this.betX = b.betX;
            this.bet2 = b.bet2;
        }
        public override string ToString()
        {
            var str = (this.bet1 ? "1" : "") + (this.betX ? "X" : "") + (this.bet2 ? "2" : "");
            return str;
        }
    }
}
