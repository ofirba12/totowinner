﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class CombinationLettersSettings
    {
        public List<CombinationSetting> LettersSettings { get; set; }
        public MinMax MinMaxA { get; set; }
        public MinMax MinMaxB { get; set; }
        public MinMax MinMaxC { get; set; }
        public MinMax MinMaxSequenceLengthA { get; set; }
        public MinMax MinMaxSequenceLengthB { get; set; }
        public MinMax MinMaxSequenceLengthC { get; set; }
        public MinMax MinMaxBreak { get; set; }
        public CombinationLettersSettings()
        {
            this.LettersSettings = new List<CombinationSetting>();
        }

        public CombinationLettersSettings(List<CombinationSetting> settings,
            MinMax minMaxA, MinMax minMaxB, MinMax minMaxC,
            MinMax minMaxSequenceLengthA, MinMax minMaxSequenceLengthB, MinMax minMaxSequenceLengthC,
            MinMax minMaxBreak)
        {
            try
            {
                if (settings == null)// || settings.Count == 0)
                    settings = new List<CombinationSetting>();
                this.LettersSettings = settings;
                this.MinMaxA = minMaxA;
                this.MinMaxB = minMaxB;
                this.MinMaxC = minMaxC;
                this.MinMaxSequenceLengthA = minMaxSequenceLengthA;
                this.MinMaxSequenceLengthB = minMaxSequenceLengthB;
                this.MinMaxSequenceLengthC = minMaxSequenceLengthC;
                this.MinMaxBreak = minMaxBreak;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(CombinationLettersSettings)}", ex);
            }

        }
    }
}
