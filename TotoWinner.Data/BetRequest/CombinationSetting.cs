﻿using System;
using System.Text.RegularExpressions;

namespace TotoWinner.Data
{
    public class CombinationSetting
    {
        public string[] Combinations { get; private set; }
        public MinMax MinMax { get; private set; }
        public bool IsActive { get; private set; }
        public CombinationType Type { get; private set; }

        public CombinationSetting(string[] combinations, MinMax minMax, bool isActive, CombinationType type)
        {
            try
            {
                if (combinations.Length > 16)
                    throw new Exception($"The specified combination item has length {combinations.Length}. it should be 16");
                if (!(minMax.Min >= 0 && minMax.Max <= 16))
                    throw new Exception($"The specified {minMax.ToString()} CombinationSetting-MinMax should be >=0 and <=16");
                Regex strPattern = null;
                if (type == CombinationType.Pattern1X2)
                    strPattern = new Regex(@"\b(1|x|X|2|x1|X1|2x|2X|21|2x1|2X1)\b");
                else if (type == CombinationType.PatternLetters)
                    strPattern = new Regex(@"\b(A|B|C|AB|AC|BC|ABC)\b");
                else
                    throw new Exception($"Combination Type {type} is not supported");
                for (int i = 0; i < combinations.Length; i++)
                {
                    if (!string.IsNullOrEmpty(combinations[i]) && !strPattern.IsMatch(combinations[i]))
                    {
                        throw new Exception(string.Format($"The value {combinations[i]} in filters options for type {type} index {i + 1} is wrong, valid values can be {strPattern.ToString()}")); 
                    }
                }
                this.Combinations = combinations;
                this.IsActive = isActive;
                this.MinMax = minMax;
                this.Type = type;
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(CombinationSetting)} with combination type {type}", ex);
            }
        }
        public void SetMinMax(MinMax minMax)
        {
            if (minMax == null)
                throw new Exception("Can set null into minMax");
            this.MinMax = minMax;
        }
    }
}
