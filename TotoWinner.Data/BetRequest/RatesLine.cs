﻿using System;

namespace TotoWinner.Data
{
    public class RatesLine
    {
        public int position { get; set; }
        public float rate1 { get; set; }
        public float rateX { get; set; }
        public float rate2 { get; set; }

        internal void Validate()
        {
            if (!(rate1 > 0.99 && rateX > 0.99 && rate2 > 0.99))
                throw new Exception("RatesLine " + position + ": all rates should be greater then 0.99");
        }
    }
}
