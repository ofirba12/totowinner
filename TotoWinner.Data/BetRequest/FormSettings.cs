﻿using System;
using System.Collections.Generic;

namespace TotoWinner.Data
{
    public class FormSettings
    {
        public IncludeSettings IncludeSettings { get; set; }
        public ExcludeSettings ExcludeSettings { get; set; }
        public List<CombinationSetting> Combination1X2Settings { get; set; }
        public CombinationLettersSettings CombinationLettersSettings { get; set; }
        public List<ShapesSettings> Shapes2 { get; set; }
        public List<ShapesSettings> Shapes3 { get; set; }
        public List<ShapesSettings> LettersShapes2 { get; set; }
        public List<ShapesSettings> LettersShapes3 { get; set; }

        internal void Validate(int numOfMembers)
        {
            IncludeSettings?.Validate(numOfMembers);
            ExcludeSettings?.Validate(numOfMembers);
        }
    }
}
