﻿using System;

namespace TotoWinner.Data
{
    public class MinMax
    {
        public decimal Min { get; set; }
        public decimal Max { get; set; }

        public MinMax()
        {
            Min = 0;
            Max = 0;
        }
        public MinMax(decimal min, decimal max)
        {
            try
            {
                if (min > max)
                    throw new Exception("The specified Max should be greater then or equal to Min");
                Min = min;
                Max = max;
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(MinMax)} from min={min}, max={max}", ex);
            }
        }
        public MinMax(decimal min, decimal max, decimal minValidValue, decimal maxValidValue) : this(min, max)
        {
            try
            {
                if (min < minValidValue)
                    throw new Exception("The specified Min should be greater equal to {minValidValue}");
                if (max > maxValidValue)
                    throw new Exception("The specified Max should be less equal to {maxValidValue}");
            }
            catch(Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(MinMax)} from min={min}, max={max}, minValidValue={minValidValue}, maxValidValue={maxValidValue}", ex);
            }
        }
        internal void Validate(string paramName)
        {
            if (Min > Max)
                throw new Exception(paramName + ": Max should be greater then or equal to Min");
        }
        public override string ToString()
        {
            return $"Min={Min};Max={Max}";
        }
    }
}
