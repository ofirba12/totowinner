﻿using System;

namespace TotoWinner.Data
{
    public class ExcludeSettings
    {
        public MinMax amountSum { get; set; }
        public MinMax amount1 { get; set; }
        public MinMax amountX { get; set; }
        public MinMax amount2 { get; set; }
        public uint sequence1 { get; set; }
        public uint sequenceX { get; set; }
        public uint sequence2 { get; set; }
        public MinMax sequenceBreak { get; set; }
        //SHOULD I ACTIVATE IT BY CHECKING VALUES ONLY
        public bool ActivateSumAmountFilter { get; set; }
        public bool ActivateOneAmountFilter { get; set; }
        public bool ActivateXAmountFilter { get; set; }
        public bool ActivateTwoAmountFilter { get; set; }
        public bool ActivateOneSequenceFilter { get; set; }
        public bool ActivateXSequenceFilter { get; set; }
        public bool ActivateTwoSequenceFilter { get; set; }
        public bool ActivateBreakFilter { get; set; }

        public ExcludeSettings()
        { }

        //public ExcludeSettings(uint numberOfEvent, MinMax amountSum)
        //{
        //    try
        //    {
        //        this.AmountSum = new MinMax(amountSum.Min, amountSum.Max);
        //        //sequence1 --- numberOfEvent);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new System.Exception($"An error occured trying to construct type {typeof(ExcludeSettings)} from amountSum={amountSum}", ex);
        //    }
        //}
        internal void Validate(int numOfMembers)
        {
            amountSum.Validate("MinMaxSettings-AmountSum");

            amount1.Validate("MinMaxSettings-Amount1");
            amountX.Validate("MinMaxSettings-AmountX");
            amount2.Validate("MinMaxSettings-Amount2");

            if (sequence1 > 16)
                throw new Exception($"The specified sequence1 value: {sequence1} should be between 0 and 16");
            if (sequenceX > 16)
                throw new Exception($"The specified sequence1 value: {sequenceX} should be between 0 and 16");
            if (sequence2 > 16)
                throw new Exception($"The specified sequence1 value: {sequence2} should be between 0 and 16");
            sequenceBreak.Validate("MinMaxSettings-SequenceBreak");

            //if (amountSum.min < numOfMembers || amountSum.max > 100)
            //    throw new Exception("AmountSum should be between " + numOfMembers + " and 100");

            //ValidateAmount(amount1, numOfMembers, "MinMaxSettings-Amount1");
            //ValidateAmount(amountX, numOfMembers, "MinMaxSettings-AmountX");
            //ValidateAmount(amount2, numOfMembers, "MinMaxSettings-Amount2");

            //ValidateAmount(sequenceBreak, numOfMembers - 1, "MinMaxSettings-SequenceBreak");
        }

        private void ValidateAmount(MinMax propertyToCheck, int numOfMembers, string paramName)
        {
            if (propertyToCheck.Max > numOfMembers)
                throw new Exception("The " + paramName + " should be between 0 and " + numOfMembers);
        }
    }
}
