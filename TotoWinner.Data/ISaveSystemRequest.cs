﻿namespace TotoWinner.Data
{
    public interface ISaveSystemRequest
    {
        string Name { get; set; }
        string UniqueId { get; set; }
    }
}
