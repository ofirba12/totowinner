﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Generator
{
    internal static class GoldserveGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger("GoldserveGenerator");
        private static GoalServeProviderServices _goalServeSrv = GoalServeProviderServices.Instance;
        private static Dictionary<ExecutionType, BgGoalServeExecution> executions = new Dictionary<ExecutionType, BgGoalServeExecution>();
        private static void LogAndDump(string msg)
        {
            _log.Info(msg);
            Console.WriteLine(msg);
        }

        internal static void Prerequisite(GoalServeSportType sport, bool execute)
        {
            try
            {
                _goalServeSrv.ResetDumper();
                for (var dDay = -1 /* Yesterday */; dDay < 3; dDay++)
                {
                    try
                    {
                        var execution = new ExecutionType(sport, (GoalServeMatchDayFetch)dDay);
                        executions.Add(execution, new BgGoalServeExecution(sport, (GoalServeMatchDayFetch)dDay));
                        if (execute)
                            executions[execution].Execute();
                    }
                    catch(Exception ex)
                    {
                        _log.Error($"Error occurred prerquiste: {sport} {(GoalServeMatchDayFetch)dDay}, execution will be skipped", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error($"Failed to execute prerequisite for sport {sport}", ex);
            }
        }
        internal static int Executes()
        {
            var toExecute = new List<BgGoalServeExecution>();
            foreach (var exe in executions.Values)
            {
                if (exe.CanExecute())
                    toExecute.Add(exe);
            }
            if (toExecute.Count > 0)
                _goalServeSrv.ResetDumper();
            foreach (var exe in toExecute)
                exe.Execute();
            return toExecute.Count();
        }
        internal static BgGoalServeRepository GenerateSportData(BgGoalServeFetchRequest fetchRequest)
        {
            Console.ForegroundColor = ConsoleColor.White;
            LogAndDump($"Fetching {fetchRequest.Sport} matches data for {fetchRequest.Type}:{fetchRequest.Date.ToShortDateString()}...{DateTime.Now}");
            var json = _goalServeSrv.FetchRawData(fetchRequest);
            LogAndDump($"Fetching {fetchRequest.Type} odds and parsing Data...");
            var skippedInfo = new List<string>();
            var repository = _goalServeSrv.ParseCategories(json, fetchRequest, skippedInfo);
            if (skippedInfo.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                LogAndDump($"{fetchRequest.Type}: Parsing process finished successfully");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                LogAndDump($"{fetchRequest.Type}: Parsing process finished successfully with some issues");
                foreach (var issue in skippedInfo)
                    _log.Warn(issue);
            }
            _goalServeSrv.DumpToCSV(repository, fetchRequest);
            Console.ForegroundColor = ConsoleColor.Green;
            LogAndDump($"{fetchRequest.Type}: CSV file created successfully");
            var persistanceFeedType = _goalServeSrv.GetPersistanceMatches(fetchRequest.Sport, repository.Matches.Keys.Select(i => i.MatchId).ToList());
            _goalServeSrv.AddOrUpdate(repository, fetchRequest.Sport, persistanceFeedType);
            LogAndDump($"{fetchRequest.Type}: Updated Database successfully. {DateTime.Now}");

            return repository;
        }
    }
}
