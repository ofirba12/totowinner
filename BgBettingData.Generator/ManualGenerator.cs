﻿using log4net;
using System;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Generator
{
    internal static class ManualGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger("ManualGenerator");
        private static void LogAndDump(string msg)
        {
            _log.Info(msg);
            Console.WriteLine(msg);
        }

        internal static void Executes()
        {
            Console.ForegroundColor = ConsoleColor.White;
            var starting = DateTime.Now;
            var systemRepository = BgSystemServices.Instance.GetBgSystemRepository();
            BgSystemServices.Instance.SetSystemField(BgSystemField.ManualProviderStartGeneration,starting);
            LogAndDump($"Fetching manual matches marked for syncing...{starting}");
            var fetchTime = systemRepository.ManualProviderStartGeneration ?? (DateTime?)null;
            var matches = ManualProviderServices.Instance.FetchManualMatchesForProccessing(fetchTime); //take last update from bgSystem
            LogAndDump($"update mapping for {matches.Count} manual matches");
            BgMapperServices.Instance.AddOrUpdateMapping(matches);
            ManualProviderServices.Instance.UpdateManualFeedWithBgIds(matches);
            LogAndDump($"update completed");
            BgSystemServices.Instance.SetSystemField(BgSystemField.ManualProviderFinishGeneration, DateTime.Now);
        }
    }
}
