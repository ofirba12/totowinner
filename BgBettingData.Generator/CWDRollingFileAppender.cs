﻿using log4net.Appender;
using System.IO;

namespace BgBettingData.Generator
{
    public class CWDRollingFileAppender : RollingFileAppender
    {
        public override string File
        {
            set
            {
                base.File = Path.Combine(Directory.GetCurrentDirectory(), value);
            }
        }
    }
}
