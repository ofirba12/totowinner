using log4net;
using System;
using System.Timers;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Services;

namespace BgBettingData.Generator
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static GoalServeProviderServices _goalServeSrv = GoalServeProviderServices.Instance;
        private static Timer timer = new Timer();
        static void Main(string[] args)
        {
            try
            {
                //BgDataGenerator.Executes();
                var execute = false;
                GoldserveGenerator.Prerequisite(GoalServeSportType.Soccer, execute);
                GoldserveGenerator.Prerequisite(GoalServeSportType.Basketball, execute);
                timer.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
                timer.Enabled = true;
                timer.Elapsed += ExecutionFactory;

            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to parse categories", ex);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine($"Parsing process has failed with unexpected error {Environment.NewLine}{ex}");
            }
            finally
            {
                ExecutionFactory(null, null);
                Console.WriteLine("Press any key to close");
                Console.ReadLine();
            }
        }

        private static void ExecutionFactory(object sender, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            try
            {
                var executedInstances = GoldserveGenerator.Executes();
                if (executedInstances > 0)
                {
                    ManualGenerator.Executes();
                    BgDataGenerator.Executes();
                }
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
            }
            finally
            {
                timer.Enabled = true;
            }
        }
    }
}
