﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;
using TotoWinner.Services;

namespace BgBettingData.Generator
{
    internal static class BgDataGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger("BgDataGenerator");
        private static void LogAndDump(string msg)
        {
            _log.Info(msg);
            Console.WriteLine(msg);
        }
        internal static void Executes()
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.White;
                var sports = new List<BgSport>() { BgSport.Soccer, BgSport.Basketball };
                //var sports = new List<BgSport>() { BgSport.Basketball };
                var starting = DateTime.Now;
                LogAndDump($"Creating Bg Feed...{starting} for sports {string.Join(','.ToString(),sports)}");
                var mapper = BgMapperServices.Instance.GetBgMapper();

                var systemRepository = BgSystemServices.Instance.GetBgSystemRepository();
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgFeedStartGeneration, starting);
                var fetchTime = systemRepository.BgFeedStartGeneration ?? (DateTime?)null;
                LogAndDump($"Fetch time: {fetchTime}");
                var matches = ManualProviderServices.Instance.FetchManualMatchesForProccessing(fetchTime); //take last update from bgSystem
                var activeDateRange = new List<DateTime>() { DateTime.Today.Date.AddDays(-1), DateTime.Today.Date, DateTime.Today.Date.AddDays(1), DateTime.Today.Date.AddDays(2) };

                foreach (var sport in sports)
                {
                    var manualData = matches
                        .Where(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue && m.Sport == sport)
                        .ToList();
                    LogAndDump($"# manual matches for {sport}: {manualData.Count}");
                    var datesToProcess = manualData.Select(m => m.GameStart.Date)
                        .Distinct()
                        .Except(activeDateRange)
                        .ToList();
                    LogAndDump($"# manual non-active dates to process for {sport}: {datesToProcess.Count}");
                    foreach (var date in datesToProcess)
                        ProccessNonActiveDaysFeed(mapper, sport, manualData, date);
                    foreach (var date in activeDateRange)
                        ProcessActiveDayFeed(mapper, sport, date);
                }
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgFeedFinishGeneration, DateTime.Now);
                LogAndDump($"Bg Feed update completed...{DateTime.Now}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now} Unexpected error occrred : {ex.Message}");
                _log.Fatal(ex);
                BgSystemServices.Instance.SetSystemField(BgSystemField.BgFeedFinishGeneration, DateTime.Now);
            }
        }

        private static void ProcessActiveDayFeed(MapperRespository mapper, BgSport sport, DateTime date)
        {
            try
            {
                LogAndDump($"** manual active date to processing for {sport} {date} **");
                var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed((GoalServeSportType)sport, date);
                VerifyFeed(goldserveFeed.Dumplicates, date);
                var bgData = BgDataServices.Instance.GenerateData(goldserveFeed.Feed, mapper);
                var generatedFeed = BgDataServices.Instance.MergeDuplicates(bgData);
                var persistanceFeed = BgDataServices.Instance.GetBgData(sport, date, mapper);
                VerifyFeed(persistanceFeed.FeedWithDuplicates, date);

                var manualDataByDate = ManualProviderServices.Instance.FetchManualMatchesByDate(date)
                    .Where(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue && m.Sport == sport)
                    .ToList();
                LogAndDump($"{date} # manual matches for {sport}: {manualDataByDate.Count}");
                manualDataByDate.ForEach(m => LogAndDump(m.ToString()));

                var finalFeed = BgDataServices.Instance.MergeAllFeeds(generatedFeed, persistanceFeed, manualDataByDate);
                MergeBgWithManual(finalFeed, sport, date, mapper);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"-ERROR- Failed to proccess active date {date}");
                _log.Error($"Failed to proccess active date {date}", ex);
            }
        }

        //private static void HandleQuarters(FeedPersistanceRepository finalFeed,DateTime date, MapperRespository mapper)
        //{
        //    var persistanceFeed = BgDataServices.Instance.GetBgData(BgSport.Basketball, date, mapper);
        //    var previousAddOrUpdate = finalFeed.BgFeedAddOrUpdate.ToDictionary(m => new BgFeedKey(m.Sport, m.GameStart, m.HomeId, m.AwayId), m => m);
        //    var bbMatchesToMerge = new List<BgMatch>();
        //    foreach (var match in persistanceFeed.FeedWithNoDuplicates)
        //    {
        //        if (previousAddOrUpdate.TryGetValue(match.Key, out var bbMatchAddOrUpdate))
        //        {
        //            bbMatchAddOrUpdate.SetMatchId(match.Value.MatchId);
        //            bbMatchesToMerge.Add(bbMatchAddOrUpdate);
        //        }
        //    }
        //    BgDataPersistanceServices.Instance.MergeQuatersBgFeed(bbMatchesToMerge);
        //    BgDataPersistanceServices.Instance.DeleteQuatersBgFeed(finalFeed.BgFeedRemove);
        //}

        private static void ProccessNonActiveDaysFeed(MapperRespository mapper, BgSport sport, List<ManualMatch> manualData, DateTime date)
        {
            try
            {
                var manualDataByDate = manualData.Where(m => m.GameStart.Date == date).ToList();
                var persistenceFeed = BgDataServices.Instance.GetBgData(sport, date, mapper);
                LogAndDump($"{date} # manual matches for {sport}: {manualDataByDate.Count}");
                manualDataByDate.ForEach(m => LogAndDump(m.ToString()));
                var finalFeed = BgDataServices.Instance.OverrideFeedWithManual(persistenceFeed.FeedWithNoDuplicates, manualDataByDate); // Manual feed must be last provider
                MergeBgWithManual(finalFeed, sport, date, mapper);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"-ERROR- Failed to proccess non active date {date}");
                _log.Error($"Failed to proccess non active date {date}", ex);
            }
        }
        private static void MergeBgWithManual(FeedPersistanceRepository finalFeed, BgSport sport, DateTime date, MapperRespository mapper)
        {
            BgDataPersistanceServices.Instance.MergeBgFeed(finalFeed.BgFeedAddOrUpdate);
            LogAndDump($"++ merging ++ {finalFeed.BgFeedAddOrUpdate.Count} matches");
            finalFeed.BgFeedAddOrUpdate.ForEach(m => _log.Debug(m.ToString()));
            var bbMatches = finalFeed.BgFeedAddOrUpdate.Where(m => m.Sport == BgSport.Basketball).ToList();
            if (bbMatches.Count > 0)
                MergeQuarters(bbMatches, date, mapper);

            if (finalFeed.BgFeedRemove.Count > 0)
            {
                var persistanceFeed = BgDataServices.Instance.GetBgData(sport, date, mapper);
                var feedRemoveCollection = finalFeed.BgFeedRemove.ToDictionary(m => new BgFeedKey(m.Sport, m.GameStart, m.HomeId, m.AwayId), m => m);
                var bbMatchesToRemove = new List<long>();
                foreach (var match in persistanceFeed.FeedWithNoDuplicates)
                {
                    if (feedRemoveCollection.TryGetValue(match.Key, out var bbMatch))
                    {
                        bbMatchesToRemove.Add(match.Value.MatchId);
                    }
                }
                BgDataPersistanceServices.Instance.IgnoreBgFeed(bbMatchesToRemove);
                LogAndDump($"-- removing -- {finalFeed.BgFeedRemove.Count} matches");
            }
        }
        private static void MergeQuarters(List<BgMatch> matches, DateTime date, MapperRespository mapper)
        {
            var persistanceFeed = BgDataServices.Instance.GetBgData(BgSport.Basketball, date, mapper);
            var previousAddOrUpdate = matches.ToDictionary(m => new BgFeedKey(m.Sport, m.GameStart, m.HomeId, m.AwayId), m => m);
            var bbMatchesToMerge = new List<BgMatch>();
            foreach (var match in persistanceFeed.FeedWithNoDuplicates)
            {
                if (previousAddOrUpdate.TryGetValue(match.Key, out var bbMatchAddOrUpdate))
                {
                    bbMatchAddOrUpdate.SetMatchId(match.Value.MatchId);
                    bbMatchesToMerge.Add(bbMatchAddOrUpdate);
                }
            }
            LogAndDump($"++ merging quarters ++ {bbMatchesToMerge.Count} matches");
            bbMatchesToMerge.ForEach(m => _log.Debug(m.ToString()));
            BgDataPersistanceServices.Instance.MergeQuatersBgFeed(bbMatchesToMerge);
        }
        #region Verify Feed
        private static void VerifyFeed(Dictionary<BgFeedKey, List<BgMatch>> dumplicates, DateTime date)
        {
            if (dumplicates.Count > 0)
            {
                LogAndDump($">>> Duplicates found in bgFeed for date {date}<<<<");
                foreach (var dup in dumplicates)
                {
                    dup.Value.ForEach(m => LogAndDump(m.ToString()));
                }
                _log.Error($"Data integrity: persistance duplicates feed found in date {date}");
            }
        }
        private static void VerifyFeed(IReadOnlyDictionary<GoldserveFeedKey, List<GoalserveMatchFeed>> dumplicates, DateTime date)
        {
            if (dumplicates.Count > 0)
            {
                LogAndDump($">>> Duplicates found in goldserve feed for date {date} <<<<");
                foreach (var dup in dumplicates)
                {
                    LogAndDump(dup.Key.ToString());
                    dup.Value.ForEach(m => LogAndDump(m.ToString()));
                }
            }
        }
        #endregion
    }
}
