﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TotoWinner.Services;

namespace BankerimTips.API.Controllers
{
    [RoutePrefix("api/Tip")]
    public class TipController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private TelegramBotPersistanceServices _botPersistanceSrv = TelegramBotPersistanceServices.Instance;
        private static Dictionary<Guid, DateTime> SkipTelegramCheck = new Dictionary<Guid, DateTime>();
        private readonly object clearMemoLock = new object();
        private readonly object addItemLock = new object();
        #region Send
        //http://localhost/BankerimTips/api/Tip/Send?guid=105E109A-7619-4A19-AFC2-C08D7E042817
        [Route("Send")]
        [HttpGet]
        public IHttpActionResult Send(Guid guid)
        {
            try
            {
                _log.Info($"Check for {guid}");
#if DEBUG
                SkipTelegramCheck.Add(guid, DateTime.Now);
#endif
                if (!SkipTelegramCheck.ContainsKey(guid))
                {
                    lock (addItemLock)
                    {
                        SkipTelegramCheck.Add(guid, DateTime.Now);
                    }
                    return Ok("** אם הגעת לדף זה, אנא לחץ שוב פעם על הטיפ **");
                }
                _botPersistanceSrv.VisitSubscriberUrl(guid);
                var tip = _botPersistanceSrv.GetTipFromGuid(guid);
                _log.Info($"Tip guid={guid} has been pressed, tipUrl={tip.TipUrl}");
                return Redirect($"{tip.TipUrl}");
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to send tip with guid {guid}", ex);
                return Ok(ex.Message);
            }
            finally
            {
                ClearMemory();
            }
        }
        //        public IHttpActionResult Send(Guid guid)
        //        {
        //            try
        //            {
        //                _log.Info($"Check for {guid}");
        //#if DEBUG
        //                SkipTelegramCheck.Add(guid, DateTime.Now);
        //#endif
        //                if (!SkipTelegramCheck.ContainsKey(guid))
        //                {
        //                    lock (addItemLock)
        //                    {
        //                        SkipTelegramCheck.Add(guid, DateTime.Now);
        //                    }
        //                    //return Ok("** אם הגעת לדף זה, אנא לחץ שוב פעם על הטיפ **");** winner change api, this does not work anymore https://www.winner.co.il/mainbook/events/4700451?utm_source=bankerim&utm_medium=in_game_menu **
        //                    return Redirect($"https://m.winner.co.il/?Referrer=Bankerim&ReferUrl=topbutton&campaign=new_mobileweb"); //moving everybody to winner homepage
        //                }
        //                _botPersistanceSrv.VisitSubscriberUrl(guid);
        //                var tip = _botPersistanceSrv.GetTipFromGuid(guid);
        //                _log.Info($"Tip guid={guid} has been pressed, tipUrl={tip.TipUrl}");
        //                //return Redirect($"{tip.TipUrl}"); ** winner change api, this does not work anymore https://www.winner.co.il/mainbook/events/4700451?utm_source=bankerim&utm_medium=in_game_menu **
        //                return Redirect($"https://m.winner.co.il/?Referrer=Bankerim&ReferUrl=topbutton&campaign=new_mobileweb"); //moving everybody to winner homepage
        //            }
        //            catch (Exception ex)
        //            {
        //                _log.Fatal($"An error occured trying to send tip with guid {guid}", ex);
        //                return Ok(ex.Message);
        //            }
        //            finally
        //            {
        //                ClearMemory();
        //            }
        //        }
        private void ClearMemory()
        {
            try
            {
                lock (clearMemoLock)
                {
                    _log.Info($"Before Clear: SkipTelegramCheck holds #{SkipTelegramCheck.Count} items");
                    SkipTelegramCheck = SkipTelegramCheck
                        .Where(kv => kv.Value > DateTime.Now.AddDays(-7))
                        .ToDictionary(kv => kv.Key, kv => kv.Value);
                    _log.Info($"After Clear: SkipTelegramCheck holds #{SkipTelegramCheck.Count} items");
                }
            }
            catch (Exception ex)
            {
                _log.Error($"Clear memory failed", ex);
            }
        }
        #endregion
    }
}
