﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using TotoWinner.Data;
using TotoWinner.Execution;
using System.Linq;
using TotoWinner.Services;
using TotoWinner.Services.Algo;
using TotoWinner.Common;
using System.Text;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class StatisticalDataTest
    {
        private Services.PersistanceServices _persistanceServices = Services.PersistanceServices.Instance;
        private Services.ExecutionServices _executionServices = Services.ExecutionServices.Instance;
        private Services.ProgramServices _programSrv = Services.ProgramServices.Instance;

        [TestMethod]
        public void CalcData_Success()
        {
            try
            {
                //ARRANGE
                var programSrv = ProgramServices.Instance;
                var staticCalcSrv = StatisticalDataServices.Instance;
                var year = 2019;
                var programType = ProgramType.Winner16;
                var beginDate = DateTime.Parse($"1 Jan {year}").Date;
                var endDate = DateTime.Parse($"1 Jan {year + 1}").Date.AddDays(-1);
                var programs = programSrv.GetPersistanceProgramsWithGamesData(programType, beginDate, endDate)
                    .OrderByDescending(p => p.EndDate).ToList();
                var amountABC = new List<BasicData3Response>();
                //ACT
                var program = programs.First(i => i.Number == 194301);
                var data = staticCalcSrv.Generate(program);
                amountABC.Add(data.AmountABC.ToBasicData3Response(program));
                //ASSERT
                Assert.IsTrue(amountABC[0].Amount1 == 8);
                Assert.IsTrue(amountABC[0].Amount2 == 3);
                Assert.IsTrue(amountABC[0].Amount3 == 5);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void Combinations()
        {
            try
            {
                //ARRANGE
                var list = new List<int>()
                {
                    1,
                    2,
                    3
                };
                //ACT
                var result = Utilities.GetMathCombinations<int>(list);
                //ASSERT
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }
        //for A,B,C ; n=2
        // return AA,AB,AC, BA,BB,BC CA,CB,CC
        [TestMethod]
        public void CombinationsABC_1X2()
        {
            try
            {
                var numberOfGames = 2;
                var list = Utilities.GetMathCombinationsWithRepetition<char>(new char[] { '1', 'X', '2' }, numberOfGames);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }
    }
}
