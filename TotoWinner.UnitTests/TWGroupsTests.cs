﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Groups;
using TotoWinner.Maintenance;
using TotoWinner.Services;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class TWGroupsTests
    {
        #region Groups_GenerateColumnsSuccess
        [TestMethod]
        public void Groups_GenerateColumnsSuccess1()
        {
            //ARRANGE
            var srv = GroupsRepositoryServices.Instance;
            //Scenario1();
            //Scenario2();
            Scenario3();
            //ACT
            var response = srv.GenerateAllColumns();

            //ASSERT
            Assert.IsNotNull(response);
        }
        [TestMethod]
        public void Groups_RobotFlow()
        {
            var srv = GroupsRepositoryServices.Instance;
            Scenario4();
            var selectedSystem = new SystemParameters() { SystemId = 423 };
            var shrinkPayload = TotoWinner.Services.SystemsServices.Instance.LoadSystemForOptimizationV2(selectedSystem.SystemId);
            var robot = new RobotRepository(selectedSystem, shrinkPayload,100);
            robot.Init();
            var list = robot.GetFinalCandidateList();

        }
        [TestMethod]
        public void Groups_RobotDraftFlow()
        {
            var srv = GroupsRepositoryServices.Instance;
            Scenario4();             
            var pivots = GroupsRepositoryServices.Instance.PivotColumns(srv.Groups[1].GameColumns);
            var columns = new List<List<string>>();
            foreach (var column in pivots)
            {
                var tur = column.Value.Values.ToList();
                columns.Add(tur);
            }
            var typeColumnCombinations = columns.Select(i => string.Join("", i)).ToList();
            var allCombinations = Utilities.GetMathCombinationsWithRepetition<char>(new char[] { '1', 'X', '2' }, columns.Count());
            var newColumnCombinations = allCombinations.Except(typeColumnCombinations).ToList();
            var game1x2ColumnCandidates = new Dictionary<int, string>(); //<1,"1XXX222">, <3,"11X21X2">
            var gamesIds = pivots.First().Value.Keys.ToList();
            var gameIndex = 0;
            foreach (var game in gamesIds)
            {
                foreach (var column in newColumnCombinations)
                {
                    if (!game1x2ColumnCandidates.ContainsKey(game))
                        game1x2ColumnCandidates.Add(game, column[gameIndex].ToString());
                    else
                        game1x2ColumnCandidates[game] = $"{game1x2ColumnCandidates[game]}{column[gameIndex]}";
                }
                gameIndex++;
            }
            var selectSystemId = 423;
            var shrinkPayload = TotoWinner.Services.SystemsServices.Instance.LoadSystemForOptimizationV2(selectSystemId);
            var rates = shrinkPayload.Definitions.Form.EventsRates;
            var gamesRate = rates.ToDictionary(x => x.position + 1, x => x);
            var game1x2AbcMapping = new Dictionary<int,Dictionary<char,char>>(); //gameid, <gameResult,letter>, e.g 1,<1,A><X,B><2,C>, 2,<1,C><X,B><2,A>
            var gameAbc1x2Mapping = new Dictionary<int, Dictionary<char, char>>(); //gameid, <letter,gameResult>, e.g 1,<A,1><B,X><C,2>, 2,<C,1><B,X><A,2>
            foreach (var game in gamesRate.Keys)
            {
                var currentRow = new[]
                {
                    (decimal)gamesRate[game].rate1,
                    (decimal)gamesRate[game].rateX,
                    (decimal)gamesRate[game].rate2,
                };
                var abc = StatisticalDataServices.Instance.GetMappedLettersRow(currentRow);
                var tur1x2Abc = new Dictionary<char, char>();
                tur1x2Abc.Add('1', abc[0]);
                tur1x2Abc.Add('X', abc[1]);
                tur1x2Abc.Add('2', abc[2]);
                var turAbc1x2 = new Dictionary<char, char>();
                turAbc1x2.Add(abc[0],'1');
                turAbc1x2.Add(abc[1],'X');
                turAbc1x2.Add(abc[2],'2');

                game1x2AbcMapping.Add(game, tur1x2Abc);
                gameAbc1x2Mapping.Add(game, turAbc1x2);
            }

            var combinationGamesAbc = new Dictionary<int, string>();//1<CBBBAAA>,3,<AABCABC>...
            foreach (var game in game1x2ColumnCandidates.Keys) //1,<1XXX222>, 3,<11X21X2>,...
            {
                if (!combinationGamesAbc.ContainsKey(game))
                    combinationGamesAbc.Add(game, string.Empty);

                var gameRes = game1x2ColumnCandidates[game];
                for (var turIndex=0; turIndex < gameRes.Count(); turIndex++)
                {
                    var charKey = gameRes[turIndex];
                    combinationGamesAbc[game] = $"{combinationGamesAbc[game]}{game1x2AbcMapping[game][charKey]}";
                }
            }
            
            //STEP 4+5
            var numberOfColumns = combinationGamesAbc.Values.First().Count();
            var sumOfColumns = new Dictionary<int, float>();
            var columnsWithLetterA = new Dictionary<int, int>();//<columnId, numberOfAs>
            for (var columnIndexAbc = 0; columnIndexAbc< numberOfColumns; columnIndexAbc++)
            {
                var numberOfLetterAInColumn = 0;
                foreach (var game in combinationGamesAbc.Keys)
                {
                    if (!sumOfColumns.ContainsKey(columnIndexAbc))
                        sumOfColumns.Add(columnIndexAbc, 0);
                    var rate = (float)0.0;
                    var letter = combinationGamesAbc[game][columnIndexAbc];
                    var turResult = gameAbc1x2Mapping[game][letter];
                    switch (turResult)
                    {
                        case '1':
                            rate = gamesRate[game].rate1;
                            break;
                        case 'X':
                            rate = gamesRate[game].rateX;
                            break;
                        case '2':
                            rate = gamesRate[game].rate2;
                            break;
                    }
                    sumOfColumns[columnIndexAbc] += rate;
                    numberOfLetterAInColumn += letter == 'A'
                        ? 1
                        : 0;
                }
                columnsWithLetterA.Add(columnIndexAbc, numberOfLetterAInColumn);
            }

            //STEP 6 - Choose candidate
            var maxNumerOfLetterA = columnsWithLetterA.Values.Max();
            var candidateColumns = new List<int>();
            for (var index = maxNumerOfLetterA; index >= 0; )
            {
                var candidates = columnsWithLetterA.Where(i => i.Value == index && !candidateColumns.Contains(i.Key)).ToList();
                if (candidates.Count() == 0)
                {
                    index--;
                    candidates = columnsWithLetterA.Where(i => i.Value == index && !candidateColumns.Contains(i.Key)).ToList();
                }
                var lowestRateSum = float.MaxValue;
                var chosenColumn = -1;
                foreach (var candidate in candidates)
                {
                    var columnId = candidate.Key;
                    var rate = sumOfColumns[columnId];
                    if (rate < lowestRateSum)
                    {
                        lowestRateSum = rate;
                        chosenColumn = columnId;
                    }
                }
                if (chosenColumn != -1)
                    candidateColumns.Add(chosenColumn);
            }
            //STEP 7 - push candidates to pricer
            //push the groups the candidate --DO THIS IN A LOOP
            foreach (var column in candidateColumns)
            {
                foreach (var game in game1x2ColumnCandidates.Keys) //1,<1XXX222>, 3,<11X21X2>,...
                {
                    var gameResult = game1x2ColumnCandidates[game][column];
                    srv.Groups[1].GameColumns.AddRobotGameResult(game, gameResult.ToString());
                }
                //call calc process and check barier
                //the whole process should be sone per group
            }

        }
        private void Scenario4()
        {
            var srv = GroupsRepositoryServices.Instance;
            var grp1 = new Group(1);
            var gameColumns1 = new Dictionary<int, Dictionary<int, string>>();//<gameId, <columnId, result>>
            gameColumns1.Add(1, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "1" }
            });
            gameColumns1.Add(3, new Dictionary<int, string>()
            {
                {1, "2" },
                {2, "X" }
            });
            grp1.SetGameColumns(gameColumns1);
            srv.Groups.Add(1, grp1);
        }
        private void Scenario3()
        {
            var srv = GroupsRepositoryServices.Instance;
            var grp1 = new Group(1);
            var gameColumns1 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns1.Add(1, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            gameColumns1.Add(2, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            grp1.SetGameColumns(gameColumns1);

            var grp2 = new Group(2);
            var gameColumns2 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns2.Add(1, new Dictionary<int, string>()
            {
                {1, "X" },
                {2, "2" }
            });
            gameColumns2.Add(2, new Dictionary<int, string>()
            {
                {1, "X" },
                {2, "1" }
            });
            grp2.SetGameColumns(gameColumns2);
            var grp3 = new Group(3);
            var gameColumns3 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns3.Add(1, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            gameColumns3.Add(2, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "X" }
            });
            grp3.SetGameColumns(gameColumns3);
            srv.Groups.Add(1, grp1);
            srv.Groups.Add(2, grp2);
            srv.Groups.Add(3, grp3);
        }

        private void Scenario2()
        {
            var srv = GroupsRepositoryServices.Instance;
            var grp1 = new Group(1);
            var gameColumns1 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns1.Add(1, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            gameColumns1.Add(2, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            grp1.SetGameColumns(gameColumns1);

            var grp2 = new Group(2);
            var gameColumns2 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns2.Add(1, new Dictionary<int, string>()
            {
                {1, "X" },
                {2, "2" }
            });
            gameColumns2.Add(2, new Dictionary<int, string>()
            {
                {1, "X" },
                {2, "1" }
            });
            grp2.SetGameColumns(gameColumns2);
            srv.Groups.Add(1, grp1);
            srv.Groups.Add(2, grp2);
        }
        private void Scenario1()
        {
            var srv = GroupsRepositoryServices.Instance;
            var grp1 = new Group(1);
            var gameColumns1 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns1.Add(1, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            gameColumns1.Add(2, new Dictionary<int, string>()
            {
                {1, "1" },
                {2, "2" }
            });
            grp1.SetGameColumns(gameColumns1);

            var grp2 = new Group(2);
            var gameColumns2 = new Dictionary<int, Dictionary<int, string>>();
            gameColumns2.Add(1, new Dictionary<int, string>()
            {
                {1, "X" }
            });
            gameColumns2.Add(2, new Dictionary<int, string>()
            {
                {1, "X" }
            });
            grp2.SetGameColumns(gameColumns2);
            srv.Groups.Add(1, grp1);
            srv.Groups.Add(2, grp2);
        }
        #endregion

    }
}
