﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;
using TotoWinner.Data.Optimizer;
using TotoWinner.Services;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class BgDataBetsSuggestions
    {
        [TestMethod]
        public void GetOpenBets()
        {
            var fetchDate = DateTime.Today;//.AddDays(-1);// DateTime.Parse("24 Jan 2022");
            var sport = BgSport.Soccer;
            var timeZoneOffset = -180;
            var mapper = BgMapperServices.Instance.GetBgMapper();
            try
            {
                var leaguesToShow = BgDataLeaguesWhitelistServices.Instance.GetAllBgLeaguesIdsWhitelist();
                var repo = BgDataServices.Instance.GetBgMatchesFeedWithFilter(sport,
                    fetchDate,
                    timeZoneOffset / 60,
                    leaguesToShow,
                    mapper);
                var impliedStatistics = BgDataServices.Instance.CreateImpliedStatistics(repo.Matches, repo.Statistics, repo.StatisticsDetails);
                var allBets = BgDataScoresServices.Instance.GetMatchesWithBets(repo, impliedStatistics, sport);
                var openBets = allBets.Where(b => !string.IsNullOrEmpty(b.Status));
                var closedBets = allBets.Where(b => string.IsNullOrEmpty(b.Status));

                foreach (var match in openBets)
                {
                    //var bgTeamsAnalysisRepository = BgDataAnalysisServices.Instance.PrepareTeamsRepository(match.HomeTeamId, match.AwayTeamId);
                    //var home = BgDataServices.Instance.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.HomeId, mapper);
                    //var away = BgDataServices.Instance.ExtractBgNameFromBgId(match.Sport, BgMapperType.Teams, match.AwayId, mapper);
                    //var league = BgDataServices.Instance.ExtractBgNameFromBgId(match.Sport, BgMapperType.Leagues, match.LeagueId, mapper);
                    //var country = BgDataServices.Instance.ExtractBgNameFromBgId(match.Sport, BgMapperType.Countries, match.CountryId.Value, mapper);
                    //var bgMatch = new BgMatch(match, home, away, league, country);
                    //var tips = BgDataScoresServices.Instance.GetMatchTips(bgMatch, bgTeamsAnalysisRepository);
                }
                //var scoreData = BgDataScoresServices.Instance.PrepareScoreData(repo, impliedStatistics, sport);

            }
            catch (Exception ex)
            {
                Assert.Fail($"{ex}");
            }
        }

    }
}

