﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using TotoWinner.Data;
using TotoWinner.Execution;
using System.Linq;
using TotoWinner.Services;
using TotoWinner.Services.Algo;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class BacktestingAlgoTest
    {
        private Services.PersistanceServices _persistanceServices = Services.PersistanceServices.Instance;
        private Services.ExecutionServices _executionServices = Services.ExecutionServices.Instance;
        private Services.ProgramServices _programSrv = Services.ProgramServices.Instance;

        [TestMethod]
        public void FetchExecutionData_Success()
        {
            try
            {
                //ARRANGE
                var execution = _persistanceServices.ExecutionQueuedGet();
                if (execution == null || execution.SystemType != SystemType.Backtesting)
                    Assert.Fail("Queued work is not a backtesting work or no work found");
                var definitions = JsonConvert.DeserializeObject<BacktestingDefinitions>(execution.Parameters);
                var work = new BacktestingExecutionData()
                {
                    ExecutionId = execution.ExecutionId,
                    Status = execution.Status,
                    Definitions = definitions
                };

                //ACT
                using (var cts = new CancellationTokenSource())
                {
                    BacktestingAlgo algo = new BacktestingAlgo();
                    algo.RunAlgo(cts.Token, work, false, false);
                }
                //ASSERT
                //Assert.IsNotNull(response);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void GetBacktestingResult_Success()
        {
            try
            {
                //ARRANGE
                var results_1 = _executionServices.GetBacktestingResults(10331, 2);
                var results_2 = _executionServices.GetBacktestingResults(10330, 10);
                //ACT
                //ASSERT
                //Assert.IsNotNull(response);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void GenerateBaseBets_Success()
        {
            //ARRANGE
            var execution = _persistanceServices.ExecutionQueuedGet();
            if (execution == null || execution.SystemType != SystemType.Backtesting)
                Assert.Fail("Queued work is not a backtesting work or no work found");
            var definitions = JsonConvert.DeserializeObject<BacktestingDefinitions>(execution.Parameters);
            var work = new BacktestingExecutionData()
            {
                ExecutionId = execution.ExecutionId,
                Status = execution.Status,
                Definitions = definitions
            };
            var programs = _programSrv.GetPersistanceProgramsWithGamesData(work.Definitions.ProgramType, //ProgramType.WinnerWorld,
                DateTime.Parse("1/1/2019"), DateTime.Today);
            var program = programs[1];

            var gameRepoHigh = new Dictionary<BtWinType, SortedDictionary<double, BtBaseGame>>();
            gameRepoHigh.Add(BtWinType.HomeWin, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
            gameRepoHigh.Add(BtWinType.GuestWin, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
            gameRepoHigh.Add(BtWinType.BreakEven, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
            gameRepoHigh.Add(BtWinType.Any, new SortedDictionary<double, BtBaseGame>(new DuplicateDescendingKeyComparer<double>()));
            var gameRepoLow = new Dictionary<BtWinType, SortedDictionary<double, BtBaseGame>>();
            gameRepoLow.Add(BtWinType.HomeWin, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
            gameRepoLow.Add(BtWinType.GuestWin, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
            gameRepoLow.Add(BtWinType.BreakEven, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
            gameRepoLow.Add(BtWinType.Any, new SortedDictionary<double, BtBaseGame>(new DuplicateAscendingKeyComparer<double>()));
            foreach (var game in program.Games)
            {
                var baseGame = new BtBaseGame(game.Value.Index, game.Value.Rate1, game.Value.RateX, game.Value.Rate2);
                gameRepoHigh[BtWinType.HomeWin].Add(baseGame.Rate1, baseGame);
                gameRepoHigh[BtWinType.BreakEven].Add(baseGame.RateX, baseGame);
                gameRepoHigh[BtWinType.GuestWin].Add(baseGame.Rate2, baseGame);
                gameRepoHigh[BtWinType.Any].Add(Math.Max(Math.Max(baseGame.Rate2, baseGame.Rate1), baseGame.RateX), baseGame);
                gameRepoLow[BtWinType.HomeWin].Add(baseGame.Rate1, baseGame);
                gameRepoLow[BtWinType.BreakEven].Add(baseGame.RateX, baseGame);
                gameRepoLow[BtWinType.GuestWin].Add(baseGame.Rate2, baseGame);
                gameRepoLow[BtWinType.Any].Add(Math.Min(Math.Min(baseGame.Rate2, baseGame.Rate1), baseGame.RateX), baseGame);
            }
            foreach (var definition in work.Definitions.GamesDefinitions)
            {
                if (definition.RowType == BtGameDefinitionType.Banker)
                {
                    var btGameDef = new BtBankerGameDefinition(
                        ConvertToBankerBetType(definition.BetType),
                        ConvertToRateType(definition.RateType),
                        ConvertToBetForType(definition.BetForType),
                        definition.RateFrom, definition.RateTo,
                        new MinMax(Convert.ToDecimal(definition.From), Convert.ToDecimal(definition.To))
                        );
                    var gamesRepo = btGameDef.RateType == BtRateType.Low ? gameRepoLow[btGameDef.BetForType] : gameRepoHigh[btGameDef.BetForType];
                    var limits = work.Definitions.BankersLimits;
                    var betsCounter = gamesRepo.Values.Count(item => item.Won.Count == 1);
                    foreach (var kpv in gamesRepo)
                    {
                        if (betsCounter >= limits.Max)
                            break;
                        if (kpv.Value.Won.Count > 0)
                            continue;
                        if (kpv.Key >= btGameDef.FromRate && kpv.Key <= btGameDef.ToRate)
                        {
                            char winChar = ' ';
                            switch (btGameDef.BetForType)
                            {
                                case BtWinType.HomeWin:
                                    winChar = '1';
                                    break;
                                case BtWinType.BreakEven:
                                    winChar = 'X';
                                    break;
                                case BtWinType.GuestWin:
                                    winChar = '2';
                                    break;
                                case BtWinType.Any:
                                    if (kpv.Key == kpv.Value.Rate1)
                                        winChar = '1';
                                    else if (kpv.Key == kpv.Value.RateX)
                                        winChar = 'X';
                                    else if (kpv.Key == kpv.Value.Rate2)
                                        winChar = '2';
                                    break;
                            }
                            kpv.Value.MarkBanker(winChar);
                            betsCounter++;
                        }
                    }
                }
            }
            var bankersCounter = gameRepoLow[BtWinType.Any].Values.Count(item => item.Won.Count == 1);
            if (bankersCounter >= work.Definitions.BankersLimits.Min && bankersCounter <= work.Definitions.BankersLimits.Max)
            {
                foreach (var kpv in gameRepoLow[BtWinType.Any])
                {
                    if (kpv.Value.Won.Count == 0)
                        kpv.Value.MarkTriple();
                }
                var persistanceBets = GenerateBaseBets(gameRepoLow[BtWinType.Any].Values.ToList());
            }
        }

        [TestMethod]
        public void CalculateBaseBets_Success()
        {
            var calcService = CalculationServices.Instance;
            var request = new BetsFormCreateRequest();
            #region ProbSettings
            request.EventsRates = new List<RatesLine>();
            request.EventsRates.Add(new RatesLine() { position = 0, rate1 = 1, rateX = 1, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 1, rate1 = 1, rateX = 2, rate2 = 3 });
            request.EventsRates.Add(new RatesLine() { position = 2, rate1 = 1, rateX = 1, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 3, rate1 = 2, rateX = 2, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 4, rate1 = 1, rateX = 2, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 5, rate1 = 2, rateX = 1, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 6, rate1 = 1, rateX = 2, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 7, rate1 = 2, rateX = 1, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 8, rate1 = (float)2.55, rateX = (float)2.8, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 9, rate1 = (float)2.55, rateX = (float)2.8, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 10, rate1 = (float)1.95, rateX = (float)2.85, rate2 = (float)2.6 });
            request.EventsRates.Add(new RatesLine() { position = 11, rate1 = (float)2.25, rateX = (float)2.6, rate2 = (float)2.4 });
            request.EventsRates.Add(new RatesLine() { position = 12, rate1 = (float)2.4, rateX = (float)2.6, rate2 = (float)2.25 });
            request.EventsRates.Add(new RatesLine() { position = 13, rate1 = (float)2.2, rateX = (float)2.6, rate2 = (float)2.45 });
            request.EventsRates.Add(new RatesLine() { position = 14, rate1 = (float)2.15, rateX = (float)2.6, rate2 = (float)2.5 });
            request.EventsRates.Add(new RatesLine() { position = 15, rate1 = (float)2.65, rateX = (float)2.6, rate2 = (float)2.05 });
            #endregion
            #region ConstraintsSettings
            request.EventsBets = new List<Bet>();
            request.EventsBets.Add(new Bet() { position = 0, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 1, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 2, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 3, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 4, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 5, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 6, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 7, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 8, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 9, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 10, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 11, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 12, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 13, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 14, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 15, bet1 = true, betX = false, bet2 = false });
            #endregion
            #region Include Settings
            request.FormSettings = new FormSettings();
            request.FormSettings.IncludeSettings = new IncludeSettings();
            request.FormSettings.IncludeSettings.amountSum = new MinMax() { Min = 16, Max = 100 };
            request.FormSettings.IncludeSettings.amount1 = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.amountX = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.amount2 = new MinMax() { Min = 1, Max = 16 };
            //request.formSettings = new FormSettings(request.eventsRatesList.Count, includeSettings);

            request.FormSettings.IncludeSettings.sequenceBreak = new MinMax() { Min = 1, Max = 15 };
            request.FormSettings.IncludeSettings.sequenceX = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.sequence1 = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.sequence2 = new MinMax() { Min = 1, Max = 16 };
            #endregion

            request.Validate();
            var constrSettings = calcService.BuildConstrSettings(request);
            var probSettings = calcService.BuildProbSettings(request);
            var validators = calcService.BuildValidators(request, probSettings);
            var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, false);
            var response = calcService.CalculateBets(betInputs);
            var fileName = "btTest.xls";
            var excelFile = calcService.ResultsToExcel(response, false, fileName, "OutputTemplate.xlsx");
        }
        private Dictionary<short, Bet> GenerateBaseBets(List<BtBaseGame> games)
        {
            var bets = new Dictionary<short, Bet>();
            var gameOrder = games.OrderBy(item => item.Index);
            foreach (var game in gameOrder)
            {
                bets.Add((short)game.Index, 
                    new Bet((short)game.Index, 
                        game.Won.Contains('1'),
                        game.Won.Contains('X'),
                        game.Won.Contains('2'))
                    );
            }
            return bets;
        }

        public class DuplicateDescendingKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
        {
            #region IComparer<TKey> Members

            public int Compare(TKey x, TKey y)
            {
                int result = x.CompareTo(y);

                if (result == 0)
                    return 1;   // Handle equality as beeing greater
                else
                    return -result;
            }

            #endregion
        }
        public class DuplicateAscendingKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
        {
            #region IComparer<TKey> Members

            public int Compare(TKey x, TKey y)
            {
                int result = x.CompareTo(y);

                if (result == 0)
                    return 1;   // Handle equality as beeing greater
                else
                    return result;
            }

            #endregion
        }
        private BtBankerBetType ConvertToBankerBetType(string from)
        {
            switch (from)
            {
                case "תיקו":
                    return BtBankerBetType.BreakEven;
                case "רגיל":
                    return BtBankerBetType.Regular;
                default:
                    throw new Exception($"{from} can not be converted to BtBankerBetType");
            }
        }
        private BtRateType ConvertToRateType(string from)
        {
            switch (from)
            {
                case "נמוך":
                    return BtRateType.Low;
                case "גבוה":
                    return BtRateType.High;
                //case "אמצע":
                //    return BtRateType.MIddle;
                default:
                    throw new Exception($"{from} can not be converted to BtRateType");
            }
        }
        private BtWinType ConvertToBetForType(string from)
        {
            switch (from)
            {
                case "קבוצה בית":
                    return BtWinType.HomeWin;
                case "תיקו":
                    return BtWinType.BreakEven;
                case "קבוצה חוץ":
                    return BtWinType.GuestWin;
                case "חופשי":
                    return BtWinType.Any;
                default:
                    throw new Exception($"{from} can not be converted to BtWinType");
            }
        }
    }
}
