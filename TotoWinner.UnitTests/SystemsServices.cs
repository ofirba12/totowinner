﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class SystemsServices
    {
        int SaveSystemId;
        #region ShrinkSystemServices_SaveSystem_Success
        [TestMethod]
        public void ShrinkSystemServices_SaveSystem_Success()
        {
            var systemServices = Services.SystemsServices.Instance;
            var calcService = CalculationServices.Instance;
            //ARRANGE
            var saveRequest = new SaveShrinkSystemRequest();
            var system = new BetsFormCreateRequest();
            CalculationServicesTests.FillFormRequest(system, 1);
            system.Validate();
            var constrSettings = calcService.BuildConstrSettings(system);
            var probSettings = calcService.BuildProbSettings(system);
            var validators = calcService.BuildValidators(system, probSettings);

            //ACT
            saveRequest.System = system;
            saveRequest.UniqueId = "18420#96";
            this.SaveSystemId = systemServices.SaveSystem(saveRequest.UniqueId, saveRequest.SystemType, saveRequest.Name, saveRequest);

            //ASSERT
            Assert.IsNotNull(SaveSystemId);
        }
        #endregion

        #region ShrinkSystemServices_LoadSystem_Success
        [TestMethod]
        public void ShrinkSystemServices_LoadSystem_Success()
        {
            var systemServices = Services.SystemsServices.Instance;
            //ARRANGE

            //ACT
            var response = systemServices.LoadSystem<SaveShrinkSystemRequest>(this.SaveSystemId);

            //ASSERT
            Assert.IsNotNull(response);
        }
        #endregion
    }
}
