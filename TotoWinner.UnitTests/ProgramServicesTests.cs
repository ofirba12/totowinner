﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TotoWinner.Data;
using TotoWinner.Maintenance;
using TotoWinner.Services;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class ProgramServicesTests
    {
        #region GetProgramData_Success
        [TestMethod]
        public void GetProgramData_Success()
        {
            //ARRANGE

            //ACT
            var programService = ProgramServices.Instance;
            var response = programService.GetProgramData(DateTime.Today.Date, ProgramType.Winner16);

            //ASSERT
            Assert.IsNotNull(response);
        }
        #endregion

        #region GetPrograms_Success
        [TestMethod]
        public void GetPrograms_Success()
        {
            //ARRANGE

            //ACT
            var programService = ProgramServices.Instance;
            var response = programService.GetPrograms(DateTime.Today.Date, ProgramType.Winner16);

            //ASSERT
            Assert.IsNotNull(response);
        }
        #endregion

        #region GetAllPrograms_Success
        [TestMethod]
        public void GetAllPrograms_Success()
        {
            //ARRANGE

            //ACT
            var programService = ProgramServices.Instance;
            var response = programService.GetAllPrograms();

            //ASSERT
            Assert.IsNotNull(response);
        }
        #endregion

        #region GetAllPrograms_Success
        [TestMethod]
        public void SyncPrograms_Success()
        {
            //ARRANGE

            //ACT
            ProgramsManager.HandleProgramSyncronization();

            //ASSERT
        }
        #endregion
    }
}
