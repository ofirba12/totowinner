﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TotoWinner.Data;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class FlowTesting
    {
        int flowExecutionId;
        int saveSystemId = -1;
        [TestMethod]
        public void LoadFromDBAndCalc()
        {
            var sysSrv = Services.SystemsServices.Instance;
            var savedOptimizedSystemId = 3;
            var systemData = sysSrv.LoadSystemForOptimizationV1(savedOptimizedSystemId);
            //calculate
            var calcSrv = Services.CalculationServices.Instance;
            var calcRequest = new Data.BetsFormCreateRequest()
            {
                FormId = 10000,
                FormSettings = systemData.System.FormSettings,
                EventsBets = systemData.System.EventsBets,
                EventsRates = systemData.System.EventsRates,
                CleanOnDiff = systemData.System.CleanOnDiff,
                GenerateOutputFile = false
            };
            var results0 = calcSrv.CalculateSystem(calcRequest);
        }
        [TestMethod]
        public void QueueOptimizer()
        {
            var exSrv = Services.ExecutionServices.Instance;
            var prSrv = Services.PersistanceServices.Instance;
            var savedOptimizedSystemId = 3;
            var executionId = exSrv.AddOptimizerExecution(new Data.ExecutionOptimizerParameters(savedOptimizedSystemId, 100, 20));
            var queued = prSrv.ExecutionQueuedGet();
            Assert.IsTrue(executionId == queued.ExecutionId);
            flowExecutionId = queued.ExecutionId;
        }
        [TestMethod]
        public void RunOptimizer()
        {
            var savedOptimizedSystemId = saveSystemId;
            var executionId = flowExecutionId;

            var exSrv = Services.ExecutionServices.Instance;
            exSrv.UpdateExecutionStatus<SearchResults>(executionId, SystemType.Optimizer, Data.ExecutionStatus.Running, null);

            var sysSrv = Services.SystemsServices.Instance;
            var systemData = sysSrv.LoadSystemForOptimizationV1(savedOptimizedSystemId);
            //calculate
            var calcSrv = Services.CalculationServices.Instance;
            var calcRequest = new Data.BetsFormCreateRequest()
            {
                FormId = executionId,
                FormSettings = systemData.System.FormSettings,
                EventsBets = systemData.System.EventsBets,
                EventsRates = systemData.System.EventsRates,
                CleanOnDiff = systemData.System.CleanOnDiff,
                GenerateOutputFile = false
            };
            var results0 = calcSrv.CalculateSystem(calcRequest);
            //move game 1
            calcRequest.EventsBets[0].bet1 = true;
            calcRequest.EventsBets[0].betX = true;
            var results1 = calcSrv.CalculateSystem(calcRequest);
            //calculate
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(1, "1X", false));
            //move game 1
            //calculate
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(2, "1X2", true));

            //ARRANGE

        }

        [TestMethod]
        public void Flow1_WithUI()
        {
            var exSrv = Services.ExecutionServices.Instance;
            var prSrv = Services.PersistanceServices.Instance;
            var queued = prSrv.ExecutionQueuedGet();
            var sysSrv = Services.SystemsServices.Instance;
            var savedOptimizedSystemId = 3;
            var systemData = sysSrv.LoadSystemForOptimizationV1(savedOptimizedSystemId);
            var executionId = queued.ExecutionId;
            exSrv.UpdateExecutionStatus<SearchResults>(executionId, SystemType.Optimizer, Data.ExecutionStatus.Running, null);
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(1, "1X", false));
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(1, "1X", true));

            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(2, "1X", false));
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(2, "1X2", true));

            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(3, "1X", false));
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(3, "1X2", false));
            exSrv.SetOptimizedBetResult(executionId, new Data.OptimizedBet(3, "1X2", true));

            var calcSrv = Services.CalculationServices.Instance;
            var calcRequest = new Data.BetsFormCreateRequest()
            {
                FormId = executionId,
                FormSettings = systemData.System.FormSettings,
                EventsBets = systemData.System.EventsBets,
                EventsRates = systemData.System.EventsRates,
                CleanOnDiff = systemData.System.CleanOnDiff,
                GenerateOutputFile = false
            };
            var results0 = calcSrv.CalculateSystem(calcRequest);
            exSrv.UpdateExecutionStatus<SearchResults>(queued.ExecutionId, SystemType.Optimizer, Data.ExecutionStatus.Finished, results0);
        }
        [TestMethod]
        public void PersistanceServices_LoadSystem_Success()
        {
            //ACT
            var services = Services.ExecutionServices.Instance;
            //ARRANGE
            services.GetOptimizerExecutionShrinkResult(10232);
            //ASSERT
        }

    }
}
