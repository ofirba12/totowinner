﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using TotoWinner.Common;
using System.Xml;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class RealTimeData
    {
        public class MainEvent
        {
            public int SportId { get; set; }
            public string CountryName { get; set; }
            public string LeagueName { get; set; }
            public int EventId { get; set; }
            public string Description { get; set; }
            public DateTime EventDate { get; set; }
            public Dictionary<string, decimal> Rates { get; set; }
            public decimal LaunderResult { get; set; }
        }
        public Dictionary<int, MainEvent> MainEvents = new Dictionary<int, MainEvent>();

        [TestMethod]
        public void RealTime()
        {
            var totoWinnerUrl = $"https://affiliates-xml.winner.co.il/xmls/RPMInternetProgramm_1.json";
            WebRequest request = WebRequest.Create(totoWinnerUrl);
            WebResponse response = request.GetResponse();
            var jsonString = string.Empty;
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                jsonString = reader.ReadToEnd();
            }
            //var programs = WebApiInvoker.JsonDeserializeDynamic(jsonString);
            var filePath = @"C:\Users\ofirba\Projects\TotoWinner\TotoWinner.UnitTests\bin\Debug\RPMInternetProgramm_1.json";
            File.WriteAllText(filePath, jsonString);
            jsonString = File.ReadAllText(filePath);
            var jss = new JavaScriptSerializer();
            jss.MaxJsonLength = 2147483647;
            var dict = jss.Deserialize<dynamic>(jsonString);
            var events = dict["events"];
            foreach (var evnt in events)
            {
                var countryName = evnt["countryName"];
                var leagueName = evnt["leagueName"];
                var description = evnt["description"];
                var homeTeamName = evnt["homeTeamName"];
                var awayTeamName = evnt["awayTeamName"];
                var eventDate = DateTime.Parse(evnt["eventDate"]);
                var eventId = evnt["id"];
                var sportId = evnt["sportId"];
                MainEvents.Add(eventId, new MainEvent()
                {
                    CountryName = countryName,
                    EventId = eventId,
                    Description = description,
                    EventDate = eventDate,
                    LeagueName = leagueName,
                    SportId = sportId
                });
                if (sportId == 100)
                {
                    foreach (var market in evnt["markets"])
                    {
                        var status = market["status"];
                        if (status == "OPEN")
                        {
                            market.TryGetValue("description", out dynamic marketDescription);
                            if ((marketDescription as String).Contains("1X2"))
                            {
                                var rates = new Dictionary<string, decimal>();
                                foreach (var outcome in market["outcomes"])
                                {
                                    var rate = outcome["internetOdd"];
                                    if (outcome["ordinalPosition"] == 1)
                                        rates.Add("1", rate);
                                    if (outcome["ordinalPosition"] == 2)
                                        rates.Add("x", rate);
                                    else if (outcome["ordinalPosition"] == 3)
                                        rates.Add("2", rate);
                                }
                                MainEvents[eventId].Rates = rates;
                                MainEvents[eventId].LaunderResult = 1 / rates["1"] + 1 / rates["x"] + 1 / rates["2"];
                            }
                        }
                    }

                }
            }
        }
        [TestMethod]
        public void GetDataForFormula4()
        {
            var totoWinnerUrl = $"https://www.winner.co.il/line/3757726"; //eventId
            WebRequest request = WebRequest.Create(totoWinnerUrl);
            WebResponse response = request.GetResponse();
            var jsonString = string.Empty;
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                jsonString = reader.ReadToEnd();
            }
        }
        [TestMethod]
        public void ParseGetDataForFormula4()
        {
            //var web = new HtmlWeb();
            //var totoWinnerUrl = $"https://www.winner.co.il/line/3757859";
            //var htmlDoc = web.Load(totoWinnerUrl);
            //var eventId = "3757859";// parameter
            var outcomeId = "32189429"; // parameter
            var takeChildHome = true;
            var filePath = @"C:\Users\ofirba\Projects\TotoWinner\TotoWinner.UnitTests\rawData.html";
            var htmlData = File.ReadAllText(filePath);
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlData);
            var nodes = htmlDoc.DocumentNode.SelectNodes("//div[@data-id='110123|100']");
            if (nodes.Count == 1)
            {
                var node = nodes[0];
                var outcome1 = node.SelectSingleNode($".//td[contains(@class, 'outcome_{outcomeId}')]");
                var titleNode = outcome1.SelectSingleNode(".//div[contains(@class, 'title')]");
                var title = titleNode.Attributes["title"].Value;
                takeChildHome = title.Contains("+"); //true - child home with parent away //false  -child away with parent home

                var span1 = outcome1.SelectSingleNode(".//span[contains(@class, 'formatted_price')]");
                var rate1 = span1.InnerText;

                var outcome2 = node.SelectSingleNode($".//td[contains(@class, 'ordpos_2')]");
                var titleNode2 = outcome2.SelectSingleNode(".//div[contains(@class, 'title')]");
                var span2 = outcome2.SelectSingleNode(".//span[contains(@class, 'formatted_price')]");
                var rate2 = span2.InnerText;

                var outcome3 = node.SelectSingleNode($".//td[contains(@class, 'ordpos_3')]");
                var span3 = outcome3.SelectSingleNode(".//span[contains(@class, 'formatted_price')]");
                var rate3 = span3.InnerText;

                var childPlusRateFromApi = Decimal.Parse("4.31");
                var childPlusRateFromSite = Decimal.Parse("4.30");
                var isSame = childPlusRateFromApi == childPlusRateFromSite;
            }
        }
    }
}