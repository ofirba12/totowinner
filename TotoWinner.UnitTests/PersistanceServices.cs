﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TotoWinner.Data;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class PersistanceServices
    {
        [TestMethod]
        public void PersistanceServices_AddOrUpdate_Success()
        {
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            //ACT
            var systemId = services.SystemAddOrUpdate(new SystemParameters()
            {
                Type = Data.SystemType.Shrink,
                Name = "צימצום זמני",
                ProgramId = 183150,
                ProgramType = Data.ProgramType.Winner16,
                SystemPayload = "{ yofi tofi }"
            });
            //ASSERT
            Assert.IsTrue(systemId > 0);
            services.SystemAddOrUpdate(new SystemParameters()
            {
                SystemId = systemId,
                Type = Data.SystemType.Shrink,
                Name = "צימצום זמני",
                ProgramId = 183150,
                ProgramType = Data.ProgramType.Winner16,
                SystemPayload = "{ yofi tofi updated}"
            });
        }
        [TestMethod]
        public void PersistanceServices_LoadSystem_Success()
        {
            //ACT
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            //ARRANGE
            var system = services.SystemLoad(1);
            //ASSERT
            Assert.IsNotNull(system.SystemPayload);
        }
        [TestMethod]
        public void PersistanceServices_LoadSystem_Failed()
        {
            try
            {
                //ACT
                //ARRANGE
                var services = Services.PersistanceServices.Instance;
                //ARRANGE
                var system = services.SystemLoad(-1);
            }
            catch(Exception )
            {
                //ASSERT
               Assert.Fail();
            }
        }
        [TestMethod]
        public void PersistanceServices_SystemDelete_Success()
        {
            //ACT
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            services.SystemDelete(1);
            //ARRANGE
            //ASSERT
        }
        [TestMethod]
        public void PersistanceServices_GetSystems_Success()
        {
            //ACT
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            var systems = services.GetSystems(Data.SystemType.Shrink);
            //ASSERT
        }
        [TestMethod]
        public void PersistanceServices_ExecutionUPSERT_Get_Success()
        {
            //ACT
            var services = Services.PersistanceServices.Instance;
            //ARRANGE
            var status1 = services.ExecutionGet(10);
            var exId = services.ExecutionStatusUpsert(null, SystemType.Optimizer, Data.ExecutionStatus.Queue, "{test}");
            var exId2 = services.ExecutionStatusUpsert(exId, SystemType.Optimizer, Data.ExecutionStatus.Running, "{test updated}");
            var status = services.ExecutionGet(exId2);
            //ASSERT
            Assert.IsTrue(exId == exId2);
            Assert.IsTrue(status.Status == Data.ExecutionStatus.Running);
            Assert.IsNull(status1);
        }
        [TestMethod]
        public void PersistanceServices_ExecutionOptimizedBetsUPSERT_Get_Success()
        {
            //ACT
            var services = Services.PersistanceServices.Instance;
            //ARRANGE
            services.ExecutionOptimizedBetsUpsert(10002, 1, "1X2", true);
            services.ExecutionOptimizedBetsUpsert(10002, 2, "1X", false);
            var result = services.ExecutionOptimizedBetsGet(10002);
            var result2 = services.ExecutionOptimizedBetsGet(10);
            //ASSERT
            Assert.IsTrue(result.Count == 2);
            Assert.IsTrue(result2.Count == 0);
        }
        [TestMethod]
        public void PersistanceServices_ExecutionGetQueued_Success()
        {
            //ACT
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            //ARRANGE
            var exe = services.ExecutionQueuedGet();
            //ASSERT
            Assert.IsNotNull(exe);
        }

        [TestMethod]
        public void PersistanceServices_OptimizedConditionResult_Success()
        {
            //ACT
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            //ARRANGE
            services.ExecutionOptimizedConditionUpsert(10200, "1", Data.ConditionType.Amount1X2, 1, 10);
            services.ExecutionOptimizedConditionUpsert(10200, "X", Data.ConditionType.Amount1X2, 2, 5);
            services.ExecutionOptimizedConditionUpsert(10200, "", Data.ConditionType.Range, 22.4, 35.6);
            services.ExecutionOptimizedConditionUpsert(10200, "", Data.ConditionType.Range, 25.4, 41.6);

            var conditions = services.ExecutionOptimizedConditionsGet(10200);
            //ASSERT
        }
        [TestMethod]
        public void PersistanceServices_OptimizedAdvancedConditionResult_Success()
        {
            //ACT
            //ARRANGE
            var services = Services.PersistanceServices.Instance;
            //ARRANGE
            services.ExecutionOptimizedAdvancedConditionUpsert(10200, Data.AdvancedConditionType.PairShape1X2, 1, 1, 10);
            services.ExecutionOptimizedAdvancedConditionUpsert(10200, Data.AdvancedConditionType.PairShape1X2, 1, 2, 5);
            services.ExecutionOptimizedAdvancedConditionUpsert(10200, Data.AdvancedConditionType.TripletShape1X2, 1, 3, 6);

            var conditions = services.ExecutionOptimizedAdvancedConditionsGet(10200);
            //ASSERT
        }
        [TestMethod]
        public void PersistanceServices_AddBacktestingBaseBets_Success()
        {
            var services = Services.PersistanceServices.Instance;
            var res = services.ExecutionGet(11674);

        }

            //[TestMethod]
            //public void PersistanceServices_AddBacktestingBaseBets_Success()
            //{
            //    //ACT
            //    //ARRANGE
            //    var services = Services.PersistanceServices.Instance;
            //    //ARRANGE
            //    var bets = new Dictionary<short, Bet>();
            //    bets.Add(1, new Bet(1, true, false, false));
            //    bets.Add(2, new Bet(2, true, true, false));
            //    bets.Add(3, new Bet(3, true, true, true));
            //    services.AddBacktestingBaseBets(12345, 105900, bets);
            //    //ASSERT
            //}
            //[TestMethod]
            //public void PersistanceServices_LoadSystem_Success()
            //{
            //    //ACT
            //    //ARRANGE
            //    var services = Services.PersistanceServices.Instance;
            //    //ARRANGE
            //    //ASSERT
            //}
        }
}
