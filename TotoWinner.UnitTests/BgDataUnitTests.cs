﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.Goalserve;
using TotoWinner.Data.Optimizer;
using TotoWinner.Services;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class BgDataUnitTests
    {
        [TestMethod]
        public void GenerateBgData()
        {
            var date = DateTime.Parse("24 Jan 2022");
            var endDate = DateTime.Parse("29 May 2022");
            var mapper = BgMapperServices.Instance.GetBgMapper();
            do
            {
                try
                {
                    date = date.AddDays(1);
                    var days = date - DateTime.Today;//27 / 1 - 22 / 2
                    GenerateBgData(date, mapper, BgSport.Soccer);
                    GenerateBgData(date, mapper, BgSport.Basketball);
                }
                catch (Exception)
                {

                }
            }
            while (date < endDate);
        }

        private static void GenerateBgData(DateTime date, MapperRespository mapper, BgSport sport)
        {
            var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed((GoalServeSportType)sport, date);
            var bgData = BgDataServices.Instance.GenerateData(goldserveFeed.Feed, mapper);
            var generatedFeed = BgDataServices.Instance.MergeDuplicates(bgData);
            var persistanceFeed = BgDataServices.Instance.GetBgData(sport, date, mapper);
            var finalFeed = BgDataServices.Instance.MergeAllFeeds(generatedFeed, persistanceFeed, new List<ManualMatch>());
            BgDataPersistanceServices.Instance.MergeBgFeed(finalFeed.BgFeedAddOrUpdate);
            var bbMatches = finalFeed.BgFeedAddOrUpdate.Where(m => m.Sport == BgSport.Basketball).ToList();
            if (bbMatches.Count > 0)
            {
                var pFeed = BgDataServices.Instance.GetBgData(BgSport.Basketball, date, mapper);
                var previousAddOrUpdate = bbMatches.ToDictionary(m => new BgFeedKey(m.Sport, m.GameStart, m.HomeId, m.AwayId), m => m);
                var bbMatchesToMerge = new List<BgMatch>();
                foreach (var match in pFeed.FeedWithNoDuplicates)
                {
                    if (previousAddOrUpdate.TryGetValue(match.Key, out var bbMatchAddOrUpdate))
                    {
                        bbMatchAddOrUpdate.SetMatchId(match.Value.MatchId);
                        bbMatchesToMerge.Add(bbMatchAddOrUpdate);
                    }
                }
                BgDataPersistanceServices.Instance.MergeQuatersBgFeed(bbMatchesToMerge);
            }
        }

        [TestMethod]
        public void GenerateMapping()
        {
            //var matchesToAdd = new HashSet<GoalserveMatch>(new GoalserveMatchComparer());
            //            var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed(GoalServeSportType.Soccer, DateTime.Parse("25 Jan 2022"));
            var date = DateTime.Parse("25 Jan 2022");
            var endDate = DateTime.Parse("22 Feb 2022");
            do
            {
                date = date.AddDays(1);
                var days = date - DateTime.Today;//27 / 1 - 22 / 2
                var goldserveFeed = GoalServeProviderServices.Instance.GetPersistanceFeed(GoalServeSportType.Basketball, days.Days);
                BgMapperServices.Instance.AddOrUpdateMapping(goldserveFeed);
            }
            while (date < endDate);
        }
        [TestMethod]
        public void MergeAllProviders()
        {
            var key1 = new BgFeedKey(BgSport.Soccer, DateTime.Parse("20/05/2022 16:00:00"), 104455, 100458);
            var match1 = new BgMatch(BgSourceProvider.Goldserve, BgSport.Soccer, 500654, "", 103, "", "", 10877, false, DateTime.Parse("20/05/2022 16:00:00"), "", 104455, "", 100458, DateTime.Parse("20/05/2022 3:00:00"));
            var key2 = new BgFeedKey(BgSport.Soccer, DateTime.Parse("20/05/2022 16:00:00"), 100458, 104455);
            var match2 = new BgMatch(BgSourceProvider.Goldserve, BgSport.Soccer, 502191, "", 140, "", "", 11132, true, DateTime.Parse("20/05/2022 16:00:00"), "", 100458, "", 104455, DateTime.Parse("20/05/2022 10:00:00"));
            var generatedFeed = new Dictionary<BgFeedKey, BgMatch>() { { key2, match2 } };
            var feedWithNoDuplicates = new Dictionary<BgFeedKey, BgMatch>() { { key1, match1 } };
            var feedWithDuplicates = new Dictionary<BgFeedKey, List<BgMatch>>();
            var all = new List<BgMatch>() { match1 };
            var persistanceFeed = new BgDataRepository(feedWithNoDuplicates, feedWithDuplicates, all);
            var manualDataByDate = new List<ManualMatch>();

            var finalFeed = BgDataServices.Instance.MergeAllFeeds(generatedFeed, persistanceFeed, manualDataByDate);
        }
        [TestMethod]
        public void MergeProviders()
        {
            var mapper = BgMapperServices.Instance.GetBgMapper();
            var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed(GoalServeSportType.Soccer, DateTime.Today.Date.AddDays(-1))
                .Feed
                .Where(m => m.CountryId == 1155 || m.CountryId == 18796 || m.CountryId == 20231)
                .ToList();

            var bgData = BgDataServices.Instance.GenerateData(goldserveFeed, mapper);
            var generatedFeed = BgDataServices.Instance.MergeDuplicates(bgData);
            var persistenceFeed = BgDataServices.Instance.GetBgData(BgSport.Soccer, DateTime.Today.Date.AddDays(-1), mapper);
            if (persistenceFeed.FeedWithDuplicates.Count > 0)
            {
                throw new Exception($"Merge failed with persistance duplicates feed for date {DateTime.Today.Date.AddDays(-1)}");
            }
            var manualDataByDate = ManualProviderServices.Instance.FetchManualMatchesByDate(DateTime.Today.Date.AddDays(-1))
                .Where(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue && m.Sport == BgSport.Soccer)
                .ToList();

            var finalFeed = BgDataServices.Instance.MergeAllFeeds(generatedFeed, persistenceFeed, manualDataByDate);
            MergeBgFeed(finalFeed.BgFeedAddOrUpdate);
            //BgDataPersistanceServices.Instance.MergeBgFeed(finalFeed.BgFeedAddOrUpdate);
        }
        public void MergeBgFeed(List<BgMatch> bgMatches)
        {
            try
            {
                var bgFeedProc = new FeedBgMergeStoredProcedure();
                var lastUpdate = DateTime.Now;
                bgFeedProc.Collection = bgMatches.ConvertAll(m => new FeedBgUDT()
                {
                    Source = (byte)m.Source,
                    SportId = (int)m.Sport,
                    CountryId = m.CountryId.Value,
                    LeagueId = m.LeagueId,
                    HomeId = m.HomeId,
                    AwayId = m.AwayId,
                    GameStart = m.GameStart,
                    Status = m.Status,
                    IsCup = m.IsCup,
                    HomeOdd = m.HomeOdd,
                    DrawOdd = m.DrawOdd,
                    AwayOdd = m.AwayOdd,
                    HomeScore = m.HomeScore,
                    AwayScore = m.AwayScore,
                    LastUpdate = lastUpdate
                });
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to merge bg mappers", ex);
            }
        }
        [TestMethod]
        public void FindDuplicates()
        {
            var mapper = BgMapperServices.Instance.GetBgMapper();
            var bgDataForDate = BgDataServices.Instance.GetBgData(BgSport.Soccer, DateTime.Today.AddDays(-1).Date, mapper);
            var candidateMatchId = new List<long>();
            foreach (var dup in bgDataForDate.FeedWithDuplicates)
            {
                var lastUpdate = dup.Value.Max(m => m.LastUpdate);
                candidateMatchId.AddRange(dup.Value.Where(m => m.LastUpdate < lastUpdate).Select(m => m.MatchId));
            }
        }
        [TestMethod]
        public void ManualMerge()
        {
            var mapper = BgMapperServices.Instance.GetBgMapper();
            var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed(GoalServeSportType.Soccer, DateTime.Today.Date);
            var bgData = BgDataServices.Instance.GenerateData(goldserveFeed.Feed, mapper);
            var filteredBgData = BgDataServices.Instance.MergeDuplicates(bgData);
            var manualDataByDate = ManualProviderServices.Instance.FetchManualMatchesByDate(DateTime.Today.Date.AddDays(-1))
                .Where(m => m.CountryId.HasValue && m.LeagueId.HasValue && m.HomeId.HasValue && m.AwayId.HasValue && m.Sport == BgSport.Soccer)
                .ToList();

            var finalFeed = BgDataServices.Instance.OverrideFeedWithManual(filteredBgData, manualDataByDate); // Manual feed must be last provider
        }
        [TestMethod]
        public void GetMappers()
        {
            var existingMapping = BgMapperServices.Instance.GetBgMapper();
        }
        [TestMethod]
        public void MapperMerge()
        {

            var persistance = BgDataPersistanceServices.Instance;
            var bgMappers = new List<BgMapper>()
            {
                new BgMapper(100, BgSport.Soccer, BgMapperType.Countries, "Israel"),
                new BgMapper(101, BgSport.Soccer, BgMapperType.Countries, "Argentina")
            };
            var bgGoalserveMapper = new List<BgGoalserveMapper>()
            {
                new BgGoalserveMapper(100, 1700, "Israel"),
                new BgGoalserveMapper(101, 1702, "Argentina"),
                new BgGoalserveMapper(100, 1703, "Israel")
            };
            persistance.MergeMappers(bgMappers, bgGoalserveMapper);
        }
        [TestMethod]
        public void GetMatchesByList()
        {
            var persistance = BgDataPersistanceServices.Instance;
            var feed = persistance.GoalserveMatchesOnlyGet(1, new List<int>() { 721547, 726144 });
        }
        [TestMethod]
        public void TestFeedHash()
        {
            var srv = GoalServeProviderServices.Instance;
            var persistance = BgDataPersistanceServices.Instance;
            //var request = new BgGoalServeFetchRequest(GoalServeSportType.Soccer,
            //    GoalServeMatchDayFetch.D_minus_1,
            //    GoalServeUrlType.Matches,
            //    null);
            //var json = srv.FetchRawData(request);
            var skippedInfo = new List<string>();
            //var repository = srv.ParseCategories(json, request, skippedInfo);
            var matches = new Dictionary<MatchKey, IBgGoalServeMatch>();
            matches.Add(new MatchKey(1234, 111, 29106, 28170), new BgGoalServeMatch("Oeste U20", 29106, "Flamengo RJ U20", 28170, "FT", 4518, DateTime.Parse("2022-01-12"), "00:45", 12345, new BgGoalServeMatchLocalVisitorResult("3", "3")));
            var odds = new Dictionary<int, List<BgGoalServeBookmaker>>();
            var leagues = new Dictionary<int, BgGoalServeLeague>();
            leagues.Add(4518, new BgGoalServeLeague(4518, "Brazil: Copa Sao Paulo De Juniores", "brazil", 4518, true));
            var repository = new BgGoalServeRepository(matches, odds, leagues, null);
            var feed = srv.GetPersistanceFeed(GoalServeSportType.Soccer, 0);
            srv.AddOrUpdate(repository, GoalServeSportType.Soccer, feed);

            //            193 0   FT  4518    brazil Brazil: Copa Sao Paulo De Juniores  4518    1   2022 - 01 - 12 00:45:00.000 Oeste U20   29106   Flamengo RJ U20 28170   NULL NULL    NULL    3   3   2022 - 01 - 12 07:52:08.153
        }
        [TestMethod]
        public void FeedBulkInsertTest()
        {
            var persistance = BgDataPersistanceServices.Instance;
            var matches = new List<GoalserveMatch>();
            var dateTime = DateTime.Today.Date;
            matches.Add(new GoalserveMatch((int)BgSport.Basketball,
                12345,
                "Postp.",
                1063,
                "ILS",
                "ILS LEAGUE",
                18931,
                true,
                dateTime,//DateTime.ParseExact("22/12/2021 17:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), //he-IL
                "ILS H",
                13126,
                "ILS A",
                8883,
                new OddsAndScores((decimal)1.89, (decimal)2.89,
                        (decimal)3.89,
                        2,
                        3,
                        new BasketballQuatersScores(12, 10, 11, 20, 33, 44, 22, 25))));

            persistance.GoalserveFeedInsert(matches);
        }
        [TestMethod]
        public void FeedBulkUpdateTest()
        {
            var persistance = BgDataPersistanceServices.Instance;
            var dateTime = DateTime.Today.Date;
            var matches = new List<GoalserveMatch>();
            matches.Add(new GoalserveMatch((int)BgSport.Basketball,
                123456,
                "Postp.",
                1063,
                "ILS",
                "ILS LEAGUE",
                18931,
                true,
                DateTime.Parse("2022-01-15 00:00:00.000"),// "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), //he-IL
                "ILS H",
                13126,
                "ILS A",
                8883,
                new OddsAndScores((decimal)1.89, (decimal)2.89,
                        (decimal)3.89,
                        2,
                        3,
                        new BasketballQuatersScores(1210, 1010, 1110, 2010, 3310, 4410, 2210, 2510))));//);

            persistance.GoalserveFeedUpdate(matches);

        }
        [TestMethod]
        public void BulkBgFeedStatistics()
        {
            var persistance = BgDataPersistanceServices.Instance;
            var dateTime = DateTime.Today.Date;
            var matches = new List<BgMatchStatistics>();
            matches.Add(new BgMatchStatistics(
                526710,
                1,
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34,
                (decimal)7.5,
                (decimal)6.4,
                DateTime.Now
                ));
            persistance.MergeBgFeedStatistics(matches);
        }
        [TestMethod]
        public void BulkBgFeedStatisticsDetails()
        {
            var persistance = BgDataPersistanceServices.Instance;
            var dateTime = DateTime.Today.Date;
            var matches = new List<BgMatchStatisticsDetails>();
            matches.Add(new BgMatchStatisticsDetails(
                526710,
                BgStatisticsDetailType.FullTime__HomeWin__HomeWin,
                0,
                BgTeamsFilter.All_All,
                5,
                83,
                DateTime.Now
                ));
            matches.Add(new BgMatchStatisticsDetails(
                526710,
                BgStatisticsDetailType.FullTime__HomeWin__HomeWin,
                1,
                BgTeamsFilter.All_Favorite,
                6,
                84,
                DateTime.Now
                ));
            persistance.MergeBgFeedStatisticsDetails(matches);
        }
        [TestMethod]
        public void FetchBgFeedStatistics()
        {
            var persistance = BgDataPersistanceServices.Instance;
            persistance.GetBgFeedStatistics(new List<long>() { 526710 });
        }
        [TestMethod]
        public void FetchBgFeedStatisticsDetails()
        {
            var persistance = BgDataPersistanceServices.Instance;
            var details = persistance.GetBgFeedStatisticsDetails(new List<long>() { 526710 })
                .ConvertAll<BgMatchStatisticsDetails>(m => new BgMatchStatisticsDetails(m.MatchId,
                (BgStatisticsDetailType)m.Type,
                m.StatisticIndex,
                (BgTeamsFilter)m.Population,
                m.Lines,
                m.Percent,
                m.LastUpdate
                ));
        }
        [TestMethod]
        public void GenerateFeedWinner()
        {
            var winnerSrv = BettingTipsServices.Instance;
            var persistance = BgDataWinnerPersistanceServices.Instance;
            //var sportsToParse = new List<SportIds>() { SportIds.Soccer, SportIds.Basketball };
            //Tennis - ok
            //Handball - ok
            //baeball - ok
            //football - no program
            var sportsToParse = new List<SportIds>() { SportIds.Baseball };
            var winnerLineJson = winnerSrv.GetWinnerLineResponse(DateTime.Today, false);
            var marketData = WebApiInvoker.JsonDeserializeDynamic(winnerLineJson);
            var dRounds = marketData["rounds"];
            var feed = new List<BgWinnerFeed>();
            var update = DateTime.Now;
            foreach (var dRound in dRounds)
            {
                var programId = BettingTipsServicesHelpers.DynamicResolver<long>(dRound, null, "id", ResolveEnum.LONG);
                var repository = winnerSrv.ParseWinnerLineResponse(marketData, programId, DateTime.Today.AddDays(1), false, sportsToParse);
                var feedData = (repository.Events as Dictionary<int, BettingTipsParentEvent>)
                    .Values.ToList()
                    .Where(m => Enum.TryParse<BetTypes>(m.Bet.BetTypeId.ToString(), out var betType))
                    .ToList()
                .ConvertAll<BgWinnerFeed>(m => new BgWinnerFeed(
                        m.EventId,
                        m.RoundId,
                        m.SportId,
                        m.TotoId,
                        m.HomeId,
                        m.HomeTeamName,
                        m.GuestId,
                        m.GuestTeamName,
                        (BetTypes)m.Bet.BetTypeId,
                        m.Place,
                        m.Bet.Name,
                        m.Time,
                        m.Status,
                        m.Bet.Home != -1 ? m.Bet.Home : (decimal?)null,
                        m.Bet.Draw != -1 ? m.Bet.Draw : (decimal?)null,
                        m.Bet.Away != -1 ? m.Bet.Away : (decimal?)null,
                        m.Country,
                        m.LeagueName,
                        update)
                    );
                feed.AddRange(feedData);
            }
            //persistance.WinnerFeedMerge(feed);
            //var currentMapper = persistance.GetWinnerMapper()
            //    .ToDictionary(m => m.WinnerId, m => m);
            //var mapper = new Dictionary<int, BgWinnerMapper>();
            //feed.ForEach(f =>
            //{
            //    if (!mapper.ContainsKey(f.HomeId) && !currentMapper.ContainsKey(f.HomeId))
            //        mapper.Add(f.HomeId, new BgWinnerMapper(f.SportId, f.HomeId, f.HomeTeam, null));
            //    if (!mapper.ContainsKey(f.GuestId) && !currentMapper.ContainsKey(f.GuestId))
            //        mapper.Add(f.GuestId, new BgWinnerMapper(f.SportId, f.GuestId, f.GuestTeam, null));
            //});
            //if (mapper.Values.Count > 0)
            //    persistance.MergeMappers(mapper.Values.ToList());

        }
        [TestMethod]
        public void CombineWinnerWithBgFeed()
        {
            var winPersistance = BgDataWinnerPersistanceServices.Instance;
            var winSrv = WinnerDataServices.Instance;
            var bgSrv = BgDataServices.Instance;
            var bgSport = BgSport.Soccer;
            var gameDate = DateTime.Today;
            var winnerSport = winSrv.BgSportToWinnerSport(bgSport);
            var winnerMapper = winSrv.GetWinnerMapper();
            var winnerData = winSrv.GetWinnerData(bgSport, gameDate, winnerMapper).Where(m => m.SportId == SportIds.Soccer
                                                                                ? m.BetTypeId == BetTypes.BetSoccer1x2
                                                                                : m.BetTypeId == BetTypes.BetBasketballAdvance || m.BetTypeId == BetTypes.BetBasketballAdvanceWithoutDraw);
            var bgMapper = BgMapperServices.Instance.GetBgMapper();
            //var bgData = bgSrv.GetBgData(bgSport, gameDate, bgMapper);
            var bgFeed = BgDataServices.Instance.GetBgFeedWithFilter(bgSport,
                    gameDate,
                    3,
                    null,
                    null,
                    bgMapper);
            var bgMatches = new Dictionary<BgFeedExactKey, BgMatch>();
            bgFeed.AllMatches.ForEach(m =>
            {
                var key = new BgFeedExactKey(bgSport, gameDate, m.HomeId, m.AwayId);
                if (!bgMatches.ContainsKey(key))
                {
                    bgMatches.Add(key, m);
                }
            }
            );
            //var bgMatches = bgFeed.AllMatches.ToDictionary(m => new BgFeedExactKey(bgSport, gameDate, m.HomeId, m.AwayId), m => m);
            var impliedStatistics = bgSrv.CreateImpliedStatistics(bgFeed.Matches, bgFeed.Statistics, bgFeed.StatisticsDetails);
            var combined = winSrv.CombineBgAndWinnerFeeds(bgSport, gameDate, winnerSport, winnerMapper, winnerData, bgMatches, impliedStatistics);
            var monitor = combined.ConvertAll<WinnerBgCombinedFeedMonitor>(m => new WinnerBgCombinedFeedMonitor(m, bgFeed));
        }
        [TestMethod]
        public void AllBgMatchWinnerMatcherNotStartedGames()
        {
            var bgSport = BgSport.Soccer;
            var gameDate = DateTime.Today; //GoalServeMatchDayFetch.Today
            var winPersistance = BgDataWinnerPersistanceServices.Instance;
            var winSrv = WinnerDataServices.Instance;
            var bgSrv = BgDataServices.Instance;
            var winnerSport = winSrv.BgSportToWinnerSport(bgSport);
            var winnerMapper = winSrv.GetWinnerMapper();
            var bgMapper = BgMapperServices.Instance.GetBgMapper();
            var winnerLineDate = DateTime.Today.Date; //Loop Over Yesterday, Today, Tommorrow
            var winnerData = winSrv.GetWinnerData(bgSport, winnerLineDate, winnerMapper);
            var bgFeed = BgDataServices.Instance.GetBgFeedWithFilter(bgSport,
                    winnerLineDate,
                    3,
                    null,
                    null,
                    bgMapper);
            var bgMatches = new Dictionary<BgFeedExactKey, BgMatch>();
            bgFeed.AllMatches.ForEach(m =>
                {
                    //could be same event/match in different programs
                    var key = new BgFeedExactKey(bgSport, winnerLineDate, m.HomeId, m.AwayId);
                    if (!bgMatches.ContainsKey(key))
                    {
                        bgMatches.Add(key, m);
                    }
                }
                );
            var combined = CombineBgAndWinnerFeedsWithNoBets(bgSport, winnerLineDate, winnerSport, winnerMapper, winnerData, bgMatches);
            var gamesNotStarted = combined.Where(g => g.BgMatch != null && g.WinnerMatch.GameStart > DateTime.Now).ToList();
            var openGamesBGLeaguesIds = new HashSet<(int LeagueId, int? CountryId)>(new TupleEqualityComparer());
            foreach (var item in gamesNotStarted)
            {
                openGamesBGLeaguesIds.Add((item.BgMatch.LeagueId, item.BgMatch.CountryId));
            }

            var goalserveLeagues = openGamesBGLeaguesIds.Select(l =>
                (bgMapper.BgIdBgMap[l.LeagueId],
                    l.CountryId.HasValue
                    ? bgMapper.BgIdBgMap[l.CountryId.Value]
                    : null))
                .ToList();
            var keyValuePairList = new List<KeyValuePair<int, int?>>();
            foreach (var (goalserveList, countryList) in goalserveLeagues)
            {
                for (int i = 0; i < goalserveList.Count; i++)
                    for (var j = 0; j < countryList.Count; j++)
                    {
                        keyValuePairList.Add(new KeyValuePair<int, int?>(goalserveList[i].GoalserveId.Value, countryList[j].GoalserveId));
                    }
            }
            //var goalserveLeaguesIds = goalserveLeagues.Select(i => i.Select(k => (k. )).SelectMany(i=> i).ToList();

            var goalserveSport = winSrv.BgSportToGoalserveSport(bgSport);
            BgGoalServeRepository previousRunRepository = null;
            var skippedInfo = new List<string>();
            var allOdds = new Dictionary<int, List<BgGoalServeBookmaker>>();
            var oddsFeedLastUpdate = new Dictionary<SportLeagueKey, long>();
            GoalServeProviderServices.Instance.ResetDumper();
            var currentLeague = 0;
            var prevLeague = 1;
            foreach (var leagueId in keyValuePairList)
            {
                currentLeague = leagueId.Key;
                try
                {
                    if (currentLeague == prevLeague)
                        continue;
                    var fetchOddRequest = new BgGoalServeFetchRequest(goalserveSport,
                        GoalServeMatchDayFetch.Today, //loop over 
                        GoalServeUrlType.Odds,
                        previousRunRepository,
                        currentLeague);
                    GoalServeProviderServices.Instance.ParseOdds(fetchOddRequest, skippedInfo, ref allOdds, oddsFeedLastUpdate, leagueId.Key, leagueId.Value);
                    prevLeague = leagueId.Key;
                }
                catch (Exception)
                {
                    //_log.Fatal($"An error occurred trying to parse scores:category[{categoryIndex}]", ex);
                }
            }
            //allOdds - holds matchId, and bookmakers (name + Dictionary of odds)
        }
        // Custom comparer for tuples
        public class TupleEqualityComparer : IEqualityComparer<(int LeagueId, int? CountryId)>
        {
            public bool Equals((int LeagueId, int? CountryId) x, (int LeagueId, int? CountryId) y)
            {
                return x.LeagueId == y.LeagueId && x.CountryId == y.CountryId;
            }

            public int GetHashCode((int LeagueId, int? CountryId) obj)
            {
                // Combine hash codes of the individual fields
                int hashLeagueId = obj.LeagueId.GetHashCode();
                int hashCountryId = obj.CountryId.HasValue ? obj.CountryId.Value.GetHashCode() : 0;
                return hashLeagueId ^ hashCountryId;
            }
        }
        //Put in Service winSrv
        public List<WinnerBgCombinedFeed> CombineBgAndWinnerFeedsWithNoBets(BgSport bgSport,
            DateTime gameDate,
            SportIds winnerSport,
            BgWinnerMapperRepository winnerMapper,
            IEnumerable<BgWinnerFeed> winnerData,
            Dictionary<BgFeedExactKey, BgMatch> bgMatches)
        {
            var combined = new List<WinnerBgCombinedFeed>();
            foreach (var data in winnerData)
            {
                var homeKey = new WinnerMapperKey(winnerSport, data.HomeId);
                var awayKey = new WinnerMapperKey(winnerSport, data.GuestId);
                if (winnerMapper.Mapper.ContainsKey(homeKey) && winnerMapper.Mapper.ContainsKey(awayKey))
                {
                    if (winnerMapper.Mapper[homeKey].BgId.HasValue && winnerMapper.Mapper[awayKey].BgId.HasValue)
                    {
                        var bgKey = new BgFeedExactKey(bgSport,
                            gameDate,
                            winnerMapper.Mapper[homeKey].BgId.Value,
                            winnerMapper.Mapper[awayKey].BgId.Value);
                        if (bgMatches.ContainsKey(bgKey))
                        {
                            var bgMatch = bgMatches[bgKey];
                            combined.Add(new WinnerBgCombinedFeed(data, bgMatch, null));
                        }
                        else
                        {
                            combined.Add(new WinnerBgCombinedFeed(data, null, null));
                        }
                    }
                }
            }

            return combined;
        }

        [TestMethod]
        public void TestMapper()
        {
            var bgMapper = BgMapperServices.Instance.GetBgMapper();
            var homeBgId = 104131;
            var awayBgId = 105429;
            var gameStart = DateTime.Parse("2025-02-21 22:30:00.000");//21/02/2025 3:30:00
            var goldserveFeed = GoalServeProviderServices.Instance.GetRawFeed(BgDataServices.Instance.BgSportToGoalserveSport(BgSport.Soccer), gameStart.Date);
            var goalserveMatch = BgMapperServices.Instance.GetGoalserveMatch(homeBgId, awayBgId, goldserveFeed, bgMapper);
            var odds = BgDataPersistanceServices.Instance.GetMatchOdds((int)GoalServeSportType.Soccer, goalserveMatch.HomeId, goalserveMatch.AwayId, goalserveMatch.GameStart);

            //var homeGoalserveId = bgMapper.BgIdBgMap[homeBgId].FirstOrDefault().GoalserveId;
            //var awayGoalserveId = bgMapper.BgIdBgMap[awayBgId].FirstOrDefault().GoalserveId;
            //var gameStart = DateTime.Parse("2025-02-09 14:00:00.000");
            //Call new SP, that gets goalservve MatchId + Odds
            /*
             * select FGO.* from Feed_Goalserve FG
            Join [Feed_Goalserve_Odds] FGO
            ON FG.MatchId = FGO.MatchId
            where FG.SportId = 0 and FG.HomeId=31798 and AwayId=31800 AND GameStart='2025-02-10 14:00:00.000'
            */
        }
    }
}

