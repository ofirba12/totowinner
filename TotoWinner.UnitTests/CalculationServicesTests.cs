﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.UnitTests
{
    [TestClass]
    public class CalculationServicesTests
    {
        private Uri _totoWinnerUrl = new Uri("http://localhost/TotoWinner/api/TotoWinnerBets/CalculateBets");
        //private Uri _totoWinnerUrl = new Uri("http://3.125.32.136/TotoWinner/api/TotoWinnerBets/CalculateBets");
        private Uri _saveUrl = new Uri("http://localhost/TotoWinner/api/TotoWinnerBets/SaveShrinkSystemForm");
        //private Uri _saveUrl = new Uri("http://3.125.32.136/TotoWinner/api/TotoWinnerBets/SaveShrinkSystemForm");
        private Uri _saveToExcelUrl = new Uri("http://localhost/TotoWinner/api/TotoWinnerBets/ResultsToExcel");
        private Dictionary<int, int> _scenarioResults = new Dictionary<int, int>()
        {
            { 1, 180 }, { 2, 139 }, { 3, 83 },{ 4, 28 }, { 5, 1 }, { 6, 11 },{ 7, 9 }, { 8, 30 }, { 9, 10 },{ 10, 30 }
        };

        private BetsCalculationInputs CreateBetsCalculationInputs(int scenario)
        {
            var calcService = CalculationServices.Instance;
            var request = new BetsFormCreateRequest();
            FillFormRequest(request, scenario);
            request.Validate();
            var constrSettings = calcService.BuildConstrSettings(request);
            var probSettings = calcService.BuildProbSettings(request);
            var validators = calcService.BuildValidators(request, probSettings);
            var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, false);

            return betInputs;
        }
        #region CalculateBets_UseConstrants_Probabality_Include_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_Include_Success()
        {
            //ARRANGE
            var scenario = 1;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Success()
        {
            //ARRANGE
            var scenario = 2;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_Success()
        {
            //ARRANGE
            var scenario = 3;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_Letter_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_Letter_Success()
        {
            //ARRANGE
            var scenario = 4;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_LetterMinMax_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_LetterMinMax_Success()
        {
            //ARRANGE
            var scenario = 5;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_LetterMinMax_2_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Combinations_LetterMinMax_2_Success()
        {
            //ARRANGE
            var scenario = 6;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Shapes2_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Shapes2_Success()
        {
            //ARRANGE
            var scenario = 7;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);

        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_Shapes3_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_Shapes3_Success()
        {
            //ARRANGE
            var scenario = 8;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_LetterShapes2_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_LetterShapes2_Success()
        {
            //ARRANGE
            var scenario = 9;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);

        }
        #endregion

        #region CalculateBets_UseConstrants_Probabality_IncludeExclude_LetterShapes2_Success
        [TestMethod]
        public void CalculateBets_UseConstrants_Probabality_IncludeExclude_LetterShapes3_Success()
        {
            //ARRANGE
            var scenario = 10;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);

            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);

        }
        #endregion

        #region FillFormRequest
        internal static void FillFormRequest(BetsFormCreateRequest request, int scenarioId)
        {

            #region ProbSettings
            request.EventsRates = new List<RatesLine>();
            request.EventsRates.Add(new RatesLine() { position = 0, rate1 = 1, rateX = 1, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 1, rate1 = 1, rateX = 2, rate2 = 3 });
            request.EventsRates.Add(new RatesLine() { position = 2, rate1 = 1, rateX = 1, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 3, rate1 = 2, rateX = 2, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 4, rate1 = 1, rateX = 2, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 5, rate1 = 2, rateX = 1, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 6, rate1 = 1, rateX = 2, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 7, rate1 = 2, rateX = 1, rate2 = 1 });
            request.EventsRates.Add(new RatesLine() { position = 8, rate1 = (float)2.55, rateX = (float)2.8, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 9, rate1 = (float)2.55, rateX = (float)2.8, rate2 = 2 });
            request.EventsRates.Add(new RatesLine() { position = 10, rate1 = (float)1.95, rateX = (float)2.85, rate2 = (float)2.6 });
            request.EventsRates.Add(new RatesLine() { position = 11, rate1 = (float)2.25, rateX = (float)2.6, rate2 = (float)2.4 });
            request.EventsRates.Add(new RatesLine() { position = 12, rate1 = (float)2.4, rateX = (float)2.6, rate2 = (float)2.25 });
            request.EventsRates.Add(new RatesLine() { position = 13, rate1 = (float)2.2, rateX = (float)2.6, rate2 = (float)2.45 });
            request.EventsRates.Add(new RatesLine() { position = 14, rate1 = (float)2.15, rateX = (float)2.6, rate2 = (float)2.5 });
            request.EventsRates.Add(new RatesLine() { position = 15, rate1 = (float)2.65, rateX = (float)2.6, rate2 = (float)2.05 });
            #endregion
            #region ConstraintsSettings
            request.EventsBets = new List<Bet>();
            request.EventsBets.Add(new Bet() { position = 0, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 1, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 2, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 3, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 4, bet1 = true, betX = true, bet2 = true });
            request.EventsBets.Add(new Bet() { position = 5, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 6, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 7, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 8, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 9, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 10, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 11, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 12, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 13, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 14, bet1 = true, betX = false, bet2 = false });
            request.EventsBets.Add(new Bet() { position = 15, bet1 = true, betX = false, bet2 = false });
            #endregion
            #region Include Settings
            request.FormSettings = new FormSettings();
            request.FormSettings.IncludeSettings = new IncludeSettings();
            request.FormSettings.IncludeSettings.amountSum = new MinMax() { Min = 16, Max = 100 };
            request.FormSettings.IncludeSettings.amount1 = new MinMax() { Min = 5, Max = 16 };
            request.FormSettings.IncludeSettings.amountX = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.amount2 = new MinMax() { Min = 1, Max = 16 };
            //request.formSettings = new FormSettings(request.eventsRatesList.Count, includeSettings);

            request.FormSettings.IncludeSettings.sequenceBreak = new MinMax() { Min = 1, Max = 15 };
            request.FormSettings.IncludeSettings.sequenceX = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.sequence1 = new MinMax() { Min = 1, Max = 16 };
            request.FormSettings.IncludeSettings.sequence2 = new MinMax() { Min = 1, Max = 16 };
            if (scenarioId != 1)
            {
                #region Exclude Settings
                //var amountSum = new MinMax() { Min = 15, Max = 30 };
                //request.formSettings.ExcludeSettings = new ExcludeSettings((uint)request.eventsRatesList.Count, amountSum);
                request.FormSettings.ExcludeSettings = new ExcludeSettings();
                request.FormSettings.ExcludeSettings.amountSum = new MinMax() { Min = 15, Max = 30 };
                request.FormSettings.ExcludeSettings.amount1 = new MinMax();
                request.FormSettings.ExcludeSettings.amountX = new MinMax();
                request.FormSettings.ExcludeSettings.amount2 = new MinMax();
                request.FormSettings.ExcludeSettings.ActivateSumAmountFilter = true;

                request.FormSettings.ExcludeSettings.sequenceBreak = new MinMax();
                #endregion
            }
            if (scenarioId > 2)
            {
                request.FormSettings.Combination1X2Settings = new List<CombinationSetting>()
                {
                    new CombinationSetting(new string[] { "1","","2x","","","","","","","","","","","","","2x" }, new MinMax(1,3), true, CombinationType.Pattern1X2),
                    new CombinationSetting(new string[] { "","1","","2x","","","","","","","","","","","","" }, new MinMax(1,2), true, CombinationType.Pattern1X2)
                };
            }
            if (scenarioId > 3)
            {
                var letterSettings = new List<CombinationSetting>()
                {
                    new CombinationSetting(new string[] { "A","","","","","","","","","","","","","","","" }, new MinMax(1,2), true, CombinationType.PatternLetters),
                    new CombinationSetting(new string[] { "","","","AB","","","","","","","","","","","","" }, new MinMax(1,3), true, CombinationType.PatternLetters)
                };
                if (scenarioId == 4)
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(letterSettings,
                        null, null, null, null, null, null, null);
                else if (scenarioId == 5)
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(letterSettings,
                        new MinMax(5, 16), new MinMax(5, 16), new MinMax(5, 16), null, null, null, null);
                if (scenarioId == 6)
                {
                    letterSettings = new List<CombinationSetting>()
                    {
                        new CombinationSetting(new string[] { "A","","","","","","","","","","","","","","","" }, new MinMax(1,2), false, CombinationType.PatternLetters),
                        new CombinationSetting(new string[] { "","","","AB","","","","","","","","","","","","" }, new MinMax(1,3), true, CombinationType.PatternLetters)
                    };
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(letterSettings,
                        new MinMax(5, 16), new MinMax(5, 16), new MinMax(5, 16), null, null, null, null);
                }
                if (scenarioId == 7)
                {
                    request.FormSettings.Combination1X2Settings = null;
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(null,
                        new MinMax(5, 16), new MinMax(5, 16), new MinMax(5, 16), null, null, null, null);
                    request.FormSettings.Shapes2 = new List<ShapesSettings>()
                    {
                        new ShapesSettings(new MinMax[]
                            {
                              new MinMax(1,15),
                              new MinMax(2,15),
                              new MinMax(3,15),
                              new MinMax(0,15),
                              new MinMax(0,15),
                              new MinMax(0,15),
                              new MinMax(0,15),
                              new MinMax(2,2),
                              new MinMax(2,9)
                            }, new MinMax(6,9), true, ShapeFilterType.RegularShapes2)
                    };
                }
                if (scenarioId == 8)
                {
                    request.FormSettings.Combination1X2Settings = null;
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(null,
                        new MinMax(5, 16), new MinMax(5, 16), new MinMax(5, 16), null, null, null, null);
                    request.FormSettings.Shapes3 = new List<ShapesSettings>()
                    {
                        new ShapesSettings(new MinMax[]
                            {
                              new MinMax(1,14), new MinMax(1,14),new MinMax(1,14),//1
                              new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                              new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                              new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                              new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),//5
                              new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                              new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                              new MinMax(0,14), new MinMax(0,14),
                              new MinMax(0,14), new MinMax(0,14),
                              new MinMax(2,14), new MinMax(1,14)//10
                            }, new MinMax(1,27), true, ShapeFilterType.RegularShapes3)
                    };
                }
                if (scenarioId == 9)
                {
                    request.FormSettings.Combination1X2Settings = null;
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(null,
                        new MinMax(5, 16), new MinMax(5, 16), new MinMax(5, 16), null, null, null, null);
                    request.FormSettings.LettersShapes2 = new List<ShapesSettings>()
                    {
                        new ShapesSettings(new MinMax[]
                            {
                              new MinMax(10,15),
                              new MinMax(4,15),
                              new MinMax(3,15),
                              new MinMax(1,15),
                              new MinMax(5,15),
                              new MinMax(6,15),
                              new MinMax(6,15),
                              new MinMax(6,15),
                              new MinMax(3,15)
                            }, new MinMax(2,9), true, ShapeFilterType.LetterShapes2)
                    };
                }
                if (scenarioId == 10)
                {
                    request.FormSettings.Combination1X2Settings = null;
                    request.FormSettings.CombinationLettersSettings = new CombinationLettersSettings(null,
                        new MinMax(5, 16), new MinMax(5, 16), new MinMax(5, 16), null, null, null, null);
                    request.FormSettings.LettersShapes3 = new List<ShapesSettings>()
                    {
                        new ShapesSettings(new MinMax[]
                            {
                                new MinMax(1,14), new MinMax(10,14),new MinMax(3,14),//1
                                new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                                new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                                new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                                new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),//5
                                new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                                new MinMax(0,14), new MinMax(0,14),new MinMax(0,14),
                                new MinMax(0,14), new MinMax(0,14),
                                new MinMax(0,14), new MinMax(5,14),
                                new MinMax(5,14), new MinMax(12,14)//10
                            }, new MinMax(2,21), true, ShapeFilterType.LetterShapes3)
                    };
                }
            }
            #endregion

        }
        #endregion

        #region WebApi_CalculateBets_Success
        [TestMethod]
        public void WebApi_CalculateBets_Success()
        {
            var calcService = CalculationServices.Instance;
            for (var i = 1; i <= 10; i++)
            {
                //ARRANGE
                var request = new BetsFormCreateRequest();
                FillFormRequest(request, i);
                request.Validate();
                var constrSettings = calcService.BuildConstrSettings(request);
                var probSettings = calcService.BuildProbSettings(request);
                var validators = calcService.BuildValidators(request, probSettings);
                var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, false);

                //ACT
                var response = calcService.CalculateBets(betInputs);
                dynamic dynamicJson = new ExpandoObject();
                dynamicJson.eventsBets = request.EventsBets;
                dynamicJson.eventsRates = request.EventsRates;
                dynamicJson.formSettings = request.FormSettings;
                var apiResponse = WebApiInvoker.Post<BetsCalculationResultsResponse>(_totoWinnerUrl, dynamicJson);

                //if (crmResponse.IsSuccess)

                //ASSERT
                Assert.IsNotNull(response);
                Assert.IsNotNull(apiResponse);
                Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[i]);
                Assert.IsTrue(apiResponse.seResults.FinalResults.Count == _scenarioResults[i]);
            }
        }
        #endregion

        #region CalculateBets_SaveToExcel_Success
        [TestMethod]
        public void CalculateBets_SaveToExcel_Success()
        {
            //ARRANGE
            var scenario = 1;
            var betInputs = CreateBetsCalculationInputs(scenario);

            //ACT
            var calcService = CalculationServices.Instance;
            var response = calcService.CalculateBets(betInputs);
            var fileName = $@"Output\{DateTime.Now.ToString("ddMMyyyy.HHmmss.fff")}_טופס_מצומצם.xlsx";//TODO: handle file existance

            var excelFile = calcService.ResultsToExcel(response, false, fileName, "OutputTemplate.xlsx");
            //ASSERT
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[scenario]);
            Assert.IsTrue(File.Exists(excelFile));
        }
        #endregion

        #region WebApi_ResultsToExcel_Success
        [TestMethod]
        public void WebApi_ResultsToExcel_Success()
        {
            var i = 3;
            var calcService = CalculationServices.Instance;
            //ARRANGE
            var request = new BetsFormCreateRequest();
            FillFormRequest(request, i);
            request.Validate();
            var constrSettings = calcService.BuildConstrSettings(request);
            var probSettings = calcService.BuildProbSettings(request);
            var validators = calcService.BuildValidators(request, probSettings);
            var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, false);
            request.GenerateOutputFile = false;

            //ACT
            var response = calcService.CalculateBets(betInputs);
            dynamic dynamicBFCRJson = new ExpandoObject();
            dynamicBFCRJson.eventsBets = request.EventsBets;
            dynamicBFCRJson.eventsRates = request.EventsRates;
            dynamicBFCRJson.formSettings = request.FormSettings;
            var apiCalcResponse = WebApiInvoker.Post<BetsCalculationResultsResponse>(_totoWinnerUrl, dynamicBFCRJson);
            //ASSERT
            Assert.IsNotNull(apiCalcResponse);
            Assert.IsNotNull(response);
            Assert.IsTrue(response.FinalResults.Count() == _scenarioResults[i]);
            Assert.IsTrue(apiCalcResponse.seResults.FinalResults.Count == _scenarioResults[i]);

            dynamic dynamicRTEJson = new ExpandoObject();
            dynamicRTEJson.seResults = apiCalcResponse.seResults;
            dynamicRTEJson.cleanOneDiff = request.CleanOnDiff;
            var apiRTEResponse = WebApiInvoker.Post<ResultsToExcelResponse>(_saveToExcelUrl, dynamicRTEJson);
            //ASSERT
            Assert.IsNotNull(apiRTEResponse);
            Assert.IsNotNull(apiRTEResponse.OutputFile);
        }
        #endregion
        #region WebApi_SaveShrinkSystemForm_Success
        [TestMethod]
        public void WebApi_SaveShrinkSystemForm_Success()
        {
            var persistanceServices = Services.SystemsServices.Instance;
            var calcService = CalculationServices.Instance;
            //ARRANGE
            var saveRequest = new SaveShrinkSystemRequest();
            var system = new BetsFormCreateRequest();
            FillFormRequest(system, 1);
            system.Validate();
            var constrSettings = calcService.BuildConstrSettings(system);
            var probSettings = calcService.BuildProbSettings(system);
            var validators = calcService.BuildValidators(system, probSettings);
            var betInputs = new BetsCalculationInputs(probSettings, constrSettings, validators, false);

            //ACT
            saveRequest.System = system;
            saveRequest.UniqueId = "18420#96";
            //var response = persistanceServices.SaveSystem(saveRequest)
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.UniqueId = saveRequest.UniqueId;
            dynamicJson.System = saveRequest.System;
            var apiResponse = WebApiInvoker.Post<SaveShrinkSystemResponse>(_saveUrl, dynamicJson);


            //ASSERT
            Assert.IsNotNull(apiResponse);
        }
        #endregion


    }
}
