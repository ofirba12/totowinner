﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using TotoWinner.Data.BgBettingData;
using TotoWinner.Data.BgBettingData.BgDataApi;
using TotoWinner.Services;

namespace BgData.API.Controllers
{
    [EnableCors(origins: "http://localhost:3000,http://localhost:80", headers: "*", methods: "*")]
    [RoutePrefix("api/Scores")]
    public class ScoresController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly BgMapperRepository _mapper = new BgMapperRepository(1440);

        #region GetBgMatches
        [Route("GetBgMatches")]
        [HttpPost]
        public IHttpActionResult GetBgMatches([FromBody] BgDataScoreRequest request)
        {
            try
            {
                var leaguesToShow = BgDataLeaguesWhitelistServices.Instance.GetAllBgLeaguesIdsWhitelist();
                var repo = BgDataServices.Instance.GetBgMatchesFeedWithFilter(request.Sport,
                    request.FetchDate,
                    request.TimezoneOffset/60,
                    leaguesToShow,
                    _mapper.GetBgMapper());
                var impliedStatistics = BgDataServices.Instance.CreateImpliedStatistics(repo.Matches, repo.Statistics, repo.StatisticsDetails);
                var scoreData = BgDataScoresServices.Instance.PrepareScoreData(repo, impliedStatistics, request.Sport);

                var bgData = new BgDataScoreResponse(request.FetchDate, request.TimezoneOffset / 60, scoreData );
                return Ok(bgData);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get all GetBgMatches", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion
        
        #region GetMatchSummaryDetails
        [Route("GetMatchSummaryDetails")]
        [HttpPost]
        public IHttpActionResult GetMatchSummaryDetails([FromBody] BgMatchSummaryDetailsRequest request)
        {
            try
            {
                var summaryDetails = BgDataScoresServices.Instance.PrepareMatchSummaryDetails(request.MatchId,
                    request.TimezoneOffset / 60,
                    _mapper.GetBgMapper());

                var bgMatchSummaryDetails = new BgMatchSummaryDetailsResponse(summaryDetails);
                return Ok(bgMatchSummaryDetails);
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occured trying to get all GetMatchSummaryDetails", ex);
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region GetTeasers
        [Route("GetTeasers")]
        [HttpPost]
        public IHttpActionResult GetTeasers()
        {
            //try
            //{
            //    var teasers = BgDataScoresServices.Instance.PrepareTeasers(_mapper.GetBgMapper());

            //    var bgTeasers = new GetTeasersResponse(teasers);
            //    return Ok(bgTeasers);
            //}
            //catch (Exception ex)
            //{
            //    _log.Fatal($"An error occured trying to get teasers", ex);
            //    return BadRequest(ex.Message);
            //}
            return Ok();
/**
 * 
 * 
 * 
{
  "successPercentage": "55%",
  "totalWins": "4974",
  "totalBets": "9034",
  "bgDataBets": [
    {
      "sport": "soccer",
      "country": "Brazil",
      "league": "Serias A",
      "homeTeam": "Sao Paulo",
      "homeId": "100127",
      "awayTeam": "Athletico-PR",
      "awayId": "101109",
      "score": "1-0",
      "status": "ST",
      "betDetails": {
        "type": "Both Team Score",
        "title": "Home Win",
        "stars": "5",
        "description": [
          "Out of the last 6 games played by FUS Rabat, FUS Rabat has a win rate of 67%.",
          "Out of the last 5 games played by Chabab Mohammedia, when Chabab Mohammedia played away and was not favorite to win, Chabab Mohammedia has a loss rate of 80%.",
          "Out of the last 5 games played by Chabab Mohammedia, Chabab Mohammedia has a loss rate of 50%.",
          "Out of the last 2 games played by FUS Rabat with head-to-head matches against Chabab Mohammedia, 50% of the matches FUS Rabat won."
        ],
        "odd": "1.44",
        "mark": "1"
      }
    },
    {
      "sport": "soccer",
      "country": "Brazil2",
      "league": "Serias A2",
      "homeTeam": "FC Cincinnati",
      "homeId": "104742",
      "awayTeam": "Toronto FC",
      "awayId": "104740",
      "score": "2-2",
      "status": "ST",
      "betDetails": {
        "type": "Bet Range",
        "title": "Range 2-3",
        "stars": "4",
        "description": [
          "Out of the last 6 games played by FC Cincinnati, when they are favored to win, 50% of the matches ended with range of 2-3 goals.",
          "Out of the last 6 games played by Toronto FC, 67% of the matches ended with range of 2-3 goals.",
          "Out of the last 6 games played by FC Cincinnati, 33% of the matches ended with range of 2-3 goals.",
          "Out of the last 3 games played by FC Cincinnati with head-to-head matches against Toronto FC, 100%  of the matches ended with range of 2-3 goals."
        ],
        "odd": "1.25",
        "mark": "X"
      }
    }
  ]
}


 */
        }
        #endregion
    }
}
