﻿namespace TotoWinner.Execution
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.executionServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.executionServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // executionServiceProcessInstaller
            // 
            this.executionServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.executionServiceProcessInstaller.Password = null;
            this.executionServiceProcessInstaller.Username = null;
            // 
            // executionServiceInstaller
            // 
            this.executionServiceInstaller.DelayedAutoStart = true;
            this.executionServiceInstaller.Description = "Executes systems with a vast number of calculations";
            this.executionServiceInstaller.DisplayName = "Toto Winner Execution Service";
            this.executionServiceInstaller.ServiceName = "TotoWinnerExecutionService";
            this.executionServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.executionServiceProcessInstaller,
            this.executionServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller executionServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller executionServiceInstaller;
    }
}