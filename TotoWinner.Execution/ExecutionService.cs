﻿using log4net;
using Newtonsoft.Json;
using System;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using TotoWinner.Data;
using TotoWinner.Services.Algo;

namespace TotoWinner.Execution
{
    public partial class ExecutionService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private System.Timers.Timer _workSeekTimer = null;
        private Services.ExecutionServices _executionSrv = Services.ExecutionServices.Instance;
        private Services.CalculationServices _calculationSrv = Services.CalculationServices.Instance;
        private Services.SystemsServices _systemSrv = Services.SystemsServices.Instance;
        private Services.PersistanceServices _persistanceServices = Services.PersistanceServices.Instance;
        public ExecutionService()
        {
            this.ServiceName = "ExecutionService";
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("Execution service started");
            try
            {
                this._workSeekTimer = new System.Timers.Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
                this._workSeekTimer.Enabled = true;
                this._workSeekTimer.AutoReset = true;
                this._workSeekTimer.Elapsed += SeekWorkTimer_Elapsed;// WorkSeekTimer_Elapsed;
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to start execution service", ex);
            }
        }

        private void SeekWorkTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                this._workSeekTimer.Stop();
                var execution = _persistanceServices.ExecutionQueuedGet();
                if (execution != null && execution.ExecutionId > 0)
                {
                    Action<CancellationToken> calcWork = null;
                    switch (execution.SystemType)
                    {
                        case SystemType.Optimizer:
                            var workOpt = GetOptimizerExecutionData(execution);
                            calcWork = (token) =>
                            {
                                var optimizer = new OptimizerAlgo();
                                optimizer.RunOptimizerGamesAlgo(token, workOpt);
                            };
                            break;
                        case SystemType.Backtesting:
                            var workBt = GetBacktestingExecutionData(execution);
                            calcWork = (token) =>
                            {
                                var backtesting = new BacktestingAlgo();
                                backtesting.RunAlgo(token, workBt, false, false);
                            };
                            break;
                        default:
                            throw new Exception($"Work of type {execution.SystemType} is not supported");
                    }
                    ExecuteWork(execution.ExecutionId, execution.SystemType, calcWork);
                    _log.Info($"Work finished");
                }
                this._workSeekTimer.Start();
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to seek for work", ex);
                this._workSeekTimer.Start();
            }
        }

        private OptimizerExecutionData GetOptimizerExecutionData(ExecutionRecord execution)
        {
            var parameters = JsonConvert.DeserializeObject<ExecutionOptimizerParameters>(execution.Parameters);
            var work = new OptimizerExecutionData()
            {
                ExecutionId = execution.ExecutionId,
                Status = execution.Status,
                Parameters = parameters
            };
            _log.Info($"Work found: {work}");
            if (work.Parameters.TargetBetsCounter == 0)
                throw new Exception("work found has TargetBetsCounter zero");
            return work;
        }
        private BacktestingExecutionData GetBacktestingExecutionData(ExecutionRecord execution)
        {
            var definitions = JsonConvert.DeserializeObject<BacktestingDefinitions>(execution.Parameters);
            var work = new BacktestingExecutionData()
            {
                ExecutionId = execution.ExecutionId,
                Status = execution.Status,
                Definitions = definitions
            };
            _log.Info($"Work found: {work}");
            //if (work.Parameters.TargetBetsCounter == 0)
            //    throw new Exception("work found has TargetBetsCounter zero");
            return work;
        }
        private void ExecuteWork(int executionId, SystemType systemType, Action<CancellationToken> calcWorkAction)
        {
            try
            {
                _executionSrv.UpdateExecutionStatus<SearchResults>(executionId, systemType, ExecutionStatus.Running, null);
                using (var cts = new CancellationTokenSource())
                {
                    Action pollWork = () =>
                    {
                        try
                        {
                            //while (_executionSrv.GetExecutionStatus(executionId).Status == ExecutionStatus.Running)
                            ExecutionStatus status = ExecutionStatus.None;
                            do
                            {
                                status = _executionSrv.GetExecutionStatus(executionId).Status;
                                Thread.Sleep(1000);
                            }
                            while (status == ExecutionStatus.Running);
                            _log.Info($"Polling status found: {status}");
                            cts.Cancel();
                        }
                        catch (Exception ex)
                        {
                            _log.Fatal($"An error occurred while polling for execution status", ex);
                            cts.Cancel();
                        }
                    };
                    var taskCalc = Task.Factory.StartNew(() => calcWorkAction(cts.Token), cts.Token);
                    var taskCheck = Task.Factory.StartNew(pollWork);

                    Task.WaitAll(
                        taskCalc.ContinueWith(t => { }),
                        taskCheck.ContinueWith(t => { })
                        );
                }
            }
            catch (Exception ex)
            {
                _log.Fatal($"An error occurred trying to work on execution id: {executionId}", ex);
                _executionSrv.UpdateExecutionStatus<SearchResults>(executionId, systemType, Data.ExecutionStatus.Error, null);
            }
        }

        protected override void OnStop()
        {
            try
            {
                _log.Info("Execution service stopped");
                this._workSeekTimer.Stop();
                this._workSeekTimer.Close();
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to stop execution service", ex);
            }
        }
    }
}
