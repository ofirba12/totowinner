﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.Backtesting
{
    public class GroupsRepository
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int numberOfItemsPerGroup = 50000000;
        private const string groupsDataFilename = @"TWN-GROUPS.txt";
        public string GroupsTxtFilesFolder { get { return "TXT"; } }
        private Dictionary<int, string[]> chunks = new Dictionary<int, string[]>();
        //private List<string> bankersGroupKeys = new List<string>();// { "A","B","C","D","E","F","G","H","I","J","K","L" };
        //private List<string> doublesGroupKeys = new List<string>();
        public Templates Templates = new Templates();
        public int BankersSize { get; private set; }
        public int DoublesSize { get; private set; }
//        public Dictionary<string, GroupData> groups = new Dictionary<string, GroupData>();
        public Dictionary<int, List<List<string>>> bankersTemplateSizeGroupExpansion = new Dictionary<int, List<List<string>>>();
        public Dictionary<int, List<List<string>>> doublesTemplateSizeGroupExpansion = new Dictionary<int, List<List<string>>>();
        public Dictionary<string, List<List<string>>> finalBankers = new Dictionary<string, List<List<string>>>();
        public Dictionary<string, List<List<string>>> finalDoubles = new Dictionary<string, List<List<string>>>();
        private Dictionary<int, string> finalPermutationsIndexKey = new Dictionary<int, string>(); // 1, "A1-B1"
        public int NumberOfFinalGroups { private set; get; }
        private Dictionary<int, List<string>> finalGroupsKeys = new Dictionary<int, List<string>>();
        public int ChosenExecutionGroup { get; private set; }
        public int FromOptionId { get; private set; }
        public int ToOptionId { get; private set; }
        private TemplatesServices templateSrv = TemplatesServices.Instance;

        public GroupsRepository(int numberOfPermutations, int bankers, int doubles)
        {
            this.BankersSize = bankers;
            this.DoublesSize = doubles;
            Initalize(numberOfPermutations);
        }

        private void Initalize(int numberOfPermutations)
        {
            this.Templates.Generate(numberOfPermutations);
            //ReadGroupData();            
            for (var i = 1; i <= numberOfPermutations; i++)
            {
                this.bankersTemplateSizeGroupExpansion.Add(i, null);
                var outputGroups = new List<List<string>>();
                ExpandGroup(i, templateSrv.BankersGroupKeys, ref outputGroups);
                this.bankersTemplateSizeGroupExpansion[i] = outputGroups;
                outputGroups = new List<List<string>>();
                this.doublesTemplateSizeGroupExpansion.Add(i, null);
                ExpandGroup(i, templateSrv.DoublesGroupKeys, ref outputGroups);
                this.doublesTemplateSizeGroupExpansion[i] = outputGroups;
            }
            this.NumberOfFinalGroups = 1;
            this.ChosenExecutionGroup = 1;
            if (this.BankersSize > 0)
                ProcessTemplates(this.BankersSize, ref bankersTemplateSizeGroupExpansion, ref this.finalBankers);
            if (this.DoublesSize > 0)
            {
                Console.WriteLine("Processing group analysis, please wait....");
                ProcessTemplates(this.DoublesSize, ref doublesTemplateSizeGroupExpansion, ref this.finalDoubles, true);
                this.finalDoubles.Clear();
                if (this.NumberOfFinalGroups > 1)
                {
                    Console.Write($"There are too many executions, please choose execution group number (1 - {this.NumberOfFinalGroups}):");
                    var in1 = Console.ReadLine();
                    this.ChosenExecutionGroup = Convert.ToInt32(in1);
                    if (this.ChosenExecutionGroup < 1 || this.ChosenExecutionGroup > this.NumberOfFinalGroups)
                        throw new Exception($"Error: invalid group to execute was chosen {in1}");
                    Console.WriteLine("Generating execution group, please wait....");
                    ProcessTemplates(this.DoublesSize, ref doublesTemplateSizeGroupExpansion, ref this.finalDoubles, false, this.ChosenExecutionGroup);
                }
                else
                    ProcessTemplates(this.DoublesSize, ref doublesTemplateSizeGroupExpansion, ref this.finalDoubles, false, 1);

            }
        }

        private void ProcessTemplates(int groupSize, ref Dictionary<int, List<List<string>>> templateSizeGroupExpansion,
            ref Dictionary<string, List<List<string>>> finalPermutations, bool checkSize = false, int groupChosen = 1)
        {
            var counter = 0;
            try
            {
                var groupTemplate = this.Templates.Collection[groupSize];
                //foreach (var template in groupTemplate)
                var stop = false;
                for (var t = 0; t < groupTemplate.Count && !stop; t++)
                {
                    var template = groupTemplate[t];
                    var key = string.Join("-", template); //e.g 1-1-1
                    foreach (var grp in templateSizeGroupExpansion[template.Count])
                    {
                        var tGroup = new List<string>();
                        for (var i = 0; i < grp.Count; i++)
                            tGroup.Add(string.Format("{0}{1}", grp[i], template[i]));

                        if (!finalPermutations.ContainsKey(key))
                        {
                            if (checkSize)
                            {
                                finalPermutations.Add(key, null);
                                this.finalPermutationsIndexKey.Add(finalPermutations.Count, key);
                            }
                            else
                            {
                                if (this.finalGroupsKeys.Count > 0)
                                {
                                    if (this.finalGroupsKeys[groupChosen].Contains(key))
                                        finalPermutations.Add(key, new List<List<string>>());
                                }
                                else
                                    finalPermutations.Add(key, new List<List<string>>());
                            }
                            counter++;
                        }
                        if (!checkSize && finalPermutations.ContainsKey(key))
                            finalPermutations[key].Add(tGroup);
                        if (this.finalGroupsKeys.Count > 0 &&
                            finalPermutations.Keys.Count == this.finalGroupsKeys[groupChosen].Count)
                        {
                            stop = true;
                        }
                        counter += tGroup.Count;
                    }
                }
                if (checkSize && counter > numberOfItemsPerGroup)
                {
                    CreateExecutionGroups(counter, finalPermutations.Keys.Count);
                    this.NumberOfFinalGroups = this.finalGroupsKeys.Keys.Count;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception raised on {counter} memory items", ex);
            }
        }
        private void CreateExecutionGroups(int counter, int numberOfKeys)
        {
            this.NumberOfFinalGroups = (counter - (counter % numberOfItemsPerGroup)) / numberOfItemsPerGroup + 1;
            var limit = numberOfKeys / this.NumberOfFinalGroups;
            var runningIndex = 0;
            for (var i = 1; i <= this.NumberOfFinalGroups; i++)
            {
                finalGroupsKeys.Add(i, new List<string>());
                for (runningIndex = (i - 1) * limit + 1; runningIndex <= i * limit; runningIndex++)
                {
                    finalGroupsKeys[i].Add(this.finalPermutationsIndexKey[runningIndex]);
                }
            }
            if (runningIndex < numberOfKeys)
            {
                finalGroupsKeys.Add(NumberOfFinalGroups + 1, new List<string>());
                for (; runningIndex <= numberOfKeys; runningIndex++)
                {
                    finalGroupsKeys[NumberOfFinalGroups + 1].Add(this.finalPermutationsIndexKey[runningIndex]);
                }
            }
        }
        //private void ReadGroupData() //OBA: use algoservice PrepareGroupsData
        //{
        //    string line;
        //    System.IO.StreamReader file = new System.IO.StreamReader(groupsDataFilename);
        //    while ((line = file.ReadLine()) != null)
        //    {
        //        if (line.StartsWith("ISIN"))
        //            continue;
        //        string[] items = line.Split("\t".ToCharArray());
        //        switch (items[0].Length)
        //        {
        //            case 1:
        //                this.bankersGroupKeys.Add(items[0]);
        //                this.groups.Add(items[0], new GroupData(items[1], items[2], items[3], items[4]));
        //                break;
        //            case 2:
        //                this.doublesGroupKeys.Add(items[0]);
        //                this.groups.Add(items[0], new GroupData(items[1], items[2], items[3], items[4], items[5]));
        //                break;
        //        }
        //    }

        //    file.Close();
        //}

        private void ExpandGroup(int groupSize, List<string> groupKeys, ref List<List<string>> groupExpansionColl, int startFrom = 0)
        {
            var highLimit = startFrom + groupSize - 1;
            if ((highLimit == 1 && groupSize == 1) || highLimit >= groupKeys.Count)
                return;
            groupExpansionColl.Add(new List<string>());
            for (var i = startFrom; i < highLimit; i++)
            {
                groupExpansionColl.Last().Add(groupKeys[i]);
            }
            groupExpansionColl.Last().Add(groupKeys[startFrom + groupSize - 1]);

            for (var i = highLimit + 1; i < groupKeys.Count; i++)
            {
                groupExpansionColl.Add(new List<string>(groupExpansionColl.Last()));
                groupExpansionColl.Last()[groupSize - 1] = groupKeys[i];
            }
            this.ExpandGroup(groupSize, groupKeys, ref groupExpansionColl, startFrom + 1);
        }
        public void RunMainLoop(int fromChunkNumber, int toChunkNumber, int programTypeInt, DateTime? fromDate, DateTime? toDate, bool matchBaseToFinalResultOnly)
        {
            var runner = new Runner(this, programTypeInt, fromDate, toDate, toChunkNumber > fromChunkNumber, matchBaseToFinalResultOnly);
            for (var chunkNumber = fromChunkNumber; chunkNumber <= toChunkNumber; chunkNumber++)
                this.ChunkRunner(chunkNumber, runner);
            if (runner.IsRangeChosen)
            {
                var drillerStr = matchBaseToFinalResultOnly ? "D_" : "";
                runner.finalResults.DumpToFile($"{drillerStr}{fromChunkNumber}__{toChunkNumber}");
            }
            //Console.WriteLine(chunkNumber);
        }
        private void ChunkRunner(int chunkNumber, Runner runner)
        {
            var fileNamePrefix = String.Empty;
            List<string> all = new List<string>();
            var drillerStr = runner.MatchBaseToFinalResultOnly? "D_" : "";
            if (this.finalBankers.Count > 0 && this.finalDoubles.Keys.Count > 0)
            {
                var bKey = this.chunks[chunkNumber][0];
                var dKey = this.chunks[chunkNumber][1];
                var total = this.finalBankers[bKey].Count * this.finalDoubles[dKey].Count;
                fileNamePrefix = $"{drillerStr}{chunkNumber}__[{bKey}]x[{dKey}]";

                var index = 1;
                foreach (var bPattern in this.finalBankers[bKey])
                    foreach (var dPattern in this.finalDoubles[dKey])
                    {
                        var template = $"{string.Join("-", bPattern)}  {string.Join("-", dPattern)}";
                        all.Add(template);
                        Utilities.DrawTextProgressBarInConsoleWindow(index, total);
                        runner.runBacktesting(bPattern, dPattern, template, fileNamePrefix);
                        index++;
                    }
            }
            else if (this.finalBankers.Count > 0)
            {
                var bKey = this.chunks[chunkNumber][0];
                var total = this.finalBankers[bKey].Count;
                fileNamePrefix = $"{drillerStr}{chunkNumber}__[{bKey}]";

                var index = 1;
                foreach (var bPattern in this.finalBankers[bKey])
                {
                    var template = $"{string.Join("-", bPattern)}";
                    all.Add(template);
                    Utilities.DrawTextProgressBarInConsoleWindow(index, total);
                    runner.runBacktesting(bPattern, new List<string>(), template, fileNamePrefix);
                    index++;
                }
            }
            else if (this.finalDoubles.Count > 0)
            {
                var dKey = this.chunks[chunkNumber][0];
                var total = this.finalDoubles[dKey].Count;
                fileNamePrefix = $"{drillerStr}{chunkNumber}__[{dKey}]";
                var index = 1;
                foreach (var dPattern in this.finalDoubles[dKey])
                {
                    var template = $"{string.Join("-", dPattern)}";
                    all.Add(template);
                    Utilities.DrawTextProgressBarInConsoleWindow(index, total);
                    runner.runBacktesting(new List<string>(), dPattern, template, fileNamePrefix);
                    index++;
                }
            }
            System.IO.File.WriteAllLines($"{GroupsTxtFilesFolder}//{fileNamePrefix}.txt", all.ToArray());
            if (!runner.IsRangeChosen)
                Console.WriteLine($"{Environment.NewLine}File created: {fileNamePrefix}.txt");
            else
                Console.WriteLine($":{chunkNumber}");
        }
        public void ShowChunksMenu()
        {
            var optionId = 1;
            if (this.NumberOfFinalGroups > 1)
                optionId = this.finalGroupsKeys.Values.First().Count * (this.ChosenExecutionGroup - 1) + 1;
            this.FromOptionId = optionId;
            if (this.finalBankers.Count > 0 && this.finalDoubles.Keys.Count > 0)
            {
                foreach (var b in this.finalBankers.Keys)
                {
                    foreach (var d in this.finalDoubles.Keys)
                    {
                        //var runningId = (fIndex - 1) * this.finalDoubles.Keys.Count + dIndex;
                        var oid = $"{optionId}.".PadRight(4);
                        var pattern = $"[{b}] x [{d}]".PadRight(20);
                        Console.WriteLine($"{oid} {pattern} {this.finalBankers[b].Count} x {this.finalDoubles[d].Count} = {this.finalBankers[b].Count * this.finalDoubles[d].Count}");
                        this.chunks.Add(optionId, new string[] { b, d });
                        optionId++;
                    }
                }
            }
            else if (this.finalBankers.Count > 0)
            {
                foreach (var b in this.finalBankers.Keys)
                {
                    var oid = $"{optionId}.".PadRight(4);
                    var pattern = $"[{b}]".PadRight(20);
                    Console.WriteLine($"{oid} {pattern} {this.finalBankers[b].Count}");
                    this.chunks.Add(optionId, new string[] { b });
                    optionId++;
                }
            }
            else if (this.finalDoubles.Count > 0)
            {
                foreach (var d in this.finalDoubles.Keys)
                {
                    var oid = $"{optionId}.".PadRight(4);
                    var pattern = $"[{d}]".PadRight(20);
                    Console.WriteLine($"{oid} {pattern} {this.finalDoubles[d].Count}");
                    this.chunks.Add(optionId, new string[] { d });
                    optionId++;
                }
            }
            this.ToOptionId = optionId - 1;
        }
    }
}
