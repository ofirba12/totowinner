﻿using System;
using System.Collections.Generic;
using TotoWinner.Data;

namespace TotoWinner.Backtesting
{
    public class Finals
    {
        private SortedDictionary<BacktestingFinalResultKey, BacktestingFinalResult> finalResult;
        private int programIntType;
        private int bankers;
        private int doubles;

        public Finals(int programTypeInt, int templateBankers, int templateDoubles)
        {
            this.finalResult = new SortedDictionary<BacktestingFinalResultKey, BacktestingFinalResult>(new DuplicateDescendingKeyComparer<BacktestingFinalResultKey>());
            //switch (programType)
            //{
            //    case ProgramType.Winner16:
            //        this.programIntType = 16;
            //        break;
            //    case ProgramType.WinnerWorld:
            //        this.programIntType = 15;
            //        break;
            //    case ProgramType.WinnerHalf:
            //        this.programIntType = 14;
            //        break;
            //}
            this.programIntType = programTypeInt;
            this.bankers = templateBankers;
            this.doubles = templateDoubles;

        }
        public void AddResult(int executionId, string template, Dictionary<int, List<string>> winningPrograms, BacktestingTotalResult totals, BacktestingSummaryResult summary)
        {
            try
            {                
                var firstWinIndex = totals.Results.Count - 1;
                var key = new BacktestingFinalResultKey(totals.Results[firstWinIndex],
                    totals.Results[firstWinIndex - 1],
                    totals.Results[firstWinIndex - 2],
                    executionId);
                var value = new BacktestingFinalResult(template, winningPrograms, summary, totals.Bets, totals.TotalProgramsWithBaseBet);
                this.finalResult.Add(key, value);
            }
            catch(Exception ex)
            {
                throw new Exception($"and error occurred trying to add final result for executionId={executionId}, template={template}", ex);
            }
        }
        
        public void DumpToFile(string fileNamePrefix)
        {
            var filename = $"{fileNamePrefix}_FINALS_RESULTS.CSV";
            List<string> all = new List<string>();
            all.Add("ProgramType,Bankers,Doubles,Template,TotalBets,TotalProgramsCounter,TotalProgramsWithBaseBets,FirstWin,SecondWin,ThirdWin,WinningProgramsFirstWin,WinningProgramsSecondWin,WinningProgramsThirdWin");
            foreach (var kpv in this.finalResult)
            {
                var winningPrograms1Str = kpv.Value.WinningPrograms != null && kpv.Value.WinningPrograms.ContainsKey(1) ? string.Join("+", kpv.Value.WinningPrograms[1]) : "N/A";
                var winningPrograms2Str = kpv.Value.WinningPrograms != null && kpv.Value.WinningPrograms.ContainsKey(2) ? string.Join("+", kpv.Value.WinningPrograms[2]) : "N/A";
                var winningPrograms3Str = kpv.Value.WinningPrograms != null && kpv.Value.WinningPrograms.ContainsKey(3) ? string.Join("+", kpv.Value.WinningPrograms[3]) : "N/A";
                var line = $"{this.programIntType},{this.bankers},{this.doubles},{kpv.Value.Template},{kpv.Value.NumOfAllBets},{kpv.Value.Summary.ProgramsCounter},{kpv.Value.TotalProgramsWithBaseBet},{kpv.Key.FirstWinCounter},{kpv.Key.SecondWinCounter},{kpv.Key.ThirdWinCounter},{winningPrograms1Str},{winningPrograms2Str},{winningPrograms3Str}";
                    all.Add(line);
            }
            System.IO.File.WriteAllLines(filename, all.ToArray());
            Console.WriteLine($"{Environment.NewLine}File created: {filename}");
        }
    }
    public class DuplicateDescendingKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
    {
        #region IComparer<TKey> Members

        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);
            if (result == 0)
                return 1;   // Handle equality as beeing greater

            return result;
        }

        #endregion
    }
}
