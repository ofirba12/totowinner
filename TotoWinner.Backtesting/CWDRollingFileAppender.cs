﻿using log4net.Appender;
using System.IO;

namespace TotoWinner.Backtesting
{
    class CWDRollingFileAppender : RollingFileAppender
    {
        public override string File
        {
            set
            {
                base.File = Path.Combine(Directory.GetCurrentDirectory(), value);
            }
        }
    }
}
