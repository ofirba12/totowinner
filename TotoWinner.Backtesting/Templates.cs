﻿using System.Collections.Generic;

namespace TotoWinner.Backtesting
{
    public class Templates
    {
        public Dictionary<int, List<List<int>>> Collection = new Dictionary<int, List<List<int>>>();
        public void Generate(int size)
        {
            for (var i = 1; i <= size; i++)
            {
                if (i == 1)
                {
                    var members = new List<int>() { 1 };
                    Collection.Add(1, new List<List<int>> { members });
                }
                else
                {
                    for (var j = 0; j < Collection[i - 1].Count; j++)
                    {
                        var members = new List<int>() { 1 };
                        members.AddRange(Collection[i - 1][j]);
                        if (!Collection.ContainsKey(i))
                            Collection.Add(i, new List<List<int>>());
                        Collection[i].Add(members);
                        members = new List<int>();
                        members.AddRange(Collection[i - 1][j]);
                        members[0]++;
                        Collection[i].Add(members);
                    }
                }
            }
        }
    }
}
