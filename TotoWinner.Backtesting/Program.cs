﻿using log4net;
using System;
using System.Globalization;
using System.IO;

namespace TotoWinner.Backtesting
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            try
            {
                Console.Write("Program Type (16 default) [16,15,14]:");
                var inprt = Console.ReadLine();
                var programTypeInt = string.IsNullOrEmpty(inprt) ? 16 : Convert.ToInt32(inprt);
                if (programTypeInt < 14 || programTypeInt > 16)
                    throw new Exception("Error: Invalid program type was chosen.");

                Console.Write("Number of permutations (8 default):");
                var in1 = Console.ReadLine();
                var numberOfPermutations = string.IsNullOrEmpty(in1) ? 8 : Convert.ToInt32(in1);

                Console.Write("Please specify number of bankers:");
                var in2 = Console.ReadLine();
                while (string.IsNullOrEmpty(in2))
                {
                    Console.Write("Please specify number of bankers:");
                    in2 = Console.ReadLine();
                }
                var bankers = Convert.ToInt32(in2);
                if (bankers > numberOfPermutations)
                {
                    throw new Exception("Error: Number of permutation must be >= bankers");
                }

                Console.Write("Please specify number of doubles:");
                var in3 = Console.ReadLine();
                while (string.IsNullOrEmpty(in3))
                {
                    Console.Write("Please specify number of doubles:");
                    in3 = Console.ReadLine();
                }
                var doubles = Convert.ToInt32(in3);
                if (doubles > numberOfPermutations)
                {
                    throw new Exception("Error: Number of permutation must be >= doubles");
                }

                if (doubles + bankers > programTypeInt)
                {
                    throw new Exception($"Error: bankers + doubles > {programTypeInt}");
                }
                var gr = new GroupsRepository(numberOfPermutations, bankers, doubles);
                gr.ShowChunksMenu();
                //var numberOfChunks = (bankers > 0
                //    ? gr.Templates.Collection[bankers].Count
                //    : 1) *
                //    (doubles > 0
                //    ? gr.Templates.Collection[doubles].Count
                //    : 1);

                //Console.WriteLine($"There are {numberOfChunks} chunks to execute, please choose range of chunks to execute (1-{numberOfChunks})");
                Console.WriteLine($"There are {gr.ToOptionId - gr.FromOptionId + 1} chunks to execute, please choose range of chunks to execute ({gr.FromOptionId}-{gr.ToOptionId})");
                Console.Write("From:");
                var in4 = Console.ReadLine();
                var from = Convert.ToInt32(in4);
                //                if (from < 1 || from > numberOfChunks)
                if (from < gr.FromOptionId || from > gr.ToOptionId)
                    throw new Exception($"Error: invalid from was chosen {in4}");
                Console.Write($"To (default {from}):");
                var in5 = Console.ReadLine();
                var to = string.IsNullOrEmpty(in5) ? from : Convert.ToInt32(in5);
                if (from > to)
                    throw new Exception($"Error: invalid range chosen from > to");
                GetDates(out var fromDate, out var toDate);
                Console.Write("Would you like to drill/match base to final result (default N)? (Y/N):");
                var in6 = Console.ReadLine();
                var matchBaseToFinalResultOnly = false;
                if (!string.IsNullOrEmpty(in6))
                {
                    if (in6.ToLower() == "y")
                        matchBaseToFinalResultOnly = true;
                    else if (in6.ToLower() == "n")
                        matchBaseToFinalResultOnly = false;
                    else
                        throw new Exception($"Error: invalid input for match base to final results was found {in6}");
                }
                if (Directory.Exists(gr.GroupsTxtFilesFolder))
                {
                    Console.Write("Would you like to delete previous group TXT folder (default Y)? (Y/N):");
                    var in7 = Console.ReadLine();
                    if ((!string.IsNullOrEmpty(in7) && in6.ToLower() == "y") || string.IsNullOrEmpty(in7))
                    {
                        Directory.Delete(gr.GroupsTxtFilesFolder, true);
                    }
                }
                if (!Directory.Exists(gr.GroupsTxtFilesFolder))
                    Directory.CreateDirectory(gr.GroupsTxtFilesFolder);

                gr.RunMainLoop(from, to, programTypeInt, fromDate, toDate, matchBaseToFinalResultOnly);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _log.Fatal("An unexpected error occurred.", ex);
            }
            finally
            {
                Console.WriteLine("Press any key to exist:");
                Console.ReadLine();
            }
        }

        private static void GetDates(out DateTime? fromDate, out DateTime? toDate)
        {
            fromDate = null;
            toDate = null;
            Console.Write("Please choose date from (All default) [dd/mm/yyyy]: ");
            var fromDateIn = Console.ReadLine();
            if (string.IsNullOrEmpty(fromDateIn))
                fromDateIn = "all";
            if (!fromDateIn.Equals("all", StringComparison.OrdinalIgnoreCase))
            {
                if (!DateTime.TryParseExact(fromDateIn, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                        DateTimeStyles.None, out DateTime fromDateInput))
                    throw new Exception("Invalid from date input");
                fromDate = fromDateInput;
                Console.Write("Please choose date to [dd/mm/yyyy]: ");
                var toDateIn = Console.ReadLine();
                if (!DateTime.TryParseExact(toDateIn, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                        DateTimeStyles.None, out DateTime toDateInput))
                    throw new Exception("Invalid to date input");
                toDate = toDateInput;

                if (fromDate > toDate)
                    throw new Exception("Invalid date input: from > to");
            }
        }
    }
}
