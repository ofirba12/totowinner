﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using TotoWinner.Algo;
using TotoWinner.Data;
using TotoWinner.Services;
using TotoWinner.Services.Algo;

namespace TotoWinner.Backtesting
{
    public class Runner
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private InMemoryBacktestingPersistanceServices _inMemoryBtPersistanceSrv = InMemoryBacktestingPersistanceServices.Instance;
        private ExecutionServices _executionSrv = ExecutionServices.Instance;
        private ProgramServices _programSrv = ProgramServices.Instance;
        private TemplatesServices _templatesSrv = TemplatesServices.Instance;
        private GroupsRepository groupsRepository;

        public Finals finalResults { get; private set; }
        public ProgramType programType { get; }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public bool IsRangeChosen { get; }
        public bool MatchBaseToFinalResultOnly { get; }

        public Runner(GroupsRepository groupsRepository, int programTypeInt, DateTime? fromDate, DateTime? toDate, 
        bool isRangeChosen, bool matchBaseToFinalResultOnly)
        {
            this.groupsRepository = groupsRepository;
            this.IsRangeChosen = isRangeChosen;
            this.MatchBaseToFinalResultOnly = matchBaseToFinalResultOnly;
            this.finalResults = new Finals(programTypeInt, groupsRepository.BankersSize, groupsRepository.DoublesSize);
            switch (programTypeInt)
            {
                case 16:
                    this.programType = ProgramType.Winner16;
                    break;
                case 15:
                    this.programType = ProgramType.WinnerWorld;
                    break;
                case 14:
                    this.programType = ProgramType.WinnerHalf;
                    break;
            }

            if (fromDate.HasValue && toDate.HasValue)
            {
                this.FromDate = fromDate.Value;
                this.ToDate = toDate.Value;
                var programs = _programSrv.GetPersistanceProgramsWithGamesData(this.programType,
                    this.FromDate, this.ToDate, true);
            }
            else //ALL
            {
                var programs = _programSrv.GetPersistanceProgramsWithGamesData(this.programType,
                    DateTime.Parse("1/1/2016"), DateTime.Today, true);
                this.FromDate = _inMemoryBtPersistanceSrv.ProgramWithGamesRepository.Min(elm => elm.EndDate);
                this.ToDate = DateTime.Today;
            }
        }
        public void runBacktesting(List<string> bPattern, List<string> dPattern, string template, string fileNamePrefix)
        {
            _log.Info($"Running template: {template}");
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var executionId = _inMemoryBtPersistanceSrv.StartExecution(template);
            var work = createExecutionWork(executionId, bPattern, dPattern);
            using (var cts = new CancellationTokenSource())
            {
                var backtesting = new BacktestingAlgo();
                backtesting.RunAlgo(cts.Token, work, true, this.MatchBaseToFinalResultOnly);
            }
            analyzeExecutionResult(executionId, fileNamePrefix);
            clearCash(executionId);
            stopwatch.Stop();
            _log.Info($"runBacktesting time in seconds={TimeSpan.FromMilliseconds(stopwatch.ElapsedMilliseconds).Seconds} template: {template}");
        }

        private void clearCash(int executionId)
        {
            _inMemoryBtPersistanceSrv.BaseBets.Clear();
            _inMemoryBtPersistanceSrv.BacktestingResults.Clear();
        }

        private void analyzeExecutionResult(int executionId, string fileNamePrefix)
        {
            if (_inMemoryBtPersistanceSrv.ExecutionStatus[executionId] != ExecutionStatus.Finished)
                return;
            var results = _executionSrv.GetBacktestingResults(executionId, 0, true);
            var firstWinIndex = results.Totals.Results.Count - 1;
            var secondWinIndex = results.Totals.Results.Count - 2;
            var thirdWinIndex = results.Totals.Results.Count - 3;
            if (results.Totals.Results[firstWinIndex] > 0 ||
                results.Totals.Results[secondWinIndex] > 0 ||
                results.Totals.Results[thirdWinIndex] > 0)
            {
                var template = _inMemoryBtPersistanceSrv.ExecutionTemplatesRepo[executionId];
                var winningPrograms = new Dictionary<int, List<string>>();
                winningPrograms.Add(1, results.Programs
                    .Where(p => p.Results.Count > 0 &&
                        (p.Results[p.Results.Count - 1] > 0))
                    .ToList()
                    .ConvertAll<string>(s => s.ProgramNumber.ToString()));
                winningPrograms.Add(2, results.Programs
                    .Where(p => p.Results.Count > 0 &&
                        (p.Results[p.Results.Count - 2] > 0))
                    .ToList()
                    .ConvertAll<string>(s => s.ProgramNumber.ToString()));
                winningPrograms.Add(3, results.Programs
                    .Where(p => p.Results.Count > 0 &&
                        (p.Results[p.Results.Count - 3] > 0))
                    .ToList()
                    .ConvertAll<string>(s => s.ProgramNumber.ToString()));
                this.finalResults.AddResult(executionId, template, winningPrograms, results.Totals, results.Summary);

                if (IsRangeChosen)
                    Console.WriteLine($"{Environment.NewLine}Result founds for template: {fileNamePrefix}");
                else
                    this.finalResults.DumpToFile(fileNamePrefix);
            }
        }

        public BacktestingExecutionData createExecutionWork(int executionId, List<string> bPattern, List<string> dPattern)
        {
            var work = new BacktestingExecutionData();
            work.Definitions = _templatesSrv.CreateBacktestingDefinitions(bPattern, dPattern);
            work.ExecutionId = executionId;
            work.Status = ExecutionStatus.Running;
//            work.Definitions = new BacktestingDefinitions();
            work.Definitions.ProgramType = this.programType;
            work.Definitions.FromDate = this.FromDate;
            work.Definitions.ToDate = this.ToDate;
            work.Definitions.BankersLimits = new MinMaxLimits() { Min = this.groupsRepository.BankersSize, Max = this.groupsRepository.BankersSize };
            work.Definitions.DoublesLimits = new MinMaxLimits() { Min = this.groupsRepository.DoublesSize, Max = this.groupsRepository.DoublesSize };
            return work;
        }


    }
}
