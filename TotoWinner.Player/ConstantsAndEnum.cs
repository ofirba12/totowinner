﻿namespace TotoWinner.Player
{
    public enum ButtonState
    {
        NeverPlayed,
        PlayedNotVerified,
        PlayedWithError,
        PlayedSuccess,
        LogoutSuccess
    }
}
