﻿using System;

namespace TotoWinner.Player
{
    public class ChunkKey
    {
        public string Key { get; }
        public int From { get; }
        public int To { get; }

        public ChunkKey(int fromIndex, int toIndex)
        {
            try
            {
                if (fromIndex > toIndex)
                    throw new Exception($"The specified fromIndex {fromIndex} > toIndex {toIndex}");
                this.Key = $"{fromIndex}-{toIndex}";
                this.From = fromIndex;
                this.To = toIndex;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ChunkKey)}; {fromIndex}-{toIndex}", ex);
            }
        }
        public override bool Equals(object obj)
        {
            var candidate = obj as ChunkKey;
            return candidate != null
                ? this.Key == candidate.Key
                : false;
        }
        public override int GetHashCode()
        {
            return this.From + this.To;
        }
    }
}
