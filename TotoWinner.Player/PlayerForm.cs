﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TotoWinner.Player
{
    public partial class PlayerForm : Form
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Player player;

        public PlayerForm(string[] args)
        {
            InitializeComponent();
            //When Open File type .twn, the full file path will be in args[0]
            //Console.WriteLine(string.Join(",", args));
            if (args.Count() == 1)
            {
                try
                {
                    player = new Player(args[0]);
                    player.OnUILogging += Player_UILogging;
                    player.OnRefreshButton += Player_OnRefreshButton;
                    player.InitAndVerifyCredetials();
                    this.Player_UILogging(null, new UILoggingEventArg($"נמצא קובץ ניגון לטעינה: {player.InputFile}"));
                    this.Player_UILogging(null, new UILoggingEventArg($"נתוני אימות: {player.Username}/{player.Password}"));
                    //This will trigger chunkSize_SelectedIndexChanged
                    this.chunkSize.SelectedIndex = this.chunkSize.FindStringExact(player.ChunkSize.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("אירעה תקלה לא צפויה בטעינה, בדוק בלוג", "תקלה לא צפויה");
                    _log.Fatal($"An error occurred trying to run player form with file input {args[0]}", ex);
                }
            }
        }

        private void Player_OnRefreshButton(object sender, EventArgs e)
        {
            var btn = sender as Button;
            btn.Refresh();
        }

        private void Player_UILogging(object sender, UILoggingEventArg e)
        {
            this.richTextBox1.AppendText($"{e.Message}{Environment.NewLine}");
        }

        private void InitForm()
        {
            this.SetBetTypeLabel();
            var btnYLocation = 0;
            var btnTabIndex = 1;
            foreach (var chunkItem in this.player.ChunksReposiroty)
            {
                var playButton = new System.Windows.Forms.Button();

                playButton.Enabled = true;
                playButton.Location = new System.Drawing.Point(177, btnYLocation);
                playButton.Name = $"playButton{chunkItem.Key.Key}";
                playButton.Size = new System.Drawing.Size(115, 23);
                playButton.Text = $"נגן טורים {chunkItem.Key.Key} ";
                playButton.BackColor = Color.PowderBlue;
                playButton.UseVisualStyleBackColor = false;
                playButton.Tag = chunkItem;
                playButton.Click += new EventHandler(this.playChunk);
                this.panel1.Controls.Add(playButton);
                chunkItem.Value.PlayButton = playButton;

                var logoutButton = new System.Windows.Forms.Button();
                btnTabIndex++;

                logoutButton.Enabled = false;
                logoutButton.Location = new System.Drawing.Point(27, btnYLocation);
                logoutButton.Name = $"logoutButton{chunkItem.Key.Key}";
                logoutButton.Size = new System.Drawing.Size(115, 23);
                logoutButton.Text = "אשר וסגור";
                logoutButton.UseVisualStyleBackColor = false;
                logoutButton.Tag = chunkItem;
                logoutButton.Click += new EventHandler(this.logoutChunk);
                this.panel1.Controls.Add(logoutButton);
                chunkItem.Value.LogoutButton = logoutButton;

                btnYLocation += 30;
                btnTabIndex++;
            }
        }
        private void SetBetTypeLabel()
        {
            try
            {
                var numOfBets = this.player.ChunksReposiroty.First().Value.BetsData.First().Value.Count();
                var title = string.Empty;
                switch (numOfBets)
                {
                    case 16:
                        title = "ווינר 16";
                        break;
                    case 15:
                        title = "ווינר עולמי";
                        break;
                    case 14:
                        title = "ווינר מחצית";
                        break;
                }
                this.betTypeLabel.Text = title;
            }
            catch (Exception ex)
            {
                var err = "An error occurred trying to extract bet type from chunk repository";
                _log.Fatal(err, ex);
                this.Player_UILogging(null, new UILoggingEventArg($"תקלה {DateTime.Now.ToString("T")}: לא ניתן לקבוע סוג טור"));
                throw new Exception(err, ex);
            }
        }
        private void playChunk(object sender, EventArgs e)
        {
            var button = (System.Windows.Forms.Button)sender;
            var chunkData = (KeyValuePair<ChunkKey, ChunkData>)button.Tag;
            this.player.PlayChunk(chunkData.Key);
        }
        private void logoutChunk(object sender, EventArgs e)
        {
            var button = (System.Windows.Forms.Button)sender;
            var chunkData = (KeyValuePair<ChunkKey, ChunkData>)button.Tag;
            this.player.Logout(chunkData.Key);
            this.player.DisposeDriver(chunkData.Key);
        }

        private void chunkSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            var newChunkSize = Convert.ToInt32(this.chunkSize.SelectedItem.ToString());
            var buttons = new List<System.Windows.Forms.Button>();
            foreach (var control in this.panel1.Controls)
            {
                var button = control as System.Windows.Forms.Button;
                if (button != null)
                {
                    if (button.Name.StartsWith("playButton") ||
                        button.Name.StartsWith("logoutButton"))
                        buttons.Add(button);
                }
            }
            foreach (var btn in buttons)
                this.panel1.Controls.Remove(btn);

            if (player.SetNewChunkSize(newChunkSize))
            {
                InitForm();
            }
        }
    }
}
