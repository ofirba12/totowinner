﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;

namespace TotoWinner.Player
{
    public class ChunkData
    {
        public IWebDriver Driver { get; private set; }
        public Dictionary<int, List<char>> BetsData { get; }
        public System.Windows.Forms.Button PlayButton { get; set; }
        public System.Windows.Forms.Button LogoutButton { get; set; }

        public ChunkData(Dictionary<int, List<char>> betsData)
        {
            try
            {
                this.InitDriver();
                this.BetsData = betsData;
            }
            catch (Exception ex)
            {
                throw new System.Exception($"An error occured trying to construct type {typeof(ChunkData)};", ex);
            }
        }
        public void SetToLaunch()
        {
            try
            {
                //not neccessary to use options
                var chromeOptions = new ChromeOptions();
                //chromeOptions.AddArguments("--verbose");
                //chromeOptions.AddArguments("--whitelisted-ips *");
                this.Driver = new ChromeDriver(chromeOptions);
                //this.Driver = new ChromeDriver();

            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to set to launch", ex);
            }
        }
        public void InitDriver()
        {
            try
            {
                this.Driver = null;
                if (this.LogoutButton != null)
                    this.LogoutButton.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred trying to init driver", ex);
            }
        }
    }
}

