﻿using System;

namespace TotoWinner.Player
{
    public delegate void UILoggingHandler(object sender, UILoggingEventArg e);

    public class UILoggingEventArg : EventArgs
    {
        public string Message { get; private set; }

        public UILoggingEventArg(string message)
        {
            this.Message = message;
        }
    }
}
