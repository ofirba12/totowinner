﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TotoWinner.Player
{
    public delegate void UIRefresh(object sender, EventArgs e);

    public class Player
    {
        public event UILoggingHandler OnUILogging;
        public event UIRefresh OnRefreshButton;
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string InputFile { get; }
        public string Username { get; }
        public string Password { get; }
        public string WinnerUrl { get; }
        public int ChunkSize { get; private set; }

        public Dictionary<ChunkKey, ChunkData> ChunksReposiroty { get; private set; }

        public Player(string filePath)
        {
            try
            {
                this.InputFile = filePath;
                this.ChunkSize = Convert.ToInt32(ConfigurationManager.AppSettings["BetsTableChunkSize"]);
                this.Username = ConfigurationManager.AppSettings["Username"];
                this.Password = ConfigurationManager.AppSettings["Password"];
            }
            catch (Exception ex)
            {
                _log.Fatal(ex);
                throw new System.Exception($"An error occured trying to construct type {typeof(Player)} with input file={filePath}", ex);
            }
        }
        public void InitAndVerifyCredetials()
        {
            try
            {
                if (!File.Exists(this.InputFile))
                {
                    this.FireUILogging(new UILoggingEventArg($"שגיאה: קובץ ניגון לא קיים. {this.InputFile}"));
                    throw new Exception($"Input file {this.InputFile} was not found");
                }
                if (string.IsNullOrEmpty(this.Username))
                {
                    this.FireUILogging(new UILoggingEventArg($"שגיאה: שם משתמש ריק Username"));
                    throw new Exception($"Username can not be empty");
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("An error occurred trying to init and verify credetials", ex);
            }
        }
        public void PlayChunk(ChunkKey chunkKey)
        {
            try
            {
                if (this.ChunksReposiroty == null || this.ChunksReposiroty.Count == 0)
                {
                    this.FireUILogging(new UILoggingEventArg($"שגיאה: קובץ ניגון ריק, לא נמצאו טורים לניגון."));
                    var errMsg = $"No bets was found to play, please check input file {this.InputFile}";
                    _log.Error(errMsg);
                    throw new Exception(errMsg);
                }
                if (!this.ChunksReposiroty.ContainsKey(chunkKey))
                {
                    this.FireUILogging(new UILoggingEventArg($"שגיאה: לא נמצאו יחידות טורים לניגון. {chunkKey.Key}"));
                    var errMsg = $"No chunk bets #{chunkKey.Key} was found to play, please check input file {this.InputFile}";
                    _log.Error(errMsg);
                    throw new Exception(errMsg);
                }
                this.ChunksReposiroty[chunkKey].SetToLaunch();
                var driver = this.ChunksReposiroty[chunkKey].Driver;
                var url = LookForUrl(chunkKey);
                driver.Navigate().GoToUrl(url);
                var loginSucc = LoginToSite(chunkKey);
                if (!loginSucc)
                {
                    this.FireUILogging(new UILoggingEventArg($"שגיאה: לוגין לאתר נכשל. {chunkKey.Key}"));
                    var errMsg = $"The system failed to login to site";
                    _log.Error(errMsg);
                    throw new Exception(errMsg);
                }
                driver.Manage().Window.Maximize();
                this.Play(chunkKey);
                var verified = Verify(chunkKey);
                if (!verified)
                {
                    this.FireUILogging(new UILoggingEventArg($"שגיאה: לא נוגנו כל הטורים, בדוק ידנית. {chunkKey.Key}"));
                    ChangeButtonState(this.ChunksReposiroty[chunkKey].PlayButton, ButtonState.PlayedNotVerified);
                    _log.Error($"The system failed to verify chunk {chunkKey.Key} number of bets {this.ChunksReposiroty[chunkKey].BetsData.Count()}");
                }
                else
                {
                    _log.Info($"The system successfully played chunk {chunkKey.Key}");
                    ChangeButtonState(this.ChunksReposiroty[chunkKey].PlayButton, ButtonState.PlayedSuccess);
                }
                this.ChunksReposiroty[chunkKey].LogoutButton.Enabled = true;
            }
            catch (Exception ex)
            {
                this.FireUILogging(new UILoggingEventArg($"תקלה {DateTime.Now.ToString("T")}: אירעה לא צפויה בניגון. {chunkKey.Key}"));
                ChangeButtonState(this.ChunksReposiroty[chunkKey].PlayButton, ButtonState.PlayedWithError);
                if (this.ChunksReposiroty[chunkKey].Driver != null)
                    this.ChunksReposiroty[chunkKey].LogoutButton.Enabled = true;
                _log.Fatal($"An error occured trying to play chunk {chunkKey.Key}", ex);
            }
        }
        private void Play(ChunkKey chunkKey)
        {
            var driver = this.ChunksReposiroty[chunkKey].Driver;
            var uiBetsTables = driver.FindElements(By.CssSelector("[class*='games'] li")).ToList();
            var tableInChunk = chunkKey.From;
            for (var tableIndex = 1; tableIndex <= uiBetsTables.Count && chunkKey.From + tableIndex - 1 <= chunkKey.To; tableIndex++)
            {
                tableInChunk = chunkKey.From + tableIndex - 1;
                var uiRows = uiBetsTables[tableIndex - 1].FindElements(By.ClassName("row"));
                var rowIndex = 1;
                foreach (var inputBet in this.ChunksReposiroty[chunkKey].BetsData[tableInChunk])
                {
                    var uiBets = uiRows[rowIndex].FindElements(By.CssSelector($"[name*='{tableIndex}-{rowIndex}']")).ToList();
                    Actions action = new Actions(driver);
                    if (tableIndex > 5 && rowIndex == 1)
                    {
                        var leftScrollerElm = driver.FindElement(By.ClassName("left-scroller"));
                        var scrollButtonElm = leftScrollerElm.FindElement(By.ClassName("scroll_button"));
                        scrollButtonElm.Click();
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                    }
                    switch (inputBet)
                    {
                        case '1':
                            action.MoveToElement(uiBets[0]).Click().Perform();
                            break;
                        case 'X':
                            action.MoveToElement(uiBets[1]).Click().Perform();
                            break;
                        case '2':
                            action.MoveToElement(uiBets[2]).Click().Perform();
                            break;
                    }
                    rowIndex++;
                }
            }
        }
        private void ChangeButtonState(Button playButton, ButtonState state)
        {
            Color stateColor = Color.AliceBlue;
            switch (state)
            {
                case ButtonState.NeverPlayed:
                    stateColor = Color.PowderBlue;
                    break;
                case ButtonState.PlayedSuccess:
                    stateColor = Color.Lime;
                    break;
                case ButtonState.PlayedNotVerified:
                    stateColor = Color.Orange;
                    break;
                case ButtonState.PlayedWithError:
                    stateColor = Color.Red;
                    break;
                case ButtonState.LogoutSuccess:
                    stateColor = Color.LightGreen;
                    break;
            }
            playButton.BackColor = stateColor;
            if (this.OnRefreshButton != null)
                OnRefreshButton(playButton, new EventArgs());
        }

        public bool SetNewChunkSize(int newChunkSize)
        {
            this.ChunkSize = newChunkSize;
            this.FireUILogging(new UILoggingEventArg($"מספר טורים לניגון בטופס: {this.ChunkSize}"));
            var result = this.ParseFile();
            if (!result)
                this.FireUILogging(new UILoggingEventArg($"נמצא קובץ לא תקין לטעינה: {this.InputFile}"));
            else
            {
                this.FireUILogging(new UILoggingEventArg($"סה\"כ טורים לניגון: {this.ChunksReposiroty.Last().Key.To}"));
                this.FireUILogging(new UILoggingEventArg($"סה\"כ טפסים למשלוח: {this.ChunksReposiroty.Count}"));
            }
            return result;
        }

        public void Logout(ChunkKey chunkKey)
        {
            try
            {
                var driver = this.ChunksReposiroty[chunkKey].Driver;
                var logoutElm = driver.FindElement(By.ClassName("logout"));
                logoutElm.Click();
                var text = driver.SwitchTo().Alert().Text;
                driver.SwitchTo().Alert().Accept();
                //driver.SwitchTo().Alert().Dismiss();
            }
            catch (Exception ex)
            {
                this.FireUILogging(new UILoggingEventArg($"תקלה {DateTime.Now.ToString("T")}: אירעה שגיאת בניתוק מהחשבון באתר. {chunkKey.Key}"));
                _log.Fatal($"An error occurred trying to logout button {chunkKey.Key}", ex);
                ChangeButtonState(this.ChunksReposiroty[chunkKey].LogoutButton, ButtonState.PlayedWithError);
            }
        }
        public void DisposeDriver(ChunkKey chunkKey)
        {
            try
            {
                var driver = this.ChunksReposiroty[chunkKey].Driver;
                driver.Close();
                driver.Quit();
                this.ChunksReposiroty[chunkKey].InitDriver();
                ChangeButtonState(this.ChunksReposiroty[chunkKey].LogoutButton, ButtonState.LogoutSuccess);
            }
            catch (Exception ex)
            {
                this.FireUILogging(new UILoggingEventArg($"תקלה {DateTime.Now.ToString("T")}: אירעה שגיאת בסגירת הדפדפן וחלון הפקודות. {chunkKey.Key}"));
                _log.Fatal($"An error occurred trying to dispose driver {chunkKey.Key}", ex);
                ChangeButtonState(this.ChunksReposiroty[chunkKey].LogoutButton, ButtonState.PlayedWithError);
            }
        }
        private bool Verify(ChunkKey chunkKey)
        {
            var succ = false;
            var driver = this.ChunksReposiroty[chunkKey].Driver;
            var numOfBetsPlayed = this.ChunksReposiroty[chunkKey].BetsData.Count();
            var numOfTablesElm = driver.FindElement(By.ClassName("num-entries"));
            if (numOfTablesElm != null)
            {
                succ = numOfTablesElm.Text == numOfBetsPlayed.ToString();
            }
            return succ;
        }

        private bool LoginToSite(ChunkKey chunkKey)
        {
            try
            {
                var driver = this.ChunksReposiroty[chunkKey].Driver;
                var usernameElm = driver.FindElement(By.Id("username"));
                usernameElm.SendKeys(this.Username);
                driver.FindElement(By.Id("password")).Click();
                var passwordElm = driver.FindElement(By.Id("password"));
                passwordElm.SendKeys(this.Password);
                var loginElm = driver.FindElement(By.ClassName("showCaptcha"));
                loginElm.Click();
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until<bool>(drv =>
                {
                    try
                    {
                        var usernameSpanElm = drv.FindElement(By.ClassName("user_name"));
                        return true;
                    }
                    catch (NoSuchElementException)
                    {
                        return false;
                    }
                });
                var usernameContainerElm = driver.FindElement(By.ClassName("user_name"));
                var response = (usernameContainerElm.Text.StartsWith("שלום בן"));
                return response;
            }
            catch (Exception ex)
            {
                this.FireUILogging(new UILoggingEventArg($"תקלה {DateTime.Now.ToString("T")}: אירעה שגיאת בלוגין לאתר. {chunkKey.Key}"));
                throw new Exception($"Login failed for user {this.Username}", ex);
            }
        }

        private string LookForUrl(ChunkKey chunkKey)
        {
            var firstTableToPlay = this.ChunksReposiroty[chunkKey].BetsData.First().Value;
            var urlLocator = $"?from={chunkKey.From}&to={chunkKey.To}";
            switch (firstTableToPlay.Count())
            {
                case 14:
                    return $"https://www.winner.co.il/half{urlLocator}";
                case 15:
                    return $"https://www.winner.co.il/olami{urlLocator}";
                case 16:
                    return $"https://www.winner.co.il/16?{urlLocator}";
                default:
                    throw new Exception($"Could not fetch winner url for table bet size of {firstTableToPlay.Count()}");
            }
        }

        private bool ParseFile()
        {
            this.ChunksReposiroty = new Dictionary<ChunkKey, ChunkData>();
            int counter = 0;
            string line;

            StreamReader file = new StreamReader(this.InputFile);
            var key = new ChunkKey(1, this.ChunkSize);
            var bets = new Dictionary<int, List<char>>();

            while ((line = file.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line) || string.IsNullOrWhiteSpace(line))
                    continue;
                counter++;
                var results = line.Split(',');
                foreach (var result in results)
                {
                    if (!bets.ContainsKey(counter))
                        bets.Add(counter, new List<char>());
                    bets[counter].Add((result[0]));
                }
                if (counter >= key.To)
                {
                    if (!this.ChunksReposiroty.ContainsKey(key))
                        this.ChunksReposiroty.Add(key, new ChunkData(bets));
                    key = new ChunkKey(counter+1, counter + this.ChunkSize);
                    bets = new Dictionary<int, List<char>>();
                }
            }
            if (counter % this.ChunkSize != 0 && bets.Count() > 0)
            {
                key = new ChunkKey(key.From, key.From + bets.Count() - 1);
                this.ChunksReposiroty.Add(key, new ChunkData(bets));
            }

            file.Close();
            return true;
        }
        private void FireUILogging(UILoggingEventArg e)
        {
            if (this.OnUILogging != null)
                OnUILogging(this, e);
        }
    }
}
