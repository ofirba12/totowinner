﻿using System.Collections.Generic;

namespace TotoWinner.Shrink.Driller
{
    internal class TemplateIndexWithProgramResult
    {
        public string TemplateIndex { get; }
        public UniqueProgramsNumber UniqueProgramNumbers { get; }
        public TemplateIndexWithProgramResult(string templateIndex, UniqueProgramsNumber uniqueProgramNumbers)
        {
            this.TemplateIndex = templateIndex;
            this.UniqueProgramNumbers = uniqueProgramNumbers;
        }
    }
}
