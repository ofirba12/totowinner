﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.Shrink.Driller
{
    public class Runner
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ShrinkDrillerServices _drillerSrv = ShrinkDrillerServices.Instance;
        private StatisticalDataServices _statisticalSrv = StatisticalDataServices.Instance;

        public IReadOnlyList<ProgramWithGames> Programs { get; }
        public IReadOnlyList<string> Templates { get; }
        internal Dictionary<int, Dictionary<UniqueProgramsNumber, List<string>>> Summary { get; } // {{ uniqeGroupSize, Dictionary<UniqueProgramsNumber>, List<templateWithIndex> }}
        public Runner(List<ProgramWithGames> programs, List<string> templates)
        {
            this.Programs = programs;
            this.Templates = templates;
            this.Summary = new Dictionary<int, Dictionary<UniqueProgramsNumber, List<string>>>();
        }
        private Dictionary<int, CalculatedProgramStatistics> PrepareStatiticalData()
        {
            var statisticsCollection = new Dictionary<int, CalculatedProgramStatistics>(); // {{ProgramNumber, CalculatedProgramStatistics}}
            foreach (var program in this.Programs)
            {
                var statisticalData = _statisticalSrv.Generate(program);
                statisticsCollection.Add(program.Number, statisticalData);
            }
            return statisticsCollection;
        }
        public void Start()
        {
            var statisticsCollection = this.PrepareStatiticalData();
            foreach (var template in this.Templates)
            {
                var templateHitMap = ProcessConditions(template, statisticsCollection);
                CombineHitMapToSummary(templateHitMap);
            }
        }

        private void CombineHitMapToSummary(Dictionary<int, List<TemplateIndexWithProgramResult>> templateHitMap)
        {
            foreach (var uniqueGroupId in templateHitMap)
            {
                if (!this.Summary.ContainsKey(uniqueGroupId.Key))
                    this.Summary.Add(uniqueGroupId.Key, new Dictionary<UniqueProgramsNumber, List<string>>());
                foreach (var templateWithProg in uniqueGroupId.Value)
                {
                    if (!this.Summary[uniqueGroupId.Key].ContainsKey(templateWithProg.UniqueProgramNumbers))
                        this.Summary[uniqueGroupId.Key].Add(templateWithProg.UniqueProgramNumbers, new List<string>());
                    this.Summary[uniqueGroupId.Key][templateWithProg.UniqueProgramNumbers].Add(templateWithProg.TemplateIndex);
                }
            }
        }

        //private Dictionary<int, Tuple<UniqueProgramsNumber, string>> ProcessHitMap(string template)
        //{
        //    var groups = new Dictionary<int, Tuple<UniqueProgramsNumber, string>>();// uniqueGroupSize, <UniqueProgramsNumber templateWithIndex>
        //    if (this.HitMap.ContainsKey(template))
        //    {
        //        foreach (var templateIndexResult in this.HitMap[template])
        //        {
        //            var uniqueGroupId = templateIndexResult.Value.Numbers.Count;
        //            if (!groups.ContainsKey(uniqueGroupId))
        //            {
        //                groups.Add(uniqueGroupId, new Tuple<UniqueProgramsNumber, string>(templateIndexResult.Value, templateIndexResult.Key));
        //            }
        //        }
        //    }
        //    return groups;
        //}

        private Dictionary<int, List<TemplateIndexWithProgramResult>> ProcessConditions(string template, Dictionary<int, CalculatedProgramStatistics> statisticsCollection)
        {
            var groups = new Dictionary<int, List<TemplateIndexWithProgramResult>>();// uniqueGroupSize, <TemplateIndexWithProgramResult= {UniqueProgramsNumber templateWithIndex}>

            var conditions = _drillerSrv.IsinIndexFromToRepository[template];
            Console.WriteLine($"{Environment.NewLine}Processing template: {template}");
            foreach (var condition in conditions)
            {
                var index = condition.Key;
                Utilities.DrawTextProgressBarInConsoleWindow(index, conditions.Count());

                var pNumbers = new List<int>();
                var templateWithIndex = $"{template}{index}";
                foreach (var prog in this.Programs)
                {
                    var item = GetStatisticalItem(template, statisticsCollection[prog.Number]);
                    if (item >= condition.Value.Min && item <= condition.Value.Max)
                    {
                        pNumbers.Add(prog.Number);
                    }
                }
                if (pNumbers.Count > 0)
                {
                    var itemToAdd = new UniqueProgramsNumber(pNumbers);
                    var combinations = Utilities.GetMathCombinations<int>(itemToAdd.Numbers);
                    foreach (var combination in combinations)
                    {
                        var combinationToAdd = new UniqueProgramsNumber(combination.ToList());
                        var uniqueGroupId = combinationToAdd.Numbers.Count;
                        var templateResult = new TemplateIndexWithProgramResult(templateWithIndex, combinationToAdd);
                        if (!groups.ContainsKey(uniqueGroupId))
                        {
                            groups.Add(uniqueGroupId, new List<TemplateIndexWithProgramResult>()
                                    { templateResult });
                        }
                        else
                        {
                            var alreadyExists = groups[uniqueGroupId].Any(i => i.UniqueProgramNumbers.Equals(combinationToAdd));
                            if (!alreadyExists)
                            {
                                groups[uniqueGroupId].Add(templateResult);
                            }
                        }
                    }
                }
                if (this.Programs.Count == pNumbers.Count)
                    break;
            }
            return groups;
        }

        private decimal GetStatisticalItem(string template, CalculatedProgramStatistics programData)
        {
            decimal item = 0;
            switch (_drillerSrv.IsinTypeRepository[template])
            {
                #region 1X2
                case DrillerShrinkTemplateType.HowMany1:
                    item = programData.Amount1X2.Amount1;
                    break;
                case DrillerShrinkTemplateType.HowManyX:
                    item = programData.Amount1X2.Amount2;
                    break;
                case DrillerShrinkTemplateType.HowMany2:
                    item = programData.Amount1X2.Amount3;
                    break;
                case DrillerShrinkTemplateType.HowManySeq1:
                    item = programData.SequenceLength1X2.Amount1;
                    break;
                case DrillerShrinkTemplateType.HowManySeqX:
                    item = programData.SequenceLength1X2.Amount2;
                    break;
                case DrillerShrinkTemplateType.HowManySeq2:
                    item = programData.SequenceLength1X2.Amount3;
                    break;
                case DrillerShrinkTemplateType.Breaks1X2:
                    item = programData.Breaks1X2.Amount;
                    break;
                case DrillerShrinkTemplateType.Shape2_1X2_0:
                    item = programData.ShapesGroups2Data1X2.Amount0;
                    break;
                case DrillerShrinkTemplateType.Shape2_1X2_1:
                    item = programData.ShapesGroups2Data1X2.Amount1;
                    break;
                case DrillerShrinkTemplateType.Shape2_1X2_2:
                    item = programData.ShapesGroups2Data1X2.Amount2;
                    break;
                case DrillerShrinkTemplateType.Shape2_1X2_3Plus:
                    item = programData.ShapesGroups2Data1X2.Amount3Plus;
                    break;
                case DrillerShrinkTemplateType.Shape3_1X2_0:
                    item = programData.ShapesGroups3Data1X2.Amount0;
                    break;
                case DrillerShrinkTemplateType.Shape3_1X2_1:
                    item = programData.ShapesGroups3Data1X2.Amount1;
                    break;
                case DrillerShrinkTemplateType.Shape3_1X2_2:
                    item = programData.ShapesGroups3Data1X2.Amount2;
                    break;
                case DrillerShrinkTemplateType.Shape3_1X2_3Plus:
                    item = programData.ShapesGroups3Data1X2.Amount3Plus;
                    break;
                #endregion
                #region ABC
                case DrillerShrinkTemplateType.HowManyA:
                    item = programData.AmountABC.Amount1;
                    break;
                case DrillerShrinkTemplateType.HowManyB:
                    item = programData.AmountABC.Amount2;
                    break;
                case DrillerShrinkTemplateType.HowManyC:
                    item = programData.AmountABC.Amount3;
                    break;
                case DrillerShrinkTemplateType.HowManySeqA:
                    item = programData.SequenceLengthABC.Amount1;
                    break;
                case DrillerShrinkTemplateType.HowManySeqB:
                    item = programData.SequenceLengthABC.Amount2;
                    break;
                case DrillerShrinkTemplateType.HowManySeqC:
                    item = programData.SequenceLengthABC.Amount3;
                    break;
                case DrillerShrinkTemplateType.BreaksABC:
                    item = programData.BreaksABC.Amount;
                    break;
                case DrillerShrinkTemplateType.Shape2_ABC_0:
                    item = programData.ShapesGroups2DataABC.Amount0;
                    break;
                case DrillerShrinkTemplateType.Shape2_ABC_1:
                    item = programData.ShapesGroups2DataABC.Amount1;
                    break;
                case DrillerShrinkTemplateType.Shape2_ABC_2:
                    item = programData.ShapesGroups2DataABC.Amount2;
                    break;
                case DrillerShrinkTemplateType.Shape2_ABC_3Plus:
                    item = programData.ShapesGroups2DataABC.Amount3Plus;
                    break;
                case DrillerShrinkTemplateType.Shape3_ABC_0:
                    item = programData.ShapesGroups3DataABC.Amount0;
                    break;
                case DrillerShrinkTemplateType.Shape3_ABC_1:
                    item = programData.ShapesGroups3DataABC.Amount1;
                    break;
                case DrillerShrinkTemplateType.Shape3_ABC_2:
                    item = programData.ShapesGroups3DataABC.Amount2;
                    break;
                case DrillerShrinkTemplateType.Shape3_ABC_3Plus:
                    item = programData.ShapesGroups3DataABC.Amount3Plus;
                    break;
                #endregion
                case DrillerShrinkTemplateType.DeltaMin:
                    item = programData.RelationsData.DeltaMin;
                    break;
                case DrillerShrinkTemplateType.DeltaAvg:
                    item = programData.RelationsData.DeltaMiddle;
                    break;
                case DrillerShrinkTemplateType.DeltaMax:
                    item = programData.RelationsData.DeltaMax;
                    break;
                case DrillerShrinkTemplateType.DeltaHome:
                    item = programData.RelationsData.DeltaHome;
                    break;
                case DrillerShrinkTemplateType.DeltaDraft:
                    item = programData.RelationsData.DeltaDraft;
                    break;
                case DrillerShrinkTemplateType.DeltaAway:
                    item = programData.RelationsData.DeltaAway;
                    break;
            }
            return item;
        }
    }
}
