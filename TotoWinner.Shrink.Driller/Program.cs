﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.Shrink.Driller
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static ShrinkDrillerServices _drillerSrv = ShrinkDrillerServices.Instance;
        private static ProgramServices _programSrv = ProgramServices.Instance;

        static void Main(string[] args)
        {
            try
            {
                Console.Write("Program Type (16 default) [16,15,14]:");
                var inprt = Console.ReadLine();
                var programTypeInt = string.IsNullOrEmpty(inprt) ? 16 : Convert.ToInt32(inprt);
                if (programTypeInt < 14 || programTypeInt > 16)
                    throw new Exception("Error: Invalid program type was chosen.");
                ProgramType programType = ProgramType.Winner16;
                switch (programTypeInt)
                {
                    case 16:
                        programType = ProgramType.Winner16;
                        break;
                    case 14:
                        programType = ProgramType.WinnerHalf;
                        break;
                    case 15:
                        programType = ProgramType.WinnerWorld;
                        break;
                }

                Console.Write("Program Number (All default) [use , or - for range example:{194001,194201-194401,194501}]:");
                var programInput = Console.ReadLine();
                var programsFromToCollection = ParseProgramsInput(programInput);
                var allPrograms = _programSrv.GetPersistanceProgramsWithGamesData(programType,
                    DateTime.Parse("1/1/2016"), DateTime.Today, true).OrderByDescending(program => program.Number).ToList();
                if (programsFromToCollection.Count == 0)
                {
                    var onlyNumbers = allPrograms.Select(p => p.Number).ToList();
                    programsFromToCollection.Add(new Tuple<int, int>(onlyNumbers.Min(), onlyNumbers.Max()));
                }
                var programs = PrepareProgramCollection(allPrograms, programsFromToCollection);
                var validInputFlag = false;
                List<string> templates = null;
                while (validInputFlag == false)
                {
                    Console.Write("Templates (All default) [use ',' example:{AA,AB,AC}]:");
                    var templatesInput = Console.ReadLine();
                    templatesInput = templatesInput.ToUpper();
                    if (!string.IsNullOrEmpty(templatesInput))
                    {
                        var templatesParsedInput = ParseTemplatesInput(templatesInput);
                        validInputFlag = templatesParsedInput.Item1;
                        if (validInputFlag == true)
                            templates = templatesParsedInput.Item2;
                    }
                    else
                    {
                        validInputFlag = true;
                        templates = _drillerSrv.IsinTypeRepository.Keys.ToList();
                    }

                }
                var runner = new Runner(programs, templates);
                runner.Start();
                DumpToFile("Results",runner.Summary, programs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _log.Fatal("An unexpected error occurred.", ex);
            }
            finally
            {
                Console.WriteLine("Press any key to exist:");
                Console.ReadLine();
            }
        }
        public static void DumpToFile(string folder, Dictionary<int, Dictionary<UniqueProgramsNumber, List<string>>> summary, List<ProgramWithGames> programs)
        {
            var programsNumbers = programs.Select(p => p.Number);
            var prgNumbersStr = string.Join(";", programsNumbers);
            var totalPrograms = programsNumbers.Count();
            var filename = Path.Combine(folder,$"DRILL_{DateTime.Now.Day:D2}{DateTime.Now.Month:D2}{DateTime.Now.Year}_{DateTime.Now.Hour:D2}{DateTime.Now.Minute:D2}.CSV");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            var firstSortSummary = summary.OrderByDescending(i => i.Key);

            List<string> all = new List<string>();
            all.Add("ProgramInput,TotalPrograms,UniqueGroup,GroupProgramContains,NumberOfTemplates,Templates,Strong,Medium,Low,StrongTemplates,MediumTemplates,LowTemplates");
            foreach (var kpv in firstSortSummary)
            {
                var secondSort = kpv.Value.OrderByDescending(i => i.Value.Count);                
                var uniqueGroup = kpv.Key;
                foreach (var combination in kpv.Value.Keys)
                {
                    var groupProgramContains = string.Join(";", combination.Numbers);
                    var numberOfTemplates = kpv.Value[combination].Count;
                    var templatesWithRange = new List<Tuple<string, TemplateQualification>>();
                    var stronger = 0;
                    var medium = 0;
                    var low = 0;
                    foreach (var template in kpv.Value[combination])
                    {
                        var pattern = @"([A-Z]+)([0-9]+)";
                        var match = Regex.Match(template, pattern);
                        var tName = match.Groups[1].ToString();
                        var index = Convert.ToInt32(match.Groups[2].ToString());
                        var templateRange = _drillerSrv.IsinIndexFromToRepository[tName][index];                        
                        var absRange = Math.Abs(templateRange.Max - templateRange.Min);
                        var isStornger = InRange(_drillerSrv.IsinTypeGradesRepository[tName].StrongRange, absRange);
                        stronger += isStornger ? 1 : 0;
                        var isMedium = InRange(_drillerSrv.IsinTypeGradesRepository[tName].MediumRange, absRange);
                        medium += isMedium ? 1 : 0;
                        var isLow = InRange(_drillerSrv.IsinTypeGradesRepository[tName].LowRange, absRange);
                        low += isLow || (!isStornger && !isMedium && !isLow) ? 1 : 0;
                        TemplateQualification templateQualification = TemplateQualification.Low;
                        if (isStornger)
                            templateQualification = TemplateQualification.Strong;
                        else if (isMedium)
                            templateQualification = TemplateQualification.Medium;
                        templatesWithRange.Add(new Tuple<string, TemplateQualification>($"{tName}{templateRange.Min}-{templateRange.Max}", templateQualification));
                    }
                    var templates = string.Join(";", templatesWithRange.Select(i => i.Item1));
                    var lowTemplates = string.Join(";", templatesWithRange
                        .Where(i => i.Item2 == TemplateQualification.Low)
                        .Select(i => i.Item1));
                    var mediumTemplates = string.Join(";", templatesWithRange
                        .Where(i => i.Item2 == TemplateQualification.Medium)
                        .Select(i => i.Item1));
                    var strongTemplates = string.Join(";", templatesWithRange
                        .Where(i => i.Item2 == TemplateQualification.Strong)
                        .Select(i => i.Item1));
                    var line = $"{prgNumbersStr},{totalPrograms},{uniqueGroup},{groupProgramContains},{numberOfTemplates},{templates},{stronger},{medium},{low},{strongTemplates},{mediumTemplates},{lowTemplates}";
                    all.Add(line);
                }
            }
            System.IO.File.WriteAllLines(filename, all.ToArray());
            Console.WriteLine($"{Environment.NewLine}File created: {filename}");
        }

        private static bool InRange(MinMax range, decimal value)
        {
            var res = value >= range.Min && value <= range.Max;
            return res;
        }
        //private static void CalcUniqePrograms(List<int> programs, ref List<UniqueProgramsNumber> unique)
        //{
        //    for (var i = 0 ; i < programs.Count; i++)
        //    {
        //        Console.WriteLine(string.Join(",",programs));
        //        unique.Add(new UniqueProgramsNumber(programs));
        //        var newList = programs.Where(p => p != programs[i]).ToList();
        //        CalcUniqePrograms(newList, ref unique);
        //    }
        //}

        private static Tuple<bool, List<string>> ParseTemplatesInput(string input)
        {
            var templates = new List<string>();
            var succ = true;
            string[] items = input.Split(",".ToCharArray());
            foreach (var item in items)
            {
                if (_drillerSrv.TemplateExists(item))
                {
                    if (!templates.Contains(item))
                        templates.Add(item);
                    else
                    {
                        Console.WriteLine($"Error: Invalid templates input, {item} already exists, please re-enter templates with no duplication!");
                        succ = false;
                        break;
                    }
                }
                else
                {
                    Console.WriteLine($"Error: Invalid templates input, {item} does not exists, please re-enter templates!");
                    succ = false;
                    break;
                }
            }
            var res = new Tuple<bool, List<string>>(succ, templates);
            return res;
        }

        private static List<ProgramWithGames> PrepareProgramCollection(List<ProgramWithGames> allPrograms, List<Tuple<int, int>> programsFromToCollection)
        {
            var programs = new List<ProgramWithGames>();
            foreach (var fromTo in programsFromToCollection)
            {
                if (fromTo.Item1 == fromTo.Item2)
                {
                    var program = allPrograms.FirstOrDefault(p => p.Number == fromTo.Item1);
                    if (program != null)
                        programs.Add(program);
                    else
                    {
                        throw new Exception($"Error: Program {fromTo.Item1} does not exists, please check you inputs!");
                    }
                }
                else
                {
                    if (allPrograms.FirstOrDefault(p => p.Number == fromTo.Item1) == null)
                    {
                        throw new Exception($"Error: Program range {fromTo.Item1}-{fromTo.Item2} is invalid, from program {fromTo.Item1} does not exists.please check you inputs!");
                    }
                    if (allPrograms.FirstOrDefault(p => p.Number == fromTo.Item2) == null)
                    {
                        throw new Exception($"Error: Program range {fromTo.Item1}-{fromTo.Item2} is invalid, from program {fromTo.Item2} does not exists.please check you inputs!");
                    }
                    var programRange = allPrograms.Where(p => p.Number >= fromTo.Item1 && p.Number <= fromTo.Item2);
                    if (programRange.Count() > 0)
                        programs.AddRange(programRange);
                    else
                    {
                        throw new Exception($"Error: Program range {fromTo.Item1}-{fromTo.Item2} yield no result, please check you inputs!");
                    }
                }
            }
            return programs.OrderBy(program => program.Number).ToList();
        }

        private static List<Tuple<int, int>> ParseProgramsInput(string input)
        {
            var res = new List<Tuple<int, int>>();
            if (string.IsNullOrEmpty(input))
                return res;

            string[] items = input.Split(",".ToCharArray());
            foreach (var item in items)
            {
                if (!item.Contains("-"))
                {
                    var programId = Convert.ToInt32(item);
                    res.Add(new Tuple<int, int>(programId, programId));
                }
                else
                {
                    string[] subItems = item.Split("-".ToCharArray());
                    res.Add(new Tuple<int, int>(Convert.ToInt32(subItems[0]), Convert.ToInt32(subItems[1])));
                }
            }
            return res;
        }
    }
}
