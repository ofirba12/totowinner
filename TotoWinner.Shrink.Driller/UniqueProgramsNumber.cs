﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Shrink.Driller
{
    internal class UniqueProgramsNumber
    {
        public List<int> Numbers { get; }
        public UniqueProgramsNumber(List<int> numbers)
        {
            this.Numbers = numbers;
            this.Numbers.Sort();
        }
        public override string ToString()
        {
            var res =  string.Join(",",this.Numbers);
            return res;
        }
        public override int GetHashCode()
        {
            //if (name == null) return 0;
            //return name.GetHashCode();
            const int seed = 487;
            const int modifier = 31;

            unchecked
            {
                return this.Numbers.Aggregate(seed, (current, item) =>
                    (current * modifier) + item.GetHashCode());
            }
        }

        public override bool Equals(object obj)
        {
            UniqueProgramsNumber other = obj as UniqueProgramsNumber;
            return other != null && other.ToString() == this.ToString();
        }
    }
}
