﻿using log4net.Appender;
using System.IO;

namespace TotoWinner.Shrink.Driller
{
    class TSDRollingFileAppender : RollingFileAppender
    {
        public override string File
        {
            set
            {
                base.File = Path.Combine(Directory.GetCurrentDirectory(), value);
            }
        }
    }
}
