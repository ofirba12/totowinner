import React, { useRef, useState } from 'react';
import DatePicker, { DateObject } from "react-multi-date-picker"
import type{Value} from "react-multi-date-picker"
import "react-multi-date-picker/styles/backgrounds/bg-dark.css"
import { MDBIcon } from 'mdb-react-ui-kit';

//https://shahabyazdi.github.io/react-multi-date-picker/installation/#npm


function ChooseDate({ dateFetchChangeCallback } : any) {
    const [value, setValue] = useState<Value>(new Date());
    const datePickerRef = useRef<any>();
    
    const onChangeDate = (newDate : DateObject) => {
        setValue(newDate.toDate());
        dateFetchChangeCallback(newDate.toDate());
      }
      
    function update(key : any, value : number, action: string): void {
        let date = datePickerRef.current.children[0].value;
        var newDate = new DateObject(date);
        if (action === 'add')        
            newDate.add(value, key)
        else
            newDate.subtract(value, key)
        onChangeDate(newDate);
    }
    
      return (
        <div className="pt-1 pb-1">
            <MDBIcon fas icon="angle-double-left" className='choose-date-arrow bg-black text-white' onClick={() => update("day", 1, "subtract")}/>
            <DatePicker value={value} onChange={onChangeDate}
                inputClass="custom-input bg-black text-white text-center"
                className="bg-dark" 
                ref={datePickerRef}/>
            <MDBIcon fas icon="angle-double-right" className='choose-date-arrow bg-black text-white' onClick={() => update("day", 1, "add")}/>
        </div>
    );
}
export default ChooseDate;

  