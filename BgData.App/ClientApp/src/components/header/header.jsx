// import logo from '../assets/logo.svg';
//import logo from '../assets/bg-logo.png';
import logo from "../../assets/BGlogo.svg";
import "./header.css";

const Header = () => (
  <header className="row header">
    <div className="col-md-3 col-2">
      <img src={logo} className="logo" alt="logo" />
    </div>
  </header>
);

export default Header;


