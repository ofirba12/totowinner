import { useEffect, useState } from "react";
import BgDataResponse  from "../interfaces/bg-data"

const useBgData = (fetchDate : Date) => {
    const [bgData, setBgData] = useState<BgDataResponse | undefined>(undefined);
    let day: number = fetchDate.getDate();
    console.warn("day", day);
    console.log('trying to fetch:',fetchDate,fetchDate.getDate(), fetchDate.getMonth()+1, fetchDate.getFullYear());
    useEffect(() => {
        const fetchBgData = async () => {
            // const rsp = dateToFetch.getDay() % 2 === 0? await fetch('./bg-1.json') : await fetch('./bg-2.json');
            const rsp = await fetch('./bg-1.json');
            const bgDataResponse : BgDataResponse = await rsp.json();
            setBgData(bgDataResponse);
            console.log('useEffect called');
        }
        fetchBgData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [day]);
    return bgData;
};

export default useBgData;