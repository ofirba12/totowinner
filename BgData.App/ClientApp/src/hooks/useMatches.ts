import { useMemo } from "react";
import BgDataResponse from "../interfaces/bg-data";

const useMatches = (allMatches : BgDataResponse | undefined) => {
    const matches = useMemo(() => {
        if (allMatches !== undefined)
            return allMatches;
    }, [allMatches])

    return matches;
};

export default useMatches;