export const SHOW_MORE_MATCHES_INITIAL_SIZE = 4;
export const SHOW_MORE_MATCHES_STEP_SIZE = 4;
export const SHOW_MORE_POPULATION_MAX_REF_OBJECTS = 5000;
export const pieChart3dConfig = {
  angle: 90,
  height: 0,
  showTooltips: false,
  showLabels: false,
  showLabelPercentage: false,
  textSize: 12,
  tooltipShowName: false,
  tooltipShowPercentage: false,
  tooltipShowValue: false,
  size: 0.9,
  ir: 0.5,
  strokeWidth: 4,
  stroke: "#332d2d",
  //  onClick: () => void,
  //   fixed?: number
  //   moveDistance?: number
};
