// Server side logic: if status == 'FT' need to set betResult with 'home'/'draw'/'away'

import { BgSport } from "./BgEnums";

// Server side logic: all statuses must be converted to up to 3 characters, e.g. After Over => AFT
export interface Match {
  matchId: number;
  sport: BgSport;
  homeTeamId: number;
  homeTeam: string;
  homeTeamSymbol: string;
  awayTeamId: number;
  awayTeam: string;
  awayTeamSymbol: string;
  gameTime: string;
  status: string;
  homeOdd: number;
  drawOdd: number;
  awayOdd: number;
  homeScore: number;
  awayScore: number;
  betResult: string;
}
export interface League {
  country: string;
  countryId: number;
  flagSymbol: string;
  leagueId: number;
  leagueName: string;
  matches: Match[];
}
export default interface BgDataResponse {
  fetchedTime: string;
  gmtAdjustment: number;
  leagues: League[];
}
