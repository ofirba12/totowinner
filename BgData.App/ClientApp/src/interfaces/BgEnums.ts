export enum BgSport {
  Soccer = 0,
  Basketball = 1,
}

export enum SummaryMatchTabs {
  Summary = 0,
  LastGames = 1,
  LastGamesH2H = 2,
  GamesScores = 3,
  GamesScoresH2H = 4,
  BettingTips = 5,
}