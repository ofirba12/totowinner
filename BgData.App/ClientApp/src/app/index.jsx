import { Component } from "react";
import { Routes, Route } from "react-router-dom";
import "../pages/main-page/main-page.css";
import MainPage from "../pages/main-page";
import MatchesView from "../pages/matches-view";
import MatchSummary from "../pages/match-summary";
import TeaserView from "../pages/teaser-view";
// import PageNotFound from "../pageNotFound";
import { connect } from "react-redux";
import { loadBgData } from "../redux/actions/bgDataActions";
import { changeDateToFetch } from "../redux/actions/bgCalendarActions";
import PropTypes from "prop-types";

class App extends Component {
  state = { bgData: {} };
  constructor(props) {
    super(props);
    let bodyElement = document.getElementsByTagName("body")[0];
    bodyElement.className = "bg-dark";
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route
            path={`${process.env.PUBLIC_URL}/matches/:sport`}
            element={
              <MatchesView
                bgData={this.props.bgData}
                dateToFetch={this.props.dateToFetch}
                sportToFetch={this.props.sportToFetch}
              />
            }
          />
          <Route
            path={`${process.env.PUBLIC_URL}/match-summary`}
            element={<MatchSummary></MatchSummary>}
          />
          <Route
            path="*"
            element={
              <TeaserView
                bgData={this.props.bgData}
                dateToFetch={this.props.dateToFetch}
                sportToFetch={this.props.sportToFetch}
              />
            }
          />
          {/* <Route
            path="*"
            element={
              <MatchesView
                bgData={this.props.bgData}
                dateToFetch={this.props.dateToFetch}
                sportToFetch={this.props.sportToFetch}
              />
            }
          /> */}
          {/* <Route path="*" element={<PageNotFound />} /> */}
        </Routes>
      </div>
    );
  }
}

App.propTypes = {
  dateToFetch: PropTypes.object,
  sportToFetch: PropTypes.number,
  bgData: PropTypes.object,
  loadBgData: PropTypes.func,
};
const mapDispatchToProps = {
  loadBgData,
  changeDateToFetch,
};
function mapStateToProps(state, ownProps) {
  return {
    dateToFetch: state.dateToFetch,
    sportToFetch: state.sportToFetch,
    bgData: state.bgData,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
