import { Component } from "react";
import { Routes, Route } from "react-router-dom";
import "./main-page.css";
import Header from "../components/header/header";
import SportMenu from "../pages/main-page/sport-menu";
import CalendarFilter from "../pages/main-page/calendar";
import ScoreMatches from "../pages/matches-view";
import DataTypeMenu from "../pages/main-page/data-type-menu";
import CalendarContext from "../context/calendarContext";
import PageNotFound from "../pageNotFound";

class App2 extends Component {
  //fetchDate!: Date;

  constructor(props) {
    super(props);
    let bodyElement = document.getElementsByTagName("body")[0];
    bodyElement.className = "bg-dark";
  }

  dateFetchChangeCallback = (newDate) => {
    this.fetchDate = newDate;
    //    console.log("Date Changed:", this.fetchDate);
  };

  componentDidMount() {}

  render() {
    return (
      <div>
        <Header />
        <DataTypeMenu />
        <SportMenu />
        <CalendarFilter
          dateFetchChangeCallback={this.dateFetchChangeCallback}
        />
        <CalendarContext.Provider value={this.fetchDate}>
          <Routes>
            <Route path="/" element={<App />} />
            <Route
              path={`${process.env.PUBLIC_URL}/matches/:sport`}
              element={<ScoreMatches />}
            />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </CalendarContext.Provider>
      </div>
    );
  }
}

export default App2;
