import { handleResponse, handleError } from "./apiUtils";
//import { BgSport } from "../interfaces/BgEnums";
//const baseUrl = process.env.REACT_APP_API_DEV_URL + "/BgData/";
//const baseUrl = "http://localhost:3001/BgData/";
const baseUrl = process.env.REACT_APP_API_URL;

export function getBgData(dateToFetch, bgSport) {
  //console.log("::before getBgData", dateToFetch, bgSport);
  // return fetch("./bg-data-mock.json").then(handleResponse).catch(handleError);

  const month = String(dateToFetch.month).padStart(2, "0");
  const day = String(dateToFetch.day).padStart(2, "0");
  let date = new Date(`${dateToFetch.year}-${month}-${day}T00:00:00Z`);
  let timeOffset = new Date().getTimezoneOffset();
  //  console.log("::before getBgData fetch", date, timeOffset);
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      sport: bgSport,
      fetchDate: date,
      timezoneOffset: timeOffset,
    }),
  };
  return fetch(baseUrl, requestOptions).then(handleResponse).catch(handleError);
}

export function getMatchSummary(matchId) {
  let timeOffset = new Date().getTimezoneOffset();
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      matchId: matchId,
      timezoneOffset: timeOffset,
    }),
  };
  return fetch(process.env.REACT_APP_BGDATA_SUMMARY_MATCH_URL, requestOptions)
    .then(handleResponse)
    .catch(handleError);
  // return fetch("./match-summary-mock.json")
  //   .then(handleResponse)
  //   .catch(handleError);
}
export function getTeasers() {
  let timeOffset = new Date().getTimezoneOffset();
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      timezoneOffset: timeOffset,
    }),
  };
  // return fetch(process.env.REACT_APP_BGDATA_TEASERS_URL, requestOptions)
  //   .then(handleResponse)
  //   .catch(handleError);
  return fetch("./teasers-mock.json").then(handleResponse).catch(handleError);
}
