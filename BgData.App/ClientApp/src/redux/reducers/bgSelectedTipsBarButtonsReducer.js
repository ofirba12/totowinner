import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSelectedTipsBarButtonsReducer(
  state = initialState.selectedTipsButtons,
  action
) {
  switch (action.type) {
    case types.SELECTED_TIPS_BUTTON:
      return action.selectedTipsButtons;
    default:
      return state;
  }
}
