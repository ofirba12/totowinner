import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgDataReducer(state = initialState.teasers, action) {
  switch (action.type) {
    case types.LOAD_TEASERS_SUCCESS:
      return action.teasers;
    default:
      return state;
  }
}
