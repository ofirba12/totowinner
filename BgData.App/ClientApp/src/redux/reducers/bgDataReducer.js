import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgDataReducer(state = initialState.bgData, action) {
  switch (action.type) {
    case types.LOAD_BGDATA_SUCCESS:
      return action.bgData;
    default:
      return state;
  }
}
