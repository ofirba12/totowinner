import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgMatchSummaryReducer(state = initialState.matchSummary, action) {
  switch (action.type) {
    case types.LOAD_MATCH_SUMMARY_SUCCESS:
      return action.matchSummary;
    default:
      return state;
  }
}
