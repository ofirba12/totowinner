import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSelectedDigestSummaryTabsReducer(
  state = initialState.selectedDigestTab,
  action
) {
  switch (action.type) {
    case types.SELECTED_DIGEST_SUMMARY_TABS:
      return action.selectedDigestTab;
    default:
      return state;
  }
}
