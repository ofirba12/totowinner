import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgDataTypeButtonsReducer(state = initialState.dataType, action) {
  switch (action.type) {
    case types.DATA_TYPE_CHANGE:
      return action.dataType;
    default:
      return state;
  }
}
