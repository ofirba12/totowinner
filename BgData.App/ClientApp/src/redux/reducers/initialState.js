import * as contants from "../../interfaces/defaultValues";
export default {
  dateToFetch: {
    day: new Date().getDate(),
    month: new Date().getMonth() + 1,
    year: new Date().getFullYear(),
    hours: 0,
    minutes: 0,
    seconds: 0,
    ms: 0,
  },
  sportToFetch: 0,
  dataType: "scores",
  bgData: {},
  teasers: {},
  selectedMatch: {},
  matchSummary: {},
  selectedSummaryMatchTab: 0,
  selectedPopulationMatchTab: {
    lastGames: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
    lastGamesH2H: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
    gamesScores: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
    gameScoresH2H: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
    summaryWDL: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
    summaryMomentum: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
    summaryScores: {
      homeTab: 0,
      homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      awayTab: 0,
      awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
    },
  },
  selectedTipsButtons: [],
  selectedScoresTipButton: "Over/Under 2.5",
  selectedDigestTab: {
    digestTabTypeIndex: 2,
    digestTabSizeIndex: 1,
  },
  digestChartClicked: false,
};
