import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSelectedScoreTipBarButtonReducer(
  state = initialState.selectedScoresTipButton,
  action
) {
  switch (action.type) {
    case types.SELECTED_SCORES_TIP_BUTTON:
      return action.selectedScoresTipButton;
    default:
      return state;
  }
}
