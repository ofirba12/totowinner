import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSelectedMatchReducer(state = initialState.selectedMatch, action) {
  switch (action.type) {
    case types.SELECTED_MATCH:
      return action.selectedMatch;
    default:
      return state;
  }
}
