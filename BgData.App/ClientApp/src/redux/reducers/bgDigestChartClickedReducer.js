import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgDigestChartClickedReducer(
  state = initialState.digestChartClicked,
  action
) {
  switch (action.type) {
    case types.DIGEST_CHART_CLICKED:
      return action.digestChartClicked;
    default:
      return state;
  }
}
