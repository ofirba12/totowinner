import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSelectedPopulationMatchTabReducer(
  state = initialState.selectedPopulationMatchTab,
  action
) {
  switch (action.type) {
    case types.SELECTED_POPULATION_MATCH_TAB:
    case types.SELECTED_POPULATION_LAST_INDEX_SHOWN: 
      return action.selectedPopulationTabIndex;
    default:
      return state;
  }
}
