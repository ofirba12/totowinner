import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSportMenuReducer(state = initialState.sportToFetch, action) {
  switch (action.type) {
    case types.SPORT_TO_FETCH_CHANGE:
      return action.sportToFetch;
    default:
      return state;
  }
}
