import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgCanledarReducer(state = initialState.dateToFetch, action) {
  switch (action.type) {
    case types.DATE_TO_FETCH_CHANGE:
      return action.dateToFetch;
    default:
      return state;
  }
}
