import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function bgSelectedSummaryMatchTabReducer(
  state = initialState.selectedSummaryMatchTab,
  action
) {
  switch (action.type) {
    case types.SELECTED_SUMMARY_MATCH_TAB:
      return action.selectedTabIndex;
    default:
      return state;
  }
}
