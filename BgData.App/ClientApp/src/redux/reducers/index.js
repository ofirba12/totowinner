import { combineReducers } from "redux";
import { withReduxStateSync } from "redux-state-sync";
import bgData from "./bgDataReducer";
import teasers from "./bgTeasersReducer";
import dateToFetch from "./bgCanledarReducer";
import sportToFetch from "./bgSportMenuReducer";
import selectedMatch from "./bgSelectedMatchReducer";
import matchSummary from "./bgMatchSummaryReducer";
import selectedSummaryMatchTab from "./bgSelectedSummaryMatchTabReducer";
import selectedPopulationMatchTab from "./bgSelectedPopulationMatchTabReducer";
import dataType from "./bgDataTypeButtonsReducer";
import selectedTipsButtons from "./bgSelectedTipsBarButtonsReducer";
import selectedScoresTipButton from "./bgSelectedScoresTipButtonReducer";
import selectedDigestTab from "./bgSelectedDigestSummaryTabsReducer";
import digestChartClicked from "./bgDigestChartClickedReducer";

const rootReducer = combineReducers({
  bgData,
  teasers,
  dateToFetch,
  sportToFetch,
  selectedMatch,
  matchSummary,
  selectedSummaryMatchTab,
  selectedDigestTab,
  selectedPopulationMatchTab,
  dataType,
  selectedTipsButtons,
  selectedScoresTipButton,
  digestChartClicked,
});

// export default rootReducer;
export default withReduxStateSync(rootReducer);
