import * as types from "./actionTypes";

export function dateToFetchChanged(dateToFetch) {
  return { type: types.DATE_TO_FETCH_CHANGE, dateToFetch };
}

export function changeDateToFetch(dateToFetch) {
  return function (dispatch) {
    dispatch(dateToFetchChanged(dateToFetch));
  };
}
