import * as types from "./actionTypes";
import * as bgDataApi from "../../api/bgDataApi";

export function loadBgDataSuccess(bgData) {
  return { type: types.LOAD_BGDATA_SUCCESS, bgData };
}

export function loadBgData(dateToFetch, bgSport) {
//  console.log("::before loadBgData", dateToFetch, bgSport);
  return function (dispatch) {
    return bgDataApi
      .getBgData(dateToFetch, bgSport)
      .then((bgData) => {
        dispatch(loadBgDataSuccess(bgData));
      })
      .catch((error) => {
        throw error;
      });
  };
}

export function loadTeasersSuccess(teasers) {
  return { type: types.LOAD_TEASERS_SUCCESS, teasers };
}

export function loadTeasers() {
  return function (dispatch) {
    return bgDataApi
      .getTeasers()
      .then((teasers) => {
        console.log("teasers", teasers);
        dispatch(loadTeasersSuccess(teasers));
      })
      .catch((error) => {
        throw error;
      });
  };
}
