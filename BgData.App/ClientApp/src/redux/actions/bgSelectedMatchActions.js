import * as types from "./actionTypes";

export function matchSelected(selectedMatch) {
  return { type: types.SELECTED_MATCH, selectedMatch };
}

export function selectMatch(selectedMatch) {
  return function (dispatch) {
    dispatch(matchSelected(selectedMatch));
  };
}
