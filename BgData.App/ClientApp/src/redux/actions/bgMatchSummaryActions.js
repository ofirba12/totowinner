import * as types from "./actionTypes";
import * as bgDataApi from "../../api/bgDataApi";

export function loadBgMatchSummarySuccess(matchSummary) {
  return { type: types.LOAD_MATCH_SUMMARY_SUCCESS, matchSummary };
}

export function loadBgMatchSummary(matchId) {
  return function (dispatch) {
    return bgDataApi
      .getMatchSummary(matchId)
      .then((matchSummary) => {
        dispatch(loadBgMatchSummarySuccess(matchSummary));
      })
      .catch((error) => {
        throw error;
      });
  };
}
