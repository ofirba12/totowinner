import * as types from "./actionTypes";

export function sportToFetchChanged(sportToFetch) {
  return { type: types.SPORT_TO_FETCH_CHANGE, sportToFetch };
}

export function changeSportToFetch(sportToFetch) {
  return function (dispatch) {
    dispatch(sportToFetchChanged(sportToFetch));
  };
}

export function dataTypeChanged(dataType) {
  return { type: types.DATA_TYPE_CHANGE, dataType };
}

export function changeDataType(dataType) {
  return function (dispatch) {
    dispatch(dataTypeChanged(dataType));
  };
}

export function selectedTipsButtonsChanged(selectedTipsButtons) {
  return { type: types.SELECTED_TIPS_BUTTON, selectedTipsButtons };
}

export function changeSelectedTipsButtons(selectedTipsButtons) {
  return function (dispatch) {
    dispatch(selectedTipsButtonsChanged(selectedTipsButtons));
  };
}

export function selectedScoresTipButtonChanged(selectedScoresTipButton) {
  return { type: types.SELECTED_SCORES_TIP_BUTTON, selectedScoresTipButton };
}

export function changeSelectedScoresTipButton(selectedScoresTipButton) {
  return function (dispatch) {
    dispatch(selectedScoresTipButtonChanged(selectedScoresTipButton));
  };
}
