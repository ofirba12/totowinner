import * as types from "./actionTypes";

export function tabSelected(selectedTabIndex) {
  return { type: types.SELECTED_SUMMARY_MATCH_TAB, selectedTabIndex };
}
export function tabPopulationSelected(selectedPopulationTabIndex) {
  return {
    type: types.SELECTED_POPULATION_MATCH_TAB,
    selectedPopulationTabIndex,
  };
}
export function tabPopulationLastIndexShownSelected(selectedPopulationTabIndex) {
  return {
    type: types.SELECTED_POPULATION_LAST_INDEX_SHOWN,
    selectedPopulationTabIndex,
  };
}
export function tabDigestMatchTabsSelected(selectedDigestTab) {
  return {
    type: types.SELECTED_DIGEST_SUMMARY_TABS,
    selectedDigestTab,
  };
}
export function tabDigestChartClicked(digestChartClicked) {
  return {
    type: types.DIGEST_CHART_CLICKED,
    digestChartClicked,
  };
}
export function selectMatchSummaryTabActions(selectedTabIndex) {
  return function (dispatch) {
    dispatch(tabSelected(selectedTabIndex));
  };
}
export function selectPopulationMatchTabActions(selectedPopulationTabIndex) {
  return function (dispatch) {
    dispatch(tabPopulationSelected(selectedPopulationTabIndex));
  };
}
export function selectPopulationLastIndexShownActions(selectedPopulationTabIndex) {
  return function (dispatch) {
    dispatch(tabPopulationLastIndexShownSelected(selectedPopulationTabIndex));
  };
}
export function selectDigestMatchTabsActions(selectedDigestTab) {
  return function (dispatch) {
    dispatch(tabDigestMatchTabsSelected(selectedDigestTab));
  };
}
export function clickedDigestChartActions(digestChartClicked) {
  return function (dispatch) {
    dispatch(tabDigestChartClicked(digestChartClicked));
  };
}