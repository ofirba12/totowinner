//import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import thunk from "redux-thunk";
import {
  createStateSyncMiddleware,
  initStateWithPrevTab,
} from "redux-state-sync";
import { configureStore } from '@reduxjs/toolkit';

// export default function configureStore(initialState) {
//   const composeEnhancers =
//     window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
//   const config = {};
//   const middlewares = [
//     thunk,
//     reduxImmutableStateInvariant(),
//     createStateSyncMiddleware(config),
//   ];
//   const store = createStore(
//     rootReducer,
//     initialState,
//     composeEnhancers(applyMiddleware(...middlewares))
//   );
//   initStateWithPrevTab(store);
//   return store;
// }

export default function setStore(initialState) {
  const config = {};
  const middlewares = [
    thunk,
    reduxImmutableStateInvariant(),
    createStateSyncMiddleware(config)
  ];
  const store = configureStore({
    middleware: middlewares,
    reducer: rootReducer,
    initialState,
  });
  initStateWithPrevTab(store)
  return store;
}