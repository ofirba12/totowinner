import LeagueMatches from "./league-matches";
import { connect } from "react-redux";
import { loadBgData } from "../../redux/actions/bgDataActions";
import PropTypes from "prop-types";

function MatchesDetails({ bgData, leagueId, loadBgData }) {
  const league = bgData.leagues.find((l) => l.leagueId === leagueId);
//  console.log("league details", league);

  return (
    <div key={league?.leagueId} className="bg-black">
      <div className="justify-content-center col-1 d-inline-block">
        <i className={`flag flag-${league?.flagSymbol} flag-middle`}></i>
      </div>
      <div className="col-11 match-leagueName d-inline-block">
        {league?.leagueName}
      </div>
      <LeagueMatches key={league?.leagueId} leagueId={league?.leagueId} />
    </div>
  );
}
MatchesDetails.propTypes = {
  bgData: PropTypes.object.isRequired,
  loadBgData: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    bgData: state.bgData,
  };
}

const mapDispatchToProps = {
  loadBgData,
};

export default connect(mapStateToProps, mapDispatchToProps)(MatchesDetails);
