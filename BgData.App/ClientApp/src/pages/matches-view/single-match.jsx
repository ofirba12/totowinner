import { useRef, useEffect } from "react";
import { connect } from "react-redux";
import * as contants from "../../interfaces/defaultValues";
import { selectMatch } from "../../redux/actions/bgSelectedMatchActions";
import PropTypes from "prop-types";
import { BgSport } from "../../interfaces/BgEnums";
import {
  selectPopulationMatchTabActions,
  selectDigestMatchTabsActions,
} from "../../redux/actions/bgSelectedMatchSummaryTabActions";
import { changeSelectedScoresTipButton } from "../../redux/actions/bgMenuActions";

function SingleMatch({
  bgData,
  dataType,
  leagueId,
  matchId,
  selectMatch,
  selectPopulationMatchTabActions,
  selectedTipsButtons,
  changeSelectedScoresTipButton,
  selectDigestMatchTabsActions,
  selectedDigestTab,
}) {
  const leagueMatches = bgData.leagues.find((l) => l.leagueId === leagueId);
  let match = leagueMatches?.matches.find((m) => m.matchId === matchId);
  let homeImage = `./teams-image/${match?.homeTeamSymbol}.png`;
  let awayImage = `./teams-image/${match?.awayTeamSymbol}.png`;
  let fallBackSrc = `./teams-image/missing-image.png`;
  let rowSpan = match.sport === BgSport.Soccer ? 3 : 2;
  const drawOddRef = useRef();
  const scoresTdRef = useRef();
  const statusTdRef = useRef();
  const oddsTdRef = useRef();
  const bet1TdRef = useRef();
  const bet1TdValueRef = useRef();
  const bet2TdRef = useRef();
  const bet2TdValueRef = useRef();
  const bet3TdRef = useRef();
  const bet3TdValueRef = useRef();
  const bet4TdRef = useRef();
  const bet4TdValueRef = useRef();
  const bet5TdRef = useRef();
  const bet5TdValueRef = useRef();
  const bet6TdRef = useRef();
  const bet6TdValueRef = useRef();
  const bet7TdRef = useRef();
  const bet7TdValueRef = useRef();
  function updateState() {
    selectPopulationMatchTabActions({
      lastGames: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
      lastGamesH2H: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
      gamesScores: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
      gamesScoresH2H: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
      summaryWDL: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
      summaryMomentum: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
      summaryScores: {
        homeTab: 0,
        awayTab: 0,
        homeTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
        awayTabLastIndexShown: contants.SHOW_MORE_MATCHES_INITIAL_SIZE,
      },
    });
  }
  function getBetValueClass(betValue) {
    switch (betValue) {
      case 5:
        return "quality-5";
      case 4:
        return "quality-4";
      case 3:
        return "quality-3";
      default:
        return "no-quality";
    }
  }
  useEffect(() => {
    if (match.sport === BgSport.Basketball) {
      drawOddRef.current?.classList.add("d-none");
    } else if (match.sport === BgSport.Soccer) {
      if (dataType === "tips") {
        scoresTdRef.current?.classList.add("d-none");
        statusTdRef.current?.classList.add("d-none");
        oddsTdRef.current?.classList.add("d-none");
      } else if (dataType === "scores") {
        scoresTdRef.current?.classList.remove("d-none");
        statusTdRef.current?.classList.remove("d-none");
        oddsTdRef.current?.classList.remove("d-none");
        bet1TdRef.current?.classList.add("d-none");
        bet2TdRef.current?.classList.add("d-none");
        bet3TdRef.current?.classList.add("d-none");
        bet4TdRef.current?.classList.add("d-none");
        bet5TdRef.current?.classList.add("d-none");
        bet6TdRef.current?.classList.add("d-none");
        bet7TdRef.current?.classList.add("d-none");
      }
    }
  }, [match.sport, dataType]);
  useEffect(() => {
    if (
      match.betTips &&
      match.sport === BgSport.Soccer &&
      dataType === "tips"
    ) {
      bet1TdRef.current?.classList.add("d-none");
      bet2TdRef.current?.classList.add("d-none");
      bet3TdRef.current?.classList.add("d-none");
      bet4TdRef.current?.classList.add("d-none");
      bet5TdRef.current?.classList.add("d-none");
      bet6TdRef.current?.classList.add("d-none");
      bet7TdRef.current?.classList.add("d-none");
      selectedTipsButtons.forEach((element) => {
        switch (element) {
          case "FullTime":
            bet1TdRef.current?.classList.remove("d-none");
            bet1TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.fullTimeQuality)
            );
            break;
          case "Over/Under 1.5":
            bet2TdRef.current?.classList.remove("d-none");
            bet2TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.overUnder1_5Quality)
            );
            break;
          case "Over/Under 2.5":
            bet3TdRef.current?.classList.remove("d-none");
            bet3TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.overUnder2_5Quality)
            );
            break;
          case "Over/Under 3.5":
            bet4TdRef.current?.classList.remove("d-none");
            bet4TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.overUnder3_5Quality)
            );
            break;
          case "Range":
            bet5TdRef.current?.classList.remove("d-none");
            bet5TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.rangeQuality)
            );
            break;
          case "Both Score":
            bet6TdRef.current?.classList.remove("d-none");
            bet6TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.bothScoreQuality)
            );
            break;
          case "Double Bet":
            bet7TdRef.current?.classList.remove("d-none");
            bet7TdValueRef.current?.classList.add(
              getBetValueClass(match.betTips.doubleQuality)
            );
            break;
          default:
            break;
        }
      });
    }
  }, [selectedTipsButtons, dataType]);
  return (
    <>
      <tr></tr>
      <tr></tr>
      <tr
        onClick={() => {
          console.log(`Clicked ${match?.homeTeam} vs ${match?.awayTeam}`);
          selectMatch(match);
          // navigate(`${process.env.PUBLIC_URL}/match-summary`); //same page, not to be used
          changeSelectedScoresTipButton("Over/Under 2.5");
          selectDigestMatchTabsActions({
            ...selectedDigestTab,
            digestTabTypeIndex: 2,
          });
          window.open("match-summary"); //same tab (develpoment)
          //new window (production)
          // window.open(
          //   "match-summary",
          //   "_blank",
          //   "toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=0,width=400"
          // );
          updateState();
        }}
      >
        <td rowSpan={rowSpan} className="align-middle team-iconn-fixed-size">
          <div className="text-center">
            <img
              src={homeImage}
              className="team-image"
              alt="Home Team"
              onError={(e) => (e.currentTarget.src = fallBackSrc)}
            ></img>
          </div>
          <div className="text-center team-image-away">
            <img
              src={awayImage}
              className="team-image"
              alt="Away Team"
              onError={(e) => (e.currentTarget.src = fallBackSrc)}
            ></img>
          </div>
        </td>
        <td rowSpan={rowSpan} className="align-middle teams-names">
          <div>{match?.homeTeam}</div>
          <div>{match?.awayTeam}</div>
        </td>
        <td
          rowSpan={rowSpan}
          className="align-middle scores-fixed-size"
          ref={scoresTdRef}
        >
          <div className="text-center">{match?.homeScore}&nbsp;</div>
          <div className="text-center">{match?.awayScore}&nbsp;</div>
        </td>
        <td
          rowSpan={rowSpan}
          className="align-middle status-fixed-size"
          ref={statusTdRef}
        >
          <div className="text-center match-status">{match?.status}</div>
        </td>
        <td
          rowSpan={rowSpan}
          className="align-middle odds-fixed-size"
          ref={oddsTdRef}
        >
          <div
            className={`text-center ${
              match?.betResult === "home" ? "mark-odd" : ""
            }`}
          >
            {match?.homeOdd === 0 ? " " : match?.homeOdd.toFixed(2)}
          </div>
          <div
            className={`text-center ${
              match?.betResult === "draw" ? "mark-odd" : ""
            }`}
            ref={drawOddRef}
          >
            {match?.drawOdd === 0 ? " " : match?.drawOdd.toFixed(2)}
          </div>
          <div
            className={`text-center ${
              match?.betResult === "away" ? "mark-odd" : ""
            }`}
          >
            {match?.awayOdd === 0 ? " " : match?.awayOdd.toFixed(2)}
          </div>
        </td>
        {match.betTips === null ? (
          <>
            <td> </td>
            <td> </td>
            <td> </td>
          </>
        ) : (
          <>
            <td className="align-middle bet-style d-none" ref={bet1TdRef}>
              <div className="text-center">{match.betTips?.fullTimeTitle}</div>
              <div className="text-center" ref={bet1TdValueRef}>
                {match.betTips?.fullTimeValue}
              </div>
            </td>
            <td className="align-middle bet-style d-none" ref={bet2TdRef}>
              <div className="text-center">
                {match.betTips?.overUnder1_5Title}
              </div>
              <div className="text-center" ref={bet2TdValueRef}>
                {match.betTips?.overUnder1_5Value}
              </div>
            </td>
            <td className="align-middle bet-style d-none" ref={bet3TdRef}>
              <div className="text-center">
                {match.betTips?.overUnder2_5Title}
              </div>
              <div className="text-center" ref={bet3TdValueRef}>
                {match.betTips?.overUnder2_5Value}
              </div>
            </td>
            <td className="align-middle bet-style d-none" ref={bet4TdRef}>
              <div className="text-center">
                {match.betTips?.overUnder3_5Title}
              </div>
              <div className="text-center" ref={bet4TdValueRef}>
                {match.betTips?.overUnder3_5Value}
              </div>
            </td>
            <td className="align-middle bet-style d-none" ref={bet5TdRef}>
              <div className="text-center">{match.betTips?.rangeTitle}</div>
              <div className="text-center" ref={bet5TdValueRef}>
                {match.betTips?.rangeValue}
              </div>
            </td>
            <td className="align-middle bet-style d-none" ref={bet6TdRef}>
              <div className="text-center">{match.betTips?.bothScoreTitle}</div>
              <div className="text-center" ref={bet6TdValueRef}>
                {match.betTips?.bothScoreValue}
              </div>
            </td>
            <td className="align-middle bet-style d-none" ref={bet7TdRef}>
              <div className="text-center">{match.betTips?.doubleTitle}</div>
              <div className="text-center" ref={bet7TdValueRef}>
                {match.betTips?.doubleValue}
              </div>
            </td>
          </>
        )}
      </tr>
    </>
  );
}

SingleMatch.propTypes = {
  selectedTipsButtons: PropTypes.array.isRequired,
  dataType: PropTypes.string.isRequired,
  bgData: PropTypes.object.isRequired,
  selectMatch: PropTypes.func.isRequired,
  changeSelectedScoresTipButton: PropTypes.func.isRequired,
  selectDigestMatchTabsActions: PropTypes.func.isRequired,
  selectedDigestTab: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    selectedTipsButtons: state.selectedTipsButtons,
    dataType: state.dataType,
    bgData: state.bgData,
    selectMatch: state.selectMatch,
    selectedDigestTab: state.selectedDigestTab,
  };
}

const mapDispatchToProps = {
  selectMatch,
  selectPopulationMatchTabActions,
  changeSelectedScoresTipButton,
  selectDigestMatchTabsActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(SingleMatch);
