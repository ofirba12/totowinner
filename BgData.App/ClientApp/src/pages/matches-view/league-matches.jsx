import SingleMatch from "./single-match";
import { connect } from "react-redux";
import { loadBgData } from "../../redux/actions/bgDataActions";
import PropTypes from "prop-types";

function LeagueMatches({ bgData, leagueId }) {
  const leagueMatches = bgData.leagues.find((l) => l.leagueId === leagueId);
  return (
    <table
      key={`${leagueMatches?.leagueId}`}
      className="table table-striped table-dark table-hover table-sm table-matches"
    >
      <tbody>
        {leagueMatches?.matches.map((m) => (
          <SingleMatch
            key={m.matchId}
            leagueId={leagueId}
            matchId={m.matchId}
          />
        ))}
      </tbody>
    </table>
  );
}
LeagueMatches.propTypes = {
  bgData: PropTypes.object.isRequired,
  loadBgData: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    bgData: state.bgData,
  };
}

const mapDispatchToProps = {
  loadBgData,
};

export default connect(mapStateToProps, mapDispatchToProps)(LeagueMatches);
