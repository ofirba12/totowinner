import useBgData from "../../hooks/useBgData";
import useMatches from "../../hooks/useMatches";
import BgDataResponse from "../../interfaces/bg-data";
import MatchesDetails from "./matches-details";
import BgDataContext from "../../context/bgDataContext";
import "./matches.css";
import CalendarContext from "../../context/calendarContext";
import { useContext } from "react";

function Matches() {
  const fetchDate = useContext<Date>(CalendarContext);
  const bgData: BgDataResponse | undefined = useBgData(
    fetchDate ? fetchDate : new Date()
  );
  //const bgData : BgDataResponse | undefined = useRefreshData(fetchDate ? fetchDate : new Date());
  const bgMatchesData = useMatches(bgData);
  console.log("bgMatchesData", bgMatchesData);

  return (
    <div className="container">
      <div className="col-12 text-white">
        <BgDataContext.Provider value={bgMatchesData?.leagues}>
          {bgData?.leagues.map((m) => (
            <MatchesDetails key={m.leagueId} leagueId={m.leagueId} />
          ))}
        </BgDataContext.Provider>
      </div>
    </div>
  );
}

export default Matches;
