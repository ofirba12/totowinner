import MatchesDetails from "./matches-details";
import MainPage from "../main-page";
import "./matches.css";
import { useEffect} from "react";
import { loadBgData } from "../../redux/actions/bgDataActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TipsBar  from "../tips-bar";

function MatchesView({ bgData, loadBgData, dateToFetch, sportToFetch }) {
  useEffect(() => {
    loadBgData(dateToFetch, sportToFetch)
      //      .then(console.log("ScoreMatches useEffect called", bgData, sportToFetch))
      .catch((error) => {
        alert("Loading bgData failed" + error);
      });
  }, [dateToFetch, sportToFetch]);

  if (bgData.leagues) {
    return (
      <>
        <MainPage />
        <TipsBar />
        <div className="container">
          <div className="col-12 text-white">
            {bgData.leagues.map((m) => (
              <MatchesDetails key={m.leagueId} leagueId={m.leagueId} />
            ))}
          </div>
        </div>
      </>
    );
  } else {
    return <MainPage />;
  }
}
MatchesView.propTypes = {
  bgData: PropTypes.object.isRequired,
  loadBgData: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    bgData: state.bgData,
  };
}

const mapDispatchToProps = {
  loadBgData,
};

export default connect(mapStateToProps, mapDispatchToProps)(MatchesView);
