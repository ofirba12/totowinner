import React from "react";
import TeaserTipHeader from "./teaser-tip-header";
import TeaserTipMatch from "./teaser-tip-match";
import TeaserTipBet from "./teaser-tip-bet";
import TeaserTipBetSend from "./teaser-tip-bet-send";
function TeaserTip({ tipImage, adjustHeight, tipIndex }) {
  const [imageLoaded, setImageLoaded] = React.useState(false);

  const handleImageLoad = () => {
    setImageLoaded(true);
  };
  const containerStyle = {
    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0), #262626), url(${tipImage})`,
    minHeight: `${adjustHeight - 200}px`,
    opacity: imageLoaded ? 1 : 0,
    transition: "opacity 0.5s",
  };
  return (
    <div className="card bg-image m-2 card-image" style={containerStyle}>
      {!imageLoaded && (
        <div
          className="placeholder"
          style={{ minHeight: `${adjustHeight - 200}px` }}
        >
          ....
        </div>
      )}
      <div className="card-body">
        <TeaserTipHeader tipIndex={tipIndex} />
        <TeaserTipMatch tipIndex={tipIndex} />
        <TeaserTipBet tipIndex={tipIndex} />
        <TeaserTipBetSend tipIndex={tipIndex} />
      </div>
      <img
        src={tipImage}
        alt="loading"
        className="d-none"
        onLoad={handleImageLoad}
      />
    </div>
  );
}

export default TeaserTip;

