import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TopTotalBar from "./teaser-content-top-totals";
import teaser3 from "../../assets/teaser3.jpg";
import teaser6 from "../../assets/teaser6.jpg";
import TeaserTip from "./teaser-tip";
import { loadTeasers } from "../../redux/actions/bgDataActions";
const bestBets = [[0], [1], [2], [3]];
function TeaserContent({ adjustHeight, loadTeasers, teasers }) {
  React.useEffect(() => {
    loadTeasers()
      .catch((error) => {
        alert("Loading teasers failed" + error);
      });
  }, []);
  return (
    teasers?.bgDataBets !== undefined && (
      <>
        <TopTotalBar />
        {teasers.bgDataBets.map((tip, index) => (
          <TeaserTip
            tipImage={index % 2 === 0 ? teaser3 : teaser6}
            adjustHeight={adjustHeight}
            key={index}
            tipIndex={index}
          />
        ))}
      </>
    )
  );
}

TeaserContent.propTypes = {
  teasers: PropTypes.object.isRequired,
  loadTeasers: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    teasers: state.teasers,
  };
}

const mapDispatchToProps = {
  loadTeasers,
};

export default connect(mapStateToProps, mapDispatchToProps)(TeaserContent);