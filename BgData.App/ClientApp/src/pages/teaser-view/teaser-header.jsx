import React from "react";
import logo from "../../assets/BGlogo.svg";
import { MDBIcon } from "mdb-react-ui-kit";
function TeaserHeader() {
  return (
    <div className="row">
      <div className="col-10">
        <img src={logo} className="logo" alt="logo" />
      </div>
      {/* <div className="col-2 text-center text-white">
        <MDBIcon fas icon="bars" className="text-white" />
      </div> */}
      <div className="col-2 text-center text-white dropdown">
        <button
          className="btn dropdown-toggle"
          type="button"
          id="dropdownMenuButton"
          data-mdb-toggle="dropdown"
          aria-expanded="false"
        >
          <MDBIcon fas icon="bars" className="text-white" />
        </button>
        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <li>
            <a className="dropdown-item" href="#">
              Winner
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="#">
              Bet24
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
export default TeaserHeader;
