import React from "react";
import TeaserHeader from "./teaser-header";
import TeaserContent from "./teaser-content";
import TeaserFooter from "./teaser-footer";
import "./styles.css";
function TeaserView() {
  const [headerHeight, setHeaderHeight] = React.useState(0);
  const [footerHeight, setFooterHeight] = React.useState(0);
  const headerRef = React.useRef(null);
  const footerRef = React.useRef(null);

  React.useEffect(() => {
    if (headerRef.current) {
      const headerHeight = headerRef.current.offsetHeight;
      setHeaderHeight(headerHeight);
      console.log("headerHeight", headerHeight);
    }

    if (footerRef.current) {
      const footerHeight = footerRef.current.offsetHeight;
      setFooterHeight(footerHeight);
      console.log("footerHeight", footerHeight);
    }
    console.log("window.innerHeight", window.innerHeight);
  }, []);
  return (
    <div className="app-container">
      <header className="sticky-header" ref={headerRef}>
        <TeaserHeader />
      </header>
      <div className="content-container">
        <main className="content">
          <TeaserContent
            adjustHeight={window.innerHeight - headerHeight - footerHeight+ 30}
          />
        </main>
      </div>
      <footer className="sticky-footer" ref={footerRef}>
        <TeaserFooter />
      </footer>
    </div>
  );
}

export default TeaserView;
