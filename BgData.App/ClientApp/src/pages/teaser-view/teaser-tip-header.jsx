import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import smallSoccerImage from "../../assets/soccer-small.png";

const imageStyle = {
  border: "#332f2f solid 1px",
  backgroundColor: "#393434",
  borderRadius: "15px",
};

function TeaserTipHeader({ tipIndex ,teasers }) {
  return (
    <div className="row mb-3">
      <div
        className="col-2 d-flex align-items-center text-center"
        style={imageStyle}
      >
        <img
          src={smallSoccerImage}
          id="soccer"
          style={{ height: "30px", width: "30px" }}
          alt="Soccer"
        />
      </div>
      <div className="col-10 text-white" style={{ fontSize: "75%" }}>
        <div>{teasers.bgDataBets[tipIndex].country}</div>
        <div>{teasers.bgDataBets[tipIndex].league }</div>
      </div>
    </div>
  );
}

TeaserTipHeader.propTypes = {
  teasers: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    teasers: state.teasers,
  };
}
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TeaserTipHeader);
