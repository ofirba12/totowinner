import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

function TeaserTipBetSend({ tipIndex, teasers }) {
  return (
    <div className="bet-send-box d-flex justify-content-evenly">
      <div className="col-4 text-white text-center">
        <div style={{ fontSize: "10px" }}>ODDS</div>
        <h5>{teasers.bgDataBets[tipIndex].betDetails.odd}</h5>
      </div>
      <div className="col-4 text-white text-center">
        <div style={{ fontSize: "10px" }}>MARK</div>
        <h5>{teasers.bgDataBets[tipIndex].betDetails.mark}</h5>
      </div>
      <div className="col-4 text-white text-center d-flex justify-content-end">
        <button
          type="button"
          className="btn btn-primary"
          style={{ fontSize: "15px" }}
        >
          Send
        </button>
      </div>
    </div>
  );
}

TeaserTipBetSend.propTypes = {
  teasers: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    teasers: state.teasers,
  };
}
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TeaserTipBetSend);