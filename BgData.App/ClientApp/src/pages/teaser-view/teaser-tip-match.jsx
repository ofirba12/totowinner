import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
const fallBackSrc = `./teams-image-big/missing-image.png`;

function TeaserTipMatch({ tipIndex, teasers }) {
  return (
    teasers?.bgDataBets !== undefined && (
      <div className="col-12 text-white text-center team-text-info d-flex align-items-center team-info-container">
        <div className="col-4 d-inline-block">
          <img
            src={`./teams-image-big/${teasers.bgDataBets[tipIndex].homeId}.png`}
            className="match-info-team-image"
            alt="Home Team"
            onError={(e) => (e.currentTarget.src = fallBackSrc)}
          ></img>
          <div>{teasers.bgDataBets[tipIndex].homeTeam}</div>
        </div>
        <div className="col-4 d-inline-block">
          <div className="team-score-info">
            {teasers.bgDataBets[tipIndex].score}
          </div>
          <div>{teasers.bgDataBets[tipIndex].status}</div>
        </div>
        <div className="col-4 d-inline-block team-text-info">
          <img
            src={`./teams-image-big/${teasers.bgDataBets[tipIndex].awayId}.png`}
            className="match-info-team-image"
            alt="Away Team"
            onError={(e) => (e.currentTarget.src = fallBackSrc)}
          ></img>
          <div>{teasers.bgDataBets[tipIndex].awayTeam}</div>
        </div>
      </div>
    )
  );
}

TeaserTipMatch.propTypes = {
  teasers: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    teasers: state.teasers,
  };
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TeaserTipMatch);
