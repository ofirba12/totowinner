import React from "react";
import soccerImage from "../../assets/soccer.svg";
import basketballImage from "../../assets/basketball.svg";
import starImage from "../../assets/star.svg";
import { useNavigate } from "react-router-dom";
function TeaserFooter() {
  const navigate = useNavigate();
  const OnMenuClick = (e) => {
    if (e.target.id !== "tips") navigate(`/bgdata/matches/${e.target.id}`);
  };
  return (
    <div className="text-center pb-4">
      <div className="col-12 pt-1 bg-black sport-menu d-flex justify-content-evenly">
        <div className="col-3 d-inline-block text-center sport-button">
          <img
            src={starImage}
            id="tips"
            className="sport-image"
            alt="Tips"
            onClick={OnMenuClick}
          />
          <div className="sport-title">Tips</div>
        </div>
        <div className="col-3 d-inline-block text-center sport-button">
          <img
            src={soccerImage}
            id="soccer"
            className="sport-image"
            alt="Soccer"
            onClick={OnMenuClick}
          />
          <div className="sport-title">Soccer</div>
        </div>
        <div className="col-3 d-inline-block text-center sport-button">
          <img
            src={basketballImage}
            id="basketball"
            className="sport-image"
            alt="Basketball"
            onClick={OnMenuClick}
          />
          <div className="sport-title">Basketball</div>
        </div>
      </div>
    </div>
  );
}

export default TeaserFooter;
