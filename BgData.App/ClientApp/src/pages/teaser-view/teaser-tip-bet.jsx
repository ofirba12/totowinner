import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Rating from "react-rating";
const onChangeCollapse = (referer, iconRef) => {
  if (
    referer.current?.classList.contains("bet-description-item-collapse-hide")
  ) {
    iconRef.current?.classList.remove("fa-angle-up");
    iconRef.current?.classList.add("fa-angle-down");
  } else {
    iconRef.current?.classList.remove("fa-angle-down");
    iconRef.current?.classList.add("fa-angle-up");
  }
};
function TeaserTipBet({ tipIndex, teasers }) {
  const descriptionRef = React.useRef(null); /* description node*/
  const collapserRef = React.useRef(null); /* collapser node*/

  return (
    <>
      <table className="table table-striped table-dark table-hover table-sm table-bet-tips mt-4">
        <tbody>
          <tr
            onClick={() => {
              descriptionRef.current?.classList.toggle(
                "bet-description-item-collapse-hide"
              );
              onChangeCollapse(descriptionRef, collapserRef);
            }}
          >
            <td
              className="text-white align-middle"
              style={{ padding: "0px 0.4rem", width: "8rem" }}
            >
              {teasers.bgDataBets[tipIndex].betDetails.type}
            </td>
            <td className="text-white align-middle" style={{ width: "6rem" }}>
              {teasers.bgDataBets[tipIndex].betDetails.title}
            </td>
            <td className="rating-item" style={{ width: "8rem" }}>
              <Rating
                stop={teasers.bgDataBets[tipIndex].betDetails.stars}
                emptySymbol={["fa fa-star fa-2x star-item"]}
                fullSymbol={["fa fa-star fa-2x star-item"]}
              />
            </td>
            <td
              className="bet-description-item-collapser"
              style={{ padding: "0.5rem 0rem" }}
            >
              <a>
                <i className="fas fa-angle-down" ref={collapserRef}></i>
              </a>
            </td>
          </tr>
          <tr
            id={`description-${tipIndex}`}
            ref={descriptionRef}
            className="bet-description-item-collapse-hide"
          >
            <td colSpan="4">
              {teasers.bgDataBets[tipIndex].betDetails.description.map(
                (s, index) => (
                  <div
                    key={`${teasers.bgDataBets[tipIndex].betDetails.stars}${index}`}
                  >
                    {s}
                  </div>
                )
              )}
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}

TeaserTipBet.propTypes = {
  teasers: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    teasers: state.teasers,
  };
}
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TeaserTipBet);