import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

function TopTotalBar({ teasers }) {
  return (
    teasers && <div className="card-body">
      <div className="card-body top-totals text-center m-2">
        <div className="col-4 text-white d-inline-block inline-total">
          <h3 className="fw-bold mb-3">{teasers.totalBets}</h3>
          <h6 className="fw-normal mb-0">Bets</h6>
        </div>

        <div className="col-4 text-white d-inline-block inline-total">
          <h3 className="fw-bold mb-3">{teasers.totalWins}</h3>
          <h6 className="fw-normal mb-0">Wins</h6>
        </div>

        <div className="col-4 text-white d-inline-block inline-total">
          <h3 className="fw-bold mb-3">{teasers.successPercentage}</h3>
          <h6 className="fw-normal mb-0">Success</h6>
        </div>
      </div>
    </div>
  );
}

TopTotalBar.propTypes = {
  teasers: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    teasers: state.teasers,
  };
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TopTotalBar);
