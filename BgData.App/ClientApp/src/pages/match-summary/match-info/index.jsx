import { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./match-info.css";

function MatchInfo({ matchSummary }) {
    let homeImage = `./teams-image-big/${matchSummary?.details.info.homeTeamSymbol}.png`;
    let awayImage = `./teams-image-big/${matchSummary?.details.info.awayTeamSymbol}.png`;
  // let homeImage = `./teams-image-big/teamA.png`;
  // let awayImage = `./teams-image-big/teamB.png`;
  let fallBackSrc = `./teams-image-big/missing-image.png`;
  useEffect(() => {
    console.log(
      "before:: MatchInfo useEffect is called: matchSummary=",
      matchSummary
    );
  }, []);
  if (Object.keys(matchSummary).length > 0)
    return (
      <div>
        <div className="col-12 text-white text-center team-text-info d-flex align-items-center team-info-container">
          <div className="col-3 d-inline-block">
            <img
              src={homeImage}
              className="match-info-team-image"
              alt="Home Team"
              onError={(e) => (e.currentTarget.src = fallBackSrc)}
            ></img>
            <div>{matchSummary?.details.info.homeTeam}</div>
          </div>
          <div className="col-6 d-inline-block">
            <div>{matchSummary?.details.info.gameDate}</div>
            <div className="team-score-info">
              {matchSummary?.details.info.homeScore}-
              {matchSummary?.details.info.awayScore}
            </div>
            <div>{matchSummary?.details.info.status}</div>
          </div>
          <div className="col-3 d-inline-block team-text-info">
            <img
              src={awayImage}
              className="match-info-team-image"
              alt="Away Team"
              onError={(e) => (e.currentTarget.src = fallBackSrc)}
            ></img>
            <div>{matchSummary?.details.info.awayTeam}</div>
          </div>
        </div>
      </div>
    );
}

MatchInfo.propTypes = {
  matchSummary: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(MatchInfo);
