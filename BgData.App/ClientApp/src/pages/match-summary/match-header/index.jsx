import soccerImage from "../../../assets/soccer.svg";
import basketballImage from "../../../assets/basketball.svg";
import { MDBIcon } from "mdb-react-ui-kit";
import "./match-header.css";

function truncate(str, n){
  return (str.length > n) ? str.slice(0, n-1) + '...' : str;
};
function MatchHeader({ sport, leagueName, leagueFlag }) {
  let sportImage = soccerImage;
  let sportTitle = "";
  switch (sport) {
    case 0:
      sportImage = soccerImage;
      sportTitle = "SOCCER";
      break;
    case 1:
      sportImage = basketballImage;
      sportTitle = "BASKETBALL";
      break;
    default:
      sportImage = soccerImage;
      sportTitle = "SOCCER";
      break;
  }
  return (
    <div
      id="match-header"
      className="col-12 pt-0 bg-black text-white d-inline-block"
    >
      <img
        src={sportImage}
        id="soccer"
        className="match-header-sport col-1"
        alt="Soccer"
      />
      <div id="match-header-info" className="d-inline-block col-11">
        <div className="d-inline-block">{sportTitle}</div>
        <MDBIcon
          fas
          icon="angle-right"
          className="d-inline-block match-header-league-flag"
        />
        <div className="justify-content-center d-inline-block col-1">
          <i className={`flag flag-${leagueFlag} flag-middle`}></i>
        </div>
        <div
          className="d-inline-block col-7"
          title={leagueName}
        >
          {truncate(leagueName, 30)}
        </div>
      </div>
    </div>
  );
}

export default MatchHeader;
