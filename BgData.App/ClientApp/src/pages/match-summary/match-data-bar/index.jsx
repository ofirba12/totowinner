import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./match-data-bar.css";
import { useRef } from "react";
import { useEffect } from "react";
import {
  selectMatchSummaryTabActions,
  clickedDigestChartActions,
} from "../../../redux/actions/bgSelectedMatchSummaryTabActions";
import { BgSport } from "../../../interfaces/BgEnums";

function MatchDataBar({
  matchSummary,
  selectMatchSummaryTabActions,
  selectedSummaryMatchTab,
  clickedDigestChartActions,
  digestChartClicked,
}) {
  let buttons = [
    [0, "summary-id", "SUMMARY", useRef(null), useRef(null)],
    [1, "lastgames-id", "LAST GAMES", useRef(null), useRef(null)],
    [2, "lastgameshth-id", "LAST GAMES HTH", useRef(null), useRef(null)],
    [3, "games-scores-id", "GAMES SCORES", useRef(null), useRef(null)],
    [4, "hth-scores-id", "H2H SCORES", useRef(null), useRef(null)],
    [5, "bettips-id", "BET TIPS", useRef(null), useRef(null)],
  ];
  if (matchSummary.details.info.sport === BgSport.Basketball)
    buttons.splice(-3);

  useEffect(() => {
    console.log("before:: MatchDataBar useEffect is called", matchSummary);
    buttons[1][4].current?.classList.remove("bg-dark");
    buttons[1][4].current?.classList.add("match-bar-active-menu-item");
    buttons[1][3].current?.classList.add("match-bar-active-data-title");
    selectMatchSummaryTabActions(1);
  }, [matchSummary.details.info.matchId]);
  useEffect(() => {
    console.log(
      "before:: MatchDataBar useEffect is called digestChartClicked",
      digestChartClicked
    );
    if (digestChartClicked === true) {
      var preActiveButton = buttons.filter((b) =>
        b[3].current.classList.contains("match-bar-active-data-title")
      );
      if (preActiveButton.length > 0) {
        activateToggleButtons(
          buttons[selectedSummaryMatchTab],
          preActiveButton[0]
        );
        clickedDigestChartActions(false);
        buttons[selectedSummaryMatchTab][3].current?.scrollIntoView();
      }
    }
  }, [digestChartClicked]);

  function activateToggleButtons(activeButton, preActiveButton) {
    activeButton[4].current?.classList.toggle("match-bar-active-menu-item");
    activeButton[3].current?.classList.toggle("match-bar-active-data-title");
    activeButton[4].current?.classList.remove("bg-dark");
    if (!preActiveButton[4].current?.classList.contains("bg-dark"))
      preActiveButton[4].current?.classList.add("bg-dark");
    if (
      preActiveButton[4].current?.classList.contains(
        "match-bar-active-menu-item"
      )
    ) {
      preActiveButton[4].current?.classList.toggle(
        "match-bar-active-menu-item"
      );
      preActiveButton[3].current?.classList.toggle(
        "match-bar-active-data-title"
      );
    }
  }
  const onClickBar = (e) => {
    var tabIndexSelected = parseInt(e.target.id);
    if (selectedSummaryMatchTab !== tabIndexSelected) {
      selectMatchSummaryTabActions(tabIndexSelected);
      let activeButton = buttons[e.target.id];
      let preActiveButton = buttons[selectedSummaryMatchTab];

      activateToggleButtons(activeButton, preActiveButton);
    }
  };
  return (
    <div id="match-bar-container">
      <hr id="hr-t" />
      <div className="scroll-div">
        {buttons?.map((b) => (
          <div key={b[1]} className="p-1 data-menu d-inline-block">
            <div className="d-inline-block text-center bg-black">
              <div
                id={b[0]}
                className="match-bar-data-title"
                onClick={onClickBar}
                ref={b[3]}
              >
                {b[2]}
              </div>
              <div className="match-bar-menu-item bg-dark" ref={b[4]}></div>
            </div>
          </div>
        ))}
      </div>
      <hr id="hr-b" />
    </div>
  );
}

MatchDataBar.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectMatchSummaryTabActions: PropTypes.func.isRequired,
  selectedSummaryMatchTab: PropTypes.number.isRequired,
  digestChartClicked: PropTypes.bool.isRequired,
  clickedDigestChartActions: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedSummaryMatchTab: state.selectedSummaryMatchTab,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
    digestChartClicked: state.digestChartClicked,
  };
}
const mapDispatchToProps = {
  selectMatchSummaryTabActions,
  clickedDigestChartActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MatchDataBar);
