import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./last-games-tab.css";
import MatchPopulationBar from "../../match-population-bar";
import PopulationData from "../population-data";
import TipsBar from "../../../tips-bar";
function LastGamesTab({
  matchSummary,
  tabType
}) {
  return (
    <div>
      {tabType === "gamesScores" || tabType === "gamesScoresHTH" ? (
        <TipsBar matchTabType="score-tab" />
      ) : (
        ""
      )}
      <div className="text-white team-title m-1">
        Last Matches: {matchSummary.details.info.homeTeam}
      </div>
      <MatchPopulationBar side="home" />
      <PopulationData side="home" tabType={tabType} />
      <div className="text-white team-title m-1">
        Last Matches: {matchSummary.details.info.awayTeam}
      </div>
      <MatchPopulationBar side="away" />
      <PopulationData side="away" tabType={tabType} />
    </div>
  );
}

LastGamesTab.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectedSummaryMatchTab: PropTypes.number.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedSummaryMatchTab: state.selectedSummaryMatchTab,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LastGamesTab);
