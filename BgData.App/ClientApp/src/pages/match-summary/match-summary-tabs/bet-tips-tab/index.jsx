import * as React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Rating from "react-rating";
import "./bet-tips-tab.css";
import {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  selectDigestMatchTabsActions,
  clickedDigestChartActions,
} from "../../../../redux/actions/bgSelectedMatchSummaryTabActions";
import { changeSelectedScoresTipButton } from "../../../../redux/actions/bgMenuActions";
function BetTips({
  matchSummary,
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  selectDigestMatchTabsActions,
  clickedDigestChartActions,
  selectedPopulationMatchTab,
  selectedDigestTab,
  changeSelectedScoresTipButton,
}) {
  const betStarts = [
    [
      "Full Time",
      matchSummary?.details?.tips?.fullTimeTitle,
      matchSummary?.details?.tips?.fullTimeStars,
      matchSummary?.details?.tips?.fullTimeTipDescription,
      React.useRef(null) /* description node*/,
      React.useRef(null) /* collapser node*/,
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
    [
      "Over/Under (1.5)",
      matchSummary?.details?.tips?.overUnder1_5Title,
      matchSummary?.details?.tips?.overUnder1_5Stars,
      matchSummary?.details?.tips?.overUnder1_5TipDescription,
      React.useRef(null),
      React.useRef(null),
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
    [
      "Over/Under (2.5)",
      matchSummary?.details?.tips?.overUnder2_5Title,
      matchSummary?.details?.tips?.overUnder2_5Stars,
      matchSummary?.details?.tips?.overUnder2_5TipDescription,
      React.useRef(null),
      React.useRef(null),
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
    [
      "Over/Under (3.5)",
      matchSummary?.details?.tips?.overUnder3_5Title,
      matchSummary?.details?.tips?.overUnder3_5Stars,
      matchSummary?.details?.tips?.overUnder3_5TipDescription,
      React.useRef(null),
      React.useRef(null),
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
    [
      "Bet Range",
      matchSummary?.details?.tips?.rangeTitle,
      matchSummary?.details?.tips?.rangeStars,
      matchSummary?.details?.tips?.rangeTipDescription,
      React.useRef(null),
      React.useRef(null),
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
    [
      "Both Team Score",
      getBothScoreTitle(matchSummary?.details?.tips?.bothScoreTitle),
      matchSummary?.details?.tips?.bothScoreStars,
      matchSummary?.details?.tips?.bothScoreTipDescription,
      React.useRef(null),
      React.useRef(null),
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
    [
      "Double Bet",
      getDoubleTitle(matchSummary?.details?.tips?.doubleTitle),
      matchSummary?.details?.tips?.doubleStars,
      matchSummary?.details?.tips?.doubleTipDescription,
      React.useRef(null),
      React.useRef(null),
      [
        React.useRef(null) /* show last games node*/,
        React.useRef(null) /* spinner last games node*/,
        React.useRef(null) /* Show H2H games node*/,
        React.useRef(null) /* spinner H2H games*/,
        React.useRef(null) /* Show charts node*/,
        React.useRef(null) /* spinner charts node*/,
      ],
    ],
  ];
  function getBothScoreTitle(title) {
    if (title === "Both Score") return "Yes";
    return "No";
  }
  function getDoubleTitle(title) {
    switch (title) {
      case "Double 2X":
        return "Away + Draw";
      case "Double 12":
        return "Home + Away";
      case "Double 1X":
        return "Home + Draw";
      default:
        return "Home + Draw";
    }
  }
  console.log("BetTips", betStarts);
  const onChangeCollapse = (referer, iconRef) => {
    if (
      referer.current?.classList.contains("bet-description-item-collapse-hide")
    ) {
      iconRef.current?.classList.remove("fa-angle-up");
      iconRef.current?.classList.add("fa-angle-down");
    } else {
      iconRef.current?.classList.remove("fa-angle-down");
      iconRef.current?.classList.add("fa-angle-up");
    }
  };
  const showLastGames = (betType) => {
    switch (betType) {
      case "Full Time":
      case "Double Bet":
        clickedDigestChartActions(true);
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          lastGames: {
            homeTab: 0,
            awayTab: 0,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
          },
        });
        selectMatchSummaryTabActions(1);
        break;
      case "Over/Under (1.5)":
      case "Over/Under (2.5)":
      case "Over/Under (3.5)":
      case "Bet Range":
      case "Both Team Score":
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          gamesScores: {
            homeTab: 0,
            awayTab: 0,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.gamesScores.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.gamesScores.awayTabLastIndexShown,
          },
        });
        if (betType.includes("1.5"))
          changeSelectedScoresTipButton("Over/Under 1.5");
        else if (betType.includes("2.5"))
          changeSelectedScoresTipButton("Over/Under 2.5");
        if (betType.includes("3.5"))
          changeSelectedScoresTipButton("Over/Under 3.5");
        clickedDigestChartActions(true);
        selectMatchSummaryTabActions(3);
        break;
    }
  };
  const showH2H = (betType) => {
    switch (betType) {
      case "Full Time":
      case "Double Bet":
        clickedDigestChartActions(true);
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          lastGamesH2H: {
            homeTab: 0,
            awayTab: 0,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.lastGamesH2H.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.lastGamesH2H.awayTabLastIndexShown,
          },
        });
        selectMatchSummaryTabActions(2);
        break;
      case "Over/Under (1.5)":
      case "Over/Under (2.5)":
      case "Over/Under (3.5)":
      case "Bet Range":
      case "Both Team Score":
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          gamesScoresH2H: {
            homeTab: 0,
            awayTab: 0,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.gamesScoresH2H.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.gamesScoresH2H.awayTabLastIndexShown,
          },
        });
        if (betType.includes("1.5"))
          changeSelectedScoresTipButton("Over/Under 1.5");
        else if (betType.includes("2.5"))
          changeSelectedScoresTipButton("Over/Under 2.5");
        if (betType.includes("3.5"))
          changeSelectedScoresTipButton("Over/Under 3.5");
        clickedDigestChartActions(true);
        selectMatchSummaryTabActions(4);
        break;
    }
  };
  const showCharts = (betType) => {
    clickedDigestChartActions(true);
    switch (betType) {
      case "Full Time":
      case "Double Bet":
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          summaryMomentum: {
            homeTab: 0,
            awayTab: 0,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.summaryMomentum.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.summaryMomentum.awayTabLastIndexShown,
          },
        });
        selectDigestMatchTabsActions({
          ...selectedDigestTab,
          digestTabTypeIndex: 2,
          digestTabSizeIndex: 1,
        });
        selectMatchSummaryTabActions(0);
        break;
      case "Over/Under (1.5)":
      case "Over/Under (2.5)":
      case "Over/Under (3.5)":
      case "Bet Range":
      case "Both Team Score":
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          summaryWDL: {
            homeTab: 0,
            awayTab: 0,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.summaryWDL.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.summaryWDL.awayTabLastIndexShown,
          },
        });
        selectDigestMatchTabsActions({
          ...selectedDigestTab,
          digestTabTypeIndex: 3,
          digestTabSizeIndex: 1,
        });
        selectMatchSummaryTabActions(0);
        break;
    }
  };
  function showDetails(linkRef, spinnerRef, index, btnType, betType) {
    linkRef.current?.classList.add("d-none");
    spinnerRef.current?.classList.remove("d-none");
    setTimeout(function () {
      if (btnType === "last-games") {
        showLastGames(betType);
      } else if (btnType === "h2h") {
        showH2H(betType);
      } else if (btnType === "charts") {
        showCharts(betType);
      }
    }, 1000);
  }
  if (matchSummary.details?.tips) {
    return (
      <>
        <table
          key={`${matchSummary.details?.info.matchId}`}
          className="table table-striped table-dark table-hover table-sm table-bet-tips"
        >
          <tbody>
            {betStarts.map((b, pIndex) => (
              <React.Fragment key={`${b[2]}-${pIndex}-top`}>
                <tr
                  onClick={() => {
                    b[4].current?.classList.toggle(
                      "bet-description-item-collapse-hide"
                    );
                    onChangeCollapse(b[4], b[5]);
                  }}
                >
                  <td className="text-white align-middle">{b[0]}</td>
                  <td className="text-white align-middle">{b[1]}</td>
                  <td className="rating-item">
                    <Rating
                      stop={b[2]}
                      emptySymbol={["fa fa-star fa-2x star-item"]}
                      fullSymbol={["fa fa-star fa-2x star-item"]}
                    />
                  </td>
                  <td className="bet-description-item-collapser">
                    <a>
                      <i className="fas fa-angle-down" ref={b[5]}></i>
                    </a>
                  </td>
                </tr>
                <tr
                  id={`description-${pIndex}`}
                  ref={b[4]}
                  className="bet-description-item-collapse-hide"
                >
                  <td colSpan="4">
                    {b[3].map((s, index) => (
                      <div key={`${b[2]}${index}`}>{s}</div>
                    ))}
                    <div className="text-center text-white show-more-matches">
                      <button
                        onClick={() => {
                          showDetails(
                            b[6][0],
                            b[6][1],
                            pIndex,
                            "last-games",
                            b[0]
                          );
                        }}
                        ref={b[6][0]}
                        className="bet-description-more-link text-white text-decoration-underline"
                      >
                        Show last games
                      </button>
                      <div
                        className="spinner-border text-warning bet-description-show-details-spinner d-none"
                        role="status"
                        ref={b[6][1]}
                      >
                        <span className="visually-hidden">Loading...</span>
                      </div>
                      <button
                        onClick={() => {
                          showDetails(b[6][2], b[6][3], pIndex, "h2h", b[0]);
                        }}
                        ref={b[6][2]}
                        className="bet-description-more-link text-white text-decoration-underline"
                      >
                        Show H2H games
                      </button>
                      <div
                        className="spinner-border text-warning bet-description-show-details-spinner d-none"
                        role="status"
                        ref={b[6][3]}
                      >
                        <span className="visually-hidden">Loading...</span>
                      </div>
                      <button
                        onClick={() => {
                          showDetails(b[6][4], b[6][5], pIndex, "charts", b[0]);
                        }}
                        ref={b[6][4]}
                        className="bet-description-more-link text-white text-decoration-underline"
                      >
                        Show charts
                      </button>
                      <div
                        className="spinner-border text-warning bet-description-show-details-spinner d-none"
                        role="status"
                        ref={b[6][5]}
                      >
                        <span className="visually-hidden">Loading...</span>
                      </div>
                    </div>
                  </td>
                </tr>
              </React.Fragment>
            ))}
          </tbody>
        </table>
      </>
    );
  } else {
    return (
      <>
        <div className="text-white text-center mt-3">No Bet Found</div>
      </>
    );
  }
}
BetTips.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectMatchSummaryTabActions: PropTypes.func.isRequired,
  selectPopulationMatchTabActions: PropTypes.func.isRequired,
  selectDigestMatchTabsActions: PropTypes.func.isRequired,
  clickedDigestChartActions: PropTypes.func.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectedDigestTab: PropTypes.object.isRequired,
  changeSelectedScoresTipButton: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
    selectedDigestTab: state.selectedDigestTab,
  };
}
const mapDispatchToProps = {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  selectDigestMatchTabsActions,
  clickedDigestChartActions,
  changeSelectedScoresTipButton,
};

export default connect(mapStateToProps, mapDispatchToProps)(BetTips);
