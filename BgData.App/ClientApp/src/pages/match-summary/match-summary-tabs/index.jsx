import { connect } from "react-redux";
import PropTypes from "prop-types";
import SummaryTab from "./summary-tab";
import LastGamesTab from "./last-games-tab";
import BetTips from "./bet-tips-tab";
import "./match-summary-tabs.css";

function MatchSummaryTabs({ selectedSummaryMatchTab }) {
  function renderTabs(selectedSummaryMatchTab) {
    switch (selectedSummaryMatchTab) {
      case 0:
        return <SummaryTab />;
      case 1:
        return <LastGamesTab tabType="lastGames" />;
      case 2:
        return <LastGamesTab tabType="lastGamesH2H" />;
      case 3:
        return <LastGamesTab tabType="gamesScores" />;
      case 4:
        return <LastGamesTab tabType="gamesScoresHTH" />;
      case 5:
        return <BetTips />;
      default:
        return <div className="text-white">Under Construction</div>;
    }
  }

  return <div>{renderTabs(selectedSummaryMatchTab)}</div>;
}

MatchSummaryTabs.propTypes = {
  selectedSummaryMatchTab: PropTypes.number.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    selectedSummaryMatchTab: state.selectedSummaryMatchTab,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(MatchSummaryTabs);
