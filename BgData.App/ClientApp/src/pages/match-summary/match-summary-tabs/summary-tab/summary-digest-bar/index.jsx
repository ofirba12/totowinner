import { connect } from "react-redux";
import PropTypes from "prop-types";
import { useEffect, useRef } from "react";
import "./summary-digest-bar.css";
import {selectDigestMatchTabsActions} from "../../../../../redux/actions/bgSelectedMatchSummaryTabActions";
import { BgSport } from "../../../../../interfaces/BgEnums";

function DigestBar({matchSummary, selectDigestMatchTabsActions, selectedDigestTab}){
    let buttons = [
      [0, "digest-overview-id", "OVERVIEW", useRef(null),useRef(null),"tip-button-style tip-button text-center tip-button-center-title"],
      [1, "digest-wdl-id", "W/D/L", useRef(null),useRef(null),"tip-button-style tip-button text-center"],
      [2, "digest-momentum-id", "MOMENTUM", useRef(null),useRef(null),"tip-button-style tip-button text-center"],
      [3, "digest-scores-id", "SCORES", useRef(null),useRef(null),"tip-button-style tip-button text-center"],
    ];
    if (matchSummary.details.info.sport === BgSport.Basketball)
      buttons.splice(-1);
    useEffect(() => {
      console.log("before:: DigestBar useEffect is called", matchSummary);
      buttons[selectedDigestTab.digestTabTypeIndex][4].current?.classList.remove("bg-dark");
      buttons[selectedDigestTab.digestTabTypeIndex][4].current?.classList.add("match-bar-active-menu-item");
      buttons[selectedDigestTab.digestTabTypeIndex][3].current?.classList.add("match-bar-active-data-title");
//        selectDigestMatchTabsActions({...selectedDigestTab, digestTabTypeIndex: 1});
    }, [matchSummary.details.info.matchId]);
    const onClickBar = (e) => {
        var tabIndexSelected = parseInt(e.target.id);
        selectDigestMatchTabsActions({
          ...selectedDigestTab,
          digestTabTypeIndex: tabIndexSelected,
        });
        let preActiveButton = buttons.filter((b) =>
          b[3].current.classList.contains("match-bar-active-data-title")
        )[0];
        let activeButton = buttons[e.target.id];
        preActiveButton[4].current?.classList.add("bg-dark");
        preActiveButton[4].current?.classList.toggle("match-bar-active-menu-item");
        preActiveButton[3].current?.classList.toggle("match-bar-active-data-title");
        activeButton[4].current?.classList.remove("bg-dark");
        activeButton[4].current?.classList.toggle("match-bar-active-menu-item");
        activeButton[3].current?.classList.toggle("match-bar-active-data-title");
    }
    return (
      <>
        <div className="container text-white button-container d-flex justify-content-evenly">
          {buttons?.map((b) => (
            <div key={b[1]} className="p-1 data-menu d-inline-block">
              <div className="d-inline-block text-center bg-black">
                <div
                  id={b[0]}
                  className="match-bar-data-title"
                  onClick={onClickBar}
                  ref={b[3]}
                >
                  {b[2]}
                </div>
                <div className="match-bar-menu-item bg-dark" ref={b[4]}></div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
}
DigestBar.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectDigestMatchTabsActions: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedDigestTab: state.selectedDigestTab
  };
}

const mapDispatchToProps = {
  selectDigestMatchTabsActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(DigestBar);