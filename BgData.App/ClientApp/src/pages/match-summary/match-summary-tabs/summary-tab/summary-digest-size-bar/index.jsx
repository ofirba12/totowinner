import { connect } from "react-redux";
import PropTypes from "prop-types";
import { useEffect, useRef } from "react";
import "./summary-digest-size-bar.css";
import { selectDigestMatchTabsActions } from "../../../../../redux/actions/bgSelectedMatchSummaryTabActions";

function DigestSizeBar({matchSummary, selectDigestMatchTabsActions, selectedDigestTab}){
    let buttons = [
      [0, "digest-5games-id", "5 GAMES", useRef(null),useRef(null),"tip-button-style tip-button text-center tip-button-center-title"],
      [1, "digest-10games-id", "10 GAMES", useRef(null),useRef(null),"tip-button-style tip-button text-center"],
      [2, "digest-15games-id", "15 GAMES", useRef(null),useRef(null),"tip-button-style tip-button text-center"],
      [3, "digest-20games-id", "20 GAMES", useRef(null),useRef(null),"tip-button-style tip-button text-center"],
    ];
      useEffect(() => {
        console.log("before:: DigestSizeBar useEffect is called", matchSummary);
        buttons[1][4].current?.classList.remove("bg-dark");
        buttons[1][4].current?.classList.add("match-bar-active-menu-item");
        buttons[1][3].current?.classList.add("match-bar-active-data-title");
        selectDigestMatchTabsActions({
          ...selectedDigestTab,
          digestTabSizeIndex: 1,
        });
      }, [matchSummary.details.info.matchId]);
    const onClickBar = (e) => {
        var tabIndexSelected = parseInt(e.target.id);
        selectDigestMatchTabsActions({
          ...selectedDigestTab,
          digestTabSizeIndex: tabIndexSelected,
        });
        let preActiveButton = buttons.filter((b) =>
          b[3].current.classList.contains("match-bar-active-data-title")
        )[0];
        let activeButton = buttons[e.target.id];
        preActiveButton[4].current?.classList.add("bg-dark");
        preActiveButton[4].current?.classList.toggle("match-bar-active-menu-item");
        preActiveButton[3].current?.classList.toggle("match-bar-active-data-title");
        activeButton[4].current?.classList.remove("bg-dark");
        activeButton[4].current?.classList.toggle("match-bar-active-menu-item");
        activeButton[3].current?.classList.toggle("match-bar-active-data-title");
    }
    return (
      <>
        <div className="container text-white button-container d-flex justify-content-evenly">
          {buttons?.map((b) => (
            <div key={b[1]} className="p-1 data-menu d-inline-block">
              <div className="d-inline-block text-center bg-black">
                <div
                  id={b[0]}
                  className="match-bar-data-title"
                  onClick={onClickBar}
                  ref={b[3]}
                >
                  {b[2]}
                </div>
                <div className="match-bar-menu-item bg-dark" ref={b[4]}></div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
}
DigestSizeBar.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectDigestMatchTabsActions: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedDigestTab: state.selectedDigestTab,
  };
}

const mapDispatchToProps = {
  selectDigestMatchTabsActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(DigestSizeBar);