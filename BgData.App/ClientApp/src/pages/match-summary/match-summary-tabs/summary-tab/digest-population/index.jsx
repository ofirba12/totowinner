import { connect } from "react-redux";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import DigestWDL from "../digest-wdl";
import DigestScores from "../digest-scores";
import DigestMomentum from "../digest-momentum";

function DigestPopulation({
  matchSummary,
  selectedDigestTab,
  selectedPopulationMatchTab,
  side,
}) {
  const [digestData, setDigestData] = useState({});
  function getPopulationData(side, digestTabTypeIndex) {
    if (side === "home") {
      let homeTab = 0 ;
      switch (digestTabTypeIndex) {
        case 1:
          homeTab = selectedPopulationMatchTab.summaryWDL.homeTab;
          break;
        case 2:
          homeTab = selectedPopulationMatchTab.summaryMomentum.homeTab;
          break;
        case 3:
          homeTab = selectedPopulationMatchTab.summaryScores.homeTab;
          break;
          default:
            console.warn(`digestTabTypeIndex home ${digestTabTypeIndex} not supported`);
      }
      // let homeTab =
      //   digestTabTypeIndex === 1
      //     ? selectedPopulationMatchTab.summaryWDL.homeTab
      //     : selectedPopulationMatchTab.summaryScores.homeTab;
      switch (homeTab) {
        case 0:
          return matchSummary.details.summary.data.home.all;
        case 1:
          return matchSummary.details.summary.data.home.favorite;
        case 2:
          return matchSummary.details.summary.data.home.netral;
        case 3:
          return matchSummary.details.summary.data.home.underdog;
        case 4:
          return matchSummary.details.summary.data.home.home;
        case 5:
          return matchSummary.details.summary.data.home.homeFavorite;
        case 6:
          return matchSummary.details.summary.data.home.homeNetral;
        case 7:
          return matchSummary.details.summary.data.home.homeUnderdog;
        case 8:
          return matchSummary.details.summary.data.home.away;
        case 9:
          return matchSummary.details.summary.data.home.awayFavorite;
        case 10:
          return matchSummary.details.summary.data.home.awayNetral;
        case 11:
          return matchSummary.details.summary.data.home.awayUnderdog;
        default:
          console.warn(
            "not supported homeTab population=",
            selectedDigestTab.digestTabSizeIndex
          );
          break;
      }
    } else {
      let awayTab = 0;
      switch (digestTabTypeIndex) {
        case 1:
          awayTab = selectedPopulationMatchTab.summaryWDL.awayTab;
          break;
        case 2:
          awayTab = selectedPopulationMatchTab.summaryMomentum.awayTab;
          break;
        case 3:
          awayTab = selectedPopulationMatchTab.summaryScores.awayTab;
          break;
        default:
          console.warn(
            `digestTabTypeIndex away ${digestTabTypeIndex} not supported`
          );
      }
      // let awayTab =
      //   digestTabTypeIndex === 1
      //     ? selectedPopulationMatchTab.summaryWDL.awayTab
      //     : selectedPopulationMatchTab.summaryScores.awayTab;
      switch (awayTab) {
        case 0:
          return matchSummary.details.summary.data.away.all;
        case 1:
          return matchSummary.details.summary.data.away.favorite;
        case 2:
          return matchSummary.details.summary.data.away.netral;
        case 3:
          return matchSummary.details.summary.data.away.underdog;
        case 4:
          return matchSummary.details.summary.data.away.home;
        case 5:
          return matchSummary.details.summary.data.away.homeFavorite;
        case 6:
          return matchSummary.details.summary.data.away.homeNetral;
        case 7:
          return matchSummary.details.summary.data.away.homeUnderdog;
        case 8:
          return matchSummary.details.summary.data.away.away;
        case 9:
          return matchSummary.details.summary.data.away.awayFavorite;
        case 10:
          return matchSummary.details.summary.data.away.awayNetral;
        case 11:
          return matchSummary.details.summary.data.away.awayUnderdog;
        default:
          console.warn(
            "not supported awayTab population=",
            selectedDigestTab.digestTabSizeIndex
          );
          break;
      }
    }
  }
  function getDigestData(side) {
    let population = getPopulationData(
      side,
      selectedDigestTab.digestTabTypeIndex
    );
    switch (selectedDigestTab.digestTabSizeIndex) {
      case 0: //5 games
        return population.last5Games;
      case 1: //10 games
        return population.last10Games;
      case 2: //15 games
        return population.last15Games;
      case 3: //20 games
        return population.last20Games;
      default:
        console.warn(
          "not supported digestTabSizeIndex=",
          selectedDigestTab.digestTabSizeIndex
        );
        break;
    }
  }
  useEffect(() => {
    if (selectedDigestTab.digestTabTypeIndex === 0) return;
    let data = getDigestData(side);
    setDigestData(data);
  }, [selectedDigestTab, selectedPopulationMatchTab]);

  if (selectedDigestTab.digestTabTypeIndex === 1) {
    return (
      <DigestWDL
        wins={digestData.wins}
        draws={digestData.draws}
        losses={digestData.losses}
      />
    );
  } else if (selectedDigestTab.digestTabTypeIndex === 2) {
    return (
      <DigestMomentum
        side={side}
        momentum={digestData.oddsPowerDetails}
      />
    );
  } else if (selectedDigestTab.digestTabTypeIndex === 3) {
    return (
      <DigestScores
        noGoals={digestData.noGoals}
        goals1={digestData.goals1}
        goals2={digestData.goals2}
        goals3={digestData.goals3}
        goals4Plus={digestData.goals4Plus}
      />
    );
  } else return <></>;
}
DigestPopulation.propTypes = {
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectedDigestTab: PropTypes.object.isRequired,
  matchSummary: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
    selectedDigestTab: state.selectedDigestTab,
    matchSummary: state.matchSummary,
  };
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DigestPopulation);
