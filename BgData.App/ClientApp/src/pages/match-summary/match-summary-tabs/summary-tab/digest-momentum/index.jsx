import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Chart } from "react-google-charts";
import { useRef } from "react";
import {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
} from "../../../../../redux/actions/bgSelectedMatchSummaryTabActions";

function DigestMomentum({
  matchSummary,
  side,
  selectedPopulationMatchTab,
  momentum,
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
  selectedDigestTab,
}) {
  let showDetailsRef = useRef();
  let showDetailsSpinnerRef = useRef();
  function getBarColor(wdl) {
    switch (wdl) {
      case "W":
        return "green";
      case "L":
        return "red";
    }
    return "yellow";
  }
  const datam = momentum?.map((d) => [
    d.gameDate,
    d.oddsPower,
    getBarColor(d.wdl),
  ]);
  const datat = [["Element", "Odd Power", { role: "style" }]];
  const data = datat.concat(datam);
  function getPopulation() {
    if (side === "home") {
      let homeTab = selectedPopulationMatchTab.summaryMomentum.homeTab;
      switch (homeTab) {
        case 0:
          return matchSummary.details.analysis.lastGamesHome.all;
        case 1:
          return matchSummary.details.analysis.lastGamesHome.favorite;
        case 2:
          return matchSummary.details.analysis.lastGamesHome.netral;
        case 3:
          return matchSummary.details.analysis.lastGamesHome.underdog;
        case 4:
          return matchSummary.details.analysis.lastGamesHome.home;
        case 5:
          return matchSummary.details.analysis.lastGamesHome.homeFavorite;
        case 6:
          return matchSummary.details.analysis.lastGamesHome.homeNetral;
        case 7:
          return matchSummary.details.analysis.lastGamesHome.homeUnderdog;
        case 8:
          return matchSummary.details.analysis.lastGamesHome.away;
        case 9:
          return matchSummary.details.analysis.lastGamesHome.awayFavorite;
        case 10:
          return matchSummary.details.analysis.lastGamesHome.awayNetral;
        case 11:
          return matchSummary.details.analysis.lastGamesHome.awayUnderdog;
        default:
          console.warn(
            "not supported homeTab analysis.lastGamesHome population=",
            homeTab
          );
          break;
      }
    } else {
      let awayTab = selectedPopulationMatchTab.summaryMomentum.awayTab;
      switch (awayTab) {
        case 0:
          return matchSummary.details.analysis.lastGamesAway.all;
        case 1:
          return matchSummary.details.analysis.lastGamesAway.favorite;
        case 2:
          return matchSummary.details.analysis.lastGamesAway.netral;
        case 3:
          return matchSummary.details.analysis.lastGamesAway.underdog;
        case 4:
          return matchSummary.details.analysis.lastGamesAway.home;
        case 5:
          return matchSummary.details.analysis.lastGamesAway.homeFavorite;
        case 6:
          return matchSummary.details.analysis.lastGamesAway.homeNetral;
        case 7:
          return matchSummary.details.analysis.lastGamesAway.homeUnderdog;
        case 8:
          return matchSummary.details.analysis.lastGamesAway.away;
        case 9:
          return matchSummary.details.analysis.lastGamesAway.awayFavorite;
        case 10:
          return matchSummary.details.analysis.lastGamesAway.awayNetral;
        case 11:
          return matchSummary.details.analysis.lastGamesAway.awayUnderdog;
        default:
          console.warn(
            "not supported awayTab analysis.lastGamesAway population=",
            awayTab
          );
          break;
      }
    }
  }
  function getChartTitle(side) {
    let team = matchSummary.details.info.homeTeam;
    if (side === "away") team = matchSummary.details.info.awayTeam;
    let size = 5;
    switch (selectedDigestTab.digestTabSizeIndex) {
      case 0:
        size = 5;
        break;
      case 1:
        size = 10;
        break;
      case 2:
        size = 15;
        break;
      case 3:
        size = 20;
        break;
      default:
        size = 10;
        break;
    }
    let title = `Last ${size} ${team} games odds`;
    return title;
  }
  var options = {
    title: getChartTitle(side),
    titleTextStyle: {
      color: "white",
      bold: true,
    },
    // titlePosition: "out",
    backgroundColor: "",
    hAxis: {
      textStyle: { color: "white" },
    },
    vAxis: {
      textStyle: { color: "white" },
      baselineColor: "yellow",
      gridlines: {
        color: "transparent",
        count: 2,
      },
      minorGridlines: {
        color: "transparent",
        count: 2,
      },
    },
    legend: { position: "none" },
  };
  function idValidData() {
    if (momentum === undefined) return false;
    return true;
  }
  function showDetails() {
    showDetailsRef.current.classList.add("d-none");
    showDetailsSpinnerRef.current.classList.remove("d-none");
    setTimeout(function () {
      clickedDigestChartActions(true);
      selectPopulationMatchTabActions({
        ...selectedPopulationMatchTab,
        lastGames: {
          homeTab: selectedPopulationMatchTab.summaryMomentum.homeTab,
          awayTab: selectedPopulationMatchTab.summaryMomentum.awayTab,
          homeTabLastIndexShown:
            selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
          awayTabLastIndexShown:
            selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
        },
      });
      selectMatchSummaryTabActions(1);
    }, 1000);
  }
  return (
    <>
      {idValidData() ? (
        <>
          <div className="chart-container">
            <Chart chartType="ColumnChart" options={options} data={data} />
          </div>
          <div className="text-center text-white show-more-matches">
            <button
              onClick={showDetails}
              ref={showDetailsRef}
              className="show-more-link text-white text-decoration-underline"
            >
              Show details
            </button>
            <div
              className="spinner-border text-warning chart-show-details-spinner d-none"
              role="status"
              ref={showDetailsSpinnerRef}
            >
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
    </>
  );
}
DigestMomentum.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectedDigestTab: PropTypes.object.isRequired,
  selectMatchSummaryTabActions: PropTypes.func.isRequired,
  selectPopulationMatchTabActions: PropTypes.func.isRequired,
  clickedDigestChartActions: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
    selectedDigestTab: state.selectedDigestTab,
  };
}
const mapDispatchToProps = {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(DigestMomentum);
