import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./summary-tab.css";
import DigestBar from "./summary-digest-bar";
import DigestSizeBar from "./summary-digest-size-bar";
import MatchPopulationBar from "../../match-population-bar";
import DigestPopulation from "./digest-population";
import SummaryOverview from "./summary-overview";

function SummaryTab({ matchSummary, selectedDigestTab }) {
  if (Object.keys(matchSummary).length > 0)
    return (
      <>
        <DigestBar />
        <hr id="hr-digest" />
        {selectedDigestTab.digestTabTypeIndex > 0 ? (
          <>
            <DigestSizeBar />
            <hr id="hr-digest" />
            <div className="text-white team-title m-1">
              Last Matches: {matchSummary.details.info.homeTeam}
            </div>
            <MatchPopulationBar side="home" />
            <DigestPopulation side="home" />
            <div className="text-white team-title m-1">
              Last Matches: {matchSummary.details.info.awayTeam}
            </div>
            <MatchPopulationBar side="away" />
            <DigestPopulation side="away" />
          </>
        ) : (
          <>
          <SummaryOverview/>
          </>
        )}
      </>
    );
}

SummaryTab.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectedDigestTab: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedDigestTab: state.selectedDigestTab,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SummaryTab);
