import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./summary-overview.css";
function SummaryOverview({ matchSummary }) {
  function getTeamHighlight(sentence, teamName, className) {
    if (sentence === teamName) return className;
    return "";
  }
  function getStatisticTitleClass() {
    if (
      matchSummary?.details.summary.overview.homeStatisticsParagraph?.sentences
        .length > 0
    )
      return "mt-2";
    return "d-none";
  }
  return (
    <>
      <div className="container summary-tab-text text-white m-1">
        <div className="fw-bold">
          <div className="text-wrap">
            {
              matchSummary?.details.summary.overview.overviewParagraph
                .sentences[0]
            }
          </div>
          <div className="text-wrap">
            {
              matchSummary?.details.summary.overview.overviewParagraph
                .sentences[1]
            }
          </div>
          <div>
            <span className="homeTeam">
              {matchSummary?.details.info.homeTeam}
            </span>{" "}
            Vs{" "}
            <span className="awayTeam">
              {matchSummary?.details.info.awayTeam}
            </span>
          </div>
        </div>
        <div className="text-wrap mt-2">
          {matchSummary?.details.summary.overview.oddsParagraph?.sentences[0]}
        </div>
        <div className="text-wrap mt-1">
          <div>
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview.homeLastGameParagraph
                  ?.sentences[0],
                matchSummary?.details.info.homeTeam,
                "homeTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview.homeLastGameParagraph
                  ?.sentences[0]
              }
            </span>
            {
              matchSummary?.details.summary.overview.homeLastGameParagraph
                ?.sentences[1]
            }
            {
              matchSummary?.details.summary.overview.homeLastGameParagraph
                ?.sentences[2]
            }
          </div>
          <div>
            {
              matchSummary?.details.summary.overview.homeLast3GameParagraph
                ?.sentences[0]
            }
          </div>
        </div>
        <div className="text-wrap mt-2">
          <div>
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview.awayLastGameParagraph
                  ?.sentences[0],
                matchSummary?.details.info.awayTeam,
                "awayTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview.awayLastGameParagraph
                  ?.sentences[0]
              }
            </span>
            {
              matchSummary?.details.summary.overview.awayLastGameParagraph
                ?.sentences[1]
            }
            {
              matchSummary?.details.summary.overview.awayLastGameParagraph
                ?.sentences[2]
            }
          </div>
          <div>
            {
              matchSummary?.details.summary.overview.awayLast3GameParagraph
                ?.sentences[0]
            }
          </div>
        </div>
        <div className="text-wrap mt-2">
          <div>
            {
              matchSummary?.details.summary.overview.h2HGameParagraph
                ?.sentences[0]
            }
          </div>
        </div>
        <div className={getStatisticTitleClass()}>
          <mark>Last Games Statistics</mark>
        </div>
        <div className="text-wrap mt-1">
          <div>
            {
              matchSummary?.details.summary.overview.homeStatisticsParagraph
                ?.sentences[0]
            }
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview.homeStatisticsParagraph
                  ?.sentences[1],
                matchSummary?.details.info.homeTeam,
                "homeTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview.homeStatisticsParagraph
                  ?.sentences[1]
              }
            </span>
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview.homeStatisticsParagraph
                  ?.sentences[2],
                matchSummary?.details.info.homeTeam,
                "homeTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview.homeStatisticsParagraph
                  ?.sentences[2]
              }
            </span>
            {
              matchSummary?.details.summary.overview.homeStatisticsParagraph
                ?.sentences[3]
            }
          </div>
          <div>
            {
              matchSummary?.details.summary.overview
                .homePlayedAtHomeStatisticsParagraph?.sentences[0]
            }
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview
                  .homePlayedAtHomeStatisticsParagraph?.sentences[1],
                matchSummary?.details.info.homeTeam,
                "homeTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview
                  .homePlayedAtHomeStatisticsParagraph?.sentences[1]
              }
            </span>
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview
                  .homePlayedAtHomeStatisticsParagraph?.sentences[2],
                matchSummary?.details.info.homeTeam,
                "homeTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview
                  .homePlayedAtHomeStatisticsParagraph?.sentences[2]
              }
            </span>
            {
              matchSummary?.details.summary.overview
                .homePlayedAtHomeStatisticsParagraph?.sentences[3]
            }
          </div>
        </div>
        <div className="text-wrap mt-2">
          <div>
            {
              matchSummary?.details.summary.overview.awayStatisticsParagraph
                ?.sentences[0]
            }
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview.awayStatisticsParagraph
                  ?.sentences[1],
                matchSummary?.details.info.awayTeam,
                "awayTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview.awayStatisticsParagraph
                  ?.sentences[1]
              }
            </span>
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview.awayStatisticsParagraph
                  ?.sentences[2],
                matchSummary?.details.info.awayTeam,
                "awayTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview.awayStatisticsParagraph
                  ?.sentences[2]
              }
            </span>
            {
              matchSummary?.details.summary.overview.awayStatisticsParagraph
                ?.sentences[3]
            }
          </div>
          <div>
            {
              matchSummary?.details.summary.overview
                .awayPlayedAwayStatisticsParagraph?.sentences[0]
            }
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview
                  .awayPlayedAwayStatisticsParagraph?.sentences[1],
                matchSummary?.details.info.awayTeam,
                "awayTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview
                  .awayPlayedAwayStatisticsParagraph?.sentences[1]
              }
            </span>
            <span
              className={getTeamHighlight(
                matchSummary?.details.summary.overview
                  .awayPlayedAwayStatisticsParagraph?.sentences[2],
                matchSummary?.details.info.awayTeam,
                "awayTeam"
              )}
            >
              {
                matchSummary?.details.summary.overview
                  .awayPlayedAwayStatisticsParagraph?.sentences[2]
              }
            </span>
            {
              matchSummary?.details.summary.overview
                .awayPlayedAwayStatisticsParagraph?.sentences[3]
            }
          </div>
        </div>
      </div>
    </>
  );
}
SummaryOverview.propTypes = {
  matchSummary: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SummaryOverview);
