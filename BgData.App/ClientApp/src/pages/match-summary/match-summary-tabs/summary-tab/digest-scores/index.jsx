import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Pie3D } from "react-pie3d";
import "./digest-scores.css";
import { pieChart3dConfig } from "./../../../../../interfaces/defaultValues";
import { useRef } from "react";
import {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
} from "../../../../../redux/actions/bgSelectedMatchSummaryTabActions";
function DigestScores({
  noGoals,
  goals1,
  goals2,
  goals3,
  goals4Plus,
  selectedPopulationMatchTab,
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
}) {
  //https://npm.runkit.com/react-pie3d
  //https://woles.github.io/react-pie3d/
  let showDetailsRef = useRef();
  let showDetailsSpinnerRef = useRef();

  const data = [
    { value: noGoals, label: "noGoals", color: "#f19845" },
    { value: goals1, label: "goals1", color: "#e7f56c" },
    { value: goals2, label: "goals2", color: "#8ef1af" },
    { value: goals3, label: "goals3", color: "#00ff55" },
    { value: goals4Plus, label: "goals4Plus", color: "#3763df" },
  ];
  const pieConfig = { ...pieChart3dConfig, onClick: () => showDetails() };
  function idValidData() {
    if (
      noGoals === undefined ||
      goals1 === undefined ||
      goals2 === undefined ||
      goals3 === undefined ||
      goals4Plus === undefined
    )
      return false;
    return true;
  }
  function showDetails() {
    showDetailsRef.current.classList.add("d-none");
    showDetailsSpinnerRef.current.classList.remove("d-none");
    setTimeout(function () {
      clickedDigestChartActions(true);
      selectPopulationMatchTabActions({
        ...selectedPopulationMatchTab,
        lastGames: {
          homeTab: selectedPopulationMatchTab.summaryScores.homeTab,
          awayTab: selectedPopulationMatchTab.summaryScores.awayTab,
          homeTabLastIndexShown:
            selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
          awayTabLastIndexShown:
            selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
        },
      });
      selectMatchSummaryTabActions(1);
    }, 1000);
  }
  return (
    <>
      {idValidData() ? <Pie3D config={pieConfig} data={data} /> : <></>}
      <div className="scores-container justify-content-center m-2">
        <div className="text-white text-center col-12 d-flex justify-content-center">
          <div className="scores-item scores-nogoals">NoGoals</div>
          <div className="scores-item scores-goals1">1 Goal</div>
          <div className="scores-item scores-goals2">2 Goals</div>
          <div className="scores-item scores-goals3">3 Goals</div>
          <div className="scores-item scores-goals4plus">4+ Goals</div>
        </div>
        <div className="text-white text-center col-12 d-flex justify-content-center">
          <div className="scores-item">{noGoals}</div>
          <div className="scores-item">{goals1}</div>
          <div className="scores-item">{goals2}</div>
          <div className="scores-item">{goals3}</div>
          <div className="scores-item">{goals4Plus}</div>
        </div>
      </div>
      <div className="text-center text-white show-more-matches">
        <button
          onClick={showDetails}
          ref={showDetailsRef}
          className="show-more-link text-white text-decoration-underline"
        >
          Show details
        </button>
        <div
          className="spinner-border text-warning chart-show-details-spinner d-none"
          role="status"
          ref={showDetailsSpinnerRef}
        >
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    </>
  );
}

DigestScores.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectMatchSummaryTabActions: PropTypes.func.isRequired,
  selectPopulationMatchTabActions: PropTypes.func.isRequired,
  clickedDigestChartActions: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
  };
}
const mapDispatchToProps = {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(DigestScores);
