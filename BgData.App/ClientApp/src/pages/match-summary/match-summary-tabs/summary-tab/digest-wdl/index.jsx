import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Pie3D } from "react-pie3d";
import { useRef } from "react";
import "./digest-wdl.css";
import { pieChart3dConfig } from "../../../../../interfaces/defaultValues";
import {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
} from "../../../../../redux/actions/bgSelectedMatchSummaryTabActions";
function DigestWDL({
  wins,
  draws,
  losses,
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  selectedPopulationMatchTab,
  clickedDigestChartActions,
}) {
  //https://npm.runkit.com/react-pie3d
  //https://woles.github.io/react-pie3d/
  let showDetailsRef = useRef();
  let showDetailsSpinnerRef = useRef();
  const data = [
    { value: wins, label: "wins", color: "#12bf37" },
    { value: draws, label: "draws", color: "#dfc615" },
    { value: losses, label: "lost", color: "#df1531" },
  ];
  function showDetails() {
    showDetailsRef.current.classList.add("d-none");
    showDetailsSpinnerRef.current.classList.remove("d-none");
    setTimeout(function () {
      clickedDigestChartActions(true);
      selectPopulationMatchTabActions({
        ...selectedPopulationMatchTab,
        lastGames: {
          homeTab: selectedPopulationMatchTab.summaryWDL.homeTab,
          awayTab: selectedPopulationMatchTab.summaryWDL.awayTab,
          homeTabLastIndexShown:
            selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
          awayTabLastIndexShown:
            selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
        },
      });
      selectMatchSummaryTabActions(1);
    }, 1000);
  }
  const pieConfig = { ...pieChart3dConfig, onClick: () => showDetails() };
  function idValidData() {
    if (wins === undefined || draws === undefined || losses === undefined)
      return false;
    return true;
  }
  return (
    <>
      {idValidData() ? <Pie3D config={pieConfig} data={data} /> : <></>}
      <div className="digest-container justify-content-center m-2">
        <div className="text-white text-center col-12 d-flex justify-content-center">
          <div className="wdl-item wdl-win">Win</div>
          <div className="wdl-item wdl-draw">Draw</div>
          <div className="wdl-item wdl-lost">Lost</div>
        </div>
        <div className="text-white text-center col-12 d-flex justify-content-center">
          <div className="wdl-item">{wins}</div>
          <div className="wdl-item">{draws}</div>
          <div className="wdl-item">{losses}</div>
        </div>
      </div>
      <div className="text-center text-white show-more-matches">
        <button
          onClick={showDetails}
          ref={showDetailsRef}
          className="show-more-link text-white text-decoration-underline"
        >
          Show details
        </button>
        <div
          className="spinner-border text-warning chart-show-details-spinner d-none"
          role="status"
          ref={showDetailsSpinnerRef}
        >
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    </>
  );
}

DigestWDL.propTypes = {
  selectMatchSummaryTabActions: PropTypes.func.isRequired,
  selectPopulationMatchTabActions: PropTypes.func.isRequired,
  clickedDigestChartActions: PropTypes.func.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
  };
}
const mapDispatchToProps = {
  selectMatchSummaryTabActions,
  selectPopulationMatchTabActions,
  clickedDigestChartActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(DigestWDL);
//export default DigestWDL;

//import { Chart } from "react-google-charts";
// function DigestMomentum({ wins, draws, losses }) {
//   //https://npm.runkit.com/react-pie3d
//   //https://woles.github.io/react-pie3d/
//   const data = [
//     ["Language", "Speakers (in millions)"],
//     [`Wins ${wins}`, wins],
//     [`Draws ${draws}`, draws],
//     [`Lost ${losses}`, losses],
//   ];

//   const options = {
//     //title: "Indian Language Use",
//     legend: "yes",
//     pieSliceText: "label",
//     backgroundColor: "",
//     colors: ["#12bf37", "#dfc615", "#e95a3a"],
//     legend: { textStyle: { color: "white" } },
//     // slices: {
//     //   2: { offset: 0.0},
//     //   12: { offset: 0.3 },
//     //   14: { offset: 0.4 },
//     //   15: { offset: 0.5 },
//     //},
//   };
//   function idValidData() {
//     if (wins === undefined || draws === undefined || losses === undefined)
//       return false;
//     return true;
//   }
//   return (
//     <div className="bg-dark">
//       {idValidData() ? (
//         <Chart
//           chartType="PieChart"
//           data={data}
//           options={options}
//           width={"100%"}
//           height={"250px"}
//           className=""
//         />
//       ) : (
//         <></>
//       )}
//       {/* <div className="digest-container justify-content-center m-2">
//         <div className="text-white text-center col-12 d-flex justify-content-center">
//           <div className="momentum-item momentum-win">Win</div>
//           <div className="momentum-item momentum-draw">Draw</div>
//           <div className="momentum-item momentum-lost">Lost</div>
//         </div>
//         <div className="text-white text-center col-12 d-flex justify-content-center">
//           <div className="momentum-item">{wins}</div>
//           <div className="momentum-item">{draws}</div>
//           <div className="momentum-item">{losses}</div>
//         </div>
//       </div> */}
//     </div>
//   );
// }

// export default DigestMomentum;
