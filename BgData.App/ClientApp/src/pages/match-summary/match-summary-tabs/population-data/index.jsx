import { connect } from "react-redux";
import { useRef, createRef, useEffect } from "react";
import PropTypes from "prop-types";
import * as contants from "../../../../interfaces/defaultValues";
import "./population-data.css";
import { SummaryMatchTabs } from "../../../../interfaces/BgEnums";
import { selectPopulationLastIndexShownActions } from "../../../../redux/actions/bgSelectedMatchSummaryTabActions";
import ScoresWdlOddsContainer from "./scores-wdl-odds-container";
import TipResultContainer from "./tip-result-container";

function GetCollection(lastGames, populationTabIndex) {
  switch (populationTabIndex) {
    case 0:
      return lastGames.all;
    case 1:
      return lastGames.favorite;
    case 2:
      return lastGames.netral;
    case 3:
      return lastGames.underdog;
    case 4:
      return lastGames.home;
    case 5:
      return lastGames.homeFavorite;
    case 6:
      return lastGames.homeNetral;
    case 7:
      return lastGames.homeUnderdog;
    case 8:
      return lastGames.away;
    case 9:
      return lastGames.awayFavorite;
    case 10:
      return lastGames.awayNetral;
    case 11:
      return lastGames.awayUnderdog;
    default:
      return [];
  }
}
function GetPopulaion(
  matchSummary,
  selectedSummaryMatchTab,
  side,
  selectedPopulationMatchTab
) {
  switch (selectedSummaryMatchTab) {
    case SummaryMatchTabs.LastGames:
      switch (side) {
        case "home":
          return GetCollection(
            matchSummary?.details.analysis.lastGamesHome,
            selectedPopulationMatchTab.lastGames.homeTab
          );
        case "away":
          return GetCollection(
            matchSummary?.details.analysis.lastGamesAway,
            selectedPopulationMatchTab.lastGames.awayTab
          );
        default:
          return [];
      }
    case SummaryMatchTabs.GamesScores:
      switch (side) {
        case "home":
          return GetCollection(
            matchSummary?.details.analysis.lastGamesHome,
            selectedPopulationMatchTab.gamesScores.homeTab
          );
        case "away":
          return GetCollection(
            matchSummary?.details.analysis.lastGamesAway,
            selectedPopulationMatchTab.gamesScores.awayTab
          );
        default:
          return [];
      }
    case SummaryMatchTabs.LastGamesH2H:
      switch (side) {
        case "home":
          return GetCollection(
            matchSummary?.details.analysis.head2HeadGamesHome,
            selectedPopulationMatchTab.lastGamesH2H.homeTab
          );
        case "away":
          return GetCollection(
            matchSummary?.details.analysis.head2HeadGamesAway,
            selectedPopulationMatchTab.lastGamesH2H.awayTab
          );
        default:
          return [];
      }
    case SummaryMatchTabs.GamesScoresH2H:
      switch (side) {
        case "home":
          return GetCollection(
            matchSummary?.details.analysis.head2HeadGamesHome,
            selectedPopulationMatchTab.gamesScoresH2H.homeTab
          );
        case "away":
          return GetCollection(
            matchSummary?.details.analysis.head2HeadGamesAway,
            selectedPopulationMatchTab.gamesScoresH2H.awayTab
          );
        default:
          return [];
      }
    default:
      return [];
  }
}
function GetNoMatchesFoundClass(population){
  if (population.length > 0)
    return "d-none";
  else 
    return "text-white text-center no-matches-found"
}
function PopulationData({
  matchSummary,
  selectedSummaryMatchTab,
  selectedPopulationMatchTab,
  selectedScoresTipButton,
  side,
  selectPopulationLastIndexShownActions,
  tabType
}) {
  let fallBackSrc = `./teams-image/missing-image.png`;
  let population = GetPopulaion(
    matchSummary,
    selectedSummaryMatchTab,
    side,
    selectedPopulationMatchTab
  );
  let showMoreRef = useRef();
  var data = [];
  for (var i = 0; i < contants.SHOW_MORE_POPULATION_MAX_REF_OBJECTS; i++) {
    data.push({});
  }
  console.log("---->population tabType=", tabType, selectedScoresTipButton);
 // console.log("---->population selectedScoresTipButton=", selectedScoresTipButton);
  //console.log("---->population", population.length, data.length);
  const refs = useRef(data.map(() => createRef()));
  //const refs = useRef(new Array());
  //const [refs] = useHookWithRefCallback(population);
  let lastIndexShown = useRef();
  lastIndexShown.current = contants.SHOW_MORE_MATCHES_INITIAL_SIZE;

  function showMore() {
    lastIndexShown.current += contants.SHOW_MORE_MATCHES_STEP_SIZE;
    updateState(lastIndexShown.current, side);
  }
  function updateState(lastIndex, side) {
    switch (selectedSummaryMatchTab) {
      case SummaryMatchTabs.LastGames:
        selectPopulationLastIndexShownActions({
          ...selectedPopulationMatchTab,
          lastGames: {
            homeTab: selectedPopulationMatchTab.lastGames.homeTab,
            awayTab: selectedPopulationMatchTab.lastGames.awayTab,
            homeTabLastIndexShown:
              side === "home"
                ? lastIndex
                : selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
            awayTabLastIndexShown:
              side === "away"
                ? lastIndex
                : selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
          },
        });
        break;
      case SummaryMatchTabs.LastGamesH2H:
        selectPopulationLastIndexShownActions({
          ...selectedPopulationMatchTab,
          lastGamesH2H: {
            homeTab: selectedPopulationMatchTab.lastGamesH2H.homeTab,
            awayTab: selectedPopulationMatchTab.lastGamesH2H.awayTab,
            homeTabLastIndexShown:
              side === "home"
                ? lastIndex
                : selectedPopulationMatchTab.lastGamesH2H.homeTabLastIndexShown,
            awayTabLastIndexShown:
              side === "away"
                ? lastIndex
                : selectedPopulationMatchTab.lastGamesH2H.awayTabLastIndexShown,
          },
        });
        break;
      case SummaryMatchTabs.GamesScores:
        selectPopulationLastIndexShownActions({
          ...selectedPopulationMatchTab,
          gamesScores: {
            homeTab: selectedPopulationMatchTab.gamesScores.homeTab,
            awayTab: selectedPopulationMatchTab.gamesScores.awayTab,
            homeTabLastIndexShown:
              side === "home"
                ? lastIndex
                : selectedPopulationMatchTab.gamesScores.homeTabLastIndexShown,
            awayTabLastIndexShown:
              side === "away"
                ? lastIndex
                : selectedPopulationMatchTab.gamesScores.awayTabLastIndexShown,
          },
        });
        break;
      case SummaryMatchTabs.GamesScoresH2H:
        selectPopulationLastIndexShownActions({
          ...selectedPopulationMatchTab,
          gamesScoresH2H: {
            homeTab: selectedPopulationMatchTab.gamesScoresH2H.homeTab,
            awayTab: selectedPopulationMatchTab.gamesScoresH2H.awayTab,
            homeTabLastIndexShown:
              side === "home"
                ? lastIndex
                : selectedPopulationMatchTab.gamesScoresH2H
                    .homeTabLastIndexShown,
            awayTabLastIndexShown:
              side === "away"
                ? lastIndex
                : selectedPopulationMatchTab.gamesScoresH2H
                    .awayTabLastIndexShown,
          },
        });
        break;
      default:
        console.error("SummaryMatchTabs not supported");
        break;
    }
  }

  useEffect(() => {
    if (selectedSummaryMatchTab === SummaryMatchTabs.LastGames) {
      lastIndexShown.current =
        side === "home"
          ? selectedPopulationMatchTab.lastGames.homeTabLastIndexShown
          : selectedPopulationMatchTab.lastGames.awayTabLastIndexShown;
    } else if (selectedSummaryMatchTab === SummaryMatchTabs.LastGamesH2H) {
      lastIndexShown.current =
        side === "home"
          ? selectedPopulationMatchTab.lastGamesH2H.homeTabLastIndexShown
          : selectedPopulationMatchTab.lastGamesH2H.awayTabLastIndexShown;
    } else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScores) {
      lastIndexShown.current =
        side === "home"
          ? selectedPopulationMatchTab.gamesScores.homeTabLastIndexShown
          : selectedPopulationMatchTab.gamesScores.awayTabLastIndexShown;
    } else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScoresH2H) {
      lastIndexShown.current =
        side === "home"
          ? selectedPopulationMatchTab.gamesScoresH2H.homeTabLastIndexShown
          : selectedPopulationMatchTab.gamesScoresH2H.awayTabLastIndexShown;
    }
    try {
      //const refsElements = population.map((element) => refs.current.push(element));
      let indexShown = lastIndexShown.current;
      if (indexShown >= population.length)
        showMoreRef.current.classList.add("d-none");
      for (let i = 0; i < population.length; i++) {
        if (i >= indexShown) refs.current[i].current.classList.add("d-none");
        else refs.current[i].current.classList.remove("d-none");
      }
      if (indexShown >= population.length)
        showMoreRef.current.classList.add("d-none");
      else showMoreRef.current.classList.remove("d-none");
    } catch (err) {
      debugger;
    }
  });
  function getFlagHtml(match) {
    if (match.leagueSymbol !== "world") {
      return (
        <i
          className={`flag flag-${match?.leagueSymbol} flag-middle flag-location`}
        ></i>
      );
    } else {
      return (
        <img
          src={`./country-image/world.png`}
          className="league-image"
          alt="world"
        ></img>
      );
    }
  }
  return (
    <>
      <table
        key={`${matchSummary?.details.info.matchId}-${side}`}
        className="table table-striped table-dark table-hover table-sm table-matches-summary"
      >
        <tbody>
          {population.map((match) => (
            <tr key={`${match.matchId}`} ref={refs.current[match.index - 1]}>
              {/* <tr key={`${match.matchId}`} ref={(elm) => refs.current.push(elm)}> */}
              <td className="align-middle game-date-fixed-size text-center">
                {match.gameDate}
              </td>
              <td className="align-middle league-flag-fixed-size">
                {getFlagHtml(match)}
              </td>
              <td className="align-middle team-iconn-fixed-size">
                <div className="text-center">
                  <img
                    src={`./teams-image/${match?.homeTeamSymbol}.png`}
                    className="team-image"
                    alt="Home Team"
                    onError={(e) => (e.currentTarget.src = fallBackSrc)}
                  ></img>
                </div>
                <div className="text-center team-image-away">
                  <img
                    src={`./teams-image/${match?.awayTeamSymbol}.png`}
                    className="team-image"
                    alt="Away Team"
                    onError={(e) => (e.currentTarget.src = fallBackSrc)}
                  ></img>
                </div>
              </td>
              <td className="align-middle teams-names">
                <div>{match?.homeTeam}</div>
                <div>{match?.awayTeam}</div>
              </td>
              <td className="align-middle scores-fixed-size">
                <div className="text-center">{match?.homeScore}&nbsp;</div>
                <div className="text-center">{match?.awayScore}&nbsp;</div>
              </td>
              {tabType !== "gamesScores" && tabType !== "gamesScoresHTH" ? (
                <ScoresWdlOddsContainer
                  wdlResult={match.wdlResult}
                  homeScore={match?.homeScore}
                  awayScore={match?.awayScore}
                  homeOdd={match?.homeOdd}
                  drawOdd={match?.drawOdd}
                  awayOdd={match?.awayOdd}
                />
              ) : (
                <TipResultContainer
                  overUnder1_5={match?.betIndexes.overUnder1_5}
                  overUnder2_5={match?.betIndexes.overUnder2_5}
                  overUnder3_5={match?.betIndexes.overUnder3_5}
                  range={match?.betIndexes.range}
                  bothScore={match?.betIndexes.bothScore}
                />
              )}
            </tr>
          ))}
        </tbody>
      </table>
      <div className="text-center text-white show-more-matches">
        <button
          onClick={showMore}
          ref={showMoreRef}
          className="show-more-link text-white"
        >
          Show more matches <i className="fas fa-angle-down"></i>
        </button>
      </div>
      <div className={GetNoMatchesFoundClass(population)}>No matches found</div>
    </>
  );
}

PopulationData.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectedSummaryMatchTab: PropTypes.number.isRequired,
  selectedScoresTipButton: PropTypes.string.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectPopulationLastIndexShownActions: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedSummaryMatchTab: state.selectedSummaryMatchTab,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
    selectedScoresTipButton: state.selectedScoresTipButton
  };
}
const mapDispatchToProps = {
  selectPopulationLastIndexShownActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(PopulationData);
