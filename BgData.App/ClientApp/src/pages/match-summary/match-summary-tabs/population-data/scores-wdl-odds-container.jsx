import { connect } from "react-redux";
import PropTypes from "prop-types";
import { BgSport } from "../../../../interfaces/BgEnums";

function ScoresWdlOddsContainer({
  matchSummary,
  wdlResult,
  homeScore,
  awayScore,
  homeOdd,
  drawOdd,
  awayOdd,
}) {
  function GetWDLClassName(wdl) {
    if (wdl.includes("#Special")) return "mark-wdl-d-special";
    switch (wdl) {
      case "W":
        return "mark-wdl-w";
      case "D":
        return "mark-wdl-d";
      case "L":
        return "mark-wdl-l";
      default:
        return "";
    }
  }
  function GetHighlightOddsClassName(sport, side) {
    let wdlClass = GetWDLClassName(wdlResult);
    if (side === "draw") {
      if (sport === BgSport.Basketball) return "d-none";
      else {
        switch (wdlClass) {
          case "mark-wdl-d-special":
            return "mark-odd-d-special";
          case "mark-wdl-d":
            return "mark-odd-d";
          default:
            return "";
        }
      }
    }
    if (
      (side === "home" && homeScore > awayScore) ||
      (side === "away" && homeScore < awayScore)
    ) {
      switch (wdlClass) {
        case "mark-wdl-w":
          return "mark-odd-w";
        case "mark-wdl-l":
          return "mark-odd-l";
        default:
          return "";
      }
    }
    return "";
  }
  return (
    <>
      <td className="align-middle text-center wdl-fixed-size">
        <div className={GetWDLClassName(wdlResult)}>
          {wdlResult.includes("#Special") ? "D" : wdlResult}
        </div>
      </td>
      <td className="align-middle odds-fixed-size">
        <div
          className={`text-center ${GetHighlightOddsClassName(
            matchSummary.details.info.sport,
            "home"
          )}`}
        >
          {homeOdd === 0 ? " " : homeOdd.toFixed(2)}
        </div>
        <div
          className={`text-center ${GetHighlightOddsClassName(
            matchSummary.details.info.sport,
            "draw"
          )}`}
        >
          {drawOdd === 0 ? " " : drawOdd.toFixed(2)}
        </div>
        <div
          className={`text-center ${GetHighlightOddsClassName(
            matchSummary.details.info.sport,
            "away"
          )}`}
        >
          {awayOdd === 0 ? " " : awayOdd.toFixed(2)}
        </div>
      </td>
    </>
  );
}
ScoresWdlOddsContainer.propTypes = {
  matchSummary: PropTypes.object.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
  };
}
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScoresWdlOddsContainer);
