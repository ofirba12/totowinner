import { connect } from "react-redux";
import PropTypes from "prop-types";

function TipResultContainer({
  selectedScoresTipButton,
  overUnder1_5,
  overUnder2_5,
  overUnder3_5,
  range,
  bothScore,
}) {
  function getTipResultClass(result) {
    switch (result) {
      case "Over":
        return "over-tip-result";
      case "Under":
        return "under-tip-result";
      case "0-1":
        return "range0-1-tip-result";
      case "2-3":
        return "range2-3-tip-result";
      case "4+":
        return "range4-plus-tip-result";
      case "Yes":
        return "yes-tip-result";
      case "No":
        return "no-tip-result";
      default:
        return "";
    }
  }
  function renderSwitch() {
    switch (selectedScoresTipButton) {
      case "Over/Under 1.5":
        return (
          <>
            <td className="align-middle text-center indexes-fixed-size">
              <div className={getTipResultClass(overUnder1_5)}>
                {overUnder1_5.toUpperCase()}
              </div>
            </td>
          </>
        );
      case "Over/Under 2.5":
        return (
          <>
            <td className="align-middle text-center indexes-fixed-size">
              <div className={getTipResultClass(overUnder2_5)}>
                {overUnder2_5.toUpperCase()}
              </div>
            </td>
          </>
        );
      case "Over/Under 3.5":
        return (
          <>
            <td className="align-middle text-center indexes-fixed-size">
              <div className={getTipResultClass(overUnder3_5)}>
                {overUnder3_5.toUpperCase()}
              </div>
            </td>
          </>
        );
      case "Range":
        return (
          <>
            <td className="align-middle text-center indexes-fixed-size">
              <div className={getTipResultClass(range)}>{range}</div>
            </td>
          </>
        );
      case "Both Score":
        return (
          <>
            <td className="align-middle text-center indexes-fixed-size">
              <div className={getTipResultClass(bothScore)}>
                {bothScore.toUpperCase()}
              </div>
            </td>
          </>
        );
      default:
        return <></>;
    }
  }
  return <>{renderSwitch()}</>;
}
TipResultContainer.propTypes = {
  selectedScoresTipButton: PropTypes.string.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    selectedScoresTipButton: state.selectedScoresTipButton,
  };
}
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps)(TipResultContainer);
