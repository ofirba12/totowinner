import Header from "../../components/header/header";
import "./match-summary.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { loadBgMatchSummary } from "../../redux/actions/bgMatchSummaryActions";
import MatchHeader from "./match-header";
import MatchInfo from "./match-info";
import MatchDataBar from "./match-data-bar";
import MatchSummaryTabs from "./match-summary-tabs";
import { RotatingLines } from "react-loader-spinner";

function MatchSummary({ selectedMatch, loadBgMatchSummary, matchSummary }) {
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    if (selectedMatch?.matchId > 0) {
      loadBgMatchSummary(selectedMatch?.matchId)
        .then(() => {
          setIsLoading(false);
        })
        .catch((error) => {
          setIsLoading(false);
          alert("Loading match Summary failed" + error);
        });
    }
  }, [selectedMatch, loadBgMatchSummary]);
  function GetInfoClass() {
    if (isLoading) return "d-none";
    return "";
  }
  function GetSpinnerClass() {
    if (isLoading) return "text-center spinner";
    return "";
  }
  if (selectedMatch && Object.keys(matchSummary).length > 0) {
    return (
      <>
        <Header />
        <div className={GetSpinnerClass()}>
          <RotatingLines
            strokeColor="orange"
            strokeWidth="3"
            animationDuration="1"
            width="96"
            visible={isLoading}
          />
        </div>
        <div className={GetInfoClass()}>
          <MatchHeader
            sport={matchSummary.details.info.sport}
            leagueName={matchSummary.details.info.leagueName}
            leagueFlag={matchSummary.details.info.flagSymbol}
          />
          <MatchInfo />
          <MatchDataBar />
          <MatchSummaryTabs />
        </div>
      </>
    );
  } else
    return (
      <>
        <Header />
        <div className={GetSpinnerClass()}>
          <RotatingLines
            strokeColor="orange"
            strokeWidth="3"
            animationDuration="1"
            width="96"
            visible={isLoading}
          />
        </div>
      </>
    );
}
MatchSummary.propTypes = {
  selectedMatch: PropTypes.object.isRequired,
  matchSummary: PropTypes.object.isRequired,
  loadBgMatchSummary: PropTypes.func.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    selectedMatch: state.selectedMatch,
    matchSummary: state.matchSummary,
  };
}
const mapDispatchToProps = {
  loadBgMatchSummary,
};

export default connect(mapStateToProps, mapDispatchToProps)(MatchSummary);
