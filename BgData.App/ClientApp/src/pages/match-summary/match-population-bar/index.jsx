import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./match-population-bar.css";
import { useRef } from "react";
import { useEffect } from "react";
import { selectPopulationMatchTabActions } from "../../../redux/actions/bgSelectedMatchSummaryTabActions";
import { SummaryMatchTabs } from "../../../interfaces/BgEnums";

function MatchPopulationBar({
  selectedDigestTab,
  selectedSummaryMatchTab,
  selectPopulationMatchTabActions,
  selectedPopulationMatchTab,
  side,
}) {
  let buttons = [
    [0, "all-id", "ALL", useRef(null), useRef(null)],
    [1, "favorite-id", "FAVORITE", useRef(null), useRef(null)],
    [2, "netral-id", "NETRAL", useRef(null), useRef(null)],
    [3, "underdog-id", "UNDERDOG", useRef(null), useRef(null)],
    [4, "home-id", "HOME", useRef(null), useRef(null)],
    [5, "home-favorite-id", "HOME FAVORITE", useRef(null), useRef(null)],
    [6, "home-netral-id", "HOME NETRAL", useRef(null), useRef(null)],
    [7, "home-underdog-id", "HOME UNDERDOG", useRef(null), useRef(null)],
    [8, "away-id", "AWAY", useRef(null), useRef(null)],
    [9, "away-favorite-id", "AWAY FAVORITE", useRef(null), useRef(null)],
    [10, "away-netral-id", "AWAY NETRAL", useRef(null), useRef(null)],
    [11, "away-underdog-id", "AWAY UNDERDOG", useRef(null), useRef(null)],
  ];

  function updateState(homeTabValue, awayTabValue) {
    switch (selectedSummaryMatchTab) {
      case SummaryMatchTabs.Summary:
        if (selectedDigestTab.digestTabTypeIndex === 1) {//WDL
          selectPopulationMatchTabActions({
            ...selectedPopulationMatchTab,
            summaryWDL: {
              homeTab: homeTabValue,
              awayTab: awayTabValue,
              homeTabLastIndexShown:
                selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
              awayTabLastIndexShown:
                selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
            },
          });
        } else if (selectedDigestTab.digestTabTypeIndex === 2) { //Momentum
          selectPopulationMatchTabActions({
            ...selectedPopulationMatchTab,
            summaryMomentum: {
              homeTab: homeTabValue,
              awayTab: awayTabValue,
              homeTabLastIndexShown:
                selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
              awayTabLastIndexShown:
                selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
            },
          });
        } else if (selectedDigestTab.digestTabTypeIndex === 3) {//Scores          
          selectPopulationMatchTabActions({
            ...selectedPopulationMatchTab,
            summaryScores: {
              homeTab: homeTabValue,
              awayTab: awayTabValue,
              homeTabLastIndexShown:
                selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
              awayTabLastIndexShown:
                selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
            },
          });
        }
        break;
      case SummaryMatchTabs.LastGames:
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          lastGames: {
            homeTab: homeTabValue,
            awayTab: awayTabValue,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.lastGames.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.lastGames.awayTabLastIndexShown,
          },
        });
        break;
      case SummaryMatchTabs.LastGamesH2H:
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          lastGamesH2H: {
            homeTab: homeTabValue,
            awayTab: awayTabValue,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.lastGamesH2H.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.lastGamesH2H.awayTabLastIndexShown,
          },
        });
        break;
      case SummaryMatchTabs.GamesScores:
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          gamesScores: {
            homeTab: homeTabValue,
            awayTab: awayTabValue,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.gamesScores.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.gamesScores.awayTabLastIndexShown,
          },
        });
        break;
      case SummaryMatchTabs.GamesScoresH2H:
        selectPopulationMatchTabActions({
          ...selectedPopulationMatchTab,
          gamesScoresH2H: {
            homeTab: homeTabValue,
            awayTab: awayTabValue,
            homeTabLastIndexShown:
              selectedPopulationMatchTab.gamesScoresH2H.homeTabLastIndexShown,
            awayTabLastIndexShown:
              selectedPopulationMatchTab.gamesScoresH2H.awayTabLastIndexShown,
          },
        });
        break;
      default:
        console.error("SummaryMatchTabs not supported");
        break;
    }
  }

  useEffect(() => {
    console.log(
      "before:: MatchPopulationBar useEffect is called: isLastGames=",
      selectedSummaryMatchTab === SummaryMatchTabs.LastGames
    );
    let homeTabValue = 0;
    let awayTabValue = 0;
    if (selectedSummaryMatchTab === SummaryMatchTabs.LastGames) {
      homeTabValue = selectedPopulationMatchTab.lastGames.homeTab;
      awayTabValue = selectedPopulationMatchTab.lastGames.awayTab;
    }
    if (selectedSummaryMatchTab === SummaryMatchTabs.LastGamesH2H) {
      homeTabValue = selectedPopulationMatchTab.lastGamesH2H.homeTab;
      awayTabValue = selectedPopulationMatchTab.lastGamesH2H.awayTab;
    }
    if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScores) {
      homeTabValue = selectedPopulationMatchTab.gamesScores.homeTab;
      awayTabValue = selectedPopulationMatchTab.gamesScores.awayTab;
    }
    if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScoresH2H) {
      homeTabValue = selectedPopulationMatchTab.gamesScoresH2H.homeTab;
      awayTabValue = selectedPopulationMatchTab.gamesScoresH2H.awayTab;
    }
    if (
      selectedSummaryMatchTab === SummaryMatchTabs.Summary &&
      selectedDigestTab.digestTabTypeIndex === 1
    ) {
      homeTabValue = selectedPopulationMatchTab.summaryWDL.homeTab;
      awayTabValue = selectedPopulationMatchTab.summaryWDL.awayTab;
    }
    if (
      selectedSummaryMatchTab === SummaryMatchTabs.Summary &&
      selectedDigestTab.digestTabTypeIndex === 2
    ) {
      homeTabValue = selectedPopulationMatchTab.summaryMomentum.homeTab;
      awayTabValue = selectedPopulationMatchTab.summaryMomentum.awayTab;
    }
    if (
      selectedSummaryMatchTab === SummaryMatchTabs.Summary &&
      selectedDigestTab.digestTabTypeIndex === 3
    ) {
      homeTabValue = selectedPopulationMatchTab.summaryScores.homeTab;
      awayTabValue = selectedPopulationMatchTab.summaryScores.awayTab;
    }
    updateState(homeTabValue, awayTabValue, side);
    let pIndex = GetPopulationIndex(
      side,
      selectedSummaryMatchTab,
      selectedPopulationMatchTab
    );
    onInitButtonActivate(pIndex);
  }, [selectedSummaryMatchTab, selectedDigestTab]);

  function onInitButtonActivate(btnIndex) {
    buttons[btnIndex][4].current?.classList.remove("bg-dark");
    buttons[btnIndex][4].current?.classList.add(
      "match-bar-population-active-menu-item"
    );
    buttons[btnIndex][3].current?.classList.add(
      "match-bar-population-active-data-title"
    );
    onInitButtonsDeActivate(btnIndex);
  }
  function onInitButtonsDeActivate(btnIndex) {
    var nonActiveButtons = buttons.filter((i) => i[0] !== btnIndex);
    nonActiveButtons.forEach((btn) => {
      btn[4].current?.classList.remove("match-bar-population-active-menu-item");
      btn[3].current?.classList.remove(
        "match-bar-population-active-data-title"
      );
    });
  }
  function GetPopulationIndex(
    side,
    selectedSummaryMatchTab,
    selectedPopulationMatchTab
  ) {
    let pIndex = 0;
    switch (side) {
      case "home":
        if (selectedSummaryMatchTab === SummaryMatchTabs.LastGames)
          pIndex = selectedPopulationMatchTab.lastGames.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.LastGamesH2H)
          pIndex = selectedPopulationMatchTab.lastGamesH2H.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScores)
          pIndex = selectedPopulationMatchTab.gamesScores.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScoresH2H)
          pIndex = selectedPopulationMatchTab.gamesScoresH2H.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 1)
          pIndex = selectedPopulationMatchTab.summaryWDL.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 2)
          pIndex = selectedPopulationMatchTab.summaryMomentum.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 3)
          pIndex = selectedPopulationMatchTab.summaryScores.homeTab;
        break;
      default:
        if (selectedSummaryMatchTab === SummaryMatchTabs.LastGames)
          pIndex = selectedPopulationMatchTab.lastGames.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.LastGamesH2H)
          pIndex = selectedPopulationMatchTab.lastGamesH2H.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScores)
          pIndex = selectedPopulationMatchTab.gamesScores.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScoresH2H)
          pIndex = selectedPopulationMatchTab.gamesScoresH2H.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 1)
          pIndex = selectedPopulationMatchTab.summaryWDL.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 2)
          pIndex = selectedPopulationMatchTab.summaryMomentum.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 3)
          pIndex = selectedPopulationMatchTab.summaryScores.awayTab;
        break;
    }
    return pIndex;
  }
  const onClickBar = (e) => {
    // console.log(
    //   "btn pressed p",
    //   e.target.id,
    //   "side",
    //   side,
    //   "selectedPopulationMatchTab",
    //   selectedPopulationMatchTab
    // );
    var tabIndexSelected = parseInt(e.target.id);
    let pIndex = GetPopulationIndex(
      side,
      selectedSummaryMatchTab,
      selectedPopulationMatchTab
    );
    if (pIndex !== tabIndexSelected) {
      let homeTabValue = 0;
      let awayTabValue = 0;
      if (side === "home") {
        if (selectedSummaryMatchTab === SummaryMatchTabs.LastGames)
          awayTabValue = selectedPopulationMatchTab.lastGames.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.LastGamesH2H)
          awayTabValue = selectedPopulationMatchTab.lastGamesH2H.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScores)
          awayTabValue = selectedPopulationMatchTab.gamesScores.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScoresH2H)
          awayTabValue = selectedPopulationMatchTab.gamesScoresH2H.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 1)
          awayTabValue = selectedPopulationMatchTab.summaryWDL.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 2)
          awayTabValue = selectedPopulationMatchTab.summaryMomentum.awayTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 3)
          awayTabValue = selectedPopulationMatchTab.summaryScores.awayTab;
        homeTabValue = tabIndexSelected;
      }
      if (side === "away") {
        if (selectedSummaryMatchTab === SummaryMatchTabs.LastGames)
          homeTabValue = selectedPopulationMatchTab.lastGames.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.LastGamesH2H)
          homeTabValue = selectedPopulationMatchTab.lastGamesH2H.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScores)
          homeTabValue = selectedPopulationMatchTab.gamesScores.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.GamesScoresH2H)
          homeTabValue = selectedPopulationMatchTab.gamesScoresH2H.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 1)
          homeTabValue = selectedPopulationMatchTab.summaryWDL.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 2)
          homeTabValue = selectedPopulationMatchTab.summaryMomentum.homeTab;
        else if (selectedSummaryMatchTab === SummaryMatchTabs.Summary && selectedDigestTab.digestTabTypeIndex === 3)
          homeTabValue = selectedPopulationMatchTab.summaryScores.homeTab;
        awayTabValue = tabIndexSelected;
      }
      updateState(homeTabValue, awayTabValue);
      activateButton(e.target.id);
      deActivateButton(pIndex);
    }
  };
  function activateButton(btnIndex) {
    let activeButton = buttons[btnIndex];
    activeButton[4].current?.classList.toggle(
      "match-bar-population-active-menu-item"
    );
    activeButton[3].current?.classList.toggle(
      "match-bar-population-active-data-title"
    );
    activeButton[4].current?.classList.remove("bg-dark");
  }
  function deActivateButton(btnIndex) {
    let preActiveButton = buttons[btnIndex];
    if (!preActiveButton[4].current?.classList.contains("bg-dark"))
      preActiveButton[4].current?.classList.add("bg-dark");
    if (
      preActiveButton[4].current?.classList.contains(
        "match-bar-population-active-menu-item"
      )
    ) {
      preActiveButton[4].current?.classList.toggle(
        "match-bar-population-active-menu-item"
      );
      preActiveButton[3].current?.classList.toggle(
        "match-bar-population-active-data-title"
      );
    }
  }
  return (
    <div id="match-bar-population-container">
      <div className="scroll-div">
        {buttons?.map((b) => (
          <div key={b[1]} className="p-1 data-menu d-inline-block">
            <div className="d-inline-block text-center bg-black match-bar-population-btn">
              <div
                id={b[0]}
                className="match-bar-population-data-title"
                onClick={onClickBar}
                ref={b[3]}
              >
                {b[2]}
              </div>
              <div
                className="match-bar-population-menu-item bg-dark d-none"
                ref={b[4]}
              ></div>
            </div>
          </div>
        ))}
      </div>
      <hr id="hr-b" />
    </div>
  );
}

MatchPopulationBar.propTypes = {
  matchSummary: PropTypes.object.isRequired,
  selectPopulationMatchTabActions: PropTypes.func.isRequired,
  selectedPopulationMatchTab: PropTypes.object.isRequired,
  selectedSummaryMatchTab: PropTypes.number.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    matchSummary: state.matchSummary,
    selectedPopulationMatchTab: state.selectedPopulationMatchTab,
    selectedSummaryMatchTab: state.selectedSummaryMatchTab,
    selectedDigestTab: state.selectedDigestTab
  };
}
const mapDispatchToProps = {
  selectPopulationMatchTabActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MatchPopulationBar);
