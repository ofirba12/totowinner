import { connect } from "react-redux";
import PropTypes from "prop-types";
import { BgSport } from "../../interfaces/BgEnums";
import { useEffect, useRef } from "react";
import "./tips-bar.css";
import {
  changeSelectedTipsButtons,
  changeSelectedScoresTipButton,
} from "../../redux/actions/bgMenuActions";

function TipsBar({
  sportToFetch,
  dataType,
  changeSelectedTipsButtons,
  matchTabType,
  changeSelectedScoresTipButton,
  digestChartClicked,
  selectedScoresTipButton,
}) {
  const tipsBarRef = useRef(null);
  let allButtons = [
    [
      0,
      "tip-fulltime-id",
      "FullTime",
      useRef(null),
      "tip-button-style tip-button text-center tip-button-center-title",
      "",
    ],
    [
      1,
      "tip-overunder1-id",
      "Over/Under 1.5",
      useRef(null),
      "tip-button-style tip-button text-center",
      "score-tab",
    ],
    [
      2,
      "tip-overunder2-id",
      "Over/Under 2.5",
      useRef(null),
      "tip-button-style tip-button text-center",
      "score-tab",
    ],
    [
      3,
      "tip-overunder3-id",
      "Over/Under 3.5",
      useRef(null),
      "tip-button-style tip-button text-center",
      "score-tab",
    ],
    [
      4,
      "tip-range-id",
      "Range",
      useRef(null),
      "tip-button-style tip-button text-center tip-button-center-title",
      "score-tab",
    ],
    [
      5,
      "tip-bothscore-id",
      "Both Score",
      useRef(null),
      "tip-button-style tip-button text-center",
      "score-tab",
    ],
    [
      6,
      "tip-double-id",
      "Double Bet",
      useRef(null),
      "tip-button-style tip-button-last text-center",
      "",
    ],
  ];
  let buttons = getButtons();
  const onClickBar = (e) => {
    var currentActiveButtons = buttons.filter((b) =>
      b[3].current.classList.contains("tip-button-active")
    );
    if (matchTabType === "score-tab") {
      if (buttons[e.target.id][5] === "score-tab") {
        currentActiveButtons[0][3].current.classList.toggle(
          "tip-button-active"
        );
        buttons[e.target.id][3].current.classList.toggle("tip-button-active");
      }
    } else {
      if (
        currentActiveButtons.length < 4 ||
        buttons[e.target.id][3].current.classList.contains("tip-button-active")
      ) {
        buttons[e.target.id][3].current.classList.toggle("tip-button-active");
      }
    }
    updateState();
  };
  function updateState() {
    var selectedButton = buttons
      .filter((b) => b[3].current.classList.contains("tip-button-active"))
      .map((b) => b[2]);
    console.log("selected buttons", selectedButton, matchTabType);
    if (matchTabType === "score-tab")
      changeSelectedScoresTipButton(selectedButton[0]);
    else changeSelectedTipsButtons(selectedButton);
  }
  function getButtons() {
    if (matchTabType === "score-tab") {
      return allButtons.map((b) => {
        if (b[5] !== "score-tab") {
          b[4] = b[4] + " disabled";
        }
        return b;
      });
    }
    return allButtons;
  }
  useEffect(() => {
    if (matchTabType === "score-tab") {
      tipsBarRef.current?.classList.remove("d-none");
      if (selectedScoresTipButton === "Over/Under 1.5")
        buttons[1][3].current?.classList.add("tip-button-active");
      else if (selectedScoresTipButton === "Over/Under 2.5")
        buttons[2][3].current?.classList.add("tip-button-active");
      else if (selectedScoresTipButton === "Over/Under 3.5")
        buttons[3][3].current?.classList.add("tip-button-active");
      else buttons[2][3].current?.classList.add("tip-button-active");
    } else {
      if (sportToFetch === BgSport.Soccer) {
        if (dataType === "tips") {
          tipsBarRef.current?.classList.remove("d-none");
          var selectedButton = buttons.filter((b) =>
            b[3].current.classList.contains("tip-button-active")
          );
          if (selectedButton.length === 0) {
            buttons[0][3].current?.classList.add("tip-button-active");
            buttons[2][3].current?.classList.add("tip-button-active");
            buttons[4][3].current?.classList.add("tip-button-active");
            updateState();
          }
        } else if (dataType === "scores") {
          tipsBarRef.current?.classList.add("d-none");
        }
      } else {
        tipsBarRef.current?.classList.add("d-none");
      }
    }
  }, [sportToFetch, dataType, matchTabType, digestChartClicked]);
  return (
    <>
      <div
        className="container text-white d-none bg-black button-container d-flex justify-content-evenly"
        ref={tipsBarRef}
      >
        {buttons?.map((b) => (
          <div
            id={b[0]}
            key={b[1]}
            className={b[4]}
            ref={b[3]}
            onClick={onClickBar}
          >
            {b[2]}
          </div>
        ))}
      </div>
    </>
  );
}

TipsBar.propTypes = {
  dataType: PropTypes.string.isRequired,
  sportToFetch: PropTypes.number.isRequired,
  changeSelectedTipsButtons: PropTypes.func.isRequired,
  changeSelectedScoresTipButton: PropTypes.func.isRequired,
  digestChartClicked: PropTypes.bool.isRequired,
};
function mapStateToProps(state, ownProps) {
  return {
    sportToFetch: state.sportToFetch,
    dataType: state.dataType,
    selectedTipsButtons: state.selectedTipsButtons,
    selectedScoresTipButton: state.selectedScoresTipButton,
    digestChartClicked: state.digestChartClicked,
  };
}

const mapDispatchToProps = {
  changeSelectedTipsButtons,
  changeSelectedScoresTipButton,
};

export default connect(mapStateToProps, mapDispatchToProps)(TipsBar);
