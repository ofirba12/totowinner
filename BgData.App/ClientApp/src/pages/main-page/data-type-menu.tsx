import { useNavigate  } from "react-router-dom"
import { useRef } from "react";

function DataTypeMenu () {
    const scoresDivRef = useRef<HTMLDivElement>(null);
    const scoresBarRef = useRef<HTMLDivElement>(null);
    const tipsDivRef = useRef<HTMLDivElement>(null);
    const tipsBarRef = useRef<HTMLDivElement>(null);
    const onClickScores = (e: any) => {
        scoresDivRef.current?.classList.remove('bg-dark');
        scoresDivRef.current?.classList.add('bg-black');
        tipsDivRef.current?.classList.remove('bg-black');
        tipsDivRef.current?.classList.add('bg-dark');  
        scoresBarRef.current?.classList.toggle('data-active-menu-item');
        if (tipsBarRef.current?.classList.contains('data-active-menu-item'))
            tipsBarRef.current?.classList.toggle('data-active-menu-item');
        else
            tipsBarRef.current?.classList.add('data-active-menu-item');
    }
    const onClickBettingTips = (e: any) => {
        scoresDivRef.current?.classList.remove('bg-black');
        scoresDivRef.current?.classList.add('bg-dark');
        tipsDivRef.current?.classList.remove('bg-dark');
        tipsDivRef.current?.classList.add('bg-black');        
        scoresBarRef.current?.classList.toggle('data-active-menu-item');
        if (tipsBarRef.current?.classList.contains('data-active-menu-item'))
            tipsBarRef.current?.classList.toggle('data-active-menu-item');
        else
            tipsBarRef.current?.classList.add('data-active-menu-item');
    }
    return (
        <div className="col-12 pt-0 bg-black data-menu" ref={scoresDivRef}>
            <div className='col-6 d-inline-block text-center'>
                <div className='data-title' onClick={onClickScores}>SCORES</div>
                <div className="data-menu-item data-active-menu-item" ref={scoresBarRef}>&nbsp;</div>                     
             </div>
             <div className='col-6 d-inline-block text-center bg-dark' ref={tipsDivRef}>
                <div className='data-title' onClick={onClickBettingTips}>BETS TIPS</div>
                <div className="data-menu-item" ref={tipsBarRef}>&nbsp;</div>                     
             </div>
        </div>
)   
}

export default DataTypeMenu;