import Header from "../../components/header/header";
import SportMenu from "./sport-menu";
import CalendarFilter from "./calendar";
import DataTypeButton from "./data-type-buttons";
import { changeDateToFetch } from "../../redux/actions/bgCalendarActions";
import { connect } from "react-redux";
import { loadBgData } from "../../redux/actions/bgDataActions";
import PropTypes from "prop-types";

function MainPage({
  dateFetchChangeCallback,
  changeDateToFetch,
}) {
  dateFetchChangeCallback = (newDate) => {
    changeDateToFetch({
      day: newDate.getDate(),
      month: newDate.getMonth() + 1,
      year: newDate.getFullYear(),
    });
  };
  return (
    <>
      <Header />
      <SportMenu />
      <div className="container pt-1 pb-1">
        <table className="calendar-container-tbl">
          <tbody>
            <tr>
              <td rowSpan={2}>
                <CalendarFilter
                  dateFetchChangeCallback={dateFetchChangeCallback}
                />
              </td>
              <td className="data-td-buttons">
                <DataTypeButton />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
}
MainPage.propTypes = {
  dateToFetch: PropTypes.object,
  sportToFetch: PropTypes.number,
  bgData: PropTypes.object,
};
const mapDispatchToProps = {
  loadBgData,
  changeDateToFetch,
};
function mapStateToProps(state, ownProps) {
  return {
    dateToFetch: state.dateToFetch,
    sportToFetch: state.sportToFetch,
    bgData: state.bgData,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
