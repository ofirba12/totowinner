import ChooseDate from "../../components/choose-date/choose-date";

function CalendarFilter({ dateFetchChangeCallback }: any) {
  return (
    <div className="col-12 text-center">
      <ChooseDate dateFetchChangeCallback={dateFetchChangeCallback} />
    </div>
  );
}

export default CalendarFilter;
