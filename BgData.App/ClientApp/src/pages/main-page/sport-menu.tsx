import { useNavigate } from "react-router-dom";
import soccerImage from "../../assets/soccer.svg";
import basketballImage from "../../assets/basketball.svg";
import { useRef } from "react";
import { connect } from "react-redux";
import { changeSportToFetch } from "../../redux/actions/bgMenuActions";
import PropTypes from "prop-types";
import { BgSport } from "../../interfaces/BgEnums";

function SportMenu({ changeSportToFetch }: any) {
  const navigate = useNavigate();
  const soccerRef = useRef<HTMLDivElement>(null);
  const basketballRef = useRef<HTMLDivElement>(null);
  // changeSportToFetch(BgSport.Soccer);
  // navigate(`${process.env.PUBLIC_URL}/matches/soccer`);
  const onClickSport = (e: any) => {
    if (e.target.id === "soccer") {
      soccerRef.current?.classList.toggle("sport-active-menu-item");
      basketballRef.current?.classList.remove("sport-active-menu-item");
      changeSportToFetch(BgSport.Soccer);
    }
    if (e.target.id === "basketball") {
      basketballRef.current?.classList.toggle("sport-active-menu-item");
      soccerRef.current?.classList.remove("sport-active-menu-item");
      changeSportToFetch(BgSport.Basketball);
    }
    navigate(`${process.env.PUBLIC_URL}/matches/${e.target.id}`);
  };
  return (
    <div className="col-12 pt-1 bg-black sport-menu">
      <div className="col-6 d-inline-block text-center">
        <img
          src={soccerImage}
          id="soccer"
          className="sport-image"
          alt="Soccer"
          onClick={onClickSport}
        />
        <div className="sport-title">Soccer</div>
        <div className="sport-menu-item sport-active-menu-item" ref={soccerRef}>
          &nbsp;
        </div>
      </div>
      <div className="col-6 d-inline-block text-center">
        <img
          src={basketballImage}
          id="basketball"
          className="sport-image"
          alt="Basketball"
          onClick={onClickSport}
        />
        <div className="sport-title">Basketball</div>
        <div className="sport-menu-item" ref={basketballRef}>
          &nbsp;
        </div>
      </div>
    </div>
  );
}

SportMenu.propTypes = {
  changeSportToFetch: PropTypes.func.isRequired,
};
function mapStateToProps(state: any) {
  return {
    sportToFetch: state.sportToFetch,
  };
}

const mapDispatchToProps = {
  changeSportToFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(SportMenu);
