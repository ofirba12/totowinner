import { useRef, useEffect } from "react";
import { connect } from "react-redux";
import { changeDataType } from "../../redux/actions/bgMenuActions";
import PropTypes from "prop-types";
import { BgSport } from "../../interfaces/BgEnums";
//import {dateToFetch} from "../../redux/actions/bgCalendarActions";

function DataTypeButton(
  { changeDataType, sportToFetch }: any
) {
  const scoresDivRef = useRef<HTMLDivElement>(null);
  const tipsDivRef = useRef<HTMLDivElement>(null);

  const onClickScores = () => {
    scoresDivRef.current?.classList.add("active-data-button");
    tipsDivRef.current?.classList.remove("active-data-button");
    changeDataType("scores");
  };
  const onClickBettingTips = () => {
    scoresDivRef.current?.classList.remove("active-data-button");
    tipsDivRef.current?.classList.add("active-data-button");
    changeDataType("tips");
  };

  useEffect(() => {
    if (sportToFetch === BgSport.Basketball) {
      tipsDivRef.current?.classList.add("d-none");
    } else if (sportToFetch === BgSport.Soccer) {
      tipsDivRef.current?.classList.remove("d-none");
    }
  }, [sportToFetch]);

  return (
    <>
      <div
        className="text-center text-white bg-black active-data-button"
        ref={scoresDivRef}
        onClick={onClickScores}
      >
        <div>Scores</div>
      </div>
      <div
        className="text-center bg-black text-white"
        ref={tipsDivRef}
        onClick={onClickBettingTips}
      >
        <div>Tips</div>
      </div>
    </>
  );
}

DataTypeButton.propTypes = {
  dataType: PropTypes.string.isRequired,
  sportToFetch: PropTypes.number.isRequired,
  changeDataType: PropTypes.func.isRequired,
//  dateToFetch: PropTypes.object.isRequired,
};
function mapStateToProps(state: any) {
  return {
    dataType: state.dataType,
    sportToFetch: state.sportToFetch,
//    dateToFetch: state.dateToFetch,
  };
}

const mapDispatchToProps = {
  changeDataType,
};

export default connect(mapStateToProps, mapDispatchToProps)(DataTypeButton);
