import { createContext } from "react";

const BgDataContext = createContext();

export default BgDataContext;