/* eslint-disable no-console */
const fs = require("fs");
const path = require("path");
const mockData = require("./mockBgData");
//const mockData = require("./mockData");

//const { courses, authors } = mockData;
const { bgData } = mockData;
//console.log("mockData", mockData);
//const data = JSON.stringify({ courses, authors });
const data = JSON.stringify({ bgData });
const filepath = path.join(__dirname, "db.json");
fs.writeFile(filepath, data, function (err) {
  err ? console.log(err) : console.log("Mock DB created.");
});
