﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TotoWinner.Common;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.Groups
{
    public class RobotRepository
    {
        private SystemParameters selectedSystem;
        public ShrinkSystemPayload ShrinkPayload { get; private set; }
        public int Barrier { get; }
        public Dictionary<int, Dictionary<int, Dictionary<int, string>>> Pivots { get; private set; } //groupId,<game,<column,result>>
        public Dictionary<int, List<List<string>>> Columns { get; private set; }
        public Dictionary<int, Dictionary<int, string>> Game1x2ColumnCandidates { get; private set; }
        public Dictionary<int, Dictionary<char, char>> Game1x2AbcMapping { get; private set; }
        public Dictionary<int, Dictionary<char, char>> GameAbc1x2Mapping { get; private set; }
        public Dictionary<int, RatesLine> GamesRate { get; private set; }
        public Dictionary<int, Dictionary<int, string>> CombinationGamesAbc { get; private set; }
        public Dictionary<int, Dictionary<int, float>> SumOfColumns { get; private set; }
        public Dictionary<int, Dictionary<int, int>> ColumnsWithLetterA { get; private set; }
        public Dictionary<int, List<int>> CandidateGroupsColumns { get; private set; }
        public List<Candidate> CandidatesColumns { get; private set; }

        public StringBuilder Dump { get; private set; }

        public class Candidate
        {
            public int GroupId { get; }
            public int ColumnId { get; }
            public int NumberOfLetterA { get; }
            public float RatesSum { get; }
            public Candidate(int groupId, int columnId, int numberOfLetterA, float ratesSum)
            {
                this.GroupId = groupId;
                this.ColumnId = columnId;
                this.NumberOfLetterA = numberOfLetterA;
                this.RatesSum = ratesSum;
            }
        }
        public RobotRepository(SystemParameters selectedSystem,
            ShrinkSystemPayload shrinkPayload,
            decimal barrier)
        {
            this.selectedSystem = selectedSystem;
            this.ShrinkPayload = shrinkPayload;
            this.Barrier = (int)barrier;
            this.Pivots = new Dictionary<int, Dictionary<int, Dictionary<int, string>>>();
            this.Columns = new Dictionary<int, List<List<string>>>();
            this.CombinationGamesAbc = new Dictionary<int, Dictionary<int, string>>();
            this.Game1x2ColumnCandidates = new Dictionary<int, Dictionary<int, string>>();
            this.SumOfColumns = new Dictionary<int, Dictionary<int, float>>();
            this.ColumnsWithLetterA = new Dictionary<int, Dictionary<int, int>>();
            this.CandidateGroupsColumns = new Dictionary<int, List<int>>();
            this.CandidatesColumns = new List<Candidate>();
            this.Dump = new StringBuilder();
            this.Dump.AppendLine($"----- Typed Information Barrier = {this.Barrier} -----");
        }
        public void Init()
        {
            this.GamesRate = this.ShrinkPayload.Definitions.Form.EventsRates.ToDictionary(x => x.position + 1, x => x);
            GenerateGameMappings();
            var groups = GroupsRepositoryServices.Instance.Groups.Where(g => g.Value.DoNotUseInRobot == false);
            foreach (var group in groups)
            {
                GeneratePivots(group);
                GenerateColumns(group);
                GenerateGame1x2ColumnCandidates(group);
                GenerateCombinationGamesAbc(group);
                GenerateColumnsCounters(group);
                GenerateCandidatesColumns(group);
            }
            //GenerateFinalCandidateList();
        }

        public List<Candidate> GetFinalCandidateList()
        {
            var finalSortedCandidates = new List<Candidate>();
            if (this.CandidatesColumns.Count == 0) //All extentions where marked as not to use in robot
            {
                this.Dump.AppendLine($"----- No Candidates were found -----");
                return finalSortedCandidates;
            }
            var candidates = this.CandidatesColumns
                .OrderByDescending(k => k.NumberOfLetterA).ToList();
            var maxNumerOfLetterA = this.CandidatesColumns.Max(i => i.NumberOfLetterA);
            for (var index = maxNumerOfLetterA; index >= 0; index--)
            {
                var candidateList = this.CandidatesColumns.Where(i => i.NumberOfLetterA == index).OrderBy(r => r.RatesSum).ToList();
                foreach (var candidate in candidateList)
                {
                    finalSortedCandidates.Add(candidate);
                }
            }
            this.Dump.AppendLine($"----- Final Candidates From All Groups -----");
            this.Dump.AppendLine($"<GroupId, ColumnId, NumberLettersOfA, RatesSum>");
            foreach (var candidate in finalSortedCandidates)
            {
                this.Dump.AppendLine($"{candidate.GroupId.ToString().PadLeft(2, '0')}, {candidate.ColumnId.ToString().PadLeft(2, '0')}, {candidate.NumberOfLetterA.ToString().PadLeft(2, '0')}, {candidate.RatesSum}");
            }

            return finalSortedCandidates;
        }

        private void GenerateCandidatesColumns(KeyValuePair<int, Group> group)
        {
            var maxNumerOfLetterA = this.ColumnsWithLetterA[group.Key].Values.Max();
            var candidateColumns = new List<int>();
            for (var index = maxNumerOfLetterA; index >= 0;)
            {
                var candidates = this.ColumnsWithLetterA[group.Key].Where(i => i.Value == index && !candidateColumns.Contains(i.Key)).ToList();
                if (candidates.Count() == 0)
                {
                    index--;
                    candidates = this.ColumnsWithLetterA[group.Key].Where(i => i.Value == index && !candidateColumns.Contains(i.Key)).ToList();
                }
                var lowestRateSum = float.MaxValue;
                var chosenColumn = -1;
                foreach (var candidate in candidates)
                {
                    var columnId = candidate.Key;
                    var rate = this.SumOfColumns[group.Key][columnId];
                    if (rate < lowestRateSum)
                    {
                        lowestRateSum = rate;
                        chosenColumn = columnId;
                    }
                }
                if (chosenColumn != -1)
                {
                    candidateColumns.Add(chosenColumn);
                    this.CandidatesColumns.Add(new Candidate(group.Key, chosenColumn, index, lowestRateSum));
                }
            }
            this.CandidateGroupsColumns.Add(group.Key, candidateColumns);
            this.Dump.AppendLine($"----- Candidates Columns Of Group#{group.Key} -----");
            foreach (var column in candidateColumns)
            {
                this.Dump.AppendLine($"{column.ToString().PadLeft(2, '0')}");
            }
        }

        private void GenerateColumnsCounters(KeyValuePair<int, Group> group)
        {
            var numberOfColumns = this.CombinationGamesAbc[group.Key].Values.First().Count();
            var sumOfColumns = new Dictionary<int, float>();
            var columnsWithLetterA = new Dictionary<int, int>();//<columnId, numberOfAs>
            for (var columnIndexAbc = 0; columnIndexAbc < numberOfColumns; columnIndexAbc++)
            {
                var numberOfLetterAInColumn = 0;
                foreach (var game in this.CombinationGamesAbc[group.Key].Keys)
                {
                    if (!sumOfColumns.ContainsKey(columnIndexAbc))
                        sumOfColumns.Add(columnIndexAbc, 0);
                    var rate = (float)0.0;
                    var letter = this.CombinationGamesAbc[group.Key][game][columnIndexAbc];
                    var turResult = this.GameAbc1x2Mapping[game][letter];
                    switch (turResult)
                    {
                        case '1':
                            rate = this.GamesRate[game].rate1;
                            break;
                        case 'X':
                            rate = this.GamesRate[game].rateX;
                            break;
                        case '2':
                            rate = this.GamesRate[game].rate2;
                            break;
                    }
                    sumOfColumns[columnIndexAbc] += rate;
                    numberOfLetterAInColumn += letter == 'A'
                        ? 1
                        : 0;
                }
                columnsWithLetterA.Add(columnIndexAbc, numberOfLetterAInColumn);
            }
            this.SumOfColumns.Add(group.Key, sumOfColumns);
            this.ColumnsWithLetterA.Add(group.Key, columnsWithLetterA);
            this.Dump.AppendLine($"----- Columns Counters ABC Of Group#{group.Key} -----");
            this.Dump.AppendLine($"<columnId, sumOfColumn, howManyLetterA>");
            foreach (var column in sumOfColumns.Keys)
            {
                this.Dump.AppendLine($"{column.ToString().PadLeft(2, '0')} {sumOfColumns[column]}, {columnsWithLetterA[column]}");
            }
        }

        private void GenerateCombinationGamesAbc(KeyValuePair<int, Group> group)
        {
            var combinationGamesAbc = new Dictionary<int, string>();//1<CBBBAAA>,3,<AABCABC>...
            foreach (var game in this.Game1x2ColumnCandidates[group.Key].Keys) //1,<1XXX222>, 3,<11X21X2>,...
            {
                if (!combinationGamesAbc.ContainsKey(game))
                    combinationGamesAbc.Add(game, string.Empty);

                var gameRes = this.Game1x2ColumnCandidates[group.Key][game];
                for (var turIndex = 0; turIndex < gameRes.Count(); turIndex++)
                {
                    var charKey = gameRes[turIndex];
                    combinationGamesAbc[game] = $"{combinationGamesAbc[game]}{this.Game1x2AbcMapping[game][charKey]}";
                }
            }
            this.CombinationGamesAbc.Add(group.Key, combinationGamesAbc);
            this.Dump.AppendLine($"----- Columns Candidates ABC Of Group#{group.Key} -----");
            foreach (var game in combinationGamesAbc.Keys)
            {
                this.Dump.AppendLine($"{game.ToString().PadLeft(2, '0')} {combinationGamesAbc[game]}");
            }
        }

        private void GenerateGameMappings()
        {
            this.Game1x2AbcMapping = new Dictionary<int, Dictionary<char, char>>(); //gameid, <gameResult,letter>, e.g 1,<1,A><X,B><2,C>, 2,<1,C><X,B><2,A>
            this.GameAbc1x2Mapping = new Dictionary<int, Dictionary<char, char>>(); //gameid, <letter,gameResult>, e.g 1,<A,1><B,X><C,2>, 2,<C,1><B,X><A,2>
            foreach (var game in this.GamesRate.Keys)
            {
                var currentRow = new[]
                {
                    (decimal)this.GamesRate[game].rate1,
                    (decimal)this.GamesRate[game].rateX,
                    (decimal)this.GamesRate[game].rate2,
                };
                var abc = StatisticalDataServices.Instance.GetMappedLettersRow(currentRow);
                var tur1x2Abc = new Dictionary<char, char>();
                tur1x2Abc.Add('1', abc[0]);
                tur1x2Abc.Add('X', abc[1]);
                tur1x2Abc.Add('2', abc[2]);
                var turAbc1x2 = new Dictionary<char, char>();
                turAbc1x2.Add(abc[0], '1');
                turAbc1x2.Add(abc[1], 'X');
                turAbc1x2.Add(abc[2], '2');

                this.Game1x2AbcMapping.Add(game, tur1x2Abc);
                this.GameAbc1x2Mapping.Add(game, turAbc1x2);
            }
            this.Dump.AppendLine("----- Games Information -----");
            foreach (var game in this.Game1x2AbcMapping.Keys)
            {
                var lineRates = $"{this.GamesRate[game].rate1}, {this.GamesRate[game].rateX}, {this.GamesRate[game].rate2}";
                var lineLetter = $"{this.Game1x2AbcMapping[game]['1']}, {this.Game1x2AbcMapping[game]['X']}, {this.Game1x2AbcMapping[game]['2']}";
                this.Dump.AppendLine($"{game.ToString().PadLeft(2,'0')} <{lineRates}> <{lineLetter}>");
            }
        }

        private void GeneratePivots(KeyValuePair<int, Group> group)
        {
            var pivots = GroupsRepositoryServices.Instance.PivotColumns(group.Value.GameColumns);
            this.Pivots.Add(group.Key, pivots);
            this.Dump.AppendLine($"----- Typed Information (Pivots Of Group#{group.Key}) -----");
            foreach (var game in pivots.Keys)
            {
                var line = string.Empty;
                foreach (var column in pivots[game].Keys)
                    line = $"{line}{pivots[game][column]},";
                this.Dump.AppendLine($"{game.ToString().PadLeft(2, '0')} {line}");
            }
        }
        private void GenerateColumns(KeyValuePair<int, Group> group)
        {
            var columns = new List<List<string>>();
            foreach (var column in this.Pivots[group.Key])
            {
                var tur = column.Value.Values.ToList();
                columns.Add(tur);
            }
            this.Columns.Add(group.Key, columns);
            this.Dump.AppendLine($"----- Typed Information (Columns Of Group#{group.Key}) -----");
            var line = string.Empty;
            var index = 1;
            foreach (var column in columns)
            {               
                this.Dump.AppendLine($"{index.ToString().PadLeft(2, '0')} {string.Join(",",column)}");
                index++;
            }
        }

        private void GenerateGame1x2ColumnCandidates(KeyValuePair<int, Group> group)
        {
            var typeColumnCombinations = this.Columns[group.Key].Select(i => string.Join("", i)).ToList();
            var allCombinations = Utilities.GetMathCombinationsWithRepetition<char>(new char[] { '1', 'X', '2' }, this.Columns[group.Key].First().Count());// this.Columns[group.Key].Count());
            var newColumnCombinations = allCombinations.Except(typeColumnCombinations).ToList();
            var game1x2ColumnCandidates = new Dictionary<int, string>(); //<1,"1XXX222">, <3,"11X21X2">
            var gamesIds = this.Pivots[group.Key].First().Value.Keys.ToList();
            var gameIndex = 0;
            foreach (var game in gamesIds)
            {
                foreach (var column in newColumnCombinations)
                {
                    if (!game1x2ColumnCandidates.ContainsKey(game))
                        game1x2ColumnCandidates.Add(game, column[gameIndex].ToString());
                    else
                        game1x2ColumnCandidates[game] = $"{game1x2ColumnCandidates[game]}{column[gameIndex]}";
                }
                gameIndex++;
            }
            this.Game1x2ColumnCandidates.Add(group.Key, game1x2ColumnCandidates);
            this.Dump.AppendLine($"----- Columns Candidates Of Group#{group.Key} -----");
            var line = string.Empty;
            foreach (var game in game1x2ColumnCandidates.Keys)
            {
                this.Dump.AppendLine($"{game.ToString().PadLeft(2, '0')} {game1x2ColumnCandidates[game]}");
            }
        }
    }
}
