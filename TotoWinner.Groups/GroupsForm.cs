﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.Groups
{
    public partial class GroupsForm : Form
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private int comboBoxChooseProgramSelectedIndex = 0;
        private List<SystemParameters> systemParameters = null;
        private BackgroundWorker loadSystemBackgroundWorker;
        private BackgroundWorker calculateWorker;
        private BackgroundWorker unionWorker; //OBA: TO BE REMOVED
        private BackgroundWorker pushToPricerWorker; //OBA: TO BE REMOVED
        private Runner runner;
        private ProgramType selectedProgramType;
        private SystemParameters selectedSystem;
        private bool robotFirstExecution = true;
        public GroupsForm()
        {
            loadSystemBackgroundWorker = new BackgroundWorker();
            loadSystemBackgroundWorker.DoWork += new DoWorkEventHandler(loadSystemBackgroundWorker_DoWork);
            loadSystemBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(loadSystemBackgroundWorker_RunWorkerCompleted);
            calculateWorker = new BackgroundWorker();
            calculateWorker.WorkerReportsProgress = true;
            calculateWorker.DoWork += CalculateWorker_DoWork;
            calculateWorker.ProgressChanged += CalculateWorker_ProgressChanged;
            calculateWorker.RunWorkerCompleted += CalculateWorker_RunWorkerCompleted;
            unionWorker = new BackgroundWorker();
            unionWorker.DoWork += UnionWorker_DoWork;
            unionWorker.RunWorkerCompleted += UnionWorker_RunWorkerCompleted;
            pushToPricerWorker = new BackgroundWorker();
            pushToPricerWorker.DoWork += PushToPricerWorker_DoWork;
            pushToPricerWorker.RunWorkerCompleted += PushToPricerWorker_RunWorkerCompleted;

            InitializeComponent();
            comboBoxChooseProgram.SelectedIndex = comboBoxChooseProgramSelectedIndex;
        }
        private void RefreshSystems()
        {
            if (!loadSystemBackgroundWorker.IsBusy)
            {
                this.comboBoxPrograms.Enabled = false;
                this.buttonCalculateAndPush.Enabled = false;
                this.comboBoxChooseProgram.Enabled = false;
                this.progressBarCalculate.Enabled = false;
                this.buttonRobot.Enabled = false;
                this.buttonRefresh.Text = "Wait..";
                this.buttonRefresh.Enabled = false;
                loadSystemBackgroundWorker.RunWorkerAsync(this.selectedProgramType);
            }
        }
        private string export(string filename)
        {
            var csv = new StringBuilder();
            foreach (var groupId in GroupsRepositoryServices.Instance.Groups.Keys)
            {
                csv.AppendLine($"Group {groupId}:");
                var columnKey = GroupsRepositoryServices.Instance.Groups[groupId].GameColumns.Keys.First();
                var headline = string.Join(",", GroupsRepositoryServices.Instance.Groups[groupId].GameColumns[columnKey].Keys);
                csv.AppendLine($"Game,{headline}");
                foreach (var game in GroupsRepositoryServices.Instance.Groups[groupId].GameIdIndex)
                {
                    var line = $"{game.Key}";
                    foreach (var column in GroupsRepositoryServices.Instance.Groups[groupId].GameColumns[game.Key])
                    {
                        line = $"{line},{column.Value}";
                    }
                    csv.AppendLine(line);
                }
                csv.AppendLine($"");
            }
            return csv.ToString();
        }
        private void EraseAllGroups()
        {
            this.tabControlGroups.Controls.Clear();
            GroupsRepositoryServices.Instance.Clear();
            buttonUnionGroups.Enabled = false;
        }
        private void PushToPricerWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show($"{runner.NewSystemName} Has been pushed to the pricer!");
            this.labelProgress.Visible = false;
        }
        //OBA: TO BE REMOVED
        private void PushToPricerWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            runner.PushResultsToPricer();
        }
        //OBA: TO BE REMOVED
        private void UnionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.labelProgress.Text = "Push Results To Pricer...";
            pushToPricerWorker.RunWorkerAsync();
        }
        //OBA: TO BE REMOVED
        private void UnionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            runner.UnionResults();
        }

        private void CalculateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!runner.RobotExecution)
            {
                this.progressBarCalculate.Visible = false;
                this.labelProgress.Text = "Union Results...";
                unionWorker.RunWorkerAsync();
            }
            else
            {
                if (e.Error != null)
                {
                    _log.Fatal("An unexpected error has occurred", e.Error);
                    MessageBox.Show("There was an error! " + e.Error.ToString());
                }
                else
                {
                    if (!string.IsNullOrEmpty(runner.NewSystemName))
                        MessageBox.Show($"{runner.NewSystemName} Has been pushed to the pricer!");
                    else
                        MessageBox.Show($"Nothing was pushed to the pricer, as an error has occurred!");
                }
                this.labelProgress.Visible = false;
                this.progressBarCalculate.Visible = false;
                this.buttonResetRobotData.Enabled = true;
            }
        }

        private void CalculateWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBarCalculate.Value = e.ProgressPercentage;
            this.labelProgress.Visible = true;
            this.labelProgress.Text = $"{e.ProgressPercentage}%";
        }

        private void CalculateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;
            if (selectedSystem != null)
            {
                var robotRepository = e.Argument != null
                        ? e.Argument as RobotRepository
                        : null;
                if (robotRepository == null)
                {
                    var shrinkPayload = SystemsServices.Instance.LoadSystemForOptimizationV2(selectedSystem.SystemId);
                    runner = new Runner(this.selectedSystem, shrinkPayload, robotRepository);
                }
                else
                {
                    runner = new Runner(this.selectedSystem, robotRepository.ShrinkPayload, robotRepository);
                }

                runner.ExecuteCalc(bw);
            }
        }

        private void comboBoxChooseProgram_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxChooseProgram.SelectedIndex != comboBoxChooseProgramSelectedIndex)
            {
                var message = $"Changing program will erase all groups. Would you like to continue?";
                var yesNoDialog = MessageBox.Show(message, "Erase All Groups", MessageBoxButtons.YesNo);
                if (yesNoDialog == DialogResult.No)
                {
                    comboBoxChooseProgram.SelectedIndex = comboBoxChooseProgramSelectedIndex;
                    return;
                }
                else
                {
                    EraseAllGroups();
                }
            }
            var selectedProgramType = (sender as ComboBox).SelectedItem;
            var maxNumberOfGroups = 16;
            this.selectedProgramType = ProgramType.Winner16;
            switch (selectedProgramType)
            {
                case "WINNER15":
                    maxNumberOfGroups = 15;
                    this.selectedProgramType = ProgramType.WinnerWorld;
                    break;
                case "WINNER14":
                    maxNumberOfGroups = 14;
                    this.selectedProgramType = ProgramType.WinnerHalf;
                    break;
            }
            GroupsRepositoryServices.Instance.SetMaxGroupWithOneGame(maxNumberOfGroups);
            comboBoxChooseProgramSelectedIndex = comboBoxChooseProgram.SelectedIndex;
            RefreshSystems();
        }

        private void buttonAddGroup_Click(object sender, EventArgs e)
        {
            if (this.tabControlGroups.TabPages.Count + 1 == GroupsRepositoryServices.Instance.MaxGroupsWithOneGame)
                this.buttonAddGroup.Enabled = false;

            var group = new Group(this.tabControlGroups.TabPages.Count + 1);
            this.tabControlGroups.Controls.Add(group.TabPage);
            GroupsRepositoryServices.Instance.AddGroup(group.GroupId, group);

            if (this.tabControlGroups.Visible == false)
                this.tabControlGroups.Visible = true;
            this.buttonDeleteGroup.Enabled = true;
        }
        //OBA: TO BE REMOVED
        private void buttonDumpGroups_Click(object sender, EventArgs e)
        {
            var dump = GroupsRepositoryServices.Instance.DumpGroups();
            var dumpDialog = new DumpDialog();
            dumpDialog.Owner = this;
            dumpDialog.Show("Groups Dump", dump.ToString());
        }

        private void buttonDeleteGroup_Click(object sender, EventArgs e)
        {
            var message = $"Would you like to delete {this.tabControlGroups.SelectedTab.Text}?";
            var yesNoDialog = MessageBox.Show(message, "Delete Group", MessageBoxButtons.YesNo);
            if (yesNoDialog == DialogResult.No)
                return;
            GroupsRepositoryServices.Instance.DeleteGroup(this.tabControlGroups.SelectedIndex + 1);
            this.tabControlGroups.Controls.RemoveAt(this.tabControlGroups.SelectedIndex);
            //learn();
            this.buttonDeleteGroup.Enabled = this.tabControlGroups.Controls.Count > 1;
        }
        //OBA: TO BE REMOVED
        private void buttonUnionGroups_Click(object sender, EventArgs e)
        {
            var validationMessage = GroupsRepositoryServices.Instance.ValidateGroups();
            if (!string.IsNullOrEmpty(validationMessage))
            {
                MessageBox.Show(validationMessage, "Validation Error", MessageBoxButtons.OK);
            }
            else
            {
                GroupsRepositoryServices.Instance.UnionGroups();
                var dump = GroupsRepositoryServices.Instance.DumpUnionGroups();
                var dumpDialog = new DumpDialog();
                dumpDialog.Owner = this;
                dumpDialog.Show("Union Groups Dump", dump.ToString());
            }
        }
        //OBA: TO BE REMOVED
        private void buttonDumpAll_Click(object sender, EventArgs e)
        {
            //var check = SystemsServices.Instance.GetAllSystemsForUnion(SystemType.Shrink, ProgramType.Winner16);
            var validationMessage = GroupsRepositoryServices.Instance.ValidateGroups();
            if (!string.IsNullOrEmpty(validationMessage))
            {
                MessageBox.Show(validationMessage, "Validation Error", MessageBoxButtons.OK);
            }
            else
            {
                var dump = GroupsRepositoryServices.Instance.DumpAllColumns();
                var dumpDialog = new DumpDialog();
                dumpDialog.Owner = this;
                dumpDialog.Show("All Columns Dump", dump.ToString());
            }
        }

        private void comboBoxPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedSystemItem = (sender as ComboBox).SelectedItem as string;
            this.selectedSystem = this.systemParameters.FirstOrDefault(i => i.Name == selectedSystemItem);
            //this.buttonCalculateAndPush.Enabled = true;
        }

        private void buttonCalculateAndPush_Click(object sender, EventArgs e)
        {
            this.progressBarCalculate.Visible = true;
            calculateWorker.RunWorkerAsync();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            RefreshSystems();
        }

        private void loadSystemBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.systemParameters = e.Result as List<SystemParameters>;
            var systemNames = (from s in this.systemParameters select s.Name).ToArray();
            this.comboBoxPrograms.Items.Clear();
            this.comboBoxPrograms.Items.AddRange(systemNames);
            this.comboBoxPrograms.Enabled = true;
            //this.buttonCalculateAndPush.Enabled = true;
            this.comboBoxChooseProgram.Enabled = true;
            this.progressBarCalculate.Enabled = true;
            this.buttonRobot.Enabled = true;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.Enabled = true;

        }

        private void loadSystemBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker bw = sender as BackgroundWorker;
                var programType = (ProgramType)e.Argument;
                var systems = SystemsServices.Instance.GetAllSystemsKeys(Data.SystemType.Shrink);
                e.Result = systems.Where(s => s.ProgramType == programType).ToList();
            }
            catch (Exception ex)
            {
                var message = $"Failed to retrieve shrink systems for program {(ProgramType)e.Argument}";
                _log.Fatal(message, ex);
                MessageBox.Show(message, "Failed to retrieve systems");
                throw new Exception(message, ex);
            }
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (this.saveFileDialogExport.ShowDialog() == DialogResult.OK)
            {
                var csvContect = export(this.saveFileDialogExport.FileName);
                File.WriteAllText(this.saveFileDialogExport.FileName, csvContect);
            }
        }

        private void buttonRobot_Click(object sender, EventArgs e)
        {
            if (!robotFirstExecution)
                GroupsRepositoryServices.Instance.ResetRobotData();
            var validationMessage = GroupsRepositoryServices.Instance.ValidateGroups();
            if (!string.IsNullOrEmpty(validationMessage))
            {
                MessageBox.Show(validationMessage, "Validation Error", MessageBoxButtons.OK);
            }
            else if (this.selectedSystem != null)
            {
                var shrinkPayload = SystemsServices.Instance.LoadSystemForOptimizationV2(this.selectedSystem.SystemId);
                var robot = new RobotRepository(this.selectedSystem, shrinkPayload, this.numericUpDownBarrier.Value);
                robot.Init();
                var list = robot.GetFinalCandidateList();//JUST FOR DUMP
                var dumpDialog = new DumpDialog();
                dumpDialog.Owner = this;
                dumpDialog.Show("Robot Algorithem Dump", robot.Dump.ToString());

                this.progressBarCalculate.Visible = true;
                this.tabControlGroups.Enabled = false;
                for (var index = 0; index < this.tabControlGroups.TabPages.Count; index++)
                    this.tabControlGroups.TabPages[index].BackColor = System.Drawing.Color.Red;
                this.buttonResetRobotData.Enabled = false;
                calculateWorker.RunWorkerAsync(robot);
                robotFirstExecution = false;
            }
            else
            {
                MessageBox.Show("Please select system the press Robot button again.", "No System Selected", MessageBoxButtons.OK);
            }
        }

        private void buttonResetRobotData_Click(object sender, EventArgs e)
        {
            GroupsRepositoryServices.Instance.ResetRobotData();
            this.tabControlGroups.Enabled = true;
            for (var index = 0; index < this.tabControlGroups.TabPages.Count; index++)
                this.tabControlGroups.TabPages[index].BackColor = System.Drawing.Color.White;

        }
    }
}
