﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TotoWinner.Data;
using TotoWinner.Services;

namespace TotoWinner.Groups
{
    internal class Runner
    {
        private SystemParameters selectedSystem;
        private List<IEnumerable<CombinedResult>> resultsCollection;
        private ShrinkSystemPayload ShrinkPayload;
        private ShrinkSystemPayload newPayload;
        private RobotRepository robotRepository;
        public string NewSystemName { get; private set; }
        public Runner(SystemParameters selectedSystem,
            ShrinkSystemPayload shrinkPayload,
            RobotRepository robot)
        {
            this.selectedSystem = selectedSystem;
            this.ShrinkPayload = shrinkPayload;
            this.robotRepository = robot;
        }

        public bool RobotExecution
        {
            get
            {
                return this.robotRepository != null;
            }
        }

        internal void ExecuteCalc(System.ComponentModel.BackgroundWorker bw)
        {
            if (this.RobotExecution && this.robotRepository.GetFinalCandidateList().Count > 0)
            {
                var candidates = this.robotRepository.GetFinalCandidateList();
                var stopProccessing = false;
                for (var index = 0; index < candidates.Count && !stopProccessing; index++)
                {
                    var candidate = candidates[index];
                    var games = this.robotRepository.Game1x2ColumnCandidates[candidate.GroupId];
                    foreach (var game in games.Keys) //1,<1XXX222>, 3,<11X21X2>,...
                    {
                        var gameResult = games[game][candidate.ColumnId];
                        GroupsRepositoryServices.Instance.Groups[candidate.GroupId].GameColumns.AddRobotGameResult(game, gameResult.ToString());
                    }
                    this.Calculate();
                    var union = CalculationServices.Instance.UnionResults(resultsCollection);
                    stopProccessing = union.UnionResult.Count() >= this.robotRepository.Barrier;
                    if (stopProccessing)
                    {
                        foreach (var game in games.Keys) //1,<1XXX222>, 3,<11X21X2>,...
                        {
                            GroupsRepositoryServices.Instance.Groups[candidate.GroupId].GameColumns.RemoveRobotGameResult(game);
                        }
                        break;
                    }
                    this.PrepareNewPayload(union);
                    int percentage = union.UnionResult.Count() * 100 / this.robotRepository.Barrier;
                    bw.ReportProgress(percentage);
                }
            }
            else
            {
                this.Calculate();
                var union = CalculationServices.Instance.UnionResults(resultsCollection);
                this.PrepareNewPayload(union);
            }
            bw.ReportProgress(100);
            PushResultsToPricer();
        }
        private void Calculate()
        {
            resultsCollection = new List<IEnumerable<CombinedResult>>();
            var columns = GroupsRepositoryServices.Instance.GenerateAllColumns();
            foreach (var column in columns)
            {
                var ordered = column.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
                var bets = new List<Bet>();
                foreach (var bet in ordered)
                {
                    bets.Add(new Bet(bet.Key - 1,
                        bet.Value == "1",
                        bet.Value == "X",
                        bet.Value == "2"));
                }
                var calcRequest = new BetsFormCreateRequest()
                {
                    FormId = 1,
                    FormSettings = ShrinkPayload.Definitions.Form.FormSettings,
                    EventsBets = bets,
                    EventsRates = ShrinkPayload.Definitions.Form.EventsRates,
                    CleanOnDiff = ShrinkPayload.Definitions.Form.CleanOnDiff,
                    GenerateOutputFile = false
                };
                var results = CalculationServices.Instance.CalculateSystem(calcRequest);
                if (results.FinalResults.Count() > 0)
                    resultsCollection.Add(results.FinalResults);
            }
        }
        internal void PushResultsToPricer()
        {
            SystemsServices.Instance.SaveSystem(ShrinkPayload.UniqueId, NewSystemName, newPayload);
        }

        internal void UnionResults()
        {
            var union = CalculationServices.Instance.UnionResults(resultsCollection);
            this.PrepareNewPayload(union);
        }
        private void PrepareNewPayload(UnionSystemResult union)
        {
            NewSystemName = $"{selectedSystem.Name}_{union.UnionResult.Count()}_TURIM_doNotOpenInPricer";
            newPayload = new ShrinkSystemPayload()
            {
                Name = NewSystemName,
                Definitions = ShrinkPayload.Definitions,
                Result = new SearchResults()
                {
                    FinalResults = union.UnionResult,
                    ResultsStatistics = new ResultsStatistics()
                    {
                        Found = union.UnionResult.Count()
                    }
                }
            };

        }
    }
}