﻿namespace TotoWinner.Groups
{
    partial class GroupsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxChooseProgram = new System.Windows.Forms.ComboBox();
            this.labelChooseProgram = new System.Windows.Forms.Label();
            this.buttonAddGroup = new System.Windows.Forms.Button();
            this.tabControlGroups = new System.Windows.Forms.TabControl();
            this.buttonDumpGroups = new System.Windows.Forms.Button();
            this.buttonDeleteGroup = new System.Windows.Forms.Button();
            this.buttonUnionGroups = new System.Windows.Forms.Button();
            this.buttonDumpAll = new System.Windows.Forms.Button();
            this.comboBoxPrograms = new System.Windows.Forms.ComboBox();
            this.buttonCalculateAndPush = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.progressBarCalculate = new System.Windows.Forms.ProgressBar();
            this.buttonExport = new System.Windows.Forms.Button();
            this.saveFileDialogExport = new System.Windows.Forms.SaveFileDialog();
            this.labelProgress = new System.Windows.Forms.Label();
            this.buttonRobot = new System.Windows.Forms.Button();
            this.numericUpDownBarrier = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonResetRobotData = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBarrier)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxChooseProgram
            // 
            this.comboBoxChooseProgram.FormattingEnabled = true;
            this.comboBoxChooseProgram.Items.AddRange(new object[] {
            "WINNER16",
            "WINNER15",
            "WINNER14"});
            this.comboBoxChooseProgram.Location = new System.Drawing.Point(137, 29);
            this.comboBoxChooseProgram.Name = "comboBoxChooseProgram";
            this.comboBoxChooseProgram.Size = new System.Drawing.Size(121, 21);
            this.comboBoxChooseProgram.TabIndex = 0;
            this.comboBoxChooseProgram.SelectedIndexChanged += new System.EventHandler(this.comboBoxChooseProgram_SelectedIndexChanged);
            // 
            // labelChooseProgram
            // 
            this.labelChooseProgram.AutoSize = true;
            this.labelChooseProgram.Location = new System.Drawing.Point(33, 32);
            this.labelChooseProgram.Name = "labelChooseProgram";
            this.labelChooseProgram.Size = new System.Drawing.Size(85, 13);
            this.labelChooseProgram.TabIndex = 1;
            this.labelChooseProgram.Text = "Choose Program";
            // 
            // buttonAddGroup
            // 
            this.buttonAddGroup.Location = new System.Drawing.Point(36, 56);
            this.buttonAddGroup.Name = "buttonAddGroup";
            this.buttonAddGroup.Size = new System.Drawing.Size(102, 23);
            this.buttonAddGroup.TabIndex = 4;
            this.buttonAddGroup.Text = "Add Group";
            this.buttonAddGroup.UseVisualStyleBackColor = true;
            this.buttonAddGroup.Click += new System.EventHandler(this.buttonAddGroup_Click);
            // 
            // tabControlGroups
            // 
            this.tabControlGroups.Location = new System.Drawing.Point(36, 85);
            this.tabControlGroups.Name = "tabControlGroups";
            this.tabControlGroups.SelectedIndex = 0;
            this.tabControlGroups.Size = new System.Drawing.Size(845, 297);
            this.tabControlGroups.TabIndex = 6;
            this.tabControlGroups.Visible = false;
            // 
            // buttonDumpGroups
            // 
            this.buttonDumpGroups.Location = new System.Drawing.Point(36, 484);
            this.buttonDumpGroups.Name = "buttonDumpGroups";
            this.buttonDumpGroups.Size = new System.Drawing.Size(75, 23);
            this.buttonDumpGroups.TabIndex = 7;
            this.buttonDumpGroups.Text = "Dump Groups";
            this.buttonDumpGroups.UseVisualStyleBackColor = true;
            this.buttonDumpGroups.Visible = false;
            this.buttonDumpGroups.Click += new System.EventHandler(this.buttonDumpGroups_Click);
            // 
            // buttonDeleteGroup
            // 
            this.buttonDeleteGroup.Enabled = false;
            this.buttonDeleteGroup.Location = new System.Drawing.Point(156, 56);
            this.buttonDeleteGroup.Name = "buttonDeleteGroup";
            this.buttonDeleteGroup.Size = new System.Drawing.Size(102, 23);
            this.buttonDeleteGroup.TabIndex = 8;
            this.buttonDeleteGroup.Text = "Delete Group";
            this.buttonDeleteGroup.UseVisualStyleBackColor = false;
            this.buttonDeleteGroup.Click += new System.EventHandler(this.buttonDeleteGroup_Click);
            // 
            // buttonUnionGroups
            // 
            this.buttonUnionGroups.Enabled = false;
            this.buttonUnionGroups.Location = new System.Drawing.Point(270, 484);
            this.buttonUnionGroups.Name = "buttonUnionGroups";
            this.buttonUnionGroups.Size = new System.Drawing.Size(75, 23);
            this.buttonUnionGroups.TabIndex = 9;
            this.buttonUnionGroups.Text = "Union Groups";
            this.buttonUnionGroups.UseVisualStyleBackColor = true;
            this.buttonUnionGroups.Visible = false;
            this.buttonUnionGroups.Click += new System.EventHandler(this.buttonUnionGroups_Click);
            // 
            // buttonDumpAll
            // 
            this.buttonDumpAll.Location = new System.Drawing.Point(152, 483);
            this.buttonDumpAll.Name = "buttonDumpAll";
            this.buttonDumpAll.Size = new System.Drawing.Size(75, 23);
            this.buttonDumpAll.TabIndex = 10;
            this.buttonDumpAll.Text = "Dump All";
            this.buttonDumpAll.UseVisualStyleBackColor = true;
            this.buttonDumpAll.Visible = false;
            this.buttonDumpAll.Click += new System.EventHandler(this.buttonDumpAll_Click);
            // 
            // comboBoxPrograms
            // 
            this.comboBoxPrograms.Enabled = false;
            this.comboBoxPrograms.FormattingEnabled = true;
            this.comboBoxPrograms.Location = new System.Drawing.Point(38, 389);
            this.comboBoxPrograms.Name = "comboBoxPrograms";
            this.comboBoxPrograms.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPrograms.TabIndex = 11;
            this.comboBoxPrograms.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrograms_SelectedIndexChanged);
            // 
            // buttonCalculateAndPush
            // 
            this.buttonCalculateAndPush.Enabled = false;
            this.buttonCalculateAndPush.Location = new System.Drawing.Point(369, 484);
            this.buttonCalculateAndPush.Name = "buttonCalculateAndPush";
            this.buttonCalculateAndPush.Size = new System.Drawing.Size(180, 23);
            this.buttonCalculateAndPush.TabIndex = 12;
            this.buttonCalculateAndPush.Text = "Calculate + Push Pricer";
            this.buttonCalculateAndPush.UseVisualStyleBackColor = true;
            this.buttonCalculateAndPush.Visible = false;
            this.buttonCalculateAndPush.Click += new System.EventHandler(this.buttonCalculateAndPush_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(165, 387);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(53, 23);
            this.buttonRefresh.TabIndex = 13;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // progressBarCalculate
            // 
            this.progressBarCalculate.Location = new System.Drawing.Point(278, 440);
            this.progressBarCalculate.Name = "progressBarCalculate";
            this.progressBarCalculate.Size = new System.Drawing.Size(100, 10);
            this.progressBarCalculate.TabIndex = 14;
            this.progressBarCalculate.Visible = false;
            // 
            // buttonExport
            // 
            this.buttonExport.Location = new System.Drawing.Point(805, 388);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(75, 23);
            this.buttonExport.TabIndex = 15;
            this.buttonExport.Text = "Export";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // saveFileDialogExport
            // 
            this.saveFileDialogExport.DefaultExt = "csv";
            this.saveFileDialogExport.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            this.saveFileDialogExport.Title = "Save Groups";
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(306, 453);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(72, 13);
            this.labelProgress.TabIndex = 16;
            this.labelProgress.Text = "progress label";
            this.labelProgress.Visible = false;
            // 
            // buttonRobot
            // 
            this.buttonRobot.Enabled = false;
            this.buttonRobot.Location = new System.Drawing.Point(178, 436);
            this.buttonRobot.Name = "buttonRobot";
            this.buttonRobot.Size = new System.Drawing.Size(75, 23);
            this.buttonRobot.TabIndex = 17;
            this.buttonRobot.Text = "Robot";
            this.buttonRobot.UseVisualStyleBackColor = true;
            this.buttonRobot.Click += new System.EventHandler(this.buttonRobot_Click);
            // 
            // numericUpDownBarrier
            // 
            this.numericUpDownBarrier.Location = new System.Drawing.Point(84, 437);
            this.numericUpDownBarrier.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownBarrier.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBarrier.Name = "numericUpDownBarrier";
            this.numericUpDownBarrier.Size = new System.Drawing.Size(85, 20);
            this.numericUpDownBarrier.TabIndex = 18;
            this.numericUpDownBarrier.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 441);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Barrier";
            // 
            // buttonResetRobotData
            // 
            this.buttonResetRobotData.Enabled = false;
            this.buttonResetRobotData.Location = new System.Drawing.Point(419, 389);
            this.buttonResetRobotData.Name = "buttonResetRobotData";
            this.buttonResetRobotData.Size = new System.Drawing.Size(145, 23);
            this.buttonResetRobotData.TabIndex = 21;
            this.buttonResetRobotData.Text = "Reset Robot Data";
            this.buttonResetRobotData.UseVisualStyleBackColor = true;
            this.buttonResetRobotData.Click += new System.EventHandler(this.buttonResetRobotData_Click);
            // 
            // GroupsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 520);
            this.Controls.Add(this.buttonResetRobotData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownBarrier);
            this.Controls.Add(this.buttonRobot);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.buttonExport);
            this.Controls.Add(this.progressBarCalculate);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.buttonCalculateAndPush);
            this.Controls.Add(this.comboBoxPrograms);
            this.Controls.Add(this.buttonDumpAll);
            this.Controls.Add(this.buttonUnionGroups);
            this.Controls.Add(this.buttonDeleteGroup);
            this.Controls.Add(this.buttonDumpGroups);
            this.Controls.Add(this.tabControlGroups);
            this.Controls.Add(this.buttonAddGroup);
            this.Controls.Add(this.labelChooseProgram);
            this.Controls.Add(this.comboBoxChooseProgram);
            this.Name = "GroupsForm";
            this.Text = "Groups Robot";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBarrier)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxChooseProgram;
        private System.Windows.Forms.Label labelChooseProgram;
        private System.Windows.Forms.Button buttonAddGroup;
        private System.Windows.Forms.TabControl tabControlGroups;
        private System.Windows.Forms.Button buttonDumpGroups;
        private System.Windows.Forms.Button buttonDeleteGroup;
        private System.Windows.Forms.Button buttonUnionGroups;
        private System.Windows.Forms.Button buttonDumpAll;
        private System.Windows.Forms.ComboBox comboBoxPrograms;
        private System.Windows.Forms.Button buttonCalculateAndPush;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.ProgressBar progressBarCalculate;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.SaveFileDialog saveFileDialogExport;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.Button buttonRobot;
        private System.Windows.Forms.NumericUpDown numericUpDownBarrier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonResetRobotData;
    }
}

