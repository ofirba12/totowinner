﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotoWinner.Common.Infra;
using TotoWinner.Common;
namespace TotoWinner.Groups
{
    public class GroupsRepositoryServices : Singleton<GroupsRepositoryServices>, IGroupsRepositoryServices
    {
        public Dictionary<int, Group> Groups { get; private set; } //groupId, Group
        public Dictionary<int, Dictionary<int, List<string>>> UnionGroupGamesColumnsResults { get; private set; } //<groupid,<columnId, list<result>>>
        public int MaxGroupsWithOneGame { get; private set; }
        private List<string> allowedResults = new List<string>()
        {
            "1",
            "2",
            "X",
            //"12",
            //"1X",
            //"2X",
            //"12X"
        };
        private GroupsRepositoryServices()
        {
            this.Groups = new Dictionary<int, Group>();
            this.UnionGroupGamesColumnsResults = new Dictionary<int, Dictionary<int, List<string>>>();
        }
        internal string ValidateGroups()
        {
            var errorMessage = string.Empty;
            var columnCounter = 0;
            foreach (var groupId in this.Groups.Keys)
            {
                var pivot = PivotColumns(this.Groups[groupId].GameColumns);
                //validate games are in acending order
                var keys = string.Join("", this.Groups[groupId].GameColumns.Keys);
                var orderedKeys = string.Join("", this.Groups[groupId].GameColumns.Keys.OrderBy(i =>i));
                if (keys != orderedKeys)
                {
                    errorMessage = $"Games order is not ascending in group {groupId}, found: {keys} expected: {orderedKeys}";
                    break;
                }
                columnCounter += this.Groups[groupId].GameColumns.Keys.Count();

                var verifier = new Dictionary<string, int>();
                foreach (var column in pivot)
                {
                    var hash = string.Join(",", column.Value);
                    if (verifier.ContainsKey(hash))
                    {
                        errorMessage = $"Dumplicate Column found in group {groupId}, column_{verifier[hash]} = column_{column.Key}";
                        break;
                    }
                    else
                        verifier.Add(hash, column.Key);
                }
            }
            if (string.IsNullOrEmpty(errorMessage) && columnCounter != this.MaxGroupsWithOneGame)
                errorMessage = $"There are invalid number of rows (games), found {columnCounter}, expected {this.MaxGroupsWithOneGame}";

            return errorMessage;
        }

        internal string DumpAllColumns()
        {
            try
            {
                var dump = new StringBuilder();
                var columns = GenerateAllColumns();
                foreach (var column in columns)
                {
                    var ordered = column.OrderBy(x => x.Key).ToDictionary(x=> x.Key, x=> x.Value);
                    dump.AppendLine(string.Join(",", ordered.Values));
                }
                return dump.ToString();
            }
            catch
            {

            }
            return string.Empty;
        }
        public List<Dictionary<int, string>> GenerateAllColumns()
        {
            var groupId = 1;
            var all = new List<Dictionary<int, string>>();
            Dictionary<int, Dictionary<int, string>> pivot1 = null;
            if (this.Groups.Count > 1)
            {
                while (groupId < this.Groups.Count)
                {
                    if (groupId == 1)
                    {
                        pivot1 = PivotColumns(this.Groups[groupId].GameColumns);
                    }
                    else
                    {
                        pivot1 = new Dictionary<int, Dictionary<int, string>>();
                        for (var i = 1; i <= all.Count; i++)
                            pivot1.Add(i, all[i - 1]);
                    }
                    var pivot2 = this.Groups.ContainsKey(groupId + 1)
                        ? PivotColumns(this.Groups[groupId + 1].GameColumns)
                        : null;
                    if (pivot2 != null)
                    {
                        all.Clear();
                        foreach (var column1 in pivot1)
                            foreach (var column2 in pivot2)
                            {
                                var dic1 = column1.Value.ToDictionary(x => x.Key, x => x.Value);
                                var dic2 = column2.Value.ToDictionary(x => x.Key, x => x.Value);
                                var dics = dic1.Union(dic2).ToDictionary(k => k.Key, v => v.Value);
                                all.Add(dics);
                            }
                        groupId += 1;
                    }
                }
            }
            else
            {
                pivot1 = PivotColumns(this.Groups[groupId].GameColumns);
                foreach (var column1 in pivot1)
                {
                    var dic = column1.Value.ToDictionary(x => x.Key, x => x.Value);
                    all.Add(dic);
                }
            }
            return all;

        }
        public Dictionary<int, Dictionary<int, string>> PivotColumns(GroupGamesColumnsResults gameColumns)
        {
            var pivoting = new Dictionary<int, Dictionary<int,string>>(); //<column, results>
            foreach (var gameId in gameColumns.Keys)
            {
                foreach (var columnId in gameColumns[gameId].Keys)
                {
                    if (!pivoting.ContainsKey(columnId))
                        pivoting.Add(columnId, new Dictionary<int, string>());
                    if (!pivoting[columnId].ContainsKey(gameId))
                        pivoting[columnId].Add(gameId, gameColumns[gameId][columnId]);
                }
            }
            return pivoting;
        }
        public Dictionary<int, List<string>> PivotColumnsOrderBy(GroupGamesColumnsResults gameColumns)
        {
            var piv = PivotColumns(gameColumns);
            var result = new Dictionary<int, List<string>>();
            foreach (var column in piv.Keys)
            {
                piv[column].OrderBy(item => item.Key);
                var list = new List<string>();
                foreach (var gameResult in piv[column].Values)
                    list.Add(gameResult);
                result.Add(column, list);
            }
            return result;
        }

        #region Union Methods
        public void UnionGroups() //This methods has been verified when each cell in the grid holds only 1 value
        {
            foreach (var groupId in this.Groups.Keys)
            {
                var union = UnionSingleGroup(this.Groups[groupId].GameColumns);
                if (!this.UnionGroupGamesColumnsResults.ContainsKey(groupId))
                    this.UnionGroupGamesColumnsResults.Add(groupId, null);
                this.UnionGroupGamesColumnsResults[groupId] = union;
            }
        }
        private Dictionary<int, List<string>> UnionSingleGroup(GroupGamesColumnsResults gameColumns)
        {
            var pivoting = PivotColumnsOrderBy(gameColumns);
            var result = UnionAlgo(pivoting);
            result = RemoveColumnAlreadyContainedInOtherColumn(result);
            return result;
        }
        private Dictionary<int, List<string>> RemoveColumnAlreadyContainedInOtherColumn(Dictionary<int, List<string>> unionResult)
        {
            if (unionResult.Count == 1)
                return unionResult;
            var result = new Dictionary<int, List<string>>();
            var flag = false;
            for (var candidateIndex = 1; candidateIndex <= unionResult.Count && !flag; candidateIndex++)
            {
                var column = 1;
                for (; column <= unionResult.Count && !flag; column++)
                {
                    if (!flag)
                    {
                        if (column == candidateIndex)
                            continue;

                        if (!unionResult[candidateIndex].ContainsPerIndex(unionResult[column]))
                        {
                            if (!result.ContainsKey(candidateIndex))
                                result.Add(candidateIndex, unionResult[candidateIndex]);
                            if (!result.ContainsKey(column))
                                result.Add(column, unionResult[column]);
                        }
                        else
                        {
                            unionResult.Remove(column);
                            result = unionResult;
                            flag = true;
                        }
                    }
                }
            }
            if (flag)
            {
                var reIndexedColumns = ReIndexColumns(result);
                if (reIndexedColumns.Count > 1)
                    result = RemoveColumnAlreadyContainedInOtherColumn(reIndexedColumns);
                else
                    result = reIndexedColumns;
            }
            return result;
        }
        private Dictionary<int, List<string>> ReIndexColumns(Dictionary<int, List<string>> columns)
        {
            var pivoting = new Dictionary<int, List<string>>();
            var col = 1;
            foreach (var pivot in columns)
            {
                pivoting.Add(col, pivot.Value);
                col++;
            }
            return pivoting;
        }
        private Dictionary<int, List<string>> UnionAlgo(Dictionary<int, List<string>> pivoting)
        {
            var pivoting2 = new Dictionary<int, List<string>>();
            var flag = false;
            for (var candidateIndex = 1; candidateIndex <= pivoting.Count && !flag; candidateIndex++)
            {
                var column = 1;
                for (; column <= pivoting.Count && !flag; column++)
                {
                    if (!flag)
                    {
                        if (column == candidateIndex)
                            continue;
                        var result = UnionColumns(pivoting[candidateIndex], pivoting[column]);
                        if (!pivoting2.ContainsKey(column) && result != null)
                        {
                            pivoting2.Add(candidateIndex, result);
                            flag = true;
                        }
                    }
                }
                if (flag)
                {
                    var joinedWith = column - 1;
                    for (var col2 = 1; col2 <= pivoting.Count; col2++)
                    {
                        if (col2 != candidateIndex && col2 != joinedWith)
                        {
                            pivoting2[col2] = pivoting[col2];
                        }
                    }
                    var reIndexedColumns = ReIndexColumns(pivoting2);
                    return UnionAlgo(reIndexedColumns);
                }
            }
            return pivoting;
        }
        private List<string> UnionColumns(List<string> list1, List<string> list2)
        {
            var diffCounter = 0;
            var diffIndex = -1;
            for (var index = 0; index < list1.Count; index++)
            {
                if (list1[index] != list2[index])
                {
                    diffIndex = index;
                    diffCounter++;
                }
            }
            if (diffCounter == 1)
            {
                var newColumn = string.Join("", list1[diffIndex].Union(list2[diffIndex]));
                list1[diffIndex] = newColumn;
                return list1;
            }
            return null;
        }
        #endregion

        #region UI Supportive Methods
        public void AddGroup(int groupId, Group group)
        {
            this.Groups.Add(groupId, group);
        }
        public void SetMaxGroupWithOneGame(int max)
        {
            this.MaxGroupsWithOneGame = max;
        }

        internal string Validate(int groupId, int rowIndex, int columnIndex, string v)
        {
            switch (columnIndex)
            {
                case 0:
                    return ValidateGame(groupId, rowIndex, v);
                default:
                    return ValidateColumn(groupId, rowIndex, columnIndex, v);
            }
        }

        private string ValidateColumn(int groupId, int rowIndex, int columnIndex, string v)
        {
            var errMessage = string.Empty;
            var sortedInput = new string(v.ToList().OrderBy(i => i).ToArray());
            if (v.Count() > 1 ||
               (!allowedResults.Contains(sortedInput)))
            {
                return $"Column {columnIndex} has invalid value[{v}]. only 1 char is allowed out of 1X2";
            }
            return errMessage;
        }
        private string ValidateGame(int groupId, int rowIndex, string v)
        {
            var errMessage = string.Empty;
            if (!Int32.TryParse(v, out var game))
                return $"Invalid game number {v}, at row {rowIndex}";

            if (this.Groups[groupId].GameIdIndex.TryGetValue(game, out var rowIdCheck))
            {
                if (rowIdCheck != rowIndex)
                    return $"Game {v} was already set";
            }
            if (game > this.MaxGroupsWithOneGame)
                return $"Game must be les equal {this.MaxGroupsWithOneGame}. Invalid value {v}";
            var gameCrossGroupsCheck = this.Groups.Values.Where(g => g.GameIdIndex.ContainsKey(game)).FirstOrDefault();
            if (gameCrossGroupsCheck != null && gameCrossGroupsCheck.GroupId != groupId)
                return $"Game {game} already exists in Group {gameCrossGroupsCheck.GroupId}.";
            return errMessage;
        }

        internal void ResetRobotData()
        {
            foreach (var group in this.Groups)
            {
                group.Value.GameColumns.ResetRobotData();
            }
        }

        internal void Clear()
        {
            foreach (var group in this.Groups.Values)
                group.Dispose();
            this.Groups.Clear();
        }

        internal void RemoveGroupGame(int groupId)
        {
            this.Groups[groupId].RefreshData();
        }

        internal void DeleteGroup(int groupId)
        {
            this.Groups.Remove(groupId);
            var newGroups = new Dictionary<int, Group>();
            for (var key = 1; key <= this.Groups.Keys.Count; key++)
            {
                if (!this.Groups.ContainsKey(key))
                {
                    var group = this.Groups[key + 1];
                    group.SetGroupId(key);
                    newGroups.Add(key, group);
                    group.RefreshGroupName();
                }
                else
                    newGroups.Add(key, this.Groups[key]);
            }
            this.Groups = newGroups;
        }

        internal void DeleteColumn(int groupId, int colIndexGridView)
        {
            var newGameColumns = new Dictionary<int, Dictionary<int, string>>();
            foreach (var game in this.Groups[groupId].GameColumns.Keys)
            {
                var newColumnIdResult = new Dictionary<int, string>();
                var row = this.Groups[groupId].GameColumns[game];
                for (var columnId = 1; columnId <= row.Keys.Count; columnId++)
                {
                    if (columnId < colIndexGridView)
                    {
                        newColumnIdResult.Add(columnId, row[columnId]);
                    }
                    else if (columnId > colIndexGridView)
                    {
                        newColumnIdResult.Add(columnId - 1, row[columnId]);
                    }
                }
                newGameColumns.Add(game, newColumnIdResult);
            }
            this.Groups[groupId].SetGameColumns(newGameColumns);
        }
        internal StringBuilder DumpGroups()
        {
            var dump = new StringBuilder();
            foreach (var groupId in GroupsRepositoryServices.Instance.Groups.Keys)
            {
                dump.AppendLine($"Group {groupId}:");
                foreach (var game in GroupsRepositoryServices.Instance.Groups[groupId].GameIdIndex)
                {
                    dump.AppendLine($"Game {game.Key} located in row {game.Value}");
                    foreach (var column in GroupsRepositoryServices.Instance.Groups[groupId].GameColumns[game.Key])
                        dump.AppendLine($"Column {column.Key} = [{column.Value}]");
                }
            }
            return dump;
        }
        internal StringBuilder DumpUnionGroups()
        {
            var dump = new StringBuilder();
            foreach (var groupId in GroupsRepositoryServices.Instance.UnionGroupGamesColumnsResults.Keys)
            {
                dump.AppendLine($"Group {groupId}:");
                foreach (var column in GroupsRepositoryServices.Instance.UnionGroupGamesColumnsResults[groupId].Keys.OrderBy(k => k))
                {
                    dump.AppendLine($"Column {column}: [{string.Join(",", GroupsRepositoryServices.Instance.UnionGroupGamesColumnsResults[groupId][column])}]");
                }
            }
            return dump;
        }
        #endregion
    }
}
