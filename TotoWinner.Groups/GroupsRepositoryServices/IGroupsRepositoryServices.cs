﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Groups
{
    public interface IGroupsRepositoryServices
    {
        void AddGroup(int groupId, Group group);
        void SetMaxGroupWithOneGame(int max);
    }
}
