﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Groups
{
    public class GroupGamesColumnsResults
    {
        public Dictionary<int, Dictionary<int, string>> GameIdColumnIdResult { get; private set; } //<gameId, <columnId, result>>
        public Dictionary<int, Dictionary<int, string>> AuditGameIdColumnIdResult { get; private set; } //<gameId, <columnId, result>>
        public Dictionary<int, string> this[int gameId]
        {
            get
            {
                return this.GameIdColumnIdResult[gameId];
            }
        }
        public Dictionary<int, Dictionary<int, string>>.KeyCollection Keys
        {
            get
            {
                return this.GameIdColumnIdResult.Keys;
            }
        }
        public GroupGamesColumnsResults()
        {
            this.GameIdColumnIdResult = new Dictionary<int, Dictionary<int, string>>();
            this.AuditGameIdColumnIdResult = new Dictionary<int, Dictionary<int, string>>();
        }
        public void Clear()
        {
            this.GameIdColumnIdResult.Clear();
            this.AuditGameIdColumnIdResult.Clear();
        }
        //FOR TESTING ONLY
        public void SetGameColumns(Dictionary<int, Dictionary<int, string>> gameColumns) 
        {
            this.GameIdColumnIdResult = gameColumns;
            this.AuditGameIdColumnIdResult = gameColumns;
        }
        public void SetGameResult(int gameId, int column, string result)
        {
            if (!this.GameIdColumnIdResult[gameId].ContainsKey(column))
            {
                this.GameIdColumnIdResult[gameId].Add(column, result);
                this.AuditGameIdColumnIdResult[gameId].Add(column, result);
            }
            else
            {
                this.GameIdColumnIdResult[gameId][column] = result;
                this.AuditGameIdColumnIdResult[gameId][column] = result;
            }
        }
        public void Add(int gameId)
        {
            this.GameIdColumnIdResult.Add(gameId, new Dictionary<int, string>());
            this.AuditGameIdColumnIdResult.Add(gameId, new Dictionary<int, string>());
        }
        public void AddGameResult(int gameId, int column, string result)
        {
            if (result != null)
            {
                this.GameIdColumnIdResult[gameId].Add(column, result);
                this.AuditGameIdColumnIdResult[gameId].Add(column, result);
            }
        }

        internal void RemoveRobotGameResult(int gameId)
        {
            var column = this.GameIdColumnIdResult[gameId].Keys.Count;
            this.GameIdColumnIdResult[gameId].Remove(column);
        }

        public void AddRobotGameResult(int gameId, string result)
        {
            if (result != null)
            {
                var column = this.GameIdColumnIdResult[gameId].Keys.Count + 1;
                this.GameIdColumnIdResult[gameId].Add(column, result);
            }
        }
        public void ResetRobotData()
        {
            this.GameIdColumnIdResult.Clear();
            foreach (var gameId in this.AuditGameIdColumnIdResult.Keys)
            {
                this.GameIdColumnIdResult.Add(gameId, new Dictionary<int, string>());
                foreach (var kpv in this.AuditGameIdColumnIdResult[gameId])
                    this.GameIdColumnIdResult[gameId].Add(kpv.Key, kpv.Value);
            }
        }
    }
}
