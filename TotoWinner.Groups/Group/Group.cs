﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotoWinner.Groups
{
    public partial class Group : IDisposable
    {
        public int GroupId { get; private set; }
        public GroupGamesColumnsResults GameColumns { get; private set; } //<gameId, <columnId, result>>
        public Dictionary<int, int> IndexGameId { get; } //rowIndex, gameId
        public Dictionary<int, int> GameIdIndex { get; } //gameId, rowIndex 
        public bool DoNotUseInRobot
        {
            get
            {
                return this.checkBoxDoNotUseRobot.Checked;
            }
        }
        public Group(int id)
        {
            this.GroupId = id;
            this.GameColumns = new GroupGamesColumnsResults();//new Dictionary<int, Dictionary<int, string>>();
            this.IndexGameId = new Dictionary<int, int>();
            this.GameIdIndex = new Dictionary<int, int>();
            this.initUI();
        }
        public void SetGameColumns(Dictionary<int, Dictionary<int, string>> gameColumns)
        {
            this.GameColumns.Clear();
            this.GameColumns.SetGameColumns(gameColumns);
        }
        public void SetGame(int gameId, int rowIndex)
        {
            RefreshData();
        }
        public void SetGroupId(int groupId)
        {
            this.GroupId = groupId;
        }
        public void SetGameResult(int gameId, int column, string result)
        {
            RefreshData();
            this.GameColumns.SetGameResult(gameId, column, result);
        }

        internal void RefreshData()
        {
            this.GameIdIndex.Clear();
            this.IndexGameId.Clear();
            this.GameColumns.Clear();
            for (var rowIndex = 0; rowIndex < this.dataGridView.Rows.Count; rowIndex++)
            {
                var row = this.dataGridView.Rows[rowIndex];
                if (row.IsNewRow)
                    continue;
                if (row.Cells[0].Value != null && 
                    Int32.TryParse(row.Cells[0].Value.ToString(), out var gameId))
                {
                    this.IndexGameId.Add(rowIndex, gameId);
                    this.GameIdIndex.Add(gameId, rowIndex);
                    this.GameColumns.Add(gameId);
                    for (var colIndex = 1; colIndex < row.Cells.Count; colIndex++)
                    {
                        var result = row.Cells[colIndex].Value?.ToString();
                        this.GameColumns.AddGameResult(gameId, colIndex, result);
                        //if (result != null)
                        //    this.GameColumns[gameId].Add(colIndex, result);
                    }
                }
            }
        }

    }
}
