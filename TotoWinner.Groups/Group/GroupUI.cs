﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TotoWinner.Groups
{
    public partial class Group : IDisposable
    {
        private System.ComponentModel.IContainer components = null;

        public System.Windows.Forms.TabPage TabPage { get; private set; }
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn GameNumber;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteRowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteColumnToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxDoNotUseRobot;

        private int rowIndexGridView = 0;
        private int colIndexGridView = 0;
        private List<string> allowedResults = new List<string>()
        {
            "1",
            "2",
            "X",
            "12",
            "1X",
            "2X",
            "12X"
        };
        public void Dispose()
        {
            if (this.components != null)
            {
                components.Dispose();
            }
        }

        private void initUI()
        {
            this.components = new System.ComponentModel.Container();
            this.TabPage = new System.Windows.Forms.TabPage();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.GameNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxDoNotUseRobot = new CheckBox();

            this.TabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.contextMenuStrip.SuspendLayout();

            this.TabPage.Controls.Add(this.dataGridView);
            this.TabPage.Controls.Add(this.checkBoxDoNotUseRobot);
            this.TabPage.Location = new System.Drawing.Point(4, 22);
            this.TabPage.Name = $"group{this.GroupId}";
            this.TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage.Size = new System.Drawing.Size(837, 400);
            this.TabPage.TabIndex = 0;
            this.TabPage.Text = $"Group {this.GroupId}";
            this.TabPage.UseVisualStyleBackColor = true;

            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.EnableHeadersVisualStyles = false;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GameNumber});
            this.dataGridView.Location = new System.Drawing.Point(7, 7);
            this.dataGridView.Name = $"dataGridView{this.GroupId}";
            this.dataGridView.Size = new System.Drawing.Size(824, 240);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseUp);
            this.dataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView_CellValidating);
            this.dataGridView.RowsRemoved += DataGridView_RowsRemoved;
            this.dataGridView.CellValueChanged += DataGridView_CellValueChanged;
            this.dataGridView.CellEndEdit += DataGridView_CellEndEdit;

            this.GameNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GameNumber.DefaultCellStyle = dataGridViewCellStyle1;
            this.GameNumber.HeaderText = "Game";
            this.GameNumber.Name = "GameNumber";
            this.GameNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.GameNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GameNumber.Width = 41;

            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addColumnToolStripMenuItem,
            this.deleteRowToolStripMenuItem,
            this.deleteColumnToolStripMenuItem});
            this.contextMenuStrip.Name = $"contextMenuStrip{this.GroupId}";
            this.contextMenuStrip.Size = new System.Drawing.Size(154, 70);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);

            this.addColumnToolStripMenuItem.Name = "addColumnToolStripMenuItem";
            this.addColumnToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addColumnToolStripMenuItem.Text = "Add Column";
            this.addColumnToolStripMenuItem.Click += new System.EventHandler(this.addColumnToolStripMenuItem_Click);

            this.deleteRowToolStripMenuItem.Name = "deleteRowToolStripMenuItem";
            this.deleteRowToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.deleteRowToolStripMenuItem.Text = "Delete Row";
            this.deleteRowToolStripMenuItem.Click += new System.EventHandler(this.deleteRowToolStripMenuItem_Click);

            this.deleteColumnToolStripMenuItem.Name = "deleteColumnToolStripMenuItem";
            this.deleteColumnToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.deleteColumnToolStripMenuItem.Text = "Delete Column";
            this.deleteColumnToolStripMenuItem.Click += new System.EventHandler(this.deleteColumnToolStripMenuItem_Click);

            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();

            this.checkBoxDoNotUseRobot.AutoSize = true;
            this.checkBoxDoNotUseRobot.Location = new Point(0, this.dataGridView.Size.Height + 10);
            this.checkBoxDoNotUseRobot.Name = "checkBoxUseRobot";
            this.checkBoxDoNotUseRobot.Size = new System.Drawing.Size(130, 17);
            this.checkBoxDoNotUseRobot.TabIndex = 18;
            this.checkBoxDoNotUseRobot.Text = "Do Not Use Robot";
            this.checkBoxDoNotUseRobot.UseVisualStyleBackColor = true;
            this.contextMenuStrip.ResumeLayout(false);
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            var value = this.dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (e.ColumnIndex == 0)
            {
                var gameId = Int32.Parse(value);
                SetGame(gameId, e.RowIndex);
                this.dataGridView.Rows[e.RowIndex].Cells[0].Style.BackColor = Color.LightGreen;
            }
            else if (this.dataGridView.Rows[e.RowIndex].Cells[0].Value != null)
            {
                var gameId = Int32.Parse(this.dataGridView.Rows[e.RowIndex].Cells[0].Value.ToString());
                SetGameResult(gameId, e.ColumnIndex, value);
            }
        }

        public void RefreshGroupName()
        {
            this.TabPage.Name = $"group{this.GroupId}";
            this.TabPage.Text = $"Group {this.GroupId}";
        }
        private void DataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > 0 && this.dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                var cell = this.dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper();
                this.dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = cell;
            }
        }

        private void DataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            GroupsRepositoryServices.Instance.RemoveGroupGame(this.GroupId);
        }

        private void dataGridView_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex >= 0)
            {
                this.dataGridView.Rows[e.RowIndex].Selected = true;
                this.rowIndexGridView = e.RowIndex;
                this.colIndexGridView = e.ColumnIndex;
                this.contextMenuStrip.Show(this.dataGridView, e.Location);
                contextMenuStrip.Show(Cursor.Position);
            }
        }
        private void addColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var nextIndex = this.dataGridView.Columns.Count;
            var newColumn = new DataGridViewTextBoxColumn();
            newColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            var columnStyle = new System.Windows.Forms.DataGridViewCellStyle();
            newColumn.DefaultCellStyle = columnStyle;
            newColumn.HeaderCell.Style.BackColor = Color.LightYellow;
            newColumn.HeaderText = $"{nextIndex}";
            newColumn.Name = $"Column_{this.GroupId}_{nextIndex}";
            newColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            newColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            newColumn.Width = 21;
            this.dataGridView.Columns.Add(newColumn);
        }

        private void deleteRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.dataGridView.Rows.RemoveAt(this.rowIndexGridView);
        }

        private void deleteColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var message = $"Would you like to delete column {this.colIndexGridView}?";
            var yesNoDialog = MessageBox.Show(message, "Delete Group", MessageBoxButtons.YesNo);
            if (yesNoDialog == DialogResult.No)
                return;
            GroupsRepositoryServices.Instance.DeleteColumn(this.GroupId, this.colIndexGridView);
            this.dataGridView.Columns.RemoveAt(this.colIndexGridView);
            for(var columnId = this.colIndexGridView; columnId < this.dataGridView.Columns.Count; columnId++)
            {
                this.dataGridView.Columns[columnId].HeaderText = $"{columnId}";
                this.dataGridView.Columns[columnId].Name = $"Column_{this.GroupId}_{columnId}";
            }
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.deleteColumnToolStripMenuItem.Enabled = this.dataGridView.Columns.Count > 2;
            this.deleteRowToolStripMenuItem.Enabled = this.dataGridView.Rows.Count > 0 && !this.dataGridView.Rows[this.rowIndexGridView].IsNewRow;
        }

        private void dataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            var value = e.FormattedValue.ToString().ToUpper();
            if (this.dataGridView.Rows[e.RowIndex].IsNewRow)
            {
                dataGridView.Rows[e.RowIndex].ErrorText = string.Empty;
                return;
            }
            var message = GroupsRepositoryServices.Instance.Validate(this.GroupId, e.RowIndex, e.ColumnIndex, value);
            if (!string.IsNullOrEmpty(message))
            {
                e.Cancel = true;
            }
            dataGridView.Rows[e.RowIndex].ErrorText = message;
        }
    }
}
