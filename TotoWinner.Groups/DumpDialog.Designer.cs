﻿namespace TotoWinner.Groups
{
    partial class DumpDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxDump = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richTextBoxDump
            // 
            this.richTextBoxDump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxDump.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxDump.Name = "richTextBoxDump";
            this.richTextBoxDump.ReadOnly = true;
            this.richTextBoxDump.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.richTextBoxDump.Size = new System.Drawing.Size(541, 450);
            this.richTextBoxDump.TabIndex = 0;
            this.richTextBoxDump.Text = "";
            // 
            // DumpDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 450);
            this.Controls.Add(this.richTextBoxDump);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DumpDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DumpDialog";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxDump;
    }
}