﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TotoWinner.Groups
{
    public partial class DumpDialog : Form
    {
        public DumpDialog()
        {
            InitializeComponent();
        }
        public void Show(string title, string messageText)
        {
            Text = title;
            this.richTextBoxDump.Text = messageText;
            this.Show(this.Owner);
        }
    }
}
